@extends('Backend.layouts.app',['menu'=>'payment_history'])

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-heart"></i> {{__('Purchase history')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table">
                        <thead>
                        <tr>
                            <th>{{__('Package name')}}</th>
                            <th>{{__('Package type')}}</th>
                            <th>{{__('Price')}} ({{get_currency()}})</th>
                            <th>{{__('Validity')}}</th>
                            <th>{{__('Listing')}}</th>
                            <th>{{__('Media')}}</th>
                            <th>{{__('Payment method')}}</th>
                            <th>{{__('Purchase date')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: false,
                ajax: '{{url('listing-manager-payment-history')}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.spotlist_directory_id);
                },
                columns: [
                    {"data": "spotlist_packages_name"},
                    {"data": "packages_type"},
                    {"data": "price"},
                    {"data": "day_validity"},
                    {"data": "num_of_listing"},
                    {"data": "num_of_photos"},
                    {"data": "payment_method"},
                    {"data": "purchase_date"},
                ],
            });
        });

    </script>
@endsection
