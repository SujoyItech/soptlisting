@extends('admin.layouts.master')
@section('title','Profile')
@section('content')
<div class="content">

    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">{{__('Profile')}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="card-box text-center">
                    <img src="{{aws_path().getImagePath('profile_picture').\Illuminate\Support\Facades\Auth::user()->picture}}" class="rounded-circle avatar-lg img-thumbnail"
                         alt="profile-image">

                    <h4 class="mb-0">{{$profiles->name ?? ''}}</h4>
                    <p class="text-muted">{{__('Admin')}}</p>

                    <div class="text-left mt-3">
                        <h4 class="font-13 text-uppercase">{{__('About Me')}}</h4>
                        <table class="table table-borderless" width="50%">
                            <tbody>
                            <tr>
                                <td width="12%">{{__('Full Name :')}}</td>
                                <td>{{$profiles->name ?? ''}}</td>
                            </tr>
                            <tr>
                                <td  width="12%">{{__('Mobile :')}}</td>
                                <td>{{$profiles->phone ?? ''}}</td>
                            </tr>
                            <tr>
                                <td  width="12%">{{__('Email :')}}</td>
                                <td>{{$profiles->email ?? ''}}</td>
                            </tr>
                            <tr>
                                <td  width="12%">{{__('Language :')}}</td>
                                <td>{{$profiles->language ?? ''}}</td>
                            </tr>
                            <tr>
                                <td  width="12%">{{__('Location :')}}</td>
                                <td>{{$profiles->country ?? ''}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <ul class="social-list list-inline mt-3 mb-0">
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i
                                        class="mdi mdi-facebook"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i
                                        class="mdi mdi-google"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-info text-info"><i
                                        class="mdi mdi-twitter"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i
                                        class="mdi mdi-github"></i></a>
                        </li>
                    </ul>
                </div> <!-- end card-box -->
            </div> <!-- end col-->
        </div>
    </div>
</div>
@endsection