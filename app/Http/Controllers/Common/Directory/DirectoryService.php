<?php


namespace App\Http\Controllers\Common\Directory;

use App\Http\Service\NotificationService;
use App\Traits\DataReturn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DirectoryService {

    use DataReturn;

    public function getDirectoryInfo($id){
        if ($id != NULL){
            $directory_info = DB::table('spotlist_directory')->where('spotlist_directory_id',$id)->first();
        }else{
            $directory_info = DB::table('spotlist_directory')->orderBy('spotlist_directory_id')->get();
        }
        return $directory_info;
    }

    public function getDirectoryAllInfo($id){
        $data['directory'] = $this->getDirectoryInfo($id);
        $data['directory_features'] = DB::table('spotlist_directory_vs_features')->where('spotlist_directory_id',$id)->pluck('spotlist_features_id')->toArray();
        $data['directory_gallery'] = DB::table('spotlist_directory_gallary')->where('spotlist_directory_id',$id)->get();
        $data['directory_schedule'] = $this->directorySchedule($id);
        $data['directory_categories'] = DB::table('spotlist_directory_vs_category')->where('spotlist_directory_id',$id)->pluck('spotlist_category_id')->toArray();
        if (isset($data['directory']->spotlist_parent_category_id) && $data['directory']->spotlist_parent_category_id == CATEGORY_HOTEL){
            $data['hotel_data'] = DB::table('spotlist_directory_type_hotel_room')->where('spotlist_directory_id',$id)->get();
        }elseif (isset($data['directory']->spotlist_parent_category_id) && $data['directory']->spotlist_parent_category_id == CATEGORY_RESTAURANT){
            $data['restaurant_data'] = DB::table('spotlist_directory_type_restaurant')->where('spotlist_directory_id',$id)->get();
        }elseif (isset($data['directory']->spotlist_parent_category_id) && $data['directory']->spotlist_parent_category_id == CATEGORY_BEAUTY){
            $data['beauty_data'] = DB::table('spotlist_directory_type_beauty')->where('spotlist_directory_id',$id)->get();
        }elseif (isset($data['directory']->spotlist_parent_category_id) && $data['directory']->spotlist_parent_category_id == CATEGORY_SHOPPING){
            $data['shop_data'] = DB::table('spotlist_directory_type_shop')->where('spotlist_directory_id',$id)->get();
        }elseif (isset($data['directory']->spotlist_parent_category_id) && $data['directory']->spotlist_parent_category_id == CATEGORY_FITNESS){
            $data['fitness_data'] = DB::table('spotlist_directory_type_fitness')->where('spotlist_directory_id',$id)->get();
        }

        return $data;
    }

    private function directorySchedule($id){
        $post_data = [];
        $schedule = DB::table('spotlist_directory_schedule')->where('spotlist_directory_id',$id)->first();
        if (!empty($schedule->monday)){
            $monday = explode('-',$schedule->monday);
            $post_data['monday_from'] = $monday[0];
            $post_data['monday_to'] = $monday[1];
        }
        if (!empty($schedule->tuesday)){
            $tuesday = explode('-',$schedule->tuesday);
            $post_data['tuesday_from'] = $tuesday[0];
            $post_data['tuesday_to'] = $tuesday[1];
        }
        if (!empty($schedule->wednesday)){
            $wednesday = explode('-',$schedule->wednesday);
            $post_data['wednesday_from'] = $wednesday[0];
            $post_data['wednesday_to'] = $wednesday[1];
        }
        if (!empty($schedule->thursday)){
            $thursday = explode('-',$schedule->thursday);
            $post_data['thursday_from'] = $thursday[0];
            $post_data['thursday_to'] = $thursday[1];
        }
        if (!empty($schedule->friday)){
            $friday = explode('-',$schedule->friday);
            $post_data['friday_from'] = $friday[0];
            $post_data['friday_to'] = $friday[1];
        }
        if (!empty($schedule->saturday)){
            $saturday = explode('-',$schedule->saturday);
            $post_data['saturday_from'] = $saturday[0];
            $post_data['saturday_to'] = $saturday[1];
        }

        if (!empty($schedule->sunday)){
            $sunday = explode('-',$schedule->sunday);
            $post_data['sunday_from'] = $sunday[0];
            $post_data['sunday_to'] = $sunday[1];
        }
        return $post_data;
    }

    public function getDirectoryList($status='',$user_id='',$verification_status='',$directory_id=''){

        $directory_list = DB::table('spotlist_directory')
        ->leftJoin('spotlist_countries','spotlist_directory.country_id','spotlist_countries.id')
        ->leftJoin('sys_users','spotlist_directory.sys_users_id','sys_users.id')
        ->leftJoin('spotlist_cities','spotlist_directory.city','spotlist_cities.spotlist_cities_id')
        ->leftJoin('spotlist_directory_vs_category','spotlist_directory.spotlist_directory_id','spotlist_directory_vs_category.spotlist_directory_id')
        ->leftJoin('spotlist_category','spotlist_directory_vs_category.spotlist_category_id','spotlist_category.spotlist_category_id')
        ->groupBy('spotlist_directory.spotlist_directory_id')
        ->orderBy('spotlist_directory.created_at','desc')
        ->select('spotlist_directory.*','sys_users.name',
            DB::raw("GROUP_CONCAT(spotlist_countries.name) as country_name,GROUP_CONCAT(spotlist_cities.spotlist_cities_name) as city_name,
            GROUP_CONCAT(spotlist_category.spotlist_category_id) as category_id,
            GROUP_CONCAT(spotlist_category.spotlist_category_name) as category_name"));
        if (!empty($directory_id)){
            $directory_list = $directory_list->where('spotlist_directory.spotlist_directory_id',$directory_id);
        }
        if ($status != NULL && $status != 'all'){
            $directory_list = $directory_list->where('spotlist_directory.status',$status);
        }
        if ($user_id != NULL){
            $directory_list = $directory_list->where('spotlist_directory.sys_users_id',$user_id);
        }
        if ($verification_status != NULL && $verification_status != 'all'){
            $directory_list = $directory_list->where('spotlist_directory.is_verified',$verification_status);
        }

        $directory_list = $directory_list->get();

        $diretory_data = [];

        foreach ($directory_list as $key=>$directory){
            $diretory_data[$key]['spotlist_directory_id'] = $directory->spotlist_directory_id;
            $diretory_data[$key]['spotlist_directory_name'] = $directory->spotlist_directory_name;
            $diretory_data[$key]['spotlist_directory_slug'] = $directory->spotlist_directory_slug;
            $diretory_data[$key]['thumb_image'] = $directory->thumb_image;
            $diretory_data[$key]['type'] = $directory->type;
            $diretory_data[$key]['name'] = explode(',',$directory->name)[0];
            $diretory_data[$key]['country_name'] = explode(',',$directory->country_name)[0];
            $diretory_data[$key]['city_name'] = explode(',',$directory->city_name)[0];
            $diretory_data[$key]['category_id'] = explode(',',$directory->category_id);
            $diretory_data[$key]['category_name'] = explode(',',$directory->category_name);
            $diretory_data[$key]['status'] = $directory->status;
            $diretory_data[$key]['is_featured'] = $directory->is_featured;
            $diretory_data[$key]['is_verified'] = $directory->is_verified;
            $diretory_data[$key]['created_at'] = $directory->created_at;
        }

       return $diretory_data;
    }

    public function adminDirectoryStatusChange(Request $request){
        $data = [];
        try {
            $data['success'] = TRUE;
            $directory =  DB::table('spotlist_directory')->where('spotlist_directory_id',$request->value)->first();
            if ($request->type == 'verified'){
                DB::table('spotlist_directory')->where('spotlist_directory_id',$request->value)->update(['is_verified'=>TRUE,'updated_at'=>Carbon::now()]);
                $data['notify_title'] = __('Directory verified');
                $data['notify_message'] = $directory->spotlist_directory_name.__(' verified successful.');
            }else if ($request->type == 'pending'){
                DB::table('spotlist_directory')->where('spotlist_directory_id',$request->value)->update(['status'=>'Inactive','updated_at'=>Carbon::now()]);
                $data['notify_title'] = __('Directory pending');
                $data['notify_message'] = $directory->spotlist_directory_name.__(' make as pending successful.');
            }else if ($request->type == 'active'){
                DB::table('spotlist_directory')->where('spotlist_directory_id',$request->value)->update(['status'=>'Active','updated_at'=>Carbon::now()]);
                $data['notify_title'] = __('Directory active.');
                $data['notify_message'] = $directory->spotlist_directory_name.__(' make as active successful.');
            }else if ($request->type == 'declined'){
                DB::table('spotlist_directory')->where('spotlist_directory_id',$request->value)->update(['status'=>'Declined','updated_at'=>Carbon::now()]);
                $data['notify_title'] = __('Directory declined.');
                $data['notify_message'] = $directory->spotlist_directory_name.__(' make as declined successful.');
            }else if ($request->type == 'add_feature'){
                DB::table('spotlist_directory')->where('spotlist_directory_id',$request->value)->update(['is_featured'=>TRUE,'updated_at'=>Carbon::now()]);
                $data['notify_title'] = __('Directory featured.');
                $data['notify_message'] = $directory->spotlist_directory_name.__(' make as featured successful.');
            }else if ($request->type == 'remove_feature'){
                DB::table('spotlist_directory')->where('spotlist_directory_id',$request->value)->update(['is_featured'=>FALSE,'updated_at'=>Carbon::now()]);
                $data['notify_title'] = __('Directory not featured .');
                $data['notify_message'] = $directory->spotlist_directory_name.__(' make as not featured  successful.');
            }else if ($request->type == 'delete'){
                DB::table('spotlist_directory')->where('spotlist_directory_id',$request->value)->delete();
                $data['notify_title'] = __('Directory deleted.');
                $data['notify_message'] = $directory->spotlist_directory_name.__(' deleted  successful.');
            }
            if ($directory->sys_users_id != Auth::user()->id){
                $this->directoryNotificationSend($directory,$data);
            }

        }catch (\Exception $exception){
            $data['success'] = FALSE;
            $data['message'] = $exception->getMessage();
        }

        return $data;

    }

    public function getWishingList($user_id = NULL){
        $join_table = [
            ['join','spotlist_directory','spotlist_directory_wishinglist.spotlist_directory_id','spotlist_directory.spotlist_directory_id'],
            ['join','sys_users','spotlist_directory_wishinglist.sys_users_id','sys_users.id'],
            ['left','spotlist_category','spotlist_directory.spotlist_parent_category_id','spotlist_category.spotlist_category_id']
            ];
        $select = ['spotlist_directory.spotlist_directory_id', 'spotlist_category.spotlist_category_name', 'spotlist_directory.spotlist_directory_name','spotlist_directory.type',
                          'spotlist_directory.thumb_image', 'sys_users.username', 'spotlist_directory.address', 'spotlist_category.spotlist_category_id',
                          'spotlist_directory.sys_users_id','spotlist_directory_wishinglist.created_at'];
        if ($user_id != NULL){
            $wishingList = $this->joinTableInfo('spotlist_directory_wishinglist',$join_table,$select,[],['spotlist_directory.sys_users_id',$user_id]);
        }else{
            $wishingList = $this->joinTableInfo('spotlist_directory_wishinglist',$join_table,$select);
        }
        return $wishingList;

    }

    private function directoryNotificationSend($directory,$data){

        $notify_body = [
            'title' => $data['notify_title'],
            'body' => $data['notify_message'],
            'directory_name' => $directory->spotlist_directory_name,
            'directory_id' => $directory->spotlist_directory_id,
            'thumb_image' => $directory->thumb_image,
        ];
        $notify_to = [
            'user_id' => $directory->sys_users_id,
            'type' => LISTING_MANAGER_DIRECTORY,
        ];
        $service = new NotificationService();
        $service->sendNotification($notify_body,$notify_to);
    }

    public function getTopDirectoryChart(Request $request){
        $directories = DB::table('spotlist_directory')->orderBy($request->type,'DESC')->limit(10);
        $data = [];
        $data['directory_name'] = $directories->pluck('spotlist_directory_name')->toArray();
        $data['visit_count'] = $directories->pluck('visit_count')->toArray();
        $data['booking_count'] = $directories->pluck('booking_count')->toArray();
        $data['wishing_count'] = $directories->pluck('wishing_count')->toArray();
        return $data;
    }

}
