<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistFeaturesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_features')->delete();
        
        \DB::table('spotlist_features')->insert(array (
            0 => 
            array (
                'spotlist_features_id' => 1,
                'spotlist_features_name' => 'Free wifi',
                'spotlist_features_icon' => 'Backend/images/features/pHICDm2PkPK9pjrN.jpg',
                'created_by' => 2,
                'updated_by' => 2,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'spotlist_features_id' => 2,
                'spotlist_features_name' => 'KIds Zone',
                'spotlist_features_icon' => 'Backend/images/features/6DtOFfK4Y4bt7ACW.jpg',
                'created_by' => 2,
                'updated_by' => 2,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'spotlist_features_id' => 3,
                'spotlist_features_name' => 'Live music',
                'spotlist_features_icon' => 'Backend/images/features/eKnKWcLueai1XDIg.jpeg',
                'created_by' => 2,
                'updated_by' => 2,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'spotlist_features_id' => 4,
                'spotlist_features_name' => 'Refreshment',
                'spotlist_features_icon' => 'Backend/images/features/cMnDXMLL1yx0HM0B.jpeg',
                'created_by' => 2,
                'updated_by' => 2,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'spotlist_features_id' => 5,
                'spotlist_features_name' => 'Gym',
                'spotlist_features_icon' => 'Backend/images/features/KoiSdLFVJR67kD8Y.jpg',
                'created_by' => 2,
                'updated_by' => 2,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'spotlist_features_id' => 6,
                'spotlist_features_name' => 'Swimming Pool',
                'spotlist_features_icon' => 'Backend/images/features/LmfD0ZegSk6kGOHA.jpg',
                'created_by' => 2,
                'updated_by' => 2,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'spotlist_features_id' => 7,
                'spotlist_features_name' => 'Indoor games',
                'spotlist_features_icon' => 'Backend/images/features/9dQOXr12mIFRfhcF.png',
                'created_by' => 2,
                'updated_by' => 2,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));

        
    }
}