<div class="tab-pane" id="basic">
    <form action="{{url('directory-basic-save')}}" class="parsley-examples" method="POST" id="submit_basic" enctype="multipart/form-data">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-form-label" for="spotlist_directory_name">{{__('Directory Name')}}</label>
                    <input type="text" class="form-control" id="spotlist_directory_name" name="spotlist_directory_name" required placeholder="{{__('Directory name')}}" value="{{$directory->spotlist_directory_name ?? ''}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group slug">
                    <label class="col-form-label" for="spotlist_directory_slug">{{__('Directory slug ( This will be shown in your directory url )')}}</label>
                    <input type="text" class="form-control" id="spotlist_directory_slug" name="spotlist_directory_slug" data-slug="" value="{{$directory->spotlist_directory_slug ?? ''}}">
                    <span class="show-text d-none"></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-form-label" for="is_featured">{{__('Featured type')}}</label>
                    @php($featured = [1=>'Featured',0=>'Not featured'])
                    {!! Form::select('is_featured', $featured, $directory->is_featured ?? '', ['class' => 'form-control', 'id'=>'is_featured']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-form-label" for="feature_type">{{__('Category')}}</label>
                    <select class="selectpicker" multiple="true" name="spotlist_category_id[]" id="select_category">
                        @if(isset($sub_categories) && !empty($sub_categories[0]))
                            @foreach($sub_categories as $sub_category)
                                <option value="{{$sub_category->spotlist_category_id}}" {{(old('location') == $sub_category->spotlist_category_id) || in_array($sub_category->spotlist_category_id, $directory_categories ?? []) ? 'selected' : ''}}>{{$sub_category->spotlist_category_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label class="col-form-label" for="description">{{__('Description')}}</label>
                    <textarea id="description" class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-validation-threshold="10">{{$directory->description ?? ''}}</textarea>
                </div>
            </div>
        </div>

        <ul class="list-inline mb-0 wizard">
            @if(isset($directory->spotlist_directory_id) && !empty($directory->spotlist_directory_id))
            <li class="next list-inline-item float-right">
                <button type="submit" class="btn btn-sm btn-success basic_submit btn-next">{{__('Save & Next')}}</button>
            </li>
            @else
                @if(\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER)
                    @if(availableListing(\Illuminate\Support\Facades\Auth::user()->id) > 0)
                        <li class="list-inline-item float-left">
                             <button type="submit" class="btn btn-info basic_submit btn-block"><i class="fa fa-save"></i> {{__('Save Directory')}}</button>
                        </li>
                    @endif
                @else
                    <li class="list-inline-item float-left">
                        <button type="submit" class="btn btn-info basic_submit btn-block"><i class="fa fa-save"></i> {{__('Save Directory')}}</button>
                    </li>
                @endif
            @endif
        </ul>
    </form>
    <input type="hidden" name="spotlist_directory_id" value="{{$directory->spotlist_directory_id ?? ''}}" id="spotlist_directory_id">

</div>

<script>
    $('.basic_submit').on('click', function (e) {
        Ladda.bind(this);
        var load = $(this).ladda();
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    if ($('#spotlist_directory_id').val().length !== 0) {
                        formData.append('spotlist_directory_id', $('#spotlist_directory_id').val());
                    }
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true) {
                            if (response.type == 'update') {
                            } else if (response.type == 'insert') {
                                var redirect_url = "{{url('directory-add')}}" + '/' + response.data.spotlist_directory_id;
                                swalRedirect(redirect_url, response.message, 'success');
                            }
                        }
                    });
                }

            });
    });

    $(document).ready(function (){
        if ($('#spotlist_directory_id').val().length !== 0){
            $('.btn-next').prop('disabled', false);
            $('#spotlist_directory_slug').prop('disabled', true);
        }else {
            $("#spotlist_directory_name").bind("keyup change", function(e) {
                var slug = slugify($(this).val());
                $('#spotlist_directory_slug').val(slug);
                var check_url = "{{route('check-directory-slug')}}";
                var diectory_data = {spotlist_directory_slug : slug}
                makeAjaxPost(diectory_data,check_url,null).done(function (response){
                    if (response.success == true){
                        $('.show-text').removeClass('text-danger');
                        $('.show-text').addClass('text-success');
                        $('.show-text').html('Slug is valid');
                        $('.show-text').removeClass('d-none');
                        $('.btn-info').prop("disabled",false);
                    }else {
                        $('.show-text').removeClass('text-success');
                        $('.show-text').addClass('text-danger');
                        $('.show-text').html('Slug is already exist');
                        $('.show-text').removeClass('d-none');
                        $('.btn-info').prop("disabled",true);
                    }
                })
            })
            $("#spotlist_directory_slug").bind("keyup change", function(e) {
                var slug = slugify($(this).val());
                $('#spotlist_directory_slug').val(slug);
                var check_url = "{{route('check-directory-slug')}}";
                var diectory_data = {spotlist_directory_slug : slug}
                makeAjaxPost(diectory_data,check_url,null).done(function (response){
                    if (response.success == true){
                        $('.show-text').removeClass('text-danger');
                        $('.show-text').addClass('text-success');
                        $('.show-text').html('Slug is valid');
                        $('.show-text').removeClass('d-none');
                        $('.btn-info').prop("disabled",false);
                    }else {
                        $('.show-text').removeClass('text-success');
                        $('.show-text').addClass('text-danger');
                        $('.show-text').html('Slug is already exist');
                        $('.show-text').removeClass('d-none');
                        $('.btn-info').prop("disabled",true);
                    }
                })
            })
            $('.btn-next').prop('disabled', true);
        }
    });
    function slugify(text) {
        return text
            .toString()                     // Cast to string
            .toLowerCase()                  // Convert the string to lowercase letters
            .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
            .trim()                         // Remove whitespace from both sides of a string
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-');        // Replace multiple - with single -
    }

</script>
