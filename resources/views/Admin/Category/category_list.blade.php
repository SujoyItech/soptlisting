@extends('Backend.layouts.app',['menu'=>'category','sub_menu'=>'parent_category_list'])
@section('style')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('Category List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        @if(isset($categories) && !empty($categories[0]))
            @foreach($categories as $category)
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <form method="post" enctype="multipart/form-data" action="{{url('admin-main-category-picture-change')}}" id="category_image_change_{{$category['spotlist_category_id']}}">
                                    <label class="col-form-label" for="cover_image">{{__('Listing cover')}}</label>
                                    <input type="file" name="cover_image" data-plugins="dropify" data-id="{{$category['spotlist_category_id']}}"
                                           data-default-file="{{isset($category['category_image']) ? getImageUrl($category['category_image']) : ''}}"
                                           data-allowed-file-extensions="png jpg jpeg jfif"
                                           data-max-file-size="2M" />
                                </form>
                            </div>
                            <h4 class="header-title mt-2">{{$category['spotlist_category_name']}}</h4>
                            <p class="sub-header font-13">{{count($category['subcategories'])}} {{__('Sub categories')}}</p>
                            @if(isset($category['subcategories']) && count($category['subcategories']) > 0)
                            <ul class="list-group">
                                @foreach($category['subcategories'] as $subcategory)
                                    <li class="list-group-item"> {{$subcategory->spotlist_category_name}}
                                        <span style="float: right;cursor: pointer" class="text-danger subcategory"
                                              data-subcategory="{{$subcategory->spotlist_category_id}}" data-category="{{$category['spotlist_category_id']}}"><strong>x</strong></span></li>
                                @endforeach
                            </ul>
                            @endif
                            <div class="row">
                                <div class="col-4 mt-2 offset-4">
                                    <input type="checkbox" class="check_status" {{$category['status'] == 'Active' ? 'checked' : ''}}
                                    data-category_id="{{$category['spotlist_category_id']}}" data-status="{{$category['status']}}"
                                    data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Enabled" data-off="Disabled">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @endif
    </div>
@endsection
@section('script')
    <script>
        $(function() {
            $('.check_status').change(function(e) {

                var toggle_check = $(this).prop('checked') == true ? $(this).data('on') : $(this).data('off') ;
                var category_status_url = "{{url('admin-main-category-status-change')}}";
                var this_data = $(this);
                var categoryStatusData = new FormData();
                var status = $(this).data('status');
                categoryStatusData.append('spotlist_category_id',$(this).data('category_id'));
                categoryStatusData.append('status',status);

                makeAjaxPostFile(categoryStatusData, category_status_url).done(function (response) {
                    if (response.success == true){
                        swalSuccess(response.message);
                    }else {
                        swalError(response.message);
                    }
                });
            })
        })

        $("input[type=file]").on('change',function(event){
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            var files = $(this)[0].files[0];
            var formData = new FormData();
            formData.append('spotlist_category_id',$(this).data('id'));
            formData.append('category_image',files);
            makeAjaxPostFile(formData, submit_url).done(function (response) {
                if (response.success == true){
                    swalSuccess(response.message);
                }else {
                    swalError(response.message);
                }
            });
        });

        $(document).on('click','.subcategory',function (e){
            var category_url = "{{url('admin-child-category-delete')}}";
            var this_data = $(this);
            var categoryData = new FormData();
            categoryData.append('category_id',$(this).data('category'));
            categoryData.append('subcategory_id',$(this).data('subcategory'));

            swalConfirm('Are You sure to remove this?').then(function (s) {
                if(s.value){
                    makeAjaxPostFile(categoryData, category_url).done(function (response) {
                        if (response.success == true){
                            this_data.closest('li').remove();
                            swalSuccess(response.message);
                        }else {
                            swalError(response.message);
                        }
                    });
                }
            });

        })
    </script>
@endsection
