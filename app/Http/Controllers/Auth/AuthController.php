<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\ForgetPasswordRequest;
use App\Http\Requests\ForgotPasswordResetRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Service\MailService;

use App\Models\User;
use App\Models\UserVerificationCode;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller {
    // Login view
    public function login() {
        if (Auth::user()) {
           if (Auth::user()->default_module_id == SYSTEM_USER){
               return redirect()->route('user.home');
           }else if (Auth::user()->default_module_id == LISTING_MANAGER){
               return redirect()->route('listing-manager-dashboard');
           }else if(Auth::user()->default_module_id == ADMIN){
               return redirect()->route('admin.home');
           }
        } else {
            if(URL::previous() !== url('user-register')){
                Session::put('backUrl', URL::previous());
            }
            return view('auth.login');
        }
    }

    // Login post
    public function postLogin(LoginRequest $request) {
        $service = new AuthService();
        $response = $service->userLogin($request);
        if ($response['success'] == TRUE){
            if (!empty($response['data'])){
                if ($response['data']->default_module_id == SYSTEM_USER){
                    return redirect()->intended('/');
                }else if ($response['data']->default_module_id == LISTING_MANAGER){
                    return redirect()->route('listing-manager-dashboard');
                }else{
                    return redirect()->route('admin.home');
                }
            }else{
                Auth::logout();
                return redirect()->route('login')->with(['success'=>$response['message']]);
            }
        }else{
            Auth::logout();
            return redirect()->route('login')->with(['dismiss'=>$response['message']]);
        }
    }

    public function postLoginModal(LoginRequest $request) {
        $service = new AuthService();
        $response = $service->userLogin($request);

        if ($response['success'] == TRUE){
            if (!empty($response['data'])){
               return response()->json($response);
            }else{
                Auth::logout();
                return response()->json($response);
            }
        }else{
            Auth::logout();
            return response()->json($response);
        }
    }


    // Social login

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToProvider($driver) {
        return Socialite::driver($driver)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($driver) {
        $userSocial = Socialite::driver($driver)->stateless()->user();
        $service = new AuthService();
        $user = $service->loginWithSocial($userSocial,$driver);
        if ($user) {
            if ($user->default_module_id == SYSTEM_USER){
                return redirect()->intended('/')->with(['success'=>__('Login successful')]);
            }else if ($user->default_module_id == LISTING_MANAGER){
                return redirect()->route('listing-manager-dashboard')->with(['success'=>__('Login successful')]);
            }else{
                return redirect()->route('admin.home')->with(['success'=>__('Login successful')]);
            }
        } else {
            // REGISTRATION
            return redirect()->route('login')->with(['dismiss'=>__('User login failed')]);
        }
    }


    //---- done
    public function logout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect()->route('login');
    }

    /*
     * forgetPassword
     *
     * Forget Password page
     *
     *
     *
     */
    public function userForgetPassword() {
        $data['pageTitle'] = __('Forget Password');
        return view('auth.forget_pass', $data);
    }

    /*
     * forgetPasswordProcess
     *
     * Forget Password Process
     *
     *
     *
     */

    public function sendForgetPasswordMail(ForgetPasswordRequest $request) {
        $response = app(AuthService::class)->sendForgotPasswordMail($request);
        if (isset($response)) {
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->back()->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with('dismiss', __('Something went wrong.'));
        }

    }

    /*
     * forgetPasswordReset
     *
     * Password reset page
     *
     *
     *
     */

    public function forgetPasswordReset() {
        $data['pageTitle'] = __('Reset Password');
        return view('auth.forgetpassreset', $data);
    }

    /*
     * forgetPasswordChange
     *
     * Change the forgotten password
     *
     *
     *
     */

    public function resetPasword($reset_code) {
        $remember_token = decrypt($reset_code);
        $user = User::where(['remember_token' => $remember_token])->first();
        $data['remember_token'] = $remember_token;
        if ($user) {
            return view('auth.reset_password', $data);
        } else {
            return redirect()->route('login')->with(['dismiss' => __('Invalid request!')]);
        }
    }

    /*
     * forgetPasswordResetProcess
     *
     * Reset process of forgotten password
     *
     *
     *
     */

    public function changePassword(ForgotPasswordResetRequest $request) {
        if (!empty($request->remember_token)) {
            $response = app(AuthService::class)->changePassword($request);
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->route('login')->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with(['dismiss' => __('Code not found.')]);
        }
    }

    /*
     * verifyEmail
     *
     * Verify email code
     *
     *
     *
     */

    public function verifyEmail($code) {
        if (isset($code)) {
            $response = app(AuthService::class)->mailVarification($code);
            if (isset($response)) {
                if (isset($response['success']) && ($response['success'] == true)) {
                    return redirect()->route('login')->with('success', $response['message']);
                } else {
                    return redirect()->route('login')->with('dismiss', $response['message']);
                }
            } else {
                return redirect()->route('login')->with('dismiss', __('Something went wrong.'));
            }
        } else {
            return redirect()->route('login')->with(['dismiss' => __('Verification Code Not found!')]);
        }
    }

    public function register(){
        return view('auth.user_register');
    }

    public function userRegistrationSave(RegistrationRequest $request){
        $service = new \App\Http\Controllers\Auth\AuthService();
        $response = $service->userRegistration($request);
        if ($response['success'] == TRUE){
            if ($response['mail_verification'] == TRUE){
                return redirect()->route('login')->with(['success'=>$response['message']]);
            }else{
                if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                    if (Auth::user()->default_module_id == SYSTEM_USER){
                        return redirect()->intended('/');
                    }else if (Auth::user()->default_module_id == LISTING_MANAGER){
                        return redirect()->route('listing-manager-dashboard');
                    }else{
                        return redirect()->route('admin.home');
                    }
                }
            }
        }else{
            return redirect()->route('login')->with(['dismiss'=>$response['message']]);
        }
    }

    public function userRegistrationModal(RegistrationRequest $request){
        $service = new \App\Http\Controllers\Auth\AuthService();
        $response = $service->userRegistration($request);
        if ($response['success'] == TRUE){
            if ($response['mail_verification'] == TRUE){
                return response()->json($response);
            }else{
                if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                    $response['data'] = Auth::user();
                }
            }
        }
        return response()->json($response);
    }

    public function userInfoCheck() {
        return view('user.auth.user-info-check');
    }


    public function userEmailVerify($code) {
        $service = new \App\Http\Controllers\Auth\AuthService();
        $response = $service->userVerifyEmail($code);
        if (isset($response)) {
            if (isset($response['status']) && ($response['status'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->route('login')->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->route('login')->with('dismiss', $response['message']);
        }
    }

    public function userLogout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect()->route('login');
    }



    public function userForgetPasswordChange($reset_token) {
        $data['pageTitle'] = __('Reset Password');
        $data['reset_token'] = $reset_token;
        $user = User::where(['reset_token' => $reset_token])->first();
        if ($user) {
            return view('user.auth.reset_pass', $data);
        } else {
            return redirect()->route('login')->with(['dismiss' => __('Invalid request!')]);
        }
    }
    public function userForgetPasswordResetProcess(ForgotPasswordResetRequest $request)
    {
        if ($request->reset_token) {
            $response = app(UserAuthService::class)->forgetPasswordChangeProcess($request);
            if (isset($response['success']) && ($response['success'] == true)) {
                return redirect()->route('login')->with('success', $response['message']);
            } else {
                return redirect()->route('login')->with('dismiss', $response['message']);
            }
        } else {
            return redirect()->back()->with(['dismiss' => __('Code not found!')]);
        }
    }

}
