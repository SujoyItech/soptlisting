<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSysUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_users', function (Blueprint $table) {
            $table->id();
            $table->string('user_code', 20)->nullable()->comment('Employee ID');
            $table->string('username', 150)->nullable();
            $table->string('designation', 150)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('password', 100);
            $table->string('password_key', 50)->nullable();
            $table->string('name', 100)->nullable();
            $table->boolean('email_verified')->default(0);
            $table->dateTime('email_verified_at')->nullable();
            $table->string('mobile', 20)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->enum('blood_group', ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-', ''])->nullable();
            $table->enum('gender', ['Female', 'Male'])->default('Male')->nullable();
            $table->enum('marital_status', ['Married', 'Unmarried', 'Devorced', 'Widow', 'Single'])->default(NULL)->nullable();
            $table->string('father_name', 100)->nullable();
            $table->string('mother_name', 100)->nullable();
            $table->string('nationality', 20)->nullable();
            $table->string('nid', 20)->nullable()->comment('NID or Birth certificate id will store in this same field');
            $table->string('tin', 20)->nullable();
            $table->string('passport', 20)->nullable();
            $table->string('user_image', 100)->default('/img/users/Avatar.png')->nullable();
            $table->text('address')->nullable()->comment('Present Address');
            $table->dateTime('last_login')->nullable();
            $table->date('date_of_join')->nullable();
            $table->string('default_url', 255)->nullable();
            $table->tinyInteger('default_module_id')->default(0)->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->timestamp('password_changed_date')->nullable();
            $table->integer('wrong_attempts_count')->default(0)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->enum('status', ['Active','Pending','Suspended','Blocked','Deleted'])->default('Active');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('sys_users');
    }
}
