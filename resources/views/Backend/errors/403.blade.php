@extends('Backend.layouts.app')
@section('content')
<div class="middle-box text-center animated fadeInDown py-xl-5 mb-xl-5">
    <h1>403</h1>
    <h2 class="font-bold">You are not Permitted</h2>
    <div class="error-desc">
        Sorry, but the page you are looking for is not allowed for you. Try to contact your Admin to get priviledge of this.
    </div>
</div>
@endsection
