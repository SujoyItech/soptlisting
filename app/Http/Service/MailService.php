<?php


namespace App\Http\Service;


use App\Jobs\SendEmailVerificationMail;
use App\Jobs\SendMail;
use Illuminate\Support\Facades\Mail;

class MailService {
    public static function sendMessageMail($email, $subject, $body) {

        dispatch(new SendMail($email, $body, $subject));
    }

    public static function sendMailProcess($email, $body, $subject) {
        Mail::send('EmailTemplate.Email.send_mail', ['body' => $body], function ($messages) use ($email, $subject) {
            $messages->to($email)->subject($subject);
        });
    }

    public static function sendEmailVerificationMail($name,$email,$subject,$data){
        $body['name'] = $name;
        $body['message'] = $data['message'];
        $body['verification_code'] = $data['verification_code'];
        dispatch(new SendEmailVerificationMail($email,$body,$subject));
    }

    public static function sendEmailVerificationMailProcess($email, $body, $subject){
        Mail::send('EmailTemplate.Email.send_verification_mail', ['body' => $body], function ($messages) use ($email, $subject) {
            $messages->to($email)->subject($subject);
        });
    }
    public static function sendResetPasswordMailProcess($email, $body, $subject){
        Mail::send('EmailTemplate.Email.reset_password_mail', ['body' => $body], function ($messages) use ($email, $subject) {
            $messages->to($email)->subject($subject);
        });
    }


}
