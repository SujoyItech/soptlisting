@extends('FontEnd.master',['menu'=>'blog'])
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Blog')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- .articles-area-start -->
    <div class="articles-area blog-area section-padding">
        @if(isset($blogs) && !empty($blogs))
        <div class="container">
            <div class="section-title sec-title-middle text-center">
                <h2>{{__('Featured Articles')}}</h2>
            </div>
            <div class="row">
                    @foreach($blogs as $blog)
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="product-item">
                                <a href="{{route('user.blog',['slug'=>$blog->slug])}}">
                                    <div class="product-img">
                                       <img src="{{getImageUrl($blog->blog_thumb_image,'others')}}" alt="" height="300">
                                    </div>
                                </a>
                                <div class="product-content">
                                    <span>{{ Carbon\Carbon::parse($blog->created_at)->format('d F Y') }}</span>
                                    <h5><a href="{{route('user.blog',['slug'=>$blog->slug])}}">{{$blog->spotlist_blogs_title}}</a></h5>
                                </div>
                                @if(isset($blog->spotlist_category_name) && !empty($blog->spotlist_category_name))
                                    <div class="thumb">
                                        <span>{{$blog->spotlist_category_name}}</span>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
        @else
            <div class="text-center no-data-image">
                <img src="{{asset('FontEnd/assets/images/error/nodata.svg')}}">
            </div>

        @endif
    </div>
    <!-- .articles-area-start -->
    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')
@endsection
