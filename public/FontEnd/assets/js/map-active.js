/*==========  Map  ==========*/
            /*==========  Map  ==========*/
            var map;
            function initialize_map() {
                if ($('.map').length) {
                    var myLatLng = new google.maps.LatLng(40.716269,-74.004566);
                    var mapOptions = {
                        zoom: 17,
                        center: myLatLng,
                        scrollwheel: false,
                        zoomControl: false,
                        scaleControl: false,
                        mapTypeControl: false,
                        streetViewControl: false
                    };
                    map = new google.maps.Map(document.getElementById('map'), mapOptions);
                } else {
                    return false;
                }

                var marker1LatLng = new google.maps.LatLng(40.715756,-74.005339);
                marker1 = new RichMarker({
                    position: marker1LatLng,
                    map: map,
                    draggable: false,
                    flat: true,
                    content: '<div class="map-wrapper" id="marker1">' +
                        '<div class="marker"><div class="hover"></div><div class="inner"><img src="assets/images/map-marker.png" alt="icon"></div></div>' +
                        '<div class="directory-item">' +
                            '<img src="assets/images/listing/1.jpg" alt="bg" class="img-responsive">' +
                            '<div class="loc-thumb">Cafe</div>' +
                            '<div class="content">' +
                                '<h3><a href="">Elevator Cafe & Commons</a></h3>' +
                                '<span>SE Main St, Pl</span>' +
                                '<div class="bottom-text"><ul class="ratting"><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li></ul><ul><li><strong>4.6</strong> (129 Reviews)</li></ul></div>' +
                            '</div> <!-- end .content -->' +
                        '</div> <!-- end .directory-item -->' +
                    '</div>'
                });
                google.maps.event.addListener(marker1, 'click', function() {
                    $('.map-wrapper').removeClass('open');
                    $('#marker1').toggleClass('open');
                });

                var marker2LatLng = new google.maps.LatLng(40.716715,-74.003352);
                marker2 = new RichMarker({
                    position: marker2LatLng,
                    map: map,
                    draggable: false,
                    flat: true,
                    content: '<div class="map-wrapper" id="marker2">' +
                        '<div class="marker"><div class="hover"></div><div class="inner"><img src="assets/images/map-marker.png" alt="icon"></div></div>' +
                        '<div class="directory-item">' +
                            '<img src="assets/images/listing/1.jpg" alt="bg" class="img-responsive">' +
                            '<div class="loc-thumb">Cafe</div>' +
                            '<div class="content">' +
                                '<h3><a href="">Elevator Cafe & Commons</a></h3>' +
                                '<span>SE Main St, Pl</span>' +
                                '<div class="bottom-text"><ul class="ratting"><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li></ul><ul><li><strong>4.6</strong> (129 Reviews)</li></ul></div>' +
                            '</div> <!-- end .content -->' +
                        '</div> <!-- end .directory-item -->' +
                    '</div>'
                });
                google.maps.event.addListener(marker2, 'click', function() {
                    $('.map-wrapper').removeClass('open');
                    $('#marker2').toggleClass('open');
                });

                var marker3LatLng = new google.maps.LatLng(40.716622,-74.007976);
                marker3 = new RichMarker({
                    position: marker3LatLng,
                    map: map,
                    draggable: false,
                    flat: true,
                    content: '<div class="map-wrapper" id="marker3">' +
                        '<div class="marker"><div class="hover"></div><div class="inner"><img src="assets/images/map-marker.png" alt="icon"></div></div>' +
                        '<div class="directory-item">' +
                            '<img src="assets/images/listing/1.jpg" alt="bg" class="img-responsive">' +
                            '<div class="loc-thumb">Cafe</div>' +
                            '<div class="content">' +
                                '<h3><a href="">Elevator Cafe & Commons</a></h3>' +
                                '<span>SE Main St, Pl</span>' +
                                '<div class="bottom-text"><ul class="ratting"><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li></ul><ul><li><strong>4.6</strong> (129 Reviews)</li></ul></div>' +
                            '</div> <!-- end .content -->' +
                        '</div> <!-- end .directory-item -->' +
                    '</div>'
                });
                google.maps.event.addListener(marker3, 'click', function() {
                    $('.map-wrapper').removeClass('open');
                    $('#marker3').toggleClass('open');
                });

                var marker4LatLng = new google.maps.LatLng(40.714898,-74.004500);
                marker4 = new RichMarker({
                    position: marker4LatLng,
                    map: map,
                    draggable: false,
                    flat: true,
                    content: '<div class="map-wrapper" id="marker4">' +
                        '<div class="marker"><div class="hover"></div><div class="inner"><img src="assets/images/map-marker.png" alt="icon"></div></div>' +
                        '<div class="directory-item">' +
                            '<img src="assets/images/listing/1.jpg" alt="bg" class="img-responsive">' +
                            '<div class="loc-thumb">Cafe</div>' +
                            '<div class="content">' +
                                '<h3><a href="">Elevator Cafe & Commons</a></h3>' +
                                '<span>SE Main St, Pl</span>' +
                                '<div class="bottom-text"><ul class="ratting"><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li><li><i class="fa fa-star"></i></li></ul><ul><li><strong>4.6</strong> (129 Reviews)</li></ul></div>' +
                            '</div> <!-- end .content -->' +
                        '</div> <!-- end .directory-item -->' +
                    '</div>'
                });
                google.maps.event.addListener(marker4, 'click', function() {
                    $('.map-wrapper').removeClass('open');
                    $('#marker4').toggleClass('open');
                });

                return false;
            }
            google.maps.event.addDomListener(window, 'load', initialize_map);
