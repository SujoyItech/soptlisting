<?php

use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

function __combo($slug = '', $data = array()) {
    extract($data);
    $selected_value = isset($selected_value) && !empty($selected_value) ? $selected_value : '';
    $name = isset($name) && !empty($name) ? $name : '';
    $attributes = isset($attributes) && !empty($attributes) ? $attributes : [];
    $sql_query = isset($sql_query) && !empty($sql_query) ? $sql_query : '';
    if (!empty($slug)) {
        $combodata = DB::table('sys_dropdowns')->where('dropdown_slug', $slug)->first();
        if (!empty($combodata)) {
            $sql = $sql_query == '' ? $combodata->sqltext . ' ' . $combodata->sqlsource . ' ' . $combodata->sqlcondition : $sql_query;
            $query = DB::select($sql);
            $option_data = array();
            $attr = '';
            $multiple = isset($multiple) ? $multiple : $combodata->multiple;
            $class = $multiple == 1 ? 'form-control multi' : 'form-control';
            $attributes = empty($attributes) ? array('class' => $class, 'id' => $combodata->dropdown_name) : $attributes;
            if (!empty($attributes)) {
                foreach ($attributes as $key => $value) {
                    $attr .= $key . '="' . $value . '" ';
                }
            }

            if ($multiple == 1) {
                $attr .= 'multiple = "true"';
            } else {
                $option_data[''] = '--Select an option--';
            }
            if (empty($name)) {
                if ($multiple == 1) {
                    $name = $combodata->dropdown_name . '[]';
                } else {
                    $name = $combodata->dropdown_name;
                }
            }
            foreach ($query as $value) {
                $value_field = $combodata->value_field;
                $option_field = $combodata->option_field;
                $option_data[$value->$value_field] = $value->$option_field;
            }
            return Form::select($name, $option_data, $selected_value, (array)$attr);
        }
    }
}

function validateDate($date, $format = 'Y-m-d') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

function currentDate() {
    date_default_timezone_set('Asia/Dhaka');
    return date('Y-m-d');
}

function currentDateTime() {
    date_default_timezone_set('Asia/Dhaka');
    return date('Y-m-d H:i:s');
}

function toDated($date = '') {
    if (!empty($date)) {
        return date("j M, Y", strtotime($date));
    } else {
        return date("j M, Y", strtotime(currentDate()));
    }
}

function toDateTimed($date = '') {
    if (!empty($date)) {
        return date("j M, Y h:i A", strtotime($date));
    } else {
        return date("j M, Y h:i A", strtotime(currentDate()));
    }
}

function toTimed($date = '') {
    if (!empty($date)) {
        return date("h:i A", strtotime($date));
    } else {
        return date("h:i A", strtotime(currentDate()));
    }
}

function generateId($slug) {
    $qresult = DB::select(DB::raw("call generateDynamicUniqueID('" . $slug . "')"));
    if ($qresult) {
        $htm = $qresult[0]->getId;
        return $htm;
    } else {
        generateId($slug);
    }
}

function isValidUrl($url) {
    $url = parse_url($url);
    if (!isset($url["host"])) return false;
    return !(gethostbyname($url["host"]) == $url["host"]);
}


function datatable_moneyFormat($value) {
    $number = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $number = !empty($number) ? $number : 0;
    $currency = str_replace($number, '', $value);
    return number_format($number, 2) . ' ' . $currency;
}

/**
 * @param array|string[] $option_group
 * @param string|null $option_key
 *
 * @return mixed|object|string
 */
function __options(array $option_group = ['application_settings'], string $option_key = null) {
    $option_value = DB::table('sys_system_settings')->select('option_key', 'option_value');
    if(is_null($option_key)){
        $option_value->whereIn('option_group', $option_group);
        $options = $option_value->get();
        $result = [];
        if(!empty($options)){
            foreach ($options as $data){
                $result[$data->option_key] = $data->option_value;
            }
        }
        return (object)$result;
    }else{
        $option_value->value('option_value');
        $option_value->where('option_key', '=', $option_key);
        return !empty($option_value->first()) ? $option_value->first()->option_value : '';
    }
}

function randomString($a) {
    $x = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $c = strlen($x) - 1;
    $z = '';
    for ($i = 0; $i < $a; $i++) {
        $y = rand(0, $c);
        $z .= substr($x, $y, 1);
    }
    return $z;
}

function __setOptions($option_group = '', $option_key = '', $option_value = ''){
    DB::table('sys_system_settings')
      ->updateOrInsert(
          ['option_group' => $option_group, 'option_key' => $option_key],
          ['option_value' => $option_value]
      );
}

function is_selected($key,$value){
    if ($key == $value) {
        return 'selected';
    }else{
        return  '';
    }
}

function _timeSelectOption(){

    $time_value = [
        'Closed' => __('Closed'),
        '12 AM' => __('12 AM'),
        '01 AM' => __('01 AM'),
        '02 AM' => __('02 AM'),
        '03 AM' => __('03 AM'),
        '05 AM' => __('05 AM'),
        '06 AM' => __('06 AM'),
        '07 AM' => __('07 AM'),
        '08 AM' => __('08 AM'),
        '09 AM' => __('09 AM'),
        '10 AM' => __('10 AM'),
        '11 AM' => __('11 AM'),
        '12 PM' => __('12 PM'),
        '01 PM' => __('01 PM'),
        '02 PM' => __('02 PM'),
        '03 PM' => __('03 PM'),
        '05 PM' => __('05 PM'),
        '06 PM' => __('06 PM'),
        '07 PM' => __('07 PM'),
        '08 PM' => __('08 PM'),
        '09 PM' => __('09 PM'),
        '10 PM' => __('10 PM'),
        '11 PM' => __('11 PM')
    ];

    return $time_value;

}

function randomNumber($a = 10) {
    $x = '123456789';
    $c = strlen($x) - 1;
    $z = '';
    for ($i = 0; $i < $a; $i++) {
        $y = rand(0, $c);
        $z .= substr($x, $y, 1);
    }
    return $z;
}

function get_currency(){
    $admin_settings = __options(['admin_settings']);
    return isset($admin_settings->currency)? $admin_settings->currency : 'usd';
}

function checkDirectoryItemLike($user_id,$spotlist_directory_id,$spotlist_directory_type_id){
    $like = DB::table('spotlist_directory_type_like_table')
              ->where(['sys_users_id'=>$user_id,'spotlist_directory_id'=>$spotlist_directory_id,'spotlist_directory_type_id'=>$spotlist_directory_type_id])->first();
    return isset($like) ? $like->like : 0;
}

function checkFollower($user_id,$follower_id){
    $is_follower = DB::table('spotlist_user_follower')->where(['user_id'=>$user_id,'follower_id'=>$follower_id,'is_follow'=>1])->first();
    return isset($is_follower) && !empty($is_follower) ? TRUE : FALSE ;
}

function checkFreeSubscription($user_id){
    $subscription = DB::table('spotlist_user_subscription')->where(['user_id'=>$user_id])->first();
    return isset($subscription->free_subscription_check) && $subscription->free_subscription_check == TRUE ? TRUE : FALSE ;
}

function user_image($user_image){
    return isset($user_image) && file_exists($user_image) ? asset($user_image):asset('Backend/images/users/avatar.svg');
}

function checkOpenCloseStatus($directory_id){

    $weekMap = [
        0 => 'sunday',
        1 => 'monday',
        2 => 'tuesday',
        3 => 'wednesday',
        4 => 'thursday',
        5 => 'friday',
        6 => 'saturday',
    ];
    $dayOfTheWeek = Carbon::now()->dayOfWeek;
    $weekday = $weekMap[$dayOfTheWeek];

    $schedule = DB::table('spotlist_directory_schedule')->where('spotlist_directory_id',$directory_id)->first();
    if (isset($schedule)){

        $date = explode('-',$schedule->$weekday);
        $date[0] = strtolower($date[0]);
        $date[1] = strtolower($date[1]);
        $now = Carbon::now();
        if ($date[0] == 'closed' && $date[1] == 'closed'){
            return  FALSE;
        }elseif ($date[0] == 'closed' && $date[1] !== 'closed'){
            $close = 'tomorrow '.$date[1];
            $tomorrow = Carbon::parse($close);
            if ($now->lte($tomorrow)){
                return  TRUE;
            }
        }elseif ($date[0] !== 'closed' && $date[1] == 'closed'){
            $open = 'today '.$date[0];
            $today = Carbon::parse($open);
            if ($now->lte($today)){
                return  TRUE;
            }
        }else{
            $open = 'today '.$date[0];
            $close = 'tomorrow '.$date[1];
            $today = Carbon::parse($open);
            $tomorrow = Carbon::parse($close);
            if ($now->gte($today) && $now->lte($tomorrow)) {
                return TRUE;
            }
        }
    }

    return FALSE;
}

function checkOpenCloseStatusByTime($schedule){

    $weekMap = [
        0 => 'sunday',
        1 => 'monday',
        2 => 'tuesday',
        3 => 'wednesday',
        4 => 'thursday',
        5 => 'friday',
        6 => 'saturday',
    ];
    $dayOfTheWeek = Carbon::now()->dayOfWeek;
    $weekday = $weekMap[$dayOfTheWeek];

    if (isset($schedule)){

        $date = explode('-',$schedule->$weekday);
        $date[0] = strtolower($date[0]);
        $date[1] = strtolower($date[1]);
        $now = Carbon::now();
        if ($date[0] == 'closed' && $date[1] == 'closed'){
            return  FALSE;
        }elseif ($date[0] == 'closed' && $date[1] !== 'closed'){
            $close = 'tomorrow '.$date[1];
            $tomorrow = Carbon::parse($close);
            if ($now->lte($tomorrow)){
                return  TRUE;
            }
        }elseif ($date[0] !== 'closed' && $date[1] == 'closed'){
            $open = 'today '.$date[0];
            $today = Carbon::parse($open);
            if ($now->lte($today)){
                return  TRUE;
            }
        }else{
            $open = 'today '.$date[0];
            $close = 'tomorrow '.$date[1];
            $today = Carbon::parse($open);
            $tomorrow = Carbon::parse($close);
            if ($now->gte($today) && $now->lte($tomorrow)) {
                return TRUE;
            }
        }
    }

    return FALSE;
}

function availableListing($user_id){
    $check_subscription = DB::table('spotlist_user_subscription')->where('user_id',$user_id)->first();
    if (isset($check_subscription)){
        $available_listing = $check_subscription->num_of_listing - $check_subscription->listing_item_used;
        return $available_listing ;
    }else{
        return FALSE;
    }
}

function availableMedia($user_id){
    $check_subscription = DB::table('spotlist_user_subscription')->where('user_id',$user_id)->first();
    if (isset($check_subscription)){
        $available_media = $check_subscription->num_of_photos - $check_subscription->photos_used;
        return $available_media ;
    }else{
        return FALSE;
    }
}

function userFollower($user_id){
    return DB::table('spotlist_user_follower')->where(['follower_id'=>$user_id,'spotlist_user_follower.is_follow'=>1])->count('spotlist_user_follower.follower_id');
}

function countUserUnreadNotification($user_id){
    $notifications = \App\Models\Notification::where(['user_id'=>$user_id,'status'=>STATUS_PENDING])->whereIn('type',[USER_DIRECTORY,MESSAGE_NOTIFICATION,USER_BOOKING,GENERAL_MESSAGE_NOTIFICATION])->count();
    return $notifications;
}
function countAdminUnreadNotification($user_id){
    $notifications = \App\Models\Notification::where(['user_id'=>$user_id,'status'=>STATUS_PENDING])->whereIn('type',[LISTING_MANAGER_DIRECTORY,LISTING_MANAGER_ADMIN_BOOKING,ADMIN_DIRECTORY,WISHING,FOLLOWING,SUBSCRIPTION_NOTIFICATION])->count();
    return $notifications;
}

function countUnreadMessage($user_id){
    $message = \App\Models\Notification::where(['user_id'=>$user_id,'status'=>STATUS_PENDING])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->count();
    return $message;
}

function getImage($image,$type=''){
    if (!empty($image) && file_exists(public_path($image))){
        return asset($image);
    }else{
        return getDefaultImage();
    }
}

function addToURL($key, $value) {
    $query = $_GET;
// replace parameter(s)
    $query[$key] = $value;
// rebuild url
    $query_result = http_build_query($query);

// new link
    return $query_result ;
}

function getDefaultImage(){
    return asset('FontEnd/assets/images/no_image.svg');
}
