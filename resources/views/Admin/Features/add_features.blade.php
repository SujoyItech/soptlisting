@extends('Backend.layouts.app',['menu'=>'feature','sub_menu'=>'feature_add'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('spotlist-feature')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New Features')}}</a>
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('spotlist-feature-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('Features list')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-globe"></i> {{__('Features')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('spotlist-feature-save')}}" class="parsley-examples" method="POST" id="submit_features" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="spotlist_features_name">{{__('Features name')}}<span class="text-danger">*</span></label>
                                <input type="text" name="spotlist_features_name" parsley-trigger="change" required value="{{$features->spotlist_features_name ?? ''}}"
                                       placeholder="Enter feature name" class="form-control" id="spotlist_features_name">
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label class="col-form-label" for="status">{{__('Status')}}</label>
                                @php($option = ['Active' =>__('Active'),'Inactive'=>__('Inactive')])
                                {!! Form::select('status', $option, $features->status ?? '', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="col-form-label" for="spotlist_features_icon">{{__('Features icon')}}</label>
                        <input type="file" name="spotlist_features_icon" data-plugins="dropify"
                               data-default-file="{{isset($features->spotlist_features_icon) ? asset($features->spotlist_features_icon) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="1M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <input type="hidden" name="spotlist_features_id" value="{{$features->spotlist_features_id ?? ''}}" id="spotlist_features_id">
    </div>
@endsection
@section('script')
    <script>
        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    if ($('#spotlist_features_id').val().length !== 0){
                        formData.append('spotlist_features_id',$('#spotlist_features_id').val());
                    }
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            var redirect_url = "{{url('spotlist-feature-list')}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'error');
                        }
                    });
                }
            });
        });
    </script>
@endsection
