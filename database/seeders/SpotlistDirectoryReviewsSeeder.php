<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectoryReviewsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory_reviews')->delete();
        
        \DB::table('spotlist_directory_reviews')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 4,
                'spotlist_directory_id' => 1,
                'rating' => 3,
                'review' => 'Some text will be written',
                'created_at' => '2020-10-13 08:04:23',
                'updated_at' => '2020-10-13 08:04:23',
                'status' => 'Active',
            ),
        ));

        
    }
}