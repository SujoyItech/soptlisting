@extends('Backend.layouts.blank')
@section('content')
    <div class="auth-fluid">
        <!--Auth fluid left content -->
        <div class="auth-fluid-form-box">
            <div class="align-items-center d-flex h-100">
                <div class="card-body">
                    <!-- Logo -->
                    <div class="auth-brand text-center text-lg-left">
                        <div class="auth-logo">
                            <a href="{{url('/')}}" class="logo logo-dark text-center">
                            <span class="logo-lg">
                                <img src="{{asset('/Backend/images/logo-dark.png')}}" alt="" height="22">
                            </span>
                            </a>
                            <a href="{{url('/')}}" class="logo logo-light text-center">
                            <span class="logo-lg">
                                <img src="{{asset('/Backend/images/logo-light.png')}}" alt="" height="22">
                            </span>
                            </a>
                        </div>
                    </div>
                    <!-- title-->
                    <h4 class="mt-0">Forget Password</h4>
                    <p class="text-muted mb-4">{{__('Enter your email address to recovery password to access account.')}}</p>
                    <form action="{{ route('forgetPasswordProcess') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="emailaddress">{{ __('Email Address') }}</label>
                            <div class="input-group input-group-merge">
                                <input name="email" class="form-control" type="email" id="emailaddress" value="nijhu.tulip@gmail.com" required placeholder="Enter your email">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group my-lg-4 text-center">
                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-send"></i> &nbsp; {{ __('Send Password Reset Link') }}</button>
                        </div>
                    </form>
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{Session::get('success')}}
                        </div>
                    @endif
                    @if(Session::has('warning'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{Session::get('warning')}}
                        </div>
                    @endif
                    <footer class="footer footer-alt">
                        <p class="text-muted"><a href="{{url('login')}}" class="text-muted ml-1"><b>Sign In</b></a></p>
                    </footer>
                </div> <!-- end .card-body -->
            </div> <!-- end .align-items-center.d-flex.h-100-->
        </div>
        <!-- end auth-fluid-form-box-->

        <!-- Auth fluid right content -->
        <div class="auth-fluid-right text-center">
            <div class="auth-user-testimonial">
                <h2 class="mb-3 text-white">I love the color!</h2>
                <p class="lead"><i class="mdi mdi-format-quote-open"></i> I've been using your theme from the previous developer for our web app, once I knew new version is out, I immediately bought with no hesitation. Great themes, good documentation with lots of customization available and sample app that really fit our need. <i class="mdi mdi-format-quote-close"></i>
                </p>
            </div> <!-- end auth-user-testimonial-->
        </div>
        <!-- end Auth fluid right content -->
    </div>
@endsection
