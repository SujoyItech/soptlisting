<div class="tab-pane" id="contact">
    <form action="{{url('directory-contact-save')}}" class="parsley-examples" method="POST" id="submit_directory_contact">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label class="col-form-label" for="website">{{__('Website')}}</label>
                    <input type="text" class="form-control" id="website" name="website" placeholder="{{__('Website')}}" value="{{$directory->website ?? ''}}">
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label class="col-form-label" for="email">{{__('Email')}}</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="{{__('Email')}}" value="{{$directory->email ?? ''}}">
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label class="col-form-label" for="contact_number">{{__('Phone number')}}</label>
                    <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="{{__('Phone number')}}" value="{{$directory->contact_number ?? ''}}">
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label class="col-form-label" for="facebook_url">{{__('Facebook')}}</label>
                    <input type="text" class="form-control" id="facebook_url" name="facebook_url" placeholder="{{__('Facebook')}}"  value="{{$directory->facebook_url ?? ''}}">
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label class="col-form-label" for="twitter_url">{{__('Twitter')}}</label>
                    <input type="text" class="form-control" id="twitter_url" name="twitter_url" placeholder="{{__('Twitter')}}" value="{{$directory->twitter_url ?? ''}}">
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label class="col-form-label" for="linkedin_url">{{__('Linkedin')}}</label>
                    <input type="text" class="form-control" id="linkedin_url" name="linkedin_url" placeholder="{{__('Linkedin')}}" value="{{$directory->linkedin_url ?? ''}}">
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label class="col-form-label" for="instagram_url">{{__('Instagram')}}</label>
                        <input type="text" class="form-control" id="instagram_url" name="instagram_url" placeholder="{{__('Instagram')}}" value="{{$directory->instagram_url ?? ''}}">
                </div>
            </div>
        </div>
        <ul class="list-inline mb-0 wizard">
            <li class="previous list-inline-item">
                <a href="javascript: void(0);" class="btn btn-sm btn-secondary">{{__('Previous')}}</a>
            </li>
            <li class="next list-inline-item float-right">
                <button type="submit" class="btn btn-sm btn-success submit_info btn-next">{{__('Save & Next')}}</button>
            </li>
        </ul>
    </form>

</div>

