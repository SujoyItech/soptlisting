@extends('FontEnd.master')
@section('content')
    @php($settings = __options(['admin_settings']))
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Sign In')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- start login-pg-section -->
    <div class="login-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col col-12">
                    <div class="login-left">
                        <div class="auth-img">
                            <img src="{{asset('FontEnd/')}}/assets/images/login.svg" alt class="lob" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col col-12">
                    <div class="login-wrap contact-area">
                        @if(Session::has('message'))
                            <div class="alert alert-success alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!! Session::get('message') !!}
                            </div>
                        @endif
                        @if(Session::has('dismiss'))
                            <div class="alert alert-danger alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('dismiss')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('success')}}
                            </div>
                        @endif
                        @if(count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible text-center" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! $error !!}
                                </div>
                            @endforeach
                        @endif
                        <h2>{{__('Sign In To Your Account')}}</h2>
                        <form method="post" action="{{ route('postLogin') }}" class="parsley-examples">
                            @csrf
                            <div class="col-12">
                                <label for="">{{__('E-mail Address')}}</label>
                                <input name="email" type="email" id="email" required class="form-control" placeholder="your@email.com">
                            </div>
                            <div class="col-12">
                                <label for="">{{__('Password')}}</label>
                                <input name="password" type="password" id="password" class="form-control" placeholder="Minimum 6 character">
                            </div>
                            <div class="col-12">
                                <p>{{__('Forgot Password?')}} <a href="{{ route('forgetPassword') }}">{{__('Click Here')}}</a></p>
                            </div>
                            <div class="submit-btn col-12">
                                <button type="submit" class="btn theme-btn">{{__('Sign in')}}</button>
                            </div>
                            @if(isset($settings->social_login_enable) && $settings->social_login_enable == TRUE)
                                <div class="text-center">
                                    <p class="mt-1 text-muted">{{__('Sign in with')}}</p>
                                    <ul class="social-list list-inline mt-3 mb-0">
                                        <li class="list-inline-item">
                                            <a href="{{url('login/facebook')}}" class="social-list-item border-primary text-primary"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="{{url('login/google')}}" class="social-list-item border-danger text-danger"><i class="fa fa-google"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                            <div class="col-12 sign-b">
                                <p>{{__('Need An Account?')}} <a href="{{route('user.register')}}">{{__('Sign Up')}}</a></p>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
