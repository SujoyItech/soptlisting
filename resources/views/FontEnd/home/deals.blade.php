<div class="deals-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <div class="section-title">
                    <h2>{{__('Popular list')}}</h2>
                    <a class="btn theme-btn" href="{{route('most-popular')}}">{{__('View All')}}</a>
                </div>
                <div class="country-r">
                    <div class="deals-carousel owl-carousel owl-theme owl-btn-center-lr owl-dots-primary-full owl-btn-3 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                       @foreach($most_popular_deals as $deals)
                            <div class="product-item">
                                <a href="{{route('user.listing').'/'.$deals->spotlist_directory_slug}}">
                                    <div class="product-img">
                                        <img src="{{getImage($deals->thumb_image,'directory')}}" alt="" height="200">
                                    </div>
                                </a>
                                <div class="product-content">
                                    <h5><a href="{{route('user.listing').'/'.$deals->spotlist_directory_slug}}">{{$deals->spotlist_directory_name}}</a></h5>
                                    <span>{{$deals->city_name.' , '.$deals->country_name}}</span>
                                    <div class="thumb-item">
                                        <ul class="list-rating">
                                            @for($x=0; $x<5 ; $x++)
                                                @if(floor($deals->rating)- $x >= 1 )
                                                    <li><i class="fa fa-star"></i></li>
                                                @elseif($deals->rating - $x > 0)
                                                    <li><i class="fa fa-star-half-o"></i></li>
                                                @else
                                                    <li><i class="fa fa-star-o"></i></li>
                                                @endif
                                            @endfor
                                        </ul>
                                        <span>({{isset($deals->rating) ? number_format($deals->rating,1) : '0.0'}})</span>
                                    </div>
                                    <p>{{$deals->address}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
