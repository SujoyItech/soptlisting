@extends('Backend.layouts.app',['menu'=>'dashboard'])
@section('title','Dashboard')
@section('style')
    <style>
        .active-btn{
            background: #0a62de;
        }
    </style>

@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">{{__('Dashboard')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="mt-1 text-dark"><span data-plugin="counterup">{{$listing->total_directory ?? 0}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{__('Published Listings')}}</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-3 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$total_reviews->total_reviews ?? 0}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{__('Total Reviews')}}</p>
                        </div>
                    </div>

                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-3 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$wishing_list->total_wishing_list ?? 0}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{__('Total wishlist')}}</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-3 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$booking_request->total_booking_request ?? 0}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{__('Total Bookings')}}</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                @if(isset($main_categories) && !empty($main_categories))
                    @foreach($main_categories as $main_category)

                        <div class="col-md-4 col-xl-3">
                            <a href="{{route('show-list-by-category',['category_name'=>$main_category->spotlist_category_name])}}">
                            <div class="card-box product-box">
                                <div class="bg-light">
                                    <img src="{{getImageUrl($main_category->category_image,'other')}}" alt="product-pic" class="img-fluid" style="height: 200px !important" />
                                </div>

                                <div class="product-info">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <h5 class="font-16 mt-0 sp-line-1">{{$main_category->spotlist_category_name}}</h5>

                                            <h5 class="m-0"> <span class="text-muted"> {{__('Total listing ')}} : {{ $main_category->total_directory }}</span></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>

                    @endforeach
                @endif
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-md-6">
            <div class="card">
                <div class="card-body" dir="ltr">
                    <figure class="highcharts-figure">
                        <div id="directory_by_category"></div>
                    </figure>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-md-6">
            <div class="card">
                <div class="card-body" dir="ltr">
                    <figure class="highcharts-figure">
                        <div id="top_user"></div>
                    </figure>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->

        <div class="col-xl-12 col-md-12">
            <div class="card">
                <div class="card-body" dir="ltr">
                    <div class="text-center">
                        <button class="btn btn-sm btn-primary chart_type" data-type="visit_count">{{__('Most visited')}}</button>
                        <button class="btn btn-sm btn-secondary chart_type" data-type="booking_count">{{__('Most booked')}}</button>
                        <button class="btn btn-sm btn-success chart_type" data-type="wishing_count">{{__('Most popular')}}</button>
                    </div>
                    <hr>
                    <figure class="highcharts-figure">
                        <div id="container"></div>
                    </figure>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->

    </div>
@endsection
@section('script')

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <script>

        $(document).ready(function (){
            var data = {
                type : 'visit_count'
            };
            var url = "{{route('getTopDirectoryChart')}}";
            makeAjaxPost(data,url,null).done(function (response){
                getTopDirectoryChart(response);
            })
            var top_user_url = "{{route('getTopUserChart')}}"
            makeAjax(top_user_url,null).done(function (response){
                getTopUserChart(response);
            }) ;

            var directory_by_category_url = "{{route('getDirectoryListByCategory')}}"
            makeAjax(directory_by_category_url,null).done(function (response){
                getDirectoryByCategory(response);
            })

        });

        $(document).on('click','.chart_type',function (){
            var data = {
                type : $(this).data('type')
            };
            var url = "{{route('getTopDirectoryChart')}}";

            makeAjaxPost(data,url,null).done(function (response){
                getTopDirectoryChart(response);
            })
        })

        function getTopDirectoryChart(response){
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Top directories'
                },
                xAxis: {
                    categories: response.directory_name,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'User count'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Most visited',
                    data: response.visit_count

                }, {
                    name: 'Most booked',
                    data: response.booking_count

                }, {
                    name: 'Most popular',
                    data: response.wishing_count

                }]
            });
        }

        function getTopUserChart(response){
            Highcharts.chart('top_user', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Top user'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}'
                        }
                    }
                },
                series: [{
                    name: 'Listing',
                    colorByPoint: true,
                    data: response.top_users
                }]
            });
        }

        function getDirectoryByCategory(response){
            Highcharts.chart('directory_by_category', {
                chart: {
                    type: 'column'
                },

                title: {
                    text: 'Total directory group by category'
                },

                xAxis: {
                    categories: response.category_name
                },

                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Number of directory'
                    }
                },

                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y + '<br/>' ;

                    }
                },
                series: [{
                    name: 'Directory',
                    data: response.total_directory
                }]
            });
        }


    </script>


@endsection
