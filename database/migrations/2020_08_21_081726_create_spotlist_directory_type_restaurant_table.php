<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistDirectoryTypeRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_directory_type_restaurant', function (Blueprint $table) {
            $table->increments('spotlist_directory_type_restaurant_id');
            $table->unsignedInteger('spotlist_directory_id');
            $table->string('menu_name');
            $table->decimal('menu_price',10,2)->nullable();
            $table->text('ingredients_item')->nullable();
            $table->string('menu_image')->nullable();
            $table->text('menu_description')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_directory_type_restaurant');
    }
}
