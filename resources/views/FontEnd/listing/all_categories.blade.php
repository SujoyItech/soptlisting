@extends('FontEnd.master')
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Categories')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- .articles-area-start -->
    <div class="articles-area blog-area section-padding">
        @if(isset($categories) && !empty($categories))
        <div class="container">
            <div class="row">
                    @foreach($categories as $category)
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="product-item">
                            <a href="{{route('show-list-by-category',['category_name'=>$category->spotlist_category_name])}}">
                                <div class="featured-item">
                                    <div class="featured-img">
                                        <img src="{{isset($category->category_image) ? asset($category->category_image) : ''}}" alt="" height="300">
                                    </div>
                                    <div class="featured-content">
                                        <h5><a href="{{route('show-list-by-category',['category_name'=>$category->spotlist_category_name])}}">{{$category->spotlist_category_name}}</a></h5>
                                    </div>
                                    <div class="thumb">
                                        <span>{{$category->total_listing ?? 0 }} {{__('Listing')}}</span>
                                    </div>
                                </div>
                            </a>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
        @else
            <div class="text-center">
                <img src="{{asset('FontEnd/assets/images/error/pro-bottom')}}">
            </div>

        @endif
    </div>
    <!-- .articles-area-start -->
    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')
@endsection
