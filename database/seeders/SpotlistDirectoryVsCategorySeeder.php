<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectoryVsCategorySeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory_vs_category')->delete();
        
        \DB::table('spotlist_directory_vs_category')->insert(array (
            0 => 
            array (
                'spotlist_directory_id' => 1,
                'spotlist_category_id' => 3,
            ),
            1 => 
            array (
                'spotlist_directory_id' => 2,
                'spotlist_category_id' => 2,
            ),
        ));

        
    }
}