<?php


namespace App\Http\Service;


use App\Models\Message;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;
use Pusher\PusherException;

class NotificationService {

    public function sendNotification($notify_body,$notify_to){
        $post_data = [];
        if (is_array($notify_to['user_id'])){
            if (!empty($notify_to['user_id'][0])){
                foreach ($notify_to['user_id'] as $key => $value){
                    $post_data['user_id'] = $value;
                    $post_data['body'] =  json_encode($notify_body);
                    $post_data['type'] =  $notify_to['type'];
                    Notification::create($post_data);
                    $channel = 'user_'.$value;
                    $this->triggerNotification($channel,'user_notification',$notify_body['title'],$notify_body['body']);
                }
            }

        }else{
            $post_data = [
                'user_id' =>$notify_to['user_id'],
                'body' => json_encode($notify_body),
                'type' => $notify_to['type']
            ];
            Notification::create($post_data);
            $channel = 'user_'.$notify_to['user_id'];
            $this->triggerNotification($channel,'user_notification',$notify_body['title'],$notify_body['body']);
        }

    }

    public function notificationMakeSeen(Request $request){
        try {
            Notification::where('id',$request->id)->update(['status'=>STATUS_SUCCESS]);
            $response['success'] = TRUE;
        }catch (\Exception $exception){
            $response['success'] = FALSE;
        }
        return $response;
    }

    public function clearAllNotification(){
        try {
            Notification::where(['user_id'=>Auth::user()->id])->whereNotIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->update(['is_cleared'=>STATUS_SUCCESS,'status'=>STATUS_SUCCESS]);
            $response['success'] = TRUE;
            $response['message'] = __('All notifications are cleared.');
        }catch (\Exception $exception){
            $response['success'] = FALSE;
            $response['message'] = __('Something went wrong.');
        }
      return $response;
    }

    public function clearAllMessage(){
        try {
            Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->update(['is_cleared'=>STATUS_SUCCESS,'status'=>STATUS_SUCCESS]);

            $response['success'] = TRUE;
            $response['message'] = __('All messages are cleared.');
        }catch (\Exception $exception){
            $response['success'] = FALSE;
            $response['message'] = __('Something went wrong.');
        }
      return $response;
    }

    public static function triggerNotification($channel = '',$event= '', $title = '', $body = '') {
        if ($channel != '') {
            $config = config('broadcasting.connections.pusher');
            try {
                $broadcust = new Pusher($config['key'], $config['secret'], $config['app_id'], $config['options'],env('APP_URL', 'http://localhost'), env('LARAVEL_WEBSOCKETS_PORT',6007));
                $data['title'] = $title;
                $data['body'] = $body;
                $broadcust->trigger($channel, $event, $data);
            } catch (PusherException $e) {

            }
        }
    }

    public function sendMessage($message_body,$message_to){
        $post_data = [];
        if (is_array($message_to['user_id'])){
            if (!empty($message_to['user_id'][0])){
                foreach ($message_to['user_id'] as $key => $value){
                    $post_data['user_id'] = $value;
                    $post_data['body'] =  json_encode($message_body);
                    $post_data['type'] =  $message_to['type'];
                    Notification::create($post_data);
                    $channel = 'user_message_'.$value;
                    $this->triggerNotification($channel,'user_message',$message_body['title'],$message_body['body']);
                }
            }

        }else{
            $post_data = [
                'user_id' =>$message_to['user_id'],
                'body' => json_encode($message_body),
                'type' => $message_to['type']
            ];
            Notification::create($post_data);
            if ($message_to['module_id'] == SYSTEM_USER){
                $channel = 'user_'.$message_to['user_id'];
                $this->triggerNotification($channel,'user_notification',$message_body['title'],$message_body['body']);
            }else{
                $channel = 'user_message_'.$message_to['user_id'];
                $this->triggerNotification($channel,'user_message',$message_body['title'],$message_body['body']);
            }

        }

    }
}
