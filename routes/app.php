<?php

/*=============================================Dashboard====================================================*/
Route::get('home', 'Backend\Dashboard\DashboardController@index')->name('home');

Route::post('delete-table-rows', 'SettingController@deleteRows')->name('delete-table-rows');
Route::post('delete-list-by-id', 'SettingController@deleteListById')->name('delete-list-by-id');


/*=============================================MASTER ENTRY====================================================*/
Route::get('settings/{type}', 'SettingController@settings')->name('settings');
Route::post('admin-frontend-settings-save', 'SettingController@frontEndSettingsSave')->name('admin-frontend-settings-save');
Route::post('admin-settings-save', 'SettingController@adminSettingsSave')->name('admin-settings-save');
Route::post('admin-currency-setting-save', 'SettingController@adminCurrencySettingsSave')->name('admin-currency-setting-save');
Route::post('admin-payment-settings-save', 'SettingController@paymentSettingsSave')->name('admin-payment-settings-save');
Route::post('admin-auth-settings-save', 'SettingController@authSettingsSave')->name('admin-auth-settings-save');
Route::get('how-it-works', 'SettingController@howItWorks')->name('how-it-works');
Route::post('how-it-work-save', 'SettingController@howItWorksSave')->name('how-it-work-save');

Route::get('about-us', 'SettingController@aboutUs')->name('about-us');
Route::post('about-us-save', 'SettingController@aboutUsSave')->name('about-us-save');

Route::get('teams', 'SettingController@teams')->name('teams');
Route::get('get-team-data/{id?}', 'SettingController@getTeamData')->name('get-team-data');
Route::post('save-team-data', 'SettingController@saveTeamData')->name('save-team-data');

Route::post('saveSettingsForm', 'CrudManager\MasterFormController@SettingsFormDataStore')->name('saveSettingsForm');

