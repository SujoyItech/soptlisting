@if(isset($types) && !empty($types[0]))
    <div class="list-gallery list-sub">
        <h2>{{__('Menu Items')}}</h2>
        <div class="row">
            @foreach($types as $type)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card mt-2">
                        <div class="product-item">
                            <div class="product-img">
                                <img src="{{getImage($type->menu_image)}}" alt="" height="200">
                                @if(isset(Auth::user()->id))
                                    <a href="javascript:void(0)"
                                       class="add-cart {{checkDirectoryItemLike(Auth::user()->id,$directory->spotlist_directory_id,$type->spotlist_directory_type_restaurant_id) == TRUE ? 'text-success' :''}}"
                                       data-directory-type-id="{{$type->spotlist_directory_type_restaurant_id}}"
                                       data-directory-id="{{$directory->spotlist_directory_id}}"
                                       data-liked="{{ checkDirectoryItemLike(Auth::user()->id,$directory->spotlist_directory_id,$type->spotlist_directory_type_restaurant_id)}}">
                                        <i class="fa fa-heart"></i>
                                    </a>
                                @endif
                            </div>
                            <div class="product-content">
                                <h5>{{$type->menu_name}}</h5>
                                @if(isset($type->menu_price))
                                    <span>{{__('Price: ')}}{{$type->menu_price}} ({{get_currency()}})</span><br>
                                @endif
                                <div class="ratting-area">
                                    <p>
                                        <span><i class="fa fa-thumbs-o-up text-success"></i></span>
                                        <span class="pl-1" data-like="{{$type->total_like}}" id="{{$type->spotlist_directory_type_restaurant_id}}">{{ $type->total_like > 1000 ? $type->total_like.'k' : $type->total_like}}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-block btn-info view_details"
                                data-id="{{$type->spotlist_directory_type_restaurant_id}}"
                                data-type="restaurant">{{__('View details')}}</button>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif


