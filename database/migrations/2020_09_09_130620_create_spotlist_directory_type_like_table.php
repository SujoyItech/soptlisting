<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistDirectoryTypeLikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_directory_type_like_table', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sys_users_id');
            $table->integer('spotlist_directory_id');
            $table->integer('spotlist_directory_type_id');
            $table->boolean('like')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_directory_type_like_table');
    }
}
