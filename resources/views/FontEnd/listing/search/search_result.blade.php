@extends('FontEnd.master',['menu'=>'listing'])
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Listing')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- map-area start-->
    <div class="map-area mb-5">
        <div id="mymap" class="map"></div>
    </div>
    <!-- map-area end-->

    <!-- listing-area start -->
    <div class="listing-area">
        @if(isset($list) && !empty($list[0]))
            <div class="container">
                <div class="row">
                    @foreach($list as $data)
                    <div class="col-lg-4 col-md-4 col-12 mb-3">
                        <div class="product-item">
                            <a href="{{route('user.listing').'/'.$data->spotlist_directory_slug}}">
                                <div class="product-img">
                                    <img src="{{getImage($data->thumb_image,'directory')}}" alt="" height="300">
                                    @if($data->is_featured == 1)
                                        <div class="pro-featured">
                                            <span class="theme-btn v3" >{{__('Featured')}}</span>
                                        </div>
                                    @endif
                                    <div class="thumb">
                                        <span>{{$data->category_name}}</span>
                                    </div>
                                </div>
                            </a>
                            <div class="product-content">
                                <span><a href="{{route('user.profile',['id'=>$data->user_id])}}"><small>{{$data->user_name}}</small></a> | <small>{{ Carbon\Carbon::parse($data->created_at)->format('d F Y') }}</small></span>
                                <h4><a href="{{route('user.listing',['slug'=>$data->spotlist_directory_slug])}}">{{$data->spotlist_directory_name}}</a></h4>
                                <span>{{isset($data->city_name) ? $data->city_name.' , ' : ''}} {{$data->country_name}}</span>
                                <div class="ratting-area">
                                    @if(isset($data->rating))
                                        <ul>
                                            @for($x = 0; $x< 5 ; $x++)
                                                @if(floor($data->rating) - $x >=1)
                                                    <li><i class="fa fa-star"></i></li>
                                                @elseif($data->rating -$x >0)
                                                    <li><i class="fa fa-star-half-o"></i></li>
                                                @else
                                                    <li><i class="fa fa-star-o"></i></li>
                                                @endif
                                            @endfor
                                        </ul>
                                    @endif
                                    <p><span>{{isset($data->rating) ? number_format($data->rating,1) : 0}}</span> ({{$data->total_reviews}} {{__('Reviews')}})</p>
                                </div>
                                <div class="pro-bottom">
                                    <ul>
                                        <li><small class="theme-btn open">Now Open</small></li>
                                        <li><a href="#"><img src="{{asset('FontEnd/')}}/assets/images/logo/Vector.png" alt="">Show On Map</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="thumb">
                                <span>{{$data->category_name}}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-5 offset-5">
                        <div class="listing-wrap">
                            <div class="pagination-wrapper">
                                {{ $list->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="container">
                <div class="row">
                    <div class="col-6 offset-4">
                        <div class="no-data-image">
                            <img src="{{asset('FontEnd/assets/images/error/nodata.svg')}}">
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
@section('script')
    <!-- google maps -->
    <script src="https://maps.googleapis.com/maps/api/js?key={{$settings->map_key ?? 'AIzaSyBj0o3QDEGBtW3S8AEq0CRlpZdpWxNZckA'}}"></script>
    <!-- Custom script for this template -->
    <script src="{{asset('FontEnd/assets/js/gmaps.js')}}"></script>
    <script src="{{asset('FontEnd/')}}/assets/js/script.js"></script>
    <script>
        var locations = <?php print_r(json_encode($all_listing ?? [])) ?>;
        getLocation();
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            var mymap = new GMaps({
                el: '#mymap',
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                zoom:12
            });
            $.each( locations, function( index, value ){
                mymap.addMarker({
                    lat: value.latitude,
                    lng: value.longitude,
                    icon: "{{asset('FontEnd/assets/images/icon.png')}}",
                    title: value.city_name,
                    infoWindow: {
                        content: '<div class="directory-item" style="width: min-content">\n' +
                            '     <img src="{{asset('')}}'+value.thumb_image+'" alt="bg" class="img-responsive text-center" width=300 height=200>\n' +
                            '     <div class="loc-thumb">'+value.category_name+'</div>\n' +
                            '     <div class="content mt-2">\n' +
                            '         <h3><a href="{{url('/user/listing/')}}'+'/'+value.spotlist_directory_slug+'">'+value.spotlist_directory_name+'</a></h3>\n' +
                            '         <span>'+ value.address +'</span>\n' +
                            '     </div>\n' +
                            ' </div>'
                    },
                    click: function(e) {
                        this.infoWindow.open(this.mymap, this);
                    }
                })
            });
        }
    </script>
@endsection
