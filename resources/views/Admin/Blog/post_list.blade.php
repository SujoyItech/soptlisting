@extends('Backend.layouts.app',['menu'=>'blog','sub_menu'=>'blog_list'])
@section('style')

@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="new_post" href="{{url('admin-blog-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New post')}}</a>
                    <button class="btn btn-info btn-xs no-display" id="view"><i class="fa fa-eye" aria-hidden="true"></i> {{__('View')}}</button>
                    <button class="btn btn-info btn-xs no-display" id="edit"><i class="fa fa-edit" aria-hidden="true"></i> {{__('Edit')}}</button>
                    <button class="btn btn-danger btn-xs no-display" id="delete-rows"><i class="fa fa-trash" aria-hidden="true"></i> {{__('Delete')}}</button>
                </div>
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('Post List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body cl-table">
                    <table id="example" class="table">
                        <thead>
                        <tr>
                            <th>{{__('Title')}}</th>
                            <th>{{__('User name')}}</th>
                            <th>{{__('Category name')}}</th>
                            <th>{{__('Thumb image')}}</th>
                            <th>{{__('Status')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.jqueryui.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: true,
                ajax: '{{url('admin-blog-list')}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.spotlist_blogs_id);
                    $(row).attr('slug',data.slug);
                },
                columns: [
                    {"data": "spotlist_blogs_title"},
                    {"data": "name"},
                    {"data": "spotlist_category_name"},
                    {"data": "blog_thumb_image"},
                    {"data": "status"}
                ],
            });
        });
        var ids = [];
        var slug = null;
        $(document).on('click', '#example tbody tr', function(){
            if($(this).toggleClass('selected')) {
                var id = $(this).attr('id');
                slug = $(this).attr('slug');
                if ($(this).hasClass('selected')) {
                    ids.push(id);
                } else {
                    ids.splice(ids.indexOf(id), 1);
                }
                if (ids.length == 1) {
                    $('#edit').show();
                    $('#view').show();
                    $('#delete-rows').show();
                }else if(ids.length > 1){
                    $('#edit').hide();
                    $('#view').hide();
                    $('#delete-rows').show();
                }else {
                    $('#edit').hide();
                    $('#view').hide();
                    $('#delete-rows').hide();
                }
            }
        });
        $("#view").on('click', function () {
            var data_id = ids[0];
            if (data_id.length === 0) {
                swalError("Please Select an Item");
                return false;
            } else {
                window.location = '<?php echo URL::to('user/blog');?>/'+slug;
            }
        });
        $("#edit").on('click', function (e) {
            var data_id = ids[0];
            if (data_id.length === 0) {
                swalError("{{__('Please_select_an_item')}}");
                return false;
            } else {
                window.location = '<?php echo URL::to('admin-blog-create');?>/'+data_id;
            }
        });
        $("#delete-rows").on('click',function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var deleted_rows = ids.length;
            if(deleted_rows === 0) {
                swalWarning("Please Select the item(s) Row from the List");
                load.ladda('stop');
            } else {
                var text = '';
                if (deleted_rows ==1){
                    text = "this row ?";
                }else{
                    text = deleted_rows+ " row (s)?";
                }
                swalConfirm("Do you really want to delete " + text).then(function (s) {
                    if(s.value){
                        var id_list = ids;
                        var url = "{{url('delete-table-rows')}}";
                        var data = {
                            deleted_ids : id_list,
                            table_name : 'spotlist_blogs',
                            primary_key : 'spotlist_blogs_id',
                        };
                        makeAjaxPost(data, url, load).done(function (response) {
                            if(response.success == false) {
                                swalError(response.message);
                            } else {
                                swalRedirect('',response.message,'success');
                            }
                        });
                    }else{
                        load.ladda('stop');
                    }
                })
            }
        });
    </script>
@endsection
