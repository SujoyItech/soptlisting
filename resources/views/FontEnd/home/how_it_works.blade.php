<div class="work-area section-padding">
    <div class="container">
        <div class="section-title section-titlev2 text-center">
            <h2>{{__('How It Works')}}</h2>
        </div>
        <div class="work-wrap">
            <div class="row">

                @php($i = 1)
                @foreach($how_it_works as $how_it_work)
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="work-sub">
                            <div class="work-item">
                                <div class="work-img">
                                    <img src="{{isset($how_it_work->icon) ? asset($how_it_work->icon) : ''}}" alt="">
                                </div>
                                <div class="work-text">
                                    <h2>{{$how_it_work->title ?? ''}}</h2>
                                    <p>{{$how_it_work->description}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

            </div>
        </div>
    </div>
</div>
