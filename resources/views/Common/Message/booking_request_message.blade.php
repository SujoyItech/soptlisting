@if(isset($messages) && !empty($messages[0]))
    @foreach($messages as $message)
        <div class="media mb-2">
            @if($message->type == 'manager')
                <img class="mr-2 rounded-circle" src="{{getImageUrl($booking_request->manager_image)}}"
                     alt="" height="32" width="32">
            @else
                <img class="mr-2 rounded-circle" src="{{getImageUrl($booking_request->user_image)}}"
                     alt="" height="32" width="32">
            @endif
            <div class="media-body">
                <h5 class="mt-0 mb-1">{{$message->type == 'manager' ? $booking_request->manager_name:$booking_request->user_name}} <small class="text-muted float-right">{{Carbon\Carbon::parse($message->time)->diffForHumans()}}</small></h5>
                {{$message->message}}

                <br/>
            </div>
        </div>
        <hr>
    @endforeach
@endif
<div class="border rounded mt-4">
    <form action="{{url('user-booking-request-message-send')}}" class="parsley-examples" method="post">
        @csrf
        <textarea rows="3" class="form-control border-0 resize-none" name="message" placeholder="Your message..." required></textarea>
        <input type="hidden" name="id" value="{{$booking_request->spotlist_booking_request_id ?? ''}}">
        <div class="p-2 bg-light d-flex justify-content-between align-items-center">
            <button type="submit" class="btn btn-info"><i class='fa fa-send'></i> {{__('Send')}}</button>
        </div>
    </form>
</div> <!-- end .border-->


