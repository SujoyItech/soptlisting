<!-- Icon fonts -->
<link href="{{asset('FontEnd/')}}/assets/css/font-awesome.min.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/flaticon.css" rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="{{asset('FontEnd/')}}/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
<!-- Plugins for this template -->
<link href="{{asset('FontEnd/')}}/assets/css/animate.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/nice-select.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/owl.carousel.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/odometer-theme-default.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/magnific-popup.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/slick.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/slick-theme.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/slicknav.min.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/jquery-ui.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/jquery.fancybox.css" rel="stylesheet">
<script src="{{asset('FontEnd/')}}/assets/js/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>

<script src="{{asset('/Backend/vendors/sweetalert/sweetalert2.all.min.js')}}"></script>
<link href="{{asset('/Backend/vendors/ladda/ladda-themeless.min.css')}}" rel="stylesheet">
<link href="{{asset('/Backend/vendors/sweetalert/sweetalert.css')}}" rel="stylesheet">

<script src="{{asset('/Backend/vendors/ladda/spin.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/ladda/ladda.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/ladda/ladda.jquery.min.js')}}"></script>
<!-- Custom styles for this template -->
<link href="{{asset('FontEnd/')}}/assets/css/style.css" rel="stylesheet">
<link href="{{asset('FontEnd/')}}/assets/css/responsive.css" rel="stylesheet">
<link href="{{asset('Backend/')}}/libs/toastr/build/toastr.min.css" rel="stylesheet" type="text/css" />


{{--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>--}}
{{--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
@yield('style')
