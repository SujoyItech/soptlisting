@extends('Backend.layouts.app',['menu'=>'all_directory','sub_menu'=>'all_wishing_list'])

@section('style')
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-heart"></i> {{__('All Wishlist')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table">
                            <thead>
                            <tr>
                                <th>{{__('Picture')}}</th>
                                <th>{{__('Title')}}</th>
                                <th>{{__('Type')}}</th>
                                <th>{{__('Category')}}</th>
                                <th>{{__('Location')}}</th>
                                <th>{{__('Uploaded by')}}</th>
                                <th>{{__('Date')}}</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.jqueryui.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: true,
                ajax: '{{url('admin-all-wishing-list')}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.spotlist_directory_id);
                },
                columns: [
                    {"data": "thumb_image"},
                    {"data": "title"},
                    {"data": "type"},
                    {"data": "category_name"},
                    {"data": "address"},
                    {"data": "username"},
                    {"data": "created_at"},
                ],
            });
        });

    </script>
@endsection
