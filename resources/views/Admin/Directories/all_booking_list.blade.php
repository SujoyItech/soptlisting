@extends('Backend.layouts.app',['menu'=>'all_booking'])

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('All Booking List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table">
                        <thead>
                        <tr>
                            <th>{{__('ID')}} </th>
                            <th>{{__('Listing')}}</th>
                            <th>{{__('Customer')}}</th>
                            <th>{{__('Additional information')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Date')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.jqueryui.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: true,
                ajax: '{{url('all-booking-request')}}',
                columnDefs: [ { type: 'date', 'targets': [5] } ],
                order: [[ 5, "asc" ]],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.spotlist_booking_request_id);
                },
                columns: [
                    {"data": "booking_id"},
                    {"data": "listing"},
                    {"data": "user_details"},
                    {"data": "additional_information"},
                    {"data": "status"},
                    {"data": "date"},
                ],
            });
        });

    </script>
@endsection
