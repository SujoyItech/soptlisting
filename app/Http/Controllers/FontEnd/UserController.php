<?php

namespace App\Http\Controllers\FontEnd;

use App\Http\Controllers\Common\Profile\ProfileService;
use App\Http\Controllers\Controller;
use App\Http\Requests\FontEnd\UserEmailSubscriptionRequest;
use App\Http\Service\NotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $service;
    public function __construct() {
        $this->service = new FontEndService();
    }

    public function followUser(Request $request){
        $response = $this->service->followUser($request);
        return response()->json($response);
    }

    public function userEmailSubscription(UserEmailSubscriptionRequest $request){
        $response = $this->service->userEmailSubscription($request);
        return $response['success'] == TRUE ? redirect()->back()->with(['success'=>$response['message']]) : redirect()->back()->with(['dismiss'=>$response['message']]);
    }

    public function userMessageSend(Request $request){
        $response = $this->service->userMessageSend($request);
        return $response['success'] == TRUE ? redirect()->back()->with(['success'=>$response['message']]) : redirect()->back()->with(['dismiss'=>$response['message']]);
    }

    public function profileSettings(){
        $profile = $this->service->getProfile();
        return view('FontEnd.profile.profile_update',$profile);
    }
    public function userProfileUpdate(Request $request){
        $service = new ProfileService();
        $response = $service->profileUpdate($request);
        if ($response['success'] == TRUE){
            return redirect()->back()->with(['success'=>$response['message']]);
        }else{
            return redirect()->back()->with(['dismiss'=>$response['message']]);
        }
    }

    public function userPasswordUpdate(Request $request){
        $service = new ProfileService();
        $response = $service->passwordUpdate($request);
        if ($response['success'] == TRUE){
            return redirect()->back()->with(['success'=>$response['message']]);
        }else{
            return redirect()->back()->with(['dismiss'=>$response['message']]);
        }
    }


}
