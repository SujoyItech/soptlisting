@if(isset($restaurant) && !empty($restaurant))
    <div class="card">
        <img class="card-img-top" src="{{getImage($restaurant->menu_image)}}" alt="" height="300">
        <div class="card-body">
            <h5 class="card-title">{{$restaurant->menu_name}}</h5>
            @if(isset($restaurant->menu_price))
                <span>{{__('Price: ')}} {{$restaurant->menu_price}} ({{get_currency()}})</span><br>
            @endif
            @if(isset($restaurant->ingredients_item))
                @php($varient = explode(',',$restaurant->ingredients_item))
                @foreach($varient as $var)
                    <span class="badge badge-success mt-3 text-white">{{$var}}</span>
                @endforeach
            @endif
            <p class="card-text">{{$restaurant->menu_description ?? ''}}</p>
        </div>
    </div>
@elseif(isset($hotel) && !empty($hotel))
    <div class="card">
        <img class="card-img-top" src="{{getImage($hotel->room_image)}}" alt="" height="300">
        <div class="card-body">
            <h5 class="card-title">{{$hotel->room_name}}</h5>
            @if(isset($hotel->room_price))
                <span>{{__('Price: ')}} {{$hotel->room_price}} ({{get_currency()}})</span><br>
            @endif
            @if(isset($hotel->room_facilities))
                @php($varient = explode(',',$hotel->room_facilities))
                @foreach($varient as $var)
                    <span class="badge badge-success mt-3 text-white">{{$var}}</span>
                @endforeach
            @endif
            <p class="card-text">{{$hotel->room_description ?? ''}}</p>
        </div>
    </div>
@elseif(isset($beauty) && !empty($beauty))
    <div class="card">
        <img class="card-img-top" src="{{getImage($beauty->service_image)}}" alt="" height="300">
        <div class="card-body">
            <h5 class="card-title">{{$beauty->service_name}}</h5>
            @if(isset($beauty->service_price))
                <span>{{__('Price: ')}} {{$beauty->service_price}} ({{get_currency()}})</span>
            @endif
            <p class="card-text">{{$beauty->service_description ?? ''}}</p>
        </div>
    </div>
@elseif(isset($fitness) && !empty($fitness))
    <div class="card">
        <img class="card-img-top" src="{{getImage($fitness->fitness_service_image)}}" alt="" height="300">
        <div class="card-body">
            <h5 class="card-title">{{$fitness->fitness_service_name}}</h5>
            <span>{{__('Price: ')}} {{$fitness->fitness_service_price ?? 0}} ({{get_currency()}})</span>
            <p class="card-text">{{$fitness->fitness_service_description ?? ''}}</p>
        </div>
    </div>
@elseif(isset($shop) && !empty($shop))
    <div class="card">
        <img class="card-img-top" src="{{getImage($shop->product_image)}}" alt="" height="300">
        <div class="card-body">
            <h5 class="card-title">{{$shop->product_name}}</h5>
            @if(isset($restaurant->product_price))
                <span>{{__('Price: ')}} {{$shop->product_price }} ({{get_currency()}})</span><br>
            @endif
            @if(isset($shop->product_varient))
                @php($varient = explode(',',$shop->product_varient))
                @foreach($varient as $var)
                    <span class="badge badge-success mt-3 text-white">{{$var}}</span>
                @endforeach
            @endif
            <p class="card-text">{{$shop->product_description ?? ''}}</p>
        </div>
    </div>
@endif
