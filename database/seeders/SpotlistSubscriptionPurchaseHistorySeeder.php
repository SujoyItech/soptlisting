<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistSubscriptionPurchaseHistorySeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_subscription_purchase_history')->delete();
        
        \DB::table('spotlist_subscription_purchase_history')->insert(array (
            0 => 
            array (
                'spotlist_subscription_purchase_history_id' => 1,
                'spotlist_packages_id' => 1,
                'user_id' => 5,
                'purchase_date' => '2020-10-13 06:24:43',
                'amount_paid' => '0.00',
                'payment_method' => 'Card',
                'purchase_log_data' => '{"amount":0,"description":"User free subscription"}',
                'payment_status' => 'Free',
                'status' => 'Active',
                'created_at' => '2020-10-13 06:24:43',
                'updated_at' => NULL,
            ),
        ));

        
    }
}