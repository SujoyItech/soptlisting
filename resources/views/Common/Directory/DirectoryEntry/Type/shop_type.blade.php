@if(isset($shop_data) && !empty($shop_data[0]))
    @foreach($shop_data as $shop)
        <div class="row mb-3 heading">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Product name')}}</label>
                            <input type="text" name="product_name[]" value="{{$shop->product_name ?? ''}}"
                                   placeholder="Enter shop name" class="form-control">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Variants (Press enter after entering every variant)')}}</label>
                            <input type="text" data-role="tagsinput" name="product_varient[]" value="{{$shop->product_varient ?? ''}}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Product price')}} ( {{get_currency()}} )</label>
                            <input type="number" class="form-control" name="product_price[]" value="{{$shop->product_price ?? ''}}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Product description( Max length 500 word )')}}</label>
                            <textarea name="product_description[]" class="form-control" maxlength="500">{{$shop->product_description ?? ''}}</textarea>
                        </div>
                    </div>

                    <div class="col-12">
                        <input type="hidden" name="spotlist_directory_type_shop_id[]" value="{{$shop->spotlist_directory_type_shop_id ?? ''}}">
                        <button class="btn btn-danger remove_info" type="button"
                                data-id="{{$shop->spotlist_directory_type_shop_id ?? ''}}" data-table="spotlist_directory_type_shop" data-primary-key="spotlist_directory_type_shop_id">
                            <i class="fa fa-minus"></i> {{__('Remove')}}</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="col-form-label">{{__('Product image')}}</label>
                    <input type="file" name="product_image[]" data-plugins="dropify" class="dropify"
                           data-default-file="{{isset($shop->product_image) ? asset($shop->product_image) : ''}}"
                           data-allowed-file-extensions="png jpg jpeg jfif"
                           data-max-file-size="2M" />
                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                </div>
            </div>
        </div>
    @endforeach
@endif
<div class="product-list">
    <div class="row mb-5">
        <div class="col-lg-9">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Product name')}}</label>
                        <input type="text" name="product_name[]" value=""
                               placeholder="Enter shop name" class="form-control">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Variants (Press enter after entering every variant)')}}</label>
                        <input type="text" value="" data-role="tagsinput" name="product_varient[]">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Product price')}} ( {{get_currency()}} )</label>
                        <input type="number" class="form-control" name="product_price[]">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Product description( Max length 500 word )')}}</label>
                        <textarea name="product_description[]" class="form-control" maxlength="500"></textarea>
                    </div>
                </div>

                <div class="col-12">
                    <button class="btn btn-info add_product" type="button"><i class="fa fa-plus"></i> {{__('Add more')}}</button>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="col-form-label">{{__('Product image')}}</label>
                <input type="file" name="product_image[]" data-plugins="dropify" class="dropify"
                       data-default-file=""
                       data-allowed-file-extensions="png jpg jpeg jfif"
                       data-max-file-size="2M" />
                <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
            </div>
        </div>
    </div>
</div>


<script>

    $('.add_product').on('click',function (){

        var product_html = '<div class="row product_heading mb-5">\n' +
            '    <div class="col-lg-9">\n' +
            '        <div class="row">\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Product name')}}</label>\n' +
            '                    <input type="text" name="product_name[]" parsley-trigger="change" required value=""\n' +
            '                           placeholder="Enter shop name" class="form-control">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Variants (Press enter after entering every variant)')}}</label>\n' +
            '                    <input type="text" value="" data-role="tagsinput" name="product_varient[]">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Product price')}} ({{get_currency()}})</label>\n' +
            '                    <input type="number" class="form-control" name="product_price[]">\n' +
            '                </div>\n' +
            '            </div>\n' +
        '                <div class="col-12">\n' +
        '                     <div class="form-group">\n' +
        '                        <label class="col-form-label">{{__('Product description( Max length 500 word )')}}</label>\n' +
        '                        <textarea name="product_description[]" class="form-control" maxlength="500"></textarea>\n' +
        '                     </div>\n' +
        '                 </div>'+
            '\n' +
            '            <div class="col-12">\n' +
            '                <button class="btn btn-danger remove_product" type="button"><i class="fa fa-minus"></i> {{__('Remove')}}</button>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '    <div class="col-lg-3">\n' +
            '        <div class="form-group">\n' +
            '            <label class="col-form-label">{{__('Product image')}}</label>\n' +
            '            <input type="file" name="product_image[]" data-plugins="dropify" class="dropify"\n' +
            '                   data-default-file=""\n' +
            '                   data-allowed-file-extensions="png jpg jpeg jfif"\n' +
            '                   data-max-file-size="2M" />\n' +
            '            <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';

        $('.product-list').append(product_html);
        $('input[data-role=tagsinput]').tagsinput();
        $('.dropify').dropify();
    });

    $(document).on('click','.remove_product',function (e) {
        e.preventDefault();
        $(this).closest('.product_heading').remove();
    })
</script>

