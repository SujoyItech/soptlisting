<?php


namespace App\Http\Controllers\Admin\UserModule;

use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\UserRequest;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserService {
    use DataSaveTrait;

    public function getAdminUserData($id = NULL){
        $data = [];
        if ($id != NULL){
            $data['user'] = $this->userListData($id);
        }
        return $data;
    }

    public function userListData($user_id = NULL){
        $user_list = DB::table('sys_users')
                       ->leftJoin('sys_users_additional_info','sys_users.id','sys_users_additional_info.sys_users_id')
                       ->select('sys_users.*', 'sys_users_additional_info.website','sys_users_additional_info.facebook_link', 'sys_users_additional_info.twitter_link',
                           'sys_users_additional_info.linkedin_link','sys_users_additional_info.about');

        if ($user_id != NULL){
            $user_list = $user_list->where('sys_users.id',$user_id)->first();
        }else{
            $user_list = $user_list->where('sys_users.id','<>',Auth::user()->id)->orderBy('id','desc')->get();
        }

        return $user_list;
    }

    public function adminUserStatusChange($user_id,$status){
        $post_data = ['status'=>$status];
        $success_message = __('User ').$status.__(' successfully');
        $warning_message = __('User ').$status.__(' failed');
        return $this->updateTable('sys_users',$post_data,'id',$user_id,$success_message,$warning_message);
    }

    public function adminUserInformationSave(UserRequest $request){
        $user_data = $this->getUserData($request);
        $user_data_response = $this->insertOrUpdateTable('sys_users','id',$request->sys_users_id ?? '',$user_data);
        $user_additional_info = $this->getUserInfoData($request,$user_data_response['data']['id']);
        $user_additional_info_response = $this->insertOrUpdateTable('sys_users_additional_info','sys_users_id',$user_data_response['data']['id'],$user_additional_info);
        return $user_data_response;
    }

    private function getUserData(Request $request){
        $post_data  = [
            'name' => $request->name,
            'username' => $request->name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'address' => $request->address,
            'default_module_id' => $request->default_module_id,
        ];

        if (!empty($request->sys_users_id)){
            $post_data['updated_at'] = Carbon::now();
        }else{
            $post_data['status'] = 'Active';
            $post_data['email_verified'] = TRUE;
            $post_data['remember_token'] = md5($request->email . uniqid() . randomString(5));
            $post_data['created_at'] = Carbon::now();
            $post_data['updated_at'] = Carbon::now();
            $post_data['password'] = bcrypt($request->password);
        }
        if (!empty($request->user_image)){
            $post_data['user_image'] = fileUpload($request->user_image,'Backend/images/users/');
        }
        return $post_data;
    }

    private function getUserInfoData(Request $request,$user_id){
        $post_data = [
            'sys_users_id' => $user_id,
            'website' => $request->website,
            'facebook_link' => $request->facebook_link,
            'twitter_link' => $request->twitter_link,
            'linkedin_link' => $request->linkedin_link,
            'about' => $request->about,
        ];
        return $post_data;
    }



    public function adminUserPasswordUpdate(Request $request){
        try {
            $insert_data = [
                'password' => bcrypt($request->password)
            ];
            DB::table('sys_users')->where('id', $request->user_id)->update($insert_data);
            $response = $this->responseData(TRUE,'User password updated successfully.');
        }catch (\Exception $exception)
        {
            $response = $this->responseData(FALSE,'User password update failed.');
        }
        return $response;
    }

    public function userEmailSubscriberListData(){
        return DB::table('spotlist_user_subscription_by_email')->orderBy('created_at','desc')->get();
    }

    public function adminSubscribedEmailStatusChange($id,$status){
        $post_data = ['status'=>$status];
        $success_message = __('User subscription ').$status.__(' successfully');
        $warning_message = __('User subscription ').$status.__(' failed');
        return $this->updateTable('spotlist_user_subscription_by_email',$post_data,'id',$id,$success_message,$warning_message);
    }

    public function userMessageListData(){
        return DB::table('spotlist_user_message_table')->orderBy('created_at','desc')->get();
    }

    public function adminGetUserMessage(Request $request){
        try {
            $message = DB::table('spotlist_user_message_table')->where('id',$request->id)->first();
            if ($message->status == 'Pending'){
                DB::table('spotlist_user_message_table')->where('id',$request->id)->update(['status'=>'Seen']);
            }
            $response = $this->responseData(TRUE,__('Message get successfully'),$message);
        }catch (\Exception $exception){
            $response = $this->responseData(TRUE,__('Message get failed'));
        }
        return $response;
    }

    public function getTopUserChartData(){
        $data = [];
        $data['top_users'] = DB::table('sys_users')
            ->join('spotlist_directory','sys_users.id','=','spotlist_directory.sys_users_id')
            ->select('sys_users.username as name',DB::raw("COUNT(spotlist_directory.spotlist_directory_id) AS y"))
            ->where('sys_users.default_module_id',LISTING_MANAGER)
            ->groupBy('sys_users.id')
            ->orderBy('y','desc')
            ->limit(10)->get()->toArray();
        return $data;
    }

}
