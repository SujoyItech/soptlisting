@extends('Backend.layouts.app',['menu'=>'settings','sub_menu'=>'teams'])

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
    <style>
        .card-widgets {
            float: right;
            height: 16px;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card edit-form no-display">
                <div class="card-header">
                    <h3 class="master-grid-title">
                        <i class="fa fa-edit"></i> {{__('Spotlist Team')}}
                        <div class="card-widgets">
                            <button class="btn btn-xs btn-outline-danger close-link"><i class="fa fa-eye-slash"></i></button>
                        </div>
                    </h3>
                </div>
                <div class="card-body team-form">

                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="master-grid-title">
                        <div class="row">
                            <div class="col-4">
                                <h4 class="page-title"><i class="fa fa-list"></i> {{__('Team List')}}</h4>
                            </div>
                            <div class="col-8">
                                <div class="card-widgets">
                                    <button class="btn btn-primary btn-xs text-white" id="new" href=""><i class="fa fa-plus" aria-hidden="true"></i> {{__('Add new')}}</button>
                                    <button class="btn btn-info btn-xs no-display" id="edit"><i class="fa fa-edit" aria-hidden="true"></i> {{__('Edit')}}</button>
                                    <button class="btn btn-danger btn-xs no-display" id="delete-rows"><i class="fa fa-trash" aria-hidden="true"></i> {{__('Delete')}}</button>
                                </div>
                            </div>
                        </div>

                    </h3>
                </div>
                <div class="card-body">
                    <table id="example" class="table">
                        <thead>
                        <tr>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Designation')}}</th>
                            <th>{{__('Picture')}}</th>
                            <th>{{__('Status')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: false,
                ajax: '{{url('teams')}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.id);
                },
                columns: [
                    {"data": "name"},
                    {"data": "designation"},
                    {"data": "picture"},
                    {"data": "status"},
                ],
            });
        });

        var ids = [];
        $(document).on('click', '#example tbody tr', function(){
            if($(this).toggleClass('selected')) {
                var id = $(this).attr('id');
                if ($(this).hasClass('selected')) {
                    ids.push(id);
                } else {
                    ids.splice(ids.indexOf(id), 1);
                }
                if (ids.length == 1) {
                    $('#edit').show();
                    $('#delete-rows').show();
                }else if(ids.length > 1){
                    $('#edit').hide();
                    $('#delete-rows').show();
                }else {
                    $('#edit').hide();
                    $('#delete-rows').hide();
                }
            }
        });

        $('#new').on('click',function (){
            Ladda.bind(this);
            var load = $(this).ladda();
            var team_data_url = "{{url('get-team-data')}}";

            makeAjaxText(team_data_url, load).done(function (response) {
                load.ladda('stop');
                $('.team-form').html(response);
                $('.edit-form').show();
            });
        });

        $('.close-link').on('click',function (){
            $(this).closest('.edit-form').hide();
        });

        $("#edit").on('click', function (e) {
            var data_id = ids[0];
            if (data_id.length === 0) {
                swalError("{{__('Please_select_an_item')}}");
                return false;
            } else {
                Ladda.bind(this);
                var load = $(this).ladda();
                var team_data_url = "{{url('get-team-data')}}"+'/'+data_id;

                makeAjaxText(team_data_url, load).done(function (response) {
                    load.ladda('stop');
                    $('.team-form').html(response);
                    $('.edit-form').show();
                });
            }
        });

        $("#delete-rows").on('click',function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var deleted_rows = ids.length;
            if(deleted_rows === 0) {
                swalWarning("Please Select the item(s) Row from the List");
                load.ladda('stop');
            } else {
                var text = '';
                if (deleted_rows ==1){
                    text = "this row ?";
                }else{
                    text = deleted_rows+ " row (s)?";
                }
                swalConfirm("Do you really want to delete " + text).then(function (s) {
                    if(s.value){
                        var id_list = ids;
                        var url = "{{url('delete-table-rows')}}";
                        var data = {
                            deleted_ids : id_list,
                            table_name : 'spotlisting_teams',
                            primary_key : 'id',
                        };
                        makeAjaxPost(data, url, load).done(function (response) {
                            if(response.success == false) {
                                swalError(response.message);
                            } else {
                                swalRedirect('',response.message,'success');
                            }
                        });
                    }else{
                        load.ladda('stop');
                    }
                })
            }
        });

    </script>
@endsection
