<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SysDropdownsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sys_dropdowns')->delete();
        
        \DB::table('sys_dropdowns')->insert(array (
            0 => 
            array (
                'id' => 12,
                'dropdown_slug' => 'module-list',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT
sys_modules.id,
sys_modules.name',
                'sqlsource' => 'FROM
sys_modules',
                'sqlcondition' => NULL,
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'id',
                'option_field' => 'name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'sys_modules_id',
                'description' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-02-11 15:33:00',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 13,
                'dropdown_slug' => 'user_level',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT sys_user_levels.id,sys_user_levels.name ',
                'sqlsource' => 'FROM `sys_user_levels`',
                'sqlcondition' => NULL,
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => 'ORDER BY sys_user_levels.id ASC',
                'sqllimit' => NULL,
                'value_field' => 'id',
                'option_field' => 'name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'sys_user_level_id',
                'description' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-02-17 17:23:07',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 14,
                'dropdown_slug' => 'user_list',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT
sys_users.id,
sys_users.name

',
                'sqlsource' => 'FROM
sys_users',
                'sqlcondition' => NULL,
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'id',
                'option_field' => 'name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'sys_user_id',
                'description' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-02-20 17:56:03',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 15,
                'dropdown_slug' => 'project_truck',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT project_truck.project_truck_id, project_truck.project_truck_name',
                'sqlsource' => 'FROM project_truck',
                'sqlcondition' => 'WHERE `status` = \'Active\'',
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'project_truck_id',
                'option_field' => 'project_truck_name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'project_truck_id',
                'description' => '',
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-06-13 04:03:53',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 16,
                'dropdown_slug' => 'parent_level',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT parent_level_id, parent_level_name FROM parent_level WHERE `status` = \'Active\'',
                'sqlsource' => NULL,
                'sqlcondition' => NULL,
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'parent_level_id',
                'option_field' => 'parent_level_name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'parent_level_id',
                'description' => '',
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-06-16 14:19:22',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 18,
                'dropdown_slug' => 'master_entry_dropdown',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT
sys_master_entry.sys_master_entry_name',
                'sqlsource' => 'FROM
sys_master_entry',
                'sqlcondition' => NULL,
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'sys_master_entry_name',
                'option_field' => 'sys_master_entry_name',
                'multiple' => NULL,
                'search_columns' => NULL,
                'dropdown_name' => 'sys_master_grid_name',
                'description' => NULL,
                'created_by' => 1,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-06-18 20:31:02',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 19,
                'dropdown_slug' => 'dropdown_slug',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT dropdown_slug',
                'sqlsource' => 'FROM sys_dropdowns',
                'sqlcondition' => NULL,
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'dropdown_slug',
                'option_field' => 'dropdown_slug',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'dropdown_slug',
                'description' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-06-19 17:57:10',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 20,
                'dropdown_slug' => 'search_panel_slug',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'select search_panel_name',
                'sqlsource' => 'from sys_search_panel',
                'sqlcondition' => NULL,
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'search_panel_name',
                'option_field' => 'search_panel_name',
                'multiple' => NULL,
                'search_columns' => NULL,
                'dropdown_name' => 'Search Panel Dropdown List',
                'description' => NULL,
                'created_by' => 1,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-06-20 21:44:51',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 21,
                'dropdown_slug' => 'spotlist_parent_category',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT spotlist_category_id as parent_id,spotlist_category_name as parent_name',
                'sqlsource' => 'from spotlist_category ',
                'sqlcondition' => 'WHERE `status` = \'Active\' AND parent_id = 0',
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'parent_id',
                'option_field' => 'parent_name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'parent_id',
                'description' => '',
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 22,
                'dropdown_slug' => 'spotlist_category',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT spotlist_category_id, spotlist_category_name',
                'sqlsource' => 'from spotlist_category ',
                'sqlcondition' => 'WHERE `status` = \'Active\'',
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'spotlist_category_id',
                'option_field' => 'spotlist_category_name',
                'multiple' => NULL,
                'search_columns' => NULL,
                'dropdown_name' => 'spotlist_category_id',
                'description' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 23,
                'dropdown_slug' => 'customer_list',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT
sys_users.id,
sys_users.name

',
                'sqlsource' => 'FROM
sys_users',
                'sqlcondition' => 'WHERE  default_module_id = 4',
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'id',
                'option_field' => 'name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'sys_user_id',
                'description' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-02-20 17:56:03',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 24,
                'dropdown_slug' => 'spotlist_packages',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT
spotlist_packages.spotlist_packages_id, 
spotlist_packages.spotlist_packages_name, 
spotlist_packages.price',
                'sqlsource' => 'FROM
spotlist_packages',
                'sqlcondition' => 'WHERE `status` = \'Active\'',
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'spotlist_packages_id',
                'option_field' => 'spotlist_packages_name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'spotlist_packages_id',
                'description' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-02-20 17:56:03',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 25,
                'dropdown_slug' => 'spotlist_countries',
                'dropdown_mode' => 'dropdown',
                'sys_search_panel_slug' => NULL,
                'sqltext' => 'SELECT *',
                'sqlsource' => 'FROM
spotlist_countries',
                'sqlcondition' => '',
                'sqlgroupby' => NULL,
                'sqlhaving' => NULL,
                'sqlorderby' => NULL,
                'sqllimit' => NULL,
                'value_field' => 'id',
                'option_field' => 'name',
                'multiple' => 0,
                'search_columns' => NULL,
                'dropdown_name' => 'country_id',
                'description' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'status' => 'Active',
                'created_at' => '2020-02-20 17:56:03',
                'updated_at' => NULL,
            ),
        ));

        
    }
}