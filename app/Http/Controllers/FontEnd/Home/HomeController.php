<?php

namespace App\Http\Controllers\FontEnd\Home;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FontEnd\FontEndService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $service;

    public function __construct(){
        $this->service = new HomeService();
    }
    public function index(){
        $data = $this->service->homePageData();
        return view('FontEnd.home.home',$data);
    }


}
