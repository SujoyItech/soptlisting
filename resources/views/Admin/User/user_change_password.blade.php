<div id="accordion" class="mb-3">
    <div class="card mb-1">
        <div class="card-header" id="headingThree">
            <h5 class="m-0">
                <a class="text-dark" data-toggle="collapse" href="#collapseThree" aria-expanded="true">
                    <i class="fa fa-key"></i> {{__('Password')}}
                </a>
            </h5>
        </div>

        <div id="collapseThree" class="collapse show mt-3" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="row">
                <div class="col-sm-12">
                    <form action="{{url('admin-user-password-update')}}" class="parsley-examples" method="POST" id="update_password" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="pass1">{{__('New Password')}}<span class="text-danger">*</span></label>
                                    <input id="pass1" type="password" minlength="8" name="password" placeholder="Password" required
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="passWord2">{{__('Confirm Password')}} <span class="text-danger">*</span></label>
                                    <input data-parsley-equalto="#pass1" minlength="8" type="password" required name="confirm_password"
                                           placeholder="Password" class="form-control" id="passWord2">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="hidden" name="user_id" value="{{$user->id ?? ''}}">
                                    <label></label>
                                    <button class="btn btn-rounded btn-primary waves-effect waves-light update_password" type="submit">
                                        <i class="fa fa-save"></i>
                                        {{__('Change Password')}}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.update_password').on('click', function (e) {
        Ladda.bind(this);
        var load = $(this).ladda();
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);
                makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                    load.ladda('stop');
                    if (response.success == true){
                        var redirect_url = "{{Request::url()}}";
                        swalRedirect(redirect_url, response.message, 'success');
                    }else{
                        var redirect_url = "{{Request::url()}}";
                        swalRedirect(redirect_url, 'Something went wrong!', 'error');
                    }
                })
            }
        });
    });

</script>
