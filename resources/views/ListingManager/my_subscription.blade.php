@extends('Backend.layouts.app',['menu'=>'my_subscription'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa fa fa-gift"></i> {{__('My subscriptions')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="card-box bg-pattern">

                <div class="text-center">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">{{__('Days remain:')}} {{$subscription->day_remain ?? 0}}</li>
                        <li class="list-group-item">{{__('Num of listing:')}} {{$subscription->listing_item_used ?? 0}}/{{$subscription->num_of_listing ?? 0}}</li>
                        <li class="list-group-item">{{__('Num of photos:')}} {{$subscription->photos_used ?? 0}}/{{$subscription->num_of_photos ?? 0}}</li>
                        <li class="list-group-item">{{__('Video ability:')}} {{isset($subscription->add_video_ability) && $subscription->add_video_ability == TRUE ? 'Video enabled' : 'Video disabled'}}</li>
                        <li class="list-group-item">{{__('Is free subscription purchased:')}} {{isset($subscription->free_subscription_check) && $subscription->free_subscription_check == TRUE ? 'Purchased' : 'Not purchased yet'}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card-box">
                <div class="text-center">
                    <a href="{{route('listing-manager-pricing')}}" class="btn btn-info btn-sm">{{__('Subscribe new packages')}}</a>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('script')

@endsection
