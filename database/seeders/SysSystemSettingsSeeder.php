<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SysSystemSettingsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sys_system_settings')->delete();
        
        \DB::table('sys_system_settings')->insert(array (
            0 => 
            array (
                'sys_system_settings_id' => 1,
                'option_group' => 'admin_settings',
                'option_key' => 'notification',
                'option_value' => 'On',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            1 => 
            array (
                'sys_system_settings_id' => 2,
                'option_group' => 'admin_settings',
                'option_key' => 'qwe',
                'option_value' => 'qwe qwe',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            2 => 
            array (
                'sys_system_settings_id' => 3,
                'option_group' => 'admin_settings',
                'option_key' => 'layout',
                'option_value' => 'detached',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            3 => 
            array (
                'sys_system_settings_id' => 4,
                'option_group' => 'admin_settings',
                'option_key' => 'theme',
                'option_value' => 'light',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            4 => 
            array (
                'sys_system_settings_id' => 5,
                'option_group' => 'admin_settings',
                'option_key' => 'theme_width',
                'option_value' => 'fluid',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            5 => 
            array (
                'sys_system_settings_id' => 6,
                'option_group' => 'admin_settings',
                'option_key' => 'menu_position',
                'option_value' => 'fixed',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            6 => 
            array (
                'sys_system_settings_id' => 7,
                'option_group' => 'admin_settings',
                'option_key' => 'sidebar_theme',
                'option_value' => 'light',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            7 => 
            array (
                'sys_system_settings_id' => 8,
                'option_group' => 'admin_settings',
                'option_key' => 'sidebar_size',
                'option_value' => 'default',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            8 => 
            array (
                'sys_system_settings_id' => 9,
                'option_group' => 'admin_settings',
                'option_key' => 'sidbar_show_user',
                'option_value' => 'true',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            9 => 
            array (
                'sys_system_settings_id' => 10,
                'option_group' => 'admin_settings',
                'option_key' => 'theme_topbar',
                'option_value' => 'dark',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            10 => 
            array (
                'sys_system_settings_id' => 18,
                'option_group' => 'admin_settings',
                'option_key' => 'favicon',
                'option_value' => 'Backend/images/favicon.ico',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            11 => 
            array (
                'sys_system_settings_id' => 19,
                'option_group' => 'admin_settings',
                'option_key' => 'dark_logo_large',
                'option_value' => 'Backend/images/dark_logo_large.jpg',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            12 => 
            array (
                'sys_system_settings_id' => 20,
                'option_group' => 'admin_settings',
                'option_key' => 'dark_logo_small',
                'option_value' => 'Backend/images/dark_logo_small.jpg',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            13 => 
            array (
                'sys_system_settings_id' => 21,
                'option_group' => 'admin_settings',
                'option_key' => 'light_logo_large',
                'option_value' => 'Backend/images/light_logo_large.jpg',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            14 => 
            array (
                'sys_system_settings_id' => 22,
                'option_group' => 'admin_settings',
                'option_key' => 'light_logo_small',
                'option_value' => 'Backend/images/light_logo_small.jpg',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            15 => 
            array (
                'sys_system_settings_id' => 23,
                'option_group' => 'admin_settings',
                'option_key' => 'copyright',
                'option_value' => '<i class="fa fa-copyright"></i> asdasdasd',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            16 => 
            array (
                'sys_system_settings_id' => 30,
                'option_group' => 'application_settings',
                'option_key' => 'file_upload_test_name',
                'option_value' => '12',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            17 => 
            array (
                'sys_system_settings_id' => 31,
                'option_group' => 'application_settings',
                'option_key' => 'doc_file',
                'option_value' => 'Backend/storage/pcidI9Sp3Umy7gmb.jpg',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            18 => 
            array (
                'sys_system_settings_id' => 32,
                'option_group' => 'application_settings',
                'option_key' => 'status',
                'option_value' => 'Active',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            19 => 
            array (
                'sys_system_settings_id' => 33,
                'option_group' => 'application_settings',
                'option_key' => 'file_image',
                'option_value' => 'Backend/images/qxtGDTVmRSvvT86F.docx',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            20 => 
            array (
                'sys_system_settings_id' => 34,
                'option_group' => 'application_settings',
                'option_key' => 'theme_settings',
                'option_value' => 'On',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            21 => 
            array (
                'sys_system_settings_id' => 36,
                'option_group' => 'application_settings',
                'option_key' => 'app_notification',
                'option_value' => 'Off',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            22 => 
            array (
                'sys_system_settings_id' => 37,
                'option_group' => 'application_settings',
                'option_key' => 'site_title',
                'option_value' => 'Lara Charm',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            23 => 
            array (
                'sys_system_settings_id' => 38,
                'option_group' => 'application_settings',
                'option_key' => 'app_lang',
                'option_value' => 'Off',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            24 => 
            array (
                'sys_system_settings_id' => 50,
                'option_group' => 'admin_settings',
                'option_key' => 'system_title',
                'option_value' => 'rewrewwer',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            25 => 
            array (
                'sys_system_settings_id' => 51,
                'option_group' => 'frontend_settings',
                'option_key' => 'status',
                'option_value' => 'Active',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            26 => 
            array (
                'sys_system_settings_id' => 52,
                'option_group' => 'frontend_settings',
                'option_key' => 'app_title',
                'option_value' => 'App titlle',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            27 => 
            array (
                'sys_system_settings_id' => 53,
                'option_group' => 'frontend_settings',
                'option_key' => 'home_page_title',
                'option_value' => 'Home page title',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            28 => 
            array (
                'sys_system_settings_id' => 54,
                'option_group' => 'frontend_settings',
                'option_key' => 'contact_email',
                'option_value' => 'sfs@gmail.com',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            29 => 
            array (
                'sys_system_settings_id' => 55,
                'option_group' => 'frontend_settings',
                'option_key' => 'contact_number',
                'option_value' => '239432882',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            30 => 
            array (
                'sys_system_settings_id' => 56,
                'option_group' => 'frontend_settings',
                'option_key' => 'contact_address',
                'option_value' => 'sfsdf',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            31 => 
            array (
                'sys_system_settings_id' => 57,
                'option_group' => 'frontend_settings',
                'option_key' => 'facebook_link',
                'option_value' => 'www.facebook.com',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            32 => 
            array (
                'sys_system_settings_id' => 58,
                'option_group' => 'frontend_settings',
                'option_key' => 'twitter_link',
                'option_value' => 'www.twitter.com',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            33 => 
            array (
                'sys_system_settings_id' => 59,
                'option_group' => 'frontend_settings',
                'option_key' => 'instagram_link',
                'option_value' => 'www.instagram.com',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            34 => 
            array (
                'sys_system_settings_id' => 60,
                'option_group' => 'frontend_settings',
                'option_key' => 'google_link',
                'option_value' => 'www.google.com',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            35 => 
            array (
                'sys_system_settings_id' => 61,
                'option_group' => 'frontend_settings',
                'option_key' => 'description',
                'option_value' => 'some text will be written',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            36 => 
            array (
                'sys_system_settings_id' => 62,
                'option_group' => 'frontend_settings',
                'option_key' => 'footer_descrpition',
                'option_value' => 'some text will be written about footer',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            37 => 
            array (
                'sys_system_settings_id' => 63,
                'option_group' => 'frontend_settings',
                'option_key' => 'copy_right_text',
                'option_value' => '@copyright 2020',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            38 => 
            array (
                'sys_system_settings_id' => 64,
                'option_group' => 'frontend_settings',
                'option_key' => 'about_us',
                'option_value' => 'some text will be written about us',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            39 => 
            array (
                'sys_system_settings_id' => 65,
                'option_group' => 'frontend_settings',
                'option_key' => 'google_location',
                'option_value' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.5873335784217!2d89.55269461195519!3d22.817750010413683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff90068a55a627%3A0x38f7c151c31a27ab!2sMutual%20Trust%20Bank%20Limited!5e0!3m2!1sen!2sbd!4v1600859967553!5m2!1sen!2sbd" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            40 => 
            array (
                'sys_system_settings_id' => 66,
                'option_group' => 'frontend_settings',
                'option_key' => 'app_logo',
                'option_value' => 'Backend/images/application/uQye6H9IMaZsg4SK.png',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            41 => 
            array (
                'sys_system_settings_id' => 67,
                'option_group' => 'frontend_settings',
                'option_key' => 'favicon_logo',
                'option_value' => 'Backend/images/application/DVGIBqwNYVnr2ktB.png',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            42 => 
            array (
                'sys_system_settings_id' => 68,
                'option_group' => 'payment_settings',
                'option_key' => 'payment_method',
                'option_value' => 'brain_tree',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            43 => 
            array (
                'sys_system_settings_id' => 69,
                'option_group' => 'payment_settings',
                'option_key' => 'brain_tree_env',
                'option_value' => 'sandbox',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            44 => 
            array (
                'sys_system_settings_id' => 70,
                'option_group' => 'payment_settings',
                'option_key' => 'brain_tree_merchant_id',
                'option_value' => 'fyjwt74tcs8qmfvb',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            45 => 
            array (
                'sys_system_settings_id' => 71,
                'option_group' => 'payment_settings',
                'option_key' => 'brain_tree_public_key',
                'option_value' => 'v7qwrrmzjmvchh3r',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            46 => 
            array (
                'sys_system_settings_id' => 72,
                'option_group' => 'payment_settings',
                'option_key' => 'brain_tree_private_key',
                'option_value' => '1150261b4483675ab74191d525ea5533',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            47 => 
            array (
                'sys_system_settings_id' => 73,
                'option_group' => 'payment_settings',
                'option_key' => 'stripe_key',
                'option_value' => 'pk_test_atkU8vZV2ey9RLLcCMEfSrXg00x2Pv7LmR',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            48 => 
            array (
                'sys_system_settings_id' => 74,
                'option_group' => 'payment_settings',
                'option_key' => 'stripe_secret',
                'option_value' => 'sk_test_Rplhic0MGNCgjgGvwDSvtvaE00vq1N5e6q',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            49 => 
            array (
                'sys_system_settings_id' => 75,
                'option_group' => 'about_us_settings',
                'option_key' => 'about_us_title',
                'option_value' => 'How We Work',
                'status' => 'Active',
                'created_at' => NULL,
                'created_by' => 1,
            ),
            50 => 
            array (
                'sys_system_settings_id' => 76,
                'option_group' => 'about_us_settings',
                'option_key' => 'about_us_description',
                'option_value' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.

Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like.

There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                51 => 
                array (
                    'sys_system_settings_id' => 77,
                    'option_group' => 'about_us_settings',
                    'option_key' => 'about_us_picture',
                    'option_value' => 'Backend/images/about_us/sZaZf6891SaTTr0k.jpg',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                52 => 
                array (
                    'sys_system_settings_id' => 78,
                    'option_group' => 'admin_settings',
                    'option_key' => 'app_title',
                    'option_value' => 'Spotlisting Admin',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                53 => 
                array (
                    'sys_system_settings_id' => 79,
                    'option_group' => 'admin_settings',
                    'option_key' => 'contact_email',
                    'option_value' => 'info@itech.international',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                54 => 
                array (
                    'sys_system_settings_id' => 80,
                    'option_group' => 'admin_settings',
                    'option_key' => 'contact_number',
                    'option_value' => '0213345663',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                55 => 
                array (
                    'sys_system_settings_id' => 81,
                    'option_group' => 'admin_settings',
                    'option_key' => 'description',
                    'option_value' => 'some text will be written here',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                56 => 
                array (
                    'sys_system_settings_id' => 82,
                    'option_group' => 'admin_settings',
                    'option_key' => 'copy_right_text',
                    'option_value' => '@copyright2020',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                57 => 
                array (
                    'sys_system_settings_id' => 83,
                    'option_group' => 'admin_settings',
                    'option_key' => 'about_us',
                    'option_value' => 'Some text will be written here',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                58 => 
                array (
                    'sys_system_settings_id' => 84,
                    'option_group' => 'admin_settings',
                    'option_key' => 'google_location',
                    'option_value' => 'ssdfsfds',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                59 => 
                array (
                    'sys_system_settings_id' => 85,
                    'option_group' => 'admin_settings',
                    'option_key' => 'favicon_logo',
                    'option_value' => 'Backend/images/application/HgcKxELjIhIRz2pT.png',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                60 => 
                array (
                    'sys_system_settings_id' => 86,
                    'option_group' => 'admin_settings',
                    'option_key' => 'app_logo_large',
                    'option_value' => 'Backend/images/application/ZdCBkg4wWzUcVBYD.png',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                61 => 
                array (
                    'sys_system_settings_id' => 87,
                    'option_group' => 'admin_settings',
                    'option_key' => 'app_logo_small',
                    'option_value' => 'Backend/images/application/wGxzi9bgnqgfdjQc.png',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                62 => 
                array (
                    'sys_system_settings_id' => 88,
                    'option_group' => 'admin_settings',
                    'option_key' => 'social_login_enable',
                    'option_value' => '1',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                63 => 
                array (
                    'sys_system_settings_id' => 89,
                    'option_group' => 'admin_settings',
                    'option_key' => 'email_verification_enable',
                    'option_value' => '0',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                64 => 
                array (
                    'sys_system_settings_id' => 90,
                    'option_group' => 'admin_settings',
                    'option_key' => 'currency',
                    'option_value' => 'BDT',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                65 => 
                array (
                    'sys_system_settings_id' => 91,
                    'option_group' => 'frontend_settings',
                    'option_key' => 'latitude',
                    'option_value' => '22.819009982916523',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
                66 => 
                array (
                    'sys_system_settings_id' => 92,
                    'option_group' => 'frontend_settings',
                    'option_key' => 'longitude',
                    'option_value' => '89.5512269327881',
                    'status' => 'Active',
                    'created_at' => NULL,
                    'created_by' => 1,
                ),
            ));

        
    }
}