@extends('FontEnd.master',['menu'=>'faq'])
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('FAQ')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- start faq-pg-section -->
    <section class="faq-pg-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-lg-10 offset-lg-1">
                    {{-- <div class="accordion" id="accordionExample">
                        @if(isset($faqs))
                            @php($i = 1)
                            @php($flag = 0)
                            @foreach($faqs as $faq)
                                <div class="card z-depth-0 bordered">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link @if($flag == 0) collapsed @endif" type="button" data-toggle="collapse" data-target="#collapse{{$faq->id}}" aria-expanded="false" aria-controls="collapseOne">
                                                {{$i++}}. {{$faq->title}}
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapse{{$faq->id}}" class="collapse  @if($flag == 0) show @endif" aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                                        <div class="card-body inside-card">
                                            {{$faq->description}}
                                        </div>
                                    </div>
                                </div>
                                @php($flag = 1)
                            @endforeach
                        @endif
                    </div> --}}

                    <div class="bs-example">
                        <h3>Frequently Asked Question</h3>
                        <div class="accordion" id="accordionExample">
                            @if (isset($faqs) && !@empty($faqs[0]))
                            @php($flag = 0)
                            @php($index = 1)
                                @foreach ($faqs as $faq)
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$faq->id}}">{{$index++}}. {{$faq->title}} <i class="fa fa-plus"></i></button>
                                        </h2>
                                    </div>
                                    <div id="collapse{{$faq->id}}" class="collapse @if($flag == 0) show @endif" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p>{{$faq->description}}</p>
                                        </div>
                                    </div>
                                </div>
                                @php($flag = 1)
                                @endforeach

                            @endif

                        </div>
                    </div>


                </div>
            </div>
        </div> <!-- end container -->
    </section>

    @include('FontEnd.pages.user_email_subscription')
@endsection
@section('script')
<script>

</script>
@endsection
