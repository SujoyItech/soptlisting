<?php

namespace App\Http\Controllers\Admin\PostModule;

use App\Http\Controllers\Controller;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    private $service;

    public function __construct() {
        $this->service  = new PostService();
    }

    public function checkBlogSlug(Request $request){
        $directory = DB::table('spotlist_blogs')->where('slug',$request->slug)->first();
        return !isset($directory) ? response()->json(['success'=>TRUE]) : response()->json(['success'=>FALSE]);
    }

    public function adminBlogList(Request $request){
        $postList = $this->service->getBlogData();
        if ($request->ajax()){

            return datatables($postList)
                ->editColumn('status', function ($item) {
                    if ($item->status == 'Active'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-warning">'.$item->status.'</span>';
                    }
                }) ->editColumn('blog_thumb_image', function ($item) {
                   return '<img src="'.asset($item->blog_thumb_image).'" class="img-rounded" width=50>';
                })
                ->rawColumns(['status','blog_thumb_image'])
                ->make(TRUE);
        }
        return view('Admin.Blog.post_list');
    }

    public function adminBlogAdd($id = NULL){
        $data = [];
        if ($id !== NULL){
            $data['blog'] = $this->service->getBlogData($id);
        }
        return view('Admin.Blog.blog_add',$data);
    }

    public function adminBlogSave(Request $request){
        $response = $this->service->adminBlogSave($request);
        return response()->json($response);
    }


}
