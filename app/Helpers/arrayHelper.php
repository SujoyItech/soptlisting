<?php
function number_to_words($number) {
    $hyphen = '-';
    $conjunction = ' and ';
    $separator = ', ';
    $negative = 'negative ';
    $decimal = ' point ';
    $dictionary = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'forty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
        return FALSE;
    }

    if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return FALSE;
    }

    if ($number < 0) {
        return $negative . number_to_words(abs($number));
    }

    $string = $fraction = NULL;

    if (strpos($number, '.') !== FALSE) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (TRUE) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int)($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int)($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= number_to_words($remainder);
            }
            break;
    }

    if (NULL !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string)$fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return ucwords($string);
}

function country($lang = null) {
    $output = [
        'AF' => __('Afghanistan'),
        'AL' => __('Albania'),
        'DZ' => __('Algeria'),
        'DS' => __('American Samoa'),
        'AD' => __('Andorra'),
        'AO' => __('Angola'),
        'AI' => __('Anguilla'),
        'AQ' => __('Antarctica'),
        'AG' => __('Antigua and Barbuda'),
        'AR' => __('Argentina'),
        'AM' => __('Armenia'),
        'AW' => __('Aruba'),
        'AU' => __('Australia'),
        'AT' => __('Austria'),
        'AZ' => __('Azerbaijan'),
        'BS' => __('Bahamas'),
        'BH' => __('Bahrain'),
        'BD' => __('Bangladesh'),
        'BB' => __('Barbados'),
        'BY' => __('Belarus'),
        'BE' => __('Belgium'),
        'BZ' => __('Belize'),
        'BJ' => __('Benin'),
        'BM' => __('Bermuda'),
        'BT' => __('Bhutan'),
        'BO' => __('Bolivia'),
        'BA' => __('Bosnia and Herzegovina'),
        'BW' => __('Botswana'),
        'BV' => __('Bouvet Island'),
        'BR' => __('Brazil'),
        'IO' => __('British Indian Ocean Territory'),
        'BN' => __('Brunei Darussalam'),
        'BG' => __('Bulgaria'),
        'BF' => __('Burkina Faso'),
        'BI' => __('Burundi'),
        'KH' => __('Cambodia'),
        'CM' => __('Cameroon'),
        'CA' => __('Canada'),
        'CV' => __('Cape Verde'),
        'KY' => __('Cayman Islands'),
        'CF' => __('Central African Republic'),
        'TD' => __('Chad'),
        'CL' => __('Chile'),
        'CN' => __('China'),
        'CX' => __('Christmas Island'),
        'CC' => __('Cocos (Keeling) Islands'),
        'CO' => __('Colombia'),
        'KM' => __('Comoros'),
        'CG' => __('Congo'),
        'CK' => __('Cook Islands'),
        'CR' => __('Costa Rica'),
        'HR' => __('Croatia (Hrvatska)'),
        'CU' => __('Cuba'),
        'CY' => __('Cyprus'),
        'CZ' => __('Czech Republic'),
        'DK' => __('Denmark'),
        'DJ' => __('Djibouti'),
        'DM' => __('Dominica'),
        'DO' => __('Dominican Republic'),
        'TP' => __('East Timor'),
        'EC' => __('Ecuador'),
        'EG' => __('Egypt'),
        'SV' => __('El Salvador'),
        'GQ' => __('Equatorial Guinea'),
        'ER' => __('Eritrea'),
        'EE' => __('Estonia'),
        'ET' => __('Ethiopia'),
        'FK' => __('Falkland Islands (Malvinas)'),
        'FO' => __('Faroe Islands'),
        'FJ' => __('Fiji'),
        'FI' => __('Finland'),
        'FR' => __('France'),
        'FX' => __('France, Metropolitan'),
        'GF' => __('French Guiana'),
        'PF' => __('French Polynesia'),
        'TF' => __('French Southern Territories'),
        'GA' => __('Gabon'),
        'GM' => __('Gambia'),
        'GE' => __('Georgia'),
        'DE' => __('Germany'),
        'GH' => __('Ghana'),
        'GI' => __('Gibraltar'),
        'GK' => __('Guernsey'),
        'GR' => __('Greece'),
        'GL' => __('Greenland'),
        'GD' => __('Grenada'),
        'GP' => __('Guadeloupe'),
        'GU' => __('Guam'),
        'GT' => __('Guatemala'),
        'GN' => __('Guinea'),
        'GW' => __('Guinea-Bissau'),
        'GY' => __('Guyana'),
        'HT' => __('Haiti'),
        'HM' => __('Heard and Mc Donald Islands'),
        'HN' => __('Honduras'),
        'HK' => __('Hong Kong'),
        'HU' => __('Hungary'),
        'IS' => __('Iceland'),
        'IN' => __('India'),
        'IM' => __('Isle of Man'),
        'ID' => __('Indonesia'),
        'IR' => __('Iran (Islamic Republic of)'),
        'IQ' => __('Iraq'),
        'IE' => __('Ireland'),
        'IL' => __('Israel'),
        'IT' => __('Italy'),
        'CI' => __('Ivory Coast'),
        'JE' => __('Jersey'),
        'JM' => __('Jamaica'),
        'JP' => __('Japan'),
        'JO' => __('Jordan'),
        'KZ' => __('Kazakhstan'),
        'KE' => __('Kenya'),
        'KI' => __('Kiribati'),
        'KP' => __('Korea, Democratic People\'s Republic of'),
        'KR' => __('Korea, Republic of'),
        'XK' => __('Kosovo'),
        'KW' => __('Kuwait'),
        'KG' => __('Kyrgyzstan'),
        'LA' => __('Lao People\'s Democratic Republic'),
        'LV' => __('Latvia'),
        'LB' => __('Lebanon'),
        'LS' => __('Lesotho'),
        'LR' => __('Liberia'),
        'LY' => __('Libyan Arab Jamahiriya'),
        'LI' => __('Liechtenstein'),
        'LT' => __('Lithuania'),
        'LU' => __('Luxembourg'),
        'MO' => __('Macau'),
        'MK' => __('Macedonia'),
        'MG' => __('Madagascar'),
        'MW' => __('Malawi'),
        'MY' => __('Malaysia'),
        'MV' => __('Maldives'),
        'ML' => __('Mali'),
        'MT' => __('Malta'),
        'MH' => __('Marshall Islands'),
        'MQ' => __('Martinique'),
        'MR' => __('Mauritania'),
        'MU' => __('Mauritius'),
        'TY' => __('Mayotte'),
        'MX' => __('Mexico'),
        'FM' => __('Micronesia, Federated States of'),
        'MD' => __('Moldova, Republic of'),
        'MC' => __('Monaco'),
        'MN' => __('Mongolia'),
        'ME' => __('Montenegro'),
        'MS' => __('Montserrat'),
        'MA' => __('Morocco'),
        'MZ' => __('Mozambique'),
        'MM' => __('Myanmar'),
        'NA' => __('Namibia'),
        'NR' => __('Nauru'),
        'NP' => __('Nepal'),
        'NL' => __('Netherlands'),
        'AN' => __('Netherlands Antilles'),
        'NC' => __('New Caledonia'),
        'NZ' => __('New Zealand'),
        'NI' => __('Nicaragua'),
        'NE' => __('Niger'),
        'NG' => __('Nigeria'),
        'NU' => __('Niue'),
        'NF' => __('Norfolk Island'),
        'MP' => __('Northern Mariana Islands'),
        'NO' => __('Norway'),
        'OM' => __('Oman'),
        'PK' => __('Pakistan'),
        'PW' => __('Palau'),
        'PS' => __('Palestine'),
        'PA' => __('Panama'),
        'PG' => __('Papua New Guinea'),
        'PY' => __('Paraguay'),
        'PE' => __('Peru'),
        'PH' => __('Philippines'),
        'PN' => __('Pitcairn'),
        'PL' => __('Poland'),
        'PT' => __('Portugal'),
        'PR' => __('Puerto Rico'),
        'QA' => __('Qatar'),
        'RE' => __('Reunion'),
        'RO' => __('Romania'),
        'RU' => __('Russian Federation'),
        'RW' => __('Rwanda'),
        'KN' => __('Saint Kitts and Nevis'),
        'LC' => __('Saint Lucia'),
        'VC' => __('Saint Vincent and the Grenadines'),
        'WS' => __('Samoa'),
        'SM' => __('San Marino'),
        'ST' => __('Sao Tome and Principe'),
        'SA' => __('Saudi Arabia'),
        'SN' => __('Senegal'),
        'RS' => __('Serbia'),
        'SC' => __('Seychelles'),
        'SL' => __('Sierra Leone'),
        'SG' => __('Singapore'),
        'SK' => __('Slovakia'),
        'SI' => __('Slovenia'),
        'SB' => __('Solomon Islands'),
        'SO' => __('Somalia'),
        'ZA' => __('South Africa'),
        'GS' => __('South Georgia South Sandwich Islands'),
        'ES' => __('Spain'),
        'LK' => __('Sri Lanka'),
        'SH' => __('St. Helena'),
        'PM' => __('St. Pierre and Miquelon'),
        'SD' => __('Sudan'),
        'SR' => __('Suriname'),
        'SJ' => __('Svalbard and Jan Mayen Islands'),
        'SZ' => __('Swaziland'),
        'SE' => __('Sweden'),
        'CH' => __('Switzerland'),
        'SY' => __('Syrian Arab Republic'),
        'TW' => __('Taiwan'),
        'TJ' => __('Tajikistan'),
        'TZ' => __('Tanzania, United Republic of'),
        'TH' => __('Thailand'),
        'TG' => __('Togo'),
        'TK' => __('Tokelau'),
        'TO' => __('Tonga'),
        'TT' => __('Trinidad and Tobago'),
        'TN' => __('Tunisia'),
        'TR' => __('Turkey'),
        'TM' => __('Turkmenistan'),
        'TC' => __('Turks and Caicos Islands'),
        'TV' => __('Tuvalu'),
        'UG' => __('Uganda'),
        'UA' => __('Ukraine'),
        'AE' => __('United Arab Emirates'),
        'UK' => __('United Kingdom'),
        'US' => __('United States'),
        'UM' => __('United States minor outlying islands'),
        'UY' => __('Uruguay'),
        'UZ' => __('Uzbekistan'),
        'VU' => __('Vanuatu'),
        'VA' => __('Vatican City State'),
        'VE' => __('Venezuela'),
        'VN' => __('Vietnam'),
        'VG' => __('Virgin Islands (British)'),
        'VI' => __('Virgin Islands (U.S.)'),
        'WF' => __('Wallis and Futuna Islands'),
        'EH' => __('Western Sahara'),
        'YE' => __('Yemen'),
        'ZR' => __('Zaire'),
        'ZM' => __('Zambia'),
        'ZW' => __('Zimbabwe')
    ];

    if ($lang == null) {
        return $output;
    } else if (is_array($lang)) {
        return array_intersect($output, $lang);
    } else {
        return isset($output[$lang]) ? $output[$lang] : $lang;
    }
}

function currency($currency = null) {
    $output = array (
        'ALL' => 'Albania Lek',
        'AFN' => 'Afghanistan Afghani',
        'ARS' => 'Argentina Peso',
        'AWG' => 'Aruba Guilder',
        'AUD' => 'Australia Dollar',
        'AZN' => 'Azerbaijan New Manat',
        'BSD' => 'Bahamas Dollar',
        'BBD' => 'Barbados Dollar',
        'BDT' => 'Bangladeshi taka',
        'BYR' => 'Belarus Ruble',
        'BZD' => 'Belize Dollar',
        'BMD' => 'Bermuda Dollar',
        'BOB' => 'Bolivia Boliviano',
        'BAM' => 'Bosnia and Herzegovina Convertible Marka',
        'BWP' => 'Botswana Pula',
        'BGN' => 'Bulgaria Lev',
        'BRL' => 'Brazil Real',
        'BND' => 'Brunei Darussalam Dollar',
        'KHR' => 'Cambodia Riel',
        'CAD' => 'Canada Dollar',
        'KYD' => 'Cayman Islands Dollar',
        'CLP' => 'Chile Peso',
        'CNY' => 'China Yuan Renminbi',
        'COP' => 'Colombia Peso',
        'CRC' => 'Costa Rica Colon',
        'HRK' => 'Croatia Kuna',
        'CUP' => 'Cuba Peso',
        'CZK' => 'Czech Republic Koruna',
        'DKK' => 'Denmark Krone',
        'DOP' => 'Dominican Republic Peso',
        'XCD' => 'East Caribbean Dollar',
        'EGP' => 'Egypt Pound',
        'SVC' => 'El Salvador Colon',
        'EEK' => 'Estonia Kroon',
        'EUR' => 'Euro Member Countries',
        'FKP' => 'Falkland Islands (Malvinas) Pound',
        'FJD' => 'Fiji Dollar',
        'GHC' => 'Ghana Cedis',
        'GIP' => 'Gibraltar Pound',
        'GTQ' => 'Guatemala Quetzal',
        'GGP' => 'Guernsey Pound',
        'GYD' => 'Guyana Dollar',
        'HNL' => 'Honduras Lempira',
        'HKD' => 'Hong Kong Dollar',
        'HUF' => 'Hungary Forint',
        'ISK' => 'Iceland Krona',
        'INR' => 'India Rupee',
        'IDR' => 'Indonesia Rupiah',
        'IRR' => 'Iran Rial',
        'IMP' => 'Isle of Man Pound',
        'ILS' => 'Israel Shekel',
        'JMD' => 'Jamaica Dollar',
        'JPY' => 'Japan Yen',
        'JEP' => 'Jersey Pound',
        'KZT' => 'Kazakhstan Tenge',
        'KPW' => 'Korea (North) Won',
        'KRW' => 'Korea (South) Won',
        'KGS' => 'Kyrgyzstan Som',
        'LAK' => 'Laos Kip',
        'LVL' => 'Latvia Lat',
        'LBP' => 'Lebanon Pound',
        'LRD' => 'Liberia Dollar',
        'LTL' => 'Lithuania Litas',
        'MKD' => 'Macedonia Denar',
        'MYR' => 'Malaysia Ringgit',
        'MUR' => 'Mauritius Rupee',
        'MXN' => 'Mexico Peso',
        'MNT' => 'Mongolia Tughrik',
        'MZN' => 'Mozambique Metical',
        'NAD' => 'Namibia Dollar',
        'NPR' => 'Nepal Rupee',
        'ANG' => 'Netherlands Antilles Guilder',
        'NZD' => 'New Zealand Dollar',
        'NIO' => 'Nicaragua Cordoba',
        'NGN' => 'Nigeria Naira',
        'NOK' => 'Norway Krone',
        'OMR' => 'Oman Rial',
        'PKR' => 'Pakistan Rupee',
        'PAB' => 'Panama Balboa',
        'PYG' => 'Paraguay Guarani',
        'PEN' => 'Peru Nuevo Sol',
        'PHP' => 'Philippines Peso',
        'PLN' => 'Poland Zloty',
        'QAR' => 'Qatar Riyal',
        'RON' => 'Romania New Leu',
        'RUB' => 'Russia Ruble',
        'SHP' => 'Saint Helena Pound',
        'SAR' => 'Saudi Arabia Riyal',
        'RSD' => 'Serbia Dinar',
        'SCR' => 'Seychelles Rupee',
        'SGD' => 'Singapore Dollar',
        'SBD' => 'Solomon Islands Dollar',
        'SOS' => 'Somalia Shilling',
        'ZAR' => 'South Africa Rand',
        'LKR' => 'Sri Lanka Rupee',
        'SEK' => 'Sweden Krona',
        'CHF' => 'Switzerland Franc',
        'SRD' => 'Suriname Dollar',
        'SYP' => 'Syria Pound',
        'TWD' => 'Taiwan New Dollar',
        'THB' => 'Thailand Baht',
        'TTD' => 'Trinidad and Tobago Dollar',
        'TRY' => 'Turkey Lira',
        'TRL' => 'Turkey Lira',
        'TVD' => 'Tuvalu Dollar',
        'UAH' => 'Ukraine Hryvna',
        'GBP' => 'United Kingdom Pound',
        'USD' => 'United States Dollar',
        'UYU' => 'Uruguay Peso',
        'UZS' => 'Uzbekistan Som',
        'VEF' => 'Venezuela Bolivar',
        'VND' => 'Viet Nam Dong',
        'YER' => 'Yemen Rial',
        'ZWD' => 'Zimbabwe Dollar'
    );

    if ($currency == null) {
        return $output;
    } else if (is_array($currency)) {
        return array_intersect($output, $currency);
    } else {
        return isset($output[$currency]) ? $output[$currency] : $currency;
    }
}

function is_checked($data,$array=[]){
    return in_array($data,$array) ? 'checked' : '';
}
