<form action="{{url('save-team-data')}}" method="post" id="spotlist_team" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>{{__('Name')}}</label>
                        <input type="text" name="name" id="name" value="{{$teamData->name ?? ''}}" class="form-control" placeholder="Enter Name">
                        <div class="help-block with-errors has-feedback"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>{{__('Designation')}}</label>
                        <input type="text" name="designation" id="designation" value="{{$teamData->designation ?? ''}}" class="form-control" placeholder="Enter designation">
                        <div class="help-block with-errors has-feedback"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>{{__('Facebook')}}</label>
                        <input type="text" name="facebook_url" id="facebook_url" value="{{$teamData->facebook_url ?? ''}}" class="form-control" placeholder="Facebook">
                        <div class="help-block with-errors has-feedback"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>{{__('Twitter')}}</label>
                        <input type="text" name="twitter_url" id="twitter_url" value="{{$teamData->twitter_url ?? ''}}" class="form-control" placeholder="Twitter">
                        <div class="help-block with-errors has-feedback"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>{{__('Instagram')}}</label>
                        <input type="text" name="instagram_url" id="instagram_url" value="{{$teamData->instagram_url ?? ''}}" class="form-control" placeholder="Instagram">
                        <div class="help-block with-errors has-feedback"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>{{__('LinkedIn')}}</label>
                        <input type="text" name="linkedin_url" id="linkedin_url" value="{{$teamData->linkedin_url ?? ''}}" class="form-control" placeholder="Linkedin">
                        <div class="help-block with-errors has-feedback"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>{{__('Whatsapp')}}</label>
                        <input type="text" name="whatsapp_url" id="whatsapp_url" value="{{$teamData->whatsapp_url ?? ''}}" class="form-control" placeholder="Whatsapp">
                        <div class="help-block with-errors has-feedback"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group ">
                        <label>{{__('Status')}}</label>
                        @php($status = [ 'Active' =>__('Active'),'Inactive' =>__('Inactive')])
                        {!! Form::select('status',  $status,$teamData->status ?? '', ['class' => 'form-control','required']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-form-label" for="picture">{{__('Picture')}}</label>
                <input type="file" name="picture" id="picture" class="dropify" data-plugins="dropify"
                       data-default-file="{{isset($teamData->picture) ? asset($teamData->picture) : ''}}"
                       data-allowed-file-extensions="png jpg jpeg jfif"
                       data-max-file-size="2M" />
                <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
            </div>
        </div>

        <div class="col-md-12 mt-2">
            <div class="form-group">
                <input type="hidden" name="id" value="{{$teamData->id ?? ''}}">
                <button class="submit_team btn btn-primary">{{__('Submit')}}</button>
            </div>
        </div>
    </div>
</form>
<script>
    $('.dropify').dropify();

    $('.submit_team').on('click', function (e) {
        Ladda.bind(this);
        var load = $(this).ladda();
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);
                makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                    load.ladda('stop');
                    var redirect_url = "{{url('teams')}}";
                    if (response.success == true){
                        swalRedirect(redirect_url,response.message,'success')

                    }else{
                        swalRedirect(redirect_url, response.message, 'error');
                    }
                });
            }
        });
    });
</script>
