@if(isset($wishlist) && $wishlist->is_wished == TRUE)
    <a href="{{route('user.directory.wishlist',['id'=>$directory->spotlist_directory_id])}}" class="btn btn-success btn-block mt-2"><i class="fa fa-check text-light"></i> {{__('Added to wishlist')}}</a>
@else
    <a href="{{route('user.directory.wishlist',['id'=>$directory->spotlist_directory_id])}}" class="btn btn-secondary btn-block mt-2"><i class="fa fa-heart"></i> {{__('Add to wishlist')}}</a>
@endif
