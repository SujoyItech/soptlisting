<?php


namespace App\Http\Controllers\Backend;


use App\Traits\DataSaveTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingService {

    use DataSaveTrait;

    public function deleteRows(Request $request){
        try {
            DB::table($request->table_name)->whereIn($request->primary_key,$request->deleted_ids)->delete();
            $response = $this->responseData(TRUE,'Data deleted successfully.');
        }catch (\Exception $exception){
            $response = $this->responseData(TRUE,'Data delete failed!');
        }
        return $response;
    }
    public function deleteListById(Request $request){
        try {
            DB::table($request->table_name)->where($request->primary_key,$request->id)->delete();
            $response = $this->responseData(TRUE,'Data deleted successfully.');
        }catch (\Exception $exception){
            $response = $this->responseData(TRUE,'Data delete failed!');
        }
        return $response;
    }

    private function insert_or_update($option_group,$option_key,$option_value){
        DB::table('sys_system_settings')->updateOrInsert(['option_group'=>$option_group,'option_key'=>$option_key],
            ['option_group'=>$option_group,'option_key'=>$option_key,'option_value'=>$option_value]);
    }

    public function howItWorkDataSave(Request $request){
        $response = $this->inserOrUpdateWorkData($request);
        return $response;
    }

    private function inserOrUpdateWorkData($request){
        $insert_data = [];
        $update_data = [];
        try {
            foreach ($request->title as $key => $value){
                if (isset($request->id[$key])){
                    $update_data[$key]['id'] = $request->id[$key];
                    $update_data[$key]['title'] = $value;
                    $update_data[$key]['description'] = $request->description[$key];
                    if (isset($request->icon[$key]) && !empty($request->icon[$key])){
                        $update_data[$key]['icon'] = fileUpload($request->icon[$key],'Backend/images/how_works/');
                    }
                }else{
                    $insert_data[$key]['title'] = $value;
                    $insert_data[$key]['description'] = $request->description[$key];
                    if (isset($request->icon[$key]) && !empty($request->icon[$key])){
                        $insert_data[$key]['icon'] = fileUpload($request->icon[$key],'Backend/images/how_works/');
                    }
                }
            }
            $data = DB::table('spotlist_how_it_works')->pluck('id')->toArray();

            if (isset($request->id) && !empty($request->id)){
                $delete_data = array_diff($data,$request->id);
                DB::table('spotlist_how_it_works')->whereIn('id',$delete_data)->delete();
            }

            if (!empty($update_data)){
                foreach ($update_data as $key=>$value){
                    DB::table('spotlist_how_it_works')->where('id',$value['id'])->update($value);
                }
            }

            if (!empty($insert_data)){
                DB::table('spotlist_how_it_works')->insert($insert_data);
            }
            $response = $this->responseData(TRUE,'Data saved successfully.');
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,'Data saved failed!');
        }

        return $response;
    }

    public function aboutUsSettingSave(Request $request){
        try {
            if (isset($request->about_us_title) && !empty($request->about_us_title)){
                $this->insert_or_update('about_us_settings','about_us_title',$request->about_us_title);
            }if (isset($request->about_us_description) && !empty($request->about_us_description)){
                $this->insert_or_update('about_us_settings','about_us_description',$request->about_us_description);
            }if (isset($request->about_us_picture) && !empty($request->about_us_picture)){
                $check_app_logo = DB::table('sys_system_settings')->where(['option_key'=>'about_us_picture','option_group'=>'about_us_settings'])->first();
                $option_value = fileUpload($request->about_us_picture,'Backend/images/about_us/',NULL,$check_app_logo->option_value ?? '');
                $this->insert_or_update('about_us_settings','about_us_picture',$option_value);
            }
            $response = $this->responseData(TRUE,__('Settings saved successfully.'));
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$exception->getMessage());
        }
        return $response;
    }

    public function teamList($id = NULL){
        $teamList = $id != NULL ? DB::table('spotlisting_teams')->where('id',$id)->first() : DB::table('spotlisting_teams')->get();
        return $teamList;
    }

    public function saveTeamData(Request $request){
        $post_data = $request->except('id');
        if (!empty($request->picture)){
            if (!empty($request->id)){
                $team = DB::table('spotlisting_teams')->where('id',$request->id)->first();
            }
            $post_data['picture'] = fileUpload($request->picture,'Backend/images/teams/',NULL,$team->picture ?? '');
        }
        return $this->insertOrUpdateTable('spotlisting_teams','id',$request->id ?? '',$post_data);
    }
}
