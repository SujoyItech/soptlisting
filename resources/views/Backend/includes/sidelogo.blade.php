<div class="user-box text-center">
    <img src="{{getImageUrl(Auth::user()->user_image)}}" alt="user-img" title="Mat Helme"
         class="rounded-circle avatar-md">
    <div class="dropdown">
        <a href="javascript: void(0);" class="text-dark font-weight-normal dropdown-toggle h5 mt-2 mb-1 d-block"
           data-toggle="dropdown">{{Auth::user()->name}}</a>
        <div class="dropdown-menu user-pro-dropdown">
            <a href="{{url('user/home')}}" class="dropdown-item notify-item">
                <i class="fa fa-globe"></i> <span>{{__('Visit website')}}</span>
            </a>
            <a href="{{url('profile-settings')}}" class="dropdown-item notify-item">
                <i class="fe-user"></i> <span>{{__('My Account')}}</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="{{url('logout')}}" class="dropdown-item text-danger">
                <i class="fe-log-out"></i> <span>{{__('Logout')}}</span>
            </a>
        </div>
    </div>
</div>
