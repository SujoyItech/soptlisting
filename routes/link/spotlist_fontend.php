<?php

/*=============================================Dashboard====================================================*/

Route::get('user/home', 'Home\HomeController@index')->name('user.home');

//Route::get('user/listing', 'Listing\ListingController@listing')->name('user.listing');
Route::get('user/listing/{slug?}', 'Listing\ListingController@listing')->name('user.listing');
Route::get('all-listing', 'Listing\ListingController@showAllListing')->name('show-all-listing');
Route::get('all-categories', 'Listing\ListingController@showAllCategories')->name('all-categories');
Route::get('all-cities', 'Listing\ListingController@showAllCities')->name('all-cities');
Route::get('most-popular', 'Listing\ListingController@showMostPopular')->name('most-popular');
Route::get('category/{category_name}', 'Listing\ListingController@showListByCategory')->name('show-list-by-category');
Route::get('list-by-place/{slug?}', 'Listing\ListingController@showListByPlace')->name('show-list-by-place');

Route::post('view-service-details', 'Listing\ListingController@viewServiceDetails')->name('view-service-details');

Route::group(['middleware'=>['auth','auth.permission']],function (){
    /*************************************Boooking*************************************/
    Route::post('check-booking-start-time', 'Booking\BookingController@checkBookingStartTime')->name('check-booking-start-time');
    Route::post('check-booking-end-time', 'Booking\BookingController@checkBookingEndTime')->name('check-booking-end-time');

    Route::post('user/booking-request-send', 'Booking\BookingController@bookingRequestSend')->name('user.booking.request.send');
    Route::get('user/bookings/{notify_id?}/{booking_id?}', 'Booking\BookingController@bookings')->name('user.bookings');
    Route::post('view-user-booking-details', 'Booking\BookingController@userBookingsDetails')->name('view-user-booking-details');
    Route::post('user/booking/request/cancel', 'Booking\BookingController@userBookingsRequestCancel')->name('user-booking-request-cancel');
    Route::post('user/booking/request/delete', 'Booking\BookingController@userBookingsRequestDelete')->name('user-booking-request-delete');
    /*************************************Boooking*************************************/

    Route::post('user/directory-type-like', 'Listing\ListingController@directoryTypeLike')->name('user.directory.type.like');
    Route::post('user/follow', 'UserController@followUser')->name('user.follow');
    Route::post('user/message/send', 'UserController@userMessageSend')->name('user.message.send');
    Route::get('user/profile/{id?}', 'PageController@profile')->name('user.profile');

    Route::get('user-profile-settings', 'UserController@profileSettings')->name('user.profile.settings');
    Route::post('user-profile-update', 'UserController@userProfileUpdate')->name('user-profile-update');
    Route::post('user-password-update', 'UserController@userPasswordUpdate')->name('user-password-update');

    Route::get('user/wishing', 'PageController@wishing')->name('user.wishing');
    Route::get('user/directory/wishlist/{id?}', 'Listing\ListingController@addListToWishlist')->name('user.directory.wishlist');

    Route::post('check-user-rating', 'Listing\ListingController@checkUserRating')->name('check-user-rating');
    Route::post('spotlist-directory-rating', 'Listing\ListingController@directoryRating')->name('spotlist-directory-rating');

    Route::get('load-user-notification-body', 'Notification\NotificationController@loadUserNotification')->name('load-user-notification-body');
    Route::post('user-notification-make-seen', 'Notification\NotificationController@userNotificationMakeSeen')->name('user-notification-make-seen');
    Route::get('clear-all-user-notification', 'Notification\NotificationController@clearAllUserNotification')->name('clear-all-user-notification');
    Route::get('user-all-notification/{notify_id?}', 'Notification\NotificationController@allUserNotification')->name('user-all-notification');


});
Route::post('user/email/subscription', 'UserController@userEmailSubscription')->name('user.email.subscription');

Route::get('directory-search', 'Listing\ListingController@searchDirectory')->name('directory-search');

Route::get('home-page-search', 'Listing\ListingController@searchHomePageDirectory')->name('home-page-search');

Route::get('user/about', 'PageController@about')->name('user.about');
Route::get('user/contact', 'PageController@contact')->name('user.contact');
Route::get('user/faq', 'PageController@faq')->name('user.faq');
Route::get('user/blog/{slug?}', 'PageController@blog')->name('user.blog');



