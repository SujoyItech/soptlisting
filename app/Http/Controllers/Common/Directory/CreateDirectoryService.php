<?php


namespace App\Http\Controllers\Common\Directory;

use App\Http\Service\NotificationService;
use App\Models\User;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateDirectoryService {

    use DataSaveTrait;


    public function directoryBasicSave(Request $request){
        $post_data = $this->postBasicData($request);
        $success_message = __('Directory basic saved successfully.');
        $warning_message = __('Directory basic save failed.');
        $response = $this->insertOrUpdateTable('spotlist_directory','spotlist_directory_id',$request->spotlist_directory_id ?? '',$post_data,$success_message,$warning_message);
        $category_data = $this->postBasicCategoryData($request,$response['data']['spotlist_directory_id']);
        if (isset($category_data) && !empty($category_data)){
            $response_category = $this->insertOrDeleteTable('spotlist_directory_vs_category','spotlist_directory_id',$response['data']['spotlist_directory_id'],$category_data,$success_message,$warning_message);
        }
        $this->registrationStepUpdate($response['data']['spotlist_directory_id'],'basic');
        if (isset($request->spotlist_directory_id)){
            $response['type'] = 'update';
        }else{
            $response['type'] = 'insert';
            if (Auth::user()->default_module_id == LISTING_MANAGER){
                $data['notify_title'] = __(' New directory create request');
                $directory = DB::table('spotlist_directory')->where('spotlist_directory_id',$response['data']['spotlist_directory_id'])->first();
                $data['notify_message'] = Auth::user()->username.__(' wants to create a new directory ').$directory->spotlist_directory_name;
                $this->directoryNotificationSend($directory,$data);
            }
        }
        return $response;
    }

    private function updateListingCapability(){
         DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->increment('listing_item_used');
    }

    private function postBasicData(Request $request){

        if (isset($request->spotlist_directory_id)){
            $post_data = $request->except('spotlist_category_id','spotlist_directory_id');
        }else{
            $post_data = $request->except('spotlist_category_id');
            $post_data['sys_users_id'] = Auth::user()->id;
        }
        $post_data['created_at'] = Carbon::now();
        $post_data['updated_at'] = Carbon::now();

        return $post_data;
    }

    private function postBasicCategoryData(Request $request,$directory_id){
        $post_data = [];
        if (isset($request->spotlist_category_id) && !empty($request->spotlist_category_id)){
            foreach ($request->spotlist_category_id as $key => $value){
                $post_data[$key]['spotlist_directory_id'] = $directory_id;
                $post_data[$key]['spotlist_category_id'] = $value;
            }
        }
        return $post_data;
    }

    public function directoryLocationSave(Request $request){
        $post_data = $request->all();
        $success_message = __('Location saved successfully.');
        $warning_message = __('Location save failed.');
        $response = $this->insertOrUpdateTable('spotlist_directory','spotlist_directory_id',$post_data['spotlist_directory_id'],$post_data,$success_message,$warning_message);
        $this->registrationStepUpdate($response['data']['spotlist_directory_id'],'location');
        return $response;
    }


    public function directoryFeaturesSave(Request $request){
        $post_data = $this->postFeaturesData($request);
        $success_message = __('Features saved successfully.');
        $warning_message = __('Features save failed.');
        $response = $this->insertOrDeleteTable('spotlist_directory_vs_features','spotlist_directory_id',$request->spotlist_directory_id,$post_data,$success_message,$warning_message);
        $this->registrationStepUpdate($request->spotlist_directory_id,'features');
        return $response;
    }

    private function postFeaturesData(Request $request){
        $post_data = [];
        if (isset($request->spotlist_features_id) && !empty($request->spotlist_features_id)){
            foreach ($request->spotlist_features_id as $key => $value){
                $post_data[$key] ['spotlist_directory_id'] = $request->spotlist_directory_id;
                $post_data[$key] ['spotlist_features_id'] = $value;
            }
        }
        return $post_data;
    }

    public function directoryGallerySave(Request $request){
        $post_data = $this->postGalleryData($request);
        $success_message = __('Gallery saved successfully.');
        $warning_message = __('Gallery save failed.');
        $response = $this->insertOrUpdateTable('spotlist_directory','spotlist_directory_id',
            $post_data['directory']['spotlist_directory_id'],$post_data['directory'],$success_message,$warning_message);

        if (isset($post_data['gallery_images']) && !empty($post_data['gallery_images'])){
            $total_image = count($post_data['gallery_images']);
            if (Auth::user()->default_module_id == LISTING_MANAGER){
                if ($this->checkAvailableMedia($total_image) >= 0){
                    DB::table('spotlist_directory_gallary')->insert($post_data['gallery_images']);
                    $this->updateAvailableMediaData($total_image);
                }else{
                    $response = $this->responseData(FALSE,__('Your media limit exceed.Purchase new package!'));
                }
            }else{
                DB::table('spotlist_directory_gallary')->insert($post_data['gallery_images']);
            }
        }
        $this->registrationStepUpdate($request->spotlist_directory_id,'gallery');
        return $response;
    }

    private function checkAvailableMedia($gallery_image){
        $media = DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->first();
        $available_media = $media->num_of_photos - ($media->photos_used + $gallery_image);
        return $available_media;
    }

    private function updateAvailableMediaData($gallery_image){
        $media = DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->first();
        $total_used_image = $media->photos_used + $gallery_image;
        $total_used_image = $total_used_image < 0 ? 0 : $total_used_image;
        DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->update(['photos_used'=>$total_used_image]);
    }

    private function postGalleryData(Request $request){
        $post_data = [];
        $post_data['directory'] = [
            'video_provider' => $request->video_provider,
            'video_url' => $request->video_url,
            'video_ability' => $request->video_ability,
            'spotlist_directory_id' => $request->spotlist_directory_id,
        ];

        $directory = DB::table('spotlist_directory')->where('spotlist_directory_id',$request->spotlist_directory_id)->first();

        if (isset($request->thumb_image) && file_exists($request->thumb_image)){
            $post_data['directory']['thumb_image'] = fileUpload($request->thumb_image,'Backend/images/directory/',null,$directory->thumb_image ?? '',300,300);
        }

        if (isset($request->cover_image) && file_exists($request->cover_image)){
            $post_data['directory']['cover_image'] = fileUpload($request->cover_image,'Backend/images/directory/',null,$directory->cover_image ?? '',600,400);
        }

        if (isset($request->gallery_images) && !empty($request->gallery_images)){
            foreach ($request->gallery_images as $key=>$value){
                $post_data['gallery_images'][$key]['spotlist_directory_id'] = $request->spotlist_directory_id;
                $post_data['gallery_images'][$key]['image'] = fileUpload($value,'Backend/images/directory/',NULL,null,600,400);
            }
        }
        return $post_data;
    }

    public function directorySeoSave(Request $request){
        $post_data = $request->all();
        $success_message = __('Seo data saved successfully.');
        $warning_message = __('Seo data save failed.');
        $response = $this->insertOrUpdateTable('spotlist_directory','spotlist_directory_id',$request->spotlist_directory_id,$post_data,$success_message,$warning_message);
        $this->registrationStepUpdate($request->spotlist_directory_id,'seo');
        return $response;
    }

    public function directoryScheduleSave(Request $request){
        $post_data = $this->postDirectoryScheduleData($request);
        $success_message = __('Schedule data saved successfully.');
        $warning_message = __('Schedule data save failed.');
        $response = $this->insertOrUpdateTable('spotlist_directory_schedule','spotlist_directory_id',$request->spotlist_directory_id,$post_data,$success_message,$warning_message);
        $this->registrationStepUpdate($request->spotlist_directory_id,'schedule');
        return $response;
    }

    private function postDirectoryScheduleData(Request $request){

        $post_data = [
            'spotlist_directory_id' => $request->spotlist_directory_id,
            'monday' => $request->monday_from.'-'.$request->monday_to,
            'tuesday' => $request->tuesday_from.'-'.$request->tuesday_to,
            'wednesday' => $request->wednesday_from.'-'.$request->wednesday_to,
            'thursday' => $request->thursday_from.'-'.$request->thursday_to,
            'friday' => $request->friday_from.'-'.$request->friday_to,
            'saturday' => $request->saturday_from.'-'.$request->saturday_to,
            'sunday' => $request->sunday_from.'-'.$request->sunday_to
        ];
        return $post_data ;
    }

    public function directoryContactSave(Request $request){
        $post_data = $request->all();
        $success_message = __('Contact data saved successfully.');
        $warning_message = __('Contact data save failed.');
        $response = $this->insertOrUpdateTable('spotlist_directory','spotlist_directory_id',$request->spotlist_directory_id ?? '',$post_data,$success_message,$warning_message);
        $this->registrationStepUpdate($request->spotlist_directory_id,'contact');
        return $response;
    }

    public function directoryTypeSave(Request $request){
        $success_message = __('Type data saved successfully.');
        $warning_message = __('Type data save failed.');
        $category= DB::table('spotlist_category')->where('spotlist_category_id',$request->parent_category_id)->first();

        DB::table('spotlist_directory')->where('spotlist_directory_id',$request->spotlist_directory_id)
                                       ->update(['spotlist_parent_category_id'=>$request->parent_category_id,'type'=>$category->spotlist_category_name]);

        $this->registrationStepUpdate($request->spotlist_directory_id,'type');
        $response = [];

        if ($request->parent_category_id == CATEGORY_HOTEL){
            $post_data = $this->postHotelData($request);
            if (isset($post_data['update_data']) && !empty($post_data['update_data'][0])){
                $response = $this->updateMultipleArray('spotlist_directory_type_hotel_room','spotlist_directory_type_hotel_room_id', $post_data['update_data'],$success_message,$warning_message);
            }
            if (isset($post_data['insert_data']) && !empty($post_data['insert_data'][0])){
                $response = $this->insertMultipleArray('spotlist_directory_type_hotel_room', $post_data['insert_data'],$success_message,$warning_message);
            }
        }elseif ($request->parent_category_id == CATEGORY_RESTAURANT){
            $post_data = $this->postRestaurantData($request);
            if (isset($post_data['update_data']) && !empty($post_data['update_data'][0])){
                $response = $this->updateMultipleArray('spotlist_directory_type_restaurant','spotlist_directory_type_restaurant_id', $post_data['update_data'],$success_message,$warning_message);
            }
            if (isset($post_data['insert_data']) && !empty($post_data['insert_data'][0])){
                $response = $this->insertMultipleArray('spotlist_directory_type_restaurant', $post_data['insert_data'],$success_message,$warning_message);
            }
        }elseif ($request->parent_category_id == CATEGORY_BEAUTY){
            $post_data = $this->postBeautyData($request);
            if (isset($post_data['update_data']) && !empty($post_data['update_data'][0])){
                $response = $this->updateMultipleArray('spotlist_directory_type_beauty','spotlist_directory_type_beauty_id', $post_data['update_data'],$success_message,$warning_message);
            }
            if (isset($post_data['insert_data']) && !empty($post_data['insert_data'][0])){
                $response = $this->insertMultipleArray('spotlist_directory_type_beauty', $post_data['insert_data'],$success_message,$warning_message);
            }
        }elseif ($request->parent_category_id == CATEGORY_SHOPPING){
            $post_data = $this->postShopData($request);
            if (isset($post_data['update_data']) && !empty($post_data['update_data'][0])){
                $response = $this->updateMultipleArray('spotlist_directory_type_shop','spotlist_directory_type_shop_id', $post_data['update_data'],$success_message,$warning_message);
            }
            if (isset($post_data['insert_data']) && !empty($post_data['insert_data'][0])) {
                $response = $this->insertMultipleArray('spotlist_directory_type_shop', $post_data['insert_data'], $success_message, $warning_message);
            }
        }elseif ($request->parent_category_id == CATEGORY_FITNESS){
            $post_data = $this->postFitnessData($request);
            if (isset($post_data['update_data']) && !empty($post_data['update_data'][0])){
                $response = $this->updateMultipleArray('spotlist_directory_type_fitness','spotlist_directory_type_fitness_id', $post_data['update_data'],$success_message,$warning_message);
            }
            if (isset($post_data['insert_data']) && !empty($post_data['insert_data'][0])) {
                $response = $this->insertMultipleArray('spotlist_directory_type_fitness', $post_data['insert_data'], $success_message, $warning_message);
            }
        }else{
            $data['data']['spotlist_directory_id'] = $request->spotlist_directory_id;
            $response = ['success'=>TRUE,$data,$success_message];
        }
        return $response;
    }

    private function postHotelData(Request $request){

        $post_data = [];
        $insert_data = [];
        $update_data = [];
        if (isset($request->room_name )&& !empty($request->room_name[0])){
            $i=0;
            foreach ($request->room_name as $key => $value){
                if (!empty($value)){
                    if (isset($request->spotlist_directory_type_hotel_room_id[$key])){
                        $update_data[$key]['spotlist_directory_type_hotel_room_id'] = $request->spotlist_directory_type_hotel_room_id[$key];
                        $update_data[$key]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $update_data[$key]['room_name'] = $value;
                        $update_data[$key]['room_price'] = $request->room_price[$key];
                        $update_data[$key]['room_facilities'] = $request->room_facilities[$key];
                        $update_data[$key]['room_description'] = $request->room_description[$key];
                        if (isset($request->room_image[$key]) && !empty($request->room_image[$key])){
                            $update_data[$key]['room_image'] = fileUpload($request->room_image[$key],'Backend/images/directory/hotel/');
                        }
                    }else{
                        $insert_data[$i]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $insert_data[$i]['room_name'] = $value;
                        $insert_data[$i]['room_price'] = $request->room_price[$key];
                        $insert_data[$i]['room_facilities'] = $request->room_facilities[$key];
                        $insert_data[$i]['room_description'] = $request->room_description[$key];
                        if (isset($request->room_image[$key]) && !empty($request->room_image[$key])){
                            $insert_data[$i]['room_image'] = fileUpload($request->room_image[$key],'Backend/images/directory/hotel/');
                        }
                        $i++;
                    }

                }

            }
        }
        $post_data['update_data'] = $update_data;
        $post_data['insert_data'] = $insert_data;
        return $post_data;
    }

    private function postRestaurantData(Request $request){
        $post_data = [];
        $insert_data = [];
        $update_data = [];

        if (isset($request->menu_name )&& !empty($request->menu_name)){
            $i= 0 ;
            foreach ($request->menu_name as $key => $value){
                if (!empty($value)){
                    if (isset($request->spotlist_directory_type_restaurant_id[$key])){
                        $update_data[$key]['spotlist_directory_type_restaurant_id'] = $request->spotlist_directory_type_restaurant_id[$key];
                        $update_data[$key]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $update_data[$key]['menu_name'] = $value;
                        $update_data[$key]['menu_price'] = $request->menu_price[$key];
                        $update_data[$key]['ingredients_item'] = $request->ingredients_item[$key];
                        $update_data[$key]['menu_description'] = $request->menu_description[$key];
                        if (isset($request->menu_image[$key]) && !empty($request->menu_image[$key])){
                            $update_data[$key]['menu_image'] = fileUpload($request->menu_image[$key],'Backend/images/directory/restaurant/');
                        }
                    }else{
                        $insert_data[$i]['spotlist_directory_id'] = $request->spotlist_directory_id;;
                        $insert_data[$i]['menu_name'] = $value;
                        $insert_data[$i]['menu_price'] = $request->menu_price[$key];
                        $insert_data[$i]['ingredients_item'] = $request->ingredients_item[$key];
                        $insert_data[$i]['menu_description'] = $request->menu_description[$key];
                        if (isset($request->menu_image[$key]) && !empty($request->menu_image[$key])){
                            $insert_data[$i]['menu_image'] = fileUpload($request->menu_image[$key],'Backend/images/directory/restaurant/');
                        }
                        $i++;
                    }
                }

            }
        }
        $post_data['update_data'] = $update_data;
        $post_data['insert_data'] = $insert_data;
        return $post_data;
    }

    private function postBeautyData(Request $request){
        $post_data = [];
        $insert_data = [];
        $update_data = [];
        if (isset($request->service_name )&& !empty($request->service_name)){
            $i=0;
            foreach ($request->service_name as $key => $value){
                if (!empty($value)){
                    if (isset($request->spotlist_directory_type_beauty_id[$key])){
                        $update_data[$key]['spotlist_directory_type_beauty_id'] = $request->spotlist_directory_type_beauty_id[$key];
                        $update_data[$key]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $update_data[$key]['service_name'] = $value;
                        $update_data[$key]['service_price'] = $request->service_price[$key];
                        $update_data[$key]['service_description'] = $request->service_description[$key];
                        if (isset($request->service_image[$key]) && !empty($request->service_image[$key])){
                            $update_data[$key]['service_image'] = fileUpload($request->service_image[$key],'Backend/images/directory/beauty/');
                        }
                    }else{
                        $insert_data[$i]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $insert_data[$i]['service_name'] = $value;
                        $insert_data[$i]['service_price'] = $request->service_price[$key];
                        $insert_data[$i]['service_description'] = $request->service_description[$key];
                        if (isset($request->service_image[$key]) && !empty($request->service_image[$key])){
                            $insert_data[$i]['service_image'] = fileUpload($request->service_image[$key],'Backend/images/directory/beauty/');
                        }
                        $i++;
                    }
                }

            }
        }
        $post_data['update_data'] = $update_data;
        $post_data['insert_data'] = $insert_data;
        return $post_data;
    }

    private function postShopData(Request $request){
        $post_data = [];
        $insert_data = [];
        $update_data = [];
        if (isset($request->product_name )&& !empty($request->product_name[0])){
            $i = 0;
            foreach ($request->product_name as $key => $value){
                if (!empty($value)){
                    if (isset($request->spotlist_directory_type_shop_id[$key])){
                        $update_data[$key]['spotlist_directory_type_shop_id'] = $request->spotlist_directory_type_shop_id[$key];
                        $update_data[$key]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $update_data[$key]['product_name'] = $value;
                        $update_data[$key]['product_varient'] = $request->product_varient[$key];
                        $update_data[$key]['product_price'] = $request->product_price[$key];
                        $update_data[$key]['product_description'] = $request->product_description[$key];
                        if (isset($request->product_image[$key]) && !empty($request->product_image[$key])){
                            $update_data[$key]['product_image'] = fileUpload($request->product_image[$key],'Backend/images/directory/shop/');
                        }
                    }else{
                        $insert_data[$i]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $insert_data[$i]['product_name'] = $value;
                        $insert_data[$i]['product_varient'] = $request->product_varient[$key];
                        $insert_data[$i]['product_price'] = $request->product_price[$key];
                        $insert_data[$i]['product_description'] = $request->product_description[$key];
                        if (isset($request->product_image[$key]) && !empty($request->product_image[$key])){
                            $insert_data[$i]['product_image'] = fileUpload($request->product_image[$key],'Backend/images/directory/beauty/');
                        }
                        $i++;
                    }
                }

            }
        }
        $post_data['update_data'] = $update_data;
        $post_data['insert_data'] = $insert_data;
        return $post_data;
    }

    private function postFitnessData(Request $request){
        $post_data = [];
        $insert_data = [];
        $update_data = [];

        if (isset($request->fitness_service_name ) && !empty($request->fitness_service_name)){
            $i =0 ;
            foreach ($request->fitness_service_name as $key => $value){
                if (!empty($value)){
                    if (isset($request->spotlist_directory_type_fitness_id[$key])){
                        $update_data[$key]['spotlist_directory_type_fitness_id'] = $request->spotlist_directory_type_fitness_id[$key];
                        $update_data[$key]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $update_data[$key]['fitness_service_name'] = $value;
                        $update_data[$key]['fitness_service_price'] = $request->fitness_service_price[$key];
                        $update_data[$key]['fitness_service_description'] = $request->fitness_service_description[$key];
                        if (isset($request->fitness_service_image[$key]) && !empty($request->fitness_service_image[$key])){
                            $update_data[$key]['fitness_service_image'] = fileUpload($request->fitness_service_image[$key],'Backend/images/directory/fitness/');
                        }
                    }else{
                        $insert_data[$i]['spotlist_directory_id'] = $request->spotlist_directory_id;
                        $insert_data[$i]['fitness_service_name'] = $value;
                        $insert_data[$i]['fitness_service_price'] = $request->fitness_service_price[$key];
                        $insert_data[$i]['fitness_service_description'] = $request->fitness_service_description[$key];
                        if (isset($request->fitness_service_image[$key]) && !empty($request->fitness_service_image[$key])){
                            $insert_data[$i]['fitness_service_image'] = fileUpload($request->fitness_service_image[$key],'Backend/images/directory/fitness/');
                        }
                        $i++;
                    }
                }

            }
        }
        $post_data['update_data'] = $update_data;
        $post_data['insert_data'] = $insert_data;
        return $post_data;
    }

    private function registrationStepUpdate($directiory_id,$registration_step){
        $directory = DB::table('spotlist_directory')->where('spotlist_directory_id',$directiory_id)->first();
        if (!empty($directory->registration_step)){
            $step_array = explode(',',$directory->registration_step);
            if (!in_array($registration_step,$step_array)){
                $new_registration_step = $directory->registration_step.','.$registration_step;
                DB::table('spotlist_directory')->where('spotlist_directory_id',$directiory_id)->update(['registration_step'=>$new_registration_step]);
            }
        }else{
            $new_registration_step = $registration_step;
            DB::table('spotlist_directory')->where('spotlist_directory_id',$directiory_id)->update(['registration_step'=>$new_registration_step]);
        }

    }

    public function directoryAllSave(Request $request){

        try {
            $directory = DB::table('spotlist_directory')->where('spotlist_directory_id',$request->spotlist_directory_id)->first();
            if ($directory->status == 'Incomplete'){
                if(Auth::user()->default_module_id == 3){

                    $update_data = [
                        'status' =>'Inactive',
                        'is_verified' => FALSE,
                    ];
                    $this->updateListingCapability();
                }else{
                    $update_data = [
                        'status' =>'Active',
                        'is_verified' => TRUE,
                    ];
                }
                DB::table('spotlist_directory')->where('spotlist_directory_id',$request->spotlist_directory_id)->update($update_data);
            }

            $data['spotlist_directory_id'] = $request->spotlist_directory_id;
            $response = $this->responseData(TRUE,'Directory saved successfully',$data);
        }catch (\Exception $e){
            $response =  $this->responseData(TRUE,'Directory save failed',$data);
        }
        return $response;
    }

    public function removeDirectoryFile(Request $request){
        $response = $this->deleteTable($request->table_name,$request->primary_key,$request->value);
        if (Auth::user()->default_module_id == LISTING_MANAGER){
            DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->decrement('photos_used');
        }
        return $response;
    }

    public function removeDirectoryGalleryFile(Request $request){
        $old_file_name = DB::table($request->table_name)->where($request->primary_key,$request->value)->first();
        $response = $this->deleteTable($request->table_name,$request->primary_key,$request->value);
        if ($response['success'] == TRUE){
            $this->unlinkedDirectoryImage($old_file_name);
        }
        if (Auth::user()->default_module_id == LISTING_MANAGER){
            DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->decrement('photos_used');
        }
        return $response;
    }

    private function unlinkedDirectoryImage($old_file_name){
        unlink($old_file_name->image);
    }

    private function directoryNotificationSend($directory,$data){
        $user = User::where('id',$directory->sys_users_id)->first();
        $notify_to_admin = User::where('default_module_id',ADMIN)->pluck('id')->toArray();
        $notify_body = [
            'title' => $data['notify_title'],
            'body' => $data['notify_message'],
            'name' => $user->username,
            'user_image' => $user->user_image,
            'directory_name' => $directory->spotlist_directory_name,
            'directory_id' => $directory->spotlist_directory_id,
            'thumb_image' => $directory->thumb_image,
        ];
        $notify_to = [
            'user_id' => $notify_to_admin,
            'type' => ADMIN_DIRECTORY,
        ];
        $service = new NotificationService();
        $service->sendNotification($notify_body,$notify_to);
    }

}
