<?php

use Intervention\Image\Facades\Image;


/**
 * @param $file
 * @param $directory
 * @param null $filename
 *
 * @return string
 */
function fileUpload($file, $directory, $filename = NULL,$old_file_name=NULL,$width=300,$height=300){
    $directory = rtrim($directory, '/');
    $upload_path = public_path($directory);
    if (!is_dir($upload_path)) {
        mkdir($upload_path, 0777, true);
    }
    if (isset($old_file_name) && $old_file_name != "" && file_exists(public_path().'/'.$old_file_name)) {
        unlink($old_file_name);
    }


    if($filename != NULL){
        $newfilename = $filename.'.'.$file->getClientOriginalExtension();
    }else{
        $newfilename = quickRandom().'.'.$file->getClientOriginalExtension();
    }
    Image::make($file)->save($upload_path.'/'.$newfilename);

    return $directory.'/'.$newfilename;
}


/**
 * Only For Image Usage Settings
 *
 * @param string $path
 * @param string $path_type = user/subscription/etc etc...
 *
 * @return string
 */
function getImageUrl($path = '', $path_type = 'user'){
    $default_image = '';
    if($path_type == 'user'){
        $default_image = asset('Backend/images/users/avatar.svg');
    }else{
        $default_image = asset('FontEnd/assets/images/no_image.svg');
    }
    return $path != '' && file_exists(public_path($path)) ? asset($path) : $default_image;
}

/**
 * Only For File Usage Settings
 *
 * @param string $path
 * @param string $path_type
 *
 * @return string
 */
function getFileUrl($path = '', $path_type = 'storage'){
    $default_image = '';
    if($path_type == 'user'){
        $default_image = asset('Backend/images/users/avatar.svg');
    }
    return $path != '' && file_exists(public_path($path)) ? asset($path) : $default_image;
}
function quickRandom($length = 16) {
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
}
