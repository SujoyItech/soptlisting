<?php


namespace App\Traits;


use Illuminate\Support\Facades\DB;

trait DataSaveTrait {

    public function responseData($success,$message,$data = []){
        $response = [
            'success' => $success,
            'message' => $message,
            'data' => $data,
        ];
        return $response;
    }

    public function insertOrUpdateTable($table_name,$primary_key,$primary_key_value,$post_data,$success_message='Data saved successfully',$warning_message='Data save failed'){

        try {
            if (isset($primary_key_value) && !empty($primary_key_value)){
                $check_data = DB::table($table_name)->where($primary_key,$primary_key_value)->first();
                if (isset($check_data)){
                    DB::table($table_name)->where($primary_key,$primary_key_value)->update($post_data);
                    $data[$primary_key] = $primary_key_value;
                }else{
                    $id = DB::table($table_name)->insertGetId($post_data);
                    $data[$primary_key] = $id;
                }
            }else{
                $id = DB::table($table_name)->insertGetId($post_data);
                $data[$primary_key] = $id;
            }
            $response = $this->responseData(TRUE,$success_message,$data);

        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$exception->getMessage());
        }
        return $response;
    }

    public function insertOrUpdateTableOnMultipleCondition($table_name,$conditions,$post_data,$success_message='Data saved successfully',$warning_message = 'Data save failed'){
        try {
            $check_data = DB::table($table_name)->where($conditions)->first();
            if (isset($check_data)){
                DB::table($table_name)->where($conditions)->update($post_data);
            }else{
                DB::table($table_name)->insert($post_data);
            }
            $data = DB::table($table_name)->where($conditions)->first();
            $response = $this->responseData(TRUE,$success_message,$data);
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$warning_message);
        }
        return $response;
    }

    public function insertOrDeleteTable($table_name,$primary_key,$primary_key_value,$post_data,$success_message='',$warning_message=''){
        try {
            $data = DB::table($table_name)->where($primary_key,$primary_key_value)->get();
            if (isset($data)){
                DB::table($table_name)->where($primary_key,$primary_key_value)->delete();
            }
            DB::table($table_name)->insert($post_data);
            $response = $this->responseData(TRUE,$success_message);
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$exception->getMessage());
        }
        return $response;
    }

    public function updateMultipleArray($table_name,$primary_key,$update_array,$success_message='',$warning_message=''){

        try {
            if (isset($update_array) && !empty($update_array)){
                foreach ($update_array as $key => $value){
                    DB::table($table_name)->where($primary_key,$value[$primary_key])->update($value);
                }
                $response = $this->responseData(TRUE,$success_message);
            }

        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$exception->getMessage());
        }
        return $response;
    }

    public function insertMultipleArray($table_name,$insert_array,$success_message='Data saved successfully!',$warning_message='Data save failed!'){

        try {
            if (isset($insert_array) && !empty($insert_array[0])){
                 DB::table($table_name)->insert($insert_array);
            }
            $response = $this->responseData(TRUE,$success_message);

        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$exception->getMessage());
        }
        return $response;
    }

    public function insertTable($table_name,$insert_data,$success_message='Data saved successfully.',$warning_message='Data save failed!'){
        try {
            $data['id'] = DB::table($table_name)->insertGetId($insert_data);
            $response = $this->responseData(TRUE,$success_message,$data);
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$exception->getMessage());
        }
        return $response;
    }

    public function updateTable($table_name,$update_data,$primary_key_name,$primary_key_value,$success_message='Data saved successfully.',$warning_message='Data save failed!'){
        try {
            DB::table($table_name)->where($primary_key_name,$primary_key_value)->update($update_data);
            $data['id'] = $primary_key_value;
            $response = $this->responseData(TRUE,$success_message,$data);
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$warning_message);
        }
        return $response;
    }

    public function deleteTable($table_name,$primary_key_name,$primary_key_value,$success_message='Data deleted successfully.',$warning_message='Data deleted failed!'){
        try {
            DB::table($table_name)->where($primary_key_name,$primary_key_value)->delete();
            $response = $this->responseData(TRUE,$success_message);
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$warning_message);
        }
        return $response;
    }

}
