<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpotlistBookingMessage extends Model
{
    protected $fillable = ['booking_id','from','to','message','status'];
}
