<?php


namespace App\Http\Controllers\FontEnd\Booking;


use App\Http\Service\NotificationService;
use App\Traits\DataReturn;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookingService {

    use DataReturn;
    use DataSaveTrait;

    public function bookingRequestSend(Request $request){
        $post_data = $this->bookingRequestData($request);
        $response = $this->insertTable('spotlist_booking_request',$post_data,'Booking request sent','Booking request send failed.');
        if ($response['success'] == TRUE){
            $this->userBooingNotificationSend($request);
            $this->updateBookingId($response['data']['id']);
        }
        return $response;
    }

    private function updateBookingId($id){
        $sp = 'BK';
        $date = date('ymd');
        $entry = str_pad($id,4,0,STR_PAD_LEFT);
        $booking_id = $sp.$date.$entry;
        DB::table('spotlist_booking_request')->where('spotlist_booking_request_id',$id)->update(['booking_id'=>$booking_id]);
    }

    private function bookingRequestData(Request $request){
        $post_data = $request->except('type');
        $post_data['customer_id'] = Auth::user()->id;
        $post_data['status'] = 'Requested';
        $post_data['created_at'] = Carbon::now();
        $post_data['updated_at'] = Carbon::now();
        return $post_data;
    }

    private function userBooingNotificationSend($reservation){
        $directory = DB::table('spotlist_directory')->where('spotlist_directory_id',$reservation->spotlist_directory_id)->first();
        $notify_body = [
            'username' => Auth::user()->name,
            'user_image' => Auth::user()->user_image,
            'title' => __('New booking request'),
            'body' => Auth::user()->name.__(' sent a booking request for ').$directory->spotlist_directory_name,
            'directory_name' => $directory->spotlist_directory_name,
            'directory_id' => $directory->spotlist_directory_id,
            'type' => strtolower($directory->type)
        ];
        $notify_to = [
            'user_id' => $directory->sys_users_id,
            'type' => LISTING_MANAGER_ADMIN_BOOKING,
        ];
        $service = new NotificationService();
        $service->sendNotification($notify_body,$notify_to);
    }

    public function listingBookingsCount($directory_id){
        DB::table('spotlist_directory')->where('spotlist_directory_id',$directory_id)->increment('booking_count',1);
    }
}
