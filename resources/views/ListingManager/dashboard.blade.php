@extends('Backend.layouts.app',['menu'=>'dashboard'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">{{__('Dashboard')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="mt-1 text-dark"><span data-plugin="counterup">{{$listing->total_directory ?? 0}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{__('Published Listings')}}</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-3 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$total_reviews->total_reviews ?? 0}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{__('Total Reviews')}}</p>
                        </div>
                    </div>

                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-3 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$wishing_list->total_wishing_list ?? 0}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{__('Total wishlist')}}</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-3 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$booking_request->total_booking_request ?? 0}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{__('Total Bookings')}}</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
    </div>
@endsection
