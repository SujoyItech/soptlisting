@extends('Backend.layouts.blank')
@section('content')
    <div class="auth-fluid">
        <!--Auth fluid left content -->
        <div class="auth-fluid-form-box">
            <div class="align-items-center d-flex h-100">
                <div class="card-body">
                    <!-- Logo -->
                    <div class="auth-brand text-center text-lg-left">
                        <div class="auth-logo">
                            <a href="{{url('/')}}" class="logo logo-dark text-center">
                            <span class="logo-lg">
                                <img src="{{asset('/Backend/images/logo-dark.png')}}" alt="" height="22">
                            </span>
                            </a>
                            <a href="{{url('/')}}" class="logo logo-light text-center">
                            <span class="logo-lg">
                                <img src="{{asset('/Backend/images/logo-light.png')}}" alt="" height="22">
                            </span>
                            </a>
                        </div>
                    </div>
                    <!-- title-->
                    <h4 class="mt-0">Reset Password</h4>
                    <p class="text-muted mb-4">{{__('Confirm your new Password')}}</p>
                    <!-- form -->

                    <form action="{{ route('forgetPasswordResetProcess') }}" method="POST" data-toggle="validator" role="form">
                        @csrf
                        <div class="form-group">
                            <a href="{{ route('forgetPassword') }}" class="text-muted float-right"><small>{{ __('Forgot Your Password?') }}</small></a>
                            <label for="password">{{ __('Password') }}</label>
                            <div class="input-group input-group-merge">
                                <input name="password" type="password" id="password" class="form-control" placeholder="Enter your password" value="123456">
                                <div class="input-group-append" data-password="false">
                                    <div class="input-group-text">
                                        <span class="password-eye font-12"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <a href="{{ route('forgetPassword') }}" class="text-muted float-right"><small>{{ __('Forgot Your Password?') }}</small></a>
                            <label for="password">{{ __('Password') }}</label>
                            <div class="input-group input-group-merge">
                                <input name="confirm_password" type="password" id="confirm_password" data-match="#password" class="form-control" placeholder="Confirm your password" value="123456" data-match-error="Whoops, these don't match">
                                <div class="input-group-append" data-password="false">
                                    <div class="input-group-text">
                                        <span class="password-eye font-12"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group mb-0 text-center">
                            <input type="hidden" name="remember_token" value="{{$remember_token}}">
                            <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-save"></i> {{ __('Update Password') }}</button>
                        </div>
                    </form>
                    @if(Session::has('warning'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{Session::get('warning')}}
                        </div>
                    @endif
                    <footer class="footer footer-alt">
                        <p class="text-muted"><a href="{{url('login')}}" class="text-muted ml-1"><b>Sign In</b></a></p>
                    </footer>

                </div> <!-- end .card-body -->
            </div> <!-- end .align-items-center.d-flex.h-100-->
        </div>
        <!-- end auth-fluid-form-box-->

        <!-- Auth fluid right content -->
        <div class="auth-fluid-right text-center">
            <div class="auth-user-testimonial">
                <h2 class="mb-3 text-white">I love the color!</h2>
                <p class="lead"><i class="mdi mdi-format-quote-open"></i> I've been using your theme from the previous developer for our web app, once I knew new version is out, I immediately bought with no hesitation. Great themes, good documentation with lots of customization available and sample app that really fit our need. <i class="mdi mdi-format-quote-close"></i>
                </p>
                <h5 class="text-white">
                    - Fadlisaad (Ubold Admin User)
                </h5>
            </div> <!-- end auth-user-testimonial-->
        </div>
        <!-- end Auth fluid right content -->
    </div>
@endsection
