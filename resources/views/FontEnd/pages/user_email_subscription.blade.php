<div class="getin-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="get-register">
                    <h2>{{__('Let’s Stay In Touch')}}</h2>
                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!! $error !!}
                            </div>
                        @endforeach
                    @endif
                    <form method="post" action="{{route('user.email.subscription')}}" >
                        @csrf
                        <div class="input">
                            <input type="email" name="email" class="form-control" placeholder="Subscribe using your email" tabindex="0" required>
                        </div>

                        <div class="submit clearfix">
                            <button class="btn theme-btn" type="submit" tabindex="0">{{__('Subscribe')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
