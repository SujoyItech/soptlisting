<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\CategoryModule\CategoryService;
use App\Http\Controllers\Admin\UserModule\UserService;
use App\Http\Controllers\Common\Directory\DirectoryService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function removeAdminFileRecord(Request $request){
        DB::table($request->table_name)->where($request->primary_key,$request->value)->delete();
        $response['success'] = TRUE;
        return $response;
    }

    public function index(){
        $data = [];
        $directory = DB::table('spotlist_directory')->where('status','Active')->get();
        if (isset($directory) && !empty($directory[0])) {
            $data['listing'] = DB::table('spotlist_directory')
                                 ->select(DB::raw("COUNT(spotlist_directory.sys_users_id) as total_directory"))
                                 ->where(['spotlist_directory.status' => 'Active'])
                                 ->first();

            $data['wishing_list'] = DB::table('spotlist_directory_wishinglist')
                                      ->join('spotlist_directory', 'spotlist_directory_wishinglist.spotlist_directory_id', '=', 'spotlist_directory.spotlist_directory_id')
                                      ->select(DB::raw("COUNT(spotlist_directory_wishinglist.id) as total_wishing_list"))
                                      ->first();

            $data['total_reviews'] = DB::table('spotlist_directory_reviews')
                                       ->join('spotlist_directory', 'spotlist_directory_reviews.spotlist_directory_id', '=', 'spotlist_directory.spotlist_directory_id')
                                       ->select(DB::raw("COUNT(spotlist_directory_reviews.id) as total_reviews"))
                                       ->first();

            $data['booking_request'] = DB::table('spotlist_booking_request')
                                        ->join('spotlist_directory','spotlist_booking_request.spotlist_directory_id','=','spotlist_directory.spotlist_directory_id')
                                         ->select(DB::raw("COUNT(spotlist_booking_request.spotlist_booking_request_id) as total_booking_request"))
                                         ->first();

            $data['main_categories'] = DB::table('spotlist_category')
                                         ->leftJoin('spotlist_directory', 'spotlist_category.spotlist_category_id', '=', 'spotlist_directory.spotlist_parent_category_id')
                                         ->select('spotlist_category.spotlist_category_id', 'spotlist_category.spotlist_category_name', 'spotlist_category.category_image',
                                             DB::raw("COUNT(spotlist_directory.spotlist_directory_id) AS total_directory"))
                                         ->where(['spotlist_category.parent_id'=>0,'spotlist_category.status'=>'Active'])
                                         ->groupBy('spotlist_category.spotlist_category_id')
                                         ->get();
        }

        return view('Admin.dashboard', $data);
    }

    public function getTopDirectoryChart(Request $request){
        $service = new DirectoryService();
        $response = $service->getTopDirectoryChart($request);
        return response()->json($response);
    }

    public function getTopUserChart(){
        $service = new UserService();
        $response = $service->getTopUserChartData();
        return response()->json($response);
    }

    public function getDirectoryListByCategory(){
        $service = new CategoryService();
        $response = $service->getDirectoryListByCategory();
        return response()->json($response);
    }
}
