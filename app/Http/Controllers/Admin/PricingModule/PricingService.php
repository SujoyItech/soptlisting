<?php


namespace App\Http\Controllers\Admin\PricingModule;


use App\Http\Service\MailService;
use App\Http\Service\NotificationService;
use App\Models\User;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PricingService {
    use DataSaveTrait;

    public function getPackageData($id = NULL){
        $query = DB::table('spotlist_packages');
        $data = $id != NULL ? $query->where('spotlist_packages_id',$id)->first():$query->get();
        return $data;
    }
    private function packageListData(){
        $package_list = DB::table('spotlist_packages')->where('status','Active')->get();
        return $package_list;
    }

    public function adminPackageSave(Request $request){
        $post_data = $this->packageData($request);
        $response = $this->insertOrUpdateTable('spotlist_packages','spotlist_packages_id',$request->spotlist_packages_id ?? '',$post_data);
        return $response;
    }

    private function packageData(Request $request){
        $post_data = $request->except('spotlist_packages_id','add_video_ability','add_contact_form_ability','is_recomended','is_featured');
        if (!empty($request->add_video_ability) && $request->add_video_ability == 'on'){
            $post_data['add_video_ability'] = 1;
        }
        if (!empty($request->add_contact_form_ability) && $request->add_contact_form_ability == 'on'){
            $post_data['add_contact_form_ability'] = 1;
        }
        if (!empty($request->is_recomended) && $request->is_recomended == 'on'){
            $post_data['is_recomended'] = 1;
        }
        if (!empty($request->is_featured) && $request->is_featured == 'on'){
            $post_data['is_featured'] = 1;
        }
        return $post_data;
    }

    public function adminUserOfflinePaymentSave(Request $request){

        DB::beginTransaction();
        try {
            $package = DB::table('spotlist_packages')->where('spotlist_packages_id',$request->spotlist_packages_id)->first();
            $subscription_data = $this->subscriptionData($request,$package);
            $post_data = [
                'sys_users_id' => $request->sys_users_id,
                'spotlist_package_id' => $request->spotlist_packages_id,
                'payment_amount' => $package->price,
                'payment_method' => $request->payment_method,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            $subscription = $this->insertOrUpdateTable('spotlist_user_subscription','user_id',$request->sys_users_id,$subscription_data,'Subscription successful.','Subscription failed!');
            DB::table('spotlist_offline_payment')->insert($post_data);
            $this->savePaymentHistory($request);
            $notify_response = $this->sendPurchaseNotification($package,$request->sys_users_id);
            $response = $this->responseData(TRUE,'Online payment successful.');

            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            $response = $this->responseData(FALSE,$exception->getMessage());
        }
        return $response;
    }

    private function subscriptionData($request,$package){

        $subscription_data = [
            'spotlist_packages_id' => $package->spotlist_packages_id,
            'user_id' => $request->sys_users_id,
            'num_of_listing' => $package->num_of_listing,
            'day_remain' => $package->day_validity,
            'num_of_categories' => $package->num_of_categories,
            'num_of_tags' => $package->num_of_tags,
            'num_of_photos' => $package->num_of_photos,
            'add_video_ability' => $package->add_video_ability,
            'add_contact_form_ability' => $package->add_contact_form_ability
        ];
        $old_package = DB::table('spotlist_user_subscription')->where('user_id',$request->sys_users_id)->first();
        if (isset($old_package) && !empty($old_package)){
            $subscription_data['day_remain'] = $package->day_validity+$old_package->day_remain;
            $subscription_data['num_of_listing'] = $package->num_of_listing+$old_package->num_of_listing;
            $subscription_data['num_of_categories'] = $package->num_of_categories+$old_package->num_of_categories;
            $subscription_data['num_of_tags'] = $package->num_of_tags+$old_package->num_of_tags;
            $subscription_data['num_of_photos'] = $package->num_of_photos+$old_package->num_of_photos;
        }else{
            $subscription_data['listing_item_used'] = 0;
            $subscription_data['category_used'] = 0;
            $subscription_data['tag_used'] = 0;
            $subscription_data['photos_used'] = 0;
        }
        return $subscription_data;
    }

    private function savePaymentHistory(Request $request){
        $post_data = [
            'spotlist_packages_id' => $request->spotlist_packages_id,
            'user_id' => $request->sys_users_id,
            'purchase_date' => Carbon::now(),
            'amount_paid' => $request->payment_amount,
            'payment_method' => $request->payment_method,
            'payment_status' => 'Paid',
            'created_at' => Carbon::now()
        ];
        DB::table('spotlist_subscription_purchase_history')->insert($post_data);

    }

    public function getPaymentHistory($id = NULL,$user_id = NULL){
        $payment_data = DB::table('spotlist_subscription_purchase_history')
          ->leftJoin('spotlist_packages','spotlist_subscription_purchase_history.spotlist_packages_id','spotlist_packages.spotlist_packages_id')
          ->leftJoin('sys_users','spotlist_subscription_purchase_history.user_id','sys_users.id')
          ->select('spotlist_subscription_purchase_history.*','sys_users.name as user_name','spotlist_packages.spotlist_packages_name',
              'spotlist_packages.packages_type','spotlist_packages.price','spotlist_packages.day_validity','spotlist_packages.num_of_listing',
              'spotlist_packages.num_of_photos');
        if ($user_id != NULL){
            $payment_data = $payment_data->where('user_id',$user_id);
        }
        return $id !== NULL ? $payment_data->where('spotlist_packages.spotlist_packages_id',$id)->first() : $payment_data->orderBy('spotlist_subscription_purchase_history.purchase_date','desc')->get();
    }

    public function sendPurchaseNotification($package,$user_id){
        try {
            $user = User::where('id',$user_id)->first();
            $notify_data = [
                'username' => Auth::user()->username,
                'user_image' => Auth::user()->user_image,
                'title' => __('New package subscription.'),
                'body' => $package->spotlist_packages_name.__(' package has been subscribed by spotlisting admin'),
            ];
            $notify_to = [
                'user_id' => $user->id,
                'type' => SUBSCRIPTION_NOTIFICATION
            ];
            $body['message'] = __('Congratulations! ').$package->spotlist_packages_name.__(' package has been subscribed by spotlisting admin');
            $body['name'] = $user->username;
            MailService::sendMessageMail($user->email, __('Spotlisting Package Subscription'),$body);
            $service = new NotificationService();
            $service->sendNotification($notify_data,$notify_to);
            $response['success'] = TRUE;
        }catch (\Exception $exception){
           $response['success'] = FALSE;
           $response['message'] = $exception->getMessage();
        }
        return $response;
    }
}
