<div class="card-box">
    <div class="text-center">
        <input type="hidden" name="package_id" value="{{$packages->spotlist_packages_id ?? ''}}" id="package_id">
        <div id="dropin-container"></div>
        <button id="subscribe-button" class="btn btn-info btn-sm">{{__('Subscribe now')}}</button>
    </div>
</div>
<script>
    $(document).on('click','#subscribe-button',function (){
        var submit_url = "{{route('free-subscription')}}";
        var formData = {
            spotlist_packages_id : $('#package_id').val(),
        };
        makeAjaxPost(formData,submit_url,null).done(function (response){
            var url ="{{url('listing-manager-payment-history')}}";
            if (response.success) {
                swalRedirect(url,response.message,'success')
            } else {
                swalRedirect(url,response.message,'warning')
            }
        });
    })
</script>
