<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistDirectoryTypeFitnessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_directory_type_fitness', function (Blueprint $table) {
            $table->increments('spotlist_directory_type_fitness_id');
            $table->unsignedInteger('spotlist_directory_id');
            $table->string('fitness_service_name');
            $table->decimal('fitness_service_price',10,2)->nullable();
            $table->text('fitness_service_description')->nullable();
            $table->string('fitness_service_image')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_directory_type_fitness');
    }
}
