<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('spotlist_category')->insert(array (
            0 =>
                array (
                    'category_icon' => NULL,
                    'category_image' => 'Backend/images/category/QnX8JIf0Swgn2Fxh.png',
                    'parent_id' => 0,
                    'spotlist_category_id' => 1,
                    'spotlist_category_name' => 'General',
                    'status' => 'Active',
                ),
            1 =>
                array (
                    'category_icon' => 'fa fa-shopping-cart',
                    'category_image' => 'Backend/images/category/KXeMEHmxvOn0J2mR.jpg',
                    'parent_id' => 0,
                    'spotlist_category_id' => 2,
                    'spotlist_category_name' => 'Hotel',
                    'status' => 'Active',
                ),
            2 =>
                array (
                    'category_icon' => 'fa fa-female',
                    'category_image' => 'Backend/images/category/LBRf0iyXAmSxQpNt.jpg',
                    'parent_id' => 0,
                    'spotlist_category_id' => 3,
                    'spotlist_category_name' => 'Restaurant',
                    'status' => 'Active',
                ),
            3 =>
                array (
                    'category_icon' => 'fa-fa-restaurant',
                    'category_image' => 'Backend/images/category/N5P2YiPqSTxFWgAk.jpg',
                    'parent_id' => 0,
                    'spotlist_category_id' => 4,
                    'spotlist_category_name' => 'Beauty',
                    'status' => 'Inactive',
                ),
            4 =>
                array (
                    'category_icon' => NULL,
                    'category_image' => 'Backend/images/category/EzwRO6BlYO3svgSH.jpg',
                    'parent_id' => 0,
                    'spotlist_category_id' => 5,
                    'spotlist_category_name' => 'Shopping',
                    'status' => 'Inactive',
                ),
            5 =>
                array (
                    'category_icon' => NULL,
                    'category_image' => 'Backend/images/category/zoYWZMDjwkgzu85a.jpg',
                    'parent_id' => 0,
                    'spotlist_category_id' => 6,
                    'spotlist_category_name' => 'Fitness',
                    'status' => 'Inactive',
                ),
        ));
    }
}
