<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <label for="name">{{__('STRIPE KEY')}}</label>
            <input type="text" name="stripe_key" parsley-trigger="change"  value="{{ $settings->stripe_key ?? ''}}"
                   placeholder="STRIPE_KEY" class="form-control" id="stripe_key">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <label for="name">{{__('STRIPE SECRET')}}</label>
            <input type="text" name="stripe_secret" parsley-trigger="change"  value="{{ $settings->stripe_secret ?? ''}}"
                   placeholder="STRIPE_SECRET" class="form-control" id="stripe_secret">
        </div>
    </div>
</div>
