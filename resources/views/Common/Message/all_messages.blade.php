@extends('Backend.layouts.app')

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('All Messages')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <!-- Left sidebar -->
                <div class="inbox-leftbar">

                    <div class="mail-list mt-4">
                        <a href="{{route('view-all-admin-message')}}" class="{{$type == NULL ? 'text-danger font-weight-medium': ''}}"><i class="dripicons-inbox mr-2"></i>{{__('ALL')}}<span class="badge badge-soft-info float-right ml-2">{{$all_message_count ?? 0}}</span></a>
                        <a href="{{route('view-all-admin-message',['type'=>'read'])}}"  class="{{$type == 'read' ? 'text-danger font-weight-medium': ''}}"><i class="dripicons-star mr-2"></i>{{__('Read')}}<span class="badge badge-soft-info float-right ml-2">{{$read_message_count ?? 0}}</span></a>
                        <a href="{{route('view-all-admin-message',['type'=>'unread'])}}" class="{{$type == 'unread' ? 'text-danger font-weight-medium': ''}}"><i class="dripicons-clock mr-2"></i>{{__('Unread')}}<span class="badge badge-soft-info float-right ml-2">{{$unread_message_count ?? 0}}</span></a>
                    </div>
                </div>
                <!-- End Left sidebar -->

                <div class="inbox-rightbar">
                    @if(isset($messages) && !empty($messages[0]))
                    <div class="mt-3">
                        <ul class="message-list">
                            @foreach($messages as $message)
                                @php($message_body = json_decode($message->body))
                                <li class="{{$message->status == 0 ? 'unread': ''}}" >
                                    <div class="col-mail col-mail-1">
                                        <div class="checkbox-wrapper-mail">
                                            <input type="checkbox" id="chk4" name="message_id">
                                            <label for="chk4" class="toggle"></label>
                                        </div>
                                        <span class="star-toggle far fa-star text-warning"></span>
                                        <a href="{{route('view-message-details',['id'=>$message->id])}}" class="title message-item" data-id="{{$message->id}}">{{$message_body->username ?? ''}}</a>
                                    </div>
                                    <div class="col-mail col-mail-2">
                                        <a href="{{route('view-message-details',['id'=>$message->id])}}" class="subject message-item" data-id="{{$message->id}}">{{ $message_body->title ?? ''}}</a>
                                        </a>
                                        <div class="date" style="width: 150px!important;">{{Carbon\Carbon::parse($message->created_at)->diffForHumans()}}</div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- end .mt-4 -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-group float-right">
                                {{$messages->links('vendor.pagination.bootstrap-4')}}
                            </div>
                        </div> <!-- end col-->
                    </div>
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center justify-content-center">
                                <p>{{__('No message found')}}</p>
                            </div>
                        </div>
                    </div>
                @endif
                    <!-- end row-->
                </div>
                <!-- end inbox-rightbar-->

                <div class="clearfix"></div>
            </div> <!-- end card-box -->

        </div> <!-- end Col -->
    </div>
@endsection
@section('script')
    <script>
        $(document).on("click", ".message-item", function (event) {
            event.preventDefault();
            var data = {
                id : $(this).data('id')
            }
            var url = "{{url('admin-notification-make-seen')}}";
            var this_directory = $(this);
            makeAjaxPost(data,url,null).done(function (response){
                if (response.success == true){
                    var redirect_url = $(this_directory).attr('href');
                    window.location = redirect_url;
                }

            });
        });
    </script>
@endsection
