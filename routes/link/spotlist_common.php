<?php
Route::group(['middleware'=>'common.permission'],function (){

    Route::post('remove-directory-gallery-file', 'Directory\CreateDirectoryController@removeDirectoryGalleryFile')->name('remove-directory-gallery-file');
    Route::post('remove-directory-file', 'Directory\CreateDirectoryController@removeDirectoryFile')->name('remove-directory-file');
    Route::post('change-cities-by-country', 'Directory\CreateDirectoryController@changeCitiesByCountry')->name('change-cities-by-country');
    Route::group(['middleware'=>'subscription.check'],function (){

        Route::post('check-directory-slug', 'Directory\CreateDirectoryController@checkDirectorySlug')->name('check-directory-slug');

        Route::get('directory-add/{id?}', 'Directory\CreateDirectoryController@directoryAdd')->name('directory-create');
        Route::post('directory-basic-save', 'Directory\CreateDirectoryController@directoryBasicSave')->name('directroy-basic-save');
        Route::post('directory-location-save', 'Directory\CreateDirectoryController@directoryLocationSave')->name('directroy-location-save');
        Route::post('directory-features-save', 'Directory\CreateDirectoryController@directoryFeaturesSave')->name('directory-features-save');
        Route::post('directory-gallery-save', 'Directory\CreateDirectoryController@directoryGallerySave')->name('directory-gallery-save');
        Route::post('directory-seo-save', 'Directory\CreateDirectoryController@directorySeoSave')->name('directory-seo-save');
        Route::post('directory-schedule-save', 'Directory\CreateDirectoryController@directoryScheduleSave')->name('directory-schedule-save');
        Route::post('directory-contact-save', 'Directory\CreateDirectoryController@directoryContactSave')->name('directory-contact-save');
        Route::post('directory-type-save', 'Directory\CreateDirectoryController@directoryTypeSave')->name('directory-type-save');
        Route::post('directory-all-save', 'Directory\CreateDirectoryController@directoryAllSave')->name('directory-all-save');

        Route::get('directory-all-save', 'Directory\CreateDirectoryController@directoryAllSave')->name('directory-all-save');
    });


    Route::get('user-booking-request/{type}/{booking_id?}', 'Booking\BookingController@userBookingRequest')->name('user-booking-request');
    Route::get('all-booking-request', 'Booking\BookingController@allBookingRequest')->name('all-booking-request');
    Route::post('booking-status-change', 'Booking\BookingController@bookingStatusChange')->name('booking-status-change');


    Route::get('profile-settings', 'Profile\ProfileController@profileSettings')->name('profile-settings');

});

Route::post('profile-update', 'Profile\ProfileController@profileUpdate')->name('profile-update');
Route::post('password-update', 'Profile\ProfileController@passwordUpdate')->name('password-update');

Route::post('view-user-booking-message', 'Message\MessageController@bookingRequestMessage')->name('view-user-booking-message');
Route::post('user-booking-request-message-send', 'Message\MessageController@bookingRequestMessageSend')->name('user-booking-request-message-send');

Route::post('view-user-booking-status-message', 'Message\MessageController@bookingStatusMessage')->name('view-user-booking-status-message');

Route::get('load-admin-notification-body', 'Message\MessageController@getAdminNotificationBody')->name('load-admin-notification-body');
Route::get('view-all-admin-notification', 'Message\MessageController@viewAllAdminNotification')->name('view-all-admin-notification');
Route::post('admin-notification-make-seen', 'Message\MessageController@notificationMakeSeen')->name('admin-notification-make-seen');
Route::get('clear-all-admin-notification', 'Message\MessageController@clearAllAdminNotification')->name('clear-all-admin-notification');

Route::get('load-admin-message-body', 'Message\MessageController@getAdminMessageBody')->name('load-admin-message-body');
Route::get('view-all-admin-message/{type?}', 'Message\MessageController@viewAllAdminMessage')->name('view-all-admin-message');
Route::get('view-message-details/{id}', 'Message\MessageController@viewMesageDetails')->name('view-message-details');
Route::get('clear-all-admin-message', 'Message\MessageController@clearAllAdminMessage')->name('clear-all-admin-message');




