<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistSubscriptionPurchaseHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_subscription_purchase_history', function (Blueprint $table) {
            $table->increments('spotlist_subscription_purchase_history_id');
            $table->unsignedInteger('spotlist_packages_id');
            $table->unsignedBigInteger('user_id');
            $table->dateTime('purchase_date')->nullable();
            $table->decimal('amount_paid',10,2)->nullable();
            $table->text('payment_method')->nullable();
            $table->text('purchase_log_data')->nullable();
            $table->string('payment_status')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_subscription_purchase_history');
    }
}
