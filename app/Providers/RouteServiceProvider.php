<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * If specified, this namespace is automatically applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::middleware('web')
                ->group(base_path('routes/web.php'));

            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                 ->namespace($this->namespace)
                 ->group(base_path('routes/auth.php'));

            Route::middleware(['web','auth','auth.permission'])
                 ->namespace($this->namespace.'\Backend')
                 ->group(base_path('routes/app.php'));

            Route::middleware(['web','auth','admin.permission','auth.permission'])
                 ->namespace($this->namespace.'\Admin')
                 ->group(base_path('routes/link/spotlist_admin.php'));

            Route::middleware(['web','auth','listing_manager.permission','auth.permission'])
                 ->namespace($this->namespace.'\ListingManager')
                 ->group(base_path('routes/link/spotlist_list_manager.php'));

            Route::middleware(['web','auth','auth.permission'])
                 ->namespace($this->namespace.'\Common')
                 ->group(base_path('routes/link/spotlist_common.php'));

            Route::middleware('web')
                 ->namespace($this->namespace.'\FontEnd')
                 ->group(base_path('routes/link/spotlist_fontend.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60);
        });
    }
}
