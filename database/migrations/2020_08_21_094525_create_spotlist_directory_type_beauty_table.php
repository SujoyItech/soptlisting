<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistDirectoryTypeBeautyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_directory_type_beauty', function (Blueprint $table) {
            $table->increments('spotlist_directory_type_beauty_id');
            $table->unsignedInteger('spotlist_directory_id');
            $table->string('service_name');
            $table->string('service_start_time')->nullable();
            $table->string('service_end_time')->nullable();
            $table->integer('service_duration')->nullable();
            $table->decimal('service_price',10,2)->nullable();
            $table->text('service_description')->nullable();
            $table->string('service_image')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_directory_type_beauty');
    }
}
