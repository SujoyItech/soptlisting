@extends('FontEnd.master')
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('My Wishlist')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <section class="faq-pg-section section-padding">
        <div class="container">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="{{route('user.profile.settings')}}"  aria-expanded="false" class="nav-link">
                        {{__('Profile')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.bookings')}}"  aria-expanded="false" class="nav-link">
                        {{__('My bookings')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.wishing')}}" aria-expanded="true" class="nav-link active">
                        {{__('My Wishlist')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user-all-notification')}}" aria-expanded="false" class="nav-link">
                        {{__('My notifications')}}
                    </a>
                </li>
            </ul>
            <div class="listing-area profile-wrap">
                @if(isset($list) && !empty($list[0]))
                    <div class="container">
                        <div class="row">
                            @foreach($list as $data)
                                <div class="col-lg-4 col-md-6 col-12 mb-3">
                                    <div class="product-item">
                                        <a href="{{route('user.listing',['slug'=>$data->spotlist_directory_slug])}}">
                                            <div class="product-img">
                                                <img src="{{getImageUrl($data->thumb_image,'other')}}" alt="" height="300">
                                                @if($data->is_featured == 1)
                                                    <div class="pro-featured">
                                                        <span class="theme-btn v3" >{{__('Featured')}}</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </a>
                                        <div class="product-content">
                                            <span><a href="{{route('user.profile',['id'=>$data->user_id])}}"><small>{{$data->user_name}}</small></a> | <small>{{ Carbon\Carbon::parse($data->created_at)->format('d F Y') }}</small></span>
                                            <h4><a href="{{route('user.listing',['slug'=>$data->spotlist_directory_slug])}}">{{$data->spotlist_directory_name}}</a></h4>
                                            <span>{{isset($data->city_name) ? $data->city_name.' , ' : ''}} {{$data->country_name}}</span>
                                            <div class="ratting-area">
                                                @if(isset($data->rating))
                                                    <ul>
                                                        @for($x = 0; $x< 5 ; $x++)
                                                            @if(floor($data->rating) - $x >=1)
                                                                <li><i class="fa fa-star"></i></li>
                                                            @elseif($data->rating -$x >0)
                                                                <li><i class="fa fa-star-half-o"></i></li>
                                                            @else
                                                                <li><i class="fa fa-star-o"></i></li>
                                                            @endif
                                                        @endfor
                                                    </ul>
                                                @else
                                                    <ul>
                                                        @for($x = 0; $x< 5 ; $x++)
                                                            <li><i class="fa fa-star-o"></i></li>
                                                        @endfor
                                                    </ul>
                                                @endif
                                                <p><span>{{isset($data->rating) ? number_format($data->rating,1) : '0.0' }}</span> ({{$data->total_reviews}} {{__('Reviews')}})</p>
                                            </div>
                                            <div class="pro-bottom">
                                                <ul>
                                                    <li><small class="theme-btn open">Now Open</small></li>
                                                    <li><a class="popup-gmaps" href="https://maps.google.com/maps?q={{$data->latitude}},{{$data->longitude}}&hl=es&z=14&amp;output=embed" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/logo/Vector.png" alt="">{{__('Show On Map')}}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="thumb">
                                            <span>{{$data->category_name}}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="listing-wrap">
                                <div class="pagination-wrapper">
                                    {{ $list->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="container">
                        <div class="row">
                            <div class="col-6 offset-4">
                                <div class="no-data-image">
                                    <img src="{{asset('FontEnd/assets/images/error/nodata.svg')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
@section('script')
    <!-- google maps -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAy-PboZ3O_A25CcJ9eoiSrKokTnWyAmt8"></script>
    <script src="{{asset('FontEnd/')}}/assets/js/richmarker.js"></script>
    <!-- Custom script for this template -->
    <script src="{{asset('FontEnd/')}}/assets/js/script.js"></script>
    <script src="{{asset('FontEnd/')}}/assets/js/map-active.js"></script>
@endsection
