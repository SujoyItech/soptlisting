<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MigrationsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migrations')->delete();
        
        \DB::table('migrations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'migration' => '2014_10_12_000000_create_users_table',
                'batch' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'migration' => '2014_10_12_100000_create_password_resets_table',
                'batch' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'migration' => '2018_08_29_200844_create_languages_table',
                'batch' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'migration' => '2018_08_29_205156_create_translations_table',
                'batch' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'migration' => '2019_08_19_000000_create_failed_jobs_table',
                'batch' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'migration' => '2020_03_02_072501_create_sys_dropdowns_table',
                'batch' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'migration' => '2020_03_02_130721_create_sys_system_settings_table',
                'batch' => 1,
            ),
            7 => 
            array (
                'id' => 8,
                'migration' => '2020_03_03_051644_create_sys_users_table',
                'batch' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'migration' => '2020_08_21_064303_create_spotlist_packages_table',
                'batch' => 1,
            ),
            9 => 
            array (
                'id' => 10,
                'migration' => '2020_08_21_065020_create_spotlist_user_subscription_table',
                'batch' => 1,
            ),
            10 => 
            array (
                'id' => 11,
                'migration' => '2020_08_21_070836_create_spotlist_subscription_purchase_history_table',
                'batch' => 1,
            ),
            11 => 
            array (
                'id' => 12,
                'migration' => '2020_08_21_071557_create_spotlist_rating_quality_table',
                'batch' => 1,
            ),
            12 => 
            array (
                'id' => 13,
                'migration' => '2020_08_21_072007_create_spotlist_offline_payment_table',
                'batch' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'migration' => '2020_08_21_072722_create_spotlist_features_table',
                'batch' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'migration' => '2020_08_21_073226_create_spotlist_directory_table',
                'batch' => 1,
            ),
            15 => 
            array (
                'id' => 16,
                'migration' => '2020_08_21_074723_create_spotlist_directory_vs_features_table',
                'batch' => 1,
            ),
            16 => 
            array (
                'id' => 17,
                'migration' => '2020_08_21_080031_create_spotlist_category_table',
                'batch' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'migration' => '2020_08_21_080354_create_spotlist_directory_vs_category_table',
                'batch' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'migration' => '2020_08_21_080857_create_spotlist_directory_type_shop_table',
                'batch' => 1,
            ),
            19 => 
            array (
                'id' => 20,
                'migration' => '2020_08_21_081726_create_spotlist_directory_type_restaurant_table',
                'batch' => 1,
            ),
            20 => 
            array (
                'id' => 21,
                'migration' => '2020_08_21_091821_create_spotlist_directory_type_hotel_room_table',
                'batch' => 1,
            ),
            21 => 
            array (
                'id' => 22,
                'migration' => '2020_08_21_092309_create_spotlist_directory_type_fitness_table',
                'batch' => 1,
            ),
            22 => 
            array (
                'id' => 23,
                'migration' => '2020_08_21_094525_create_spotlist_directory_type_beauty_table',
                'batch' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'migration' => '2020_08_21_094911_create_spotlist_directory_schedule_table',
                'batch' => 1,
            ),
            24 => 
            array (
                'id' => 25,
                'migration' => '2020_08_21_095654_create_spotlist_directory_gallary_table',
                'batch' => 1,
            ),
            25 => 
            array (
                'id' => 26,
                'migration' => '2020_08_21_101205_create_spotlist_countries_table',
                'batch' => 1,
            ),
            26 => 
            array (
                'id' => 27,
                'migration' => '2020_08_21_104229_create_spotlist_cities_table',
                'batch' => 1,
            ),
            27 => 
            array (
                'id' => 28,
                'migration' => '2020_08_21_110028_create_spotlist_booking_request_table',
                'batch' => 1,
            ),
            28 => 
            array (
                'id' => 29,
                'migration' => '2020_08_24_060426_create_spotlist_blog_table',
                'batch' => 1,
            ),
            29 => 
            array (
                'id' => 30,
                'migration' => '2020_09_03_054146_create_spotlist_directory_reviews_table',
                'batch' => 1,
            ),
            30 => 
            array (
                'id' => 31,
                'migration' => '2020_09_08_123713_create_spotlist_directory_wishinglist_table',
                'batch' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'migration' => '2020_09_09_130620_create_spotlist_directory_type_like_table',
                'batch' => 1,
            ),
            32 => 
            array (
                'id' => 33,
                'migration' => '2020_09_11_093946_create_spotlist_user_follower_table',
                'batch' => 1,
            ),
            33 => 
            array (
                'id' => 34,
                'migration' => '2020_09_11_095525_create_spotlist_user_subscription_by_email_table',
                'batch' => 1,
            ),
            34 => 
            array (
                'id' => 35,
                'migration' => '2020_09_11_134351_create_spotlist_user_message_table',
                'batch' => 1,
            ),
            35 => 
            array (
                'id' => 36,
                'migration' => '2020_10_01_130706_create_jobs_table',
                'batch' => 1,
            ),
            36 => 
            array (
                'id' => 37,
                'migration' => '2020_10_12_071850_create_sys_users_additional_info',
                'batch' => 1,
            ),
            37 => 
            array (
                'id' => 38,
                'migration' => '2020_10_12_072325_create_spotlisting_teams',
                'batch' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'migration' => '2020_10_12_074928_create_spotlist_how_it_works',
                'batch' => 1,
            ),
            39 => 
            array (
                'id' => 40,
                'migration' => '2020_10_12_075247_create_spotlist_faqs',
                'batch' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'migration' => '2020_10_13_131722_create_user_verification_codes_table',
                'batch' => 2,
            ),
        ));

        
    }
}