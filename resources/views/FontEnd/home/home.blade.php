@extends('FontEnd.master',['menu'=>'home'])
@section('content')
    <!-- start of hero -->
    @include('FontEnd.home.slider')
    <!-- end of hero slider -->
    <!-- serch-area-start -->
    @include('FontEnd.home.search')
    <!-- serch-area-end -->
    @if(isset($featured_category) && !empty($featured_category[0]))
    <!-- .featured-area-start -->
        @include('FontEnd.home.feature_categories')
    <!-- .featured-area-start -->
    @endif
    @if(isset($popular_location) && !empty($popular_location[0]))
    <!-- .popular-area-start -->
        @include('FontEnd.home.popular')
    <!-- .popular-area-start -->
    @endif
    @if(isset($most_popular_deals) && !empty($most_popular_deals[0]))
    <!-- .deals-area-start -->
        @include('FontEnd.home.deals')
    <!-- .popular-area-start -->
    @endif
    @if(isset($recent_lists) && !empty($recent_lists[0]))
    <!-- .recent-area-start -->
        @include('FontEnd.home.recent_activity')
    <!-- .recent-area-start -->
    @endif
    @if(isset($how_it_works) && !empty($how_it_works[0]))
    <!-- work-area start -->
        @include('FontEnd.home.how_it_works')
    <!-- work-area end -->
    @endif

    @if(isset($recent_reviews) && !empty($recent_reviews[0]))
    <!-- testimonial-area start -->
        @include('FontEnd.home.testimonial')
    <!-- testimonial-area end -->
    @endif

    @if(isset($featured_articles) && !empty($featured_articles[0]))
        @include('FontEnd.home.featured_article')
    @endif
    <!-- cta-area start -->
    <div class="cta-area section-padding">
        <div class="container">
            <div class="cta-wrap">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cta-text">
                            <h2>Do You Have Questions ?</h2>
                            <div class="btns">
                                <a href="{{route('user.contact')}}" class="btn theme-btn">Get In Touch</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- cta-area end -->
@endsection
