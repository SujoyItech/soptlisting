@if(isset($beauty_data) && !empty($beauty_data[0]))
    @foreach($beauty_data as $beauty)
        <div class="row mb-5 heading">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Service name')}}</label>
                            <input type="text" name="service_name[]" value="{{$beauty->service_name ?? ''}}"
                                   placeholder="Enter shop name" class="form-control">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Service price')}} ( {{get_currency()}} ) </label>
                            <input type="number" class="form-control" name="service_price[]" value="{{$beauty->service_price ?? ''}}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Description ( Max length 500 word )')}}</label>
                            <textarea type="text" name="service_description[]" class="form-control" maxlength="500">{{$beauty->service_description ?? ''}}</textarea>
                        </div>
                    </div>

                    <div class="col-12">
                        <input type="hidden" name="spotlist_directory_type_beauty_id[]" value="{{$beauty->spotlist_directory_type_beauty_id ?? ''}}">
                        <button class="btn btn-danger remove_info" type="button"
                                data-id="{{$beauty->spotlist_directory_type_beauty_id ?? ''}}" data-table="spotlist_directory_type_beauty" data-primary-key="spotlist_directory_type_beauty_id">
                            <i class="fa fa-minus"></i> {{__('Remove')}}</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="col-form-label">{{__('Service image')}}</label>
                    <input type="file" name="service_image[]" data-plugins="dropify" class="dropify"
                           data-default-file="{{isset($beauty->service_image) ? asset($beauty->service_image) : ''}}"
                           data-allowed-file-extensions="png jpg jpeg jfif"
                           data-max-file-size="1M" />
                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                </div>
            </div>
        </div>
    @endforeach
@endif
<div class="beauty-list">
    <div class="row mb-5">
        <div class="col-lg-9">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Service name')}}</label>
                        <input type="text" name="service_name[]" value=""
                               placeholder="Enter shop name" class="form-control">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Service price')}} ( {{get_currency()}} )</label>
                        <input type="number" class="form-control" name="service_price[]">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Description ( Max length 500 word )')}}</label>
                        <textarea type="text" name="service_description[]" class="form-control" maxlength="500"></textarea>
                    </div>
                </div>

                <div class="col-12">
                    <button class="btn btn-info add_more_beauty" type="button"><i class="fa fa-plus"></i> {{__('Add more')}}</button>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="col-form-label">{{__('Service image')}}</label>
                <input type="file" name="service_image[]" data-plugins="dropify" class="dropify"
                       data-default-file=""
                       data-allowed-file-extensions="png jpg jpeg jfif"
                       data-max-file-size="1M" />
                <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
            </div>
        </div>
    </div>
</div>



<script>

    $(document).on('click','.add_more_beauty',function (){

        var beauty_html = '<div class="row mb-5 beauty_heading">\n' +
            '    <div class="col-lg-9">\n' +
            '        <div class="row">\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Service name')}}</label>\n' +
            '                    <input type="text" name="service_name[]" parsley-trigger="change" required value=""\n' +
            '                           placeholder="Enter shop name" class="form-control">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Service price')}}({{get_currency()}})</label>\n' +
            '                    <input type="number" class="form-control" name="service_price[]">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Description ( Max length 500 word )')}}</label>\n' +
            '                    <textarea type="text" name="service_description[]" class="form-control" maxlength="500"></textarea>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '\n' +
            '\n' +
            '            <div class="col-12">\n' +
            '                <button class="btn btn-danger remove_beauty" type="button"><i class="fa fa-minus"></i> {{__('Remove')}}</button>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '    <div class="col-lg-3">\n' +
            '        <div class="form-group">\n' +
            '            <label class="col-form-label">{{__('Service image')}}</label>\n' +
            '            <input type="file" name="service_image[]" data-plugins="dropify" class="dropify"\n' +
            '                   data-default-file=""\n' +
            '                   data-allowed-file-extensions="png jpg jpeg jfif"\n' +
            '                   data-max-file-size="1M" />\n' +
            '            <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';

        $('.beauty-list').append(beauty_html);
        $('.dropify').dropify();
    });

    $(document).on('click','.remove_beauty',function (e) {
        e.preventDefault();
        $(this).closest('.beauty_heading').remove();
    })
</script>

