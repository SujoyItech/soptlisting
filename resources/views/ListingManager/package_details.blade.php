@extends('Backend.layouts.app')
@section('content')
    @php($settings = __options(['payment_settings']))
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa fa-credit-card"></i> {{__('Pricing')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="card-box bg-pattern">
                <div class="text-center">
                    <h4 class="mb-1 font-18">{{$packages->spotlist_packages_name}}</h4>
                    @if($packages->packages_type === 'Free')
                        <span class="badge badge-success">{{$packages->packages_type}}</span>
                    @else
                        <p class="text-muted  font-14">{{$packages->price}}({{get_currency()}})</p>
                        <p class="text-muted  font-14">( {{$packages->day_validity}} {{__('Days')}} )</p>
                    @endif
                    @if($packages->is_recomended === TRUE)
                        <span class="badge badge-secondary">{{__('Recommended')}}</span>
                    @endif
                </div>

                <p class="font-14 text-center text-muted">
                    {{$packages->description}}
                </p>

                <div class="text-center">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">{{__('Num of listing:')}} {{$packages->num_of_listing}}</li>
                        <li class="list-group-item">{{__('Num of photos:')}} {{$packages->num_of_photos}}</li>
                        <li class="list-group-item">{{__('Video ability:')}} {{$packages->add_video_ability == TRUE ? 'Video enabled' : 'Video disabled'}}</li>
                        <li class="list-group-item">{{__('Is featured:')}} {{$packages->is_featured == TRUE ? 'Featured' : 'Not featured'}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-4">
            @if($packages->packages_type === 'Paid')
                @if(isset($settings->payment_method))
                    @if($settings->payment_method == 'stripe')
                        <div class="stripe-payment">
                            @include('ListingManager.payment.stripe')
                        </div>
                    @elseif($settings->payment_method == 'brain_tree')
                        <div class="braintree-payment">
                            @include('ListingManager.payment.brain_tree')
                        </div>
                    @endif
                @endif
            @else
                <div class="free">
                    @include('ListingManager.payment.free')
                </div>
            @endif

        </div>

    </div>
@endsection
@section('script')

@endsection
