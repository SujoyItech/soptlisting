@if(isset($notifications) &&!empty($notifications[0]))
    @foreach($notifications as $notify)
        @php($notify_body = json_decode($notify->body))
        @if($notify->type == LISTING_MANAGER_ADMIN_BOOKING)
        <!-- item-->
        <a href="{{route('user-booking-request',['type'=>$notify_body->type ?? 'all','booking_id'=>$notify_body->id ?? '']) }}" class="dropdown-item notify-item" data-id="{{$notify->id}}">
            <div class="notify-icon">
                <img src="{{user_image($notify_body->user_image)}}" class="rounded-circle" alt="" width="40" height="40"/>
            </div>
            <p class="notify-details">
                {{$notify_body->username ?? ''}}
            </p>
            <p class="text-muted mb-0 user-msg">
                <small class="{{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}}">{{$notify_body->title}}</small><br>
                <small class="text-muted">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</small>
            </p>
        </a>
        @elseif($notify->type == ADMIN_DIRECTORY || $notify->type == LISTING_MANAGER_DIRECTORY)
            <a href="{{ \Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER ? route('list-manager-directory-list',['directory_id'=>$notify_body->directory_id ?? '']) : route('directory-list',['directory_id'=>$notify_body->directory_id ?? ''])}}" class="dropdown-item notify-item" data-id="{{$notify->id}}">
                <div class="notify-icon">
                    <img src="{{getImage($notify_body->directory_image ?? '','directory')}}" class="rounded-circle" alt="" width="40" height="40" />
                </div>
                <p class="notify-details {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}}">{{$notify_body->title ?? ''}}
                    <small class="text-muted">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</small>
                </p>
            </a>
        @elseif($notify->type == WISHING)
            <a href="{{ \Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER ? route('list-manager-wishing-list') : route('admin-wishing-list')}}" class="dropdown-item notify-item" data-id="{{$notify->id}}">
                <div class="notify-icon">
                    <img src="{{user_image($notify_body->user_image)}}" class="rounded-circle" alt="" width="40" height="40"/>
                </div>
                <p class="notify-details">
                    {{$notify_body->username ?? ''}}
                </p>
                <p class="text-muted mb-0 user-msg">
                    <small class="{{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}}">{{$notify_body->title}}</small><br>
                    <small class="text-muted">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</small>
                </p>
            </a>
        @elseif($notify->type == SUBSCRIPTION_NOTIFICATION)
            <a href="{{\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER ? url('listing-manager-subscription') : url('admin-payment-history')}}" class="dropdown-item notify-item" data-id="{{$notify->id}}">
                <div class="notify-icon">
                    <img src="{{user_image($notify_body->user_image)}}" class="rounded-circle" alt="" width="40" height="40"/>
                </div>
                <p class="notify-details">
                    {{$notify_body->username ?? ''}}
                </p>
                <p class="text-muted mb-0 user-msg">
                    <small class="{{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}}">{{$notify_body->title}}</small><br>
                    <small class="text-muted">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</small>
                </p>
            </a>
        @endif
    @endforeach
@else
    <p class="text-center">{{__('No new notifications')}}</p>
@endif
