<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistCategorySeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_category')->delete();
        
        \DB::table('spotlist_category')->insert(array (
            0 => 
            array (
                'spotlist_category_id' => 1,
                'spotlist_category_name' => 'General',
                'parent_id' => 0,
                'category_image' => 'Backend/images/category/QnX8JIf0Swgn2Fxh.png',
                'category_icon' => NULL,
                'status' => 'Active',
            ),
            1 => 
            array (
                'spotlist_category_id' => 2,
                'spotlist_category_name' => 'Hotel',
                'parent_id' => 0,
                'category_image' => 'Backend/images/category/KXeMEHmxvOn0J2mR.jpg',
                'category_icon' => 'fa fa-shopping-cart',
                'status' => 'Active',
            ),
            2 => 
            array (
                'spotlist_category_id' => 3,
                'spotlist_category_name' => 'Restaurant',
                'parent_id' => 0,
                'category_image' => 'Backend/images/category/LBRf0iyXAmSxQpNt.jpg',
                'category_icon' => 'fa fa-female',
                'status' => 'Active',
            ),
            3 => 
            array (
                'spotlist_category_id' => 4,
                'spotlist_category_name' => 'Beauty',
                'parent_id' => 0,
                'category_image' => 'Backend/images/category/N5P2YiPqSTxFWgAk.jpg',
                'category_icon' => 'fa-fa-restaurant',
                'status' => 'Active',
            ),
            4 => 
            array (
                'spotlist_category_id' => 5,
                'spotlist_category_name' => 'Shopping',
                'parent_id' => 0,
                'category_image' => 'Backend/images/category/EzwRO6BlYO3svgSH.jpg',
                'category_icon' => NULL,
                'status' => 'Active',
            ),
            5 => 
            array (
                'spotlist_category_id' => 6,
                'spotlist_category_name' => 'Fitness',
                'parent_id' => 0,
                'category_image' => 'Backend/images/category/zoYWZMDjwkgzu85a.jpg',
                'category_icon' => NULL,
                'status' => 'Active',
            ),
        ));

        
    }
}