<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistUserSubscriptionSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_user_subscription')->delete();
        
        \DB::table('spotlist_user_subscription')->insert(array (
            0 => 
            array (
                'spotlist_user_subscription_id' => 1,
                'spotlist_packages_id' => 1,
                'user_id' => 5,
                'day_remain' => 10,
                'num_of_listing' => 2,
                'listing_item_used' => 2,
                'num_of_categories' => NULL,
                'category_used' => 0,
                'num_of_tags' => 1,
                'tag_used' => 0,
                'num_of_photos' => 10,
                'photos_used' => 7,
                'add_video_ability' => 1,
                'add_contact_form_ability' => 1,
                'free_subscription_check' => 1,
                'status' => 'Active',
            ),
        ));

        
    }
}