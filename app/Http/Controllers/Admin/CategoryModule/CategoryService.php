<?php


namespace App\Http\Controllers\Admin\CategoryModule;


use App\Traits\DataSaveTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryService {
    use DataSaveTrait;

    public function adminSubCategorySave(Request $request){
        $post_data = $request->except('spotlist_category_id','_token','category_image');
        if ($request->has('spotlist_category_id')){
            $category_data = DB::table('spotlist_category')->where('spotlist_category_id',$request->spotlist_category_id)->first();
        }
        if ($request->has('category_image')){
            $post_data['category_image'] = fileUpload($request->category_image,'Backend/images/category/',NULL,$category_data->category_image ?? '');
        }

        $response = $this->insertOrUpdateTable('spotlist_category','spotlist_category_id',$category_data->spotlist_category_id ?? '',$post_data);
        return $response;
    }

    public function getSubCategoryList(){
        $categoryList = DB::table('spotlist_category')
                          ->select('spotlist_category.*', 'parent_category.spotlist_category_name as parent_name')
                          ->leftjoin('spotlist_category as parent_category', 'parent_category.spotlist_category_id', '=', 'spotlist_category.parent_id')
                          ->where('spotlist_category.parent_id','<>',0)
                          ->get();
        return $categoryList;
    }


    public function getCategoryData(){
        $categories = DB::table('spotlist_category')->where(['parent_id'=>'0'])->get();
        $category_array = [];
        foreach ($categories as $key=>$category) {
            $category_array[$key]['spotlist_category_id'] = $category->spotlist_category_id;
            $category_array[$key]['spotlist_category_name'] = $category->spotlist_category_name;
            $category_array[$key]['category_image'] = $category->category_image;
            $category_array[$key]['category_icon'] = $category->category_icon;
            $category_array[$key]['status'] = $category->status;
            $category_array[$key]['subcategories'] = DB::table('spotlist_category')->where('parent_id',$category->spotlist_category_id)->get()->toArray();
        }
        return $category_array;
    }

    public function removeChildCategory(Request $request){
        try {
            DB::table('spotlist_category')->where('spotlist_category_id',$request->subcategory_id)->update(['parent_id'=>NULL]);
            $response = $this->responseData(TRUE,'Sub category deleted successfully.');
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,'Sub category deleted failed.');
        }
        return $response;
    }

    public function adminMainCategoryStatusChange(Request $request){
        if ($request->status == 'Active'){
            $success_message = __('Category disabled successfully.');
            $warning_message = __('Category disable failed.');
            $response = $this->updateTable('spotlist_category',['status'=>'Inactive'],'spotlist_category_id',$request->spotlist_category_id,$success_message,$warning_message);
        }else{
            $success_message = __('Category enabled successfully.');
            $warning_message = __('Category enabled failed.');
            $response = $this->updateTable('spotlist_category',['status'=>'Active'],'spotlist_category_id',$request->spotlist_category_id,$success_message,$warning_message);
        }
        return $response;
    }

    public function categoryPictureChange(Request $request){
        $category = DB::table('spotlist_category')->where('spotlist_category_id',$request->spotlist_category_id)->first();
        $post_data['category_image'] = fileUpload($request->category_image,'Backend/images/category/','',$category->category_image ?? '');
        $response = $this->updateTable('spotlist_category',$post_data,'spotlist_category_id',$request->spotlist_category_id);
        return $response;
    }

    public function getCityData($id = NULL){
        $data = [];
        if ($id != NULL){
            $data['city'] = DB::table('spotlist_cities')->where('spotlist_cities_id',$id)->first();
        }
        return $data;
    }
    public function getCityWithCountryData($id = NULL){
        $data = [];
        $query = DB::table('spotlist_cities')
          ->leftJoin('spotlist_countries','spotlist_cities.country_id','spotlist_countries.id')
          ->select('spotlist_cities.*','spotlist_countries.name as spotlist_country_name');
        $data = $id != NULL ? $query->where('spotlist_cities_id',$id)->first() : $query->get();
        return $data;
    }

    public function adminCitySave(Request $request ){
        $post_data = $this->adminCityData($request);
        $response = $this->insertOrUpdateTable('spotlist_cities','spotlist_cities_id',$request->spotlist_cities_id ?? '',$post_data);
        return $response;
    }

    public function checkCityUsedInDirectory(Request $request ){
        $directory = DB::table('spotlist_directory')->where('city',$request->id)->get();
        if (isset($directory) && !empty($directory[0])){
            $response['success'] = FALSE;
            $response['message'] = __('This city is used in directory.');
        }else{
            $response['success'] = TRUE;
            $response['message'] = __('This city is not used in directory.');
        }
        return $response;
    }

    private function adminCityData($request){
        $post_data = $request->all();
        if ($request->has('city_image')){
            if (isset($request->spotlist_cities_id) && !empty($request->spotlist_cities_id)){
                $city_data = DB::table('spotlist_cities')->where('spotlist_cities_id',$request->spotlist_cities_id)->first();
            }
            $post_data['city_image'] = fileUpload($request->city_image,'Backend/images/city/',NULL,$city_data->city_image ?? '');
        }
        return $post_data;
    }

    public function getFeaturesData($id = NULL){
        return $id != NULL ? DB::table('spotlist_features')->where('spotlist_features_id',$id)->first() : DB::table('spotlist_features')->get();
    }

    public function spotlistFeaturesSave(Request $request ){
        $post_data = $request->all();
        if ($request->has('spotlist_features_icon')){
            if (isset($request->spotlist_features_id) && !empty($request->spotlist_features_id)){
                $features_data = DB::table('spotlist_features')->where('spotlist_features_id',$request->spotlist_features_id)->first();
            }
            $post_data['spotlist_features_icon'] = fileUpload($request->spotlist_features_icon,'Backend/images/features/',NULL,$features_data->spotlist_features_icon ?? '');
        }
        $response = $this->insertOrUpdateTable('spotlist_features','spotlist_features_id',$request->spotlist_features_id ?? '',$post_data);
        return $response;
    }

    public function getFaqsData($id = NULL){
        return $id != NULL ? DB::table('spotlist_faqs')->where('id',$id)->first() : DB::table('spotlist_faqs')->get();
    }

    public function spotlistFaqsSave(Request $request ){
        $post_data = $request->all();
        $response = $this->insertOrUpdateTable('spotlist_faqs','id',$request->id ?? '',$post_data);
        return $response;
    }

    public function getDirectoryListByCategory(){
        $total_categories = DB::table('spotlist_category')
            ->select('spotlist_category.spotlist_category_name',DB::raw("COUNT(spotlist_directory.spotlist_directory_id) AS total_directory"))
            ->leftJoin('spotlist_directory','spotlist_category.spotlist_category_id','=','spotlist_directory.spotlist_parent_category_id')
            ->where(['spotlist_category.parent_id'=>0,'spotlist_category.status'=>'Active'])
            ->groupBy('spotlist_category.spotlist_category_id')
            ->get();
        $data = [];
        $data['category_name'] = $total_categories->pluck('spotlist_category_name')->toArray();
        $data['total_directory'] = $total_categories->pluck('total_directory')->toArray();
        return $data;
    }

}
