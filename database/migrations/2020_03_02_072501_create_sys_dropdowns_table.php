<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSysDropdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_dropdowns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dropdown_slug',100)->nullable();
            $table->enum('dropdown_mode',['dropdown','dropdown_grid'])->default('dropdown');
            $table->string('sys_search_panel_slug',100)->nullable();
            $table->text('sqltext')->nullable();
            $table->text('sqlsource')->nullable();
            $table->text('sqlcondition')->nullable();
            $table->text('sqlgroupby')->nullable();
            $table->text('sqlhaving')->nullable();
            $table->text('sqlorderby')->nullable();
            $table->integer('sqllimit')->nullable();
            $table->string('value_field',50)->nullable();
            $table->string('option_field',100)->nullable();
            $table->boolean('multiple')->nullable();
            $table->text('search_columns')->nullable();
            $table->string('dropdown_name',100)->nullable();
            $table->text('description')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->enum('status',['Active','Inactive'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_dropdowns');
    }
}
