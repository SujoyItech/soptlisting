<div class="dropdown caret message">
    <button aria-expanded="false" aria-haspopup="true" class="btn dropdown-toggle notify-expand" data-toggle="dropdown" id="dropdownMenuButton" type="button">
                                    <span class="icon-circle">
                                        <span class="signal-amount" id="count_notification">{{countUserUnreadNotification(\Illuminate\Support\Facades\Auth::user()->id)}}</span>
                                        <i class="fa fa-bell"></i>
                                    </span>
    </button>
    <div aria-labelledby="dropdownMenuButton" class="dropdown-menu dropdown-menu-right" style="">
        <div class="card" style="min-width: 18rem;">
            <div class="card-header">
                <span class="notify-head"><i class="fa fa-bell"></i> {{__('Notifications:')}} <strong id="notification_body">{{countUserUnreadNotification(\Illuminate\Support\Facades\Auth::user()->id)}}</strong> </span>
                <h5><a href="{{route('clear-all-user-notification')}}" class="text-dark">
                        <small>{{__('Clear All')}}</small>
                    </a>
                </h5>
            </div>
            <div class="notification-content">

            </div>

            <div class="card-footer">
                <a href="{{route('user-all-notification')}}">{{__('all notification')}}</a>
            </div>
        </div>
    </div>
</div>
<script src="https://js.pusher.com/3.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/laravel-echo/1.8.1/echo.iife.min.js"></script>
<script>
    Pusher.logToConsole = true;
    window.Echo = new Echo({
        broadcaster: 'pusher',
        wsHost: window.location.hostname,
        wsPort: 6001,
        wssPort: 443,
        key: 'test',
        cluster: 'mt1',
        encrypted: false,
        disableStats: true
    });
</script>
<script>

    Pusher.logToConsole = true;
    var channel_name = 'user_'+{{\Illuminate\Support\Facades\Auth::user()->id}};
    Echo.channel(channel_name).listen('.user_notification', (response) => {
        notifyIconUpdate();
        makeToast(response.body, response.title);
    });

    function makeToast(title, body) {
        toastr.success(title, body);
    }

    function notifyIconUpdate() {
        var count = $('#count_notification').html() == '' ? 0 : $('#count_notification').html();
        var new_count = parseInt(count) + 1;
        $('#count_notification').html(new_count);
        $('#notification_body').html(new_count);
        $('#count_notification').show();
        $('#count_notification').addClass('notifying');
        setTimeout(function () {
            $('#count_notification').removeClass('notifying')
        }, 1000);
    }

    $(document).on("click", ".notify-expand", function (event) {
        event.preventDefault();
        $( ".notification-content" ).load("{{route('load-user-notification-body')}}");
    });

</script>
