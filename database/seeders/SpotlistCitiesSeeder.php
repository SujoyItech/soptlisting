<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistCitiesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_cities')->delete();
        
        \DB::table('spotlist_cities')->insert(array (
            0 => 
            array (
                'spotlist_cities_id' => 1,
                'spotlist_cities_name' => 'New York',
                'city_image' => 'Backend/images/city/MHCFbYetXS6SphGD.jpg',
                'country_id' => 236,
                'created_at' => NULL,
                'created_by' => 2,
                'status' => 'Active',
            ),
            1 => 
            array (
                'spotlist_cities_id' => 2,
                'spotlist_cities_name' => 'Washington',
                'city_image' => 'Backend/images/city/BkH40eB5Ri6xHku3.jpg',
                'country_id' => 236,
                'created_at' => NULL,
                'created_by' => 2,
                'status' => 'Active',
            ),
            2 => 
            array (
                'spotlist_cities_id' => 3,
                'spotlist_cities_name' => 'London',
                'city_image' => 'Backend/images/city/9UUWbQ7pBJbYHOcI.jpeg',
                'country_id' => 235,
                'created_at' => NULL,
                'created_by' => 2,
                'status' => 'Active',
            ),
            3 => 
            array (
                'spotlist_cities_id' => 4,
                'spotlist_cities_name' => 'Brooklyn',
                'city_image' => 'Backend/images/city/mzaRuxqFJM8tUT7R.jpg',
                'country_id' => 236,
                'created_at' => NULL,
                'created_by' => 2,
                'status' => 'Active',
            ),
            4 => 
            array (
                'spotlist_cities_id' => 5,
                'spotlist_cities_name' => 'Queens',
                'city_image' => 'Backend/images/city/z4zTAzyoVUGZ0HWu.jpg',
                'country_id' => 236,
                'created_at' => NULL,
                'created_by' => 2,
                'status' => 'Active',
            ),
            5 => 
            array (
                'spotlist_cities_id' => 6,
                'spotlist_cities_name' => 'Paris',
                'city_image' => 'Backend/images/city/FT5ThP09mcQZVhEB.jpg',
                'country_id' => 76,
                'created_at' => NULL,
                'created_by' => 2,
                'status' => 'Active',
            ),
        ));

        
    }
}