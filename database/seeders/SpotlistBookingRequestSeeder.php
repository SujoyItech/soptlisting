<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistBookingRequestSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_booking_request')->delete();
        
        \DB::table('spotlist_booking_request')->insert(array (
            0 => 
            array (
                'spotlist_booking_request_id' => 1,
                'spotlist_directory_id' => 1,
                'customer_id' => 4,
                'reservation_for' => 'restaurant',
                'reservation_contact' => '01771010093',
                'adult_guest' => 2,
                'child_guest' => 1,
                'request_time' => '12.50PM',
                'reservation_start_time' => '2020-10-13 15:20:22',
                'reservation_end_time' => NULL,
                'reservation_note' => NULL,
                'status_message' => NULL,
                'message' => NULL,
                'spotlist_directory_type_hotel_room_id' => NULL,
                'spotlist_directory_type_restaurant_id' => NULL,
                'spotlist_directory_type_beauty_id' => NULL,
                'spotlist_directory_type_fitness_id' => NULL,
                'spotlist_directory_type_shop_id' => NULL,
                'status' => 'Requested',
                'created_at' => '2020-10-13 15:20:24',
                'updated_at' => '2020-10-13 15:20:22',
                'booking_id' => NULL,
            ),
            1 => 
            array (
                'spotlist_booking_request_id' => 2,
                'spotlist_directory_id' => 1,
                'customer_id' => 4,
                'reservation_for' => 'restaurant',
                'reservation_contact' => '01771010093',
                'adult_guest' => 2,
                'child_guest' => 1,
                'request_time' => '12.50PM',
                'reservation_start_time' => '2020-10-13 15:20:22',
                'reservation_end_time' => NULL,
                'reservation_note' => NULL,
                'status_message' => NULL,
                'message' => '[{"time": "2020-10-13T09:54:24.868051Z", "type": "user", "message": "Hello"}]',
                'spotlist_directory_type_hotel_room_id' => NULL,
                'spotlist_directory_type_restaurant_id' => NULL,
                'spotlist_directory_type_beauty_id' => NULL,
                'spotlist_directory_type_fitness_id' => NULL,
                'spotlist_directory_type_shop_id' => NULL,
                'status' => 'Canceled',
                'created_at' => '2020-10-13 15:20:26',
                'updated_at' => '2020-10-13 08:52:11',
                'booking_id' => NULL,
            ),
            2 => 
            array (
                'spotlist_booking_request_id' => 3,
                'spotlist_directory_id' => 1,
                'customer_id' => 4,
                'reservation_for' => 'restaurant',
                'reservation_contact' => '01771010093',
                'adult_guest' => 2,
                'child_guest' => 1,
                'request_time' => '11.50 PM',
                'reservation_start_time' => NULL,
                'reservation_end_time' => NULL,
                'reservation_note' => 'Some text will be written here',
                'status_message' => NULL,
                'message' => NULL,
                'spotlist_directory_type_hotel_room_id' => NULL,
                'spotlist_directory_type_restaurant_id' => NULL,
                'spotlist_directory_type_beauty_id' => NULL,
                'spotlist_directory_type_fitness_id' => NULL,
                'spotlist_directory_type_shop_id' => NULL,
                'status' => 'Requested',
                'created_at' => '2020-10-13 09:24:52',
                'updated_at' => '2020-10-13 09:24:52',
                'booking_id' => NULL,
            ),
            3 => 
            array (
                'spotlist_booking_request_id' => 4,
                'spotlist_directory_id' => 1,
                'customer_id' => 4,
                'reservation_for' => 'restaurant',
                'reservation_contact' => '01771010093',
                'adult_guest' => 2,
                'child_guest' => 2,
                'request_time' => '12.00 PM',
                'reservation_start_time' => NULL,
                'reservation_end_time' => NULL,
                'reservation_note' => 'sdfsdf sdfdsf',
                'status_message' => NULL,
                'message' => NULL,
                'spotlist_directory_type_hotel_room_id' => NULL,
                'spotlist_directory_type_restaurant_id' => NULL,
                'spotlist_directory_type_beauty_id' => NULL,
                'spotlist_directory_type_fitness_id' => NULL,
                'spotlist_directory_type_shop_id' => NULL,
                'status' => 'Requested',
                'created_at' => '2020-10-13 09:28:05',
                'updated_at' => '2020-10-13 09:28:05',
                'booking_id' => NULL,
            ),
            4 => 
            array (
                'spotlist_booking_request_id' => 5,
                'spotlist_directory_id' => 1,
                'customer_id' => 4,
                'reservation_for' => 'restaurant',
                'reservation_contact' => '01771010093',
                'adult_guest' => 2,
                'child_guest' => 1,
                'request_time' => '12.00 PM',
                'reservation_start_time' => '10/02/2020',
                'reservation_end_time' => NULL,
                'reservation_note' => 'sdf sdfdsgg dfddsf',
                'status_message' => NULL,
                'message' => NULL,
                'spotlist_directory_type_hotel_room_id' => NULL,
                'spotlist_directory_type_restaurant_id' => NULL,
                'spotlist_directory_type_beauty_id' => NULL,
                'spotlist_directory_type_fitness_id' => NULL,
                'spotlist_directory_type_shop_id' => NULL,
                'status' => 'Requested',
                'created_at' => '2020-10-13 09:32:20',
                'updated_at' => '2020-10-13 09:32:20',
                'booking_id' => NULL,
            ),
            5 => 
            array (
                'spotlist_booking_request_id' => 6,
                'spotlist_directory_id' => 2,
                'customer_id' => 4,
                'reservation_for' => 'hotel',
                'reservation_contact' => '01771010093',
                'adult_guest' => 2,
                'child_guest' => 1,
                'request_time' => NULL,
                'reservation_start_time' => '10/02/2020',
                'reservation_end_time' => '10/10/2020',
                'reservation_note' => 'Some text will be written',
                'status_message' => 'You have successfully complete you booking. Thanks',
                'message' => '[{"time": "2020-10-13T10:44:16.159242Z", "type": "manager", "message": "You have successfully complete you booking. Thanks"}]',
                'spotlist_directory_type_hotel_room_id' => 1,
                'spotlist_directory_type_restaurant_id' => NULL,
                'spotlist_directory_type_beauty_id' => NULL,
                'spotlist_directory_type_fitness_id' => NULL,
                'spotlist_directory_type_shop_id' => NULL,
                'status' => 'Completed',
                'created_at' => '2020-10-13 10:40:48',
                'updated_at' => '2020-10-13 10:40:48',
                'booking_id' => NULL,
            ),
            6 => 
            array (
                'spotlist_booking_request_id' => 7,
                'spotlist_directory_id' => 2,
                'customer_id' => 18,
                'reservation_for' => 'hotel',
                'reservation_contact' => '01771010093',
                'adult_guest' => 2,
                'child_guest' => 1,
                'request_time' => NULL,
                'reservation_start_time' => '10/01/2020',
                'reservation_end_time' => '10/09/2020',
                'reservation_note' => 'sdfsf',
                'status_message' => NULL,
                'message' => NULL,
                'spotlist_directory_type_hotel_room_id' => 1,
                'spotlist_directory_type_restaurant_id' => NULL,
                'spotlist_directory_type_beauty_id' => NULL,
                'spotlist_directory_type_fitness_id' => NULL,
                'spotlist_directory_type_shop_id' => NULL,
                'status' => 'Requested',
                'created_at' => '2020-10-14 05:11:37',
                'updated_at' => '2020-10-14 05:11:37',
                'booking_id' => NULL,
            ),
        ));

        
    }
}