<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistOfflinePaymentSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_offline_payment')->delete();
        

        
    }
}