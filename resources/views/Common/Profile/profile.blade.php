@extends('Backend.layouts.app',['menu'=>'profile'])
@section('content')

    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title"><i class="fa fa-user-circle"></i> {{__('Profile')}}</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                @include('Common.Profile.profile_card')

                <div class="col-lg-8 col-xl-8">
                     @include('Common.Profile.basic_settings')
                     @include('Common.Profile.password_settings')
                </div> <!-- end col -->
            </div>
            <!-- end row-->

        </div> <!-- container -->

    </div> <!-- content -->
@endsection

@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        $(document).ready(function () {
            $('#profile').addClass('active show');
            $('#profiletab').addClass('active');
        });
        $('.submit_info').on('click', function (e) {
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, null).done(function (response) {
                        //swalSuccess(response.message);
                        if (response.success == true){
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            if(response.data.type == 'error'){
                                swalError(response.message);
                            }else{
                                swalRedirect(redirect_url, response.message, 'error');
                            }
                        }
                    })
                }
            });
        });
    </script>
@endsection
