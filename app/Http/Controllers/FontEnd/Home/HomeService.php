<?php


namespace App\Http\Controllers\FontEnd\Home;


use App\Http\Controllers\FontEnd\FontEndService;
use App\Http\Controllers\FontEnd\Listing\ListingService;
use Illuminate\Support\Facades\DB;

class HomeService {

    public function homePageData(){
        $listing_service = new ListingService();
        $frontend_service = new FontEndService();
        $data = [];
        $spotlist_directory = DB::table('spotlist_directory')->where('status','Active')->get();
        if (isset($spotlist_directory) && !empty($spotlist_directory[0])){
            $data['featured_category'] = DB::table('spotlist_category')
                                           ->select('spotlist_category.spotlist_category_id','spotlist_category.spotlist_category_name','spotlist_category.category_image',
                                               DB::raw("COUNT(spotlist_category.spotlist_category_id) as total_listing"))
                                           ->join('spotlist_directory','spotlist_category.spotlist_category_id','spotlist_directory.spotlist_parent_category_id')
                                           ->join('spotlist_cities','spotlist_directory.city','spotlist_cities.spotlist_cities_id')
                                           ->where(['spotlist_category.status'=>'Active','spotlist_directory.status'=>'Active','spotlist_cities.status'=>'Active'])
                                           ->groupBy('spotlist_category.spotlist_category_id')
                                           ->get();

            $data['popular_location'] = DB::table('spotlist_cities')
                                          ->select('spotlist_cities.spotlist_cities_id','spotlist_cities.spotlist_cities_name','spotlist_cities.spotlist_cities_slug','spotlist_cities.city_image',
                                              DB::raw("COUNT(spotlist_cities.spotlist_cities_id) as total_listing"))
                                          ->join('spotlist_directory','spotlist_cities.spotlist_cities_id','spotlist_directory.city')
                                          ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','spotlist_category.spotlist_category_id')
                                          ->where(['spotlist_cities.status'=>'Active','spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active'])
                                          ->groupBy('spotlist_cities.spotlist_cities_id')
                                          ->get();

            $data['recent_lists'] = $listing_service->getDirectoryList(NULL,NULL,6,[],['spotlist_directory.status'=>'Active']);
            $data['most_popular_deals'] = $this->mostVisitedList();
            $data['recent_reviews'] = DB::table('spotlist_directory_reviews')
                                        ->select('spotlist_directory_reviews.user_id','spotlist_directory_reviews.rating',
                                            'spotlist_directory_reviews.review','sys_users.username','sys_users.user_image','spotlist_directory_reviews.created_at')
                                        ->join('sys_users','spotlist_directory_reviews.user_id','=','sys_users.id')
                                        ->orderBy('spotlist_directory_reviews.created_at','desc')
                                        ->paginate(10);

            $data['featured_articles'] = $frontend_service->getBlog(NULL,3,TRUE)['blogs'];
            $data['how_it_works'] = DB::table('spotlist_how_it_works')->get();

        }
        $data['main_categories'] = DB::table('spotlist_category')->where(['parent_id'=> 0 ,'status'=>'Active'])->get();
        $data['locations'] = DB::table('spotlist_cities')->where(['status'=>'Active'])->get();

        return $data;
    }

    private function mostVisitedList(){
        $most_bookings = DB::table('spotlist_directory')
                           ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','=','spotlist_category.spotlist_category_id')
                           ->join('spotlist_cities','spotlist_directory.city','=','spotlist_cities.spotlist_cities_id')
                           ->select('spotlist_directory.spotlist_directory_id')
                           ->where(['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active'])
                           ->orderBy('spotlist_directory.booking_count','DESC')
                           ->limit(3);
        $most_visitings = DB::table('spotlist_directory')
                            ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','=','spotlist_category.spotlist_category_id')
                            ->join('spotlist_cities','spotlist_directory.city','=','spotlist_cities.spotlist_cities_id')
                            ->select('spotlist_directory.spotlist_directory_id')
                            ->where(['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active'])
                            ->orderBy('spotlist_directory.visit_count','DESC')
                            ->limit(3);
        $most_wishing = DB::table('spotlist_directory')
                          ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','=','spotlist_category.spotlist_category_id')
                          ->join('spotlist_cities','spotlist_directory.city','=','spotlist_cities.spotlist_cities_id')
                          ->select('spotlist_directory.spotlist_directory_id')
                          ->where(['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active'])
                          ->orderBy('spotlist_directory.wishing_count','DESC')
                          ->limit(3);
        $directory_id = $most_bookings->union($most_visitings)->union($most_wishing)->pluck('spotlist_directory_id')->toArray();
        $where_in[0] = 'spotlist_directory.spotlist_directory_id';
        $where_in[1] = $directory_id;
        $listing_service = new ListingService();
        return $listing_service->getDirectoryList(NULL,NULL,NULL,$where_in);
    }
}
