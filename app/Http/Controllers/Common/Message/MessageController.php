<?php

namespace App\Http\Controllers\Common\Message;

use App\Http\Controllers\Controller;
use App\Http\Service\NotificationService;
use App\Models\Message;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    private $service;

    public function __construct(){
        $this->service = new MessageService();
    }
    public function bookingRequestMessage(Request $request){
        $data['booking_request'] = DB::table('spotlist_booking_request')
            ->select('spotlist_booking_request.spotlist_booking_request_id', 'spotlist_booking_request.spotlist_directory_id', 'spotlist_booking_request.customer_id', 'users.username as user_name',
                'users.user_image as user_image', 'manager.username as manager_name', 'manager.user_image as manager_image',
                'spotlist_directory.sys_users_id', 'spotlist_booking_request.message')
            ->leftJoin('sys_users as users','spotlist_booking_request.customer_id', '=', 'users.id')
            ->leftJoin('spotlist_directory','spotlist_booking_request.spotlist_directory_id', '=', 'spotlist_directory.spotlist_directory_id')
            ->leftJoin('sys_users as manager','spotlist_directory.sys_users_id', '=', 'manager.id')
            ->where('spotlist_booking_request.spotlist_booking_request_id',$request->id)
            ->first();
        $data['messages'] = json_decode($data['booking_request']->message ?? '');
        return view('Common.Message.booking_request_message',$data);
    }

    public function bookingRequestMessageSend(Request $request){
       $response = $this->service->bookingRequestMessageSend($request);
       if ($response['success'] == TRUE){
           return redirect()->back()->with(['success'=>$response['message']]);
       }else{
           return redirect()->back()->with(['dismiss'=>$response['message']]);
       }
    }

    public function bookingStatusMessage(Request $request){
        $data['message'] = DB::table('spotlist_booking_request')
                                     ->select('spotlist_booking_request.status_message')
                                     ->where('spotlist_booking_request.spotlist_booking_request_id',$request->id)
                                     ->first();
        return view('Common.Message.booking_status_message',$data);
    }

    public function getAdminNotificationBody(){
        $user = Auth::user();
        $data['notifications'] = Notification::where(['user_id'=>Auth::user()->id])->where('is_cleared',STATUS_PENDING)->orderBy('created_at','desc')->limit(20)->get();
        return view('Backend.includes.notification_body',$data);
    }

    public function viewAllAdminNotification(){
        $data['notifications'] = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[LISTING_MANAGER_DIRECTORY,LISTING_MANAGER_ADMIN_BOOKING,ADMIN_DIRECTORY,WISHING,FOLLOWING,SUBSCRIPTION_NOTIFICATION])->orderBy('created_at','desc')->get();
        return view('Common.Notification.all_notification',$data);
    }

    public function notificationMakeSeen(Request $request){
        $notify_service = new NotificationService();
        $response = $notify_service->notificationMakeSeen($request);
        return response()->json($response);
    }

    public function clearAllAdminNotification(){
        $notify_service = new NotificationService();
        $response = $notify_service->clearAllNotification();
        if ($response['success'] == TRUE){
            return redirect()->back()->with(['success'=>$response['message']]);
        }else{
            return redirect()->back()->with(['dismiss'=>$response['message']]);
        }
    }

    public function getAdminMessageBody(){
        $data['messages'] = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->where('is_cleared',STATUS_PENDING)->orderBy('created_at','desc')->limit(20)->get();
        return view('Backend.includes.message_body',$data);
    }

    public function viewAllAdminMessage($type = NULL){
        $query = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION]);
        if ($type == 'read'){
            $query = $query->where('status',STATUS_SUCCESS);
        }elseif ($type == 'unread'){
            $query = $query->where('status',STATUS_PENDING);
        }
        $data['messages'] = $query->orderBy('created_at','desc')->paginate(20);
        $data['all_message_count'] = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->count();
        $data['read_message_count'] = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->where('status',STATUS_SUCCESS)->count();
        $data['unread_message_count'] = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->where('status',STATUS_PENDING)->count();
        $data['type'] = $type;

        return view('Common.Message.all_messages',$data);
    }

    public function viewMesageDetails($id){
        $data['message'] = Notification::where('id',$id)->first();
        $data['all_message_count'] = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->count();
        $data['read_message_count'] = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->where('status',STATUS_SUCCESS)->count();
        $data['unread_message_count'] = Notification::where(['user_id'=>Auth::user()->id])->whereIn('type',[MESSAGE_NOTIFICATION,GENERAL_MESSAGE_NOTIFICATION])->where('status',STATUS_PENDING)->count();
        return view('Common.Message.view_message',$data);
    }

    public function messageMakeSeen(Request $request){
        $notify_service = new NotificationService();
        $response = $notify_service->notificationMakeSeen($request);
        return response()->json($response);
    }

    public function clearAllAdminMessage(){
        $notify_service = new NotificationService();
        $response = $notify_service->clearAllMessage();
        if ($response['success'] == TRUE){
            return redirect()->back()->with(['success'=>$response['message']]);
        }else{
            return redirect()->back()->with(['dismiss'=>$response['message']]);
        }
    }


}
