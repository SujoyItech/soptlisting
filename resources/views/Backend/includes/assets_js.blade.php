<script src="{{asset('/Backend/libs/dropzone/min/dropzone.min.js')}}"></script>
<script src="{{asset('/Backend/libs/dropify/js/dropify.min.js')}}"></script>
<script src="{{asset('/Backend/js/pages/form-fileuploads.init.js')}}"></script>
<script src="{{asset('/Backend/js/LaraframeScript.js')}}"></script>
<script src="{{asset('/Backend/libs/morris.js06/morris.min.js')}}"></script>
<script src="{{asset('/Backend/libs/raphael/raphael.min.js')}}"></script>

<script src="{{asset('/Backend/')}}/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{{asset('/Backend/')}}/js/pages/form-wizard.init.js"></script>
<script src="{{asset('/Backend/')}}/libs/bootstrap-select/js/bootstrap-select.min.js"></script>
<script src="{{asset('/Backend/js/app.min.js')}}"></script>
<script src="{{asset('/Backend/')}}/libs/summernote/summernote-bs4.min.js"></script>
<script src="{{asset('/Backend/')}}/js/pages/form-summernote.init.js"></script>
<script src="{{asset('/Backend/libs/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('Backend/')}}/libs/toastr/build/toastr.min.js"></script>

<script>

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500,
        autoWidth: true,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    @if(Session::has('dismiss') && !empty(Session::get('dismiss')))
         Toast.fire({type: 'warning', text: '{{Session::get('dismiss')}}'});
    @endif
    @if(Session::has('success') && !empty(Session::get('success')))
         Toast.fire({type: 'success', text: '{{Session::get('success')}}'});
    @endif

</script>

@yield('script')
