<div class="row">
    <div class="col-12">
        <div class="card">
            @if(isset($hotel_data) && !empty($hotel_data))
                <a href="{{route('user.listing',['slug'=>$reservation_data->spotlist_directory_slug])}}">
                    <img class="card-img-top" src="{{getImage($hotel_data->room_image)}}" alt="Card image cap" height="250">
                </a>
                <div class="card-body">
                    <a href="{{route('user.listing',['slug'=>$reservation_data->spotlist_directory_slug])}}"><h4>{{$hotel_data->room_name}}</h4></a>
                    <h6>{{__('Booking ID #')}}{{$booking_id?? ''}}</h6>
                    @if(!empty($reservation_data->adult_guest))
                         <h6>{{__('Adult Guest :')}} {{$reservation_data->adult_guest}}</h6>
                    @endif
                    @if(!empty($reservation_data->child_guest))
                        <h6>{{__('Child Guest :')}} {{$reservation_data->child_guest}}</h6>
                    @endif
                    @if(!empty($reservation_data->created_at))
                        <h6>{{__('Booking at : ')}}{{\Carbon\Carbon::parse($reservation_data->created_at)->format('d-m-Y h:iA')}}</h6>
                    @endif
                    @if(!empty($reservation_data->reservation_start_date))
                        <h6>{{__('Reservation Starts: ')}}{{ !empty($reservation_data->reservation_start_date) ? \Carbon\Carbon::parse($reservation_data->reservation_start_date)->format('d-m-Y') : ''}}</h6>
                    @endif
                    @if(!empty($reservation_data->reservation_end_date))
                        <h6>{{__('Reservation Ends: ')}}{{ !empty($reservation_data->reservation_end_date) ? \Carbon\Carbon::parse($reservation_data->reservation_end_date)->format('d-m-Y') : ''}}</h6>
                    @endif
                    @if(!empty($reservation_data->reservation_note))
                        <p class="card-text">{{$reservation_data->reservation_note}}</p>
                    @endif
                </div>
            @elseif(isset($restaurant_data) && !empty($restaurant_data))
                <a href="{{route('user.listing',['slug'=>$restaurant_data->spotlist_directory_slug])}}">
                    <img class="card-img-top" src="{{getImage($restaurant_data->thumb_image)}}" alt="Card image cap" height="250">
                </a>
                <div class="card-body">
                    <a href="{{route('user.listing',['slug'=>$restaurant_data->spotlist_directory_slug])}}">
                        <h4>{{$restaurant_data->spotlist_directory_name}}</h4>
                    </a>
                    <h6>{{__('Booking ID #')}}{{$booking_id?? ''}}</h6>
                    @if(!empty($restaurant_data->adult_guest))
                        <h6>{{__('Adult Guest :')}} {{$restaurant_data->adult_guest}}</h6>
                    @endif
                    @if(!empty($restaurant_data->child_guest))
                        <h6>{{__('Child Guest :')}} {{$restaurant_data->child_guest}}</h6>
                    @endif
                    @if(!empty($restaurant_data->created_at))
                        <h6>{{__('Booking at : ')}}{{\Carbon\Carbon::parse($restaurant_data->created_at)->format('d-m-Y h:iA')}}</h6>
                    @endif
                    @if(!empty($restaurant_data->reservation_start_date))
                        <h6>{{__('Reservation Date: ')}}{{ !empty($restaurant_data->reservation_start_date) ? \Carbon\Carbon::parse($restaurant_data->reservation_start_date)->format('d-m-Y') : ''}}</h6>
                    @endif
                    @if(!empty($restaurant_data->reservation_start_time))
                        <h6>{{__('Reservation Time: ')}}{{ !empty($restaurant_data->reservation_start_time) ? \Carbon\Carbon::parse($restaurant_data->reservation_start_time)->format('h:iA') : ''}}</h6>
                    @endif
                    @if(!empty($restaurant_data->reservation_note))
                        <p class="card-text">{{$restaurant_data->reservation_note}}</p>
                    @endif
                </div>
            @elseif(isset($beauty_data)  && !empty($beauty_data))
                <a href="{{route('user.listing',['slug'=>$reservation_data->spotlist_directory_slug])}}">
                    <img class="card-img-top" src="{{getImage($beauty_data->service_image)}}" alt="Card image cap" height="250">
                </a>
                <div class="card-body">
                    <a href="{{route('user.listing',['slug'=>$reservation_data->spotlist_directory_slug])}}">
                        <h4>{{$beauty_data->service_name}}</h4>
                    </a>
                    <h6>{{__('Booking ID #')}}{{$booking_id?? ''}}</h6>
                    <h6>{{__('Service price ')}} : {{$beauty_data->service_price}} ({{get_currency()}})</h6>
                    @if(!empty($reservation_data->created_at))
                        <h6>{{__('Booking at')}} : {{\Carbon\Carbon::parse($reservation_data->created_at)->format('d-m-Y h:iA')}}</h6>
                    @endif
                    @if(!empty($reservation_data->reservation_start_date))
                        <h6>{{__('Reservation date ')}} : {{ !empty($reservation_data->reservation_start_date) ? \Carbon\Carbon::parse($reservation_data->reservation_start_date)->format('d-m-Y') : ''}}</h6>
                    @endif
                    @if(!empty($reservation_data->reservation_start_time))
                        <h6>{{__('Start time ')}} : {{ !empty($reservation_data->reservation_start_time) ? \Carbon\Carbon::parse($reservation_data->reservation_start_time)->format('h:iA') : ''}}</h6>
                    @endif
                    @if(!empty($reservation_data->reservation_end_time))
                        <h6>{{__('End time ')}} : {{ !empty($reservation_data->reservation_end_time) ?  \Carbon\Carbon::parse($reservation_data->reservation_end_time)->format('h:iA') : ''}}</h6>
                    @endif
                    @if(!empty($reservation_data->reservation_note))
                        <p class="card-text">{{$reservation_data->reservation_note}}</p>
                    @endif
                </div>
            @elseif(isset($fitness_data)  && !empty($fitness_data))
                <a href="{{route('user.listing',['slug'=>$fitness_data->spotlist_directory_slug])}}">
                    <img class="card-img-top" src="{{getImage($fitness_data->thumb_image)}}" alt="Card image cap" height="250">
                </a>
                <div class="card-body">
                    <a href="{{route('user.listing',['slug'=>$fitness_data->spotlist_directory_slug])}}">
                        <h4>{{$fitness_data->spotlist_directory_name}}</h4>
                    </a>
                    <h6>{{__('Booking ID #')}}{{$booking_id?? ''}}</h6>
                    @if(!empty($fitness_data->reservation_note))
                        <p class="card-text">{{$fitness_data->reservation_note}}</p>
                    @endif
                </div>
            @endif
        </div>
    </div>
</div>
