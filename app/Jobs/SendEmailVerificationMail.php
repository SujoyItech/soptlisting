<?php

namespace App\Jobs;

use App\Http\Service\MailService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmailVerificationMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $email,$body,$subject;
    public function __construct($email,$body,$subject)
    {
        $this->email =$email;
        $this->body = $body;
        $this->subject =$subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        MailService::sendEmailVerificationMailProcess($this->email,$this->body,$this->subject);
    }
}
