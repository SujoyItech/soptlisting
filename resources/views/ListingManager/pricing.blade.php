@extends('Backend.layouts.app',['menu'=>'pricing'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa fa-credit-card"></i> {{__('Pricing')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        @if(isset($pricing) && !empty($pricing))
            @foreach($pricing as $price)
                <div class="col-lg-4">
                    <div class="card-box bg-pattern">
                        <div class="text-center">
                            <h4 class="mb-1 font-18">{{$price->spotlist_packages_name}}</h4>
                            @if($price->packages_type === 'Free')
                                <span class="badge badge-success">{{$price->packages_type}}</span>
                            @else
                                <p class="text-muted  font-14">{{$price->price}}( {{get_currency()}} )</p>
                                <p class="text-muted  font-14">( {{$price->day_validity}} {{__('Days')}} )</p>
                            @endif
                            @if($price->is_recomended === TRUE)
                                <span class="badge badge-secondary">{{__('Recommended')}}</span>
                            @endif
                        </div>

                        <p class="font-14 text-center text-muted">
                            {{$price->description}}
                        </p>

                        <div class="text-center">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">{{__('Num of listing:')}} {{$price->num_of_listing}}</li>
                                <li class="list-group-item">{{__('Num of photos:')}} {{$price->num_of_photos}}</li>
                                <li class="list-group-item">{{__('Video ability:')}} {{$price->add_video_ability == TRUE ? 'Video enabled' : 'Video disabled'}}</li>
                                <li class="list-group-item">{{__('Is featured:')}} {{$price->is_featured == TRUE ? 'Featured' : 'Not featured'}}</li>
                            </ul>
                        </div>

                        <div class="row mt-4 text-center">
                            <div class="col-12">
                                @if($price->packages_type === 'Free' && checkFreeSubscription(\Illuminate\Support\Facades\Auth::user()->id))
                                    <span class="text-danger text-center">{{__('You have already subscribed free subscription.')}}</span>
                                @else
                                <a href="{{route('listing-manager-package-subscription-process',['package_id'=>$price->spotlist_packages_id])}}" class="btn btn-sm btn-success btn-rounded text-white">{{$price->packages_type == 'Free' ?__('Get now'):__('Purchase now')}}</a>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection
@section('script')
    <script>
        $('.package_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();

            var submit_url = "{{route('listing-manager-package-subscription')}}";
            var data = {
                spotlist_packages_id : $(this).data('package-id')
            };
            makeAjaxPost(data, submit_url, load).done(function (response) {
                load.ladda('stop');
                if (response.success == true){
                    swalSuccess(response.message);
                }else {
                    swalError(response.message);
                }
            });
        });
    </script>
@endsection
