<?php

namespace App\Http\Controllers\ListingManager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function inbox(){
        return view('ListingManager.inbox');
    }

    public function chat(){
        return view('ListingManager.chats');
    }
}
