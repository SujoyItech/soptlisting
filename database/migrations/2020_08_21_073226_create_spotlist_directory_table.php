<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistDirectoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_directory', function (Blueprint $table) {
            $table->increments('spotlist_directory_id');
            $table->unsignedBigInteger('sys_users_id');
            $table->unsignedInteger('spotlist_parent_category_id')->nullable();
            $table->string('spotlist_directory_name');
            $table->text('description')->nullable();
            $table->boolean('is_featured')->default(0);
            $table->text('google_analytics_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city')->nullable();
            $table->text('address')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->text('google_location')->nullable();
            $table->string('thumb_image')->nullable();
            $table->string('cover_image')->nullable();
            $table->enum('video_provider',['Youtube','Vimeo','Facebook','HTML file'])->default('Youtube');
            $table->string('video_url')->nullable();
            $table->text('seo_meta_keywords')->nullable();
            $table->text('seo_meta_description')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('linkedin_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('type')->default('General');
            $table->string('registration_step');
            $table->integer('created_by')->default(2);
            $table->integer('updated_by')->default(2);
            $table->boolean('is_verified')->default(0);
            $table->boolean('video_ability')->default(0);
            $table->bigInteger('visit_count')->default(0);
            $table->bigInteger('booking_count')->default(0);
            $table->bigInteger('wishing_count')->default(0);
            $table->enum('status',['Active','Inactive','Declined','Incomplete'])->default('Incomplete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_directory');
    }
}
