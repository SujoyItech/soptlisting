<div class="modal fade" id="bookingMessageModal" tabindex="-1" role="dialog"
     aria-labelledby="bookingMessageModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="bookingMessageModalTitle">{{__('Booking messages')}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body message_modal">

            </div>
        </div>
    </div>
</div>
