<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectoryTypeLikeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory_type_like_table')->delete();
        
        \DB::table('spotlist_directory_type_like_table')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sys_users_id' => 4,
                'spotlist_directory_id' => 1,
                'spotlist_directory_type_id' => 1,
                'like' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'sys_users_id' => 4,
                'spotlist_directory_id' => 1,
                'spotlist_directory_type_id' => 2,
                'like' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'sys_users_id' => 4,
                'spotlist_directory_id' => 2,
                'spotlist_directory_type_id' => 1,
                'like' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'sys_users_id' => 4,
                'spotlist_directory_id' => 2,
                'spotlist_directory_type_id' => 2,
                'like' => 1,
            ),
        ));

        
    }
}