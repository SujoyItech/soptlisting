<?php

namespace App\Http\Controllers\ListingManager\Pricing;

use App\Http\Controllers\Controller;
use App\Traits\DataSaveTrait;
use Braintree\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;

class PricingController extends Controller
{
    use DataSaveTrait;
    private $service;
    public function __construct(){
        $this->service = new PricingService();
    }
    public function pricing(){
        $data['pricing'] = $this->service->getPricingList();
        return view('ListingManager.pricing',$data);
    }

    public function packageSubscriptionProcess($package_id){
        $data['packages'] = DB::table('spotlist_packages')->where('spotlist_packages_id',$package_id)->first();
        return view('ListingManager.package_details',$data);
    }

    public function packageSubscription(Request $request){
        try {
            $payload = $request->input('payload', false);
            $nonce = $payload['nonce'];
            $status = Transaction::sale([
                'amount' => floatval($request->price),
                'paymentMethodNonce' => $nonce,
                'options' => [
                    'submitForSettlement' => True
                ]
            ]);
            $response = $this->service->packageSubscription($request->spotlist_packages_id,$status);
            return response()->json($status);
        }catch (\Exception $exception){
            $status['success'] = FALSE;
            return response()->json($status);
        }

    }

    public function packageSubscriptionByStripe(Request $request){
        try {
            $settings = __options(['payment_settings']);
            Stripe::setApiKey($settings->stripe_secret);
            $stripe_response = Charge::create ([
                "amount" => 100 * floatval($request->price),
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from itsolutionstuff.com."
            ]);

            $response = $this->service->packageSubscription($request->spotlist_packages_id,$stripe_response);

            if ($response['success'] == TRUE){
                return redirect()->route('listing-manager-payment-history')->with(['success'=>__('Payment successful.')]);
            }else{
                return redirect()->back()->with(['dismiss'=>__('Payment failed!')]);
            }
        }catch (\Exception $exception){
            return redirect()->back()->with(['dismiss'=>__('Something went wrong!')]);
        }


    }

    public function packageSubscriptionFree(Request $request){
        $status = [
            'amount'=> 0,
            "description" => "User free subscription"
        ];
        $user_package = DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->first();
        if (isset($user_package) && $user_package->free_subscription_check == true){
            $response['success'] = FALSE;
            $response['message'] = __('Sorry! you have already taken free subscription.');
        }else{
            $response = $this->service->packageSubscription($request->spotlist_packages_id,$status);
        }

        return response()->json($response);
    }

    public function paymentHistory(Request $request){
        $service = new \App\Http\Controllers\Admin\PricingModule\PricingService();
        $paymenHistory = $service->getPaymentHistory(NULL,Auth::user()->id);
        if ($request->ajax()){

            return datatables($paymenHistory)
                ->editColumn('packages_type', function ($item) {
                    if ($item->packages_type == 'Free'){
                        return '<span class="badge badge-success">'.$item->packages_type.'</span>';
                    }else{
                        return '<span class="badge badge-warning">'.$item->packages_type.'</span>';
                    }
                })
                ->rawColumns(['packages_type'])
                ->make(TRUE);
        }
        return view('ListingManager.purchase_history');
    }

    public function getSubscription(){
        $data['subscription'] = DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->first();
        return view('ListingManager.my_subscription',$data);
    }

}
