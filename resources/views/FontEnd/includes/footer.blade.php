
<footer class="site-footer sticky-stopper" id="scroll-to">
    <div class="upper-footer">
        <div class="container">
            <div class="row">
                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="widget about-widget">
                        <div class="logo widget-title">
                            <a href="{{url('/')}}"><img src="{{isset($settings->app_logo) ? asset($settings->app_logo) : asset('FontEnd/assets/images/logo/logo.png')}}" alt=""></a>
                        </div>
                        <p>{{$settings->footer_descrpition ?? ''}}</p>
                    </div>
                </div>
                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="widget link-widget">
                        <div class="widget-title">
                            <h3>{{__('Quick Links')}}</h3>
                        </div>
                        <ul>
                            <li><a href="{{route('user.home')}}">{{__('Home')}}</a></li>
                            <li><a href="{{route('user.listing')}}">{{__('Listing')}}</a></li>
                            <li><a href="{{route('user.profile')}}">{{__('Profile')}}</a></li>
                            <li><a href="{{route('user.blog')}}">{{__('Blog')}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="widget link-widget">
                        <div class="widget-title">
                            <h3>{{__('Company')}}</h3>
                        </div>
                        <ul>
                            <li><a href="{{route('user.about')}}">{{__('About Us')}}</a></li>
                            <li><a href="{{route('user.contact')}}">{{__('Contact Us')}}</a></li>
                            <li><a href="{{route('user.faq')}}">{{__('FAQ')}}</a></li>
                            <li><a href="{{route('login')}}">{{__('Sign In')}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col col-lg-3 col-lg-offset-1 col-md-3 col-sm-12 col-12">
                    <div class="widget market-widget service-link-widget">
                        <div class="widget-title">
                            <h3>{{__('Contact Us')}} </h3>
                        </div>
                        <div class="contact-ft">
                            <ul>
                                <li>{{$settings->contact_address ?? ''}}</li>
                                <li>{{$settings->contact_email ?? ''}}</li>
                                <li>{{$settings->contact_number}}</li>
                            </ul>
                        </div>
                        <ul class="socilal-link">
                            <li><a href="{{$settings->facebook_link ?? ''}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{$settings->twitter_link ?? ''}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="{{$settings->instagram_link ?? ''}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="{{$settings->google_link ?? ''}}" target="_blank"><i class="fa fa-google"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </div>
    <div class="lower-footer">
        <div class="sticky-stopper"></div>
        <div class="container">
            <div class="row">
                <div class="col col-xl-12">
                    <p class="copyright"> {{$settings->copy_right_text ?? ''}}</p>
                </div>
            </div>
        </div>
    </div>
</footer>

@include('FontEnd.login.login_modal')
