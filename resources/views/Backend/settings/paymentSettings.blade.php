@extends('Backend.layouts.app',['menu'=>'settings','sub_menu'=>'payment_settings'])
@section('style')

@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-cogs"></i> {{__('Payment settings')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('admin-payment-settings-save')}}" class="parsley-examples" method="POST" id="submit_payment_settings" enctype="multipart/form-data">
            @csrf
            <div class="row mb-4 text-center">
                <div class="col-12">
                    <div class="radio radio-info form-check-inline">
                        <input type="radio" id="brainTree" value="brain_tree" name="payment_method" @if(isset($settings) && $settings->payment_method == 'brain_tree') checked @endif class="method">
                        <label for="brainTree"> {{__('Braintree')}} </label>
                    </div>
                    <div class="radio radio-info form-check-inline">
                        <input type="radio" id="stripe" value="stripe" name="payment_method" @if(isset($settings) && $settings->payment_method == 'stripe') checked @endif class="method">
                        <label for="stripe"> {{__('Stripe')}} </label>
                    </div>
                </div>
            </div>
            <div class="brain_tree @if(isset($settings) && $settings->payment_method == 'brain_tree') @else d-none  @endif">
                @include('Backend.settings.Payment.brain_tree')
            </div>
            <div class="stripe @if(isset($settings) && $settings->payment_method == 'stripe') @else d-none  @endif">
                @include('Backend.settings.Payment.stripe')
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-info btn-sm submit_button"><i class="fa fa-save"></i> {{__('Save')}}</button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        $('.method').on('click',function (){
            var val = $(this).val();
            if (val == 'brain_tree'){
                $('.brain_tree').removeClass('d-none');
                $('.stripe').addClass('d-none');
            }else if( val == 'stripe'){
                $('.stripe').removeClass('d-none');
                $('.brain_tree').addClass('d-none');
            }
        });

        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                console.log('hello');
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            swalSuccess(response.message)
                        }else{
                            swalError(response.message)
                        }
                    });
                }
            });
        });
    </script>
@endsection

