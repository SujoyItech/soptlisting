<?php

namespace App\Http\Controllers\Common\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $service;

    public function __construct() {
        $this->service = new ProfileService();
    }
    public function profileSettings(){
        $profile_data = $this->service->getProfileData();
        return view('Common.Profile.profile',$profile_data);
    }

    public function profileUpdate(Request  $request){
        $response = $this->service->profileUpdate($request);
        return response()->json($response);
    }

    public function passwordUpdate(Request  $request){
        $response = $this->service->passwordUpdate($request);
        return response()->json($response);
    }
}
