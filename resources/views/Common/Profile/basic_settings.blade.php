<div class="card-box">
    <form action="{{route('profile-update')}}" class="parsley-examples" method="POST" id="user_profile_basic" enctype="multipart/form-data">
        @csrf
        <h5 class="mb-4 text-uppercase"><i class="mdi mdi-account-circle mr-1"></i> {{__('Personal Info')}}</h5>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="firstname">{{__('Full Name')}}</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter full name" value="{{$profile->name ?? ''}}" parsley-trigger="change" required >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="lastname">{{__('Designation')}}</label>
                    <input type="text" class="form-control" id="designation" name="designation" placeholder="Enter designation" value="{{$profile->designation ?? ''}}" parsley-trigger="change" required >
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="userbio">{{__('Bio')}}</label>
                    <textarea class="form-control" id="about" name="about" rows="4" placeholder="Write something...">{{$profile->about ?? ''}}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="userbio">{{__('Address')}}</label>
                    <textarea class="form-control" id="address" name="address" rows="4" placeholder="Write something...">{{$profile->address ?? ''}}</textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="useremail">{{__('Email Address')}}</label>
                    <input type="email" disabled class="form-control" id="email" name="email" placeholder="Enter email" value="{{$profile->email ?? ''}}" parsley-trigger="change" required >
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="mobile">{{__('Phone Number')}}</label>
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter mobile" value="{{$profile->mobile ?? ''}}">
                </div>
            </div>

        </div>

        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-earth mr-1"></i> {{__('Social')}}</h5>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="social-fb">{{__('Facebook')}}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-facebook-square"></i></span>
                        </div>
                        <input type="text" class="form-control" id="social-fb" name="facebook_link" placeholder="Url" value="{{$profile->facebook_link ?? ''}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="social-tw">{{__('Twitter')}}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-twitter"></i></span>
                        </div>
                        <input type="text" class="form-control" id="social-tw" placeholder="Url" name="twitter_link" value="{{$profile->twitter_link ?? ''}}">
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="social-insta">{{__('Instagram')}}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-instagram"></i></span>
                        </div>
                        <input type="text" class="form-control" id="social-insta" placeholder="Url" name="instagram_link" value="{{$profile->instagram_link ?? ''}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="social-lin">{{__('Linkedin')}}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                        </div>
                        <input type="text" class="form-control" id="social-lin" placeholder="Url" name="linkedin_link" value="{{$profile->linkedin_link ?? ''}}">
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="social-sky">{{__('Website')}}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                        </div>
                        <input type="text" class="form-control" id="social-sky" placeholder="@username" name="website" value="{{$profile->website ?? ''}}">
                    </div>
                </div>
            </div>
        </div> <!-- end row -->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-form-label" for="user_image">{{__('User Image')}}</label>
                    <input type="file" name="user_image" data-plugins="dropify" class="dropify"
                           data-default-file="{{isset($profile->user_image) ? getImageUrl($profile->user_image) : ''}}"
                           data-allowed-file-extensions="png jpg jpeg jfif"
                           data-max-file-size="1M" />
                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                </div>
            </div>
        </div>

        <div class="text-left">
            <input type="hidden" name="id" value="{{$profile->id ?? ''}}">
            <button type="submit" class="btn btn-success waves-effect waves-light mt-2 submit_info"><i class="fa fa-save"></i> {{__('Save')}}</button>
        </div>
    </form>
</div>
