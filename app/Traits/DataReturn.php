<?php


namespace App\Traits;


use Illuminate\Support\Facades\DB;

trait DataReturn {

    public function basicTableInfo($table_name,$select_column=[],$primary_key_value=[],$foreign_key_value=[],$where_column=[],$order_by=[],$paginate=null){
        $query = DB::table($table_name);
        $query = !empty($select_column) ? $query->select($select_column) : $query;
        $query = !empty($where_column) ? $query->where($where_column) : $query;

        if (!empty($primary_key_value)){
            $query = $query->where($primary_key_value[0],$primary_key_value[1])->first();
            return $query;

        }elseif (!empty($foreign_key_value)){
            $query = $query->where($foreign_key_value[0],$foreign_key_value[1]);
        }

        $query = !empty($order_by) ? $query->orderBy($order_by[0],$order_by[1]) : $query;
        $query = $paginate != NULL ? $query->paginate($paginate):$query->get();
        return $query;
    }

    public function joinTableInfo($table_name,$join_table_array=[],$select_column=[],$primary_key_value=[],$foreign_key_value=[],$where_column=[],$order_by=[],$paginate=NULL,$group_by=[],$where_in=[],$where_not_in=[]){

        $query = DB::table($table_name)->select($select_column);
        if (!empty($join_table_array)){
            foreach ($join_table_array as $join_array){
                if ($join_array[0] == 'left'){
                    $query = $query->leftJoin($join_array[1],$join_array[2],$join_array[3]);
                }elseif($join_array[0] == 'right'){
                    $query = $query->rightJoin($join_array[1],$join_array[2],$join_array[3]);
                }else{
                    $query = $query->join($join_array[1],$join_array[2],$join_array[3]);
                }
            }
        }
        $query = !empty($group_by) ? $query->groupBy($group_by) : $query;
        $query = !empty($where_column) ? $query->where($where_column) : $query;
        if (!empty($primary_key_value)){
            $query = $query->where($primary_key_value[0],$primary_key_value[1])->first();
            return $query;
        }
        $query = !empty($foreign_key_value) ? $query->where($foreign_key_value[0],$foreign_key_value[1]) : $query;
        $query = !empty($where_in) ? $query->whereIn($where_in[0],$where_in[1]) : $query;
        $query = !empty($where_not_in) ? $query->whereNotIn($where_not_in[0],$where_not_in[1]) : $query;
        $query = !empty($order_by) ? $query->orderBy($order_by[0],$order_by[1]) : $query;
        $query = $paginate != NULL ? $query->paginate($paginate) : $query->get();
        return $query;
    }
}
