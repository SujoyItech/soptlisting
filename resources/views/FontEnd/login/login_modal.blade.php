@php($settings = __options(['admin_settings']))
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loginModalLabel">{{__('Sign In')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="show-danger">

                </div>
                <div class="login">
                    @include('FontEnd.login.login')
                </div>
                <div class="register d-none">
                    @include('FontEnd.login.register')
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click','.login_btn',function (){
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);
                makeAjaxPostUser(formData, submit_url).done(function (response) {
                    console.log(response)
                    if (response.success == true){
                        var redirect_url = "{{Request::url()}}";
                        window.location = redirect_url;
                    }else {
                        var html = '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
                            '                    <strong class="alert-message">'+response.message+'</strong>\n' +
                            '                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                            '                        <span aria-hidden="true">&times;</span>\n' +
                            '                    </button>\n' +
                            '                </div>';

                        $('.show-danger').html(html);
                    }
                });
            }
        });
    });
    $(document).on('click','.register_btn',function (){
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);
                makeAjaxPostUser(formData, submit_url).done(function (response) {
                    if (response.success == true){
                        if (response.mail_verification == true){
                            var html = '<div class="alert alert-success alert-dismissible fade show" role="alert">\n' +
                                '                    <strong class="alert-message">'+response.message+'</strong>\n' +
                                '                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                                '                        <span aria-hidden="true">&times;</span>\n' +
                                '                    </button>\n' +
                                '                </div>';

                            $('.show-danger').html(html);
                            $('.register').addClass('d-none');
                            $('.login').removeClass('d-none');
                        }else {
                            var redirect_url = "{{Request::url()}}";
                            window.location = redirect_url;
                        }

                    }else {
                        var html = '<div class="alert alert-danger alert-dismissible fade show" role="alert">\n' +
                            '                    <strong class="alert-message">'+response.message+'</strong>\n' +
                            '                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                            '                        <span aria-hidden="true">&times;</span>\n' +
                            '                    </button>\n' +
                            '                </div>';

                        $('.show-danger').html(html);
                    }
                });
            }
        });
    });
    function makeAjaxPostUser(data, url) {
        return $.ajax({
            url: url,
            type: 'post',
            headers: { 'X-CSRF-Token' : $('meta[name=laraframe]').attr('content') },
            data: data,
            cache: false,
            processData: false,
            contentType: false,
        }).always(function() {

        }).fail(function(err) {
            var allerror = Object.values(err.responseJSON);
            $('#email_error').html(allerror[1].email[0]);
            $('#email_error').removeClass('d-none');
        })
    }
</script>
