@extends('Backend.layouts.app',['menu'=>'blog','sub_menu'=>'blog_add'])
@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-blog-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New post')}}</a>
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-blog-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('Blog list')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-align-left"></i> {{__('Blogs')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('admin-blog-save')}}" class="parsley-examples" method="POST" id="submit_blog" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">{{__('Blog title')}}<span class="text-danger">*</span></label>
                                <input type="text" name="spotlist_blogs_title" parsley-trigger="change" required value="{{ $blog->spotlist_blogs_title ?? ''}}"
                                       placeholder="Blog title" class="form-control" id="spotlist_blogs_title">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">{{__('Slug')}}</label>
                                <input type="text" name="slug" parsley-trigger="change" value="{{ $blog->slug ?? ''}}"
                                       placeholder="Blog title" class="form-control" id="spotlist_blogs_slug">
                                <span class="show-text d-none"></span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label class=" col-form-label" for="country">{{__('Category')}}</label>
                                @php($pram = array('selected_value'=>$blog->spotlist_category_id ?? ''))
                                {{__combo('spotlist_category',$pram)}}
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label class=" col-form-label" for="country">{{__('Featured')}}</label>
                                @php($featured = [ 1 =>__('Featured'),0 =>__('Not featured')])
                                {!! Form::select('is_featured',  $featured, $blog->is_featured ?? '', ['class' => 'form-control','required']) !!}
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group ">
                                <label class=" col-form-label" for="country">{{__('Blog text')}}</label>
                                <textarea name="contents" id="summernote-basic">{!! $blog->contents ?? '' !!}</textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                                    <i class="fa fa-save"></i>
                                    {{__('Save')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="col-form-label" for="blog_thumb_image">{{__('Blog thumbnail')}}</label>
                        <input type="file" name="blog_thumb_image" data-plugins="dropify"
                               data-default-file="{{isset($blog->blog_thumb_image) && !empty($blog->blog_thumb_image) ? getImageUrl($blog->blog_thumb_image) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="1M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                    </div>

                    <div class="form-group">
                        <label class="col-form-label" for="blog_cover_image">{{__('Blog cover')}}</label>
                        <input type="file" name="blog_cover_image" data-plugins="dropify"
                               data-default-file="{{isset($blog->blog_cover_image) && !empty($blog->blog_cover_image) ? getImageUrl($blog->blog_cover_image) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="1M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                    </div>
                </div>
            </div>
        </form>
        <input type="hidden" name="spotlist_blogs_id" value="{{$blog->spotlist_blogs_id ?? ''}}" id="spotlist_blogs_id">
    </div>
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>

    <script>
        $(document).ready(function (){
            if ($('#spotlist_blogs_id').val().length !== 0){
                $('.submit_button').prop('disabled', false);
                $('#spotlist_blogs_slug').prop('disabled', true);
            }else {
                $("#spotlist_blogs_title").bind("keyup change", function(e) {
                    var slug = slugify($(this).val());
                    $('#spotlist_blogs_slug').val(slug);
                    var check_url = "{{route('check-blog-slug')}}";
                    var diectory_data = {slug : slug}
                    makeAjaxPost(diectory_data,check_url,null).done(function (response){
                        if (response.success == true){
                            $('.show-text').removeClass('text-danger');
                            $('.show-text').addClass('text-success');
                            $('.show-text').html('Slug is valid');
                            $('.show-text').removeClass('d-none');
                            $('.submit_button').prop("disabled",false);
                        }else {
                            $('.show-text').removeClass('text-success');
                            $('.show-text').addClass('text-danger');
                            $('.show-text').html('Slug is already exist');
                            $('.show-text').removeClass('d-none');
                            $('.submit_button').prop("disabled",true);
                        }
                    })
                })
                $("#spotlist_blogs_slug").bind("keyup change", function(e) {
                    var slug = slugify($(this).val());
                    $('#spotlist_blogs_slug').val(slug);
                    var check_url = "{{route('check-blog-slug')}}";
                    var diectory_data = {slug : slug}
                    makeAjaxPost(diectory_data,check_url,null).done(function (response){
                        if (response.success == true){
                            $('.show-text').removeClass('text-danger');
                            $('.show-text').addClass('text-success');
                            $('.show-text').html('Slug is valid');
                            $('.show-text').removeClass('d-none');
                            $('.submit_button').prop("disabled",false);
                        }else {
                            $('.show-text').removeClass('text-success');
                            $('.show-text').addClass('text-danger');
                            $('.show-text').html('Slug is already exist');
                            $('.show-text').removeClass('d-none');
                            $('.submit_button').prop("disabled",true);
                        }
                    })
                })
            }
        });
        function slugify(text) {
            return text
                .toString()                     // Cast to string
                .toLowerCase()                  // Convert the string to lowercase letters
                .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
                .trim()                         // Remove whitespace from both sides of a string
                .replace(/\s+/g, '-')           // Replace spaces with -
                .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                .replace(/\-\-+/g, '-');        // Replace multiple - with single -
        }

        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    if ($('#spotlist_blogs_id').val().length !== 0){
                        formData.append('spotlist_blogs_id',$('#spotlist_blogs_id').val());
                    }
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            var redirect_url = "{{url('admin-blog-list')}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'error');
                        }
                    });
                }
            });
        });
    </script>
@endsection

