@extends('Backend.layouts.app')

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('All Notifications')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                @if(isset($notifications) &&!empty($notifications[0]))
                    <!-- users -->
                    <div class="row">
                        <div class="col">
                            <div data-simplebar>
                                @foreach($notifications as $notify)
                                    @php($notify_body = json_decode($notify->body))
                                    @if($notify->type == LISTING_MANAGER_ADMIN_BOOKING)
                                        <a href="{{isset($notify_body->type) ? url('user-booking-request'.'/'.$notify_body->type) :url('user-booking-request'.'/'.'all')}}" class="text-body notify-item" data-id="{{$notify->id}}">
                                            <div class="media p-2">
                                                <img src="{{getImageUrl($notify_body->user_image ?? '','user')}}" class="mr-2 rounded-circle" height="42" alt="" />
                                                <div class="media-body">
                                                    <h5 class="mt-0 mb-0 font-14">
                                                        <span class="float-right text-muted font-weight-normal font-12">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</span><br>
                                                        <span class="float-right text-muted font-weight-normal font-12">{{$notify->status == STATUS_SUCCESS ? 'seen' : ''}}</span>
                                                        {{$notify_body->username ?? ''}}
                                                    </h5>
                                                    <p class="mt-1 mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                        <span class="w-75">{{$notify_body->title}}</span>
                                                    </p>
                                                    <p class="mt-1 mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                        <span class="w-75">{{$notify_body->body ?? ''}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    @elseif($notify->type == ADMIN_DIRECTORY || $notify->type == LISTING_MANAGER_DIRECTORY)
                                        <a href="{{ \Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER ? route('list-manager-directory-list') : route('directory-list')}}" class="text-body notify-item" data-id="{{$notify->id}}">
                                            <div class="media p-2">
                                                <img src="{{getImageUrl($notify_body->directory_image ?? '','other')}}" class="mr-2 rounded-circle" height="42" alt="" />
                                                <div class="media-body">
                                                    <h5 class="mt-0 mb-0 font-14">
                                                        <span class="float-right text-muted font-weight-normal font-12">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</span><br>
                                                        <span class="float-right text-muted font-weight-normal font-12">{{$notify->status == STATUS_SUCCESS ? 'seen' : ''}}</span>
                                                        {{$notify_body->directory_name ?? ''}}
                                                    </h5>
                                                    <p class="mt-1 mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                        <span class="w-75">{{$notify_body->title ?? ''}}</span>
                                                    </p>
                                                    <p class="mt-1 mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                        <span class="w-75">{{$notify_body->body ?? ''}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    @elseif($notify->type == WISHING)

                                        <a href="{{ \Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER ? route('list-manager-wishing-list') : route('directory-list')}}" class="text-body notify-item" data-id="{{$notify->id}}">
                                            <div class="media p-2">
                                                <img src="{{getImageUrl($notify_body->user_image ?? '','user')}}" class="mr-2 rounded-circle" height="42" alt="" />
                                                <div class="media-body">
                                                    <h5 class="mt-0 mb-0 font-14">
                                                        <span class="float-right text-muted font-weight-normal font-12">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</span><br>
                                                        <span class="float-right text-muted font-weight-normal font-12">{{$notify->status == STATUS_SUCCESS ? 'seen' : ''}}</span>
                                                        {{$notify_body->username ?? ''}}
                                                    </h5>
                                                    <p class="mt-1 mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                        <span class="w-75">{{$notify_body->title ?? ''}}</span>
                                                    </p>
                                                    <p class="mt-1 mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                        <span class="w-75">{{$notify_body->body ?? ''}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    @elseif($notify->type == SUBSCRIPTION_NOTIFICATION)
                                        <a href="{{\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER ? url('listing-manager-subscription') : url('admin-payment-history')}}" class="text-body notify-item" data-id="{{$notify->id}}">
                                            <div class="media p-2">
                                                <img src="{{getImageUrl($notify_body->user_image ?? '')}}" class="mr-2 rounded-circle" height="42" alt="" />
                                                <div class="media-body">
                                                    <h5 class="mt-0 mb-0 font-14">
                                                        <span class="float-right text-muted font-weight-normal font-12">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</span><br>
                                                        <span class="float-right text-muted font-weight-normal font-12">{{$notify->status == STATUS_SUCCESS ? 'seen' : ''}}</span>
                                                        {{$notify_body->username ?? ''}}
                                                    </h5>
                                                    <p class="mt-1 mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                        <span class="w-75">{{$notify_body->title ?? ''}}</span>
                                                    </p>
                                                    <p class="mt-1 mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                        <span class="w-75">{{$notify_body->body ?? ''}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                    <hr>
                                @endforeach
                            </div> <!-- end slimscroll-->
                        </div> <!-- End col -->
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('Common.Booking.booking_message')
@endsection
@section('script')
    <script>
        $(document).on("click", ".notify-item", function (event) {
            event.preventDefault();
            var data = {
                id : $(this).data('id')
            }
            var url = "{{url('admin-notification-make-seen')}}";
            var this_directory = $(this);
            makeAjaxPost(data,url,null).done(function (response){
                if (response.success == true){
                    var redirect_url = $(this_directory).attr('href');
                    window.location = redirect_url;
                }

            });
        });
    </script>
@endsection
