@php($settings = __options(['admin_settings','frontend_settings']))
<div class="tab-pane" id="location">
    <form action="{{url('directory-location-save')}}" class="parsley-examples" method="POST" id="submit_directory_location" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="form-group ">
                    <label class="col-form-label" for="country">{{__('Country')}}</label>
                    @php( $data_param = ['selected_value' => $directory->country_id ?? ''])
                    {!! __combo('spotlist_countries',$data_param) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group ">
                    <label class=" col-form-label" for="country">{{__('City')}}</label>
                    <select class="form-control" name="city" id="city_dropdown">
                        <option value="">{{__('Select City')}}</option>
                        @if(isset($cities) && !empty($cities))
                            @foreach($cities as $city)
                                <option value="{{$city->spotlist_cities_id}}" {{is_selected($city->spotlist_cities_id,$directory->city ?? '')}}>{{$city->spotlist_cities_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-md-5 col-12">
                <div class="form-group ">
                    <label class=" col-form-label" for="country">{{__('Latitude')}}</label>
                    <input type="text" class="form-control" id="latitude" name="latitude" value="">
                </div>
            </div>
            <div class="col-md-5 col-12">
                <div class="form-group ">
                    <label class=" col-form-label" for="country">{{__('Longitude')}}</label>
                    <input type="text" class="form-control" id="longitude" name="longitude" value="">
                </div>
            </div>

            <div class="col-md-2 col-12  mt-4">
                <div class="form-group">
                    <button type="button" class="btn btn-sm btn-info map_show"><i class="fa fa-location-arrow"></i> {{__('Change Location')}}</button>
                </div>
            </div>
            <div class="col-12 map_show_div d-none">
                <div id="restaurant_address_map" style="height: 300px"></div>
            </div>
            <div class="col-12">
                <div class="form-group ">
                    <label class=" col-form-label" for="address">{{__('Address')}}</label>
                    <textarea id="address" class="form-control" name="address" data-parsley-trigger="keyup" data-parsley-validation-threshold="10">{{$directory->address ?? ''}}</textarea>
                </div>
            </div>
        </div>
        <ul class="list-inline mb-0 wizard">
            <li class="previous list-inline-item">
                <a href="javascript: void(0);" class="btn btn-sm btn-secondary">{{__('Previous')}}</a>
            </li>
            <li class="next list-inline-item float-right">
                <button type="submit" class="btn btn-sm btn-success directory_location_submit btn-next">{{__('Save & Next')}}</button>
            </li>
        </ul>
    </form>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script>
    function getLocation() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition,showError);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }
    function showPosition(position) {
        initMap(position.coords.latitude,position.coords.longitude);
        $('.map_show_div').removeClass('d-none');
    }
    function initMap(latitude,longitue) {
        var previous_lat = "{{$directory->latitude ?? ''}}";
        var previous_lon = "{{$directory->longitude ?? ''}}";
        var local_lat =  latitude;
        var local_lon =  longitue;
        var myLatlng = {lat: local_lat, lng: local_lon};
        $('#latitude').val(previous_lat);
        $('#longitude').val(previous_lon);
        var map = new google.maps.Map(
            document.getElementById('restaurant_address_map'), {zoom: 15, center: myLatlng});

        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: {lat: local_lat, lng: local_lon }
        });

        google.maps.event.addListener(marker, 'dragend', function (a) {
            console.log(a)
            var lat_long = a.latLng;
            $('#latitude').val(lat_long.lat());
            $('#longitude').val(lat_long.lng());
        });
    }

    function showError(error) {
        $('.map_show_div').addClass('d-none');
        switch(error.code) {
            case error.PERMISSION_DENIED:
                initMap(parseFloat("{{$settings->latitude ?? 22.819009982916523}}"),parseFloat("{{$settings->longitude ?? 89.5512269327881}}"));
                $('.map_show_div').removeClass('d-none');
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.")
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.") ;
                break;
        }
    }



</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{$settings->map_key ?? 'AIzaSyAy-PboZ3O_A25CcJ9eoiSrKokTnWyAmt8'}}&libraries=geometry,places&callback=initMap">
</script>

<script>

    $(document).on('click','.map_show',function (){
        getLocation();
    })

    $('#country_id').on('change',function () {
        var country_id = $(this).val();
        var postdata = {country_id : country_id};
        var url = "{{url('change-cities-by-country')}}";
        var city_text = "{{__('Select City')}}";
        makeAjaxPost(postdata, url).then(response => {
            var city_data = response.data;

            var options = '<option>'+city_text+'</option>\n';
            $.each(city_data, function (index, city_data) {
                options += '<option value="'+city_data.spotlist_cities_id+'">'+city_data.spotlist_cities_name+'</option>\n';
            });
            console.log(options);
            $('#city_dropdown').html(options);
        });
    });

    $('.directory_location_submit').on('click', function (e) {
        Ladda.bind(this);
        var load = $(this).ladda();
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);

                if ($('#spotlist_directory_id').val().length !== 0){
                    formData.append('spotlist_directory_id',$('#spotlist_directory_id').val());
                }
                makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                    load.ladda('stop');
                });
            }
        });
    });
</script>
