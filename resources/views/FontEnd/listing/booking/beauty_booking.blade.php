<div class="list-single-wiget book">
    <h2>{{__('Appointment')}}</h2>
    <form action="{{route('user.booking.request.send')}}" method="post"  class="parsley-examples" id="beauty_booking">
        <div class="form-group">
            <input type="text" name="reservation_contact" parsley-trigger="change" parsley-type="phone" required id="phone">
        </div>
        <div class="form-group">
            <select name="spotlist_directory_type_beauty_id" class="form-control" required>
                <option value="">{{__('Select a service')}}</option>
                @if(isset($types) && !empty($types[0]))
                    @foreach($types as $type)
                        <option value="{{$type->spotlist_directory_type_beauty_id}}">{{$type->service_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group form-group2 beauty_date_check">
            <div class="form-1">
                <input id="booking_date" type="text" width="276" class="reservation_date" placeholder="Date" name="reservation_start_date" required readonly />
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/10.svg" alt=""></div>
                <span class="text-danger d-none date_error"></span>
            </div>

        </div>
        <div class="form-group form-group2">
            <div class="form-1">
                <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                    <input type="text" class="form-control beauty_reservation_start_time" placeholder="From: " name="reservation_start_time" id="reservation_start_time" parsley-trigger="change" readonly required>
                    <span class="text-danger d-none start_time_error"></span>
                </div>
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/11.svg" alt=""></div>
            </div>
            <div class="form-1">
                <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                    <input type="text" class="form-control beauty_reservation_end_time" placeholder="To: " name="reservation_end_time" id="reservation_end_time" parsley-trigger="change" readonly required>
                    <span class="text-danger d-none end_time_error"></span>
                </div>
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/11.svg" alt=""></div>
            </div>
        </div>
        <div class="form-group">
            <textarea class="form-control" rows="3" name="reservation_note" placeholder="{{__('Add a note here')}}"></textarea>
        </div>
        <div class="form-group form-btn">
            <input type="hidden" name="type" value="{{$directory->spotlist_parent_category_id}}">
            <input type="hidden" name="spotlist_directory_id" value="{{$directory->spotlist_directory_id}}">
            <input type="hidden" name="reservation_for" value="beauty">
            @if(isset(Auth::user()->id))
                <button type="submit" class="theme-btn booking_now beauty_booking">{{__('BOOK NOW')}}</button>
                @include('FontEnd.listing.booking.wish_list')
            @else
                <a href="{{route('login')}}" class="theme-btn btn-block text-center">{{__('Login')}}</a>
            @endif
        </div>
    </form>
</div>
<script>
    $(document).ready(function (){
        $('#booking_date').datepicker(
        {
            minDate: 0,
        });
        $('#booking_date').on('change',function (){
            if ($(this).val() != "" && $(this).siblings('ul.parsley-errors-list').length > 0) {
                $(this).siblings('ul').remove();
            }
            $('.date_error').addClass('d-none');
        })
    })
    $(document).ready(function (){
        $(".beauty_reservation_start_time").bind("keyup change", function(e) {
            if ($(this).val() != "" && $(this).siblings('ul.parsley-errors-list').length > 0) {
                $(this).siblings('ul').remove();
            }
            var beauty_reservation_date = $('.beauty_date_check').children().find('.reservation_date').val();
            if (beauty_reservation_date.length !== 0){
                $('.date_error').addClass('d-none')
                var check_beauty_booking_start_time_data = {
                    spotlist_directory_id: "{{$directory->spotlist_directory_id}}",
                    booking_date :beauty_reservation_date ,
                    start_time : $(this).val()
                }
                var check_beauty_booking_start_time_url = "{{url('check-booking-start-time')}}";
                makeAjaxPost(check_beauty_booking_start_time_data,check_beauty_booking_start_time_url,null).done(function (response){
                    if (response.success == true){
                        $('.start_time_error').addClass('d-none');
                        $('.beauty_booking').prop('disabled',false);
                    }else {
                        $('.start_time_error').html('Service not open now');
                        $('.start_time_error').removeClass('d-none');
                        $('.beauty_booking').prop('disabled',true);
                    }
                })
            }else {
                $('.date_error').html('Please select date first.');
                $('.date_error').removeClass('d-none');
                $('.beauty_booking').prop('disabled',true);
            }

        });

        $(".beauty_reservation_end_time").bind("keyup change", function(e) {
            if ($(this).val() != "" && $(this).siblings('ul.parsley-errors-list').length > 0) {
                $(this).siblings('ul').remove();
            }
            var beauty_reservation_date = $('.beauty_date_check').children().find('.reservation_date').val();
            if (beauty_reservation_date.length !== 0){
                $('.date_error').addClass('d-none');
                var check_beauty_booking_end_time_data = {
                    spotlist_directory_id: "{{$directory->spotlist_directory_id}}",
                    booking_date :beauty_reservation_date,
                    end_time : $(this).val()
                }
                var check_booking_end_time_url = "{{url('check-booking-end-time')}}";
                makeAjaxPost(check_beauty_booking_end_time_data,check_booking_end_time_url,null).done(function (response){
                    if (response.success == true){
                        $('.end_time_error').addClass('d-none');
                        $('.beauty_booking').prop('disabled',false);
                    }else {
                        $('.end_time_error').html('Service not open now');
                        $('.end_time_error').removeClass('d-none');
                        $('.beauty_booking').prop('disabled',true);
                    }
                })
            }else {
                $('.date_error').html('Please select date first.');
                $('.date_error').removeClass('d-none');
                $('.beauty_booking').prop('disabled',true);
            }

        });
    })
</script>
@include('FontEnd.listing.booking.phone_script')
