@extends('Backend.layouts.app',['menu'=>'settings','sub_menu'=>'fontend_settings'])
@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-cogs"></i> {{__('Frontend settings')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('admin-frontend-settings-save')}}" class="parsley-examples" method="POST" id="submit_settings" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Application title')}}<span class="text-danger">*</span></label>
                        <input type="text" name="app_title" parsley-trigger="change" required value="{{ $settings->app_title ?? ''}}"
                               placeholder="App title" class="form-control" id="app_title">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Home page title')}}</label>
                        <input type="text" name="home_page_title" parsley-trigger="change"  value="{{ $settings->home_page_title ?? ''}}"
                               placeholder="Home page title" class="form-control" id="home_page_title">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Contact Email')}}</label>
                        <input type="email" name="contact_email" parsley-trigger="change"  value="{{ $settings->contact_email ?? ''}}"
                               placeholder="Email" class="form-control" id="contact_email">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Contact number')}}</label>
                        <input type="text" name="contact_number" parsley-trigger="change"  value="{{ $settings->contact_number ?? ''}}"
                               placeholder="Contact number" class="form-control" id="contact_number">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Contact address')}}</label>
                        <input type="text" name="contact_address" parsley-trigger="change"  value="{{ $settings->contact_address ?? ''}}"
                               placeholder="Contact address" class="form-control" id="contact_address">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Facebook')}}</label>
                        <input type="text" name="facebook_link" parsley-trigger="change"  value="{{ $settings->facebook_link ?? ''}}"
                               placeholder="Facebook" class="form-control" id="facebook_link">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Twitter')}}</label>
                        <input type="text" name="twitter_link" parsley-trigger="change"  value="{{ $settings->twitter_link ?? ''}}"
                               placeholder="Twitter" class="form-control" id="twitter_link">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Instagram')}}</label>
                        <input type="text" name="instagram_link" parsley-trigger="change"  value="{{ $settings->instagram_link ?? ''}}"
                               placeholder="Instagram" class="form-control" id="instagram_link">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Google')}}</label>
                        <input type="text" name="google_link" parsley-trigger="change"  value="{{ $settings->google_link ?? ''}}"
                               placeholder="Google" class="form-control" id="google_link">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('Description')}}</label>
                        <textarea name="description" parsley-trigger="change" placeholder="Description here" class="form-control" id="description">{{$settings->description ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('Footer Description')}}</label>
                        <textarea name="footer_descrpition" parsley-trigger="change" placeholder="Footer description here here" class="form-control" id="footer_descrpition">{{$settings->footer_descrpition ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('Copyright text')}}</label>
                        <textarea name="copy_right_text" parsley-trigger="change" placeholder="Footer description here here" class="form-control" id="copy_right_text">{{$settings->copy_right_text ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('About us')}}</label>
                        <textarea name="about_us" parsley-trigger="change" placeholder="About us" class="form-control" id="about_us">{{$settings->about_us ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group ">
                        <label class=" col-form-label" for="country">{{__('Google map key')}}</label>
                        <input type="text" class="form-control" id="google_map_key" name="google_map_key" value="{{$settings->google_map_key ?? ''}}">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="form-group ">
                        <label class=" col-form-label" for="country">{{__('Latitude')}}</label>
                        <input type="text" class="form-control" id="latitude" name="latitude" value="">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="form-group ">
                        <label class=" col-form-label" for="country">{{__('Longitude')}}</label>
                        <input type="text" class="form-control" id="longitude" name="longitude" value="">
                    </div>
                </div>
                <div class="col-md-2  mt-4">
                    <div class="form-group">
                        <button type="button" class="btn btn-sm btn-info map_show"><i class="fa fa-location-arrow"></i> {{__('Change Location')}}</button>
                    </div>
                </div>
                <div class="col-12 map_show_div d-none">
                    <div id="restaurant_address_map" style="height: 300px"></div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="col-form-label" for="app_logo">{{__('App logo')}}</label>
                        <input type="file" name="app_logo" data-plugins="dropify"
                               data-default-file="{{isset($settings->app_logo) && !empty($settings->app_logo) ? getImageUrl($settings->app_logo) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="2M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="col-form-label" for="blog_cover_image">{{__('Favicon')}}</label>
                        <input type="file" name="favicon_logo" data-plugins="dropify"
                               data-default-file="{{isset($settings->favicon_logo) && !empty($settings->favicon_logo) ? getImageUrl($settings->favicon_logo) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="2M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        function showPosition(position) {
            initMap(position.coords.latitude,position.coords.longitude);
        }
        function initMap(latitude,longitue) {
            var previous_lat = "{{$settings->latitude ?? ''}}";
            var previous_lon = "{{$settings->longitude ?? ''}}";
            var local_lat =  latitude;
            var local_lon =  longitue;
            var myLatlng = {lat: local_lat, lng: local_lon};
            $('#latitude').val(previous_lat);
            $('#longitude').val(previous_lon);
            var map = new google.maps.Map(
                document.getElementById('restaurant_address_map'), {zoom: 15, center: myLatlng});

            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: {lat: local_lat, lng: local_lon }
            });

            google.maps.event.addListener(marker, 'dragend', function (a) {
                var lat_long = a.latLng;
                $('#latitude').val(lat_long.lat());
                $('#longitude').val(lat_long.lng());
            });
        }



    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6aQ3YW26Wx7LKrRwSm2L5OqIqHvS9H10&libraries=geometry,places&callback=initMap"></script>
    <script>
        $(document).ready(function (){
            getLocation();
        })
        $(document).on('click','.map_show',function (){
            $('.map_show_div').toggleClass('d-none');
        })
        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            swalSuccess(response.message)
                        }else{
                            swalError(response.message)
                        }
                    });
                }
            });
        });
    </script>
@endsection

