<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    use DataSaveTrait;
    private $service;

    public function __construct() {
        $this->service = new SettingService();
    }

    public function deleteRows(Request $request){
       $response = $this->service->deleteRows($request);
       return response()->json($response);
    }

    public function deleteListById(Request $request){
       $response = $this->service->deleteListById($request);
       return response()->json($response);
    }

    public function settings($type){
        if ($type == 'fontend_settings'){
            $data['settings'] = __options(['frontend_settings']);
            return view('Backend.settings.frontEndSettings',$data);
        }elseif ($type == 'admin_settings'){
            $data['settings'] = __options(['admin_settings']);
            return view('Backend.settings.backEndSettings',$data);
        }elseif ($type == 'payment_settings'){
            $data['settings'] = __options(['payment_settings']);
            return view('Backend.settings.paymentSettings',$data);
        }elseif ($type == 'auth_settings'){
            $data['settings'] = __options(['authentication']);
            return view('Backend.settings.authSettings',$data);
        }
    }

    private function insert_or_update($option_group,$option_key,$option_value){
        DB::table('sys_system_settings')->updateOrInsert(['option_group'=>$option_group,'option_key'=>$option_key],
            ['option_group'=>$option_group,'option_key'=>$option_key,'option_value'=>$option_value]);
    }
    public function frontEndSettingsSave(Request $request){
        try {
            if (isset($request->app_title) && !empty($request->app_title)){
                $this->insert_or_update('frontend_settings','app_title',$request->app_title);
            }if (isset($request->home_page_title) && !empty($request->home_page_title)){
                $this->insert_or_update('frontend_settings','home_page_title',$request->home_page_title);
            }if (isset($request->contact_email) && !empty($request->contact_email)){
                $this->insert_or_update('frontend_settings','contact_email',$request->contact_email);
            }if (isset($request->contact_email) && !empty($request->contact_email)){
                $this->insert_or_update('frontend_settings','contact_number',$request->contact_number);
            }if (isset($request->contact_address) && !empty($request->contact_address)){
                $this->insert_or_update('frontend_settings','contact_address',$request->contact_address);
            }if (isset($request->facebook_link) && !empty($request->facebook_link)){
                $this->insert_or_update('frontend_settings','facebook_link',$request->facebook_link);
            }if (isset($request->twitter_link) && !empty($request->twitter_link)){
                $this->insert_or_update('frontend_settings','twitter_link',$request->twitter_link);
            }if (isset($request->instagram_link) && !empty($request->instagram_link)){
                $this->insert_or_update('frontend_settings','instagram_link',$request->instagram_link);
            }if (isset($request->google_link) && !empty($request->google_link)){
                $this->insert_or_update('frontend_settings','google_link',$request->google_link);
            }if (isset($request->google_map_key) && !empty($request->google_map_key)){
                $this->insert_or_update('frontend_settings','google_map_key',$request->google_map_key);
            }if (isset($request->description) && !empty($request->description)){
                $this->insert_or_update('frontend_settings','description',$request->description);
            }if (isset($request->footer_descrpition) && !empty($request->footer_descrpition)){
                $this->insert_or_update('frontend_settings','footer_descrpition',$request->footer_descrpition);
            }if (isset($request->copy_right_text) && !empty($request->copy_right_text)){
                $this->insert_or_update('frontend_settings','copy_right_text',$request->copy_right_text);
            }if (isset($request->about_us) && !empty($request->about_us)){
                $this->insert_or_update('frontend_settings','about_us',$request->about_us);
            }if (isset($request->latitude) && !empty($request->latitude)){
                $this->insert_or_update('frontend_settings','latitude',$request->latitude);
            }if (isset($request->longitude) && !empty($request->longitude)){
                $this->insert_or_update('frontend_settings','longitude',$request->longitude);
            }
            if (isset($request->app_logo) && !empty($request->app_logo)){
                $check_app_logo = DB::table('sys_system_settings')->where(['option_key'=>'app_logo','option_group'=>'frontend_settings'])->first();
                $option_value = fileUpload($request->app_logo,'Backend/images/application/',NULL,$check_app_logo->option_value ?? '');
                $this->insert_or_update('frontend_settings','app_logo',$option_value);
            }
            if (isset($request->favicon_logo) && !empty($request->favicon_logo)){
                $check_favicon_logo = DB::table('sys_system_settings')->where(['option_key'=>'favicon_logo','option_group'=>'frontend_settings'])->first();
                $option_value = fileUpload($request->favicon_logo,'Backend/images/application/',NULL,$check_favicon_logo->option_value ?? '');
                $this->insert_or_update('frontend_settings','favicon_logo',$option_value);
            }
            $response = $this->responseData(TRUE,__('Settings saved successfully.'));
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,__('Settings save failed.'));
        }

        return response()->json($response);

    }

    public function adminSettingsSave(Request $request){

        try {
            if (isset($request->app_title) && !empty($request->app_title)){
                $this->insert_or_update('admin_settings','app_title',$request->app_title);
            }if (isset($request->contact_email) && !empty($request->contact_email)){
                $this->insert_or_update('admin_settings','contact_email',$request->contact_email);
            }if (isset($request->contact_email) && !empty($request->contact_email)){
                $this->insert_or_update('admin_settings','contact_number',$request->contact_number);
            }if (isset($request->contact_address) && !empty($request->contact_address)){
                $this->insert_or_update('admin_settings','contact_address',$request->contact_address);
            }if (isset($request->description) && !empty($request->description)){
                $this->insert_or_update('admin_settings','description',$request->description);
            }if (isset($request->copy_right_text) && !empty($request->copy_right_text)){
                $this->insert_or_update('admin_settings','copy_right_text',$request->copy_right_text);
            }if (isset($request->about_us) && !empty($request->about_us)){
                $this->insert_or_update('admin_settings','about_us',$request->about_us);
            }if (isset($request->map_key) && !empty($request->map_key)){
                $this->insert_or_update('admin_settings','map_key',$request->map_key);
            }
            if (isset($request->app_logo_large) && !empty($request->app_logo_large)){
                $check_app_logo = DB::table('sys_system_settings')->where(['option_key'=>'app_logo_large','option_group'=>'admin_settings'])->first();
                $option_value = fileUpload($request->app_logo_large,'Backend/images/application/',NULL,$check_app_logo->option_value ?? '');
                $this->insert_or_update('admin_settings','app_logo_large',$option_value);
            }
            if (isset($request->app_logo_small) && !empty($request->app_logo_small)){
                $check_app_logo = DB::table('sys_system_settings')->where(['option_key'=>'app_logo_small','option_group'=>'admin_settings'])->first();
                $option_value = fileUpload($request->app_logo_small,'Backend/images/application/',NULL,$check_app_logo->option_value ?? '');
                $this->insert_or_update('admin_settings','app_logo_small',$option_value);
            }
            if (isset($request->favicon_logo) && !empty($request->favicon_logo)){
                $check_favicon_logo = DB::table('sys_system_settings')->where(['option_key'=>'favicon_logo','option_group'=>'admin_settings'])->first();
                $option_value = fileUpload($request->favicon_logo,'Backend/images/application/',NULL,$check_favicon_logo->option_value ?? '');
                $this->insert_or_update('admin_settings','favicon_logo',$option_value);
            }
            $response = $this->responseData(TRUE,__('Settings saved successfully.'));
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,$exception->getMessage());
        }

        return response()->json($response);

    }

    public function paymentSettingsSave(Request $request){
        try {
            if (isset($request->payment_method) && !empty($request->payment_method)){
                $this->insert_or_update('payment_settings','payment_method',$request->payment_method);
            }if (isset($request->brain_tree_env) && !empty($request->brain_tree_env)){
                $this->insert_or_update('payment_settings','brain_tree_env',$request->brain_tree_env);
            }if (isset($request->brain_tree_merchant_id) && !empty($request->brain_tree_merchant_id)){
                $this->insert_or_update('payment_settings','brain_tree_merchant_id',$request->brain_tree_merchant_id);
            }if (isset($request->brain_tree_public_key) && !empty($request->brain_tree_public_key)){
                $this->insert_or_update('payment_settings','brain_tree_public_key',$request->brain_tree_public_key);
            }if (isset($request->brain_tree_private_key) && !empty($request->brain_tree_private_key)){
                $this->insert_or_update('payment_settings','brain_tree_private_key',$request->brain_tree_private_key);
            }if (isset($request->stripe_key) && !empty($request->stripe_key)){
                $this->insert_or_update('payment_settings','stripe_key',$request->stripe_key);
            }if (isset($request->stripe_secret) && !empty($request->stripe_secret)){
                $this->insert_or_update('payment_settings','stripe_secret',$request->stripe_secret);
            }
            $response = $this->responseData(TRUE,__('Settings saved successfully.'));
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,__('Settings save failed.'));
        }

        return $response;

    }

    public function adminCurrencySettingsSave(Request $request){
        try {
            if(!empty($request->social_login)){
                $this->insert_or_update('admin_settings','social_login_enable',TRUE);
            }else{
                $this->insert_or_update('admin_settings','social_login_enable',FALSE);
            }

            if(!empty($request->email_verification)){
                $this->insert_or_update('admin_settings','email_verification_enable',TRUE);
            }else{
                $this->insert_or_update('admin_settings','email_verification_enable',FALSE);
            }

            if (!empty($request->currency)){
                $this->insert_or_update('admin_settings','currency',$request->currency);
            }
            $response = $this->responseData(TRUE,__('Settings saved successfully.'));
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,__('Settings save failed.'));
        }
        return $response;

    }

    public function authSettingsSave(Request $request){
        try {
            if (isset($request->facebook_app_id) && !empty($request->facebook_app_id)){
                $this->insert_or_update('authentication','facebook_app_id',$request->facebook_app_id);
            }if (isset($request->facebook_app_secret) && !empty($request->facebook_app_secret)){
                $this->insert_or_update('authentication','facebook_app_secret',$request->facebook_app_secret);
            }if (isset($request->facebook_redirect) && !empty($request->facebook_redirect)){
                $this->insert_or_update('authentication','facebook_redirect',$request->facebook_redirect);
            }if (isset($request->google_client_id) && !empty($request->google_client_id)){
                $this->insert_or_update('authentication','google_client_id',$request->google_client_id);
            }if (isset($request->google_client_secret) && !empty($request->google_client_secret)){
                $this->insert_or_update('authentication','google_client_secret',$request->google_client_secret);
            }if (isset($request->google_redirect) && !empty($request->google_redirect)){
                $this->insert_or_update('authentication','google_redirect',$request->google_redirect);
            }
            $response = $this->responseData(TRUE,__('Settings saved successfully.'));
        }catch (\Exception $exception){
            $response = $this->responseData(FALSE,__('Settings save failed.'));
        }
        return $response;

    }

    public function howItWorks(){
        $data['how_it_works'] = DB::table('spotlist_how_it_works')->get();
        $data['total_counts'] = DB::table('spotlist_how_it_works')->count();
        return view('Backend.settings.how_it_works',$data);
    }

    public function howItWorksSave(Request $request){
        $response = $this->service->howItWorkDataSave($request);
        return response()->json($response);
    }

    public function aboutUs(){
        $data['about_us'] = __options(['about_us_settings']);
        return view('Backend.settings.about_us',$data);
    }

    public function aboutUsSave(Request $request){
        $response = $this->service->aboutUsSettingSave($request);
        return response()->json($response);
    }

    public function teams(Request $request){

        $team_data = $this->service->teamList();

        if ($request->ajax()){

            return datatables($team_data)
                ->editColumn('picture',function ($item){
                    return '<img src="'.$item->picture.'" class="rounded-circle" width="50" height=50>';
                })->editColumn('status',function ($item){
                    return $item->status == 'Active' ? '<span class="badge badge-success">'.$item->status.'</span>' : '<span class="badge badge-warning">'.$item->status.'</span>';
                })
                ->rawColumns(['status','picture'])
                ->make(TRUE);
        }
        return view('Backend.settings.teams');
    }

    public function getTeamData($id=NULL){
        $data = [];
        if ($id !== NULL){
            $data['teamData'] = $this->service->teamList($id);
        }
        return view('Backend.settings.teams_view',$data);
    }

    public function saveTeamData(Request $request){
        $response = $this->service->saveTeamData($request);
        return response()->json($response);
    }
}
