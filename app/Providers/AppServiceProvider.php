<?php

namespace App\Providers;

use Braintree\Configuration;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            $payment_settings = __options(['payment_settings']);
            if (!empty($payment_settings)){
                Configuration::environment($payment_settings->brain_tree_env ?? '');
                Configuration::merchantId($payment_settings->brain_tree_merchant_id ?? '');
                Configuration::publicKey($payment_settings->brain_tree_public_key ?? '');
                Configuration::privateKey($payment_settings->brain_tree_private_key ?? '');
            }
            $social_settings = __options(['authentication']);
            if (isset($social_settings)){
                config('services.facebook.client_id',$social_settings->facebook_app_id ?? env('FACEBOOK_APP_ID'));
                config('services.facebook.client_secret',$social_settings->facebook_app_secret ?? env('FACEBOOK_APP_SECRET'));
                config('services.facebook.redirect',$social_settings->facebook_redirect ?? env('FACEBOOK_REDIRECT'));
                config('services.google.client_id',$social_settings->google_client_id ?? env('GOOGLE_CLIENT_ID'));
                config('services.google.client_secret',$social_settings->google_client_secret ?? env('GOOGLE_CLIENT_SECRET'));
                config('services.google.redirect',$social_settings->google_redirect ?? env('GOOGLE_REDIRECT'));
            }
        }catch (\Exception $exception){
            Configuration::environment(config('services.braintree.environment'));
            Configuration::merchantId(config('services.braintree.merchant_id'));
            Configuration::publicKey(config('services.braintree.public_key'));
            Configuration::privateKey(config('services.braintree.private_key'));
        }

    }
}
