@if(isset($types) && !empty($types[0]))
    <div class="list-gallery list-sub">
        <h2>{{__('Services')}}</h2>
        <div class="row">
            @foreach($types as $type)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="product-item">
                            <div class="product-img">
                                <img src="{{getImage($type->fitness_service_image)}}" alt="" height="200">
                                @if(isset(Auth::user()->id))
                                <a href="javascript:void(0)" class="add-cart {{ checkDirectoryItemLike(Auth::user()->id,$directory->spotlist_directory_id,$type->spotlist_directory_type_fitness_id) == TRUE ? 'text-success' :''}}"  data-directory-type-id ="{{$type->spotlist_directory_type_fitness_id}}"
                                   data-directory-id="{{$directory->spotlist_directory_id}}" data-liked="{{ checkDirectoryItemLike(Auth::user()->id,$directory->spotlist_directory_id,$type->spotlist_directory_type_fitness_id)}}">
                                    <i class="fa fa-heart"></i>
                                </a>
                                @endif
                            </div>
                            <div class="product-content">
                                <h5>{{$type->fitness_service_name}}</h5>
                                @if(isset($type->fitness_service_price))
                                    <span>{{__('Price: ')}}{{$type->fitness_service_price}} ( {{get_currency()}} )</span>
                                @endif
                                <div class="ratting-area">
                                    <p>
                                        <span><i class="fa fa-thumbs-o-up text-success"></i></span>
                                         <span class="pl-1" data-like="{{$type->total_like}}" id="{{$type->spotlist_directory_type_fitness_id}}">{{$type->total_like > 1000 ? $type->total_like.'k' : $type->total_like}}</span>
                                    </p>
                                </div>
                            </div>
                            <button class="btn btn-block btn-info view_details" data-id="{{$type->spotlist_directory_type_fitness_id}}" data-type="fitness">{{__('View details')}}</button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
