<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Service\MailService;
use App\Models\User;
use App\Models\UserVerificationCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthService extends Controller {
    private $data = [];

    public function userLogin(Request $request){
        $settings = __options(['admin_settings']);
        $response = [
            'success' => FALSE,
            'data' => [],
            'message' => ''
        ];
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            if (auth::user()->status == 'Pending') {
                if (isset($settings->email_verification_enable) && $settings->email_verification_enable == TRUE) {
                    if (Auth::user()->email_verified == STATUS_PENDING) {
                        $user = Auth::user();
                        $mail_key = randomNumber(6);
                        UserVerificationCode::where(['user_id' => auth::user()->id])->update(['status' => STATUS_SUCCESS]);
                        UserVerificationCode::create(['user_id' => auth::user()->id, 'code' => $mail_key, 'type' => 1, 'status' => STATUS_PENDING, 'expired_at' => date('Y-m-d', strtotime('+15 days'))]);
                        $userName = $user->name;
                        $userEmail = $user->email;
                        $subject = __('Spotlisting Email Verification.');

                        $userData['message'] = __('Hello! ') . $userName . __(' Please Verify Your Email.');
                        $userData['verification_code'] = $mail_key;
                        $userData['email'] = $userEmail;
                        $userData['name'] = $userName;
                        MailService::sendEmailVerificationMailProcess($userEmail,$userData,$subject);
                        $data['message'] = __('Your email is not verified. Please verify your email to get full access.');
                        $response['success'] = TRUE;
                        $response['message'] = $data['message'];
                    }
                }
                $data['message'] = __('Your account is pending. Please wait for admin approval.');
                $response['success'] = TRUE;
                $response['message'] = $data['message'];
            }
            elseif (Auth::user()->status == 'Active'){
                $response['success'] = TRUE;
                $response['message'] = __('Login successful.');
                $response['data'] = Auth::user();

            } elseif (Auth::user()->status == 'Suspended') {
                $response['message'] = __('Your Account has been suspended. please contact support team to active again!');
            } elseif (Auth::user()->status == 'Deleted') {
                $response['message'] = __('Your Account has been deleted. please contact support team to active again!');
            }
            elseif (auth::user()->status == 'Blocked') {
                $response['message'] = __('You are blocked. Contact with the support team.');
            }

        } else {
            $response['message'] = __('Email or Password Not matched!');
        }
        return $response;
    }
    public function userRegistration(Request $request){
        $admin_settings = __options(['admin_settings']);
        $response = [
            'success' => FALSE,
            'data' => [],
            'message' => ''
        ];
        DB::beginTransaction();
        try {
            $user_data = [
                'username' => $request->name,
                'email' => $request->email,
                'name' => $request->name,
                'default_module_id' => intval($request->module_id),
                'remember_token' => md5($request->email . uniqid() . randomString(5)),
                'password' => Hash::make($request->password),
                'password_changed_date' => Carbon::now(),
            ];
            if ( isset($admin_settings->email_verification_enable) && $admin_settings->email_verification_enable == TRUE ) {
                $user_data['status'] = 'Pending';
            }else{
                $user_data['status'] = 'Active';
            }
                $user = User::create($user_data);
            if ( isset($admin_settings->email_verification_enable) && $admin_settings->email_verification_enable == TRUE ){
                $this->userMailVerification($user);
                $response['success'] = TRUE;
                $response['mail_verification'] = TRUE;
                $response['message'] = __('Please verify your email first to get login.');
            }else{
                $response['success'] = TRUE;
                $response['mail_verification'] = FALSE;
                $response['message'] = __('Registration successful.');
            }

            DB::commit();

        }catch (\Exception $exception){
            DB::rollBack();
            $response['success'] = FALSE;
//            $response['message'] = __('Registration failed!');
            $response['message'] = $exception->getMessage();
        }
        return $response;
    }

    public function loginWithSocial($user,$driver){
        if ($driver == 'facebook'){
            return $this->socialLoginAttempt($user,'facebook_id');
        }elseif($driver == 'google'){
            return $this->socialLoginAttempt($user,'google_id');
        }
    }

    private function socialLoginAttempt($user,$driver_key){

        $isUser = User::where($driver_key, $user->id)->first();
        if($isUser){
            Auth::login($isUser);
            return auth()->user();
        }else{
           return $this->userSocialCreateAccount($user,$driver_key);
        }
    }

    public function userSocialCreateAccount($userSocial,$driver_key){
        $image_name = '';
        if (isset($userSocial->avatar_original)){
            $profile_contents = file_get_contents($userSocial->avatar_original);
            $image_name = 'Backend/images/users/'.md5($userSocial->user['email']) . '.jpg';
            file_put_contents($image_name, $profile_contents);
        }

        $user_data = [
            'username' => $userSocial->user['name'],
            'email' =>  $userSocial->user['email'],
            'name' => $userSocial->user['name'],
            'user_image' => $image_name,
             $driver_key => $userSocial->id,
             'is_social' => TRUE,
            'remember_token' => md5($userSocial->user['email'] . uniqid() . randomString(5)),
            'default_module_id' => 4,
            'password' => bcrypt(123456),
            'password_changed_date' => Carbon::now(),
            'email_verified' => STATUS_SUCCESS,
            'status'  => 'Active',
        ];

        $createUser = User::create($user_data);
        Auth::login($createUser);
        return $createUser;
    }

    public function userMailVerification($user) {
        $mail_key = randomNumber(6);
        UserVerificationCode::create(['user_id' => $user->id, 'code' => $mail_key, 'type' => 1, 'status' => STATUS_PENDING, 'expired_at' => date('Y-m-d', strtotime('+15 days'))]);
        $userName = $user->name;
        $userEmail = $user->email;
        $subject = __('Spotlisting Email Verification.');

        $userData['message'] = __('Hello! ') . $userName . __(' Please Verify Your Email.');
        $userData['verification_code'] = $mail_key;
        $userData['email'] = $userEmail;
        $userData['name'] = $userName;
        MailService::sendEmailVerificationMailProcess($userEmail,$userData,$subject);
    }

    public function userVerifyEmail($code) {
        $code = $this->checkValidId($code);
        $user_verification = UserVerificationCode::where(['code' => $code])->first();
        $response = [];
        if ($user_verification) {
            DB::beginTransaction();
            try {
                UserVerificationCode::where(['id' => $user_verification->user_id])->update(['status' => STATUS_SUCCESS]);
                $user = User::where('id',$user_verification->user_id)->first();
                if (!empty($user)) {
                    if ($user->email_verified == STATUS_PENDING) {
                        $user_update = User::where('id',$user_verification->user_id)->update(['email_verified' => STATUS_SUCCESS, 'status' => 'Active']);
                        $response = ['status' => TRUE, 'message' => __('Email successfully verified.')];
                    } else {
                        $response = ['status' => TRUE, 'message' => __('You already verified email!')];
                    }
                } else {
                    $response = ['status' => FALSE, 'message' => __('Email Verification Failed')];
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                $response = ['status' => FALSE, 'data' => [], 'message' => $e->getMessage()];
            }
        } else {
            $response = ['status' => FALSE, 'message' => __('Verification Code Not Found!')];
        }
        return $response;
    }

    private function  checkValidId($id){
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return ['success'=>false];
        }
        return $id;

    }

    public function sendForgotPasswordMail($request) {
        try {
            $user = User::where(['email' => $request->email])->first();

            if ($user) {
                // send verifyemail
                $userName = $user->username;
                $userEmail = $request->email;
                $subject = __('Forget Password');
                $data['name'] = $userName;
                $data['remember_token'] = $user->remember_token;
                MailService::sendResetPasswordMailProcess($userEmail,$data,$subject);

                return [
                    'success' => TRUE,
                    'role'    => $user->role,
                    'message' => __('Mail sent successfully.')
                ];
            } else {
                return [
                    'success' => FALSE,
                    'message' => __('Your email is not correct !')
                ];
            }
        } catch (\Exception $e) {
            return [
                'success' => FALSE,
                'message' => __('Something went wrong . Please try again!')
            ];
        }
    }

    public function changePassword(Request $request){
        try {
            $user = User::where(['remember_token' => $request->remember_token])->first();
            if ($user) {
                $update_password['remember_token'] = md5($user->email . uniqid() . randomString(5));
                $update_password['password'] = Hash::make($request->password);
                $update_password['email_verified'] = STATUS_SUCCESS;

                $updated = User::where(['id' => $user->id, 'remember_token' => $user->remember_token])->update($update_password);

                if ($updated) {
                    return [
                        'success' => TRUE,
                        'message' => __('Password changed successfully.')
                    ];
                } else {
                    return [
                        'success' => FALSE,
                        'message' => __('Password not changed try again.')
                    ];
                }
            } else {
                return [
                    'success' => FALSE,
                    'message' => __('Password not changed try again.')
                ];
            }
        } catch (\Exception $e) {
            return [
                'success' => FALSE,
                'message' => __('Something went wrong . Please try again!')
            ];
        }
    }


}
