@extends('FontEnd.master')
@section('style')
    <style>
        .parsley-required,.parsley-type,.parsley-equalto{
            color: red;
        }
    </style>
@endsection
@section('content')

    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Sign Up')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- start login-pg-section -->
    <div class="login-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col col-12">
                    <div class="login-left">
                        <div class="auth-img">
                            <img src="{{asset('FontEnd/')}}/assets/images/login.svg" alt class="lob" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col col-12">
                    <div class="login-wrap contact-area-s2">
                        @if(Session::has('message'))
                            <div class="alert alert-success alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!! Session::get('message') !!}
                            </div>
                        @endif
                        @if(Session::has('dismiss'))
                            <div class="alert alert-danger alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('dismiss')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible text-center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('success')}}
                            </div>
                        @endif
                        @if(count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible text-center" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {!! $error !!}
                                </div>
                            @endforeach
                        @endif
                        <h2>{{__('Signup your account')}}</h2>
                        <form method="post" action="{{route('user.registration.save')}}" class="parsley-examples">
                            @csrf
                            <div class="col-12">
                                <label for="">{{__('Register as')}}</label>
                                <select class="form-control" name="module_id">
                                    <option value="{{SYSTEM_USER}}">{{__('User')}}</option>
                                    <option value="{{LISTING_MANAGER}}">{{__('Listing Manager')}}</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <label for="">{{__('Full Name')}}</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Enter Your Full Name" value="{{ old('name') }}" parsley-trigger="change" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12">
                                <label for="">{{__('E-mail Address')}}</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="your@email.com" value="{{__(old('email'))}}" parsley-trigger="change" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12">
                                <label for="">{{__('Password')}}</label>
                                <input type="password" name="password" id="pass1" class="form-control" placeholder="Minimum 6 character" parsley-trigger="change" required>
                                @error('password')
                                     <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12">
                                <label for="">{{__('Confirm Password')}}</label>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Minimum 6 character" data-parsley-equalto="#pass1" required>
                            </div>
                            <div class="submit-btn col-12">
                                <button type="submit" class="btn theme-btn">{{__('Sign Up')}}</button>
                            </div>
                            <div class="col-12 sign-b">
                                <p>{{__('Already have an account?')}} <a href="{{route('login')}}">{{__('Return to Sign In')}}</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
@endsection
