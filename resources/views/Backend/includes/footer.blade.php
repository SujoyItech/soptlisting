@php($settings = __options(['admin_settings']))
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                @if(isset($settings->copy_right_text))
                    {{$settings->copy_right_text}}
                @else
                    <script>document.write(new Date().getFullYear())</script> &copy; Itech Soft Solution
                @endif
            </div>
            <div class="col-md-6">

            </div>
        </div>
    </div>
</footer>
