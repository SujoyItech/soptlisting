@extends('FontEnd.master',['menu'=>'about'])
@section('style')
    <style>
        .team-text ul li{
            padding: 3px !important;
        }
        .team-text ul li:first-child a {
            margin-right: 0px !important;
        }
    </style>
@endsection
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('About Us')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- .about-area-start -->
    <div class="about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-img">
                        <img src="{{ isset($how_we_work->about_us_picture) ? asset($how_we_work->about_us_picture) : ''}}" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-text">
                        <h2>{{$how_we_work->about_us_title ?? ''}}</h2>
                        <p>{{$how_we_work->about_us_description ?? ''}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .about-area-start -->

    @if(isset($teams) && !empty($teams[0]))
    <!-- team-area strat -->
    <div class="team-area section-padding">
        <div class="container">
            <div class="col-12">
                <div class="section-title sec-title-middle text-center">
                    <h2>{{__('Our Team')}}</h2>
                </div>
            </div>
            <div class="row">
                @foreach($teams as $team)
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="team-item">
                            <div class="team-img">
                                <img src="{{ isset($team->picture) ? asset($team->picture) : ''}}" alt="" height="300">
                            </div>
                            <div class="team-text">
                                <h3>{{$team->name ?? ''}}</h3>
                                <span>{{$team->designation ?? ''}}</span>
                                <ul>
                                    @if(!empty($team->facebook_url))
                                        <li><a href="{{$team->facebook_url}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    @endif
                                    @if(!empty($team->instagram_url))
                                        <li><a href="{{$team->instagram_url}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                    @endif
                                    @if(!empty($team->twitter_url))
                                        <li><a href="{{$team->twitter_url}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    @endif
                                    @if(!empty($team->linkedin_url))
                                        <li><a href="{{$team->linkedin_url}}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                    @endif
                                    @if(!empty($team->whatsapp_url))
                                        <li><a href="{{$team->whatsapp_url}}" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- team-area strat -->
    @endif
    @if(isset($how_it_works) && !empty($how_it_works[0]))
    <!-- work-area start -->
    <div class="work-area-2 section-padding">
        <div class="container">
            <div class="section-title sec-title-middle text-center">
                <h2>{{__('How It Works')}}</h2>
            </div>
            <div class="work-wrap">
                <div class="row">
                    @php($i = 1)
                    @foreach($how_it_works as $how_it_work)
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="work-sub">
                                <div class="work-item">
                                    <div class="work-img">
                                        <img src="{{isset($how_it_work->icon) ? asset($how_it_work->icon) : ''}}" alt="">
                                        <div class="work-thumb">
                                            <span>{{$i++}}</span>
                                        </div>
                                    </div>
                                    <div class="work-text">
                                        <h2>{{$how_it_work->title ?? ''}}</h2>
                                        <p>{{$how_it_work->description}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- work-area end -->
    @endif
    @if(isset($recent_reviews) && !empty($recent_reviews[0]))
    <!-- testimonial-area start -->
    <div class="testimonial-area v2 section-padding">
        <div class="container">
            <div class="section-title sec-title-middle text-center">
                <h2>{{__('Testimonials')}}</h2>
            </div>
            <div class="row">
                <div class="col-xs-12 col-lg-12 col-md-12 col-12">
                    <div class="test-wrap">
                        <div class="testimonial-slider2 clearfix">
                                @foreach($recent_reviews as $review)
                                    <div class="grid">
                                        <div class="client-info">
                                            <div class="client-img">
                                                <img src="{{isset($review->user_image) && file_exists($review->user_image) ? asset($review->user_image) : asset('Backend/images/users/avatar.svg')}}" alt="" height="50">
                                            </div>
                                            <div class="client-text">
                                                <h5>{{$review->username ?? ''}}</h5>
                                                <span>{{isset($review->created_at) ? Carbon\Carbon::parse($review->created_at)->diffForHumans() : ''}}</span>
                                            </div>
                                        </div>
                                        <div class="ratting">
                                            <ul>
                                                @for($x=0; $x<5 ; $x++)
                                                    @if(floor($review->rating)- $x >= 1 )
                                                        <li><i class="fa fa-star"></i></li>
                                                    @elseif($review->rating - $x > 0)
                                                        <li><i class="fa fa-star-half-o"></i></li>
                                                    @else
                                                        <li><i class="fa fa-star-o"></i></li>
                                                    @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="quote">
                                            <p>{{$review->review ?? ''}}</p>
                                        </div>
                                    </div>
                                @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- testimonial-area end -->
    @endif
    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')
@endsection
