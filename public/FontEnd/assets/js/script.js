(function ($) {

    "use strict";

    $('.select').niceSelect();

    $(".menu-toggler").on("click", function (e) {
        $('body').toggleClass('side-menu');
        e.stopPropogation();
    });

    $(".menu-close").on("click", function (e) {
        $('body').removeClass('side-menu');
        e.stopPropogation();
    });


    // Parallax background
    function bgParallax() {
        if ($(".parallax").length) {
            $(".parallax").each(function () {
                var height = $(this).position().top;
                var resize = height - $(window).scrollTop();
                var doParallax = -(resize / 5);
                var positionValue = doParallax + "px";
                var img = $(this).data("bg-image");

                $(this).css({
                    backgroundImage: "url(" + img + ")",
                    backgroundPosition: "50%" + positionValue,
                    backgroundSize: "cover"
                });
            });
        }
    }


    // Hero slider background setting
    function sliderBgSetting() {
        if ($(".hero-slider .slide").length) {
            $(".hero-slider .slide").each(function () {
                var $this = $(this);
                var img = $this.find(".slider-bg").attr("src");

                $this.css({
                    backgroundImage: "url(" + img + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "center center"
                })
            });
        }
    }


    //Setting hero slider
    function heroSlider() {
        if ($(".hero-slider").length) {
            $(".hero-slider").slick({
                autoplay: true,
                autoplaySpeed: 6000,
                pauseOnHover: true,
                arrows: true,
                prevArrow: '<button type="button" class="slick-prev">Previous</button>',
                nextArrow: '<button type="button" class="slick-next">Next</button>',
                dots: false,
                fade: true,
                cssEase: 'linear'
            });
        }
    }

    // // stickey menu
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop(),
            mainHeader = $('#sticky-header'),
            mainHeaderHeight = mainHeader.innerHeight();

        // console.log(mainHeader.innerHeight());
        if (scroll > 1) {
            $("#sticky-header").addClass("sticky");
        } else {
            $("#sticky-header").removeClass("sticky");
        }
    });

    /*------------------------------------------
        = HIDE PRELOADER
    -------------------------------------------*/
    function pageLoader() {
        if ($('.page-loader').length) {
            $('.page-loader').delay(100).fadeOut(500, function () {

                //Active heor slider
                heroSlider();

            });
        }
    }

    /*================================
    slicknav
    ==================================*/

    $('.main-menu .nav_mobile_menu').slicknav({
        label: '',
        duration: 1000,
        easingOpen: "easeOutBounce", //available with jQuery UI
        prependTo: '.mobile_menu'
    });

    /*------------------------------------------
        = WOW ANIMATION SETTING
    -------------------------------------------*/
    var wow = new WOW({
        boxClass: 'wow',      // default
        animateClass: 'animated', // default
        offset: 0,          // default
        mobile: true,       // default
        live: true        // default
    });

    /*------------------------------------------
        = CONTACT FORM SUBMISSION
    -------------------------------------------*/
    if ($("#contact-form").length) {
        $("#contact-form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },

                fname: "required",

                lname: "required",

                subject: "required",

                email: "required",

                address: "required",
            },

            messages: {
                fname: "Please enter your First name",
                lname: "Please enter your Last name",
                subject: "Please enter your Subject",
                email: "Please enter your email address",
                address: "Please enter your address",
            },

            submitHandler: function (form) {
                $.ajax({
                    type: "POST",
                    url: "mail.php",
                    data: $(form).serialize(),
                    success: function () {
                        $("#loader").hide();
                        $("#success").slideDown("slow");
                        setTimeout(function () {
                            $("#success").slideUp("slow");
                        }, 3000);
                        form.reset();
                    },
                    error: function () {
                        $("#loader").hide();
                        $("#error").slideDown("slow");
                        setTimeout(function () {
                            $("#error").slideUp("slow");
                        }, 3000);
                    }
                });
                return false; // required to block normal submit since you used ajax
            }

        });
    }


    /*------------------------------------------
        = STICKY HEADER
    -------------------------------------------*/

    // Function for clone an element for sticky menu
    function cloneNavForSticyMenu($ele, $newElmClass) {
        $ele.addClass('original').clone().insertAfter($ele).addClass($newElmClass).removeClass('original');
    }

    // clone home style 1 navigation for sticky menu
    if ($('.site-header .navigation').length) {
        cloneNavForSticyMenu($('.site-header .navigation'), "sticky-header");
    }

    var lastScrollTop = '';

    function stickyMenu($targetMenu, $toggleClass) {
        var st = $(window).scrollTop();
        var mainMenuTop = $('.site-header .navigation');

        if ($(window).scrollTop() > 1000) {
            if (st > lastScrollTop) {
                // hide sticky menu on scroll down
                $targetMenu.removeClass($toggleClass);

            } else {
                // active sticky menu on scroll up
                $targetMenu.addClass($toggleClass);
            }

        } else {
            $targetMenu.removeClass($toggleClass);
        }

        lastScrollTop = st;
    }

    /*==========================================================================
        WHEN WINDOW SCROLL
    ==========================================================================*/
    $(window).on("scroll", function () {
        toggleBackToTopBtn();

    });

    /*==========================================================================
        WHEN DOCUMENT LOADING
    ==========================================================================*/
    $(window).on('load', function () {

        pageLoader();

        sliderBgSetting();

        sortingGallery();

    });


    /*================================
     Gift-carousel
     ==================================*/
    function testimonial_carousel() {
        var owl = $(".testimonial-slider2");
        owl.owlCarousel({
            loop: true,
            margin: 30,
            nav: false,
            items: 5,
            smartSpeed: 2000,
            dots: true,
            autoplay: false,
            autoplayTimeout: 5000,
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                760: {
                    items: 2
                },
                1080: {
                    items: 2
                }
            }
        });
    }

    testimonial_carousel();


    $(document).ready(function () {

        $('.team-active ').owlCarousel({
            loop: true,
            margin: 30,
            autoplay: false,
            smartSpeed: 1500,
            mouseDrag: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
    });

    /*================================
     Gift-carousel
     ==================================*/
    function gift_carousel() {
        var owl = $(".Gift-carousel");
        owl.owlCarousel({
            loop: true,
            margin: 0,
            navText: false,
            nav: false,
            items: 5,
            smartSpeed: 1000,
            dots: false,
            autoplay: false,
            autoplayTimeout: 3000,
            responsive: {
                0: {
                    items: 3
                },
                480: {
                    items: 2
                },
                760: {
                    items: 4
                },
                1080: {
                    items: 6
                }
            }
        });
    }

    gift_carousel();
    /*------------------------------------------
        = FUNFACE
    -------------------------------------------*/
    if ($(".odometer").length) {
        $('.odometer').appear();
        $(document.body).on('appear', '.odometer', function (e) {
            var odo = $(".odometer");
            odo.each(function () {
                var countNumber = $(this).attr("data-count");
                $(this).html(countNumber);
            });
        });
    }
    /*------------------------------------------
        = BACK TO TOP BTN SETTING
    -------------------------------------------*/
    $("body").append("<a href='#' class='back-to-top'><i class='fa fa-angle-up'></i></a>");

    function toggleBackToTopBtn() {
        var amountScrolled = 1000;
        if ($(window).scrollTop() > amountScrolled) {
            $("a.back-to-top").fadeIn("slow");
        } else {
            $("a.back-to-top").fadeOut("slow");
        }
    }

    $(".back-to-top").on("click", function () {
        $("html,body").animate({
            scrollTop: 0
        }, 700);
        return false;
    })


    /*------------------------------------------
        = ACTIVE POPUP IMAGE
    -------------------------------------------*/
    if ($(".fancybox").length) {
        $(".fancybox").fancybox({
            openEffect: "elastic",
            closeEffect: "elastic",
            wrapCSS: "project-fancybox-title-style"
        });
    }

    /*------------------------------------------
        = POPUP VIDEO
    -------------------------------------------*/
    if ($(".video-play-btn").length) {
        $(".video-play-btn").on("click", function () {
            $.fancybox({
                href: this.href,
                type: $(this).data("type"),
                'title': this.title,
                helpers: {
                    title: {type: 'inside'},
                    media: {}
                },

                beforeShow: function () {
                    $(".fancybox-wrap").addClass("gallery-fancybox");
                }
            });
            return false
        });
    }


    /*------------------------------------------
        = POPUP YOUTUBE, VIMEO, GMAPS
    -------------------------------------------*/
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });


    /*------------------------------------------
        = ACTIVE POPUP IMAGE
    -------------------------------------------*/
    if ($(".popup-image").length) {
        $('.popup-image').magnificPopup({
            type: 'image',
            zoom: {
                enabled: true,

                duration: 300,
                easing: 'ease-in-out',
                opener: function (openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        });
    }


    /*------------------------------------------
        = ACTIVE GALLERY POPUP IMAGE
    -------------------------------------------*/
    if ($(".popup-gallery").length) {
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',

            gallery: {
                enabled: true
            },

            zoom: {
                enabled: true,

                duration: 300,
                easing: 'ease-in-out',
                opener: function (openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            }
        });
    }


    /*------------------------------------------
        = FUNCTION FORM SORTING GALLERY
    -------------------------------------------*/
    function sortingGallery() {
        if ($(".sortable-gallery .gallery-filters").length) {
            var $container = $('.gallery-container');
            $container.isotope({
                filter: '*',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false,
                }
            });

            $(".gallery-filters li a").on("click", function () {
                $('.gallery-filters li .current').removeClass('current');
                $(this).addClass('current');
                var selector = $(this).attr('data-filter');
                $container.isotope({
                    filter: selector,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false,
                    }
                });
                return false;
            });
        }
    }

    sortingGallery();

    /*------------------------------------------
        country-carousel
    -------------------------------------------*/
    if ($(".country-carousel".length)) {
        $(".country-carousel").owlCarousel({
            loop: !0,
            autoplaySpeed: 3e3,
            navSpeed: 3e3,
            paginationSpeed: 3e3,
            slideSpeed: 3e3,
            smartSpeed: 3e3,
            autoplay: false,
            margin: 30,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            dots: !1,
            responsive: {0: {items: 1}, 480: {items: 1}, 600: {items: 2}, 1024: {items: 2}, 1200: {items: 2}},
        });
    }

    /*------------------------------------------
        country-carousel
    -------------------------------------------*/
    if ($(".populer-carousel".length)) {
        $(".populer-carousel").owlCarousel({
            loop: !0,
            autoplaySpeed: 3e3,
            navSpeed: 3e3,
            paginationSpeed: 3e3,
            slideSpeed: 3e3,
            smartSpeed: 3e3,
            autoplay: false,
            margin: 30,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            dots: !1,
            responsive: {0: {items: 1}, 480: {items: 1}, 600: {items: 2}, 1024: {items: 2}, 1200: {items: 3}},
        });
    }

    /*------------------------------------------
    deals-carousel
    -------------------------------------------*/
    if ($(".deals-carousel".length)) {
        $(".deals-carousel").owlCarousel({
            loop: !0,
            autoplaySpeed: 3e3,
            navSpeed: 3e3,
            paginationSpeed: 3e3,
            slideSpeed: 3e3,
            smartSpeed: 3e3,
            autoplay: false,
            margin: 30,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            dots: !1,
            responsive: {0: {items: 1}, 480: {items: 1}, 600: {items: 1}, 1024: {items: 1}, 1200: {items: 4}},
        });
    }

    /*------------------------------------------
    deals-carousel
    -------------------------------------------*/
    if ($(".testimonial-slider".length)) {
        $(".testimonial-slider").owlCarousel({
            loop: !0,
            autoplaySpeed: 3e3,
            navSpeed: 3e3,
            paginationSpeed: 3e3,
            slideSpeed: 3e3,
            smartSpeed: 3e3,
            autoplay: false,
            margin: 30,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            dots: !1,
            responsive: {
                0: {items: 1},
                480: {items: 1},
                600: {items: 2},
                1024: {items: 2},
                1200: {items: 3},
                1500: {items: 3}
            },
        });
    }


    /*------------------------------------------
        = MASONRY GALLERY SETTING
    -------------------------------------------*/
    function masonryGridSetting() {
        if ($('.masonry-gallery').length) {
            var $grid = $('.masonry-gallery').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-item',
                percentPosition: true
            });

            $grid.imagesLoaded().progress(function () {
                $grid.masonry('layout');
            });
        }
    }

    // masonryGridSetting();

    $(document).on('click', function (event) {
        if (!$(event.target).closest('.map-wrapper').length) {
            $('.map-wrapper').removeClass('open');
        }
    });


    /*----------------------------
      price-slider active
     ------------------------------ */
    $("#slider-range").slider({
        range: true,
        min: 12,
        max: 200,
        values: [0, 100],
        slide: function (event, ui) {
            $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });


    $("#amount").val("$" + $("#slider-range").slider("values", 0) +
        " - $" + $("#slider-range").slider("values", 1));
    /*-- price range End --*/


    /*----------------------------
      distance-slider active
     ------------------------------ */
    $("#slider-range2").slider({
        range: true,
        min: 12,
        max: 200,
        values: [0, 100],
        slide: function (event, ui) {
            $("#amount2").val("KM" + ui.values[0] + " - KM" + ui.values[1]);
        }
    });


    $("#amount2").val("KM" + $("#slider-range2").slider("values", 0) +
        " - KM" + $("#slider-range2").slider("values", 1));
    /*-- distance range End --*/


    $(document).ready(function () {
        $("#date").datepicker();
    });


    /*-----------------------
       cart-plus-minus-button
     -------------------------*/

    $(".cart-plus-minus").append('<div class="dec qtybutton">-</div><div class="inc qtybutton">+</div>');
    $(".qtybutton").on("click", function () {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find("input").val(newVal);
    });


    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.show").each(function () {
            $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function () {
            $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
    });
    



    // var searchSticky = $('.list-wiget.search');
    // var $stickyrStopper = $('.sticky-stopper');
    // var gridBtns = $('.grid-btns');
    // var colWidth = $('.listing-col').width();
    // if ($('#mymap').length) {
    //     $(document).on('scroll', window, function () {
    //         if ($(window).scrollTop() >= 608) {
    //             searchSticky.addClass('search_sticky');
    //             gridBtns.addClass('grid_button_sticky');
    //             searchSticky.css('width', colWidth);
    //         } else {
    //             searchSticky.removeClass('search_sticky');
    //             gridBtns.removeClass('grid_button_sticky');
    //             searchSticky.css('width', 'unset');
    //         }
    //     });
    // } else {
    //     $(document).on('scroll', window, function () {
    //         if ($(window).scrollTop() >= 165) {
    //             searchSticky.addClass('search_sticky');
    //             gridBtns.addClass('grid_button_sticky');
    //             searchSticky.css('width', colWidth);
    //         } else {
    //             searchSticky.removeClass('search_sticky');
    //             gridBtns.removeClass('grid_button_sticky');
    //             searchSticky.css('width', 'unset');
    //         }
    //     });

    // }


   
})(window.jQuery);
