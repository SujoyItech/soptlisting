<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_category', function (Blueprint $table) {
            $table->increments('spotlist_category_id');
            $table->string('spotlist_category_name');
            $table->integer('parent_id')->default(0);
            $table->string('category_image')->nullable();
            $table->string('category_icon')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_category');
    }
}
