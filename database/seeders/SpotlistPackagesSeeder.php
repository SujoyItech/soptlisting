<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistPackagesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_packages')->delete();
        
        \DB::table('spotlist_packages')->insert(array (
            0 => 
            array (
                'spotlist_packages_id' => 1,
                'spotlist_packages_name' => 'Free subscription',
                'packages_type' => 'Free',
                'price' => '0.00',
                'day_validity' => 10,
                'num_of_listing' => 1,
                'num_of_categories' => NULL,
                'num_of_tags' => 1,
                'num_of_photos' => 5,
                'add_video_ability' => 1,
                'add_contact_form_ability' => 1,
                'is_recomended' => 1,
                'is_featured' => 1,
                'description' => NULL,
                'created_by' => 0,
                'updated_by' => 0,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'spotlist_packages_id' => 2,
                'spotlist_packages_name' => 'Basic',
                'packages_type' => 'Paid',
                'price' => '29.00',
                'day_validity' => 30,
                'num_of_listing' => 10,
                'num_of_categories' => NULL,
                'num_of_tags' => 10,
                'num_of_photos' => 10,
                'add_video_ability' => 1,
                'add_contact_form_ability' => 1,
                'is_recomended' => 1,
                'is_featured' => 1,
                'description' => NULL,
                'created_by' => 0,
                'updated_by' => 0,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'spotlist_packages_id' => 3,
                'spotlist_packages_name' => 'Standard',
                'packages_type' => 'Paid',
                'price' => '49.00',
                'day_validity' => 90,
                'num_of_listing' => 20,
                'num_of_categories' => NULL,
                'num_of_tags' => 10,
                'num_of_photos' => 20,
                'add_video_ability' => 1,
                'add_contact_form_ability' => 1,
                'is_recomended' => 1,
                'is_featured' => 1,
                'description' => NULL,
                'created_by' => 0,
                'updated_by' => 0,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'spotlist_packages_id' => 4,
                'spotlist_packages_name' => 'Premium',
                'packages_type' => 'Paid',
                'price' => '99.00',
                'day_validity' => 365,
                'num_of_listing' => 50,
                'num_of_categories' => NULL,
                'num_of_tags' => 50,
                'num_of_photos' => 50,
                'add_video_ability' => 1,
                'add_contact_form_ability' => 1,
                'is_recomended' => 1,
                'is_featured' => 1,
                'description' => NULL,
                'created_by' => 0,
                'updated_by' => 0,
                'status' => 'Active',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));

        
    }
}