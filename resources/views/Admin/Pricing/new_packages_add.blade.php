@extends('Backend.layouts.app',['menu'=>'pricing','sub_menu'=>'pricing_add'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-package-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New package')}}</a>
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-packages-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('Packages list')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-globe"></i> {{__('Add new packages')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('admin-package-save')}}" class="parsley-examples" method="POST" id="submit_packages" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="form-group ">
                        <label for="packages_type">{{__('Package type')}} <span class="text-danger">*</span></label>
                        @php($package_option = [''=>__('Select type'),'Free'=>__('Free'),'Paid'=>__('Paid')])
                        {!! Form::select('packages_type',  $package_option, $package->packages_type ?? '', ['class' => 'form-control','required']) !!}
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="spotlist_packages_name">{{__('Package Name')}}<span class="text-danger">*</span></label>
                        <input type="text" name="spotlist_packages_name" parsley-trigger="change" required value="{{$package->spotlist_packages_name ?? ''}}"
                               placeholder="Package name" class="form-control" id="spotlist_packages_name">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="price">{{__('Price')}}( {{get_currency()}} )<span class="text-danger">*</span></label>
                        <input type="number" name="price" parsley-trigger="change" required value="{{$package->price ?? ''}}"
                               placeholder="Price" step=".01" class="form-control" id="price">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="day_validity">{{__('Validity in days')}}<span class="text-danger">*</span></label>
                        <input type="number" name="day_validity" parsley-trigger="change" required value="{{$package->day_validity ?? ''}}"
                               placeholder="Validity in days" class="form-control" id="day_validity">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="num_of_listing">{{__('Number of listings')}} <span class="text-danger">*</span></label>
                        <input type="number" name="num_of_listing" parsley-trigger="change" required value="{{$package->num_of_listing ?? ''}}"
                               placeholder="Number of listing" class="form-control" id="num_of_listing">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="num_of_tags">{{__('Number of tags')}} <span class="text-danger">*</span></label>
                        <input type="number" name="num_of_tags" parsley-trigger="change" required value="{{$package->num_of_tags ?? ''}}"
                               placeholder="Number of tags" class="form-control" id="num_of_tags">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <label for="name">{{__('Number of photos')}} <span class="text-danger">*</span></label>
                        <input type="number" name="num_of_photos" parsley-trigger="change" required value="{{$package->num_of_photos ?? ''}}"
                               placeholder="Number of photos" class="form-control" id="num_of_photos">
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group ">
                        <label for="packages_type">{{__('Status')}} <span class="text-danger">*</span></label>
                        @php($package_status = ['Active'=>__('Active'),'Inactive'=>__('Inactive')])
                        {!! Form::select('status',  $package_status, $package->status ?? '', ['class' => 'form-control','required']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <div class="custom-checkbox">
                            <input type="checkbox"  id="addVideo" name="add_video_ability" {{isset($package->add_video_ability) && $package->add_video_ability == 1 ? 'checked': ''}}>
                            <label for="addVideo">{{__('Ability to add video')}}</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <div class="custom-checkbox">
                            <input type="checkbox"  id="addContactForm" name="add_contact_form_ability" {{isset($package->add_contact_form_ability) && $package->add_contact_form_ability == 1 ? 'checked': ''}}>
                            <label  for="addContactForm">{{__('Ability to add contact form')}}</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <div class="custom-checkbox">
                            <input type="checkbox"  id="isRecomended" name="is_recomended" {{isset($package->is_recomended) && $package->is_recomended == 1 ? 'checked': ''}}>
                            <label  for="isRecomended">{{__('Mark this packages as recommended')}}</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="form-group">
                        <div class="custom-checkbox">
                            <input type="checkbox"  id="isFeatured" name="is_featured" {{isset($package->is_featured) && $package->is_featured == 1 ? 'checked': ''}}>
                            <label for="isFeatured">{{__('Featured')}}</label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <input type="hidden" name="spotlist_packages_id" value="{{$package->spotlist_packages_id ?? ''}}" id="spotlist_packages_id">
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>

        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    if ($('#spotlist_packages_id').val().length !== 0){
                        formData.append('spotlist_packages_id',$('#spotlist_packages_id').val());
                    }
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            var redirect_url = "{{url('admin-packages-list')}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'error');
                        }
                    });
                }
            });
        });
    </script>
@endsection
