@extends('Backend.layouts.app',['menu'=>'directory','sub_menu'=>'create_directory'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" href="{{url('directory-add')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('Add new directory')}}</a>
                    @if(\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER)
                       <a class="btn btn-primary btn-xs text-white" href="{{url('list-manager-directory-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('Directory List')}}</a>
                    @else
                        <a class="btn btn-primary btn-xs text-white" href="{{url('directory-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('Directory List')}}</a>
                    @endif
                </div>
                <h4 class="page-title"><i class="fa fa-sitemap"></i> {{__('Directories')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER && availableListing(\Illuminate\Support\Facades\Auth::user()->id) <=0)
                    <div class="card-header bg-danger text-light text-center">
                        <span class="font-18"><i class="fa fa-warning"></i> {{__('Your listing limit is full.')}} <a href="{{route('listing-manager-pricing')}}">{{__('Please subscribe a package.')}}</a></span>
                    </div>
                @endif
                <div class="card-body">
                    <div class="row">
                        <div class="col-4">
                            <h4 class="header-title mb-3">{{__('Add a new directory')}} </h4>
                        </div>
                        @if(\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER)
                            @if(isset($directory_subscription))
                                <div class="col-8 text-right mb-3">
                                    <span class="pr-4">{{__('Directory: ') }} {{$directory_subscription->listing_item_used}} / {{$directory_subscription->num_of_listing }} </span>
                                    <span>{{__('Media : ')}} {{$directory_subscription->photos_used}} / {{$directory_subscription->num_of_photos }} </span><br>
                                </div>
                            @else
                                <div class="col-8 text-right mb-3">
                                    <p class="text-danger">{{__('You have not subscribed any package. Please subscribe now.')}}</p>
                                </div>
                            @endif
                        @endif
                    </div>
                        <div id="progressbarwizard">

                            <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3">
                                @if(isset($directory->spotlist_directory_id) && !empty($directory->spotlist_directory_id))
                                <li class="nav-item">
                                    <a href="#basic" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <span class="d-sm-inline">{{__('Basic')}}</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a href="#location" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <span class="d-sm-inline">{{__('Location')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#features" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <span class="d-sm-inline">{{__('Features')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#media" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <span class="d-sm-inline">{{__('Media')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#schedule" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <span class="d-sm-inline">{{__('Schedule')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#contact" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <span class="d-sm-inline">{{__('Contact')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#type" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <span class="d-sm-inline">{{__('Type')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#finish" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                        <span class="d-sm-inline">{{__('Finish')}}</span>
                                    </a>
                                </li>
                                @else
                                    <li class="nav-item">
                                        <a href="#basic" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" style="cursor: auto">
                                            <span class="d-sm-inline">{{__('Create new directory')}}</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>

                            <div class="tab-content b-0 mb-0 pt-0">
                                @if(isset($directory->spotlist_directory_id) && !empty($directory->spotlist_directory_id))
                                    <div id="bar" class="progress mb-3" style="height: 7px;">
                                        <div class="bar progress-bar progress-bar-striped progress-bar-animated bg-success"></div>
                                    </div>
                                @endif
                               @include('Common.Directory.DirectoryEntry.basic')
                                @if(isset($directory->spotlist_directory_id) && !empty($directory->spotlist_directory_id))
                                   @include('Common.Directory.DirectoryEntry.location')
                                   @include('Common.Directory.DirectoryEntry.features')
                                   @include('Common.Directory.DirectoryEntry.media')
                                   @include('Common.Directory.DirectoryEntry.schedule')
                                   @include('Common.Directory.DirectoryEntry.contact')
                                   @include('Common.Directory.DirectoryEntry.type')
                                   @include('Common.Directory.DirectoryEntry.finish')
                                @endif
                            </div>
                        </div>
                </div>
            </div>
        </div>
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>

        $('.submit_info').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);

                    if ($('#spotlist_directory_id').val().length !== 0){
                        formData.append('spotlist_directory_id',$('#spotlist_directory_id').val());
                    }
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                    });
                }
            });
        });
    </script>
@endsection
