<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectoryVsFeaturesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory_vs_features')->delete();
        
        \DB::table('spotlist_directory_vs_features')->insert(array (
            0 => 
            array (
                'spotlist_directory_id' => 1,
                'spotlist_features_id' => 1,
            ),
            1 => 
            array (
                'spotlist_directory_id' => 1,
                'spotlist_features_id' => 4,
            ),
            2 => 
            array (
                'spotlist_directory_id' => 1,
                'spotlist_features_id' => 6,
            ),
            3 => 
            array (
                'spotlist_directory_id' => 1,
                'spotlist_features_id' => 7,
            ),
            4 => 
            array (
                'spotlist_directory_id' => 2,
                'spotlist_features_id' => 1,
            ),
            5 => 
            array (
                'spotlist_directory_id' => 2,
                'spotlist_features_id' => 2,
            ),
            6 => 
            array (
                'spotlist_directory_id' => 2,
                'spotlist_features_id' => 3,
            ),
            7 => 
            array (
                'spotlist_directory_id' => 2,
                'spotlist_features_id' => 4,
            ),
            8 => 
            array (
                'spotlist_directory_id' => 2,
                'spotlist_features_id' => 5,
            ),
            9 => 
            array (
                'spotlist_directory_id' => 2,
                'spotlist_features_id' => 6,
            ),
            10 => 
            array (
                'spotlist_directory_id' => 2,
                'spotlist_features_id' => 7,
            ),
        ));

        
    }
}