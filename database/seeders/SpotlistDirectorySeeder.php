<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectorySeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory')->delete();
        
        \DB::table('spotlist_directory')->insert(array (
            0 => 
            array (
                'spotlist_directory_id' => 1,
                'sys_users_id' => 5,
                'spotlist_parent_category_id' => 3,
                'spotlist_directory_name' => 'Testing Directory',
                'spotlist_directory_slug' => 'sd',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Why do we use it?
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
                'is_featured' => 1,
                'google_analytics_id' => NULL,
                'country_id' => 236,
                'city' => 1,
                'address' => 'New York',
                'latitude' => '40.6971494',
                'longitude' => '74.2598743,10z',
                'google_location' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387193.3060592708!2d-74.25987430508168!3d40.69714939799659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sbd!4v1602570994682!5m2!1sen!2sbd" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
                'thumb_image' => 'Backend/images/directory/fXMJmQJP6Ds9pfCp.jpg',
                'cover_image' => 'Backend/images/directory/S3Uf6SHPuajWF8g2.jpg',
                'video_provider' => 'Youtube',
                'video_url' => 'https://www.youtube.com/embed/KYt26tjah3s',
                'seo_meta_keywords' => NULL,
                'seo_meta_description' => NULL,
                'website' => 'www.website.com',
                'email' => 'info@itech.com',
                'contact_number' => '1235456334',
                'facebook_url' => 'www.facebook.com',
                'linkedin_url' => NULL,
                'twitter_url' => 'www.twitter.com',
                'instagram_url' => NULL,
                'type' => 'Restaurant',
                'registration_step' => 'basic,location,features,gallery,schedule,contact,type',
                'created_by' => 2,
                'updated_by' => 2,
                'is_verified' => 1,
                'video_ability' => 1,
                'visit_count' => 38,
                'booking_count' => 4,
                'wishing_count' => 1,
                'status' => 'Active',
                'created_at' => '2020-10-13 06:35:10',
                'updated_at' => '2020-10-13 06:35:10',
            ),
            1 => 
            array (
                'spotlist_directory_id' => 2,
                'sys_users_id' => 5,
                'spotlist_parent_category_id' => 2,
                'spotlist_directory_name' => 'Testing directory 2',
                'spotlist_directory_slug' => 'sd2',
                'description' => 'Some text will be written here',
                'is_featured' => 1,
                'google_analytics_id' => NULL,
                'country_id' => 236,
                'city' => 2,
                'address' => 'Some text will be written',
                'latitude' => '22.825694876537334',
                'longitude' => '89.55199940898439',
                'google_location' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387193.3060592708!2d-74.25987430508168!3d40.69714939799659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sbd!4v1602570994682!5m2!1sen!2sbd" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
                'thumb_image' => 'Backend/images/directory/0nhDJbGT8HA9xmEt.jpg',
                'cover_image' => 'Backend/images/directory/3nSUl48u8tHiapyP.jpg',
                'video_provider' => 'Youtube',
                'video_url' => 'https://www.youtube.com/embed/bNXRYfKDQg0',
                'seo_meta_keywords' => NULL,
                'seo_meta_description' => NULL,
                'website' => 'www.website.com',
                'email' => 'info@itech.com',
                'contact_number' => '2394328823',
                'facebook_url' => 'www.facebook.com',
                'linkedin_url' => NULL,
                'twitter_url' => 'www.twitter.com',
                'instagram_url' => NULL,
                'type' => 'Hotel',
                'registration_step' => 'basic,location,features,gallery,schedule,contact,type',
                'created_by' => 2,
                'updated_by' => 2,
                'is_verified' => 1,
                'video_ability' => 1,
                'visit_count' => 7,
                'booking_count' => 2,
                'wishing_count' => 1,
                'status' => 'Active',
                'created_at' => '2020-10-13 10:23:34',
                'updated_at' => '2020-10-13 10:23:34',
            ),
        ));

        
    }
}