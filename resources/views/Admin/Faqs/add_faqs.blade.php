@extends('Backend.layouts.app',['menu'=>'faqs','sub_menu'=>'faqs_add'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('spotlist-faqs')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New Faqs')}}</a>
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('spotlist-faqs-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('Faqs list')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-globe"></i> {{__('Spotlist Faqs')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('spotlist-faqs-save')}}" class="parsley-examples" method="POST" id="submit_faqs" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="spotlist_features_name">{{__('Question title')}}<span class="text-danger">*</span></label>
                                <input type="text" name="title" parsley-trigger="change" required value="{{$faqs->title ?? ''}}"
                                       placeholder="Enter your question here" class="form-control" id="title">
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="spotlist_features_icon">{{__('Description')}}<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="description" rows="4">{{$faqs->description ?? ''}}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label class="col-form-label" for="status">{{__('Status')}}</label>
                                @php($option = ['Active' =>__('Active'),'Inactive'=>__('Inactive')])
                                {!! Form::select('status', $option, $faqs->status ?? '', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <input type="hidden" name="id" value="{{$faqs->id ?? ''}}" id="id">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
@section('script')
    <script>
        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            var redirect_url = "{{url('spotlist-faqs-list')}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'error');
                        }
                    });
                }
            });
        });
    </script>
@endsection
