@extends('Backend.layouts.app',['menu'=>'settings','sub_menu'=>'admin_settings'])
@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-cogs"></i> {{__('Admin settings')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form method="post" action="{{url('admin-currency-setting-save')}}" id="currency_save">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group text-center">
                        <label>{{__('Email Verificaton')}}</label><br>
                        <input type="checkbox" name="email_verification" {{isset($settings->email_verification_enable) && $settings->email_verification_enable == true ? 'checked' : ''}}
                               data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="On" data-off="Off">

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group text-center">
                        <label>{{__('Social login')}}</label><br>
                        <input type="checkbox" name="social_login" {{isset($settings->social_login_enable) && $settings->social_login_enable == true ? 'checked' : ''}}
                               data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="On" data-off="Off">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{__('Currency')}}</label>
                        <select class="form-control" name="currency">
                            @foreach(currency() as $key=>$value)
                                <option value="{{$key}}" {{is_selected($key,$settings->currency ?? '')}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2 ">
                    <div class="form-group mt-3">
                        <button class="btn  btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="card-box">
        <form action="{{url('admin-settings-save')}}" class="parsley-examples" method="POST" id="submit_settings" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Application title')}}<span class="text-danger">*</span></label>
                        <input type="text" name="app_title" parsley-trigger="change" required value="{{ $settings->app_title ?? ''}}"
                               placeholder="App title" class="form-control" id="app_title">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Contact Email')}}</label>
                        <input type="email" name="contact_email" parsley-trigger="change"  value="{{ $settings->contact_email ?? ''}}"
                               placeholder="Email" class="form-control" id="contact_email">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('Contact number')}}</label>
                        <input type="text" name="contact_number" parsley-trigger="change"  value="{{ $settings->contact_number ?? ''}}"
                               placeholder="Contact number" class="form-control" id="contact_number">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('Description')}}</label>
                        <textarea name="description" parsley-trigger="change" placeholder="Description here" class="form-control" id="description">{{$settings->description ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('Copyright text')}}</label>
                        <textarea name="copy_right_text" parsley-trigger="change" placeholder="Footer description here here" class="form-control" id="copy_right_text">{{$settings->copy_right_text ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('About us')}}</label>
                        <textarea name="about_us" parsley-trigger="change" placeholder="About us" class="form-control" id="about_us">{{$settings->about_us ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('Address')}}</label>
                        <textarea name="address" parsley-trigger="change" placeholder="Address" class="form-control" id="address">{{$settings->address ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name">{{__('Map key')}}</label>
                        <input type="text" class="form-control" value="{{$settings->map_key ?? ''}}" id="map_key" name="map_key">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="col-form-label" for="app_logo">{{__('App logo large')}}</label>
                        <input type="file" name="app_logo_large" data-plugins="dropify"
                               data-default-file="{{isset($settings->app_logo_large) && !empty($settings->app_logo_large) ? getImageUrl($settings->app_logo_large) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="2M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="col-form-label" for="app_logo">{{__('App logo small')}}</label>
                        <input type="file" name="app_logo_small" data-plugins="dropify"
                               data-default-file="{{isset($settings->app_logo_small) && !empty($settings->app_logo_small) ? getImageUrl($settings->app_logo_small) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="2M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-form-label" for="blog_cover_image">{{__('Favicon')}}</label>
                        <input type="file" name="favicon_logo" data-plugins="dropify"
                               data-default-file="{{isset($settings->favicon_logo) && !empty($settings->favicon_logo) ? getImageUrl($settings->favicon_logo) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="2M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            swalSuccess(response.message)
                        }else{
                            swalError(response.message)
                        }
                    });
                }
            });
        });
    </script>
@endsection

