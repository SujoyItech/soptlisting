<div class="tab-pane" id="media">
    <form action="{{url('directory-gallery-save')}}" class="parsley-examples" method="POST" id="submit_directory_gallery" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="col-form-label" for="thumb_image">{{__('Listing thumbnail')}}</label>
                    <input type="file" name="thumb_image" data-plugins="dropify"
                           data-default-file="{{isset($directory->thumb_image) && !empty($directory->thumb_image) ? getImageUrl($directory->thumb_image) : ''}}"
                           data-allowed-file-extensions="png jpg jpeg jfif"
                           data-max-file-size="1M" />
                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label class="col-form-label" for="cover_image">{{__('Listing cover')}}</label>
                    <input type="file" name="cover_image" data-plugins="dropify"
                           data-default-file="{{isset($directory->cover_image) && !empty($directory->cover_image) ? getImageUrl($directory->cover_image) : ''}}"
                           data-allowed-file-extensions="png jpg jpeg jfif"
                           data-max-file-size="1M" />
                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                </div>
            </div>
        </div>
        @if(isset($directory_subscription) && $directory_subscription->add_video_ability == TRUE)
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label class="col-form-label" for="picture">{{__('Video provider')}}</label>
                    @php($video_provider = ['Youtube'=>'Youtube','Vimeo'=>'Vimeo','HTML5'=>'HTML5'])
                    {!! Form::select('video_provider', $video_provider, $directory->video_provider ?? '', ['class' => 'form-control', 'id'=>'video_provider']) !!}
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label class="col-form-label" for="picture">{{__('Video url')}}</label>
                    <input type="text" class="form-control" id="video_url" name="video_url" placeholder="{{__('Video url')}}" value="{{$directory->video_url ?? ''}}">
                </div>
            </div>
            <input type="hidden" name="video_ability" value="1">
        </div>
        @endif
        <div class="row addlist">
            @if(isset($directory_gallery) && !empty($directory_gallery))
                @foreach($directory_gallery as $gallery)
                    <div class="col-lg-4 mb-3 image_list_heading">
                        <div class="form-group">
                            <label class="col-form-label" for="picture">{{__('Listing gallery images')}}</label>
                            <input type="file" name="gallery_images[]" data-plugins="dropify" disabled="disabled" class="dropify"
                                   data-default-file="{{getImageUrl($gallery->image)}}"
                                   data-allowed-file-extensions="png jpg jpeg jfif"
                                   data-max-file-size="1M" multiple />
                            <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>

                        </div>
                        <div class="text-center">
                            <button class="btn btn-sm btn-danger remove_list_image" type="button" data-id="{{$gallery->spotlist_directory_gallary_id}}"><i class="fa fa-minus"></i> {{__('Remove')}}</button>
                        </div>
                    </div>
                @endforeach
            @endif
            @if(\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER)
            @if(availableMedia(\Illuminate\Support\Facades\Auth::user()->id) > 0)
                <div class="col-lg-4 mb-3">
                    <div class="form-group">
                        <label class="col-form-label" for="picture">{{__('Listing gallery images')}}</label>
                        <input type="file" name="gallery_images[]" data-plugins="dropify" class="dropify"
                               data-default-file=""
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="1M" multiple />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>

                    </div>
                    <div class="text-center">
                        <button class="btn btn-sm btn-info add_image" type="button"><i class="fa fa-plus"></i> {{__('Add more')}}</button>
                    </div>
                </div>
            @else
                <div class="col-12">
                    <div class="alert alert-danger text-center">
                        <p>{{__('No media available.')}} <a href="{{route('listing-manager-pricing')}}"> {{__('Subscribe a new package.')}}</a></p>
                    </div>
                </div>
            @endif
            @else
                <div class="col-lg-4 mb-3">
                    <div class="form-group">
                        <label class="col-form-label" for="picture">{{__('Listing gallery images')}}</label>
                        <input type="file" name="gallery_images[]" data-plugins="dropify" class="dropify"
                               data-default-file=""
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="1M" multiple />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>

                    </div>
                    <div class="text-center">
                        <button class="btn btn-sm btn-info add_image" type="button"><i class="fa fa-plus"></i> {{__('Add more')}}</button>
                    </div>
                </div>
            @endif
        </div>
        <ul class="list-inline mb-0 wizard">
            <li class="previous list-inline-item">
                <a href="javascript: void(0);" class="btn btn-sm btn-secondary">{{__('Previous')}}</a>
            </li>
            <li class="next list-inline-item float-right">
                <button type="submit" class="btn btn-sm btn-success submit_info btn-next">{{__('Save & Next')}}</button>
            </li>
        </ul>
    </form>
</div>

<script>

    var image_html = ' <div class="col-lg-4 mb-3 image_heading">\n' +
        '            <div class="form-group">\n' +
        '                <label class="col-form-label" for="picture">{{__('Listing gallery images')}}</label>\n' +
        '                <input type="file" name="gallery_images[]" data-plugins="dropify" class="dropify"\n' +
        '                       data-default-file=""\n' +
        '                       data-allowed-file-extensions="png jpg jpeg jfif"\n' +
        '                       data-max-file-size="1M" multiple />\n' +
        '                <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>\n' +
        '\n' +
        '            </div>\n' +
        '            <div class="text-center">\n' +
        '                <button class="btn btn-sm btn-danger remove_image" type="button"><i class="fa fa-minus"></i> {{__('Remove')}}</button>\n' +
        '            </div>\n' +
        '\n' +
        '        </div>';

    @if(\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER)
        var available_media = "{{availableMedia(\Illuminate\Support\Facades\Auth::user()->id)}}";
        var x = 1;
        $(document).on('click','.add_image',function (e) {
            e.preventDefault();
            if (x<available_media){
                $('.addlist').append(image_html);
                $('.dropify').dropify();
                x++;
            }else {
                swalWarning('Sorry! you have no media available. Please purchase now.');
            }

        });
    @else
        $(document).on('click','.add_image',function (e) {
            e.preventDefault();
            $('.addlist').append(image_html);
            $('.dropify').dropify();
        });
    @endif

    $(document).on('click','.remove_image',function (e) {
        e.preventDefault();
        $(this).closest('.image_heading').remove();
        x--;
    });

    $('.remove_list_image').on('click', function (e) {
        e.preventDefault();
        Ladda.bind(this);
        var obj = $(this);
        var load = $(this).ladda();
        var id = $(this).data('id');
        var url = "{{url('remove-directory-gallery-file')}}";
        swalConfirm('Are You sure to delete this file?').then(function (s) {
            if(s.value){
                var data = {
                    value: id,
                    table_name: 'spotlist_directory_gallary',
                    primary_key: 'spotlist_directory_gallary_id'
                };
                makeAjaxPost(data, url, load).done(function(sresult){
                    $(obj).closest('.image_list_heading').remove();
                    swalSuccess('Deleted Successfully');
                });
            }
        });
    });


</script>
