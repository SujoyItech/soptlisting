
<div class="list-single-wiget book">
    <h2>{{__('Book a table')}}</h2>
    <form action="{{route('user.booking.request.send')}}" method="post" class="parsley-examples" id="restaurant_booking">
        <div class="form-group">
            <input type="text"  name="reservation_contact" required parsley-trigger="change" parsley-type="phone" id="phone">
        </div>
        <div class="form-group form-group2">
            <div class="form-1 bk">
                <div class="form-3">
                    <input placeholder="Adult" disabled />
                    <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/10.svg" alt=""></div>
                </div>
                <div class="quantity cart-plus-minus">
                    <input type="text" value="1" name="adult_guest" required min="1" />
                </div>
            </div>
        </div>
        <div class="form-group form-group2">
            <div class="form-3">
                <input placeholder="Child" disabled />
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/10.svg" alt=""></div>
            </div>
            <div class="form-1">
                <div class="quantity cart-plus-minus">
                    <input type="text" value="1" name="child_guest" required parsley-trigger="change" />
                </div>
            </div>
        </div>
        <div class="form-group form-group2 restaurant_date_check">
            <div class="form-1 frt">
                <input id="reservation_date" class="reservation_date" type="text" width="276" name="reservation_start_date" placeholder="Date" required parsley-trigger="change" readonly/>
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/10.svg" alt=""></div>
                <span class="text-danger d-none date_error"></span>
            </div>
            <div class="form-2 frt">
                <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                    <input type="text" class="form-control restaurant_reservation_start_time" placeholder="Time" name="reservation_start_time" required readonly>
                    <span class="text-danger d-none start_time_error"></span>
                </div>
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/11.svg" alt=""></div>
            </div>
        </div>
        <div class="form-group">
            <textarea class="form-control" rows="3" name="reservation_note" placeholder="{{__('Add a note here')}}"></textarea>
        </div>
        <div class="form-group form-btn">
            <input type="hidden" name="type" value="{{$directory->spotlist_parent_category_id}}">
            <input type="hidden" name="spotlist_directory_id" value="{{$directory->spotlist_directory_id}}">
            <input type="hidden" name="reservation_for" value="restaurant">
            @if(isset(Auth::user()->id))
                <button type="submit" class="theme-btn  booking_now restaurant_booking">{{__('BOOK NOW')}}</button>
                @include('FontEnd.listing.booking.wish_list')
            @else
                <a href="{{route('login')}}" class="theme-btn btn-block text-center">{{__('Login')}}</a>
            @endif

        </div>
    </form>
</div>

<script>
    $(document).ready(function (){
        $('#reservation_date').datepicker(
            {
                minDate: 0,
            });

        $('.reservation_date').on('change',function (){
            if ($(this).val() != "" && $(this).siblings('ul.parsley-errors-list').length > 0) {
                $(this).siblings('ul').remove();
            }
            $('.date_error').addClass('d-none');
            $(".restaurant_reservation_start_time").val('');
        })

        $(".restaurant_reservation_start_time").bind("keyup change", function(e) {
            var restaurant_reservation_date = $('.restaurant_date_check').children().find('.reservation_date').val();
            var this_reservation_time = $(this);
            if (restaurant_reservation_date.length !== 0){
                $('.date_error').addClass('d-none');
                if ($(this_reservation_time).val() != "" && $(this_reservation_time).siblings('ul.parsley-errors-list').length > 0) {
                    $(this_reservation_time).siblings('ul').remove();
                }
                var check_restaurant_booking_start_time_data = {
                    spotlist_directory_id: "{{$directory->spotlist_directory_id}}",
                    booking_date :restaurant_reservation_date,
                    start_time : $(this).val()
                }
                var check_restaurant_booking_start_time_url = "{{url('check-booking-start-time')}}";
                makeAjaxPost(check_restaurant_booking_start_time_data,check_restaurant_booking_start_time_url,null).done(function (response){
                    if (response.success == true){
                        $('.start_time_error').addClass('d-none');
                        $('.restaurant_booking').prop('disabled',false);

                    }else {
                        $('.start_time_error').html('Service not open now');
                        $('.start_time_error').removeClass('d-none');
                        $('.restaurant_booking').prop('disabled',true);
                    }
                })
            }else {
                $('.date_error').html('Please select date first.');
                $('.date_error').removeClass('d-none');
                $('.restaurant_booking').prop('disabled',true);
            }

        });
    })


</script>
@include('FontEnd.listing.booking.phone_script')
