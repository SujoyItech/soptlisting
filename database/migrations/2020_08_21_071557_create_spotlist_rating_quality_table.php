<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistRatingQualityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_rating_quality', function (Blueprint $table) {
            $table->increments('spotlist_rating_quality_id');
            $table->string('spotlist_rating_quality_name')->nullable();
            $table->decimal('rating_from',3,1)->nullable();
            $table->decimal('rating_to',3,1)->nullable();
            $table->text('description')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_rating_quality');
    }
}
