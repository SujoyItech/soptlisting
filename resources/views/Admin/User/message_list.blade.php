@extends('Backend.layouts.app',['menu'=>'user','sub_menu'=>'user_message'])
@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('Message List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table">
                        <thead>
                        <tr>
                            <th>{{__('First name')}}</th>
                            <th>{{__('Last name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Subject')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Sent at')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="standard-modalLabel">{{__('Message')}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h6 id="name"></h6>
                            <p id="email"></p>
                            <hr>
                            <h6 id="subject"></h6>
                            <p id="message"></p>
                        </div>
                    </div>
                    <h5>{{__('Reply message')}}</h5>
                    <form method="post" action="{{url('send-email-to-user')}}" class="parsley-examples">
                        @csrf
                        <div class="row mt-1">
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <input class="form-control" type="text" name="subject" placeholder="Enter Subject here">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <textarea class="form-control" name="message" placeholder="Write something.." rows="4" parsley-trigger="change" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group no-margin">
                                    <input type="hidden" name="id" id="reply_id">
                                    <button class="btn btn-sm btn-success"><i class="fa fa-reply"></i> {{__('Reply')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div><

@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.jqueryui.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: true,
                ajax: '{{url('admin-user-message-list')}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.id);
                },
                columns: [
                    {"data": "fname"},
                    {"data": "lname"},
                    {"data": "email"},
                    {"data": "subject"},
                    {"data": "status"},
                    {"data": "created_at"},
                    {"data": "action"},
                ],
            });
        });
        $(document).on('click','.view_message',function (){
            Ladda.bind(this);
            var load = $(this).ladda();

            var id = $(this).data('id');
            var data = {
                id : id
            }
            var url = "{{url('admin-get-user-message')}}";
            makeAjaxPost(data,url,load).done(function (response){
                load.ladda('stop');
                if (response.success == true){
                    $('#name').html(response.data.fname+' '+response.data.lname);
                    $('#email').html(response.data.email);
                    $('#subject').html(response.data.subject);
                    $('#message').html(response.data.message);
                    $('#reply_id').val(response.data.id);

                }else {
                    swalError(response.message)
                }
            });
            $('#standard-modal').modal('show');

        });
    </script>
@endsection
