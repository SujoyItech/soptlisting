<script src="{{asset('FontEnd/')}}/assets/js/jquery.min.js"></script>
<script src="{{asset('FontEnd/')}}/assets/js/bootstrap.min.js"></script>
<!-- Plugins for this template -->
<script src="{{asset('FontEnd/')}}/assets/js/jquery-plugin-collection.js"></script>
<script src="{{asset('FontEnd/')}}/assets/js/jquery.slicknav.min.js"></script>
<script src="{{asset('FontEnd/')}}/assets/js/jquery-ui.min.js"></script>
<script src="{{asset('FontEnd/')}}/assets/js/script.js"></script>
<script src="{{asset('Backend/')}}/libs/toastr/build/toastr.min.js"></script>
<script src="{{asset('/Backend/js/LaraframeScript.js')}}"></script>
<script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
<script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>

@yield('script')

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3500,
        autoWidth: true,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    @if(Session::has('dismiss') && !empty(Session::get('dismiss')))
    Toast.fire({type: 'warning', text: '{{Session::get('dismiss')}}'});
    @endif
    @if(Session::has('success') && !empty(Session::get('success')))
    Toast.fire({type: 'success', text: '{{Session::get('success')}}'});
    @endif

</script>




