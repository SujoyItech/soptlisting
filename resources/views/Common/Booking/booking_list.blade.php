@extends('Backend.layouts.app',['menu'=>'my_booking'])

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('My Booking List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table">
                        <thead>
                        <tr>
                            <th>{{__('ID')}} </th>
                            <th>{{__('Listing')}}</th>
                            <th>{{__('Customer')}}</th>
                            <th>{{__('Additional information')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Date')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>

                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('Common.Booking.booking_message')
@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.jqueryui.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: true,
                ajax: '{{route('user-booking-request',['type'=>$type,'booking_id'=>$booking_id])}}',
                columnDefs: [ { type: 'date', 'targets': [5] } ],
                order: [[ 5, "asc" ]],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.spotlist_booking_request_id);
                },
                columns: [
                    {"data": "booking_id"},
                    {"data": "listing"},
                    {"data": "user_details"},
                    {"data": "additional_information"},
                    {"data": "status"},
                    {"data": "date"},
                    {"data": "action"},
                ],
            });
        });

        $(document).on('click','.change_booking_status', function (e) {
            e.preventDefault();
            var booking_id = $(this).data('id');
            var type = $(this).data('type');
            var type_data = type.charAt(0).toUpperCase() + type.slice(1);
            var confirm_text = '';
            if (type == 'delete'){
                confirm_text = 'Are you sure to delete this?';
            }
            else{
                confirm_text = 'Are you sure to make this '+type_data+'?';
            }

            swalMessage(confirm_text,type_data).then((result) => {
                if (result.value){
                    var booking_data = {
                        id: booking_id,
                        type: type,
                        message: result.value.message
                    };
                    var url = "{{url('booking-status-change')}}";
                    makeAjaxPost(booking_data, url, null).done(function(response){
                        var redirect_url = "{{Request::url()}}";
                        if (response.success == true){
                            swalRedirect(redirect_url,response.message,'success')
                        }else{
                            swalRedirect(redirect_url,response.message,'error')
                        }
                    });
                }

            });
        });

        function swalMessage(msg,type){
           return  Swal.fire({
                html: '<div>\n' +
                    '    <div class="card-body">\n' +
                    '    <div class="card-header">\n' +
                    '        '+msg+'\n' +
                    '    </div>\n' +
                    '        <div class="form-group">\n' +
                    '            <textarea class="form-control" name="message" rows="6" placeholder="Do you want to write something ?"></textarea>\n' +
                    '        </div>\n' +
                    '    </div>\n' +
                    '</div>',
                reverseButtons: true,
                allowOutsideClick: false,
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Confirm',
                showLoaderOnConfirm: true,
                preConfirm: function() {
                   return new Promise((resolve, reject) => {
                       // get your inputs using their placeholder or maybe add IDs to them
                       resolve({
                           message: $('textarea[name="message"]').val(),
                       });
                   });
               }
            })
        }


        $(document).on('click','.message',function (){
            var message_url = "{{url('view-user-booking-message')}}";
            var message_data = {
                id : $(this).data('id')
            }
            makeAjaxPostText(message_data,message_url,null).done(function (response) {
                $('.message_modal').html(response)
                $('#bookingMessageModal').modal('show');
            });
        })
    </script>
@endsection
