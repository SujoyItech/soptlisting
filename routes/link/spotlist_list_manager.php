<?php

/*=============================================Dashboard====================================================*/
Route::get('listing-manager-dashboard', 'DashboardController@index')->name('listing-manager-dashboard');
Route::get('listing-manager-list', 'ListingController@index')->name('listing-manager-list');
Route::get('list-manager-add-listing', 'ListingController@createList')->name('listing-manager-add-listing');

Route::get('list-manager-create-directory', 'ListingController@createDirectory')->name('list-manager-create-directory');
Route::get('list-manager-directory-list/{directory_id?}', 'ListingController@directoryList')->name('list-manager-directory-list');

Route::get('listing-manager-bookings', 'Booking\BookingController@index')->name('listing-manager-bookings');
Route::get('list-manager-inbox', 'MessageController@inbox')->name('list-manager-inbox');
Route::get('listing-manager-chat', 'MessageController@chat')->name('listing-manager-chat');
Route::get('list-manager-wishing-list', 'ListingController@wishingList')->name('list-manager-wishing-list');
Route::get('list-manager-profile', 'DashboardController@profile')->name('list-manager-profile');
Route::get('listing-manager-pricing', 'Pricing\PricingController@pricing')->name('listing-manager-pricing');

Route::post('listing-manager-package-subscription', 'Pricing\PricingController@packageSubscription')->name('listing-manager-package-subscription');
Route::get('listing-manager-package-subscription-process/{package_id}', 'Pricing\PricingController@packageSubscriptionProcess')->name('listing-manager-package-subscription-process');

Route::post('free-subscription', 'Pricing\PricingController@packageSubscriptionFree')->name('free-subscription');
Route::post('payment/make', 'Pricing\PricingController@packageSubscription')->name('payment.make');
Route::post('stripe/post', 'Pricing\PricingController@packageSubscriptionByStripe')->name('stripe.post');

Route::get('listing-manager-payment-history', 'Pricing\PricingController@paymentHistory')->name('listing-manager-payment-history');
Route::get('listing-manager-subscription', 'Pricing\PricingController@getSubscription')->name('listing-manager-subscription');


