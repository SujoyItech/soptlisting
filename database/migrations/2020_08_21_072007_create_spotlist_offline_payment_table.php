<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistOfflinePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_offline_payment', function (Blueprint $table) {
            $table->bigIncrements('spotlist_offline_payment_id');
            $table->unsignedBigInteger('sys_users_id');
            $table->unsignedInteger('spotlist_package_id');
            $table->decimal('payment_amount',10,2);
            $table->string('payment_method')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_offline_payment');
    }
}
