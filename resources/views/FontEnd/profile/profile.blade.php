@extends('FontEnd.master',['menu'=>'profile'])
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Profile')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->

    <!-- profile-area start -->
    <div class="profile-area section-padding">
        <div class="container">
            <div class="profile-wrap">
                <div class="profile-sub">
                    <div class="row">
                        <div class="col-lg-4 col-md-12">
                            <div class="profile-text">
                                <div class="pro-text-item">
                                    <h2>{{__('Address')}}</h2>
                                    <p>{{$user->address ?? ''}}</p>
                                </div>
                                <div class="pro-text-item">
                                    <h2>{{__('Phone')}}</h2>
                                    <p>{{$user->mobile ?? ''}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="profile-details">
                                <div class="profile-img">
                                    <img src="{{user_image($user->user_image)}}" alt="">
                                </div>
                                <h3>{{$user->name}}</h3>
                                @if($user->id == \Illuminate\Support\Facades\Auth::user()->id)
                                @else
                                    <p><span>{{userFollower($user->id)}}</span> {{__('followers')}}</p><br>
                                @endif
                                <ul class="profile-social">
                                    <li><a href="{{$user->facebook_link}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/16.svg" alt=""></a></li>
                                    <li><a href="{{$user->twitter_link}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/17.svg" alt=""></a></li>
                                    <li><a href="{{$user->instagram_link}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/18.svg" alt=""></a></li>
                                    <li><a href="{{$user->linkedin_link}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/19.svg" alt=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="profile-text">
                                <div class="pro-text-item">
                                    <h2>{{__('Email')}}</h2>
                                    <p>{{$user->email}}</p>
                                </div>
                                <div class="pro-text-item">
                                    <h2>{{__('Website')}}</h2>
                                    <p><a href="{{$user->website}}" target="_blank">
                                        </a> {{$user->website}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-sub-bottom">
                    <h2>{{__('About')}} {{$user->name}}</h2>
                    <p>
                        {{$user->about}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- profile-area end -->
    <!-- .recent-area-start -->
    <div class="listing-area">
        @if(isset($listing) && !empty($listing[0]))
            <div class="container">
                <h1 class="text-center mb-5">{{__('User Listing')}}</h1>
                <div class="row">
                    @foreach($listing as $data)
                        <div class="col-lg-4 col-md-6 col-12 mb-3">
                            <div class="product-item">
                                <div class="product-img">
                                    <img src="{{asset($data->thumb_image)}}" alt="" height="300">
                                    <div class="pro-featured">
                                        <a class="btn theme-btn" href="#">{{$data->is_featured == 1 ? 'Featured' : 'Not Featured'}}</a>
                                    </div>
                                </div>
                                <div class="product-content">
                                    <span><a href="{{route('user.profile',['id'=>$data->user_id])}}"><small>{{$data->user_name}}</small></a> | <small>{{ Carbon\Carbon::parse($data->created_at)->format('d F Y') }}</small></span>
                                    <h4><a href="{{route('user.listing',['slug'=>$data->spotlist_directory_slug])}}">{{$data->spotlist_directory_name}}</a></h4>
                                    <span>{{isset($data->city_name) ? $data->city_name.' , ' : ''}} {{$data->country_name}}</span>
                                    <div class="ratting-area">
                                        @if(isset($data->rating))
                                            <ul>
                                                @for($x = 0; $x< 5 ; $x++)
                                                    @if(floor($data->rating) - $x >=1)
                                                        <li><i class="fa fa-star"></i></li>
                                                    @elseif($data->rating -$x >0)
                                                        <li><i class="fa fa-star-half-o"></i></li>
                                                    @else
                                                        <li><i class="fa fa-star-o"></i></li>
                                                    @endif
                                                @endfor
                                            </ul>
                                        @endif
                                        <p><span>{{isset($data->rating) ? number_format($data->rating,1) : 0}}</span> ({{$data->total_reviews}} {{__('Reviews')}})</p>
                                    </div>
                                    <div class="pro-bottom">
                                        <ul>
                                            <li><small class="theme-btn open">{{__('Now Open')}}</small></li>
                                            <li><a href="https://www.google.com/maps/search/?api=1&query={{$data->latitude}},{{$data->longitude}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/logo/Vector.png" alt="">{{__('Show On Map')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="thumb">
                                    <span>{{$data->category_name}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-5 offset-5">
                        <div class="listing-wrap">
                            <div class="pagination-wrapper">
                                {{ $listing->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <!-- .recent-area-start -->
    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')
@endsection

@section('script')


@endsection
