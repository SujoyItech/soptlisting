<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->status == 'Active'){
            return $next($request);
        }elseif (Auth::user()->status == 'Suspended') {
            Auth::logout();
            return redirect()->route('login')->with(['dismiss' => __('Your Account has been suspended. please contact support team to active again!')]);
        } elseif (Auth::user()->status == 'Deleted') {
            Auth::logout();
            return redirect()->route('login')->with(['dismiss' => __('Your Account has been deleted. please contact support team to active again!')]);
        }
        elseif (auth::user()->status == 'Blocked') {
            Auth::logout();
            return redirect()->route('login')->with(['dismiss' => __('You are blocked. Contact with the support team.')]);
        }

    }
}
