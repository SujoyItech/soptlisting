<div class="modal fade" id="serviceDetails" tabindex="-1" role="dialog" aria-labelledby="serviceDetailsTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body service_details">
                
            </div>
            <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
