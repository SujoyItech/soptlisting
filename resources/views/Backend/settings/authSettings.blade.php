@extends('Backend.layouts.app',['menu'=>'settings','sub_menu'=>'auth_settings'])
@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-cogs"></i> {{__('Auth settings')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('admin-auth-settings-save')}}" class="parsley-examples" method="POST" id="submit_settings" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('FACEBOOK_APP_ID')}}<span class="text-danger">*</span></label>
                        <input type="text" name="facebook_app_id" parsley-trigger="change" required value="{{ $settings->facebook_app_id ?? ''}}"
                               placeholder="2658874191094481" class="form-control" id="app_title">
                    </div>
                </div> <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('FACEBOOK_APP_SECRET')}}<span class="text-danger">*</span></label>
                        <input type="text" name="facebook_app_secret" parsley-trigger="change" required value="{{ $settings->facebook_app_secret ?? ''}}"
                               placeholder="d1faeecca9d0df78093a370d26bd839f" class="form-control" id="app_title">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('FACEBOOK_REDIRECT')}}</label>
                        <input type="text" name="facebook_redirect" parsley-trigger="change"  value="{{ $settings->facebook_redirect ?? ''}}"
                               placeholder="https://example.com/login/facebook/callback" class="form-control" id="facebook_redirect">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('GOOGLE_CLIENT_ID')}}<span class="text-danger">*</span></label>
                        <input type="text" name="google_client_id" parsley-trigger="change" required value="{{ $settings->google_client_id ?? ''}}"
                               placeholder="851827167283-obhkollt85gbqq9lqjd0mttm2vg186qg.apps.googleusercontent.com" class="form-control" id="google_client_id">
                    </div>
                </div> <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('GOOGLE_CLIENT_SECRET')}}<span class="text-danger">*</span></label>
                        <input type="text" name="google_client_secret" parsley-trigger="change" required value="{{ $settings->google_client_secret ?? ''}}"
                               placeholder="EnPwYLZmrdNyxLaN4xoo7gSD" class="form-control" id="app_title">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="name">{{__('GOOGLE_REDIRECT')}}</label>
                        <input type="text" name="google_redirect" parsley-trigger="change"  value="{{ $settings->google_redirect ?? ''}}"
                               placeholder="https://example.com/login/facebook/callback/login/google/callback" class="form-control" id="google_redirect">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            swalSuccess(response.message)
                        }else{
                            swalError(response.message)
                        }
                    });
                }
            });
        });
    </script>
@endsection

