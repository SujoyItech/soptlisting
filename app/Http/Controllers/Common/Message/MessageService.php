<?php


namespace App\Http\Controllers\Common\Message;


use App\Http\Service\NotificationService;
use App\Models\Message;
use App\Models\SpotlistBookingMessage;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageService {
    public function bookingRequestMessageSend(Request $request){
        try {
            $this->insertNewMessage($request);
            if (!empty($this->messageColumnUpdate($request))){
                DB::table('spotlist_booking_request')->where('spotlist_booking_request_id',$request->id)->update(['message'=>$this->messageColumnUpdate($request)]);

                $data['success'] = TRUE;
                $data['message'] = __('Message sent successfully!');
            }else{
                $data['success'] = FALSE;
                $data['message'] = __('Message can\'t be empty!');
            }
        }catch (\Exception $exception){
            $data['success'] = FALSE;
//            $data['message'] = __('Message sending failed!');
            $data['message'] = $exception->getMessage();
        }
        return $data;
    }

    private function getUser($user_id){
        return User::where('id',$user_id)->first();
    }

    private function insertNewMessage($request){
        $booking = DB::table('spotlist_booking_request')
                     ->leftJoin('spotlist_directory','spotlist_booking_request.spotlist_directory_id','=','spotlist_directory.spotlist_directory_id')
                     ->where('spotlist_booking_request.spotlist_booking_request_id',$request->id)
                     ->select('spotlist_directory.type','spotlist_booking_request.customer_id as user_id','spotlist_directory.sys_users_id as manager_id')
                     ->first();
        $message = [];
        if (!empty($request->message)){
            $message['booking_id'] = $request->id;
            if (Auth::user()->id != $booking->user_id){
                $message['from'] = $booking->manager_id;
                $message['to'] = $booking->user_id;
                $message['message'] = $request->message;
            }else{
                $message['from'] = $booking->user_id;
                $message['to'] = $booking->manager_id;
                $message['message'] = $request->message;
            }
            SpotlistBookingMessage::create($message);
            $this->sendMessageNotification($message,$booking->type,$request->id);
        }
    }
    private function messageColumnUpdate($request){
        if (!empty($request->message)){
            $message = [
                'type'=> Auth::user()->default_module_id == SYSTEM_USER ? 'user':'manager',
                'message' => $request->message,
                'time' => Carbon::now()
            ];
            $booking_request = DB::table('spotlist_booking_request')->where('spotlist_booking_request_id',$request->id)->first();
            if (!empty($booking_request->message)){
                $booking_message = json_decode($booking_request->message);
            }else{
                $booking_message = [];
            }
            array_push($booking_message,$message);
            return json_encode($booking_message);
        }
        return [];
    }

    private function sendMessageNotification($message,$type,$booking_id = ''){
        $from_user = $this->getUser($message['from']);
        $to_user = $this->getUser($message['to']);

        $notify['title'] = __('Booking message');
        $notify['body'] = $from_user->username.__(' sent you a message');

        $message_body = [
            'username' => $from_user->username,
            'user_image' =>  $from_user->user_image,
            'id' =>  $booking_id,
            'title' =>  __('Booking message'),
            'body' =>  $from_user->username.__(' sent you a message'),
            'message' =>  $message['message'],
            'type' => strtolower($type)
        ];
        $message_to = [
            'user_id'=>$to_user->id,
            'module_id'=>$to_user->default_module_id,
            'type' =>  MESSAGE_NOTIFICATION
        ];
        $service = new NotificationService();
        $service->sendMessage($message_body,$message_to);

    }
}
