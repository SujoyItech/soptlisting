<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_packages', function (Blueprint $table) {
            $table->increments('spotlist_packages_id');
            $table->string('spotlist_packages_name');
            $table->enum('packages_type',['Free','Paid'])->default('Paid');
            $table->decimal('price',10,2)->default(0);
            $table->integer('day_validity')->nullable();
            $table->integer('num_of_listing')->nullable();
            $table->integer('num_of_categories')->nullable();
            $table->integer('num_of_tags')->nullable();
            $table->integer('num_of_photos')->nullable();
            $table->boolean('add_video_ability')->default(0);
            $table->boolean('add_contact_form_ability')->default(0);
            $table->boolean('is_recomended')->default(0);
            $table->boolean('is_featured')->default(0);
            $table->text('description')->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_packages');
    }
}
