<div class="recent-area section-padding">
    <div class="container">
        <div class="section-title text-center sec-title-middle">
            <h2>{{__('Recently Added')}}</h2>
        </div>
        <div class="row">

                @foreach($recent_lists as $recent_list)
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="product-item">
                            <a href="{{route('user.listing').'/'.$recent_list->spotlist_directory_slug}}">
                                <div class="product-img">
                                    <img src="{{getImage($recent_list->thumb_image,'directory')}}" alt="">
                                </div>
                            </a>
                            <div class="product-content">
                                <h5><a href="{{route('user.listing').'/'.$recent_list->spotlist_directory_slug}}">{{$recent_list->spotlist_directory_name}}</a></h5>
                                <span>{{$recent_list->address ?? ''}}</span>
                                <div class="thumb-item">
                                    <ul class="list-rating">
                                        @for($x=0; $x<5 ; $x++)
                                            @if(floor($recent_list->rating)- $x >= 1 )
                                                <li><i class="fa fa-star"></i></li>
                                            @elseif($recent_list->rating - $x > 0)
                                                <li><i class="fa fa-star-half-o"></i></li>
                                            @else
                                                <li><i class="fa fa-star-o"></i></li>
                                            @endif
                                        @endfor
                                    </ul>
                                    <span>({{isset($recent_list->rating) ? number_format($recent_list->rating,1) : '0.0'}})</span>
                                </div>
                            </div>
                            @if($recent_list->is_featured == TRUE)
                                <div class="pro-featured">
                                    <p class="theme-btn v3">{{__('Featured')}}</p>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach

            <div class="col-12">
                <div class="btn text-center">
                    <a class="theme-btn" href="{{route('user.listing')}}">{{__('Explore More')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>


