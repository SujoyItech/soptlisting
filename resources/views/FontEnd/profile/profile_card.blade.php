<div class="col-lg-4 col-xl-4">
    <div class="card">
        <img class="card-img-top" src="{{user_image($user->user_image)}}">
        <div class="card-body">
            <h4 class="mb-0">{{$user->username ?? ''}}</h4>
            <p class="text-muted">{{'@'.$user->designation ?? ''}}</p>

            <div class="text-left mt-3 pro-textp">
                <h4 class="font-13 text-uppercase">{{__('About Me :')}}</h4>
                <p class="text-muted font-13 mb-3">
                    {{$user->about ?? ''}}
                </p>
                <p class="text-muted mb-2 font-13">
                    <strong>{{__('Full Name :')}}</strong>
                    <span class="ml-2">{{$user->username ?? ''}}</span>
                </p>

                <p class="text-muted mb-2 font-13">
                    <strong>{{__('Mobile : ')}}</strong>
                    <span class="ml-2">{{$user->mobile ?? ''}}</span>
                </p>

                <p class="text-muted mb-2 font-13"><strong>{{__('Email :')}}</strong> <span class="ml-2 ">{{$user->email ?? ''}}</span></p>

                <p class="text-muted mb-1 font-13"><strong>{{__('Location :')}}</strong> <span class="ml-2">{{$user->address ?? ''}}</span></p>
            </div>

            <ul class="profile-social">
                <li><a href="{{$user->facebook_link ?? ''}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/16.svg" alt=""></a></li>
                <li><a href="{{$user->twitter_link ?? ''}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/17.svg" alt=""></a></li>
                <li><a href="{{$user->instagram_link ?? ''}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/18.svg" alt=""></a></li>
                <li><a href="{{$user->linkedin_link ?? ''}}" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/19.svg" alt=""></a></li>
            </ul>
        </div>
    </div>
</div>
