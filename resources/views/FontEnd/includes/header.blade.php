<header>
    <div class="header-area @yield('head')" id="sticky-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-2 col-lg-2 col-md-3 col-5">
                    <div class="logo">
                        <a href="{{url('/')}}"><img src="{{isset($settings->app_logo) ? asset($settings->app_logo) : asset('FontEnd/assets/images/logo/logo.png')}}" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-8 d-none d-lg-block">
                    <div class="main-menu">
                        <nav class="nav_mobile_menu">
                            <ul>
                                <li class="@if(isset($menu) && $menu == 'home') active @endif">
                                    <a href="{{route('user.home')}}">{{__('Home')}}</a>
                                </li>
                                <li class="@if(isset($menu) && $menu == 'listing') active @endif">
                                    <a href="{{route('user.listing')}}">{{__('Listing')}}</a>
                                </li>
                                <li class="@if(isset($menu) && $menu == 'profile') active @endif"><a href="{{route('user.profile')}}">{{__('Profile')}}</a></li>
                                <li class="@if(isset($menu) && $menu == 'blog') active @endif">
                                    <a href="{{route('user.blog')}}">{{__('Blog')}}</a>
                                </li>
                                <li class="@if(isset($menu) && $menu == 'about') active @endif"><a href="{{route('user.about')}}">{{__('About Us')}}</a></li>
                                <li class="@if(isset($menu) && $menu == 'contact') active @endif"><a href="{{route('user.contact')}}">{{__('Contact Us')}}</a></li>
                                <li class="@if(isset($menu) && $menu == 'faq') active @endif"><a href="{{route('user.faq')}}">{{__('FAQ')}}</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-2 col-md-8 col-5">
                    <div class="head-btn-right">
                        @if(isset(\Illuminate\Support\Facades\Auth::user()->id))
                        <div class="bottom-header">
                            <ul>
                                <li>
                                    @include('FontEnd.includes.notification')
                                </li>
                            </ul>
                        </div>
                        @endif
                        <div class="header-btn">
                            <ul>
                                @if(isset(\Illuminate\Support\Facades\Auth::user()->id))
                                    <li class="pro-btn">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{isset(Auth::user()->user_image) && file_exists(Auth::user()->user_image) ? asset(Auth::user()->user_image) : asset('Backend/images/users/avatar.svg')}}" width="40" height="40" class="rounded-circle">
                                            <i class="text-light fa fa-caret-down" aria-hidden="true"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{url('user-profile-settings')}}"><i class="fa fa-user" aria-hidden="true"></i>  {{__('Profile')}}</a>
                                            <a class="dropdown-item" href="{{route('user.bookings')}}"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i>  {{__('My bookings')}}</a>
                                            <a class="dropdown-item" href="{{route('user.wishing')}}"><i class="fa fa-list" aria-hidden="true"></i>  {{__('My Wishlist')}}</a>
                                            <a class="dropdown-item" href="{{route('user-all-notification')}}"><i class="fa fa-bell" aria-hidden="true"></i>  {{__('My Notifications')}}</a>
                                            @if(\Illuminate\Support\Facades\Auth::user()->default_module_id == ADMIN)
                                                <a class="dropdown-item" href="{{route('admin.home')}}"><i class="fa fa-user" aria-hidden="true"></i>  {{__('Admin Dashboard')}}</a>
                                            @elseif(\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER)
                                                <a class="dropdown-item" href="{{route('listing-manager-dashboard')}}"><i class="fa fa-user" aria-hidden="true"></i>  {{__('Listing Manager Dashboard')}}</a>
                                            @endif
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="{{route('logout')}}"><i class="fa fa-sign-out"></i> {{__('Logout')}}</a>
                                        </div>
                                    </li>
                                @else
                                    <li><a class="sign-btn" href="{{route('login')}}">{{__('Sign in')}}</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-block d-lg-none col-2">
                    <div class="mobile_menu"></div>
                </div>
            </div>
        </div>
    </div>
</header>
