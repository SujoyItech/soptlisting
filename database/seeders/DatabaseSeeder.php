<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        $this->call(SysSystemSettingsSeeder::class);
        $this->call(SysDropdownsSeeder::class);
        $this->call(SpotlistCountriesSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(SysUsersSeeder::class);
        $this->call(SysUsersAdditionalInfoSeeder::class);
        $this->call(SpotlistUserSubscriptionSeeder::class);
        $this->call(SpotlistSubscriptionPurchaseHistorySeeder::class);
        $this->call(SpotlistPackagesSeeder::class);
        $this->call(SpotlistFeaturesSeeder::class);
        $this->call(SpotlistFaqsSeeder::class);
        $this->call(SpotlistDirectoryVsFeaturesSeeder::class);
        $this->call(SpotlistDirectoryVsCategorySeeder::class);
        $this->call(SpotlistDirectoryTypeRestaurantSeeder::class);
        $this->call(SpotlistDirectoryTypeLikeTableSeeder::class);
        $this->call(SpotlistDirectoryTypeHotelRoomSeeder::class);
        $this->call(SpotlistDirectoryScheduleSeeder::class);
        $this->call(SpotlistDirectoryReviewsSeeder::class);
        $this->call(SpotlistDirectoryGallarySeeder::class);
        $this->call(SpotlistDirectorySeeder::class);
        $this->call(SpotlistCitiesSeeder::class);
        $this->call(SpotlistCategorySeeder::class);
        $this->call(SpotlistBookingRequestSeeder::class);
        $this->call(SpotlistBlogsSeeder::class);
        $this->call(MigrationsSeeder::class);
        $this->call(LanguagesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(SpotlistBookingMessagesSeeder::class);
        $this->call(SpotlistDirectoryMessagesSeeder::class);
        $this->call(SpotlistDirectoryTypeBeautySeeder::class);
        $this->call(SpotlistDirectoryTypeFitnessSeeder::class);
        $this->call(SpotlistDirectoryTypeShopSeeder::class);
        $this->call(SpotlistDirectoryWishinglistSeeder::class);
        $this->call(SpotlistHowItWorksSeeder::class);
        $this->call(SpotlistOfflinePaymentSeeder::class);
        $this->call(SpotlistRatingQualitySeeder::class);
        $this->call(SpotlistUserFollowerSeeder::class);
        $this->call(SpotlistUserMessageTableSeeder::class);
        $this->call(SpotlistUserSubscriptionByEmailSeeder::class);
        $this->call(SpotlistingTeamsSeeder::class);
        $this->call(TranslationsSeeder::class);
        $this->call(UserVerificationCodesSeeder::class);
    }
}
