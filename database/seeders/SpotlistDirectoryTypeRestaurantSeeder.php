<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectoryTypeRestaurantSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory_type_restaurant')->delete();
        
        \DB::table('spotlist_directory_type_restaurant')->insert(array (
            0 => 
            array (
                'spotlist_directory_type_restaurant_id' => 1,
                'spotlist_directory_id' => 1,
                'menu_name' => 'Pizza',
                'menu_price' => '120.00',
                'ingredients_item' => 'Half',
                'menu_image' => 'Backend/images/directory/restaurant/QekntPdXCgRFf3of.jpg',
                'menu_description' => NULL,
                'status' => 'Active',
            ),
            1 => 
            array (
                'spotlist_directory_type_restaurant_id' => 2,
                'spotlist_directory_id' => 1,
                'menu_name' => 'Burger',
                'menu_price' => '20.00',
                'ingredients_item' => 'Chesse ,Chicken',
                'menu_image' => 'Backend/images/directory/restaurant/1agf3wcmF5AScL8N.jpg',
                'menu_description' => NULL,
                'status' => 'Active',
            ),
        ));

        
    }
}