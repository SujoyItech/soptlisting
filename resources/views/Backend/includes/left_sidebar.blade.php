<div class="left-side-menu">
    <div class="h-100" data-simplebar>
        @include('Backend.includes.sidelogo')
        <div id="sidebar-menu" class="mt-3">
            <ul id="side-menu">
                <li @if(!empty($menu) && $menu == 'dashboard') class="menuitem-active" @endif >
                    <a href="{{route('admin.home')}}" @if(!empty($menu) && $menu == 'dashboard') class="active" @endif>
                        <i class="fa fa-home"></i>
                        <span> {{__('Dashboard')}} </span>
                    </a>
                </li>
                <li @if(!empty($menu) && $menu == 'category') class="menuitem-active" @endif>
                    <a href="#sidebarCategory" data-toggle="collapse">
                        <i class="fa fa-list-alt"></i>
                        <span> {{__('Category')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse  @if(!empty($menu) && $menu == 'category') show @endif" id="sidebarCategory">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin-sub-category',['id'=>''])}}" @if(!empty($sub_menu) && $sub_menu == 'category_add') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new category')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-sub-category-list')}}" @if(!empty($sub_menu) && $sub_menu == 'category_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Category list')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-main-category')}}" @if(!empty($sub_menu) && $sub_menu == 'parent_category_list') class="active" @endif>
                                    <i class="fa fa-globe"></i>
                                    <span> {{__('Parent Category')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'all_directory') class="menuitem-active" @endif>
                    <a href="#sidebarAllDirectory" data-toggle="collapse">
                        <i class="fa fa-sitemap"></i>
                        <span> {{__('All Directory')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'all_directory') show @endif" id="sidebarAllDirectory">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('directory-list')}}" @if(!empty($sub_menu) && $sub_menu == 'all_directory_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Directory list')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-all-wishing-list')}}"  @if(!empty($sub_menu) && $sub_menu == 'all_wishing_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Directory Wishlist')}} </span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>

                <li @if(!empty($menu) && $menu == 'directory') class="menuitem-active" @endif>
                    <a href="#sidebarDirectory" data-toggle="collapse">
                        <i class="fa fa-sitemap"></i>
                        <span> {{__('My Directory')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'directory') show @endif" id="sidebarDirectory">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('directory-create')}}" @if(!empty($sub_menu) && $sub_menu == 'create_directory') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new Directory')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-drectory')}}"  @if(!empty($sub_menu) && $sub_menu == 'my_directory') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('My Directory')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-wishing-list')}}"  @if(!empty($sub_menu) && $sub_menu == 'my_wishing') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('My Wishlist')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'all_bookings') class="menuitem-active" @endif>
                    <a href="{{route('all-booking-request')}}">
                        <i class="fa fa-calendar"></i>
                        <span> {{__('All Bookings')}} </span>
                    </a>
                </li>
                <li @if(!empty($menu) && $menu == 'my_booking') class="menuitem-active" @endif>
                    <a href="#sidebarBooking" data-toggle="collapse">
                        <i class="fa fa-calendar"></i>
                        <span> {{__('My Bookings')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'booking') show @endif" id="sidebarBooking">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{url('user-booking-request/all')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('All')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user-booking-request/hotel')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Hotel')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user-booking-request/restaurant')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Restaurant')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user-booking-request/beauty')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Beauty')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user-booking-request/fitness')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Fitness')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'blog') class="menuitem-active" @endif>
                    <a href="#sidebarBlog" data-toggle="collapse" >
                        <i class="fa fa-align-left"></i>
                        <span> {{__('Blog')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'blog') show @endif" id="sidebarBlog">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin-blog-add')}}" @if(!empty($sub_menu) && $sub_menu == 'blog_add') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new post')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-blog-list')}}"  @if(!empty($sub_menu) && $sub_menu == 'blog_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Posts')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'city') class="menuitem-active" @endif>
                    <a href="#sidebarCity" data-toggle="collapse">
                        <i class="fa fa-location-arrow"></i>
                        <span> {{__('Cities')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'city') show @endif" id="sidebarCity">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin-city-create')}}" @if(!empty($sub_menu) && $sub_menu == 'city_add') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new city')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-city-list')}}" @if(!empty($sub_menu) && $sub_menu == 'city_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Cities')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'feature') class="menuitem-active" @endif>
                    <a href="#sidebarFeature" data-toggle="collapse" >
                        <i class="fa fa-list-alt"></i>
                        <span> {{__('Features')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'feature') show @endif " id="sidebarFeature">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('spotlist-feature')}}" @if(!empty($sub_menu) && $sub_menu == 'feature_add') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new feature')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('spotlist-feature-list')}}" @if(!empty($sub_menu) && $sub_menu == 'feature_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Features')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'faqs') class="menuitem-active" @endif>
                    <a href="#sidebarFaqs" data-toggle="collapse" >
                        <i class="fa fa-question-circle"></i>
                        <span> {{__('Faqs')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'faqs') show @endif" id="sidebarFaqs">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('spotlist-faqs')}}" @if(!empty($sub_menu) && $sub_menu == 'faqs_add') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new faqs')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('spotlist-faqs-list')}}" @if(!empty($sub_menu) && $sub_menu == 'faqs_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Faqs')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'pricing') class="menuitem-active" @endif>
                    <a href="#sidebarPricing" data-toggle="collapse" >
                        <i class="fa fa-credit-card"></i>
                        <span> {{__('Pricing')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'pricing') show @endif" id="sidebarPricing">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin-package-create')}}" @if(!empty($sub_menu) && $sub_menu == 'pricing_add') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new package')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-packages-list')}}" @if(!empty($sub_menu) && $sub_menu == 'pricing_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Packages')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'payment_history') class="menuitem-active" @endif>
                    <a href="{{route('admin-payment-history')}}">
                        <i class="fa fa-paperclip"></i>
                        <span> {{__('Payment history')}} </span>
                    </a>
                </li>

                <li @if(!empty($menu) && $menu == 'offline_payment') class="menuitem-active" @endif>
                    <a href="{{route('admin-offline-payment')}}">
                        <i class="fa fa-archive"></i>
                        <span> {{__('Offline payment')}} </span>
                    </a>
                </li>
                <li @if(!empty($menu) && $menu == 'user') class="menuitem-active" @endif>
                    <a href="#sidebarUser" data-toggle="collapse">
                        <i class="fa fa-user"></i>
                        <span> {{__('Users')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'user') show @endif" id="sidebarUser">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin-user-create')}}" @if(!empty($sub_menu) && $sub_menu == 'user_add') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new user')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-users-list')}}" @if(!empty($sub_menu) && $sub_menu == 'user_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Users')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-subscribed-email-list')}}" @if(!empty($sub_menu) && $sub_menu == 'email_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Subscribed emails')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin-user-message-list')}}" @if(!empty($sub_menu) && $sub_menu == 'message_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Messages List')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'settings') class="menuitem-active" @endif>
                    <a href="#sidebarSettings" data-toggle="collapse">
                        <i class="fa fa-cogs"></i>
                        <span> {{__('Settings')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'settings') show @endif" id="sidebarSettings">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{url('settings/admin_settings')}}" @if(!empty($sub_menu) && $sub_menu == 'admin_settings') class="active" @endif>
                                    <i class="fa fa-cog"></i>
                                    <span> {{__('Admin Setting')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('settings/fontend_settings')}}"  @if(!empty($sub_menu) && $sub_menu == 'fontend_settings') class="active" @endif>
                                    <i class="fa fa-cog"></i>
                                    <span> {{__('FontEnd Setting')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('settings/payment_settings')}}" @if(!empty($sub_menu) && $sub_menu == 'payment_settings') class="active" @endif>
                                    <i class="fa fa-cog"></i>
                                    <span> {{__('Payment Setting')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('how-it-works')}}" @if(!empty($sub_menu) && $sub_menu == 'how_works') class="active" @endif>
                                    <i class="fa fa-question-circle"></i>
                                    <span> {{__('How it works ?')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('about-us')}}" @if(!empty($sub_menu) && $sub_menu == 'about_us') class="active" @endif>
                                    <i class="fa fa-user-friends"></i>
                                    <span> {{__('About us')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('teams')}}" @if(!empty($sub_menu) && $sub_menu == 'teams') class="active" @endif>
                                    <i class="fa fa-user-friends"></i>
                                    <span> {{__('Our Team')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
