@extends('Backend.layouts.app',['menu'=>'category','sub_menu'=>'category_add'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-sub-category')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New Category')}}</a>
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-sub-category-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('Category list')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('Add new category')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{route('admin-subcategory-save')}}" class="parsley-examples" method="POST" id="submit_category" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">{{__('Category Name')}}<span class="text-danger">*</span></label>
                                <input type="text" name="spotlist_category_name" parsley-trigger="change" required value="{{$category->spotlist_category_name ?? ''}}"
                                       placeholder="Enter category name" class="form-control" id="spotlist_category_name">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label class="col-form-label" for="country">{{__('Parent category')}}</label>
                                <select class="form-control" name="parent_id">
                                    @if(isset($parent_category) && !empty($parent_category[0]))
                                        @foreach($parent_category as $pt)
                                            <option value="{{$pt->spotlist_category_id}}" {{is_selected($category->parent_id ?? '',$pt->spotlist_category_id)}}>{{$pt->spotlist_category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label class="col-form-label" for="country">{{__('Status')}}</label>
                                <select class="form-control" name="status">
                                    <option value="Active" {{is_selected($category->status ?? '','Active')}}>{{__('Active')}}</option>
                                    <option value="Inactive" {{is_selected($category->status ?? '','Inactive')}}>{{__('Inactive')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button class="btn btn-rounded btn-primary waves-effect waves-light category_submit" type="submit">
                                    <i class="fa fa-save"></i>
                                    {{__('Save')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="col-form-label" for="picture">{{__('Category Logo')}}</label>
                        <input type="file" name="category_image" data-plugins="dropify"
                               data-default-file="{{isset($category->category_image) ? asset($category->category_image) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="1M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                    </div>
                </div>
            </div>
        </form>
        <input type="hidden" name="spotlist_category_id" value="{{$category->spotlist_category_id ?? ''}}" id="spotlist_category_id">
    </div>
@endsection
@section('script')
    <script>
        $('.category_submit').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);

                    if ($('#spotlist_category_id').val().length !== 0){
                        formData.append('spotlist_category_id',$('#spotlist_category_id').val());
                    }
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            var redirect_url = "{{url('admin-sub-category-list')}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else {
                            swalError(response.message);
                        }
                    });
                }
            });
        });
    </script>
@endsection
