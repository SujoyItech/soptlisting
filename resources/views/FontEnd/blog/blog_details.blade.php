@extends('FontEnd.master',['menu'=>'blog'])
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Blog')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- blog-area-start -->
    <div class="blog-area section-padding">
        <div class="container">
            <div class="row">
                <div class="blog-wrap">
                    <div class="blog-top">
                        <h2>{{$blogs->spotlist_blogs_title}}</h2>
                        <div class="blog-ul">
                            <ul class="entry-meta">
                                <li>{{ Carbon\Carbon::parse($blogs->created_at)->format('d F Y') }}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="blog-bottom">
                        <img src="{{getImageUrl($blogs->blog_cover_image,'other')}}" alt="" width="100%" height="500">
                        <div class="blog-b-text">
                            {!! $blogs->contents  !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .articles-area-start -->
        <div class="articles-area blog-area">
            <div class="container">
                <div class="section-title text-center">
                    <h2>{{__('Check More Articles')}}</h2>
                    <a class="btn theme-btn" href="{{route('user.blog')}}">{{__('View All')}}</a>
                </div>
                <div class="row">

                    @if(isset($blog_lists) && !empty($blog_lists[0]))
                        <div class="deals-area mt-5">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                                        <div class="country-r">
                                            <div class="deals-carousel owl-carousel owl-theme owl-btn-center-lr owl-dots-primary-full owl-btn-3 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                                                @foreach($blog_lists as $blog_list)
                                                    <div class="product-item">
                                                        <a href="{{route('user.blog',['slug'=>$blog_list->slug])}}">
                                                            <div class="product-img">
                                                                <img src="{{getImage($blog_list->blog_thumb_image,'blog')}}" alt="" height="300">
                                                            </div>
                                                        </a>
                                                        <div class="product-content">
                                                            <h5><a href="{{route('user.blog',['slug'=>$blog_list->slug])}}">{{$blog_list->spotlist_blogs_title}}</a></h5>
                                                            <span>{{ Carbon\Carbon::parse($blog_list->created_at)->format('d F Y') }}</span>
                                                        </div>
                                                        @if(isset($blog_list->spotlist_category_name) && !empty($blog_list->spotlist_category_name))
                                                            <div class="thumb">
                                                                <span>{{$blog_list->spotlist_category_name}}</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- .articles-area-start -->
    </div>
    <!-- blog-area-end -->
    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')
@endsection
