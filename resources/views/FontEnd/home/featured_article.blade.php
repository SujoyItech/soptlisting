<!-- .articles-area-start -->
<div class="articles-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <div class="section-title">
                    <h2>{{__('Featured Articles')}}</h2>
                    <a class="btn theme-btn" href="{{route('user.blog')}}">{{__('View All')}}</a>
                </div>
                <div class="country-r">
                    <div class="country-carousel owl-carousel owl-theme owl-btn-center-lr owl-dots-primary-full owl-btn-3 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">

                        @foreach($featured_articles as $article)
                            <div class="item">
                                <a href="{{route('user.blog',['slug'=>$article->slug])}}">
                                    <div class="featured-item">
                                        <div class="featured-img">
                                            <img src="{{getImage($article->blog_thumb_image,'blog')}}" alt="" height="300">
                                        </div>
                                        <div class="featured-content">
                                            <p> {{ Carbon\Carbon::parse($article->created_at)->format('d F Y') }}</p>
                                            <h5><a href="{{route('user.blog',['slug'=>$article->slug])}}">{{$article->spotlist_blogs_title ?? ''}}</a></h5>
                                        </div>
                                        @if(isset($article->spotlist_category_name))
                                            <div class="thumb">
                                                <span>{{$article->spotlist_category_name}}</span>
                                            </div>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- .articles-area-start -->

