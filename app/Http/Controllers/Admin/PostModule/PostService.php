<?php


namespace App\Http\Controllers\Admin\PostModule;


use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostService {
    use DataSaveTrait;

    private function blogListData(){
        $blog_list = DB::table('spotlist_blogs')
                       ->leftJoin('spotlist_category','spotlist_blogs.spotlist_blogs_id','spotlist_category.spotlist_category_id')
                       ->select('spotlist_blogs.spotlist_blogs_id','spotlist_blogs.spotlist_blogs_title','spotlist_blogs.status','spotlist_category.spotlist_category_name as category_name')
                       ->get();
        return $blog_list;
    }
    public function getBlogData($blog_id=NULL){
        $query = DB::table('spotlist_blogs')->leftJoin('sys_users','spotlist_blogs.sys_users_id','=','sys_users.id')
        ->leftJoin('spotlist_category','spotlist_blogs.spotlist_category_id','=','spotlist_category.spotlist_category_id')
        ->select('spotlist_blogs.*','sys_users.name','spotlist_category.spotlist_category_name');
        return $blog_id != NULL ?  $query->where('spotlist_blogs.spotlist_blogs_id',$blog_id)->first():$query->orderBy('updated_at','desc')->get();
    }

    public function adminBlogSave(Request $request){
        $post_data = $this->postBlogData($request);
        $response = $this->insertOrUpdateTable('spotlist_blogs','spotlist_blogs_id',$request->spotlist_blogs_id ?? '',$post_data);
        return $response;
    }

    private function postBlogData(Request $request){
        $post_data  = [
            'spotlist_blogs_title' => $request->spotlist_blogs_title,
            'spotlist_category_id' => $request->spotlist_category_id,
            'is_featured' => $request->is_featured,
            'contents' => $request->contents
        ];
        if (!empty($request->spotlist_blogs_id)){
            $post_data['updated_at'] = Carbon::now();
            $post = DB::table('spotlist_blogs')->where('spotlist_blogs_id',$request->spotlist_blogs_id)->first();
        }else{
            $post_data['created_at'] = Carbon::now();
            $post_data['updated_at'] = Carbon::now();
            $post_data['slug'] = $request->slug;
        }
        if (!empty($request->blog_thumb_image)){
            $post_data['blog_thumb_image'] = fileUpload($request->blog_thumb_image,'Backend/images/blogs/',NULL,$post->blog_thumb_image ?? '',300,300);
        }
        if (!empty($request->blog_cover_image)){
            $post_data['blog_cover_image'] = fileUpload($request->blog_cover_image,'Backend/images/blogs/',NULL,$post->blog_cover_image ?? '',600,400);
        }
        return $post_data;
    }

}
