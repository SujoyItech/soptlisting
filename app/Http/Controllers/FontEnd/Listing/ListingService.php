<?php


namespace App\Http\Controllers\FontEnd\Listing;


use App\Http\Service\NotificationService;
use App\Traits\DataReturn;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ListingService {

    use DataReturn;
    use DataSaveTrait;

    public function getDirectoryList($id=NULL,$user_id=NULL,$paginate=NULL,$where_in=[],$where=[],$where_not_in=[]){
        if (!empty($id)){
            $join_table = [
                ['left','spotlist_countries','spotlist_countries.id','spotlist_directory.country_id'],
                ['left','spotlist_cities','spotlist_cities.spotlist_cities_id','spotlist_directory.city'],
                ['left','sys_users','spotlist_directory.sys_users_id','sys_users.id'],
                ['left','spotlist_category','spotlist_directory.spotlist_parent_category_id','spotlist_category.spotlist_category_id'],
                ['left','spotlist_directory_reviews','spotlist_directory.spotlist_directory_id','spotlist_directory_reviews.spotlist_directory_id'],
            ];
        }else{
            $join_table = [
                ['join','spotlist_countries','spotlist_countries.id','spotlist_directory.country_id'],
                ['join','spotlist_cities','spotlist_cities.spotlist_cities_id','spotlist_directory.city'],
                ['join','sys_users','spotlist_directory.sys_users_id','sys_users.id'],
                ['join','spotlist_category','spotlist_directory.spotlist_parent_category_id','spotlist_category.spotlist_category_id'],
                ['join','spotlist_directory_reviews','spotlist_directory.spotlist_directory_id','spotlist_directory_reviews.spotlist_directory_id'],
            ];
        }

        $select_table = ['spotlist_directory.*','spotlist_countries.name as country_name',
                         'spotlist_cities.spotlist_cities_name as city_name','sys_users.id as user_id','sys_users.name as user_name',
                         'spotlist_category.spotlist_category_name as category_name',
                         DB::raw("COUNT(spotlist_directory_reviews.id) as total_reviews,AVG(spotlist_directory_reviews.rating) as rating")];
        $group_by = ['spotlist_directory.spotlist_directory_id'];
        if ($id != NULL){
            return $this->joinTableInfo('spotlist_directory',$join_table,$select_table,['spotlist_directory.spotlist_directory_id',$id],[],[],[],NULL,$group_by);
        }elseif ($user_id != NULL){
            return $this->joinTableInfo('spotlist_directory',$join_table,$select_table,[],['spotlist_directory.sys_users_id',$user_id],[],['spotlist_directory.created_at','desc'],$paginate ?? '',$group_by);
        }else{
            $order_table = ['spotlist_directory.spotlist_directory_id','desc'];
            return $this->joinTableInfo('spotlist_directory',$join_table,$select_table,[],[],$where ?? [],$order_table,$paginate,$group_by,$where_in,$where_not_in);
        }
    }

    public function getList($id=NULL,$user_id=NULL){
        if ($id != NULL){
            $data['directory'] = $this->getDirectoryList($id);
            $data['features'] = $this->getSpotListFeatures($id);
            $data['working_hours'] = $this->basicTableInfo('spotlist_directory_schedule',[],['spotlist_directory_id',$id]);
            $data['gallery'] = $this->basicTableInfo('spotlist_directory_gallary',[],[],['spotlist_directory_id',$id]);
            $data['user'] = $this->basicTableInfo('sys_users',[],['id',$data['directory']->sys_users_id]);
            $reviews_join_table = [['left','sys_users','spotlist_directory_reviews.user_id','sys_users.id']];
            $reviews_select_table = ['spotlist_directory_reviews.*','sys_users.name','sys_users.user_image'];
            $data['reviews'] = $this->joinTableInfo('spotlist_directory_reviews',$reviews_join_table,$reviews_select_table,[],['spotlist_directory_id',$id],[],['created_at','desc'],20);
            $data['types'] = $this->getDirectoryTypes($id, $data['directory']->spotlist_parent_category_id ?? '');
            $where_not [0] = 'spotlist_directory.spotlist_directory_id';
            $where_not [1] = [0=>$id];
            $data['list_by_category'] = $this->getDirectoryList(NULL,NULL,NULL,[],['spotlist_directory.spotlist_parent_category_id'=>$data['directory']->spotlist_parent_category_id ,'sys_users.status'=>'Active'],$where_not);

            if (isset(Auth::user()->id)){
                $data['wishlist'] = DB::table('spotlist_directory_wishinglist')->where(['sys_users_id'=>Auth::user()->id,'spotlist_directory_id'=>$id])->first();
                $data['user_ratings'] = DB::table('spotlist_directory_reviews')->where(['user_id'=>Auth::user()->id,'spotlist_directory_id'=>$id])->first();
            }

        }else{
            $data['list'] = $this->getDirectoryList(NULL,NULL,21,[],['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active','sys_users.status'=>'Active']);
            $data['categories'] = $this->basicTableInfo('spotlist_category',[],[],[],['status'=>'Active','parent_id'=>0]);
            $data['locations'] = $this->basicTableInfo('spotlist_cities',[],[],[],['status'=>'Active']);
            $data['features'] = $this->basicTableInfo('spotlist_features',[],[],[],['status'=>'Active']);
        }

        return $data;
    }

    private function getSpotListFeatures($id){
        $feature_join_table = [
            ['left','spotlist_features','spotlist_directory_vs_features.spotlist_features_id','spotlist_features.spotlist_features_id']
        ];
        $select_table = ['spotlist_features.spotlist_features_name','spotlist_features.spotlist_features_icon','spotlist_directory_vs_features.spotlist_features_id'];

        return $this->joinTableInfo('spotlist_directory_vs_features',$feature_join_table,$select_table,[],['spotlist_directory_vs_features.spotlist_directory_id',$id]);
    }

    private function getDirectoryTypes($id,$parent_category_id){
        if ($parent_category_id == CATEGORY_HOTEL){
            return DB::table('spotlist_directory_type_hotel_room')
                      ->leftJoin('spotlist_directory_type_like_table','spotlist_directory_type_hotel_room.spotlist_directory_type_hotel_room_id','=','spotlist_directory_type_like_table.spotlist_directory_type_id')
                      ->select('spotlist_directory_type_hotel_room.*',DB::raw("SUM(spotlist_directory_type_like_table.like) as total_like"))
                      ->where('spotlist_directory_type_hotel_room.spotlist_directory_id',$id)
                      ->groupBy('spotlist_directory_type_hotel_room.spotlist_directory_type_hotel_room_id')
                      ->get();
        }elseif ($parent_category_id == CATEGORY_RESTAURANT){
            return DB::table('spotlist_directory_type_restaurant')
                      ->leftJoin('spotlist_directory_type_like_table','spotlist_directory_type_restaurant.spotlist_directory_type_restaurant_id','=','spotlist_directory_type_like_table.spotlist_directory_type_id')
                      ->select('spotlist_directory_type_restaurant.*',DB::raw("SUM(spotlist_directory_type_like_table.like) as total_like"))
                      ->where('spotlist_directory_type_restaurant.spotlist_directory_id',$id)
                      ->groupBy('spotlist_directory_type_restaurant.spotlist_directory_type_restaurant_id')
                      ->get();
        }elseif ($parent_category_id == CATEGORY_BEAUTY){
            return DB::table('spotlist_directory_type_beauty')
                     ->leftJoin('spotlist_directory_type_like_table','spotlist_directory_type_beauty.spotlist_directory_type_beauty_id','=','spotlist_directory_type_like_table.spotlist_directory_type_id')
                     ->select('spotlist_directory_type_beauty.*',DB::raw("SUM(spotlist_directory_type_like_table.like) as total_like"))
                     ->where('spotlist_directory_type_beauty.spotlist_directory_id',$id)
                     ->groupBy('spotlist_directory_type_beauty.spotlist_directory_type_beauty_id')
                     ->get();
        }elseif ($parent_category_id == CATEGORY_SHOPPING){
            $data = DB::table('spotlist_directory_type_shop')
                      ->leftJoin('spotlist_directory_type_like_table','spotlist_directory_type_shop.spotlist_directory_type_shop_id','=','spotlist_directory_type_like_table.spotlist_directory_type_id')
                ->select('spotlist_directory_type_shop.*',DB::raw("SUM(spotlist_directory_type_like_table.like) as total_like"))
                ->where('spotlist_directory_type_shop.spotlist_directory_id',$id)
                ->groupBy('spotlist_directory_type_shop.spotlist_directory_type_shop_id')
                ->get();
           return $data;
        }elseif ($parent_category_id == CATEGORY_FITNESS){
            return DB::table('spotlist_directory_type_fitness')
              ->leftJoin('spotlist_directory_type_like_table','spotlist_directory_type_fitness.spotlist_directory_type_fitness_id','=','spotlist_directory_type_like_table.spotlist_directory_type_id')
              ->select('spotlist_directory_type_fitness.*',DB::raw("SUM(spotlist_directory_type_like_table.like) as total_like"))
              ->where('spotlist_directory_type_fitness.spotlist_directory_id',$id)
              ->groupBy('spotlist_directory_type_fitness.spotlist_directory_type_fitness_id')
              ->get();
        }else{
            return [];
        }
    }

    public function showListBySearch($data){
        $query = DB::table('spotlist_directory')
            ->join('sys_users','spotlist_directory.sys_users_id','=','sys_users.id')
            ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','=','spotlist_category.spotlist_category_id')
            ->join('spotlist_cities','spotlist_directory.city','=','spotlist_cities.spotlist_cities_id')
           ->select('spotlist_directory.spotlist_directory_id')
           ->groupBy('spotlist_directory.spotlist_directory_id')
           ->where(['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active','sys_users.status'=>'Active']);

        if (!empty($data['search_key']['search_text'])){
            $query = $query->where('spotlist_directory.spotlist_directory_name','like',$data['search_key']['search_text']);
        }
        if (!empty($data['search_key']['features'])){
            $query = $query->leftJoin('spotlist_directory_vs_features','spotlist_directory.spotlist_directory_id','spotlist_directory_vs_features.spotlist_directory_id')
                           ->whereIn('spotlist_directory_vs_features.spotlist_features_id',$data['search_key']['features']);
        }
        if (!empty($data['search_key']['rating'])){
            $query = $query->leftJoin('spotlist_directory_reviews','spotlist_directory.spotlist_directory_id','spotlist_directory_reviews.spotlist_directory_id')
                           ->havingRaw( DB::raw("FLOOR( AVG(spotlist_directory_reviews.rating)) IN ( ".implode(',',$data['search_key']['rating']). ")"));
        }

        if (!empty($data['search_key']['categories']) ){
            $query = $query->whereIn('spotlist_directory.spotlist_parent_category_id',$data['search_key']['categories']);
        }

        if (!empty($data['search_key']['locations'])){
            $query = $query->whereIn('spotlist_directory.city',$data['search_key']['locations']);
        }

        $query = $query->pluck('spotlist_directory_id')->toArray();
        $where_in [0] = 'spotlist_directory.spotlist_directory_id';
        $where_in [1] = $query;
        $data['search_result'] = $this->getDirectoryList(NULL,NULL,21,$where_in,['spotlist_directory.status'=>'Active']);
        $data['search_result_map'] = $this->getDirectoryList(NULL,NULL,NULL,$where_in,['spotlist_directory.status'=>'Active']);
        return $data;
    }

    private function getTypeDataByTable($table_name,$table_primary_key,$id){

        $data = DB::table($table_name)
                 ->leftJoin('spotlist_directory_type_like_table',$table_primary_key,'=','spotlist_directory_type_like_table.spotlist_directory_type_id')
                 ->select($table_name.'.*',DB::raw("SUM(spotlist_directory_type_like_table.like) as total_like"))
                 ->where(['spotlist_directory_type_like_table.spotlist_directory_id'=>$id,$table_name.'.status'=>'Active'])
                 ->groupBy($table_primary_key)
                 ->get();
        return $data;
    }

    public function getUserRating(Request $request){
       return $this->basicTableInfo('spotlist_directory_reviews',[],['user_id',Auth::user()->id],['spotlist_directory_id',$request->spotlist_directory_id ?? '']);
    }

    public function directoryRatingSave(Request $request){
        $post_data = $request->except('_token','id');
        $post_data['user_id'] = Auth::user()->id;
        $post_data['created_at'] = Carbon::now();
        $post_data['updated_at'] = Carbon::now();
        $response = $this->insertOrUpdateTable('spotlist_directory_reviews','id',$request->id,$post_data,'Your rating saved successfully','Your rating save failed!');
        return $response;
    }

    public function addListToWishlist($id){
        $is_wished = DB::table('spotlist_directory_wishinglist')->where(['sys_users_id' => Auth::user()->id, 'spotlist_directory_id' => $id])->first();
        $directory = DB::table('spotlist_directory')->where('spotlist_directory_id',$id)->first();
        $post_data = [
            'sys_users_id' => Auth::user()->id,
            'spotlist_directory_id' => $id,
            'created_at' =>  Carbon::now(),
        ];
        $noify = [];
        $noify['user_id'] = $directory->sys_users_id;
        if (isset($is_wished)){
            if ($is_wished->is_wished == TRUE){
                $post_data['is_wished'] = FALSE;
                $success_message = __('Directory removed from wishlist.');
                $noify['title'] =  __('Removed from wishlist.');
                $noify['body'] = Auth::user()->username.__(' removed ').$directory->spotlist_directory_name.__(' from wishlist');
                $this->listingWishingCount($id,FALSE);
            }else{
                $post_data['is_wished'] = TRUE;
                $success_message = __('Directory added to wishlist.');
                $noify['title'] =__('Added to wishlist.');
                $noify['body'] = Auth::user()->username.__(' added ').$directory->spotlist_directory_name.__(' to wishlist');
                $this->listingWishingCount($id,TRUE);
            }
        }else{
            $post_data['is_wished'] = TRUE;
            $success_message = __('Directory added to wishlist.');
            $noify['title'] = __('Added to wishlist.');
            $noify['body'] = Auth::user()->username.__(' added ').$directory->spotlist_directory_name.__(' to wishlist');
            $this->listingWishingCount($id,TRUE);
        }
        $conditions = ['sys_users_id' => Auth::user()->id, 'spotlist_directory_id' => $id];
        $response = $this->insertOrUpdateTableOnMultipleCondition('spotlist_directory_wishinglist',$conditions,$post_data,$success_message,__('wishlist failed'));
        $this->userWishingNotificationSend($noify);
        return $response;
    }

    public function directoryTypeLikeSave(Request $request){
        $post_data = $request->except('spotlist_like');
        $post_data['sys_users_id'] = Auth::user()->id;
        $post_data['like'] = (int)$request->spotlist_like === 1 ? 0 : 1;
        $condition_array = [
            'sys_users_id'=>Auth::user()->id,
            'spotlist_directory_id'=>$request->spotlist_directory_id,
            'spotlist_directory_type_id'=>$request->spotlist_directory_type_id
        ];
        $response = $this->insertOrUpdateTableOnMultipleCondition('spotlist_directory_type_like_table',$condition_array,$post_data);
        return $response;
    }



    public function showAllListing(){
        return $this->getDirectoryList(NULL,NULL,21,[],['spotlist_directory.status'=>'Active']);
    }

    public function showListByCategory($category_id){
        return $this->getDirectoryList(NULL,NULL,21,[],['spotlist_directory.spotlist_parent_category_id'=>$category_id,'spotlist_directory.status'=>'Active','spotlist_cities.status'=>'Active']);
    }
    public function showListByPlace($place_id){
        if ($place_id != NULL){
            return $this->getDirectoryList(NULL,NULL,21,[],['spotlist_directory.city'=>$place_id,'spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active']);
        }else{
            return $this->getDirectoryList(NULL,NULL,21,[],['spotlist_directory.status'=>'Active']);
        }

    }

    public function listingBookingsCount($directory_id){
        DB::table('spotlist_directory')->where('spotlist_directory_id',$directory_id)->increment('booking_count',1);
    }
    private function listingWishingCount($directory_id,$increment){
        $query = DB::table('spotlist_directory')->where('spotlist_directory_id',$directory_id);
        $query = $increment == TRUE ? $query->increment('wishing_count',1) : $query->decrement('wishing_count',1);
    }
    public function visitCount($directory_id){
        DB::table('spotlist_directory')->where('spotlist_directory_id',$directory_id)->increment('visit_count',1);
    }



    private function userWishingNotificationSend($notify){
        $notify_body = [
            'username' => Auth::user()->name,
            'user_image' => Auth::user()->user_image,
            'title' => $notify['title'],
            'body' => $notify['body'],
        ];
        $notify_to = [
            'user_id' => $notify['user_id'],
            'type' => WISHING,
        ];
        $service = new NotificationService();
        $service->sendNotification($notify_body,$notify_to);
    }


}
