<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->default_module_id == SYSTEM_ADMIN || Auth::user()->default_module_id == ADMIN){
            return $next($request);
        }else{
            return redirect()->route('no-permission');
        }

    }
}
