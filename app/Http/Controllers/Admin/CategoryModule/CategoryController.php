<?php

namespace App\Http\Controllers\Admin\CategoryModule;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    private $service;

    public function __construct() {
        $this->service  = new CategoryService();
    }

    public function adminSubCategory($id = NULL){
        $data = [];
        if ($id !== NULL){
            $data['category'] = DB::table('spotlist_category')->where(['spotlist_category_id'=>$id])->first();
        }
        $data['parent_category'] = DB::table('spotlist_category')->where(['parent_id'=>0,'status'=>'Active'])->get();
        return view('Admin.Category.sub_category_add',$data);
    }

    public function adminSubCategorySave(Request $request){
        $response = $this->service->adminSubCategorySave($request);
        return response()->json($response);
    }

    public function adminSubCategoryList(Request $request){
        $categoryList = $this->service->getSubCategoryList();
        if ($request->ajax()){
            return datatables($categoryList)
                ->editColumn('category_image', function ($item) {
                    return '<img src="'.asset($item->category_image).'" class="rounded-circle" width="50" height=50>';
                })
               ->editColumn('status', function ($item) {
                    if ($item->status == 'Active'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-warning">'.$item->status.'</span>';
                    }
                })
                ->rawColumns(['category_image','status'])
                ->make(TRUE);
        }
        return view('Admin.Category.sub_category_list');
    }

    public function adminMainCategory(){
        $data['categories'] = $this->service->getCategoryData();
        return view('Admin.Category.category_list',$data);
    }

    public function adminMainCategoryPictureChange(Request $request){
        $response = $this->service->categoryPictureChange($request);
        return response()->json($response);
    }

    public function adminChildCategoryDelete(Request $request){
        $response = $this->service->removeChildCategory($request);
        return response()->json($response);
    }

    public function adminMainCategoryStatusChange(Request $request){
        $response = $this->service->adminMainCategoryStatusChange($request);
        return response()->json($response);
    }

    public function adminCityList(Request  $request){
        $cityList = $this->service->getCityWithCountryData();

        if ($request->ajax()){
            return datatables($cityList)
                ->editColumn('city_image', function ($item) {
                    return '<img src="'.asset($item->city_image).'" class="rounded-circle" width="50" height=50>';
                })->editColumn('status', function ($item) {
                    if ($item->status == 'Active'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-warning">'.$item->status.'</span>';
                    }
                })->editColumn('action', function ($item) {
                    $html = '<a href="'.url('admin-city-create').'/'.$item->spotlist_cities_id.'" class="btn btn-xs btn-info edit" data-id="'.$item->spotlist_cities_id.'"><i class="fa fa-edit"></i></a>
                            <button class="btn btn-xs btn-danger delete_row" data-id="'.$item->spotlist_cities_id.'"><i class="fa fa-trash"></i></button>';
                    return $html;

                })
                ->rawColumns(['city_image','status','action'])
                ->make(TRUE);
        }
        return view('Admin.Cities.city_list');
    }

    public function checkCitySlug(Request $request){
        $city = DB::table('spotlist_cities')->where('spotlist_cities_slug','=',$request->slug)->first();
        if (isset($city) && !empty($city)){
            $response['success'] = FALSE;
        }else{
            $response['success'] = TRUE;
        }
        return response()->json($response);
    }
    public function adminCityAdd($id = NULL){
        $data = $this->service->getCityData($id);
        $data['countries'] = DB::table('spotlist_countries')->get();
        return view('Admin.Cities.city_add',$data);
    }

    public function adminCitySave(Request  $request){
        $response = $this->service->adminCitySave($request);
        return response()->json($response);
    }

    public function checkCityUsedInDirectory(Request $request){
        $response = $this->service->checkCityUsedInDirectory($request);
        return response()->json($response);
    }


    public function spotlistFeatures($id = NULL){
        $data = [];
        if ($id != NULL){
            $data['features'] = $this->service->getFeaturesData($id);
        }
        return view('Admin.Features.add_features',$data);
    }

    public function spotlistFeaturesList(Request $request){
        $featuresList = $this->service->getFeaturesData();
        if ($request->ajax()){
            return datatables($featuresList)
                ->editColumn('spotlist_features_icon', function ($item) {
                    return '<img src="'.asset($item->spotlist_features_icon).'" class="rounded-circle" width="50" height=50>';
                })->editColumn('status', function ($item) {
                    if ($item->status == 'Active'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-warning">'.$item->status.'</span>';
                    }
                })
                ->rawColumns(['spotlist_features_icon','status'])
                ->make(TRUE);
        }
        return view('Admin.Features.spotlist_features_list');
    }

    public function spotlistFeaturesSave(Request $request){
        $response = $this->service->spotlistFeaturesSave($request);
        return response()->json($response);
    }

    public function spotlistFaqs($id = NULL){
        $data = [];
        if ($id != NULL){
            $data['faqs'] = $this->service->getFaqsData($id);
        }
        return view('Admin.Faqs.add_faqs',$data);
    }

    public function spotlistFaqsList(Request $request){
        $faqsList = $this->service->getFaqsData();
        if ($request->ajax()){
            return datatables($faqsList)
                ->editColumn('description', function ($item) {
                    return substr($item->description,0,200);
                })->editColumn('status', function ($item) {
                    if ($item->status == 'Active'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-warning">'.$item->status.'</span>';
                    }
                })
                ->rawColumns(['status'])
                ->make(TRUE);
        }
        return view('Admin.Faqs.spotlist_faqs_list');
    }

    public function spotlistFaqsSave(Request $request){
        $response = $this->service->spotlistFaqsSave($request);
        return response()->json($response);
    }
}
