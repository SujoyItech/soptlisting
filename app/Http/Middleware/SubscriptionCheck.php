<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SubscriptionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->default_module_id == LISTING_MANAGER){
            if (availableListing(Auth::user()->id) >= 0){
                return $next($request);
            }else{
                return redirect()->route('listing-manager-pricing');
            }
        }
        return $next($request);
    }


}
