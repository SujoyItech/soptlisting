<link href="{{asset('/Backend/css/bootstrap-modern.min.css')}}" rel="stylesheet" id="bs-default-stylesheet">
<link href="{{asset('/Backend/css/app.min.css')}}" rel="stylesheet" id="app-default-stylesheet">
<link href="{{asset('/Backend/css/bootstrap-modern-dark.min.css')}}" rel="stylesheet" id="bs-dark-stylesheet" disabled>
<link href="{{asset('/Backend/css/app-dark.min.css')}}" rel="stylesheet" id="app-dark-stylesheet" disabled>

<link href="{{asset('/Backend/css/icons.min.css')}}" rel="stylesheet">

<link href="{{asset('/Backend/')}}/libs/summernote/summernote-bs4.min.css" rel="stylesheet" type="text/css" />


<link href="{{asset('/Backend/vendors/ladda/ladda-themeless.min.css')}}" rel="stylesheet">
<link href="{{asset('/Backend/vendors/sweetalert/sweetalert.css')}}" rel="stylesheet">
<link href="{{asset('/Backend/vendors/bsDatepickers/daterangepicker.css')}}" rel="stylesheet"/>

<link href="{{asset('/Backend/vendors/DataTables/css/datatables.min.css')}}" rel="stylesheet"/>
<link href="{{asset('/Backend/vendors/DataTables/css/dataTables.jqueryui.min.css')}}" rel="stylesheet"/>
<link href="{{asset('/Backend/vendors/DataTables/css/jquery.dataTables.min.css')}}" rel="stylesheet"/>


<link href="{{asset('/Backend/libs/dropzone/min/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/Backend/libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />

<script src="{{asset('/Backend/js/vendor.min.js')}}"></script>
<link href="{{asset('/Backend/css/laraframe.css')}}" rel="stylesheet newest">

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js" type="text/javascript"></script>

<script src="{{asset('/Backend/js/validator.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/bsDatepickers/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/bsDatepickers/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/bsDatepickers/daterangepicker.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/ladda/spin.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/ladda/ladda.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/ladda/ladda.jquery.min.js')}}"></script>
<script src="{{asset('/Backend/vendors/sweetalert/sweetalert2.all.min.js')}}"></script>
<link href="{{asset('Backend/')}}/libs/toastr/build/toastr.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('/Backend/libs/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />

@yield('style')
