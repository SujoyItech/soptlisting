<form method="post" action="{{route('userRegistrationModal')}}" class="parsley-examples" id="registration">
    <div class="col-12  mb-3">
        <label for="">{{__('Full Name')}}</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Enter Your Full Name" value="{{ old('name') }}" parsley-trigger="change" required>
        <input type="hidden" name="module_id" value="{{SYSTEM_USER}}">
    </div>
    <div class="col-12  mb-3">
        <label for="">{{__('E-mail Address')}}</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="your@email.com" value="{{__(old('email'))}}" parsley-trigger="change" required>
        <span class="text-danger d-none" id="email_error"></span>
    </div>
    <div class="col-12  mb-3">
        <label for="">{{__('Password')}}</label>
        <input type="password" name="password" id="pass1" class="form-control" placeholder="Minimum 6 character" parsley-trigger="change" required>
    </div>
    <div class="col-12 mb-3">
        <label for="">{{__('Confirm Password')}}</label>
        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Minimum 6 character" data-parsley-equalto="#pass1" required>
    </div>
    <div class="submit-btn col-12  mb-3">
        <button type="submit" class="theme-btn register_btn">{{__('Sign Up')}}</button>
    </div>
    <div class="col-12 sign-b">
        <p>{{__('Already have an account?')}} <a href="javascript:void(0);" class="login_btn">{{__('Return to Sign In')}}</a></p>
    </div>
</form>
<script>
    $(document).on('click','.login_btn',function (e){
        $('#loginModalLabel').html('Login');
        $('.login').removeClass('d-none');
        $('.register').addClass('d-none');
    })
</script>
