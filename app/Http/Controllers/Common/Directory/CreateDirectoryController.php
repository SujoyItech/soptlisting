<?php

namespace App\Http\Controllers\Common\Directory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateDirectoryController extends Controller
{
    private $create_directory_service,$directory_service;

    public function __construct() {
        $this->create_directory_service  = new CreateDirectoryService();
        $this->directory_service  = new DirectoryService();
    }

    public function checkDirectorySlug(Request $request){
        $directory = DB::table('spotlist_directory')->where('spotlist_directory_slug',$request->spotlist_directory_slug)->first();
        return !isset($directory) ? response()->json(['success'=>TRUE]) : response()->json(['success'=>FALSE]);
    }

    public function directoryAdd($id = NULL){
        $data = [];
        if ($id != NULL){
            $data = $this->directory_service->getDirectoryAllInfo($id);
        }
        $data['features'] = DB::table('spotlist_features')->where('status','Active')->get();
        $data['main_categories'] = DB::table('spotlist_category')->where(['parent_id'=>0,'status'=>'Active'])->get();
        $data['sub_categories'] = DB::table('spotlist_category')->where('parent_id','<>',0)->where(['status'=>'Active'])->get();
        $data['cities'] = DB::table('spotlist_cities')->where(['status'=>'Active'])->get();
        if (Auth::user()->default_module_id == LISTING_MANAGER){
            $data['directory_subscription'] = DB::table('spotlist_user_subscription')
                ->where('user_id',Auth::user()->id)->first();
        }
        return view('Common.Directory.directory_add',$data);
    }


    public function directoryBasicSave(Request $request){

        $response = $this->create_directory_service->directoryBasicSave($request);
        return response()->json($response);
    }

    public function directoryLocationSave(Request $request){
        $response = $this->create_directory_service->directoryLocationSave($request);
        return response()->json($response);
    }

    public function directoryFeaturesSave(Request $request){
        $response = $this->create_directory_service->directoryFeaturesSave($request);
        return response()->json($response);
    }

    public function directoryGallerySave(Request $request){
        $response = $this->create_directory_service->directoryGallerySave($request);
        return response()->json($response);
    }

    public function directorySeoSave(Request $request){
        $response = $this->create_directory_service->directorySeoSave($request);
        return response()->json($response);
    }

    public function directoryScheduleSave(Request $request){
        $response = $this->create_directory_service->directoryScheduleSave($request);
        return response()->json($response);
    }

    public function directoryContactSave(Request $request){
        $response = $this->create_directory_service->directoryContactSave($request);
        return response()->json($response);
    }

    public function directoryTypeSave(Request $request){
        $response = $this->create_directory_service->directoryTypeSave($request);
        return response()->json($response);
    }

    public function directoryAllSave(Request $request){
        $response = $this->create_directory_service->directoryAllSave($request);
        return response()->json($response);
    }

    public function removeDirectoryFile(Request $request){
        $response = $this->create_directory_service->removeDirectoryFile($request);
        return response()->json($response);
    }

    public function removeDirectoryGalleryFile(Request $request){
        $response = $this->create_directory_service->removeDirectoryGalleryFile($request);
        return response()->json($response);
    }

    public function changeCitiesByCountry(Request $request){

        $response['data'] = DB::table('spotlist_cities')->where(['country_id' => $request->country_id, 'status'=>'Active'])->get();
        return response()->json($response);
    }

}
