<div class="testimonial-area section-padding">
    <div class="container">
        <div class="section-title text-center sec-title-middle">
            <h2>{{__('Testimonials')}}</h2>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <div class="test-wrap">
                    <div class="testimonial-slider clearfix">

                            @foreach($recent_reviews as $review)
                                <div class="grid">
                                    <div class="client-info">
                                        <div class="client-img">
                                            <img src="{{isset($review->user_image) && file_exists($review->user_image) ? asset($review->user_image) : asset('Backend/images/users/avatar.svg')}}" alt="" height="50">
                                        </div>
                                        <div class="client-text">
                                            <h5>{{$review->username ?? ''}}</h5>
                                            <span>{{isset($review->created_at) ? Carbon\Carbon::parse($review->created_at)->diffForHumans() : ''}}</span>
                                        </div>
                                    </div>
                                    <div class="ratting">
                                        <ul>
                                            @for($x=0; $x<5 ; $x++)
                                                @if(floor($review->rating)- $x >= 1 )
                                                    <li><i class="fa fa-star"></i></li>
                                                @elseif($review->rating - $x > 0)
                                                    <li><i class="fa fa-star-half-o"></i></li>
                                                @else
                                                    <li><i class="fa fa-star-o"></i></li>
                                                @endif
                                            @endfor
                                        </ul>
                                    </div>
                                    <div class="quote">
                                        <p>{{$review->review ?? ''}}</p>
                                    </div>
                                </div>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
