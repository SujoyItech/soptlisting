<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top: 100px!important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title text-center" id="exampleModalLabel">{{__('Give your ratings & review')}}</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{url('spotlist-directory-rating')}}" id="send_review" class="parsley-examples" >
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-8">{{__('Rating')}}</div>
                        <div class="col-4">
                            <div class="rating-stars">
                                @if(isset($user_ratings))
                                    <ul class="list-rating">
                                        @for($x = 0; $x< 5 ; $x++)
                                            @if(floor($user_ratings->rating) - $x >=1)
                                                <li data-value="{{$x+1}}" class="star selected"><i class="fa fa-star"></i></li>
                                            @elseif($user_ratings->rating -$x >0)
                                                <li data-value="{{$x+1}}" class="star selected"><i class="fa fa-star-half-o"></i></li>
                                            @else
                                                <li data-value="{{$x+1}}" class="star"><i class="fa fa-star-o"></i></li>
                                            @endif
                                        @endfor
                                    </ul>
                                @else
                                    <ul id="stars" class="list-rating">
                                        <li class="star" title="Poor" data-value="1">
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li class="star" title="Fair" data-value="2">
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li class="star" title="Good" data-value="3">
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li class="star" title="Excellent" data-value="4">
                                            <i class="fa fa-star"></i>
                                        </li>
                                        <li class="star" title="WOW!!!" data-value="5">
                                            <i class="fa fa-star"></i>
                                        </li>
                                    </ul>
                                @endif

                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                @if(isset($user_ratings->id) && !empty($user_ratings->id))
                                    <label for="message-text" class="col-form-label" id="message_title">{{__('Do you want to change your review ? (Review must be within 500 words)')}}</label>
                                @else
                                    <label for="message-text" class="col-form-label" id="message_title">{{__('Give your review (Review must be within 500 words) : ')}}</label>
                                @endif
                                <input type="hidden" name="spotlist_directory_id" value="{{$directory->spotlist_directory_id ?? ''}}">
                                <input type="hidden" name="rating" id="ratings" value="{{$user_ratings->rating ?? ''}}">
                                <input type="hidden" name="id" value="{{$user_ratings->id ?? ''}}" id="id">
                                <textarea class="form-control" id="message_text" name="review" maxlength="500" required>{{$user_ratings->review ?? ''}}</textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-star"></i> {{isset($user_ratings) ? __('Change review'): __('Give review')}}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function(){
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function(e){
            if (e < onStar) {
                $(this).addClass('hover');
            }
            else {
                $(this).removeClass('hover');
            }
        });

    }).on('mouseout', function(){
        $(this).parent().children('li.star').each(function(e){
            $(this).removeClass('hover');
        });
    });

    /* 2. Action to perform on click */
    $(document).on('click','#stars li', function(){
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');
        $(this).children().removeClass('fa-star-o')
        $(this).children().addClass('fa fa-star')
        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }

        // JUST RESPONSE (Not needed)
        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        var msg = "";
        if (ratingValue > 1) {
            msg = "Thanks! You rated this " + ratingValue + " stars.";
        }
        else {
            msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
        }

        $('#exampleModalLabel').html(msg);
        $('#ratings').val(ratingValue);

    });
</script>
