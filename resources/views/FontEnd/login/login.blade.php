<form method="post" action="{{ route('postLoginModal') }}" id="login" enctype="multipart/form-data" class="parsley-examples">
    <div class="col-12">
        <label>{{__('E-mail Address')}}</label>
        <input name="email" type="email" id="email" required class="form-control" placeholder="your@email.com">
        <span class="text-danger d-none email_error"></span>
    </div>
    <div class="col-12">
        <label>{{__('Password')}}</label>
        <input name="password" type="password" id="password" class="form-control" placeholder="Minimum 6 character">
        <span class="text-danger d-none password_error"></span>
    </div>
    <div class="col-12">
        <p>{{__('Forgot Password?')}} <a href="{{ route('forgetPassword') }}">{{__('Click Here')}}</a></p>
    </div>
    <div class="submit-btn col-12">
        <button type="submit" class="theme-btn login_btn">{{__('Sign in')}}</button>
    </div>
    @if(isset($settings->social_login_enable) && $settings->social_login_enable == TRUE)
        <div class="text-center">
            <p class="mt-1 text-muted">{{__('Sign in with')}}</p>
            <ul class="social-list list-inline mt-3 mb-0">
                <li class="list-inline-item">
                    <a href="{{url('login/facebook')}}" class="social-list-item border-primary text-primary"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="list-inline-item">
                    <a href="{{url('login/google')}}" class="social-list-item border-danger text-danger"><i class="fa fa-google"></i></a>
                </li>
            </ul>
        </div>
    @endif
    <div class="col-12 sign-b">
        <p>{{__('Need An Account?')}} <a href="javascript:void(0)" class="signup_btn">{{__('Sing Up Now')}}</a></p>
    </div>
</form>

<script>
    $(document).on('click','.signup_btn',function (e){
        $('#loginModalLabel').html('Register');
        $('.login').addClass('d-none');
        $('.register').removeClass('d-none');
    })
</script>
