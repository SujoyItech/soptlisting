@extends('layouts.app')
@section('content')
<div class="middle-box text-center animated fadeInDown">
    <h1>500</h1>
    <h2 class="font-bold">Wrong Server Request</h2>
    <div class="error-desc mt-5">
        Sorry, but the page you are looking for has not been found. Try checking the URL for error, then hit the refresh button on your browser or try found something else in our app.
    </div>
</div>

@endsection
