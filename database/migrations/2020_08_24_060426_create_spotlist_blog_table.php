<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_blogs', function (Blueprint $table) {
            $table->increments('spotlist_blogs_id');
            $table->bigInteger('sys_users_id')->nullable();
            $table->string('spotlist_blogs_title');
            $table->integer('spotlist_category_id')->nullable();
            $table->string('contents')->nullable();
            $table->string('blog_thumb_image')->nullable();
            $table->string('blog_cover_image')->nullable();
            $table->boolean('is_featured')->default(0);
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_blogs');
    }
}
