<?php


namespace App\Http\Controllers\Common\Profile;
use App\Traits\DataSaveTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileService {

    use DataSaveTrait;
    public function getProfileData(){
        $data['profile'] = DB::table('sys_users')->where('sys_users.id',Auth::user()->id)
            ->leftJoin('sys_users_additional_info','sys_users.id','sys_users_additional_info.sys_users_id')
            ->select('sys_users.*', 'sys_users_additional_info.website','sys_users_additional_info.facebook_link','sys_users_additional_info.twitter_link',
                'sys_users_additional_info.linkedin_link','sys_users_additional_info.instagram_link','sys_users_additional_info.about')
            ->first();

        return $data;
    }

    public function profileUpdate(Request $request){
        $user_data = $this->userInfo($request);
        $success_message = __('User profile data saved successfully.');
        $warning_message = __('User profile data saved failed.');
        $user_data_response = $this->updateTable('sys_users',$user_data,'id',$request->id,$success_message,$warning_message);
        $user_additional_info = $this->userAdditionalInfo($request);
        $user_additional_info_response = $this->insertOrUpdateTable('sys_users_additional_info','sys_users_id',$request->id,$user_additional_info);
        return $user_data_response;
    }

    private function userInfo(Request  $request){
        $post_data = [
            'name' => $request->name,
            'username' => $request->name,
            'mobile' => $request->mobile,
            'address' => $request->address
        ];
        if ($request->has('designation')){
            $post_data['designation'] = $request->designation;
        }

        if (isset($request->user_image) && file_exists($request->user_image)){
            $post_data['user_image'] = fileUpload($request->user_image,'Backend/images/users','',Auth::user()->user_image ?? '');
        }
        return $post_data;
    }

    private function userAdditionalInfo(Request $request){
        $post_data = [
            'sys_users_id' => $request->id,
            'website' => $request->website,
            'facebook_link' => $request->facebook_link,
            'instagram_link' => $request->instagram_link,
            'twitter_link' => $request->twitter_link,
            'linkedin_link' => $request->linkedin_link,
            'about' => $request->about
        ];

        return $post_data;

    }

    public function passwordUpdate(Request $request){

        if (Hash::check($request->old_password,Auth::user()->password)){

            $update_data = [
                'password' => bcrypt($request->password)
            ];
            $success_message = __('Password changed successfully.');
            $warning_message = __('Password change failed!');
            $response = $this->updateTable('sys_users',$update_data,'id',$request->id,$success_message,$warning_message);
            return $response;
        }else{
            $data['type'] = 'error';
            $response = $this->responseData(FALSE,__('Incorrect old password.'),$data);
            return $response;
        }

    }
}
