<?php


namespace App\Http\Controllers\FontEnd;


use App\Http\Controllers\FontEnd\Listing\ListingService;
use App\Http\Service\NotificationService;
use App\Models\User;
use App\Traits\DataReturn;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FontEndService {
    use DataReturn, DataSaveTrait;

    public function getBlog($slug,$paginate=9,$is_featured = NULL){
        $primary_key_value = [];
        $join_table = [
            ['left','spotlist_category','spotlist_blogs.spotlist_category_id','spotlist_category.spotlist_category_id']
        ];
        $select_table = ['spotlist_blogs.*','spotlist_category.spotlist_category_name'];
        $order_table = ['spotlist_blogs.created_at','desc'];
        $where_table = ['spotlist_blogs.status'=>'Active'];
        if ($is_featured != NULL){
            $where_table['is_featured'] = $is_featured;
        }
        if ($slug != NULL){
            $primary_key_value = ['slug',$slug];
        }
        $blogs = DB::table('spotlist_blogs')->where('status','Active')->get();
        $data = [];
        if (isset($blogs) && !empty($blogs[0])){
            $data['blogs'] = $this->joinTableInfo('spotlist_blogs',$join_table,$select_table,$primary_key_value,[],$where_table,$order_table,$paginate);
        }

        if ($slug != NULL){
            $blog = DB::table('spotlist_blogs')->where('slug',$slug)->first();
            $where_not_in[0] = 'spotlist_blogs.spotlist_blogs_id';
            $where_not_in[1] = [0=>$blog->spotlist_blogs_id];
            $data['blog_lists'] = $this->joinTableInfo('spotlist_blogs',$join_table,$select_table,[],[],$where_table,$order_table,12,NULL,NULL,$where_not_in);
        }
        return $data;
    }

    public function getPackages(){
        $data['packages'] = $this->basicTableInfo('spotlist_packages',[],[],[],['status'=>'Active']);
        return $data;
    }

    public function getProfile($id=NULL){
        $join_table = [
            ['left','sys_users_additional_info','sys_users.id','sys_users_additional_info.sys_users_id']
        ];
        $select_column = ['sys_users.*','sys_users_additional_info.website','sys_users_additional_info.website',
                          'sys_users_additional_info.facebook_link','sys_users_additional_info.twitter_link',
                          'sys_users_additional_info.linkedin_link','sys_users_additional_info.instagram_link',
                          'sys_users_additional_info.about'];

        $user_id = $id == NULL ? Auth::user()->id : $id;
        $data['user'] = $this->joinTableInfo('sys_users',$join_table,$select_column,['sys_users.id',$user_id]);
        $data['ratings'] = DB::table('sys_users')
                 ->select(DB::raw("AVG(spotlist_directory_reviews.rating) AS rating"))
                 ->leftJoin('spotlist_directory','sys_users.id','=','spotlist_directory.sys_users_id')
                 ->leftJoin('spotlist_directory_reviews','spotlist_directory.spotlist_directory_id','=','spotlist_directory_reviews.spotlist_directory_id')
                 ->where('sys_users.id',$user_id)
                 ->groupBy('sys_users.id')
                ->first();

        if ($id !== NULL){
            $listing_service = new ListingService();
            $data['listing'] = $listing_service->getDirectoryList(NULL,$id,4);
        }
        return $data;
    }

    public function followUser(Request $request){
        $post_data = $request->all();
        $post_data['user_id'] = Auth::user()->id;
        $response = $this->insertOrUpdateTableOnMultipleCondition('spotlist_user_follower',['user_id'=>$post_data['user_id'],'follower_id'=>$post_data['follower_id']],$post_data);
        return $response;
    }

    public function userEmailSubscription(Request $request){
        return $this->insertTable('spotlist_user_subscription_by_email',['email'=>$request->email,'created_at'=>Carbon::now()],'Subscription successful.','Subscription failed!');
    }

    public function userMessageSend(Request $request){
        $post_data = $request->except('_token');
        $post_data['created_at'] = Carbon::now();
        $post_data['updated_at'] = Carbon::now();
        $response = $this->insertTable('spotlist_user_message_table',$post_data,'Message sent successfully.','Message sent failed!');
        $this->sendUserMessageNotification($post_data);
        return $response;
    }

    private function sendUserMessageNotification($post_data){
        $notify_body = [
            'username' => $post_data['fname'].' '.$post_data['lname'],
            'title' => $post_data['subject'],
            'body' => __('New message from spotlisting user'),
            'message' => $post_data['message']
        ];
        $user_id = User::where('default_module_id',ADMIN)->pluck('id')->toArray();
        $notify_to = [
            'user_id' => $user_id,
            'type' => GENERAL_MESSAGE_NOTIFICATION
        ];
        $service = new NotificationService();
        $service->sendMessage($notify_body,$notify_to);
    }



}
