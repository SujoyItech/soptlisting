@extends('FontEnd.master')
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Pricing')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->

    <!-- pricing-area start -->

    <div class="pricing-area section-padding">
        <div class="container">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2>{{__('The Best Pricing We Offered')}}</h2>
                </div>
            </div>
            <div class="row">
                @if(isset($pricing) && !empty($pricing))
                    @foreach($pricing as $price)
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="pricing-item">
                            <div class="price-top">
                                <h3>Basic</h3>
                            </div>
                            <div class="price-bottom">
                                <h2>$29</h2>
                                <span>(Per Month)</span>
                                <ul>
                                    <li>One Listing</li>
                                    <li>90 Days Availability</li>
                                    <li>Non-Featured</li>
                                    <li>Limited Support</li>
                                    <li>4 Photos Per Listing</li>
                                    <li>7 Tags Per Listing</li>
                                    <li>Featured Listings Unallowed</li>
                                </ul>
                            </div>
                            <a class="theme-btn p-btn" href="#">Choose Plan</a>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <!-- pricing-area end -->

    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')

@endsection
