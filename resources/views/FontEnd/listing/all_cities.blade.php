@extends('FontEnd.master')
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Cities')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- .articles-area-start -->
    <div class="articles-area blog-area section-padding">
        @if(isset($cities) && !empty($cities))
        <div class="container">
            <div class="section-title text-center">
                <h2>{{__('All cities')}}</h2>
            </div>
            <div class="row">
                    @foreach($cities as $city)
                        <div class="col-lg-4 col-md-4 col-12">
                            <div class="product-item">
                                <a href="{{route('show-list-by-place',['slug'=>$city->spotlist_cities_slug])}}">
                                    <div class="popular-item">
                                        <div class="popular-img">
                                            <img src="{{isset($city->city_image) ? asset($city->city_image) :  ''}}" alt="" height="300">
                                        </div>
                                        <div class="popular-content">
                                            <h5><a href="{{route('show-list-by-place',['slug'=>$city->spotlist_cities_slug])}}">{{$city->spotlist_cities_name}}</a></h5>
                                        </div>
                                        <div class="thumb">
                                            <span>{{$city->total_listing ?? 0 }} {{__('Listing')}}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
        @else
            <div class="text-center no-data-image">
                <img src="{{asset('FontEnd/assets/images/error/nodata.svg')}}">
            </div>

        @endif
    </div>
    <!-- .articles-area-start -->
    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')
@endsection
