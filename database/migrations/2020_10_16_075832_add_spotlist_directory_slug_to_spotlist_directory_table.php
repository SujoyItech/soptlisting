<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSpotlistDirectorySlugToSpotlistDirectoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spotlist_directory', function (Blueprint $table) {
            $table->string('spotlist_directory_slug')->after('spotlist_directory_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spotlist_directory', function (Blueprint $table) {
            $table->dropColumn('spotlist_directory_slug');
        });
    }
}
