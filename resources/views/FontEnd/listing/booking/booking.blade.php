@if(isset($directory->spotlist_parent_category_id) && (in_array($directory->spotlist_parent_category_id,[CATEGORY_HOTEL,CATEGORY_RESTAURANT,CATEGORY_BEAUTY,CATEGORY_FITNESS])))
    <a class="booking-btn" data-user-id="{{\Illuminate\Support\Facades\Auth::user()->id ?? ''}}" href="javascript:void(0)">
        <i class="fa fa-star"></i> {{__('Booking Now')}}
    </a>
@endif
<div class="booking d-none">
    @if($directory->spotlist_parent_category_id == CATEGORY_HOTEL)
        @include('FontEnd.listing.booking.hotel_booking')
    @elseif($directory->spotlist_parent_category_id == CATEGORY_RESTAURANT)
        @include('FontEnd.listing.booking.restaurant_booking')
    @elseif($directory->spotlist_parent_category_id == CATEGORY_BEAUTY)
        @include('FontEnd.listing.booking.beauty_booking')
    @elseif($directory->spotlist_parent_category_id == CATEGORY_FITNESS)
        @include('FontEnd.listing.booking.fitness_booking')
    @endif
</div>

<script>
    $(document).on('click','.booking-btn',function (){
        if ($(this).data('user-id').length !== 0){
            $('.booking').removeClass('d-none');
            $(this).addClass('d-none');
        }else {
            $('#loginModal').modal({
                backdrop: 'static',
                keyboard: false
            })
        }

    })
    $('.booking_now').on('click', function (e) {
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);
                makeAjaxPostFile(formData, submit_url, null).done(function (response) {
                    if (response.success == true){
                        var redirect_url = "{{Request::url()}}";
                        swalRedirect(redirect_url, response.message, 'success');
                    }else {
                        swalError(response.message)
                    }
                });
            }
        });
    });
</script>
