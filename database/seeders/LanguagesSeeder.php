<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LanguagesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('languages')->delete();
        
        \DB::table('languages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => NULL,
                'language' => 'en',
                'created_at' => '2020-10-12 12:40:03',
                'updated_at' => '2020-10-12 12:40:03',
            ),
        ));

        
    }
}