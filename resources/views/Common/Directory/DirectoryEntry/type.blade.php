<link href="{{asset('Backend/multiselect/tagsinput.css')}}" rel="stylesheet" type="text/css">
<script src="{{asset('Backend/multiselect/tagsinput.js')}}"></script>
<div class="tab-pane" id="type">
    <form method="post" action="{{url('directory-type-save')}}" enctype="multipart/form-data" id="directory-type-save">
        <div class="row">
            @if(isset($directory->spotlist_parent_category_id) && !empty($directory->spotlist_parent_category_id))
                <input type="hidden" value="{{$directory->spotlist_parent_category_id}}" name="parent_category_id">
            <div class="col-12 mb-2 text-center">
                @if(isset($main_categories) && !empty($main_categories))
                    @foreach($main_categories as $main_category)
                        <div class="radio radio-info form-check-inline">
                            <input type="radio" id="{{$main_category->spotlist_category_id}}" value="{{$main_category->spotlist_category_id}}" name="parent_category_id"
                                {{isset($directory->spotlist_parent_category_id) && ($directory->spotlist_parent_category_id == $main_category->spotlist_category_id) ? 'checked':'' }} disabled>
                            <label for="{{$main_category->spotlist_category_id}}"> {{$main_category->spotlist_category_name}}</label>
                        </div>
                    @endforeach
                @endif
            </div>
            @else
                <div class="col-12 mb-2 text-center">
                    @if(isset($main_categories) && !empty($main_categories))
                        @foreach($main_categories as $main_category)
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" id="{{$main_category->spotlist_category_id}}" value="{{$main_category->spotlist_category_id}}" name="parent_category_id">
                                <label for="{{$main_category->spotlist_category_id}}"> {{$main_category->spotlist_category_name}}</label>
                            </div>
                        @endforeach
                    @endif
                </div>
            @endif
        </div>
        <div class="col-md-12 hotel_type {{isset($directory->spotlist_parent_category_id) && $directory->spotlist_parent_category_id == CATEGORY_HOTEL ? '': 'd-none' }}">
            @include('Common.Directory.DirectoryEntry.Type.hotel_type')
        </div>
        <div class="col-md-12 restaurant_type {{isset($directory->spotlist_parent_category_id) && $directory->spotlist_parent_category_id == CATEGORY_RESTAURANT ? '': 'd-none' }}">
            @include('Common.Directory.DirectoryEntry.Type.restaurant_type')
        </div>
        <div class="col-md-12 shop_type {{isset($directory->spotlist_parent_category_id) && $directory->spotlist_parent_category_id == CATEGORY_SHOPPING ? '': 'd-none' }}">
            @include('Common.Directory.DirectoryEntry.Type.shop_type')
        </div>
        <div class="col-md-12 beauty_type {{isset($directory->spotlist_parent_category_id) && $directory->spotlist_parent_category_id == CATEGORY_BEAUTY ? '': 'd-none' }}">
            @include('Common.Directory.DirectoryEntry.Type.beauty_type')
        </div>
        <div class="col-md-12 fitness_type {{isset($directory->spotlist_parent_category_id) && $directory->spotlist_parent_category_id == CATEGORY_FITNESS ? '': 'd-none' }}">
            @include('Common.Directory.DirectoryEntry.Type.fitness_type')
        </div>
        <ul class="list-inline mb-0 wizard">
            <li class="previous list-inline-item">
                <a href="javascript: void(0);" class="btn btn-sm btn-secondary">{{__('Previous')}}</a>
            </li>
            <li class="next list-inline-item float-right">
                <button type="submit" class="btn btn-sm btn-success submit_info btn-next">{{__('Save & Next')}}</button>
            </li>
        </ul>
    </form>

</div>

<script>
    $("input[name='parent_category_id']").change(function (){
        var value = $(this).val();
        if (value === "{{CATEGORY_HOTEL}}"){
            $('.hotel_type').removeClass('d-none');
            $('.restaurant_type').addClass('d-none');
            $('.shop_type').addClass('d-none');
            $('.beauty_type').addClass('d-none');
            $('.fitness_type').addClass('d-none');
        }else if (value === "{{CATEGORY_RESTAURANT}}"){
            $('.restaurant_type').removeClass('d-none');
            $('.hotel_type').addClass('d-none');
            $('.shop_type').addClass('d-none');
            $('.beauty_type').addClass('d-none');
            $('.fitness_type').addClass('d-none');
        }else if (value === "{{CATEGORY_SHOPPING}}"){
            $('.shop_type').removeClass('d-none');
            $('.restaurant_type').addClass('d-none');
            $('.hotel_type').addClass('d-none');
            $('.beauty_type').addClass('d-none');
            $('.fitness_type').addClass('d-none');
        }else if (value === "{{CATEGORY_BEAUTY}}"){
            $('.beauty_type').removeClass('d-none');
            $('.shop_type').addClass('d-none');
            $('.restaurant_type').addClass('d-none');
            $('.hotel_type').addClass('d-none');
            $('.fitness_type').addClass('d-none');
        }else if (value === "{{CATEGORY_FITNESS}}"){
            $('.fitness_type').removeClass('d-none');
            $('.shop_type').addClass('d-none');
            $('.restaurant_type').addClass('d-none');
            $('.hotel_type').addClass('d-none');
            $('.beauty_type').addClass('d-none');
        }else {
            $('.beauty_type').addClass('d-none');
            $('.shop_type').addClass('d-none');
            $('.restaurant_type').addClass('d-none');
            $('.hotel_type').addClass('d-none');
            $('.fitness_type').addClass('d-none');
        }
    });

    $('.remove_info').on('click', function (e) {
        e.preventDefault();
        Ladda.bind(this);
        var obj = $(this);
        var load = $(this).ladda();
        var id = $(this).data('id');
        var table_name = $(this).data('table');
        var primary_key = $(this).data('primary-key');
        var url = "{{url('remove-directory-file')}}";
        swalConfirm('Are You sure to delete this file?').then(function (s) {
            if(s.value){
                var data = {
                    value: id,
                    table_name: table_name,
                    primary_key: primary_key
                };
                makeAjaxPost(data, url, load).done(function(sresult){
                    $(obj).closest('.heading').remove();
                    swalRedirect("{{url(Request::url())}}",'Deleted Successfully');
                });
            }
        });
    });

</script>
