@extends('FontEnd.master')
@section('style')

@endsection
@section('head','v2')
@section('content')
    <!-- listing-area start -->
    <section class="error-404-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-xs-12">
                    <div class="content clearfix">
                        <div class="error">
                            <img src="{{asset('FontEnd/assets/images/error/403.svg')}}" alt="">
                        </div>
                        <div class="error-message">
                            <h3>Oops! Permission Denied!</h3>
                            <p>We’re sorry but we can’t seem to find the page you requested. This might be because you have typed the web address incorrectly.</p>
                            <a href="index.html" class="btn theme-btn">Back to home</a>
                        </div>
                    </div>
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </section>
    <!-- listing-area start -->
@endsection

