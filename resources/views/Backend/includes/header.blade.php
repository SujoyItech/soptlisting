<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">
            {{-------------- Language ----------------}}
            @if(isset($settings->app_lang) && $settings->app_lang == 'On')
                @if(isset($settings->admin_lang) && $settings->admin_lang == 'On')
                    <li class="dropdown d-none d-lg-inline-block topbar-dropdown">
                        <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <img src="{{asset('Backend/images/flags/us.jpg')}}" alt="user-image" height="16">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="javascript:void(0);" class="dropdown-item">
                                <img src="{{asset('Backend/images/flags/germany.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">German</span>
                            </a>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <img src="{{asset('Backend/images/flags/italy.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Italian</span>
                            </a>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <img src="{{asset('Backend/images/flags/spain.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Spanish</span>
                            </a>
                            <a href="javascript:void(0);" class="dropdown-item">
                                <img src="{{asset('Backend/images/flags/russia.jpg')}}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Russian</span>
                            </a>

                        </div>
                    </li>
                @endif
            @endif
            {{-------------- Notification ----------------}}
            <li class="dropdown notification-list topbar-dropdown">
                @include('Backend.includes.message')
            </li>

            <li class="dropdown notification-list topbar-dropdown">
                @include('Backend.includes.notification')
            </li>

            {{-------------- Profile ----------------}}

            <li class="dropdown notification-list topbar-dropdown">
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{getImageUrl(Auth::user()->user_image ?? '')}}" alt="user-image" class="rounded-circle">
                    <span class="pro-user-name ml-1">{{Auth::user()->name ?? ''}}<i class="mdi mdi-chevron-down"></i></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                    <a href="{{url('user/home')}}" class="dropdown-item notify-item">
                        <i class="fa fa-globe"></i> <span>{{__('Visit website')}}</span>
                    </a>
                    <a href="{{url('profile-settings')}}" class="dropdown-item notify-item">
                        <i class="fe-user"></i> <span>{{__('My Account')}}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{url('logout')}}" class="dropdown-item text-danger">
                        <i class="fe-log-out"></i> <span>{{__('Logout')}}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- LOGO -->
        <div class="logo-box">
            <a href="{{url(\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER ? 'listing-manager-dashboard' : 'admin/home')}}" class="logo logo-light text-center">
                <span class="logo-sm"><img src="{{getImageUrl($settings->app_logo_small)}}" alt="" height="22"></span>
                <span class="logo-lg"><img src="{{getImageUrl($settings->app_logo_large)}}" alt="" height="22"></span>
            </a>
        </div>
        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                    <i class="fe-menu"></i>
                </button>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
