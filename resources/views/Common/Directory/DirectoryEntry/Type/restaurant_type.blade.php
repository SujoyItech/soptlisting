@if(isset($restaurant_data) && !empty($restaurant_data[0]))
    @foreach($restaurant_data as $restaurant)
        <div class="row mb-5">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Menu name')}}</label>
                            <input type="text" name="menu_name[]" value="{{$restaurant->menu_name ?? ''}}"
                                   placeholder="Enter menu name" class="form-control">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Menu price')}} ( {{get_currency()}} )</label>
                            <input type="number" class="form-control" name="menu_price[]" value="{{$restaurant->menu_price ?? ''}}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Items (Press enter after entering every variant)')}}</label>
                            <input type="text" value="{{$restaurant->ingredients_item ?? ''}}" data-role="tagsinput" name="ingredients_item[]">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('Description(Max length 500 word)')}}</label>
                            <textarea name="menu_description[]" class="form-control" maxlength="500">{{$restaurant->menu_description ?? ''}}</textarea>
                        </div>
                    </div>

                    <div class="col-12">
                        <input type="hidden" name="spotlist_directory_type_restaurant_id[]" value="{{$restaurant->spotlist_directory_type_restaurant_id ?? ''}}">
                        <button class="btn btn-danger remove_info" type="button"
                                data-id="{{$restaurant->spotlist_directory_type_restaurant_id ?? ''}}" data-table="spotlist_directory_type_restaurant" data-primary-key="spotlist_directory_type_restaurant_id">
                            <i class="fa fa-minus"></i> {{__('Remove')}}</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label class="col-form-label" for="picture">{{__('Menu image')}}</label>
                    <input type="file" name="menu_image[]" data-plugins="dropify" class="dropify"
                           data-default-file="{{isset($restaurant->menu_image) ? asset($restaurant->menu_image) : ''}}"
                           data-allowed-file-extensions="png jpg jpeg jfif"
                           data-max-file-size="2M" />
                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                </div>
            </div>
        </div>
        <hr>
    @endforeach
@endif
<div class="restaurant-list">
    <div class="row mb-5">
        <div class="col-lg-9">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Menu name')}}</label>
                        <input type="text" name="menu_name[]" value=""
                               placeholder="Enter menu name" class="form-control">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Menu price')}} ( {{get_currency()}} )</label>
                        <input type="number" class="form-control" name="menu_price[]">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Items (Press enter after entering every variant)')}}</label>
                        <input type="text" value="" data-role="tagsinput" name="ingredients_item[]">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label">{{__('Description ( Max length 500 word )')}}</label>
                        <textarea name="menu_description[]" class="form-control" maxlength="500"></textarea>
                    </div>
                </div>

                <div class="col-12">
                    <button class="btn btn-info add_restaurant" type="button"><i class="fa fa-plus"></i> {{__('Add more')}}</button>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="col-form-label" for="picture">{{__('Menu image')}}</label>
                <input type="file" name="menu_image[]" data-plugins="dropify" class="dropify"
                       data-default-file=""
                       data-allowed-file-extensions="png jpg jpeg jfif"
                       data-max-file-size="2M" />
                <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
            </div>
        </div>
    </div>
</div>


<script>

    $('.add_restaurant').on('click',function (){

        var restaurant_html = '<div class="row restaurant_heading mb-5">\n' +
            '    <div class="col-lg-9">\n' +
            '        <div class="row">\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Menu name')}}</label>\n' +
            '                    <input type="text" name="menu_name[]" parsley-trigger="change" required value=""\n' +
            '                           placeholder="Enter menu name" class="form-control">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Menu price')}} ({{get_currency()}})</label>\n' +
            '                    <input type="number" class="form-control" name="menu_price[]">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Items (Press enter after entering every variant)')}}</label>\n' +
            '                    <input type="text" value="" data-role="tagsinput" name="ingredients_item[]">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '           <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label">{{__('Description(Max length 500 word)')}}</label>\n' +
            '                    <textarea name="menu_description[]" class="form-control" maxlength="500"></textarea>\n' +
            '                </div>\n' +
            '            </div>'+
            '\n' +
            '            <div class="col-12">\n' +
            '                <button class="btn btn-danger remove_restaurant" type="button"><i class="fa fa-minus"></i> {{__('Remove')}}</button>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '    <div class="col-lg-3">\n' +
            '        <div class="form-group">\n' +
            '            <label class="col-form-label" for="picture">{{__('Menu image')}}</label>\n' +
            '            <input type="file" name="menu_image[]" class="dropify" data-plugins="dropify"\n' +
            '                   data-default-file=""\n' +
            '                   data-allowed-file-extensions="png jpg jpeg jfif"\n' +
            '                   data-max-file-size="2M" />\n' +
            '            <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';

        $('.restaurant-list').append(restaurant_html);
        $('input[data-role=tagsinput]').tagsinput();
        $('.dropify').dropify();
    });

    $(document).on('click','.remove_restaurant',function (e) {
        e.preventDefault();
        $(this).closest('.restaurant_heading').remove();
    })
</script>

