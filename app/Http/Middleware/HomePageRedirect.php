<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomePageRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (isset(Auth::user()->default_module_id)){
            if (Auth::user()->default_module_id == ADMIN || Auth::user()->default_module_id == SYSTEM_ADMIN){
                return redirect()->route('admin.home');
            }else if (Auth::user()->default_module_id == LISTING_MANAGER){
                return redirect()->route('listing-manager-dashboard');
            }
        }
        return $next($request);
    }
}
