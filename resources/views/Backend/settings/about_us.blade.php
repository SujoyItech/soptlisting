@extends('Backend.layouts.app',['menu'=>'settings','sub_menu'=>'about_us'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-user-friends"></i> {{__('About us')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('about-us-save')}}" class="parsley-examples" method="POST" id="submit_about_us" enctype="multipart/form-data">
            <div class="works_title">
                <div class="card-box mb-3">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="about_us_title">{{__('Title')}} <span class="text-danger">*</span></label>
                                        <input type="text" name="about_us_title"  value="{{$about_us->about_us_title ?? ''}}" parsley-trigger="change" required  placeholder="Title" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="about_us_description">{{__('Description')}}</label>
                                        <textarea name="about_us_description" placeholder="Description here" class="form-control" rows="3">{{$about_us->about_us_description ?? ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="name">{{__('Picture')}}</label>
                                <input type="file" name="about_us_picture" data-plugins="dropify" class="dropify"
                                       data-default-file="{{isset($about_us->about_us_picture) ? asset($about_us->about_us_picture) : ''}}"
                                       data-allowed-file-extensions="png jpg jpeg jfif"
                                       data-max-file-size="2M" />
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <div class="form-group">
                                <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                                    <i class="fa fa-save"></i>
                                    {{__('Save')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>

        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            swalSuccess(response.message)
                        }else{
                            swalError(response.message)
                        }
                    });
                }
            });
        });
    </script>
@endsection

