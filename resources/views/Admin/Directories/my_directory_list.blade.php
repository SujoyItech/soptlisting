@extends('Backend.layouts.app',['menu'=>'my_directory','sub_menu'=>'my_directory_lis'])

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('My Directory List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table dt-responsive nowrap" style="width: 100%">
                            <thead>
                            <tr>
                                <th>{{__('Image')}}</th>
                                <th>{{__('Title')}}</th>
                                <th>{{__('Type')}}</th>
                                <th>{{__('Categories')}}</th>
                                <th>{{__('Location')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Action')}}</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if(isset($directory_list) && !empty($directory_list))
                                @foreach($directory_list as $directory)
                                    <tr>
                                        <td>
                                            <img src="{{asset($directory['thumb_image'])}}" class="rounded-circle" width="50" height="50">
                                        </td>
                                        <td>
                                            @if($directory['status'] == 'Active')
                                                <strong><a href="{{route('user.listing',['slug'=>$directory['spotlist_directory_slug']])}}">{{$directory['spotlist_directory_name']}}</a></strong><br>
                                            @else
                                                <strong>{{$directory['spotlist_directory_name']}}</strong><br>
                                            @endif
                                            <small>{{$directory['name']}}<br>{{\Carbon\Carbon::parse($directory['created_at'])->format('Y-m-d h:iA')}} </small>
                                        </td>
                                        <td>
                                            {{$directory['type']}}
                                        </td>
                                        <td>
                                            @if(isset($directory['category_name']) && !empty($directory['category_name'][0]))
                                                @foreach($directory['category_name'] as $category)
                                                    <span class="badge badge-secondary">{{$category}}</span><br>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{$directory['city_name'].', '.$directory['country_name']}}</td>
                                        <td>
                                            @if($directory['status'] == 'Incomplete')
                                                <span class="badge badge-warning">{{__('Incomplete')}} <i class="fa fa-exclamation-circle"></i></span><br>
                                            @elseif($directory['status'] == 'Inactive')
                                                <span class="badge badge-warning">{{__('Pending')}}</span><br>
                                            @elseif($directory['status'] == 'Active')
                                                <span class="badge badge-success">{{__('Active')}}</span><br>
                                                @if($directory['is_featured'] == TRUE)
                                                    <span class="badge badge-success">{{__('Featured')}}</span><br>
                                                @endif
                                            @else
                                                <span class="badge badge-danger">{{__('Declined')}} <i class="fa fa-ban"></i></span><br>
                                            @endif
                                            @if($directory['is_verified'] == TRUE)
                                                <span class="badge badge-success">{{__('Verified')}}</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-rounded btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {{__('Action')}}
                                                </button>
                                                <div class="dropdown-menu">
                                                    @if($directory['status'] == 'Incomplete')
                                                        <a class="dropdown-item" href="{{url('directory-add/'.$directory['spotlist_directory_id'])}}">{{__('Make it complete')}}</a>
                                                    @else
                                                    <a class="dropdown-item" href="#">{{__('View in website')}}</a>
                                                    <a class="dropdown-item" href="{{url('directory-add/'.$directory['spotlist_directory_id'])}}">{{__('Edit')}}</a>

                                                    @if($directory['is_verified'] == TRUE)
                                                        @if($directory['status'] == 'Active')
                                                             <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="pending" data-text="">{{__('Make as pending')}}</button>
                                                             <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="declined">{{__('Declined')}}</button>
                                                        @elseif($directory['status'] == 'Inactive')
                                                            <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="active">{{__('Make as active')}}</button>
                                                            <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="declined">{{__('Declined')}}</button>
                                                        @else
                                                            <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="active">{{__('Make as active')}}</button>
                                                            <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="pending">{{__('Make as pending')}}</button>
                                                        @endif
                                                        @if($directory['is_featured'] == TRUE )
                                                            <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="remove_feature">{{__('Remove from feature')}}</button>
                                                        @else
                                                            <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="add_feature">{{__('Add to feature')}}</button>
                                                        @endif

                                                    @else
                                                        <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="verified">{{__('Verify')}}</button>
                                                    @endif
                                                    @endif
                                                    <div class="dropdown-divider"></div>
                                                    <button class="dropdown-item directory_status" data-id="{{$directory['spotlist_directory_id']}}" data-type="delete">{{__('Delete')}}</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.jqueryui.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>
    <script>
        $('#example').dataTable( {
            "order": []
        });

        $(document).on('click','.directory_status', function (e) {
            e.preventDefault();
            Ladda.bind(this);
            var obj = $(this);
            var load = $(this).ladda();
            var id = $(this).data('id');
            var type = $(this).data('type');
            var confirm_text = '';
            if (type == 'delete'){
                confirm_text = 'Are You sure to delete this directory?';
            }else if (type == 'remove_feature'){
                confirm_text = 'Are you sure to remove feature to this directory?';
            }else if (type == 'add_feature'){
                confirm_text = 'Are you sure to add feature to this directory?';
            }
            else{
                confirm_text = 'Are You sure to make '+type+' this directory?';
            }

            var url = "{{url('directory-status-change')}}";
            swalConfirm(confirm_text).then(function (s) {
                if(s.value){
                    var data = {
                        value: id,
                        type: type,
                        table_name: 'spotlist_directory',
                        primary_key: 'spotlist_directory_id'
                    };
                    makeAjaxPost(data, url, load).done(function(response){
                        if (response.success == true){
                            swalRedirect("{{Request::url()}}",response.message,'success')
                        }else{
                            swalRedirect("{{Request::url()}}",response.message,'error')
                        }
                    });
                }
            });
        });

    </script>

@endsection
