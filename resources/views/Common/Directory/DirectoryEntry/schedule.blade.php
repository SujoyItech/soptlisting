<div class="tab-pane" id="schedule">
    <form action="{{url('directory-schedule-save')}}" class="parsley-examples" method="POST" id="submit_directory_schedule">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Monday opening')}}</label>
                    {!! Form::select('monday_from',  _timeSelectOption(), $directory_schedule['monday_from'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Monday closed')}}</label>
                    {!! Form::select('monday_to',  _timeSelectOption(), $directory_schedule['monday_to'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Tuesday opening')}}</label>
                    {!! Form::select('tuesday_from',  _timeSelectOption(), $directory_schedule['tuesday_from'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Tuesday closed')}}</label>
                    {!! Form::select('tuesday_to',  _timeSelectOption(), $directory_schedule['tuesday_to'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Wednesday opening')}}</label>
                    {!! Form::select('wednesday_from',  _timeSelectOption(), $directory_schedule['wednesday_from'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Wednesday closed')}}</label>
                    {!! Form::select('wednesday_to',  _timeSelectOption(), $directory_schedule['wednesday_to'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Thursday opening')}}</label>
                    {!! Form::select('thursday_from',  _timeSelectOption(), $directory_schedule['thursday_from'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Thursday closed')}}</label>
                    {!! Form::select('thursday_to',  _timeSelectOption(), $directory_schedule['thursday_to'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Friday opening')}}</label>
                    {!! Form::select('friday_from',  _timeSelectOption(), $directory_schedule['friday_from'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Friday closed')}}</label>
                    {!! Form::select('friday_to',  _timeSelectOption(), $directory_schedule['friday_to'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Saturday opening')}}</label>
                    {!! Form::select('saturday_from',  _timeSelectOption(), $directory_schedule['saturday_from'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Saturday closed')}}</label>
                    {!! Form::select('saturday_to',  _timeSelectOption(), $directory_schedule['saturday_to'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Sunday opening')}}</label>
                    {!! Form::select('sunday_from',  _timeSelectOption(), $directory_schedule['sunday_from'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group mb-3">
                    <label>{{__('Sunday closed')}}</label>
                    {!! Form::select('sunday_to',  _timeSelectOption(), $directory_schedule['sunday_to'] ?? '', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <ul class="list-inline mb-0 wizard">
            <li class="previous list-inline-item">
                <a href="javascript: void(0);" class="btn btn-sm btn-secondary">{{__('Previous')}}</a>
            </li>
            <li class="next list-inline-item float-right">
                <button type="submit" class="btn btn-sm btn-success directory_schedule_submit btn-next">{{__('Save & Next')}}</button>
            </li>
        </ul>
    </form>
    <input type="hidden" name="spotlist_directory_id" value="{{$schedule->spotlist_directory_schedule_id ?? ''}}" id="spotlist_directory_schedule_id">

</div>
<script>
    $('.directory_schedule_submit').on('click', function (e) {
        Ladda.bind(this);
        var load = $(this).ladda();
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);

                if ($('#spotlist_directory_id').val().length !== 0){
                    formData.append('spotlist_directory_id',$('#spotlist_directory_id').val());
                }
                if ($('#spotlist_directory_schedule_id').val().length !== 0){
                    formData.append('spotlist_directory_schedule_id',$('#spotlist_directory_schedule_id').val());
                }
                makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                    load.ladda('stop');
                });
            }
        });
    });
</script>
