<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectoryTypeHotelRoomSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory_type_hotel_room')->delete();
        
        \DB::table('spotlist_directory_type_hotel_room')->insert(array (
            0 => 
            array (
                'spotlist_directory_type_hotel_room_id' => 1,
                'spotlist_directory_id' => 2,
                'room_name' => 'Single bed Ac',
                'room_price' => '100.00',
                'room_description' => 'Some text will be written here',
                'room_image' => 'Backend/images/directory/hotel/Zs1b8YteP6d3t6vW.jpeg',
                'room_facilities' => 'Tools1,Tools2',
                'status' => 'Active',
            ),
            1 => 
            array (
                'spotlist_directory_type_hotel_room_id' => 2,
                'spotlist_directory_id' => 2,
                'room_name' => 'Super delux',
                'room_price' => '200.00',
                'room_description' => 'some text will be written',
                'room_image' => 'Backend/images/directory/hotel/oMmrP6EDQyylQiBC.jpg',
                'room_facilities' => 'Free Meal,Bufe',
                'status' => 'Active',
            ),
        ));

        
    }
}