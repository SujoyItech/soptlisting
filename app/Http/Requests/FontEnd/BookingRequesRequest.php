<?php

namespace App\Http\Requests\FontEnd;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

class BookingRequesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules=[];
        if (!empty($this->reservation_contact)){
            $rules['reservation_contact'] = 'required|numeric|phone';
        }if (!empty($this->spotlist_directory_type_beauty_id)){
            $rules['spotlist_directory_type_beauty_id'] = 'required';
        }if (!empty($this->reservation_start_time)){
            $rules['reservation_start_time'] = 'required';
        }if (!empty($this->request_time)){
            $rules['request_time'] = 'required';
        }
        return $rules;
    }

    public function messages()
    {
        $messages = [
            'reservation_contact.required' => __('Contact field can not be empty!'),
            'reservation_contact.numeric' => __('Invalid contact number!'),
            'reservation_contact.phone' => __('Invalid contact number!'),
            'spotlist_directory_type_beauty_id.required' => __('Please select a service!'),
            'reservation_start_time.required' => __('Reservation date can not be empty!'),
            'request_time.required' => __('Reservation time can not be empty!')
        ];
        return $messages;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->header('accept') == "application/json") {
            $errors = [];
            if ($validator->fails()) {
                $e = $validator->errors()->all();
                foreach ($e as $error) {
                    $errors[] = $error;
                }
            }
            $json = ['success'=>false,
                'data'=>[],
                'message' => $errors[0],
            ];
            $response = new JsonResponse($json, 200);

            throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
        } else {
            throw (new ValidationException($validator))
                ->errorBag($this->errorBag)
                ->redirectTo($this->getRedirectUrl());
        }

    }
}
