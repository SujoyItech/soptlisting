<div class="col-xl-4 col-lg-6 col-md-6 col-12">
    <div class="product-item f-pro-item">
        <a href="{{route('user.listing').'/'.$data->spotlist_directory_slug}}">
            <div class="product-img">
                <img src="{{getImage($data->thumb_image,'directory')}}" alt="" height="300">

            </div>
        </a>
        <div class="product-content">
            <span><a href="{{route('user.profile',['id'=>$data->user_id])}}"><small>{{$data->user_name}}</small></a> | <small>{{ Carbon\Carbon::parse($data->created_at)->format('d F Y') }}</small></span>
            <h4><a href="{{route('user.listing').'/'.$data->spotlist_directory_slug}}">{{$data->spotlist_directory_name}}</a></h4>
            <span>{{isset($data->city_name) ? $data->city_name.' , ' : ''}} {{$data->country_name}}</span>
            <div class="ratting-area">
                @if(isset($data->rating))
                    <ul>
                        @for($x = 0; $x< 5 ; $x++)
                            @if(floor($data->rating) - $x >=1)
                                <li><i class="fa fa-star"></i></li>
                            @elseif($data->rating -$x >0)
                                <li><i class="fa fa-star-half-o"></i></li>
                            @else
                                <li><i class="fa fa-star-o"></i></li>
                            @endif
                        @endfor
                    </ul>
                @else
                    <ul>
                        @for($x = 0; $x< 5 ; $x++)
                            <li><i class="fa fa-star-o"></i></li>
                        @endfor
                    </ul>
                @endif
                <p><span>{{isset($data->rating) ? number_format($data->rating,1) : '0.0'}}</span> ({{$data->total_reviews}} {{__('Reviews')}})</p>
            </div>
            <div class="pro-bottom">
                <ul>
                    @if (checkOpenCloseStatus($data->spotlist_directory_id) == TRUE)
                        <li><small class="theme-btn open">{{__('Now Open')}}</small></li>
                    @else
                        <li><small class="theme-btn close">{{__('Closed')}}</small></li>
                    @endif
                     <li><a class="popup-gmaps" href="https://maps.google.com/maps?q={{$data->latitude}},{{$data->longitude}}&hl=es&z=14&amp;output=embed" target="_blank"><img src="{{asset('FontEnd/')}}/assets/images/logo/Vector.png" alt="">{{__('Show On Map')}}</a></li>
                </ul>
            </div>
        </div>
        <div class="thumb">
            <span>{{$data->category_name}}</span>
        </div>
        @if($data->is_featured == 1)
            <div class="pro-featured">
                <span class="theme-btn v3" >{{__('Featured')}}</span>
            </div>
        @endif
    </div>
</div>
