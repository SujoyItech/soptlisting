<?php

/*=================================================Admin=============================================*/



Route::get('admin/home', 'HomeController@index')->name('admin.home');

Route::get('admin-sub-category/{id?}', 'CategoryModule\CategoryController@adminSubCategory')->name('admin-sub-category');
Route::get('admin-sub-category-list', 'CategoryModule\CategoryController@adminSubCategoryList')->name('admin-sub-category-list');
Route::post('admin-subcategory-save', 'CategoryModule\CategoryController@adminSubCategorySave')->name('admin-subcategory-save');

Route::get('spotlist-feature/{id?}', 'CategoryModule\CategoryController@spotlistFeatures')->name('spotlist-feature');
Route::get('spotlist-feature-list', 'CategoryModule\CategoryController@spotlistFeaturesList')->name('spotlist-feature-list');
Route::post('spotlist-feature-save', 'CategoryModule\CategoryController@spotlistFeaturesSave')->name('spotlist-feature-save');

Route::get('spotlist-faqs/{id?}', 'CategoryModule\CategoryController@spotlistFaqs')->name('spotlist-faqs');
Route::get('spotlist-faqs-list', 'CategoryModule\CategoryController@spotlistFaqsList')->name('spotlist-faqs-list');
Route::post('spotlist-faqs-save', 'CategoryModule\CategoryController@spotlistFaqsSave')->name('spotlist-faqs-save');

Route::get('admin-main-category', 'CategoryModule\CategoryController@adminMainCategory')->name('admin-main-category');
Route::post('admin-main-category-picture-change', 'CategoryModule\CategoryController@adminMainCategoryPictureChange')->name('admin-main-category-picture-change');
Route::post('admin-child-category-delete', 'CategoryModule\CategoryController@adminChildCategoryDelete')->name('admin-child-category-delete');
Route::post('admin-main-category-status-change', 'CategoryModule\CategoryController@adminMainCategoryStatusChange')->name('admin-main-category-status-change');

Route::get('admin-city-list', 'CategoryModule\CategoryController@adminCityList')->name('admin-city-list');
Route::get('admin-city-create/{id?}', 'CategoryModule\CategoryController@adminCityAdd')->name('admin-city-create');
Route::post('admin-city-save', 'CategoryModule\CategoryController@adminCitySave')->name('admin-city-save');
Route::post('admin-check-city-name', 'CategoryModule\CategoryController@checkCitySlug')->name('checkCitySlug');
Route::post('check-city-used', 'CategoryModule\CategoryController@checkCityUsedInDirectory')->name('check-city-used');

Route::get('directory-list/{directory_id?}', 'DirectoryModule\DirectoryController@adminAllDirectoryList')->name('directory-list');
Route::get('admin-drectory', 'DirectoryModule\DirectoryController@adminMyDirectoryList')->name('admin-drectory');
Route::get('admin-wishing-list', 'DirectoryModule\DirectoryController@adminWishingList')->name('admin-wishing-list');
Route::get('admin-all-wishing-list', 'DirectoryModule\DirectoryController@adminAllWishingList')->name('admin-all-wishing-list');


Route::post('directory-status-change', 'DirectoryModule\DirectoryController@adminDirectoryStatusChange')->name('directory-status-change');
Route::get('admin-claimed-drectory', 'DirectoryModule\DirectoryController@adminClaimedDirectory')->name('admin-claimed-drectory');
Route::get('admin-reported-directory', 'DirectoryModule\DirectoryController@adminReportedDirectory')->name('admin-reported-directory');

Route::post('check-blog-slug', 'PostModule\PostController@checkBlogSlug')->name('check-blog-slug');

Route::get('admin-blog-list', 'PostModule\PostController@adminBlogList')->name('admin-blog-list');
Route::get('admin-blog-create/{id?}', 'PostModule\PostController@adminBlogAdd')->name('admin-blog-add');
Route::post('admin-blog-save', 'PostModule\PostController@adminBlogSave')->name('admin-blog-save');



Route::get('admin-payment-history', 'PricingModule\PricingController@adminPaymentHistory')->name('admin-payment-history');

Route::get('admin-package-create/{id?}', 'PricingModule\PricingController@adminPackageAdd')->name('admin-package-create');
Route::post('admin-package-save', 'PricingModule\PricingController@adminPackageSave')->name('admin-package-save');
Route::get('admin-packages-list', 'PricingModule\PricingController@adminPackageList')->name('admin-packages-list');

Route::get('admin-offline-payment', 'PricingModule\PricingController@adminOfflinePayment')->name('admin-offline-payment');
Route::post('admin-user-offline-payment-save', 'PricingModule\PricingController@adminUserOfflinePaymentSave')->name('admin-user-offline-payment-save');

Route::get('admin-users-list', 'UserModule\UserController@adminUserList')->name('admin-users-list');
Route::get('admin-user-create/{id?}', 'UserModule\UserController@adminUserAdd')->name('admin-user-create');
Route::get('admin-user-status-change/{user_id}/{status}', 'UserModule\UserController@adminUserStatusChange')->name('admin-user-status-change');
Route::post('admin-user-information-save', 'UserModule\UserController@adminUserInformationSave')->name('admin-user-information-save');
Route::post('admin-user-password-update', 'UserModule\UserController@adminUserPasswordUpdate')->name('admin-user-password-update');

Route::get('admin-subscribed-email-list', 'UserModule\UserController@adminSubscribedEmails')->name('admin-subscribed-email-list');
Route::get('admin-user-email-subscriber-status-change/{id}/{status}', 'UserModule\UserController@adminSubscribedEmailStatusChange')->name('admin-user-email-subscriber-status-change');

Route::get('admin-user-message-list', 'UserModule\UserController@adminUserMessageList')->name('admin-user-message-list');
Route::post('admin-get-user-message', 'UserModule\UserController@adminGetUserMessage')->name('admin-get-user-message');

Route::post('send-email-to-user', 'UserModule\UserController@sendEmailToUser')->name('send-email-to-user');


Route::post('get-top-directory-chart', 'HomeController@getTopDirectoryChart')->name('getTopDirectoryChart');
Route::get('get-top-user-chart', 'HomeController@getTopUserChart')->name('getTopUserChart');
Route::get('get-directory-list-by-category', 'HomeController@getDirectoryListByCategory')->name('getDirectoryListByCategory');


