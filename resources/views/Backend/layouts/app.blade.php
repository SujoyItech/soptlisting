@php($settings = __options(['application_settings', 'admin_settings']))
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="laraframe" content="{{ csrf_token() }}"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{getImageUrl($settings->favicon_logo ?? 'other')}}">
    <title>{{isset($settings->app_title) ? $settings->app_title.' : ' : '' }} @yield('title')</title>
    @include('Backend.includes.assets_header')
</head>
<body data-layout-mode="detached" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": true}, "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": false}'>

<div id="wrapper">
    @include('Backend.includes.header')
    @if(Auth::user()->default_module_id == LISTING_MANAGER)
        @include('Backend.includes.left_sidebar_listing_manager')
    @else
         @include('Backend.includes.left_sidebar')
    @endif
    <div class="content-page">
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        @include('Backend.includes.footer')
    </div>
</div>
@include('Backend.includes.assets_js')
</body>
</html>
