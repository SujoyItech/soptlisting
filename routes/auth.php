<?php
Route::group(['middleware'=>'home.check'],function (){
    Route::get('/', 'FontEnd\Home\HomeController@index')->name('home');
});

Route::get('login', 'Auth\AuthController@login')->name('login');
Route::post('post-login', 'Auth\AuthController@postLogin')->name('postLogin');
Route::post('post-login-modal', 'Auth\AuthController@postLoginModal')->name('postLoginModal');

Route::get('user-register', 'Auth\AuthController@register')->name('user.register');
Route::post('user-register-save', 'Auth\AuthController@userRegistrationSave')->name('user.registration.save');
Route::post('user-register-modal-save', 'Auth\AuthController@userRegistrationModal')->name('userRegistrationModal');

Route::get('user-email-verify/{code}', 'Auth\AuthController@userEmailVerify')->name('user-email-verify');
Route::get('logout', 'Auth\AuthController@logout')->name('logout');
Route::post('multi-login-action', 'User\UserController@multiLoginAction');
Route::post('notify-dismiss', 'User\UserController@notifyDismiss');

/**************************User Forgot Password*******************/

Route::get('forget-password', 'Auth\AuthController@userForgetPassword')->name('forgetPassword');
Route::post('send-forget-password-mail', 'Auth\AuthController@sendForgetPasswordMail')->name('sendForgetPasswordMail');
Route::get('reset-password/{remember_token}', 'Auth\AuthController@resetPasword')->name('resetPassword');
Route::post('password-change', 'Auth\AuthController@changePassword')->name('password-change');

/************************Permission Denied**********************/

Route::get('login/{driver}', 'Auth\AuthController@redirectToProvider')->name('socialLogin');
Route::get('login/{driver}/callback', 'Auth\AuthController@handleProviderCallback');

