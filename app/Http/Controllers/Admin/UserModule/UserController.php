<?php

namespace App\Http\Controllers\Admin\UserModule;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Service\MailService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $service;

    public function __construct() {
        $this->service  = new UserService();
    }
    public function adminUserList(Request $request){

        $user_data = $this->service->userListData();

        if ($request->ajax()){

            return datatables($user_data)
                ->editColumn('default_module_id', function ($item) {
                    if ($item->default_module_id == 1){
                        return 'Super Admin';
                    }elseif ($item->default_module_id == 2) {
                        return 'Admin';
                    }elseif ($item->default_module_id == 3){
                        return 'Listing Manager';
                    }elseif ($item->default_module_id == 4){
                        return 'User';
                    }
                })->editColumn('status', function ($item) {
                    if ($item->status == 'Active'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }elseif ($item->status == 'Pending') {
                        return '<span class="badge badge-warning">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-danger">'.$item->status.'</span>';
                    }
                })->editColumn('user_image', function ($item) {
                    $html = '<img src="'.asset($item->user_image).'" class="rounded" width="50">';
                    return $html;
                })->editColumn('action', function ($item) {
                    $html = ' <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-rounded btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.__('Action').'</button>'.
                                    '<div class="dropdown-menu">
                                        <a class="dropdown-item" href="'.url('admin-user-create/'.$item->id).'">'.__('Edit').'</a>';
                        if ($item->status == 'Pending'){
                            $html .= '<a class="dropdown-item" href="'.url('admin-user-status-change/'.$item->id.'/Active').'">'.__('Active').'</a>';
                        }elseif ($item->status == 'Active'){
                            $html .= '<a class="dropdown-item" href="'.url('admin-user-status-change/'.$item->id.'/Suspended').'">'.__('Suspend').'</a>';
                            $html .= '<a class="dropdown-item" href="'.url('admin-user-status-change/'.$item->id.'/Deleted').'">'.__('Delete').'</a>';
                            $html .= '<a class="dropdown-item" href="'.url('admin-user-status-change/'.$item->id.'/Blocked').'">'.__('Block').'</a>';
                        }else{
                            $html .= '<a class="dropdown-item" href="'.url('admin-user-status-change/'.$item->id.'/Active').'">'.__('Active').'</a>';
                        }
                        $html .='</div>';
                        $html .='</div>';

                    return $html;
                })
                ->rawColumns(['user_image','status','action'])
                ->make(TRUE);
        }
        return view('Admin.User.user_list');
    }

    public function adminUserStatusChange($user_id,$status){
        $response = $this->service->adminUserStatusChange($user_id,$status);
        return $response['success'] == TRUE ? redirect()->route('admin-users-list')->with(['success'=>$response['message']]) : redirect()->route('admin-users-list')->with(['dismiss'=>$response['message']]);
    }

    public function adminUserAdd($id = NULL){
        $data = $this->service->getAdminUserData($id);
        return view('Admin.User.user_add',$data);
    }

    public function adminUserInformationSave(UserRequest $request){
        $response = $this->service->adminUserInformationSave($request);
        return response()->json($response);
    }

    public function adminUserPasswordUpdate(Request $request){
       $response = $this->service->adminUserPasswordUpdate($request);
      return $response;
    }

    public function adminSubscribedEmails(Request $request){
        $user_data = $this->service->userEmailSubscriberListData();
        if ($request->ajax()){

            return datatables($user_data)
              ->editColumn('status', function ($item) {
                    if ($item->status == 'Active'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-danger">'.$item->status.'</span>';
                    }
                })->editColumn('action', function ($item) {
                    $html = ' <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-rounded btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.__('Action').'</button>'.
                        '<div class="dropdown-menu">';
                    if ($item->status == 'Active'){
                        $html .= '<a class="dropdown-item" href="'.url('admin-user-email-subscriber-status-change/'.$item->id.'/Suspended').'">'.__('Suspend').'</a>';
                    }else{
                        $html .= '<a class="dropdown-item" href="'.url('admin-user-email-subscriber-status-change/'.$item->id.'/Active').'">'.__('Active').'</a>';
                    }
                    $html .='</div>';
                    $html .='</div>';

                    return $html;
                })
                ->rawColumns(['status','action'])
                ->make(TRUE);
        }
        return view('Admin.User.email_subscriber_list');
    }

    public function adminSubscribedEmailStatusChange($id,$status){
        $response = $this->service->adminSubscribedEmailStatusChange($id,$status);
        return $response['success'] == TRUE ? redirect()->route('admin-subscribed-email-list')->with(['success'=>$response['message']]) : redirect()->route('admin-subscribed-email-list')->with(['dismiss'=>$response['message']]);
    }

    public function adminUserMessageList(Request $request){
        $message_data = $this->service->userMessageListData();
        if ($request->ajax()){
            return datatables($message_data)
                ->editColumn('status', function ($item) {
                    if ($item->status == 'Pending'){
                        return '<span class="badge badge-warning">'.$item->status.'</span>';
                    }else if ($item->status == 'Seen'){
                        return '<span class="badge badge-secondary">'.$item->status.'</span>';
                    }else if ($item->status == 'Replied'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }
                })->editColumn('action', function ($item) {
                    return '<button class="btn btn-sm btn-info view_message" data-id="'.$item->id.'" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></button>';
                })
                ->rawColumns(['status','action'])
                ->make(TRUE);
        }
        return view('Admin.User.message_list');
    }

    public function adminGetUserMessage(Request $request){
        $response = $this->service->adminGetUserMessage($request);
        return response()->json($response);
    }

    public function sendEmailToUser(Request $request){
        try {
            $message = DB::table('spotlist_user_message_table')->where('id',$request->id)->first();
            $body['message'] = $request->message;
            $body['name'] = $message->fname.' '.$message->lname;
            MailService::sendMessageMail($message->email,$request->subject ?? $message->subject,$body);
            if ($message->status !== 'Replied'){
                DB::table('spotlist_user_message_table')->where('id',$request->id)->update(['status'=>'Replied']);
            }
            return redirect()->route('admin-user-message-list')->with(['success'=>__('Email successfully sent to user.')]);
        }catch (\Exception $exception){
            return redirect()->route('admin-user-message-list')->with(['dismiss'=>__('Email sent failed.')]);
        }
    }
}
