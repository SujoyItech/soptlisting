@extends('Backend.layouts.app',['menu'=>'city','sub_menu'=>'city_add'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-city-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New City')}}</a>
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-city-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('City list')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-globe"></i> {{__('Cities')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('admin-city-save')}}" class="parsley-examples" method="POST" id="submit_city" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="spotlist_cities_name">{{__('City Name')}}<span class="text-danger">*</span></label>
                                <input type="text" name="spotlist_cities_name" parsley-trigger="change" required value="{{$city->spotlist_cities_name ?? ''}}"
                                       placeholder="Enter city name" class="form-control" id="spotlist_cities_name">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="spotlist_cities_name">{{__('City Slug')}}<span class="text-danger">*</span></label>
                                <input type="text" name="spotlist_cities_slug" parsley-trigger="change" required value="{{$city->spotlist_cities_slug ?? ''}}" class="form-control" id="spotlist_cities_slug">
                                <span class="text-danger d-none show-text"></span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label class="col-form-label" for="country_name">{{__('Country')}}</label>
                                <select class="form-control" name="country_id" required>
                                    <option value="">{{__('--Select country--')}}</option>
                                    @if(isset($countries) && !empty($countries[0]))
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{is_selected($country->id,$city->country_id ?? '')}}>{{$country->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group ">
                                <label class="col-form-label" for="country_name">{{__('Status')}}</label>
                                @php($option = ['Active' =>__('Active'),'Inactive'=>__('Inactive')])
                                {!! Form::select('status', $option, $city->status ?? '', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="col-form-label" for="city_image">{{__('City Logo')}}</label>
                        <input type="file" name="city_image" data-plugins="dropify"
                               data-default-file="{{isset($city->city_image) ? asset($city->city_image) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="1M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light city_submit" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <input type="hidden" name="spotlist_cities_id" value="{{$city->spotlist_cities_id ?? ''}}" id="spotlist_cities_id">
    </div>
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        $( "#spotlist_cities_name" ).keyup(function( event ) {
            var slug = slugify($(this).val());
            $('#spotlist_cities_slug').val(slug);
            var check_city_name_url = "{{route('checkCitySlug')}}";
            var city_data = {
                slug : slug
            };
            makeAjaxPost(city_data,check_city_name_url,null).done(function (response){
                if (response.success == true){
                    $('.show-text').removeClass('text-danger');
                    $('.show-text').addClass('text-success');
                    $('.show-text').html('Slug is valid');
                    $('.show-text').removeClass('d-none');
                    $('.city_submit').prop("disabled",false);
                }else {
                    $('.show-text').removeClass('text-success');
                    $('.show-text').addClass('text-danger');
                    $('.show-text').html('Slug is already exist');
                    $('.show-text').removeClass('d-none');
                    $('.city_submit').prop("disabled",true);
                }
            });
        });

        $( "#spotlist_cities_slug" ).keyup(function( event ) {
            var slug = slugify($(this).val());
            $('#spotlist_cities_slug').val(slug);
            var check_city_name_url = "{{route('checkCitySlug')}}";
            var city_data = {
                slug : slug
            };
            makeAjaxPost(city_data,check_city_name_url,null).done(function (response){
                if (response.success == true){
                    $('.show-text').removeClass('text-danger');
                    $('.show-text').addClass('text-success');
                    $('.show-text').html('Slug is valid');
                    $('.show-text').removeClass('d-none');
                    $('.city_submit').prop("disabled",false);
                }else {
                    $('.show-text').removeClass('text-success');
                    $('.show-text').addClass('text-danger');
                    $('.show-text').html('Slug is already exist');
                    $('.show-text').removeClass('d-none');
                    $('.city_submit').prop("disabled",true);
                }
            });
        });


        $('.city_submit').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    if ($('#spotlist_cities_id').val().length !== 0){
                        formData.append('spotlist_cities_id',$('#spotlist_cities_id').val());
                    }
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            var redirect_url = "{{url('admin-city-list')}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'error');
                        }
                    });
                }
            });
        });
        function slugify(text) {
            return text
                .toString()                     // Cast to string
                .toLowerCase()                  // Convert the string to lowercase letters
                .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
                .trim()                         // Remove whitespace from both sides of a string
                .replace(/\s+/g, '-')           // Replace spaces with -
                .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                .replace(/\-\-+/g, '-');        // Replace multiple - with single -
        }
    </script>
@endsection
