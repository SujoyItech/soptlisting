<div class="col-lg-4 col-xl-4">
    <div class="card-box text-center">
        <img src="{{user_image($profile->user_image)}}" class="rounded-circle avatar-lg img-thumbnail"
             alt="profile-image">

        <h4 class="mb-0">{{$profile->username ?? ''}}</h4>
        <p class="text-muted">{{'@'.$profile->designation ?? ''}}</p>

        <div class="text-left mt-3">
            <h4 class="font-13 text-uppercase">{{__('About Me :')}}</h4>
            <p class="text-muted font-13 mb-3">
                {{$profile->about ?? ''}}
            </p>
            <p class="text-muted mb-2 font-13">
                <strong>{{__('Full Name :')}}</strong>
                <span class="ml-2">{{$profile->username ?? ''}}</span>
            </p>

            <p class="text-muted mb-2 font-13">
                <strong>{{__('Mobile : ')}}</strong>
                <span class="ml-2">{{$profile->mobile ?? ''}}</span>
            </p>

            <p class="text-muted mb-2 font-13"><strong>{{__('Email :')}}</strong> <span class="ml-2 ">{{$profile->email ?? ''}}</span></p>

            <p class="text-muted mb-1 font-13"><strong>{{__('Location :')}}</strong> <span class="ml-2">{{$profile->address ?? ''}}</span></p>
        </div>

        <ul class="social-list list-inline mt-3 mb-0">
            <li class="list-inline-item">
                <a href="{{$profile->facebook_link ?? ''}}" target="_blank" class="social-list-item border-primary text-primary"><i
                        class="mdi mdi-facebook"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="{{$profile->twitter_link ?? ''}}" target="_blank" class="social-list-item border-danger text-danger"><i
                        class="mdi mdi-google"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="{{$profile->instagram_link ?? ''}}" target="_blank" class="social-list-item border-info text-info"><i
                        class="mdi mdi-twitter"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="{{$profile->linkedin_link ?? ''}}" target="_blank" class="social-list-item border-secondary text-secondary"><i
                        class="mdi mdi-linkedin"></i></a>
            </li>
        </ul>
    </div>
</div>
