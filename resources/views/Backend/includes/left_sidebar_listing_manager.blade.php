
<div class="left-side-menu">
    <div class="h-100" data-simplebar>
        @include('Backend.includes.sidelogo')
        <div id="sidebar-menu" class="mt-3">
            <ul id="side-menu">
                <li @if(!empty($menu) && $menu == 'dashboard') class="menuitem-active" @endif>
                    <a href="{{route('listing-manager-dashboard')}}">
                        <i class="fa fa-home"></i>
                        <span> {{__('Dashboard')}} </span>
                    </a>
                </li>
                <li @if(!empty($menu) && $menu == 'wishing_list') class="menuitem-active" @endif>
                    <a href="{{route('list-manager-wishing-list')}}">
                        <i class="fa fa-heart"></i>
                        <span> {{__('Wishlist')}} </span>
                    </a>
                </li>
                <li @if(!empty($menu) && $menu == 'my_subscription') class="menuitem-active" @endif>
                    <a href="{{route('listing-manager-subscription')}}">
                        <i class="fa fa-gift"></i>
                        <span> {{__('My subscription')}} </span>
                    </a>
                </li>
                <li @if(!empty($menu) && $menu == 'pricing') class="menuitem-active" @endif>
                    <a href="{{route('listing-manager-pricing')}}">
                        <i class="fa fa-credit-card"></i>
                        <span> {{__('Pricing')}} </span>
                    </a>
                </li>
                <li @if(!empty($menu) && $menu == 'payment_history') class="menuitem-active" @endif>
                    <a href="{{route('listing-manager-payment-history')}}">
                        <i class="fa fa-paperclip"></i>
                        <span> {{__('Purchase history')}} </span>
                    </a>
                </li>
                <li @if(!empty($menu) && $menu == 'directory') class="menuitem-active" @endif>
                    <a href="#sidebarDirectory" data-toggle="collapse">
                        <i class="fa fa-sitemap"></i>
                        <span> {{__('Directory')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse @if(!empty($menu) && $menu == 'directory') show @endif" id="sidebarDirectory">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('list-manager-directory-list')}}" @if(!empty($sub_menu) && $sub_menu == 'directory_list') class="active" @endif>
                                    <i class="fa fa-list"></i>
                                    <span> {{__('All Directory')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('directory-create')}}" @if(!empty($sub_menu) && $sub_menu == 'create_directory') class="active" @endif>
                                    <i class="fa fa-plus"></i>
                                    <span> {{__('Add new Directory')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'booking') class="menuitem-active" @endif>
                    <a href="#sidebarBooking" data-toggle="collapse">
                        <i class="fa fa-calendar"></i>
                        <span> {{__('Bookings')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarBooking">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{url('user-booking-request/all')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('All')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user-booking-request/hotel')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Hotel')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user-booking-request/restaurant')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Restaurant')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user-booking-request/beauty')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Beauty')}} </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user-booking-request/fitness')}}">
                                    <i class="fa fa-list"></i>
                                    <span> {{__('Fitness')}} </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li @if(!empty($menu) && $menu == 'profile') class="menuitem-active" @endif>
                    <a href="{{route('profile-settings')}}">
                        <i class="fa fa-user-circle"></i>
                        <span> {{__('Profile')}} </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
