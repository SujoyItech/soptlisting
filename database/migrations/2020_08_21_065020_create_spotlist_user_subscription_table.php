<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistUserSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_user_subscription', function (Blueprint $table) {
            $table->increments('spotlist_user_subscription_id');
            $table->unsignedInteger('spotlist_packages_id');
            $table->unsignedBigInteger('user_id');
            $table->integer('day_remain')->nullable();
            $table->integer('num_of_listing')->nullable();
            $table->integer('listing_item_used')->nullable();
            $table->integer('num_of_categories')->nullable();
            $table->integer('category_used')->nullable();
            $table->integer('num_of_tags')->nullable();
            $table->integer('tag_used')->nullable();
            $table->integer('num_of_photos')->nullable();
            $table->integer('photos_used')->nullable();
            $table->integer('add_video_ability')->nullable();
            $table->integer('add_contact_form_ability')->nullable();
            $table->boolean('free_subscription_check')->default(0);
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->foreign('user_id')->references('id')->on('sys_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_user_subscription');
    }
}
