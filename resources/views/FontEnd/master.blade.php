<!DOCTYPE html>
<html lang="en">
@php($settings = __options(['frontend_settings']))
{{--{{dd($settings)}}--}}
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="laraframe" content="{{ csrf_token() }}"/>
    <link rel="shortcut icon" type="image/png" href="{{isset($settings->favicon_logo) ? asset($settings->favicon_logo):''}}">
    <!-- Page Title -->
    <title>{{$settings->app_title ?? __('Spotlisting Web HTML')}} </title>
    @include('FontEnd.includes.asset_header')
    @yield('style')
</head>

<body>
<div class="page-wrapper">
    <!-- start page-loader -->
    <div class="page-loader">
        <div class="page-loader-inner">
            <div class="inner"></div>
        </div>
    </div>
    <!-- end page-loader -->
    <!-- header-area start -->
    @include('FontEnd.includes.header')
    <!-- header-area end -->
    @yield('content')
    <!-- start site-footer -->
    @include('FontEnd.includes.footer')
    <!-- end site-footer -->
</div>
<!-- All JavaScript files
================================================== -->
@include('FontEnd.includes.asset_js')


</body>

</html>
