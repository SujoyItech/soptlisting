<?php
namespace App\Http\Controllers\Admin\PricingModule;
use App\Http\Controllers\Controller;
use App\Http\Service\MailService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PricingController extends Controller
{
    private $service;

    public function __construct() {
        $this->service  = new PricingService();
    }

    public function adminPackageList(Request $request){
        $packageList = $this->service->getPackageData();
        if ($request->ajax()){

            return datatables($packageList)
                ->editColumn('status', function ($item) {
                    if ($item->status == 'Active'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-warning">'.$item->status.'</span>';
                    }
                })
                ->rawColumns(['status'])
                ->make(TRUE);
        }
        return view('Admin.Pricing.package_list');
    }

    public function adminPackageAdd($id = NULL){
        $data = [];
        if ($id !== NULL){
            $data['package'] = $this->service->getPackageData($id);
        }
        return view('Admin.Pricing.new_packages_add',$data);
    }

    public function adminPackageSave(Request $request){
        $response = $this->service->adminPackageSave($request);
        return response()->json($response);
    }


    public function adminOfflinePayment(){
        $data['users'] = DB::table('sys_users')->where(['status'=>'Active','default_module_id'=>LISTING_MANAGER])->select('id','name')->get();
        $data['packages'] = DB::table('spotlist_packages')->where(['status'=>'Active','packages_type'=>'Paid'])->get();
        return view('Admin.Pricing.offline_payment',$data);
    }

    public function adminUserOfflinePaymentSave(Request $request){
       $response = $this->service->adminUserOfflinePaymentSave($request);
        return $response;
    }


    public function adminPaymentHistory(Request $request){
        $paymenHistory = $this->service->getPaymentHistory();
        if ($request->ajax()){

            return datatables($paymenHistory)
                ->editColumn('packages_type', function ($item) {
                    if ($item->packages_type == 'Free'){
                        return '<span class="badge badge-success">'.$item->packages_type.'</span>';
                    }else{
                        return '<span class="badge badge-warning">'.$item->packages_type.'</span>';
                    }
                })
                ->rawColumns(['packages_type'])
                ->make(TRUE);
        }
        return view('Admin.Pricing.payment_history');
    }


}
