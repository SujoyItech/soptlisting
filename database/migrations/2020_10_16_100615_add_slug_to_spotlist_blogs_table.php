<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugToSpotlistBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spotlist_blogs', function (Blueprint $table) {
            $table->string('slug')->after('spotlist_blogs_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spotlist_blogs', function (Blueprint $table) {
            //
            $table->dropColumn('slug');
        });
    }
}
