<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SysUsersSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sys_users')->delete();
        
        \DB::table('sys_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_code' => '',
                'username' => 'Super Admin',
                'designation' => NULL,
                'email' => 'superadmin@gmail.com',
                'password' => '$2y$10$Mdpv8wuH7XyOR7OsA.0fQ.Qo6X5zwiitDLxe4LycvegQYRT5IxdVS',
                'password_key' => '',
                'name' => 'Nirjhar Mandal',
                'email_verified' => 0,
                'email_verified_at' => NULL,
                'mobile' => '+8801855149196',
                'date_of_birth' => '1993-01-04',
                'blood_group' => '',
                'gender' => 'Male',
                'marital_status' => 'Married',
                'father_name' => '',
                'mother_name' => '',
                'nationality' => '',
                'nid' => '',
                'tin' => '',
                'passport' => NULL,
                'user_image' => '',
                'address' => 'Nikunjo, Uttra, Dhaka',
                'last_login' => NULL,
                'date_of_join' => '2019-01-07',
                'default_url' => '',
                'default_module_id' => 1,
                'remember_token' => 'aybPFmCgv1qNa1ulBgVnTjURcACfeZnxvJ9HksbuAEcIjqIsNMAUpxnMCFzj',
                'password_changed_date' => '2020-01-01 03:50:27',
                'wrong_attempts_count' => 0,
                'created_by' => NULL,
                'updated_by' => 1,
                'created_at' => '2019-05-05 11:15:53',
                'updated_at' => '2020-06-24 03:01:53',
                'status' => 'Active',
            ),
            1 => 
            array (
                'id' => 2,
                'user_code' => NULL,
                'username' => 'Admin',
                'designation' => 'softwaredeveloper',
                'email' => 'admin@gmail.com',
                'password' => '$2y$10$qhgMGkbyDCU0WDRo6ZT/H.dFBrWlWGf6zWooF.gpwJqARdyXENwbS',
                'password_key' => NULL,
                'name' => 'Sujoy Nath',
                'email_verified' => 0,
                'email_verified_at' => NULL,
                'mobile' => '22131234',
                'date_of_birth' => NULL,
                'blood_group' => NULL,
                'gender' => 'Male',
                'marital_status' => NULL,
                'father_name' => NULL,
                'mother_name' => NULL,
                'nationality' => NULL,
                'nid' => NULL,
                'tin' => NULL,
                'passport' => NULL,
                'user_image' => 'Backend/images/users/gBZGm4aXfgo6d6aK.jpg',
                'address' => 'sdfsdfsdf',
                'last_login' => NULL,
                'date_of_join' => NULL,
                'default_url' => NULL,
                'default_module_id' => 2,
                'remember_token' => NULL,
                'password_changed_date' => '2020-09-22 06:36:01',
                'wrong_attempts_count' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2020-09-22 06:36:01',
                'updated_at' => '2020-09-22 06:36:01',
                'status' => 'Active',
            ),
            2 => 
            array (
                'id' => 3,
                'user_code' => NULL,
                'username' => 'Listing Manager',
                'designation' => NULL,
                'email' => 'manager@gmail.com',
                'password' => '$2y$10$Mdpv8wuH7XyOR7OsA.0fQ.Qo6X5zwiitDLxe4LycvegQYRT5IxdVS',
                'password_key' => NULL,
                'name' => 'Test demo',
                'email_verified' => 0,
                'email_verified_at' => NULL,
                'mobile' => NULL,
                'date_of_birth' => NULL,
                'blood_group' => NULL,
                'gender' => 'Male',
                'marital_status' => NULL,
                'father_name' => NULL,
                'mother_name' => NULL,
                'nationality' => NULL,
                'nid' => NULL,
                'tin' => NULL,
                'passport' => NULL,
                'user_image' => '',
                'address' => NULL,
                'last_login' => NULL,
                'date_of_join' => NULL,
                'default_url' => NULL,
                'default_module_id' => 3,
                'remember_token' => NULL,
                'password_changed_date' => '2020-09-22 07:35:43',
                'wrong_attempts_count' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2020-09-22 07:35:43',
                'updated_at' => '2020-09-22 07:35:43',
                'status' => 'Active',
            ),
            3 => 
            array (
                'id' => 4,
                'user_code' => NULL,
                'username' => 'Sujoy Nath',
                'designation' => 'software developer',
                'email' => 'user@gmail.com',
                'password' => '$2y$10$Mdpv8wuH7XyOR7OsA.0fQ.Qo6X5zwiitDLxe4LycvegQYRT5IxdVS',
                'password_key' => NULL,
                'name' => 'Sujoy Nath',
                'email_verified' => 0,
                'email_verified_at' => NULL,
                'mobile' => '21312555',
                'date_of_birth' => NULL,
                'blood_group' => NULL,
                'gender' => 'Male',
                'marital_status' => NULL,
                'father_name' => NULL,
                'mother_name' => NULL,
                'nationality' => NULL,
                'nid' => NULL,
                'tin' => NULL,
                'passport' => NULL,
                'user_image' => 'Backend/images/users/5tAMIa9kmLqTHnWO.jpg',
                'address' => 'sdfds',
                'last_login' => NULL,
                'date_of_join' => NULL,
                'default_url' => NULL,
                'default_module_id' => 4,
                'remember_token' => NULL,
                'password_changed_date' => '2020-09-22 07:43:52',
                'wrong_attempts_count' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2020-09-22 07:43:52',
                'updated_at' => '2020-09-22 07:43:52',
                'status' => 'Active',
            ),
            4 => 
            array (
                'id' => 5,
                'user_code' => NULL,
                'username' => 'List manager Test',
                'designation' => NULL,
                'email' => 'listmanagertest@gmail.com',
                'password' => '$2y$10$Mdpv8wuH7XyOR7OsA.0fQ.Qo6X5zwiitDLxe4LycvegQYRT5IxdVS',
                'password_key' => NULL,
                'name' => 'List manager Test',
                'email_verified' => 0,
                'email_verified_at' => NULL,
                'mobile' => NULL,
                'date_of_birth' => NULL,
                'blood_group' => NULL,
                'gender' => 'Male',
                'marital_status' => NULL,
                'father_name' => NULL,
                'mother_name' => NULL,
                'nationality' => NULL,
                'nid' => NULL,
                'tin' => NULL,
                'passport' => NULL,
                'user_image' => '/img/users/Avatar.png',
                'address' => NULL,
                'last_login' => NULL,
                'date_of_join' => NULL,
                'default_url' => NULL,
                'default_module_id' => 3,
                'remember_token' => NULL,
                'password_changed_date' => '2020-10-13 06:23:33',
                'wrong_attempts_count' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2020-10-13 06:23:33',
                'updated_at' => '2020-10-13 06:23:33',
                'status' => 'Active',
            ),
            5 => 
            array (
                'id' => 17,
                'user_code' => NULL,
                'username' => 'Sujoy Nath',
                'designation' => NULL,
                'email' => 'sujoy.csesust@gmail.com',
                'password' => '$2y$10$gztBxrCzTV1BxHGJ8J20HePnPivbACl2d/mNjw9R6jZlsNlcmkeBK',
                'password_key' => NULL,
                'name' => 'Sujoy Nath',
                'email_verified' => 1,
                'email_verified_at' => NULL,
                'mobile' => NULL,
                'date_of_birth' => NULL,
                'blood_group' => NULL,
                'gender' => 'Male',
                'marital_status' => NULL,
                'father_name' => NULL,
                'mother_name' => NULL,
                'nationality' => NULL,
                'nid' => NULL,
                'tin' => NULL,
                'passport' => NULL,
                'user_image' => '/img/users/Avatar.png',
                'address' => NULL,
                'last_login' => NULL,
                'date_of_join' => NULL,
                'default_url' => NULL,
                'default_module_id' => 4,
                'remember_token' => NULL,
                'password_changed_date' => '2020-10-14 05:07:20',
                'wrong_attempts_count' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2020-10-14 05:07:20',
                'updated_at' => '2020-10-14 05:08:04',
                'status' => 'Active',
            ),
            6 => 
            array (
                'id' => 18,
                'user_code' => NULL,
                'username' => 'Sujoy Nath',
                'designation' => NULL,
                'email' => 'sujoy.cse21@gmail.com',
                'password' => '$2y$10$bs84pF3Lkh6d883OoTYPWOFiVctva7zXTF/ax7vGJ7AE5zlr.xVOC',
                'password_key' => NULL,
                'name' => 'Sujoy Nath',
                'email_verified' => 0,
                'email_verified_at' => NULL,
                'mobile' => NULL,
                'date_of_birth' => NULL,
                'blood_group' => NULL,
                'gender' => 'Male',
                'marital_status' => NULL,
                'father_name' => NULL,
                'mother_name' => NULL,
                'nationality' => NULL,
                'nid' => NULL,
                'tin' => NULL,
                'passport' => NULL,
                'user_image' => '/img/users/Avatar.png',
                'address' => NULL,
                'last_login' => NULL,
                'date_of_join' => NULL,
                'default_url' => NULL,
                'default_module_id' => 4,
                'remember_token' => NULL,
                'password_changed_date' => '2020-10-14 05:09:48',
                'wrong_attempts_count' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2020-10-14 05:09:48',
                'updated_at' => '2020-10-14 05:09:48',
                'status' => 'Active',
            ),
        ));

        
    }
}