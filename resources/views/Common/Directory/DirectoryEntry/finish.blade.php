<div class="tab-pane" id="finish">
    <div class="row">
        <div class="col-12">
            @if(isset($directory->spotlist_directory_id) && !empty($directory->spotlist_directory_id))
            <form method="post" action="{{url('directory-all-save')}}" id="directory-all-save">
                <div class="text-center">
                    <h2 class="mt-0"><i class="mdi mdi-check-all"></i></h2>
                    <h3 class="mt-0">{{__('Thank you !')}}</h3>

                    <p class="w-75 mb-2 mx-auto">{{__('You are almost there. Please check if you have provided all the necessary things.')}}</p>

                    <div class="mb-3">
                        <button type="submit" class="btn btn-info btn-rounded directory_finish">{{__('Submit')}}</button>
                    </div>
                </div>
            </form>
            @else
                <p class="w-75 mb-2 mx-auto text-center">{{__('Please create your directory first.')}}</p>
            @endif
        </div>
    </div>
</div>
<script>
    $('.directory_finish').on('click', function (e) {
        Ladda.bind(this);
        var load = $(this).ladda();
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);

                if ($('#spotlist_directory_id').val().length !== 0){
                    formData.append('spotlist_directory_id',$('#spotlist_directory_id').val());
                }
                makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                    load.ladda('stop');
                    if (response.success == true){
                        var redirect_url = "{{\Illuminate\Support\Facades\Auth::user()->default_module_id == LISTING_MANAGER ? url('list-manager-directory-list') : url('directory-list') }}";
                        swalRedirect(redirect_url, response.message, 'success');
                    }else{
                        var redirect_url = "{{Request::url()}}";
                        swalRedirect(redirect_url, response.message, 'error');
                    }
                });
            }
        });
    });
</script>
