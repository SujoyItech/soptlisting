<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_cities', function (Blueprint $table) {
            $table->increments('spotlist_cities_id');
            $table->string('spotlist_cities_name');
            $table->string('city_image')->nullable();
            $table->integer('country_id')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->integer('created_by')->default(2);
            $table->enum('status',['Active','Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_cities');
    }
}
