<?php


namespace App\Http\Controllers\ListingManager\Pricing;


use App\Http\Service\MailService;
use App\Http\Service\NotificationService;
use App\Models\User;
use App\Traits\DataReturn;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PricingService {
    use DataReturn,DataSaveTrait;
    public function getPricingList(){
        return $this->basicTableInfo('spotlist_packages',[],[],[],['status'=>'Active']);
    }

    public function packageSubscription($package_id,$status){
        $package = DB::table('spotlist_packages')->where('spotlist_packages_id',$package_id)->first();
        $subscription_data = $this->subscriptionData($package);
        $response = $this->insertOrUpdateTable('spotlist_user_subscription','user_id',Auth::user()->id,$subscription_data,'Subscription successful.','Subscription failed!');
        if ($response['success'] == TRUE){
            $subscription_history = $this->subscriptionHistoryData($package,$status);
            $this->sendPurchaseMail($package,Auth::user()->id);
            $this->sendPurchaseNotificationToAdmin($package);
            $response_history = $this->insertTable('spotlist_subscription_purchase_history',$subscription_history);
        }
        return $response;
    }

    private function subscriptionData($package){
        $subscription_data = [
            'spotlist_packages_id' => $package->spotlist_packages_id,
            'user_id' => Auth::user()->id,
            'num_of_listing' => $package->num_of_listing,
            'day_remain' => $package->day_validity,
            'num_of_categories' => $package->num_of_categories,
            'num_of_tags' => $package->num_of_tags,
            'num_of_photos' => $package->num_of_photos,
            'add_video_ability' => $package->add_video_ability,
            'add_contact_form_ability' => $package->add_contact_form_ability
        ];
        $old_package = DB::table('spotlist_user_subscription')->where('user_id',Auth::user()->id)->first();
        if (isset($old_package) && !empty($old_package)){
            $subscription_data['day_remain'] = $package->day_validity+$old_package->day_remain;
            $subscription_data['num_of_listing'] = $package->num_of_listing+$old_package->num_of_listing;
            $subscription_data['num_of_categories'] = $package->num_of_categories+$old_package->num_of_categories;
            $subscription_data['num_of_tags'] = $package->num_of_tags+$old_package->num_of_tags;
            $subscription_data['num_of_photos'] = $package->num_of_photos+$old_package->num_of_photos;
        }else{
            $subscription_data['listing_item_used'] = 0;
            $subscription_data['category_used'] = 0;
            $subscription_data['tag_used'] = 0;
            $subscription_data['photos_used'] = 0;
        }

        if ($package->packages_type == 'Free'){
            if (isset($old_package) && !empty($old_package)){
                if ($old_package->free_subscription_check !== TRUE){
                    $subscription_data['free_subscription_check'] = TRUE;
                }
            }else{
                $subscription_data['free_subscription_check'] = TRUE;
            }
        }

        return $subscription_data;

    }

    private function subscriptionHistoryData($package,$status){

        $history = [
            'spotlist_packages_id' => $package->spotlist_packages_id,
            'user_id' => Auth::user()->id,
            'purchase_date' => Carbon::now(),
            'amount_paid' => $package->price,
            'payment_method' => $package->packages_type == 'Free'? 'Free':'Card',
            'payment_status' => $package->packages_type,
            'created_at' => Carbon::now(),
            'purchase_log_data' => json_encode($status)
        ];
        return $history;

    }

    public function sendPurchaseMail($package,$user_id){
        try {
            $user = User::where('id',$user_id)->first();
            $body['message'] = __('Congratulations! ').$package->spotlist_packages_name.__(' package has been subscribed.');
            $body['name'] = $user->username;
            MailService::sendMessageMail($user->email, __('Spotlisting Package Subscription'),$body);
            $response['success'] = TRUE;
        }catch (\Exception $exception){
            $response['success'] = FALSE;
            $response['message'] = $exception->getMessage();
        }
        return $response;
    }

    public function sendPurchaseNotificationToAdmin($package){
        $notify_body = [
            'username' => Auth::user()->username,
            'user_image' => Auth::user()->user_image,
            'title' => __('New package subscription'),
            'body' => Auth::user()->username.__(' has been subscribed a new package ').$package->spotlist_packages_name
        ];

        $notify_to = [
             'user_id' => User::where('default_module_id',ADMIN)->pluck('id')->toArray(),
             'type' => SUBSCRIPTION_NOTIFICATION
        ];

        $service = new NotificationService();
        $service->sendNotification($notify_body,$notify_to);
    }
}
