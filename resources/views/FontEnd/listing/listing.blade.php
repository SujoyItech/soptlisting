@extends('FontEnd.master',['menu'=>'listing'])
@section('style')
@endsection
@php($settings = __options(['admin_settings','frontend_settings']))
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Listing')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    @if(isset($list) && !empty($list[0]))
    <!-- map-area start-->
    <div class="map-area mb-5">
        <div id="mymap" class="map d-none"></div>
    </div>
    <!-- map-area end-->
    @php($grid_view = isset($_GET['view']) ?  $_GET['view'] == 'grid' ? 1 : 0 : 1)
    @php($list_view = isset($_GET['view']) ? $_GET['view'] == 'list' ? 1 : 0 : 0)
    <!-- listing-area start -->
    <div class="listing-area">
        {{-- <div class="list-v-wrap">
            <div class="view-list grid-btns">
                <div class="list-view-btn text-right">
                    <a href="{{route('user.listing',['view'=>'grid'])}}" class="btn btn-xs btn-info"><i class="fa fa-th"></i></a>
                    <a href="{{route('user.listing',['view'=>'list'])}}" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>
                </div>
            </div>
        </div> --}}
        <div class="container custom-container">
            <div class="row">
                @include('FontEnd.listing.search_listing')
                <div class="col-lg-9">
                    <div class="list-v-top">
                        <div class="row">
                            <div class="col-md-4 col-sm-8">
                                <p class="total-product"> Showing {{$list->count() > 0 ? ($list->currentpage()-1)*$list->perpage()+1 : 0}} - {{$list->currentpage()*$list->perpage() < $list->total() ? $list->currentpage()*$list->perpage() : $list->total()}}
                                    of   {{$list->total()}} entries</p>
                            </div>
                            <div class="col-md-8 col-sm-4">
                                <div class="view-list-icon">
                                    <div class="list-view-btn text-right">
                                        <a href="{{route('user.listing',['view'=>'grid'])}}" class="btn btn-xs btn-info"><i class="fa fa-th"></i></a>
                                        <a href="{{route('user.listing',['view'=>'list'])}}" class="btn btn-xs btn-info"><i class="fa fa-th-list"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="listing-wrap">
                        <div class="row">
                            @foreach($list as $data)
                               @includeWhen($grid_view,'FontEnd.listing.listing_item_grid_view')
                               @includeWhen($list_view,'FontEnd.listing.listing_item_list_view')
                            @endforeach
                        </div>
                        <div class="col-5 offset-5">
                            <div class="listing-wrap">
                                <div class="pagination-wrapper">
                                    {{ $list->appends($_GET)->links('vendor.pagination.bootstrap-4') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
        <div class="text-center no-data-image">
            <img src="{{asset('FontEnd/assets/images/error/nodata.svg')}}">
        </div>

    @endif
@endsection
@section('script')
    <!-- google maps -->
    <script src="https://maps.googleapis.com/maps/api/js?key={{$settings->map_key ?? 'AIzaSyBj0o3QDEGBtW3S8AEq0CRlpZdpWxNZckA'}}"></script>
    <script src="{{asset('FontEnd/assets/js/gmaps.js')}}"></script>
    <!-- Custom script for this template -->
    <script src="{{asset('FontEnd/')}}/assets/js/script.js"></script>
    <script>

        var locations = <?php print_r(json_encode($all_listing ?? [])) ?>;
        getLocation()
        function getLocation() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(showPosition,showError);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        function imageExists(url, callback) {
            var img = new Image();
            img.onload = function() { callback(true); };
            img.onerror = function() { callback(false); };
            img.src = url;
        }
        function showPosition(position) {
            $('#mymap').removeClass('d-none');
            var mymap = new GMaps({
                el: '#mymap',
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                zoom:12
            });
            $.each( locations, function( index, value ){
                var latitude = value.latitude;
                var longitude = value.longitude;
                if (latitude.length > 0 && longitude.length > 0){
                    var image_url = "{{asset('')}}"+value.thumb_image;
                    var checked_image_url = '';

                    var checkImageExist = new Promise(function (resolve){
                        imageExists(image_url,function (exists){
                            if (exists === true){
                                var checked_image_url =  image_url;
                                resolve(checked_image_url);
                            }else {
                                var checked_image_url = "{{asset('FontEnd/assets/images/listing.jpg')}}";
                                resolve(checked_image_url);
                            }
                        });
                    });

                    checkImageExist.then(function (checked_image_url){
                        mymap.addMarker({
                            lat: value.latitude,
                            lng: value.longitude,
                            icon: "{{asset('FontEnd/assets/images/icon.png')}}",
                            title: value.city_name,
                            infoWindow: {
                                content: '<div class="directory-item" style="width: min-content">\n' +
                                    '     <img src="'+checked_image_url+'" alt="bg" class="img-responsive text-center" width=300 height=200>\n' +
                                    '     <div class="loc-thumb">'+value.category_name+'</div>\n' +
                                    '     <div class="content mt-2 text-center">\n' +
                                    '         <h3><a href="{{url('/user/listing/')}}'+'/'+value.spotlist_directory_slug+'">'+value.spotlist_directory_name+'</a></h3>\n' +
                                    '         <span>'+ value.address +'</span>\n' +
                                    '     </div>\n' +
                                    ' </div>'
                            },
                            click: function(e) {
                                this.infoWindow.open(this.mymap, this);
                            }
                        });
                    });
                }

            });
        }

        function showError(error) {
            $('#mymap').addClass('d-none');
            switch(error.code) {
                case error.PERMISSION_DENIED:
                    $('#mymap').removeClass('d-none');
                    var mymap = new GMaps({
                        el: '#mymap',
                        lat: parseFloat("{{$settings->latitude ?? 22.819009982916523}}"),
                        lng: parseFloat("{{$settings->longitude ?? 89.5512269327881}}"),
                        zoom:12
                    });
                    $.each( locations, function( index, value ){
                        var latitude = value.latitude;
                        var longitude = value.longitude;
                        if (latitude.length > 0 && longitude.length > 0){
                            var image_url = "{{asset('')}}"+value.thumb_image;
                            var checked_image_url = '';

                            var checkImageExist = new Promise(function (resolve){
                                imageExists(image_url,function (exists){
                                    if (exists === true){
                                        var checked_image_url =  image_url;
                                        resolve(checked_image_url);
                                    }else {
                                        var checked_image_url = "{{asset('FontEnd/assets/images/listing.jpg')}}";
                                        resolve(checked_image_url);
                                    }
                                });
                            });

                            checkImageExist.then(function (checked_image_url){
                                mymap.addMarker({
                                    lat: value.latitude,
                                    lng: value.longitude,
                                    icon: "{{asset('FontEnd/assets/images/icon.png')}}",
                                    title: value.city_name,
                                    infoWindow: {
                                        content: '<div class="directory-item" style="width: min-content">\n' +
                                            '     <img src="'+checked_image_url+'" alt="bg" class="img-responsive text-center" width=300 height=200>\n' +
                                            '     <div class="loc-thumb">'+value.category_name+'</div>\n' +
                                            '     <div class="content mt-2">\n' +
                                            '         <h3><a href="{{url('/user/listing/')}}'+'/'+value.spotlist_directory_slug+'">'+value.spotlist_directory_name+'</a></h3>\n' +
                                            '         <span>'+ value.address +'</span>\n' +
                                            '     </div>\n' +
                                            ' </div>'
                                    },
                                    click: function(e) {
                                        this.infoWindow.open(this.mymap, this);
                                    }
                                });
                            });
                        }

                    });

                    break;
                case error.POSITION_UNAVAILABLE:
                    alert("Location information is unavailable.");
                    break;
                case error.TIMEOUT:
                    alert("The request to get user location timed out.")
                    break;
                case error.UNKNOWN_ERROR:
                    alert("An unknown error occurred.") ;
                    break;
            }
        }
    </script>
@endsection
