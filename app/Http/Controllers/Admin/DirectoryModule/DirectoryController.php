<?php

namespace App\Http\Controllers\Admin\DirectoryModule;

use App\Http\Controllers\Common\Directory\DirectoryService;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DirectoryController extends Controller
{
    private $directory_service;

    public function __construct() {
        $this->directory_service  = new DirectoryService();
    }


    public function adminAllDirectoryList(Request $request,$directory_id = ''){
        $status = '';
        $user_id = '';
        $verification_status = '';

        if (isset($_GET['status'])){
            $status = $_GET['status'];
        }
        if (isset($_GET['sys_user_id'])){
            $user_id = $_GET['sys_user_id'];
        }
        if (isset($_GET['verification_status'])){
            $verification_status = $_GET['verification_status'];
        }

        $directory_list = $this->directory_service->getDirectoryList($status,$user_id,$verification_status,$directory_id);

        $data['directory_list'] = $directory_list;
        $data['status'] = $status;
        $data['user_id'] = $user_id;
        $data['verification_status'] = $verification_status;

        return view('Admin.Directories.directory_list',$data);
    }

    public function adminMyDirectoryList(Request $request){
        $data['directory_list'] = $this->directory_service->getDirectoryList(NULL,Auth::user()->id,NULL);
        return view('Admin.Directories.my_directory_list',$data);
    }

    public function adminDirectoryStatusChange(Request $request){
        $response = $this->directory_service->adminDirectoryStatusChange($request);
        return response()->json($response);
    }

    public function adminWishingList(Request $request){
        $user_id = Auth::user()->id;
        $wishing_list = $this->directory_service->getWishingList($user_id);
        if ($request->ajax()) {

            return datatables($wishing_list)

                ->editColumn('thumb_image', function ($item) {
                    $html = '<img src="'.asset($item->thumb_image).'" class="rounded" width="50">';
                    return $html;
                })->editColumn('title', function ($item) {
                    $html = '<strong>'.$item->spotlist_directory_name.'</strong><br>';
                    return $html;
                })
                ->editColumn('category_name', function ($item) {
                    $html = '<span class="badge badge-secondary">' . $item->spotlist_category_name . '</span><br>';
                    return $html;
                }) ->editColumn('created_at', function ($item) {
                    $html = '<span class="badge badge-secondary">' . Carbon::parse($item->created_at )->format('d F Y'). '</span><br>';
                    return $html;
                })
                ->rawColumns(['thumb_image', 'title', 'category_name','created_at'])
                ->make(TRUE);
        }
        return view('Admin.Directories.wishing_list');
    }
    public function adminAllWishingList(Request $request){

        $wishing_list = $this->directory_service->getWishingList();
        if ($request->ajax()) {

            return datatables($wishing_list)

                ->editColumn('thumb_image', function ($item) {
                    $html = '<img src="'.asset($item->thumb_image).'" class="rounded" width="50">';
                    return $html;
                })->editColumn('title', function ($item) {
                    $html = '<strong>'.$item->spotlist_directory_name.'</strong><br>';
                    return $html;
                })
                ->editColumn('category_name', function ($item) {
                    $html = '<span class="badge badge-secondary">' . $item->spotlist_category_name . '</span><br>';
                    return $html;
                }) ->editColumn('created_at', function ($item) {
                    $html = '<span class="badge badge-secondary">' . Carbon::parse($item->created_at )->format('d F Y'). '</span><br>';
                    return $html;
                })
                ->rawColumns(['thumb_image', 'title', 'category_name','created_at'])
                ->make(TRUE);
        }
        return view('Admin.Directories.all_wishing_list');
    }


}
