<section class="hero hero-slider-wrapper hero-style-1">
    <div class="hero-slider">
        <div class="slide">
            <img src="{{asset('FontEnd/')}}/assets/images/slider/1.png" alt class="slider-bg">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-8 offset-lg-2 slide-caption">
                        <h2>{{__('Explore And Travel')}}</h2>
                        <p>{{__('Tourism is travel for recreation work. A tourist is a person who travels and stays outside his / her environment for a limited time for leisure, business, or other purposes.')}}</p>
                        <div class="btns text-center">
                            <a class="btn theme-btn" href="{{url('user/listing')}}">{{__('Listing here')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <img src="{{asset('FontEnd/')}}/assets/images/slider/2.png" alt class="slider-bg">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-8 offset-lg-2 slide-caption">
                        <h2>{{__('Explore And Travel')}}</h2>
                        <p>{{__('Tourism is travel for recreation work. A tourist is a person who travels and stays outside his / her environment for a limited time for leisure, business, or other purposes.')}}</p>
                        <div class="btns text-center">
                            <a class="btn theme-btn" href="{{url('user/listing')}}">Listing here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
