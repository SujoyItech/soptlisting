@extends('FontEnd.master',['menu'=>'profile'])
@section('style')
    <link href="{{asset('/Backend/libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Profile')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->

    <!-- profile-area start -->
    <div class="profile-area section-padding">
        <div class="container">
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="{{route('user.profile.settings')}}"  aria-expanded="true" class="nav-link active">
                            {{__('Profile')}}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('user.bookings')}}"  aria-expanded="false" class="nav-link">
                            {{__('My bookings')}}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('user.wishing')}}" aria-expanded="false" class="nav-link">
                            {{__('My Wishlist')}}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('user-all-notification')}}" aria-expanded="false" class="nav-link">
                            {{__('My notifications')}}
                        </a>
                    </li>
                </ul>
                <div class="profile-wrap">
                    <div class="profile-sub">
                        <div class="row">
                            @include('FontEnd.profile.profile_card')
                            <div class="col-lg-8 col-xl-8">
                                @include('FontEnd.profile.profile_basic_update')
                                @if(isset(\Illuminate\Support\Facades\Auth::user()->id) && \Illuminate\Support\Facades\Auth::user()->is_social == FALSE)
                                     @include('FontEnd.profile.profile_password_update')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('FontEnd.pages.user_email_subscription')
@endsection

@section('script')
    <script src="{{asset('/Backend/libs/dropify/js/dropify.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        $('.dropify').dropify();

    </script>

@endsection
