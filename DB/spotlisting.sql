-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: localhost    Database: spotlisting
-- ------------------------------------------------------
-- Server version	8.0.21-0ubuntu0.20.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint unsigned NOT NULL,
  `reserved_at` int unsigned DEFAULT NULL,
  `available_at` int unsigned NOT NULL,
  `created_at` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,NULL,'en','2020-10-12 12:40:03','2020-10-12 12:40:03');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `is_cleared` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_08_29_200844_create_languages_table',1),(4,'2018_08_29_205156_create_translations_table',1),(5,'2019_08_19_000000_create_failed_jobs_table',1),(6,'2020_03_02_072501_create_sys_dropdowns_table',1),(7,'2020_03_02_130721_create_sys_system_settings_table',1),(8,'2020_03_03_051644_create_sys_users_table',1),(9,'2020_08_21_064303_create_spotlist_packages_table',1),(10,'2020_08_21_065020_create_spotlist_user_subscription_table',1),(11,'2020_08_21_070836_create_spotlist_subscription_purchase_history_table',1),(12,'2020_08_21_071557_create_spotlist_rating_quality_table',1),(13,'2020_08_21_072007_create_spotlist_offline_payment_table',1),(14,'2020_08_21_072722_create_spotlist_features_table',1),(15,'2020_08_21_073226_create_spotlist_directory_table',1),(16,'2020_08_21_074723_create_spotlist_directory_vs_features_table',1),(17,'2020_08_21_080031_create_spotlist_category_table',1),(18,'2020_08_21_080354_create_spotlist_directory_vs_category_table',1),(19,'2020_08_21_080857_create_spotlist_directory_type_shop_table',1),(20,'2020_08_21_081726_create_spotlist_directory_type_restaurant_table',1),(21,'2020_08_21_091821_create_spotlist_directory_type_hotel_room_table',1),(22,'2020_08_21_092309_create_spotlist_directory_type_fitness_table',1),(23,'2020_08_21_094525_create_spotlist_directory_type_beauty_table',1),(24,'2020_08_21_094911_create_spotlist_directory_schedule_table',1),(25,'2020_08_21_095654_create_spotlist_directory_gallary_table',1),(26,'2020_08_21_101205_create_spotlist_countries_table',1),(27,'2020_08_21_104229_create_spotlist_cities_table',1),(28,'2020_08_21_110028_create_spotlist_booking_request_table',1),(29,'2020_08_24_060426_create_spotlist_blog_table',1),(30,'2020_09_03_054146_create_spotlist_directory_reviews_table',1),(31,'2020_09_08_123713_create_spotlist_directory_wishinglist_table',1),(32,'2020_09_09_130620_create_spotlist_directory_type_like_table',1),(33,'2020_09_11_093946_create_spotlist_user_follower_table',1),(34,'2020_09_11_095525_create_spotlist_user_subscription_by_email_table',1),(35,'2020_09_11_134351_create_spotlist_user_message_table',1),(36,'2020_10_01_130706_create_jobs_table',1),(37,'2020_10_12_071850_create_sys_users_additional_info',1),(38,'2020_10_12_072325_create_spotlisting_teams',1),(39,'2020_10_12_074928_create_spotlist_how_it_works',1),(40,'2020_10_12_075247_create_spotlist_faqs',1),(41,'2020_10_13_131722_create_user_verification_codes_table',2),(42,'2020_10_15_132748_create_notifications_table',3),(43,'2020_10_16_075832_add_spotlist_directory_slug_to_spotlist_directory_table',3),(44,'2020_10_16_100615_add_slug_to_spotlist_blogs_table',3),(45,'2020_10_18_032722_create_spotlist_directory_messages_table',3),(46,'2020_10_19_052233_change_notification_table_column',3),(47,'2020_10_19_123254_add_column_to_spotlist_booking_request',4),(48,'2020_10_20_062533_add_is_cleard_column_to_notifications',4),(49,'2020_10_20_134843_rename_product_imag_column',5),(50,'0000_00_00_000000_create_websockets_statistics_entries_table',6),(51,'2020_10_21_124245_create_messages_table',6),(52,'2020_10_21_134210_create_spotlist_booking_messages_table',6),(53,'2020_10_28_015217_edit_contents_column_to_spotlist_blogs',7),(54,'2020_10_28_103840_add_booking_date_to_spotlist_booking_request_table',7),(55,'2020_11_17_092907_add_slug_to_spotlist_cities_table',8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notifications` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_cleared` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,NULL,'{\"title\":\"Tiger garden new directory create request\",\"name\":\"List manager Test\",\"user_image\":\"\\/img\\/users\\/Avatar.png\",\"directory_name\":\"Tiger garden\",\"directory_id\":8,\"thumb_image\":null}',5,3,'2020-10-19 11:03:21','2020-10-22 09:32:50',1),(2,5,'{\"title\":\"Tiger garden verified successful.\",\"directory_name\":\"Tiger garden\",\"directory_id\":8,\"thumb_image\":null}',3,1,'2020-10-19 11:05:05','2020-10-19 11:05:46',0),(3,5,'{\"title\":\"Tiger garden make as active successful.\",\"directory_name\":\"Tiger garden\",\"directory_id\":8,\"thumb_image\":null}',3,1,'2020-10-19 11:05:12','2020-10-19 11:05:39',0),(4,5,'{\"title\":\"Tiger garden make as not featured  successful.\",\"directory_name\":\"Tiger garden\",\"directory_id\":8,\"thumb_image\":null}',3,0,'2020-10-20 07:51:29','2020-10-20 07:51:29',0),(5,NULL,'{\"title\":\"KFC1 new directory create request\",\"name\":\"Thiago Silva\",\"user_image\":\"Backend\\/images\\/users\\/Y2S69FCOB4zboKnM.jpg\",\"directory_name\":\"KFC1\",\"directory_id\":9,\"thumb_image\":null}',5,1,'2020-10-20 10:53:39','2020-10-22 09:32:50',1),(6,20,'{\"title\":\"KFC1 verified successful.\",\"directory_name\":\"KFC1\",\"directory_id\":9,\"thumb_image\":null}',3,1,'2020-10-20 10:58:55','2020-10-20 11:13:21',0),(7,20,'{\"title\":\"KFC1 make as active successful.\",\"directory_name\":\"KFC1\",\"directory_id\":9,\"thumb_image\":null}',3,1,'2020-10-20 10:59:09','2020-10-20 11:13:06',0),(8,20,'{\"username\":\"Almenda Lidia\",\"user_image\":\"\\/img\\/users\\/Avatar.png\",\"title\":\"KFC1 booking request\",\"directory_name\":\"KFC1\",\"directory_id\":9,\"type\":\"restaurant\"}',4,1,'2020-10-20 11:09:52','2020-10-20 11:12:29',0),(9,2,'{\"username\":\"Samuel Lima\",\"user_image\":\"\\/img\\/users\\/Avatar.png\",\"title\":\"New package subscription\",\"body\":\"Samuel Lima has been subscribed a new package Special Pack\"}',9,1,'2020-10-23 06:27:03','2020-10-28 11:38:15',1),(10,2,'{\"username\":\"Samuel Lima\",\"user_image\":\"\\/img\\/users\\/Avatar.png\",\"title\":\"New package subscription\",\"body\":\"Samuel Lima has been subscribed a new package Basic\"}',9,1,'2020-10-23 06:43:05','2020-10-28 11:38:15',1),(11,5,'{\"title\":\"Directory deleted.\",\"body\":\"Testing Directory deleted  successful.\",\"directory_name\":\"Testing Directory\",\"directory_id\":1,\"thumb_image\":\"Backend\\/images\\/directory\\/fXMJmQJP6Ds9pfCp.jpg\"}',3,0,'2020-10-23 07:19:59','2020-10-23 07:19:59',0),(12,5,'{\"title\":\"Directory deleted.\",\"body\":\"Testing directory 2 deleted  successful.\",\"directory_name\":\"Testing directory 2\",\"directory_id\":2,\"thumb_image\":\"Backend\\/images\\/directory\\/t2JmK0IQ4r6La7WU.jpeg\"}',3,0,'2020-10-23 07:20:06','2020-10-23 07:20:06',0),(13,20,'{\"title\":\"Directory deleted.\",\"body\":\"City Inn deleted  successful.\",\"directory_name\":\"City Inn\",\"directory_id\":3,\"thumb_image\":\"Backend\\/images\\/directory\\/gQNoWQnCcyJzeOdp.jpg\"}',3,0,'2020-10-23 07:20:12','2020-10-23 07:20:12',0),(14,20,'{\"title\":\"Directory deleted.\",\"body\":\"KFC deleted  successful.\",\"directory_name\":\"KFC\",\"directory_id\":5,\"thumb_image\":\"Backend\\/images\\/directory\\/5vdOs2KroLnxgvm9.jpg\"}',3,0,'2020-10-23 07:21:07','2020-10-23 07:21:07',0),(15,5,'{\"title\":\"Directory deleted.\",\"body\":\"Tiger garden deleted  successful.\",\"directory_name\":\"Tiger garden\",\"directory_id\":8,\"thumb_image\":null}',3,0,'2020-10-23 07:21:32','2020-10-23 07:21:32',0),(16,20,'{\"title\":\"Directory deleted.\",\"body\":\"KFC1 deleted  successful.\",\"directory_name\":\"KFC1\",\"directory_id\":9,\"thumb_image\":\"Backend\\/images\\/directory\\/ovOmtff26Rncbrul.jpg\"}',3,0,'2020-10-23 07:21:37','2020-10-23 07:21:37',0),(17,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Almenda Lidia added Mexican Spicy Restaurant to wishing\"}',6,1,'2020-10-23 08:53:31','2020-10-23 12:48:37',0),(18,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Removed from wishing.\",\"body\":\"Almenda Lidia removed Mexican Spicy Restaurant from wishing\"}',6,0,'2020-10-23 08:53:32','2020-10-23 08:53:32',0),(19,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Almenda Lidia added Mexican Spicy Restaurant to wishing\"}',6,0,'2020-10-23 08:53:39','2020-10-23 08:53:39',0),(20,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"New booking request\",\"body\":\"Almenda Lidia sent a booking request for Mexican Spicy Restaurant\",\"directory_name\":\"Mexican Spicy Restaurant\",\"directory_id\":12,\"type\":\"restaurant\"}',4,0,'2020-10-23 08:58:05','2020-10-23 08:58:05',0),(21,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Booking message\",\"body\":\"Almenda Lidia sent you a message\",\"message\":\"Hello\",\"type\":\"restaurant\"}',8,0,'2020-10-23 09:51:04','2020-10-23 09:51:04',0),(22,19,'{\"username\":\"Shafi Felipno\",\"user_image\":\"\\/img\\/users\\/Avatar.png\",\"title\":\"Booking message\",\"body\":\"Shafi Felipno sent you a message\",\"message\":\"Thank you.\",\"type\":\"restaurant\"}',8,1,'2020-10-23 10:27:00','2020-11-02 09:58:21',1),(23,2,'{\"title\":\" New directory create request\",\"body\":\"Samuel Lima wants to create a new directory Hotel Royel\",\"name\":\"Samuel Lima\",\"user_image\":\"Backend\\/images\\/users\\/FEkStOLCHC5tzTj6.png\",\"directory_name\":\"Hotel Royel\",\"directory_id\":11,\"thumb_image\":null}',5,1,'2020-10-23 11:27:49','2020-10-28 11:38:15',1),(24,26,'{\"title\":\"Directory verified\",\"body\":\"Hotel Royel verified successful.\",\"directory_name\":\"Hotel Royel\",\"directory_id\":11,\"thumb_image\":\"Backend\\/images\\/directory\\/9evqb8AjXQkR5dZZ.jpg\"}',3,0,'2020-10-23 11:41:42','2020-10-23 11:41:42',0),(25,26,'{\"title\":\"Directory active.\",\"body\":\"Hotel Royel make as active successful.\",\"directory_name\":\"Hotel Royel\",\"directory_id\":11,\"thumb_image\":\"Backend\\/images\\/directory\\/9evqb8AjXQkR5dZZ.jpg\"}',3,0,'2020-10-23 11:42:10','2020-10-23 11:42:10',0),(26,2,'{\"title\":\" New directory create request\",\"body\":\"Shafi Felipno wants to create a new directory Sam Shop\",\"name\":\"Shafi Felipno\",\"user_image\":\"\\/img\\/users\\/Avatar.png\",\"directory_name\":\"Sam Shop\",\"directory_id\":14,\"thumb_image\":\"Backend\\/images\\/directory\\/kgUEIV8UybgWmG91.jpg\"}',5,1,'2020-10-23 13:02:35','2020-10-28 11:38:15',1),(27,2,'{\"title\":\" New directory create request\",\"body\":\"Samuel Lima wants to create a new directory Fitness First\",\"name\":\"Samuel Lima\",\"user_image\":\"Backend\\/images\\/users\\/FEkStOLCHC5tzTj6.png\",\"directory_name\":\"Fitness First\",\"directory_id\":17,\"thumb_image\":\"Backend\\/images\\/directory\\/ZZkCQfhnaOfwi7qw.jpg\"}',5,1,'2020-10-23 13:22:15','2020-10-28 11:38:15',1),(28,22,'{\"title\":\"Directory verified\",\"body\":\"Sam Shop verified successful.\",\"directory_name\":\"Sam Shop\",\"directory_id\":14,\"thumb_image\":\"Backend\\/images\\/directory\\/kgUEIV8UybgWmG91.jpg\"}',3,0,'2020-10-23 13:22:45','2020-10-23 13:22:45',0),(29,26,'{\"title\":\"Directory verified\",\"body\":\"Fitness First verified successful.\",\"directory_name\":\"Fitness First\",\"directory_id\":17,\"thumb_image\":\"Backend\\/images\\/directory\\/ZZkCQfhnaOfwi7qw.jpg\"}',3,0,'2020-10-23 13:22:58','2020-10-23 13:22:58',0),(30,26,'{\"title\":\"Directory active.\",\"body\":\"Fitness First make as active successful.\",\"directory_name\":\"Fitness First\",\"directory_id\":17,\"thumb_image\":\"Backend\\/images\\/directory\\/ZZkCQfhnaOfwi7qw.jpg\"}',3,0,'2020-10-23 13:23:15','2020-10-23 13:23:15',0),(31,22,'{\"title\":\"Directory active.\",\"body\":\"Sam Shop make as active successful.\",\"directory_name\":\"Sam Shop\",\"directory_id\":14,\"thumb_image\":\"Backend\\/images\\/directory\\/kgUEIV8UybgWmG91.jpg\"}',3,0,'2020-10-23 13:23:25','2020-10-23 13:23:25',0),(32,22,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Sofia Cavalcanti added Sam Shop to wishing\"}',6,0,'2020-10-26 13:53:04','2020-10-26 13:53:04',0),(33,2,'{\"title\":\" New directory create request\",\"body\":\"Shafi Felipno wants to create a new directory Jamuna Shopping Complex\",\"name\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"directory_name\":\"Jamuna Shopping Complex\",\"directory_id\":16,\"thumb_image\":\"Backend\\/images\\/directory\\/5ITm3c5uTrMAHyir.jpg\"}',5,1,'2020-10-26 14:30:15','2020-10-28 11:38:15',1),(34,22,'{\"username\":\"Sujoy Nath\",\"user_image\":\"Backend\\/images\\/users\\/gBZGm4aXfgo6d6aK.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Admin added Jamuna Shopping Complex to wishing\"}',6,0,'2020-10-26 15:13:45','2020-10-26 15:13:45',0),(35,22,'{\"title\":\"Directory verified\",\"body\":\"Jamuna Shopping Complex verified successful.\",\"directory_name\":\"Jamuna Shopping Complex\",\"directory_id\":16,\"thumb_image\":\"Backend\\/images\\/directory\\/5ITm3c5uTrMAHyir.jpg\"}',3,0,'2020-10-26 15:23:30','2020-10-26 15:23:30',0),(36,22,'{\"title\":\"Directory not featured .\",\"body\":\"Jamuna Shopping Complex make as not featured  successful.\",\"directory_name\":\"Jamuna Shopping Complex\",\"directory_id\":16,\"thumb_image\":\"Backend\\/images\\/directory\\/5ITm3c5uTrMAHyir.jpg\"}',3,0,'2020-10-26 15:23:44','2020-10-26 15:23:44',0),(37,22,'{\"title\":\"Directory active.\",\"body\":\"Jamuna Shopping Complex make as active successful.\",\"directory_name\":\"Jamuna Shopping Complex\",\"directory_id\":16,\"thumb_image\":\"Backend\\/images\\/directory\\/5ITm3c5uTrMAHyir.jpg\"}',3,0,'2020-10-26 15:23:53','2020-10-26 15:23:53',0),(38,22,'{\"title\":\"Directory featured.\",\"body\":\"Jamuna Shopping Complex make as featured successful.\",\"directory_name\":\"Jamuna Shopping Complex\",\"directory_id\":16,\"thumb_image\":\"Backend\\/images\\/directory\\/5ITm3c5uTrMAHyir.jpg\"}',3,0,'2020-10-26 15:24:59','2020-10-26 15:24:59',0),(39,22,'{\"title\":\"Directory pending\",\"body\":\"Jamuna Shopping Complex make as pending successful.\",\"directory_name\":\"Jamuna Shopping Complex\",\"directory_id\":16,\"thumb_image\":\"Backend\\/images\\/directory\\/5ITm3c5uTrMAHyir.jpg\"}',3,0,'2020-10-26 15:25:25','2020-10-26 15:25:25',0),(40,2,'{\"username\":\"Almeida Ana\",\"title\":null,\"body\":\"New message from spotlisting user\",\"message\":\"Thank you\"}',10,1,'2020-10-26 15:31:59','2020-10-28 11:38:15',1),(41,22,'{\"title\":\"Directory active.\",\"body\":\"Jamuna Shopping Complex make as active successful.\",\"directory_name\":\"Jamuna Shopping Complex\",\"directory_id\":16,\"thumb_image\":\"Backend\\/images\\/directory\\/5ITm3c5uTrMAHyir.jpg\"}',3,0,'2020-10-26 15:34:30','2020-10-26 15:34:30',0),(42,26,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Sofia Cavalcanti added Exclusive Parlor to wishing\"}',6,0,'2020-10-26 16:57:12','2020-10-26 16:57:12',0),(43,26,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"New booking request\",\"body\":\"Sofia Cavalcanti sent a booking request for Exclusive Parlor\",\"directory_name\":\"Exclusive Parlor\",\"directory_id\":15,\"type\":\"beauty\"}',4,0,'2020-10-26 17:00:32','2020-10-26 17:00:32',0),(44,2,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"New booking request\",\"body\":\"Sofia Cavalcanti sent a booking request for Hotel Cityinn\",\"directory_name\":\"Hotel Cityinn\",\"directory_id\":13,\"type\":\"hotel\"}',4,1,'2020-10-26 20:20:20','2020-10-28 11:38:15',1),(45,22,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"New booking request\",\"body\":\"Sofia Cavalcanti sent a booking request for Mexican Spicy Restaurant\",\"directory_name\":\"Mexican Spicy Restaurant\",\"directory_id\":12,\"type\":\"restaurant\"}',4,0,'2020-10-26 20:22:33','2020-10-26 20:22:33',0),(46,22,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"New booking request\",\"body\":\"Sofia Cavalcanti sent a booking request for Mexican Spicy Restaurant\",\"directory_name\":\"Mexican Spicy Restaurant\",\"directory_id\":12,\"type\":\"restaurant\"}',4,1,'2020-10-26 20:22:55','2020-10-26 21:33:30',0),(47,2,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Sofia Cavalcanti added Gold Plaza to wishing\"}',6,1,'2020-10-26 20:26:20','2020-10-28 11:38:15',1),(48,26,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"New booking request\",\"body\":\"Sofia Cavalcanti sent a booking request for Fitness First\",\"directory_name\":\"Fitness First\",\"directory_id\":17,\"type\":\"fitness\"}',4,0,'2020-10-26 20:50:03','2020-10-26 20:50:03',0),(49,28,'{\"directory_name\":\"Mexican Spicy Restaurant\",\"directory_image\":\"Backend\\/images\\/directory\\/JcWV1hHP627mjGTx.jpg\",\"title\":\"Mexican Spicy Restaurant Booking request approved.\",\"body\":\"Your booking request has been approved.\",\"message\":\"ok\"}',2,1,'2020-10-26 21:33:56','2020-10-26 21:46:54',1),(50,28,'{\"directory_name\":\"Mexican Spicy Restaurant\",\"directory_image\":\"Backend\\/images\\/directory\\/JcWV1hHP627mjGTx.jpg\",\"title\":\"Mexican Spicy Restaurant Booking completed.\",\"body\":\"Thanks! your booking has been completed successfully.\",\"message\":\"ok\"}',2,1,'2020-10-26 21:34:08','2020-10-26 21:46:54',1),(51,28,'{\"directory_name\":\"Mexican Spicy Restaurant\",\"directory_image\":\"Backend\\/images\\/directory\\/JcWV1hHP627mjGTx.jpg\",\"title\":\"Mexican Spicy Restaurant Booking request approved.\",\"body\":\"Your booking request has been approved.\",\"message\":null}',2,1,'2020-10-26 21:34:20','2020-10-26 21:46:54',1),(52,28,'{\"directory_name\":\"Hotel Cityinn\",\"directory_image\":\"Backend\\/images\\/directory\\/gEEioTv68WKTdeIM.jpg\",\"title\":\"Hotel Cityinn Booking completed.\",\"body\":\"Thanks! your booking has been completed successfully.\",\"message\":\"yes\"}',2,1,'2020-10-26 21:35:19','2020-10-26 21:46:54',1),(53,28,'{\"username\":\"Admin\",\"user_image\":\"Backend\\/images\\/users\\/gBZGm4aXfgo6d6aK.jpg\",\"title\":\"Booking message\",\"body\":\"Admin sent you a message\",\"message\":\"ok\",\"type\":\"hotel\"}',8,1,'2020-10-26 21:35:28','2020-10-26 21:46:54',1),(54,28,'{\"username\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"title\":\"Booking message\",\"body\":\"Shafi Felipno sent you a message\",\"message\":\"yes\",\"type\":\"restaurant\"}',8,1,'2020-10-26 21:37:38','2020-10-26 21:46:54',1),(55,28,'{\"directory_name\":\"Mexican Spicy Restaurant\",\"directory_image\":\"Backend\\/images\\/directory\\/JcWV1hHP627mjGTx.jpg\",\"title\":\"Mexican Spicy Restaurant Booking request approved.\",\"body\":\"Your booking request has been approved.\",\"message\":null}',2,1,'2020-10-26 21:38:48','2020-10-26 21:46:54',1),(56,28,'{\"directory_name\":\"Mexican Spicy Restaurant\",\"directory_image\":\"Backend\\/images\\/directory\\/JcWV1hHP627mjGTx.jpg\",\"title\":\"Mexican Spicy Restaurant Booking request declined.\",\"body\":\"Sorry! your booking request has been declined. Please contact with admin.\",\"message\":null}',2,1,'2020-10-26 21:39:18','2020-10-26 21:46:54',1),(57,28,'{\"username\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"title\":\"Booking message\",\"body\":\"Shafi Felipno sent you a message\",\"message\":\"ok\",\"type\":\"restaurant\"}',8,1,'2020-10-26 21:40:45','2020-10-26 21:46:54',1),(58,2,'{\"username\":\"Samuel Silva\",\"user_image\":\"\\/img\\/users\\/Avatar.png\",\"title\":\"New package subscription\",\"body\":\"Samuel Silva has been subscribed a new package Special Pack\"}',9,1,'2020-10-27 07:04:11','2020-10-28 12:45:16',1),(59,2,'{\"title\":\" New directory create request\",\"body\":\"Samuel Silva wants to create a new directory KFC\",\"name\":\"Samuel Silva\",\"user_image\":\"\\/img\\/users\\/Avatar.png\",\"directory_name\":\"KFC\",\"directory_id\":20,\"thumb_image\":\"Backend\\/images\\/directory\\/GbxB7RA3bfp7AtDR.jpg\"}',5,1,'2020-10-27 07:12:24','2020-10-28 12:45:07',1),(60,2,'{\"title\":\" New directory create request\",\"body\":\"Shafi Felipno wants to create a new directory Mexican Spicy\",\"name\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"directory_name\":\"Mexican Spicy\",\"directory_id\":21,\"thumb_image\":null}',5,0,'2020-10-28 12:49:31','2020-10-28 12:49:31',0),(61,2,'{\"username\":\"Rebeca Dias\",\"user_image\":\"Backend\\/images\\/users\\/2IGly7bF38RPV9sg.jpg\",\"title\":\"New package subscription\",\"body\":\"Rebeca Dias has been subscribed a new package Special Pack\"}',9,1,'2020-10-28 13:03:12','2020-10-28 13:03:34',0),(62,2,'{\"title\":\" New directory create request\",\"body\":\"Rebeca Dias wants to create a new directory Top in town\",\"name\":\"Rebeca Dias\",\"user_image\":\"Backend\\/images\\/users\\/2IGly7bF38RPV9sg.jpg\",\"directory_name\":\"Top in town\",\"directory_id\":22,\"thumb_image\":null}',5,1,'2020-10-28 13:23:27','2020-10-28 13:24:34',0),(63,2,'{\"username\":\"Ana Lidia\",\"title\":\"Problem\",\"body\":\"New message from spotlisting user\",\"message\":\"I need help\"}',10,1,'2020-10-28 13:49:24','2020-10-28 14:20:14',0),(64,2,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"New booking request\",\"body\":\"Almenda Lidia sent a booking request for Hotel Cityinn\",\"directory_name\":\"Hotel Cityinn\",\"directory_id\":13,\"type\":\"restaurant\"}',4,0,'2020-10-28 14:25:32','2020-10-28 14:25:32',0),(65,19,'{\"username\":\"Admin\",\"user_image\":\"Backend\\/images\\/users\\/gBZGm4aXfgo6d6aK.jpg\",\"title\":\"Booking message\",\"body\":\"Admin sent you a message\",\"message\":\"Thank you for check in\",\"type\":\"restaurant\"}',8,1,'2020-10-28 14:27:43','2020-11-02 09:58:21',1),(66,19,'{\"directory_name\":\"Hotel Cityinn\",\"directory_image\":\"Backend\\/images\\/directory\\/gEEioTv68WKTdeIM.jpg\",\"title\":\"Hotel Cityinn Booking request approved.\",\"body\":\"Your booking request has been approved.\",\"message\":null}',2,1,'2020-10-28 14:30:22','2020-11-02 09:58:21',1),(67,19,'{\"directory_name\":\"Hotel Cityinn\",\"directory_image\":\"Backend\\/images\\/directory\\/gEEioTv68WKTdeIM.jpg\",\"title\":\"Hotel Cityinn Booking request expired.\",\"body\":\"Sorry! your booking request has been expired.\",\"message\":null}',2,1,'2020-10-28 14:30:43','2020-11-02 09:58:21',1),(68,28,'{\"username\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"title\":\"Booking message\",\"body\":\"Shafi Felipno sent you a message\",\"message\":\"Thank ypou\",\"type\":\"restaurant\"}',8,0,'2020-10-28 19:18:02','2020-10-28 19:18:02',0),(69,28,'{\"username\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"title\":\"Booking message\",\"body\":\"Shafi Felipno sent you a message\",\"message\":\"ok\",\"type\":\"restaurant\"}',8,1,'2020-10-28 19:20:19','2020-10-29 07:40:32',0),(70,26,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"Booking message\",\"body\":\"Sofia Cavalcanti sent you a message\",\"message\":\"Thank you\",\"type\":\"fitness\"}',8,0,'2020-10-29 07:17:23','2020-10-29 07:17:23',0),(71,2,'{\"username\":\"Salman Shafi\",\"title\":\"Need to talk\",\"body\":\"New message from spotlisting user\",\"message\":\"I need some help\"}',10,1,'2020-10-29 07:41:46','2020-10-29 09:53:13',0),(72,2,'{\"username\":\"Castro Luan\",\"user_image\":\"Backend\\/images\\/users\\/6cFJfjwHwYZlHL04.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Castro Luan added Gold Plaza to wishing\"}',6,0,'2020-10-29 07:43:08','2020-10-29 07:43:08',0),(73,2,'{\"title\":\" New directory create request\",\"body\":\"Rebeca Dias wants to create a new directory Fashion House\",\"name\":\"Rebeca Dias\",\"user_image\":\"Backend\\/images\\/users\\/2IGly7bF38RPV9sg.jpg\",\"directory_name\":\"Fashion House\",\"directory_id\":23,\"thumb_image\":null}',5,1,'2020-10-29 07:55:18','2020-10-29 07:55:56',0),(74,26,'{\"username\":\"Castro Luan\",\"user_image\":\"Backend\\/images\\/users\\/6cFJfjwHwYZlHL04.jpg\",\"title\":\"New booking request\",\"body\":\"Castro Luan sent a booking request for Exclusive Parlor\",\"directory_name\":\"Exclusive Parlor\",\"directory_id\":15,\"type\":\"beauty\"}',4,0,'2020-10-29 09:39:37','2020-10-29 09:39:37',0),(75,26,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"New booking request\",\"body\":\"Sofia Cavalcanti sent a booking request for Hotel Royel\",\"directory_name\":\"Hotel Royel\",\"directory_id\":11,\"type\":\"hotel\"}',4,0,'2020-10-29 10:06:31','2020-10-29 10:06:31',0),(76,31,'{\"username\":\"Sofia Cavalcanti\",\"user_image\":\"Backend\\/images\\/users\\/EX9BIheg146Jihn6.jpg\",\"title\":\"New booking request\",\"body\":\"Sofia Cavalcanti sent a booking request for Top in town\",\"directory_name\":\"Top in town\",\"directory_id\":22,\"type\":\"restaurant\"}',4,1,'2020-10-29 10:33:58','2020-10-29 10:34:19',0),(77,28,'{\"username\":\"Rebeca Dias\",\"user_image\":\"Backend\\/images\\/users\\/2IGly7bF38RPV9sg.jpg\",\"title\":\"Booking message\",\"body\":\"Rebeca Dias sent you a message\",\"message\":\"Thank you\",\"type\":\"restaurant\"}',8,0,'2020-10-29 10:34:38','2020-10-29 10:34:38',0),(78,28,'{\"directory_name\":\"Top in town\",\"directory_image\":\"Backend\\/images\\/directory\\/2tKHb4ouGxVhcXw3.jpg\",\"title\":\"Top in town Booking request declined.\",\"body\":\"Sorry! your booking request has been declined. Please contact with admin.\",\"message\":null}',2,0,'2020-10-29 10:34:44','2020-10-29 10:34:44',0),(79,2,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"New booking request\",\"body\":\"Almenda Lidia sent a booking request for Hotel Cityinn\",\"directory_name\":\"Hotel Cityinn\",\"directory_id\":13,\"type\":\"restaurant\"}',4,1,'2020-10-29 14:03:21','2020-10-29 14:03:36',0),(80,19,'{\"directory_name\":\"Hotel Cityinn\",\"directory_image\":\"Backend\\/images\\/directory\\/gEEioTv68WKTdeIM.jpg\",\"title\":\"Hotel Cityinn Booking request approved.\",\"body\":\"Your booking request has been approved.\",\"message\":null}',2,1,'2020-10-29 14:04:20','2020-11-02 09:58:21',1),(81,19,'{\"directory_name\":\"Hotel Cityinn\",\"directory_image\":\"Backend\\/images\\/directory\\/gEEioTv68WKTdeIM.jpg\",\"title\":\"Hotel Cityinn Booking request expired.\",\"body\":\"Sorry! your booking request has been expired.\",\"message\":null}',2,1,'2020-10-29 14:06:29','2020-11-02 09:58:21',1),(82,2,'{\"title\":\" New directory create request\",\"body\":\"Shafi Felipno wants to create a new directory Exclusive Parlor1\",\"name\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"directory_name\":\"Exclusive Parlor1\",\"directory_id\":24,\"thumb_image\":null}',5,0,'2020-10-29 14:23:58','2020-10-29 14:23:58',0),(83,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Almenda Lidia added Mexican Spicy to wishing\"}',6,0,'2020-11-02 07:48:40','2020-11-02 07:48:40',0),(84,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Removed from wishing.\",\"body\":\"Almenda Lidia removed Mexican Spicy from wishing\"}',6,0,'2020-11-02 07:52:27','2020-11-02 07:52:27',0),(85,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Almenda Lidia added Mexican Spicy to wishing\"}',6,0,'2020-11-02 07:52:42','2020-11-02 07:52:42',0),(86,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Removed from wishing.\",\"body\":\"Almenda Lidia removed Mexican Spicy from wishing\"}',6,0,'2020-11-02 07:53:36','2020-11-02 07:53:36',0),(87,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Almenda Lidia added Mexican Spicy to wishing\"}',6,0,'2020-11-02 07:53:58','2020-11-02 07:53:58',0),(88,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"New booking request\",\"body\":\"Almenda Lidia sent a booking request for Mexican Spicy\",\"directory_name\":\"Mexican Spicy\",\"directory_id\":21,\"type\":\"restaurant\"}',4,0,'2020-11-02 07:54:35','2020-11-02 07:54:35',0),(89,26,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Almenda Lidia added Exclusive Parlor to wishing\"}',6,0,'2020-11-02 10:23:37','2020-11-02 10:23:37',0),(90,31,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Almenda Lidia added Top in town to wishing\"}',6,0,'2020-11-03 05:42:41','2020-11-03 05:42:41',0),(91,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Removed from wishing.\",\"body\":\"Almenda Lidia removed Mexican Spicy from wishing\"}',6,0,'2020-11-03 06:35:41','2020-11-03 06:35:41',0),(92,22,'{\"username\":\"Almenda Lidia\",\"user_image\":\"Backend\\/images\\/users\\/k6drJ2svmyFRtNEQ.jpg\",\"title\":\"Added to wishing.\",\"body\":\"Almenda Lidia added Mexican Spicy to wishing\"}',6,0,'2020-11-03 06:38:56','2020-11-03 06:38:56',0),(93,2,'{\"title\":\" New directory create request\",\"body\":\"Rebeca Dias wants to create a new directory Le M\\u00e9ridien Dhaka\",\"name\":\"Rebeca Dias\",\"user_image\":\"Backend\\/images\\/users\\/2IGly7bF38RPV9sg.jpg\",\"directory_name\":\"Le M\\u00e9ridien Dhaka\",\"directory_id\":26,\"thumb_image\":null}',5,0,'2020-11-10 13:51:10','2020-11-10 13:51:10',0),(94,31,'{\"title\":\"Directory verified\",\"body\":\"Le M\\u00e9ridien Dhaka verified successful.\",\"directory_name\":\"Le M\\u00e9ridien Dhaka\",\"directory_id\":26,\"thumb_image\":\"Backend\\/images\\/directory\\/fXhpovoZAwG6A3No.jpg\"}',3,0,'2020-11-10 14:05:32','2020-11-10 14:05:32',0),(95,31,'{\"title\":\"Directory active.\",\"body\":\"Le M\\u00e9ridien Dhaka make as active successful.\",\"directory_name\":\"Le M\\u00e9ridien Dhaka\",\"directory_id\":26,\"thumb_image\":\"Backend\\/images\\/directory\\/fXhpovoZAwG6A3No.jpg\"}',3,0,'2020-11-10 14:05:38','2020-11-10 14:05:38',0),(96,31,'{\"title\":\"Directory verified\",\"body\":\"Fashion House verified successful.\",\"directory_name\":\"Fashion House\",\"directory_id\":23,\"thumb_image\":\"Backend\\/images\\/directory\\/EjojPztv7Sc0VF7j.jpg\"}',3,0,'2020-11-10 14:18:56','2020-11-10 14:18:56',0),(97,31,'{\"title\":\"Directory active.\",\"body\":\"Fashion House make as active successful.\",\"directory_name\":\"Fashion House\",\"directory_id\":23,\"thumb_image\":\"Backend\\/images\\/directory\\/EjojPztv7Sc0VF7j.jpg\"}',3,0,'2020-11-10 14:19:02','2020-11-10 14:19:02',0),(98,29,'{\"title\":\"Directory verified\",\"body\":\"KFC verified successful.\",\"directory_name\":\"KFC\",\"directory_id\":20,\"thumb_image\":\"Backend\\/images\\/directory\\/GbxB7RA3bfp7AtDR.jpg\"}',3,0,'2020-11-10 14:19:18','2020-11-10 14:19:18',0),(99,29,'{\"title\":\"Directory active.\",\"body\":\"KFC make as active successful.\",\"directory_name\":\"KFC\",\"directory_id\":20,\"thumb_image\":\"Backend\\/images\\/directory\\/GbxB7RA3bfp7AtDR.jpg\"}',3,0,'2020-11-10 14:19:26','2020-11-10 14:19:26',0),(100,26,'{\"title\":\"Directory deleted.\",\"body\":\"abc deleted  successful.\",\"directory_name\":\"abc\",\"directory_id\":19,\"thumb_image\":null}',3,0,'2020-11-19 10:35:04','2020-11-19 10:35:04',0),(101,2,'{\"title\":\" New directory create request\",\"body\":\"Shafi Felipno wants to create a new directory Hotel Castle Salam\",\"name\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"directory_name\":\"Hotel Castle Salam\",\"directory_id\":34,\"thumb_image\":null}',5,0,'2020-11-19 12:54:11','2020-11-19 12:54:11',0),(102,2,'{\"title\":\" New directory create request\",\"body\":\"Shafi Felipno wants to create a new directory Hotel Sea Crown\",\"name\":\"Shafi Felipno\",\"user_image\":\"Backend\\/images\\/users\\/w4dx5VZXykUXqXFL.jpg\",\"directory_name\":\"Hotel Sea Crown\",\"directory_id\":35,\"thumb_image\":null}',5,0,'2020-11-19 12:54:33','2020-11-19 12:54:33',0);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_blogs`
--

DROP TABLE IF EXISTS `spotlist_blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_blogs` (
  `spotlist_blogs_id` int unsigned NOT NULL AUTO_INCREMENT,
  `sys_users_id` bigint DEFAULT NULL,
  `spotlist_blogs_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spotlist_category_id` int DEFAULT NULL,
  `contents` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `blog_thumb_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_cover_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`spotlist_blogs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_blogs`
--

LOCK TABLES `spotlist_blogs` WRITE;
/*!40000 ALTER TABLE `spotlist_blogs` DISABLE KEYS */;
INSERT INTO `spotlist_blogs` VALUES (12,NULL,'The Best and Most Popular Food Articles of 2016','the-best-and-most-popular-food-articles-of-2016',3,'<p>\r\n that time of the year again when we look at our page views and time \r\ndevoted on our site by our viewers to put together a list of 10 of the \r\nmost engaging articles of 2016. These articles reflect various trends of\r\n the year, and from the looks of it, we can safely say that there seems \r\nto be a shift towards natural way of living. The popular themes this \r\nyear have been traditional home remedies for everyday ailments that \r\ncrossover with Ayurveda, eating seasonal fruits that are plump with \r\nessential nutrients, using age-old herbs that have stood the test of \r\ntime and proven to be effective time and again, and snacking on healthy \r\nnuts that are rich in natural fat, vitamins and minerals. These articles\r\n were well-received, are definitely worth your time and we hope you \r\nenjoy the read!</p><div class=\"ins_instory_dv_cont\"><strong>1. Why Soaked Almonds Are Better than Raw Almonds</strong></div><p>Time\r\n and again, our mothers have stressed on the importance of soaked \r\nalmonds. They\'re good for your hair, skin and brain they say. Some \r\nnutritionists come from the school of thought that raw almonds can get \r\nthe same job done. We finally disclose the myth.</p><p><span itemscope=\"\" itemtype=\"https://schema.org/NewsArticle\"><span itemprop=\"articleBody\"></span></span></p><h3><strong>Raw Almonds vs Soaked Almonds</strong></h3><p>If your mother pleaded with you every morning to eat your daily dose of <a href=\"https://food.ndtv.com/food-drinks/why-almonds-are-good-for-you-694280\" target=\"_blank\">soaked almonds</a>,\r\n then she may have been right. Choosing between soaked almonds and raw \r\nalmonds isn\'t just a matter of taste, it\'s about picking the healthier \r\noption.&nbsp; <br></p><p>Why soaked almonds are better - Firstly, the \r\nbrown peel of almonds contains tannin which inhibits nutrient \r\nabsorption. Once you soak almonds the peel comes off easily and allows \r\nthe nut to release all nutrients easily.</p><p>How to soak? Soak\r\n a handful of almonds in half a cup of water. Cover them and allow them \r\nto soak for 8 hours. Drain the water, peel off the skin and store them \r\nin plastic container. These soaked almonds will last you for about a \r\nweek.</p>','Backend/images/blogs/krHrcwvk9GZEYYp1.jpg','Backend/images/blogs/whMPdQNDfOZBqxU9.jpg',1,'Active','2020-11-09 13:57:43','2020-11-09 13:57:43'),(13,NULL,'These Are The Most Popular Restaurants In Koramangala','these-are-the-most-popular-restaurants-in-koramangala',3,'<p>No better place to do that than by visiting the many restaurants in \r\nKoramangala. Indeed, there are a lot of restaurants here; more than you \r\ncan probably count on a single day. We have gone ahead and selected few \r\nin terms of food, ambiance, décor, and vibe.</p>\r\n\r\n\r\n\r\n<h3>13 Best Restaurants In Koramangala</h3>\r\n\r\n\r\n\r\n<ol><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Chianti\">Chianti</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Rajdhani_Thali\">Rajdhani Thali</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#BonSouth\">BonSouth</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Onesta<\">Onesta</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Karama_Restaurant\">Karama Restaurant</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Anjappar\">Anjappar</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Sultans_of_Spice\">Sultans of Spice</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Koramangala_Social\">Koramangala Social</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Truffles\">Truffles</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Meghana_Foods\">Meghana Foods</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Nasi_and_Mee\">Nasi and Mee</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Kopper_Kadai\">Kopper Kadai</a></li><li><a href=\"https://www.oyorooms.com/travel-guide/restaurants-in-koramangala/#Root’s\">Root’s</a></li></ol>\r\n\r\n\r\n\r\n<h3 id=\"Chianti\" style=\"margin-top:-90px; padding-top:90px\">1. Chianti</h3>\r\n\r\n\r\n\r\n<p>Situated on 1st A Cross Road, 5th Block, Koramangala, Chianti is \r\nKoramangala’s hot favourite joint for people willing to indulge in \r\nauthentic Italian food. On weekends, the tables fill up pretty fast and \r\nyou may have hard luck finding a table without a reservation. Even a \r\ntable for two may not be forthcoming without prior reservation. Yet, if \r\nyou are in a mood to gorge on the finest bruschetta or pasta over some \r\nred wine, this is the place to be in. This is a great family restaurant \r\nwhere couples, as well as families, are at equal ease. This place is \r\nfamous for Spaghetti Aglio Olio, Pizza with Parma Ham, Eggplant \r\nParmesan, Fusilli Primavera, Pollo Ripieni, etc.</p>\r\n\r\n\r\n\r\n<h3 id=\"Rajdhani_Thali\" style=\"margin-top:-90px; padding-top:90px\">2. Rajdhani Thali</h3>\r\n\r\n\r\n\r\n<p>Located on no. 115, 1st Floor, Ganapathi rd., Koramangala, 7th Block,\r\n Rajdhani Thali is a hot favourite family restaurant for vegetarian food\r\n lovers. People with a cosmopolitan palette also visit this place for a \r\nchange of taste. If you are craving for some pure vegetarian food, this \r\nis the place to be in. You can savour a large variety of vegetarian \r\ndishes in the thali served at this veg restaurant. The comfortable \r\nseating arrangement lets you enjoy the meals and the ambiance. The meals\r\n literally keep coming in from starters to desserts. The thalis served \r\nhere cover an entire meal. Plates like the Khichdi thali are a favourite\r\n with people visiting Rajdhani Thali Restaurant.</p>\r\n\r\n\r\n\r\n<h3 id=\"BonSouth\" style=\"margin-top:-90px; padding-top:90px\">3. BonSouth</h3>\r\n\r\n\r\n\r\n<p>Located on 130, 1st Cross, Jyoti Nivas College Road, Koramangala, \r\nBonSouth is Koramangala’s offering in the bouquet of south Indian \r\nnon-veg family restaurant. In this non-veg restaurant, you can get an \r\narray of South Indian non-vegetarian dishes that you can enjoy while \r\nsitting in the outdoor arena. There is an open kitchen in this \r\nrestaurant, which lets you have a peep into the preparation and the \r\ningredients. You can get everything starting from appetizers to \r\ndesserts. The main course is a sumptuous affair with a range of vadas, \r\ndahi vada, fried items, Mutton Patties, Chicken Grilled with a fare of \r\nSouth Indian Spices, a range of mutton, chicken, and sea fish curries \r\ngradually unfold over the open kitchen. You can also taste their \r\nbiryanis. A range of desserts or a cup of typical South Indian filter \r\ncoffee can help you gulp down the rest of the bits.</p>\r\n\r\n\r\n\r\n<h3 id=\"Onesta<\" style=\"margin-top:-90px; padding-top:90px\">4. Onesta</h3>\r\n\r\n\r\n\r\n<p>Onesta, the Italian theme restaurant, is located on plot no 562, 8th \r\nMain road, Next To Exide Battery Showroom. It is indeed an honest effort\r\n at satiating the cosmopolitan palette of a cosmopolitan megacity. Here,\r\n you can get appetizing Italian food, and this is what makes this place \r\nquite popular, especially with young boys, girls, and couples. The \r\npasta, pizza, and other European cuisines available here carry the \r\nauthentic European flavour. After a soulful meal, you need to try their \r\nsinful desserts like Tiramisu. Some of the most popular items of this \r\njoint are Bacon Pizza, Gourmet Pizza, Prawn Pizza, etc. With an outdoor \r\nseating arrangement in an Italian-themed ambiance, the restaurant is a \r\nnice family restaurant in Koramangala.</p>\r\n\r\n\r\n\r\n<h3 id=\"Karama_Restaurant\" style=\"margin-top:-90px; padding-top:90px\">5. Karama Restaurant</h3>\r\n\r\n\r\n\r\n<p>On the fifth floor of plot no. 103, in the 5th Block near Jyothi \r\nNivas College, Koramangala sits Karama restaurant. It is one restaurant \r\nin Koramangala where you can have a blend of cuisine from Punjab, Arab, \r\nand Karachi under one roof. The dining arrangement of this restaurant is\r\n opulent. It is not just the spread of items on offer, but also the \r\ndécor that matches the origin of the cuisine. In essence, it is a great \r\ncombination of the right vibe and right food that helps foodies gulp \r\ndown all the chickens, muttons, and biryanis with ease. It has not been \r\nlong since this restaurant has opened. However, within this short span \r\nof time, it has carved out a niche for itself. It is also a perfect \r\nplace for private dining in a family restaurant.</p>\r\n\r\n\r\n\r\n<h3 id=\"Anjappar\" style=\"margin-top:-90px; padding-top:90px\">6. Anjappar</h3>\r\n\r\n\r\n\r\n<p>Located near the BDA Complex in Koramangala, Anjappar Restaurant is \r\nthe destination for the city dwellers who cannot think of eating \r\nvegetarian. It is a South Indian theme restaurant known for its \r\nChettinad preparations, which are famous for spice. Indeed, the spice \r\nlevel is pretty high in the preparations here, but they are also very \r\ntasty. It is one of the oldest restaurants here in Koramangala. Here you\r\n can have a unique dining experience with a platter of food dipped in \r\nhome-made spices. Some of the most famous items here are the various \r\nchicken starters, tandoori chicken, Pepper Chicken, Chettinad Thali, \r\nCrab Soup, etc. There is outdoor seating arrangement in the restaurant, \r\nbut you can also order home delivery.</p>\r\n\r\n\r\n\r\n<h3 id=\"Sultans_of_Spice\" style=\"margin-top:-90px; padding-top:90px\">7. Sultans of Spice</h3>\r\n\r\n\r\n\r\n<p>On Jyoti Nivas College Road in the 5th Block, Koramangala you can get\r\n Sultans Of Spice; a theme restaurant that can make you travel back in \r\ntime to the period of the Delhi Sultanates. The array of items here is \r\nmeant to bear the authentic flavours from the period of sultans. There \r\nare both vegetarian and non-vegetarian options. With a platter of \r\nkebabs, and a range of chicken, mutton and seafood options, this place \r\nis a heaven for people who love spicy non-vegetarian food. A delectable \r\nspread of spicy and meaty items like Dahi Kebab, Sultani Platter, Shahi \r\nTawa Nalli, Rumwaali Masaledaar Chops, etc. make this one of the most \r\nsought after dining destinations in Koramangala.</p>\r\n\r\n\r\n\r\n<h3 id=\"Koramangala_Social\" style=\"margin-top:-90px; padding-top:90px\">8. Koramangala Social</h3>\r\n\r\n\r\n\r\n<p>Situated on the third floor of 118, Koramangala Industrial Area, \r\nOpposite ICICI Bank, is Koramangala Social, a place where the city’s \r\nparty animals hang out over weekends. It is not just a place to have \r\nsome great food; but also a place where you can take a break and let \r\nyour hair down. This is a place where you can get a splendid combination\r\n of sumptuous food and enchanting music. It is also one of the most \r\npopular co-working spaces in the country and a happening place at night.\r\n Here you can have Paneer Tikka, Pork Ribs, Chicken Biryani, Chilli \r\nChicken, LIIT, etc.</p>\r\n\r\n\r\n\r\n<h3 id=\"Truffles\" style=\"margin-top:-90px; padding-top:90px\">9. Truffles</h3>\r\n\r\n\r\n\r\n<p>Situated on the ground floor of Apex Building, 5th Block, Industrial \r\nArea, Koramangala, Truffles is known for serving a delicious array of \r\nburgers. It is not a dining restaurant per se; it just offers comfort \r\nfood. However, the array of comfort food like burgers on offer is simply\r\n mouth-watering. It is a restaurant-cum-cafe where you can get some \r\nlip-smacking shakes and delicious fast food. Despite not being a \r\nwholesome dining restaurant, it can tickle your taste buds with a \r\nplatter of food. Indeed, it may leave you craving for more. Stuff like \r\nLamb Burger, Devil’s Chicken, Chicken Diane, Cheese Burst Lamb Burger, \r\nChicken Cordon Bleu, etc. is very popular with people in Bangalore.</p>\r\n\r\n\r\n\r\n<h3 id=\"Meghana_Foods\" style=\"margin-top:-90px; padding-top:90px\">10. Meghana Foods</h3>\r\n\r\n\r\n\r\n<p>On the 1st Cross of KHB Colony, Koramangala you can find Meghana \r\nFoods, one of the most popular eating joints for people who just cannot \r\ndo without non-vegetarian food. It is a popular family restaurant. Here,\r\n you can have a wholesome non-vegetarian meal at a cost that is less \r\nthan many of the famous food joints of Bangalore. Meghana Foods has a \r\nchain of restaurants across the city, and the Koramangala unit has been \r\nestablished in 2006. So even if you are not in Koramangala, you can \r\nenjoy plates from Meghana Foods. The restaurant is also a favourite with\r\n corporates for their lunch requirements. Since the price of the dishes \r\nare less compared to other places, you can find a slew of college \r\nstudents’, with their friends or boyfriends in tow, satiating their \r\nhunger pangs as well as chatting amongst themselves. The restaurant \r\nboasts of a variety of biryanis, which make this place a favourite \r\ndestination for young couples, boys, and girls. Mutton Biryani and \r\nChicken Boneless Biryani are two of the most popular dishes here.</p>\r\n\r\n\r\n\r\n<h3 id=\"Nasi_and_Mee\" style=\"margin-top:-90px; padding-top:90px\">11. Nasi and Mee</h3>\r\n\r\n\r\n\r\n<p>Bangalore is a cosmopolitan city that hosts a number of people from \r\nall over the world and the country. On 974, Koramangala 4th Block lies \r\nNasi and Mee, a place that has been catering to the cosmopolitan taste \r\nbuds of inhabitants of Bangalore and people coming here from all over \r\nthe world and India. This South East Asia theme restaurant showcases the\r\n quintessential flavours of South East Asia. With a spread of items \r\ncrafted in Singapore, the restaurant is helping people partake in the \r\nflavours of South East Asia through its array of noodles, dim sums, \r\ncurries, and others. The literal meaning of Nasi and Mee is rice and \r\nnoodles. The range of items available here has evolved from the cuisine \r\ncommon to countries like Singapore, Philippines, Vietnam, Thailand, etc.\r\n The irresistible array of flavours and tastes makes this a must visit <a href=\"https://www.oyorooms.com/travel-guide/best-restaurants-in-bangalore/\">theme restaurant in Bangalore</a>.\r\n Items like Shitake Mushrooms, Pad Thai, Nasi Goreng, Crispy Corn are \r\npopular here. You also have the option of gulping down the delicacies \r\nwith some exquisite wine.</p>\r\n\r\n\r\n\r\n<h3 id=\"Kopper_Kadai\" style=\"margin-top:-90px; padding-top:90px\">12. Kopper Kadai</h3>\r\n\r\n\r\n\r\n<p>Located on the Ground Floor of Cygnus Chambers, JNC Road, \r\nKoramangala, Kopper Kadai is popular for elaborate meals as well as some\r\n quick bites. The presentation of the platter is a unique aspect of this\r\n restaurant. Even the type of utensils used here showcases the thrust on\r\n creativity and presentation. The variety of food available here is also\r\n as amazing as the presentation and utensils. Whether it is the \r\npreparation of Laal Maas presented on a hammer, Galouti Kebab presented \r\nin an iron box or Ganna Chicken placed on sugarcane skewers, it creates a\r\n unique impression that also helps the gastric juice flowing. They have \r\nan item called Cooker Mein Kukkad where you have to take the food from a\r\n pressure cooker. This amazing variety of food items and presentations \r\nhas made sure that it is known as a great family restaurant. You may \r\nhave to wait for a long time if you don’t make prior reservations. Some \r\nof the most popular items of this restaurant are Chicken Biryani, Mutton\r\n Kalmi, Ganna Chicken and Dahi Ke Kebab. This restaurant is also a \r\nfavourite option for corporate lunches.</p>\r\n\r\n\r\n\r\n<h3 id=\"Root’s\" style=\"margin-top:-90px; padding-top:90px\">13. Root’s</h3>\r\n\r\n\r\n\r\n<p>Located on 43/A, 1st Main, Jakkasandra Road, Koramangala, Roots’ is a\r\n private dining restaurant known for its platter of authentic North \r\nIndian cuisine. It is a non-vegetarian restaurant with an array of \r\ndelicious platters for those die-hard fans of north Indian cuisine that \r\nalso includes some popular vegetarian dishes like Dum Aloo and Chilli \r\nPaneer. Some of the popular items here are Kadhai Murgh, Palak Paneer, \r\nButter Chicken, Paneer Khurchan, etc. If there is a special event coming\r\n up such as marriage anniversary, book a table here since the place is \r\nquite crowded.</p>\r\n\r\n\r\n\r\n<p>The cosmopolitan culture of Bangalore has been aptly upheld by the \r\nplatter of cuisine available in the restaurants in Koramangala. You can \r\nhave a gala time roaming the streets of Koramangala and trying out \r\nsnacks, and lunch. If you are going out for dinner you are spoilt for \r\nchoice. Whether you need authentic South Indian Food, spicy North Indian\r\n Food or are craving for a more continental spread, restaurants in \r\nKoramangala have you covered. You just need to decide what you want to \r\neat and where to get that perfect blend of taste, flavour, and ambiance \r\nthat will tickle your taste buds and get the gastric juices flowing. \r\nKoramangala is not just the most happening place in Bangalore; it is \r\nalso the gourmet’s paradise and a destination for those looking to \r\nchoose the perfect family restaurant from a wide variety of restaurants \r\navailable.</p>','Backend/images/blogs/SsJAFjX8vu1sNymC.jpg','Backend/images/blogs/yABIMhj28QTuks5d.jpg',1,'Active','2020-11-09 14:03:52','2020-11-09 14:03:52'),(14,NULL,'Ten Foods That Are Super Healthy for Your Health','foods-that-are-super-healthy',12,'<h2><div><a name=\"fruit\"> 1–6: Fruits and berries</a></div></h2><p><a href=\"https://www.healthline.com/nutrition/is-fruit-good-or-bad-for-your-health\" class=\"content-link css-5r4717\">Fruits</a> and berries are among the world’s most popular health foods.</p><p>These sweet, nutritious foods are very easy to incorporate into your diet because they require little to no preparation.</p><h3>1. Apples</h3><p><a href=\"https://www.healthline.com/nutrition/10-health-benefits-of-apples\" class=\"content-link css-5r4717\">Apples</a>\r\n are high in fiber, vitamin C, and numerous antioxidants. They are very \r\nfilling and make the perfect snack if you find yourself hungry between \r\nmeals.</p><h3>2. Avocados</h3><p><a href=\"https://www.healthline.com/nutrition/12-proven-benefits-of-avocado\" class=\"content-link css-5r4717\">Avocados</a>\r\n are different than most fruits because they are loaded with healthy \r\nfats instead of carbs. Not only are they creamy and tasty but also high \r\nin fiber, potassium, and vitamin C.</p><h3>3. Bananas</h3><p><a href=\"https://www.healthline.com/nutrition/11-proven-benefits-of-bananas\" class=\"content-link css-5r4717\">Bananas</a> are among the world’s best sources of potassium. They’re also high in vitamin B6 and fiber, as well as convenient and portable.</p><h3>4. Blueberries</h3><p><a href=\"https://www.healthline.com/nutrition/10-proven-benefits-of-blueberries\" class=\"content-link css-5r4717\">Blueberries</a> are not only delicious but also among the most powerful sources of antioxidants in the world.</p><h3>5. Oranges</h3><p><a href=\"https://www.healthline.com/nutrition/citrus-fruit-benefits\" class=\"content-link css-5r4717\">Oranges</a> are well known for their vitamin C content. What’s more, they’re high in fiber and antioxidants.</p><h3>6. Strawberries</h3><p>Strawberries are highly nutritious and low in both carbs and calories.</p><p>They are loaded with vitamin C, fiber, and <a href=\"https://www.healthline.com/nutrition/manganese-benefits\" class=\"content-link css-5r4717\">manganese</a> and are arguably among the most delicious foods in existence.</p><h3>Other healthy fruits</h3><p>Other health fruits and berries include cherries, grapes, grapefruit, kiwifruit, <a href=\"https://www.healthline.com/nutrition/6-lemon-health-benefits\" class=\"content-link css-5r4717\">lemons</a>, mango, melons, olives, peaches, pears, pineapples, plums, and raspberries.</p><div class=\"css-0\"><span style=\"font-size:0;line-height:0\"></span><h2><div><a name=\"eggs\"> 7. Eggs</a></div></h2><p><a href=\"https://www.healthline.com/nutrition/10-proven-health-benefits-of-eggs\" class=\"content-link css-5r4717\">Eggs</a> are among the most nutritious foods on the planet.</p><p>They were previously demonized for being high in <a href=\"https://www.healthline.com/nutrition/how-many-eggs-should-you-eat\" class=\"content-link css-5r4717\">cholesterol</a>, but new studies show that they’re perfectly safe and healthy (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/16340654\" target=\"_blank\" rel=\"noopener noreferrer\" class=\"content-link css-5r4717\">1<span class=\"css-1mdvjzu icon-hl-trusted-source-after\"><span class=\"sro\">Trusted Source</span></span></a><span></span>, <a href=\"http://www.bmj.com/content/346/bmj.e8539\" target=\"_blank\" rel=\"noopener noreferrer\" class=\"content-link css-5r4717\">2<span class=\"css-1mdvjzu icon-hl-trusted-source-after\"><span class=\"sro\">Trusted Source</span></span></a><span></span>).</p><div class=\"css-20w1gi\"><span style=\"font-size: 0px; line-height: 0;\"></span><aside class=\"css-0\"></aside><span style=\"font-size: 0px; line-height: 0;\"></span></div></div><div class=\"css-0\"><span style=\"font-size:0;line-height:0\"></span><h2><div><a name=\"meat\"> 8–10: Meats</a></div></h2><p>Unprocessed, gently cooked meat is one of the most nutritious foods you can eat.</p><h3>8. Lean beef</h3><p>Lean beef is among the best sources of protein in existence and loaded with <a href=\"https://www.healthline.com/nutrition/11-healthy-iron-rich-foods\" class=\"content-link css-5r4717\">highly bioavailable iron</a>. Choosing the fatty cuts is fine if you’re on a <a href=\"https://www.healthline.com/nutrition/low-carb-diet-meal-plan-and-menu\" class=\"content-link css-5r4717\">low-carb diet</a>.</p><h3>9. Chicken breasts</h3><p>Chicken breast is low in fat and calories but extremely high in <a href=\"https://www.healthline.com/nutrition/how-much-protein-per-day\" class=\"content-link css-5r4717\">protein</a>. It’s a great source of many nutrients. Again, feel free to eat fattier cuts of chicken if you’re not eating that many carbs.</p><h3>10. Lamb</h3><p>Lambs are usually grass-fed, and their meat tends to be high in omega-3 fatty acids.</p></div><span style=\"font-size:0;line-height:0\"></span><h2><div><a name=\"nuts-and-seeds\"> 11–15: Nuts and seeds</a></div></h2><p>Despite being high in fat and calories, nuts and seeds may help you lose weight (<a href=\"https://www.ncbi.nlm.nih.gov/pubmed/24898229\" target=\"_blank\" rel=\"noopener noreferrer\" class=\"content-link css-5r4717\">3<span class=\"css-1mdvjzu icon-hl-trusted-source-after\"><span class=\"sro\">Trusted Source</span></span></a><span></span>, <a href=\"https://www.ncbi.nlm.nih.gov/pubmed/18296372\" target=\"_blank\" rel=\"noopener noreferrer\" class=\"content-link css-5r4717\">4<span class=\"css-1mdvjzu icon-hl-trusted-source-after\"><span class=\"sro\">Trusted Source</span></span></a><span></span>).</p><p>These\r\n foods are crunchy, filling, and loaded with important nutrients that \r\nmany people don’t get enough of, including magnesium and vitamin E.</p><p>They also require almost no preparation, so they’re easy to add to your routine.</p>','Backend/images/blogs/kGiOjaxaJACRuQZQ.jpg','Backend/images/blogs/Oo2YPyCXjuNbI3K9.jpg',1,'Active','2020-11-09 14:07:24','2020-11-09 14:12:23'),(15,NULL,'What Is Junk Food? Why Is It Bad For You?','what-is-junk-food-why-is-it-bad-for-you',12,'<p><span itemscope=\"\" itemtype=\"https://schema.org/NewsArticle\"><span itemprop=\"articleBody\">Picture\r\n this: a plate of hot French fries and a huge burger with hot cheese \r\noozing out of it, and a large serving of any aerated drink; looks like a\r\n treat! This calorie dense junk food does look mouthwatering, but is \r\nknown to be nutritionally poor. Junk food or fast food has become an \r\nincreasingly popular food choice to grab when on the go. Ideally, junk \r\nfoods are defined as processed foods with negligible nutrient value and \r\nare often high in salt, sugar and fat. But we often confuse fast foods \r\nwith junk foods. How are they different? Or what is junk food really ? \r\nWe have got the answers for you!<br><br><strong>What is junk food?</strong><br><br>Junk\r\n foods are processed foods consisting of high calories, but that is \r\nconsidered only as a broad umbrella. These foods are prepared in a way \r\nthat they look appealing and are enjoyable so you are chemically \r\nprogrammed to ask for more. According to Dr. Sunali Sharma, Dietician \r\n&amp; Nutritionist, Amandeep Hospital, \"Commercial products including \r\nbut not limited to salted snack foods, gum, candy, sugary desserts, \r\nfried fast food, and sweetened carbonated beverages that have little or \r\nno nutritional value but are high in calories, salt, and fats may be \r\nconsidered junk foods. Though not all fast foods are junk foods, but a \r\ngreat number of them are. For instance, a salad may be fast food, but is\r\n definitely not junk food. Some foods like burgers, pizzas, and tacos \r\nmay alternate between junk and healthy categories depending on the \r\ningredients, calories and process of manufacturing.\"</span></span></p>','Backend/images/blogs/LkvI2Cvlh3aY8xc6.jpg','Backend/images/blogs/lcWlsd4rRPAMaFO2.jpg',0,'Active','2020-11-09 14:09:44','2020-11-09 14:09:44'),(16,NULL,'How to Understand Instagram Insights for Your Business','how-to-understand-instagram-insights-for-your-business',1,'<p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">If you already use Instagram, you’ll know how satisfying it is to watch your followers and engagement grow.&nbsp;<a href=\"https://www.jimdo.com/blog/how-to-use-instagram-for-your-business-tips-trends-for-2019/\" style=\"color: rgb(0, 130, 140); box-shadow: none;\">In this post</a>, we got you set up with an Instagram Business account and explained how to use Instagram for your business—even if you’re a B2B brand.&nbsp;</p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">Now that you’re up and running, you can start optimizing your Instagram business profile so it generates new leads. Seeing your numbers change is a brilliant motivator—plus, increased engagement means more exposure for your brand. Great news, no matter what size your company is.&nbsp;</p><h2 style=\"margin-top: 3rem; margin-bottom: 1rem; font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-weight: 700; line-height: 44px; color: rgb(29, 43, 54); font-size: 32px; padding: 0px; background-color: rgb(250, 250, 250);\">What is Instagram Insights?</h2><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">Insights is Instagram’s native statistics-tracking feature—and it’s only available via the Instagram mobile app. You can use it to learn more about your visitors, what kind of content they engage with most, and analyze how and why some of your posts perform better than others. The really cool bit? You can use this info to adjust your strategy to give your audience more of what they want, and broaden your reach on Instagram.</p><div class=\"jimdo-alert\" style=\"width: 682.656px; padding: 1.5rem 0px 0.5rem 2rem; margin: 2rem 0px 2.5rem; border-top: 0px; border-right: 0px; border-bottom: 0px; border-image: initial; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); border-left: 8px solid rgb(174, 228, 232) !important;\"><p style=\"padding: 0px; margin-right: 0px !important; margin-bottom: 1rem !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Note:</span>&nbsp;To see the statistics and insights, you need to&nbsp;<a href=\"https://help.instagram.com/502981923235522?helpref=page_content\" style=\"color: rgb(0, 130, 140); box-shadow: none;\">create an Instagram business account</a>. It’s free.<em>&nbsp;</em></p></div><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">Instagram Insights are some of the easiest statistics to understand, because all of the numbers include a short explanation of exactly what they mean. So even if you’ve never looked into your performance stats before, you’ll be able to improve your results right away.</p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">You can read the more detailed definition for every metric on&nbsp;<a href=\"https://help.instagram.com/788388387972460\" style=\"color: rgb(0, 130, 140); box-shadow: none;\">Instagram’s help page</a>.&nbsp;</p><h3 style=\"margin-top: 2rem; margin-bottom: 1rem; font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-weight: 700; line-height: 38px; color: rgb(29, 43, 54); font-size: 22px; padding: 0px; background-color: rgb(250, 250, 250);\">What metrics are available on Instagram Insights?</h3><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">When you tap on the “Insights” tab in your Instagram app, you’ll see quite a few metrics just at first glance. This statistics area shows you a short, summarized version at first, and you can tap each heading for more details. Let’s take a closer look at Instagram Insights’ main categories:</p><ol style=\"margin-bottom: 1.5rem; border: 0px; outline: 0px; font-size: 18px; vertical-align: baseline; background: rgb(250, 250, 250); padding-left: 2rem; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\"><li style=\"margin: 12px 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;\"><span style=\"font-weight: 700;\">Recent Highlights.&nbsp;</span>Right at the top, Instagram tells you about any significant increase in your performance over the last 7 days and compares it to your data from the previous period. For example, “You received 134%+ more content interactions in the last 7 days.”&nbsp;</li></ol><ol start=\"2\" style=\"margin-bottom: 1.5rem; border: 0px; outline: 0px; font-size: 18px; vertical-align: baseline; background: rgb(250, 250, 250); padding-left: 2rem; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\"><li style=\"margin: 12px 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;\"><span style=\"font-weight: 700;\">Overview.&nbsp;</span>This section includes three important metrics:&nbsp;<span style=\"font-weight: 700;\">Accounts Reached</span>,&nbsp;<span style=\"font-weight: 700;\">Content Interactions</span>, and&nbsp;<span style=\"font-weight: 700;\">Total Followers</span>. From the main page, you can see the total number for each metric above its title, plus the percentage by which it has increased or decreased over the last 7 days. You can then tap them to get more information. We’ll talk more about these metrics below.</li></ol><ol start=\"3\" style=\"margin-bottom: 1.5rem; border: 0px; outline: 0px; font-size: 18px; vertical-align: baseline; background: rgb(250, 250, 250); padding-left: 2rem; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\"><li style=\"margin: 12px 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;\"><span style=\"font-weight: 700;\">Content You Shared.&nbsp;</span>Here you can see all the posts you’ve shared in your Instagram feed, in your Stories, and via IGTV (Instagram TV) in the last 7 days.</li></ol><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">The<span style=\"font-weight: 700;\">&nbsp;Overview&nbsp;</span>section is where you’ll find information like how many people have seen and interacted with your posts, what actions they took on your page, and how many followers you have overall—including any fluctuations in that number.&nbsp;</p><div class=\"jimdo-alert\" style=\"width: 682.656px; padding: 1.5rem 0px 0.5rem 2rem; margin: 2rem 0px 2.5rem; border-top: 0px; border-right: 0px; border-bottom: 0px; border-image: initial; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); border-left: 8px solid rgb(174, 228, 232) !important;\"><p style=\"padding: 0px; margin-right: 0px !important; margin-bottom: 1rem !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Tip:</span>&nbsp;Don’t get too bogged down in the number of followers you have. If your goal is to grow your business on Instagram, then engagement and interactions metrics—like how many people like, comment on, watch, reply, and save your content—are much more important.</p></div><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Accounts Reached</span>&nbsp;lets you see how well your content has performed by arranging your top-performing posts, stories, and videos by reach. Most importantly, you can check how many people have:</p><ul style=\"margin-bottom: 1.5rem; border: 0px; outline: 0px; font-size: 18px; vertical-align: baseline; background: rgb(250, 250, 250); padding-left: 2rem; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\"><li style=\"margin: 12px 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;\">Seen your content on Instagram.</li><li style=\"margin: 12px 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;\">Visited your profile or clicked on your website link.</li><li style=\"margin: 12px 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;\">Tapped the&nbsp;<a href=\"https://www.jimdo.com/blog/create-better-calls-to-action/\" style=\"color: rgb(0, 130, 140); box-shadow: none;\">call-to-action button</a>&nbsp;on your profile.</li></ul><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Why is reach such an important Instagram metric?</span></p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">Your “reach” refers to how many unique accounts see your content on Instagram, regardless of how many times they view it or if they interact with it or not.</p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">Reach shouldn’t be confused with “impressions,” which refers to the total number of times a piece of content has been displayed. For example, if you scroll past the same post twice in your feed, Instagram will count that as two distinct impressions, but only one instance of reach. If your goal is to build brand awareness, tracking reach is the best way to monitor your progress.</p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Content Interactions</span>&nbsp;shows you how people have interacted with your posts, stories, and videos in more detail. For example, you can see the likes, comments, shares, saves, and replies for individual posts. This metric is useful for seeing how people are reacting to your posts and which content types they like best.&nbsp;</p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Total Followers&nbsp;</span>offers more than you might think. Yes, it<span style=\"font-weight: 700;\">&nbsp;</span>shows you how many followers you have and if you’ve gained or lost any in the last 7 days. But you can also get useful information like their geographical location and age range,<span style=\"font-weight: 700;\">&nbsp;</span>and when they are most active on Instagram.<span style=\"font-weight: 700;\">&nbsp;</span>Checking this metric can help you work out when the best time is to post on Instagram or if you should try posting at a different time of day to get more interaction from your followers. This data also really comes in handy when you want to monitor overall activity. For example, to see how successful your latest competition or promotion was for promoting your page.</p><h2 style=\"margin-top: 3rem; margin-bottom: 1rem; font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-weight: 700; line-height: 44px; color: rgb(29, 43, 54); font-size: 32px; padding: 0px; background-color: rgb(250, 250, 250);\">How to check Insights on your Instagram posts, stories, and IGTV videos</h2><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">To see insights for a specific post, story, or video, go to the post tap “<span style=\"font-weight: 700;\">View Insights</span>.” This section is split into Interactions, Discovery, and Promotion.</p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Interactions</span></p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">In the&nbsp;<span style=\"font-weight: 700;\">Interactions</span>&nbsp;tab, you can see how many likes, comments, and saves your content has earned. Saves are especially insightful because they indicate that a follower is so interested in your content that they want to come back to it later or share it with a friend—in contrast to tapping ‘Like’ as they scroll past. Tracking the number of saves your content receives over time will help you work out what type of content resonates most with your audience, so you can focus on creating more of it.</p><div class=\"jimdo-alert\" style=\"width: 682.656px; padding: 1.5rem 0px 0.5rem 2rem; margin: 2rem 0px 2.5rem; border-top: 0px; border-right: 0px; border-bottom: 0px; border-image: initial; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); border-left: 8px solid rgb(174, 228, 232) !important;\"><p style=\"padding: 0px; margin-right: 0px !important; margin-bottom: 1rem !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Tip:&nbsp;</span>Try checking the interaction numbers on your latest story—how many people replied to it or tapped your stickers? If lots of viewers are leaving right away, consider adjusting the type of content you’re using to make your stories more interesting to your followers. If, on the other hand, you find that your followers are tapping back to return to your stories, this means you’ve managed to hook them with captivating content—nicely done!</p></div><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Discovery</span></p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">Alongside your post’s interaction numbers, you’ll also find the&nbsp;<span style=\"font-weight: 700;\">Discovery&nbsp;</span>metric. This shows you how many new accounts you’ve reached, and how you reached them. For example, if you notice that lots of new followers have found you via certain hashtags, you can use these hashtag combinations again to get more followers on Instagram.</p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\"><span style=\"font-weight: 700;\">Promotion</span></p><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-left: 0px !important;\">If you’ve paid to promote a piece of content, you’ll find information and statistics on how well it performs in the&nbsp;<span style=\"font-weight: 700;\">Promotion</span>&nbsp;tab.&nbsp;</p><div class=\"jimdo-alert\" style=\"width: 682.656px; padding: 1.5rem 0px 0.5rem 2rem; margin: 2rem 0px 2.5rem; border-top: 0px; border-right: 0px; border-bottom: 0px; border-image: initial; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); border-left: 8px solid rgb(174, 228, 232) !important;\"><p style=\"padding: 0px; margin-right: 0px !important; margin-bottom: 1rem !important; margin-left: 0px !important;\">Fancy being featured on Jimdo’s Instagram? We’d love to see your Insta-accounts and give them a shout-out in our newsletter and on our social channels.&nbsp;<a href=\"https://www.instagram.com/jimdo_official/\" target=\"_blank\" rel=\"noreferrer noopener\" style=\"color: rgb(0, 130, 140); box-shadow: none;\">Follow us on Instagram</a>&nbsp;and tag us in your posts and stories for your chance to get featured!</p></div><p style=\"padding: 0px; color: rgb(29, 43, 54); font-family: &quot;Brandon Text&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 18px; background-color: rgb(250, 250, 250); margin-right: 0px !important; margin-bottom: 1rem !important; margin-left: 0px !important;\">If you’re serious about growing your business with Instagram, then Insights will provide the concrete data you need to optimize your performance. I hope these tips will inspire you to give your brand an extra boost with Instagram marketing.<br><br>And once you’re on your way to Insta-fame,&nbsp;<a href=\"https://www.jimdo.com/blog/streamline-your-business-social-media/\" style=\"color: rgb(0, 130, 140); box-shadow: none;\">this strategy</a>&nbsp;will help you streamline your social media routine.</p>','Backend/images/blogs/20l5jcVZnWaGaDqo.jpg','Backend/images/blogs/wG9s47hQTW0j9HKW.jpg',1,'Active','2020-11-13 07:31:06','2020-11-13 07:31:06');
/*!40000 ALTER TABLE `spotlist_blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_booking_messages`
--

DROP TABLE IF EXISTS `spotlist_booking_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_booking_messages` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint unsigned NOT NULL,
  `from` bigint unsigned NOT NULL,
  `to` bigint unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_booking_messages`
--

LOCK TABLES `spotlist_booking_messages` WRITE;
/*!40000 ALTER TABLE `spotlist_booking_messages` DISABLE KEYS */;
INSERT INTO `spotlist_booking_messages` VALUES (1,17,19,22,'Hello',0,'2020-10-23 09:51:04','2020-10-23 09:51:04'),(2,17,22,19,'Thank you.',0,'2020-10-23 10:27:00','2020-10-23 10:27:00'),(3,19,2,28,'ok',0,'2020-10-26 21:35:28','2020-10-26 21:35:28'),(4,20,22,28,'yes',0,'2020-10-26 21:37:38','2020-10-26 21:37:38'),(5,20,22,28,'ok',0,'2020-10-26 21:40:45','2020-10-26 21:40:45'),(6,23,2,19,'Thank you for check in',0,'2020-10-28 14:27:43','2020-10-28 14:27:43'),(7,20,22,28,'Thank ypou',0,'2020-10-28 19:18:02','2020-10-28 19:18:02'),(8,20,22,28,'ok',0,'2020-10-28 19:20:19','2020-10-28 19:20:19'),(9,22,28,26,'Thank you',0,'2020-10-29 07:17:23','2020-10-29 07:17:23'),(10,26,31,28,'Thank you',0,'2020-10-29 10:34:38','2020-10-29 10:34:38');
/*!40000 ALTER TABLE `spotlist_booking_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_booking_request`
--

DROP TABLE IF EXISTS `spotlist_booking_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_booking_request` (
  `spotlist_booking_request_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_directory_id` int NOT NULL,
  `customer_id` bigint unsigned NOT NULL,
  `reservation_for` enum('hotel','restaurant','shop','beauty','fitness') COLLATE utf8mb4_unicode_ci NOT NULL,
  `reservation_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_guest` int DEFAULT NULL,
  `child_guest` int DEFAULT NULL,
  `request_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_start_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_end_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_note` text COLLATE utf8mb4_unicode_ci,
  `status_message` text COLLATE utf8mb4_unicode_ci,
  `message` json DEFAULT NULL,
  `spotlist_directory_type_hotel_room_id` int DEFAULT NULL,
  `spotlist_directory_type_restaurant_id` int DEFAULT NULL,
  `spotlist_directory_type_beauty_id` int DEFAULT NULL,
  `spotlist_directory_type_fitness_id` int DEFAULT NULL,
  `spotlist_directory_type_shop_id` int DEFAULT NULL,
  `status` enum('Requested','Confirmed','Approve','Declined','Expired','Completed','Canceled') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `booking_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_start_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`spotlist_booking_request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_booking_request`
--

LOCK TABLES `spotlist_booking_request` WRITE;
/*!40000 ALTER TABLE `spotlist_booking_request` DISABLE KEYS */;
INSERT INTO `spotlist_booking_request` VALUES (1,1,4,'restaurant','01771010093',2,1,'12.50PM','2020-10-13 15:20:22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Requested','2020-10-13 15:20:24','2020-10-13 15:20:22',NULL,NULL,NULL),(2,1,4,'restaurant','01771010093',2,1,'12.50PM','2020-10-13 15:20:22',NULL,NULL,NULL,'[{\"time\": \"2020-10-13T09:54:24.868051Z\", \"type\": \"user\", \"message\": \"Hello\"}]',NULL,NULL,NULL,NULL,NULL,'Canceled','2020-10-13 15:20:26','2020-10-13 08:52:11',NULL,NULL,NULL),(3,1,4,'restaurant','01771010093',2,1,'11.50 PM',NULL,NULL,'Some text will be written here',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Requested','2020-10-13 09:24:52','2020-10-13 09:24:52',NULL,NULL,NULL),(4,1,4,'restaurant','01771010093',2,2,'12.00 PM',NULL,NULL,'sdfsdf sdfdsf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Requested','2020-10-13 09:28:05','2020-10-13 09:28:05',NULL,NULL,NULL),(5,1,4,'restaurant','01771010093',2,1,'12.00 PM','10/02/2020',NULL,'sdf sdfdsgg dfddsf',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Requested','2020-10-13 09:32:20','2020-10-13 09:32:20',NULL,NULL,NULL),(6,2,4,'hotel','01771010093',2,1,NULL,'10/02/2020','10/10/2020','Some text will be written','You have successfully complete you booking. Thanks','[{\"time\": \"2020-10-13T10:44:16.159242Z\", \"type\": \"manager\", \"message\": \"You have successfully complete you booking. Thanks\"}]',1,NULL,NULL,NULL,NULL,'Completed','2020-10-13 10:40:48','2020-10-13 10:40:48',NULL,NULL,NULL),(7,2,18,'hotel','01771010093',2,1,NULL,'10/01/2020','10/09/2020','sdfsf',NULL,NULL,1,NULL,NULL,NULL,NULL,'Requested','2020-10-14 05:11:37','2020-10-14 05:11:37',NULL,NULL,NULL),(8,2,19,'hotel','Canada',2,0,NULL,'10/11/2020','10/14/2020',NULL,NULL,NULL,2,NULL,NULL,NULL,NULL,'Requested','2020-10-14 13:46:54','2020-10-14 13:46:54',NULL,NULL,NULL),(9,3,19,'hotel','New York',0,0,NULL,'2020-10-15','2020-10-17','Accept it fast',NULL,'[{\"time\": \"2020-10-15T12:41:20.848748Z\", \"type\": \"manager\", \"message\": \"You need to select at least 1 person\"}, {\"time\": \"2020-10-15T13:04:18.097948Z\", \"type\": \"user\", \"message\": \"Ok thank you\"}]',3,NULL,NULL,NULL,NULL,'Approve','2020-10-15 12:39:07','2020-10-15 12:39:07',NULL,NULL,NULL),(10,3,19,'hotel','Canada',1,1,NULL,'2020-10-14','2020-10-22','abc',NULL,NULL,3,NULL,NULL,NULL,NULL,'Requested','2020-10-16 07:46:26','2020-10-16 07:46:26',NULL,NULL,NULL),(11,6,19,'beauty','abc',NULL,NULL,'sdfsdf','10/11/2020',NULL,'asdads',NULL,NULL,NULL,NULL,1,NULL,NULL,'Approve','2020-10-16 12:21:54','2020-10-16 12:28:18',NULL,NULL,NULL),(12,6,19,'beauty','abc',NULL,NULL,'sdfsdf','10/11/2020',NULL,'asdads',NULL,NULL,NULL,NULL,1,NULL,NULL,'Canceled','2020-10-16 12:21:54','2020-10-16 12:28:28',NULL,NULL,NULL),(13,6,19,'beauty','abc',NULL,NULL,'asdas','10/11/2020',NULL,'asd',NULL,NULL,NULL,NULL,1,NULL,NULL,'Requested','2020-10-16 12:22:30','2020-10-16 12:22:30',NULL,NULL,NULL),(14,6,19,'beauty','acv',NULL,NULL,'sdfsdf','10/11/2020',NULL,'asd',NULL,NULL,NULL,NULL,1,NULL,NULL,'Requested','2020-10-16 12:29:28','2020-10-16 12:29:28',NULL,NULL,NULL),(15,6,19,'beauty','acv',NULL,NULL,'sdfsdf','10/11/2020',NULL,'asd',NULL,NULL,NULL,NULL,1,NULL,NULL,'Requested','2020-10-16 12:29:29','2020-10-16 12:29:29',NULL,NULL,NULL),(16,9,19,'restaurant','asd',0,0,'02:16','10/19/2020',NULL,'Avc',NULL,'[{\"time\": \"2020-10-20T11:12:07.295781Z\", \"type\": \"user\", \"message\": \"Hello\"}]',NULL,NULL,NULL,NULL,NULL,'Requested','2020-10-20 11:09:52','2020-10-20 11:09:52','BK2010200016',NULL,NULL),(17,12,19,'restaurant','avc',0,0,'00:15','10/16/2020',NULL,'Done',NULL,'[{\"time\": \"2020-10-23T09:51:04.757791Z\", \"type\": \"user\", \"message\": \"Hello\"}, {\"time\": \"2020-10-23T10:27:00.895131Z\", \"type\": \"manager\", \"message\": \"Thank you.\"}]',NULL,NULL,NULL,NULL,NULL,'Requested','2020-10-23 08:58:05','2020-10-23 08:58:05','BK2010230017',NULL,NULL),(18,15,28,'beauty','asd',NULL,NULL,'11:00','10/27/2020',NULL,'ok',NULL,NULL,NULL,NULL,2,NULL,NULL,'Requested','2020-10-26 17:00:32','2020-10-26 17:00:32','BK2010260018',NULL,NULL),(19,13,28,'hotel','asd',1,1,NULL,'2020-10-28','2020-10-28','ok','yes','[{\"time\": \"2020-10-26T21:35:19.283638Z\", \"type\": \"manager\", \"message\": \"yes\"}, {\"time\": \"2020-10-26T21:35:28.637124Z\", \"type\": \"manager\", \"message\": \"ok\"}]',5,NULL,NULL,NULL,NULL,'Completed','2020-10-26 20:20:20','2020-10-26 20:20:20','BK2010260019',NULL,NULL),(20,12,28,'restaurant','asc',2,1,'00:15','10/28/2020',NULL,NULL,NULL,'[{\"time\": \"2020-10-26T21:37:38.735570Z\", \"type\": \"manager\", \"message\": \"yes\"}, {\"time\": \"2020-10-26T21:40:45.226267Z\", \"type\": \"manager\", \"message\": \"ok\"}, {\"time\": \"2020-10-28T19:18:02.078644Z\", \"type\": \"manager\", \"message\": \"Thank ypou\"}, {\"time\": \"2020-10-28T19:20:19.183070Z\", \"type\": \"manager\", \"message\": \"ok\"}]',NULL,NULL,NULL,NULL,NULL,'Declined','2020-10-26 20:22:33','2020-10-26 20:22:33','BK2010260020',NULL,NULL),(21,12,28,'restaurant','123',1,1,'00:15','10/28/2020',NULL,NULL,'ok','[{\"time\": \"2020-10-26T21:33:56.339592Z\", \"type\": \"manager\", \"message\": \"ok\"}, {\"time\": \"2020-10-26T21:34:08.246894Z\", \"type\": \"manager\", \"message\": \"ok\"}]',NULL,NULL,NULL,NULL,NULL,'Completed','2020-10-26 20:22:55','2020-10-26 20:22:55','BK2010260021',NULL,NULL),(22,17,28,'fitness','1222',NULL,NULL,NULL,NULL,NULL,'done',NULL,'[{\"time\": \"2020-10-29T07:17:23.577411Z\", \"type\": \"user\", \"message\": \"Thank you\"}]',NULL,NULL,NULL,NULL,NULL,'Requested','2020-10-26 20:50:03','2020-10-26 20:50:03','BK2010260022',NULL,NULL),(23,13,19,'restaurant','123',0,0,NULL,'14:15',NULL,'120',NULL,'[{\"time\": \"2020-10-28T14:27:44.003358Z\", \"type\": \"manager\", \"message\": \"Thank you for check in\"}]',NULL,NULL,NULL,NULL,NULL,'Expired','2020-10-28 14:25:32','2020-10-28 14:30:43','BK2010280023','10/27/2020',NULL),(24,15,33,'beauty','23165165654654654654654654',NULL,NULL,NULL,'13:11','13:23','ok',NULL,NULL,NULL,NULL,2,NULL,NULL,'Requested','2020-10-29 09:39:37','2020-10-29 09:39:37','BK2010290024','10/29/2020',NULL),(25,11,28,'hotel','123564jhgjh',0,0,NULL,NULL,NULL,NULL,NULL,NULL,6,NULL,NULL,NULL,NULL,'Requested','2020-10-29 10:06:31','2020-10-29 10:06:31','BK2010290025','10/29/2020','10/29/2020'),(26,22,28,'restaurant','sdfg',1,1,NULL,'13:05',NULL,'sfg',NULL,'[{\"time\": \"2020-10-29T10:34:38.100446Z\", \"type\": \"manager\", \"message\": \"Thank you\"}]',NULL,NULL,NULL,NULL,NULL,'Declined','2020-10-29 10:33:58','2020-10-29 10:34:44','BK2010290026','10/29/2020',NULL),(27,13,19,'restaurant','12343',1,0,NULL,'01:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Expired','2020-10-29 14:03:21','2020-10-29 14:06:29','BK2010290027','10/29/2020',NULL),(28,21,19,'restaurant','256932565',1,1,NULL,'01:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Requested','2020-11-02 07:54:35','2020-11-02 07:54:35','BK2011020028','11/09/2020',NULL);
/*!40000 ALTER TABLE `spotlist_booking_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_category`
--

DROP TABLE IF EXISTS `spotlist_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_category` (
  `spotlist_category_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int NOT NULL DEFAULT '0',
  `category_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_category`
--

LOCK TABLES `spotlist_category` WRITE;
/*!40000 ALTER TABLE `spotlist_category` DISABLE KEYS */;
INSERT INTO `spotlist_category` VALUES (1,'General',0,'Backend/images/category/QnX8JIf0Swgn2Fxh.png',NULL,'Active'),(2,'Hotel',0,'Backend/images/category/KXeMEHmxvOn0J2mR.jpg','fa fa-shopping-cart','Active'),(3,'Restaurant',0,'Backend/images/category/LBRf0iyXAmSxQpNt.jpg','fa fa-female','Active'),(4,'Beauty',0,'Backend/images/category/YWeWe2HueUIfy8Rw.jpg','fa-fa-restaurant','Active'),(5,'Shopping',0,'Backend/images/category/EzwRO6BlYO3svgSH.jpg',NULL,'Active'),(6,'Fitness',0,'Backend/images/category/zoYWZMDjwkgzu85a.jpg',NULL,'Active'),(7,'Three star',2,'Backend/images/category/voazWKcTSLTD03Th.jpg',NULL,'Active'),(12,'Fast Food',3,'Backend/images/category/GrYhlZXFLEEBmfFO.jpg',NULL,'Active'),(13,'Accessories',0,'Backend/images/category/kRhKIm5bhKpD92nh.jpg',NULL,'Active'),(14,'Five Star',2,'Backend/images/category/E7ffLDycnGrXYmth.jpg',NULL,'Active'),(15,'AC Servicing',1,'Backend/images/category/guyMPevJ1S8c2pQV.jpg',NULL,'Active'),(16,'Muscular Strength',6,'Backend/images/category/4G0MUHYz3Q3hlhih.jpg',NULL,'Active'),(17,'Male salon',4,'Backend/images/category/017jNKDW8amBtHzL.jpg',NULL,'Active'),(18,'Accessories',5,'Backend/images/category/iNx6JYuQXSX18wJ4.jpg',NULL,'Active'),(19,'Jewellery',13,'Backend/images/category/PC26wpTUaad6JpaW.jpg',NULL,'Active'),(20,'Buffet',3,'Backend/images/category/6OyfOZ1int6yaPxK.jpg',NULL,'Active');
/*!40000 ALTER TABLE `spotlist_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_cities`
--

DROP TABLE IF EXISTS `spotlist_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_cities` (
  `spotlist_cities_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_cities_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int NOT NULL DEFAULT '2',
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `spotlist_cities_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`spotlist_cities_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_cities`
--

LOCK TABLES `spotlist_cities` WRITE;
/*!40000 ALTER TABLE `spotlist_cities` DISABLE KEYS */;
INSERT INTO `spotlist_cities` VALUES (1,'New York','Backend/images/city/Ko9o22cvP9SUuhWP.jpg',236,NULL,2,'Active','new-york'),(2,'Washington','Backend/images/city/cYVS8v6HKSLCZ9GA.jpg',236,NULL,2,'Active','washington'),(3,'London','Backend/images/city/J0fm5yHhHHCr4hto.jpg',235,NULL,2,'Active','london'),(4,'Brooklyn','Backend/images/city/0KlAS5KgMKpfVVgk.jpg',236,NULL,2,'Active','brooklyn'),(5,'Queens','Backend/images/city/2GTno72qhYuVeIb3.jpg',236,NULL,2,'Active','queens'),(7,'Khulna','Backend/images/city/xhSnzN3a2FrCAdK0.jpg',19,NULL,2,'Active','khulna'),(8,'Dhaka','Backend/images/city/gzaqDH1ikIQXVXR9.jpg',19,NULL,2,'Active','dhaka'),(9,'Nomi','Backend/images/city/4uvgrswwJfACG8KF.jpg',111,NULL,2,'Inactive','nomi');
/*!40000 ALTER TABLE `spotlist_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_countries`
--

DROP TABLE IF EXISTS `spotlist_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_countries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_char_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `three_char_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_countries`
--

LOCK TABLES `spotlist_countries` WRITE;
/*!40000 ALTER TABLE `spotlist_countries` DISABLE KEYS */;
INSERT INTO `spotlist_countries` VALUES (1,'Afghanistan','AF','AFG'),(2,'Aland Islands','AX','ALA'),(3,'Albania','AL','ALB'),(4,'Algeria','DZ','DZA'),(5,'American Samoa','AS','ASM'),(6,'Andorra','AD','AND'),(7,'Angola','AO','AGO'),(8,'Anguilla','AI','AIA'),(9,'Antarctica','AQ','ATA'),(10,'Antigua and Barbuda','AG','ATG'),(11,'Argentina','AR','ARG'),(12,'Armenia','AM','ARM'),(13,'Aruba','AW','ABW'),(14,'Australia','AU','AUS'),(15,'Austria','AT','AUT'),(16,'Azerbaijan','AZ','AZE'),(17,'Bahamas','BS','BHS'),(18,'Bahrain','BH','BHR'),(19,'Bangladesh','BD','BGD'),(20,'Barbados','BB','BRB'),(21,'Belarus','BY','BLR'),(22,'Belgium','BE','BEL'),(23,'Belize','BZ','BLZ'),(24,'Benin','BJ','BEN'),(25,'Bermuda','BM','BMU'),(26,'Bhutan','BT','BTN'),(27,'Bolivia','BO','BOL'),(28,'Bonaire, Sint Eustatius and Saba','BQ','BES'),(29,'Bosnia and Herzegovina','BA','BIH'),(30,'Botswana','BW','BWA'),(31,'Bouvet Island','BV','BVT'),(32,'Brazil','BR','BRA'),(33,'British Indian Ocean Territory','IO','IOT'),(34,'Brunei','BN','BRN'),(35,'Bulgaria','BG','BGR'),(36,'Burkina Faso','BF','BFA'),(37,'Burundi','BI','BDI'),(38,'Cambodia','KH','KHM'),(39,'Cameroon','CM','CMR'),(40,'Canada','CA','CAN'),(41,'Cape Verde','CV','CPV'),(42,'Cayman Islands','KY','CYM'),(43,'Central African Republic','CF','CAF'),(44,'Chad','TD','TCD'),(45,'Chile','CL','CHL'),(46,'China','CN','CHN'),(47,'Christmas Island','CX','CXR'),(48,'Cocos (Keeling) Islands','CC','CCK'),(49,'Colombia','CO','COL'),(50,'Comoros','KM','COM'),(51,'Congo','CG','COG'),(52,'Cook Islands','CK','COK'),(53,'Costa Rica','CR','CRI'),(54,'Ivory Coast','CI','CIV'),(55,'Croatia','HR','HRV'),(56,'Cuba','CU','CUB'),(57,'Curacao','CW','CUW'),(58,'Cyprus','CY','CYP'),(59,'Czech Republic','CZ','CZE'),(60,'Democratic Republic of the Congo','CD','COD'),(61,'Denmark','DK','DNK'),(62,'Djibouti','DJ','DJI'),(63,'Dominica','DM','DMA'),(64,'Dominican Republic','DO','DOM'),(65,'Ecuador','EC','ECU'),(66,'Egypt','EG','EGY'),(67,'El Salvador','SV','SLV'),(68,'Equatorial Guinea','GQ','GNQ'),(69,'Eritrea','ER','ERI'),(70,'Estonia','EE','EST'),(71,'Ethiopia','ET','ETH'),(72,'Falkland Islands (Malvinas)','FK','FLK'),(73,'Faroe Islands','FO','FRO'),(74,'Fiji','FJ','FJI'),(75,'Finland','FI','FIN'),(76,'France','FR','FRA'),(77,'French Guiana','GF','GUF'),(78,'French Polynesia','PF','PYF'),(79,'French Southern Territories','TF','ATF'),(80,'Gabon','GA','GAB'),(81,'Gambia','GM','GMB'),(82,'Georgia','GE','GEO'),(83,'Germany','DE','DEU'),(84,'Ghana','GH','GHA'),(85,'Gibraltar','GI','GIB'),(86,'Greece','GR','GRC'),(87,'Greenland','GL','GRL'),(88,'Grenada','GD','GRD'),(89,'Guadaloupe','GP','GLP'),(90,'Guam','GU','GUM'),(91,'Guatemala','GT','GTM'),(92,'Guernsey','GG','GGY'),(93,'Guinea','GN','GIN'),(94,'Guinea-Bissau','GW','GNB'),(95,'Guyana','GY','GUY'),(96,'Haiti','HT','HTI'),(97,'Heard Island and McDonald Islands','HM','HMD'),(98,'Honduras','HN','HND'),(99,'Hong Kong','HK','HKG'),(100,'Hungary','HU','HUN'),(101,'Iceland','IS','ISL'),(102,'India','IN','IND'),(103,'Indonesia','ID','IDN'),(104,'Iran','IR','IRN'),(105,'Iraq','IQ','IRQ'),(106,'Ireland','IE','IRL'),(107,'Isle of Man','IM','IMN'),(108,'Israel','IL','ISR'),(109,'Italy','IT','ITA'),(110,'Jamaica','JM','JAM'),(111,'Japan','JP','JPN'),(112,'Jersey','JE','JEY'),(113,'Jordan','JO','JOR'),(114,'Kazakhstan','KZ','KAZ'),(115,'Kenya','KE','KEN'),(116,'Kiribati','KI','KIR'),(117,'Kosovo','XK','---'),(118,'Kuwait','KW','KWT'),(119,'Kyrgyzstan','KG','KGZ'),(120,'Laos','LA','LAO'),(121,'Latvia','LV','LVA'),(122,'Lebanon','LB','LBN'),(123,'Lesotho','LS','LSO'),(124,'Liberia','LR','LBR'),(125,'Libya','LY','LBY'),(126,'Liechtenstein','LI','LIE'),(127,'Lithuania','LT','LTU'),(128,'Luxembourg','LU','LUX'),(129,'Macao','MO','MAC'),(130,'Macedonia','MK','MKD'),(131,'Madagascar','MG','MDG'),(132,'Malawi','MW','MWI'),(133,'Malaysia','MY','MYS'),(134,'Maldives','MV','MDV'),(135,'Mali','ML','MLI'),(136,'Malta','MT','MLT'),(137,'Marshall Islands','MH','MHL'),(138,'Martinique','MQ','MTQ'),(139,'Mauritania','MR','MRT'),(140,'Mauritius','MU','MUS'),(141,'Mayotte','YT','MYT'),(142,'Mexico','MX','MEX'),(143,'Micronesia','FM','FSM'),(144,'Moldava','MD','MDA'),(145,'Monaco','MC','MCO'),(146,'Mongolia','MN','MNG'),(147,'Montenegro','ME','MNE'),(148,'Montserrat','MS','MSR'),(149,'Morocco','MA','MAR'),(150,'Mozambique','MZ','MOZ'),(151,'Myanmar (Burma)','MM','MMR'),(152,'Namibia','NA','NAM'),(153,'Nauru','NR','NRU'),(154,'Nepal','NP','NPL'),(155,'Netherlands','NL','NLD'),(156,'New Caledonia','NC','NCL'),(157,'New Zealand','NZ','NZL'),(158,'Nicaragua','NI','NIC'),(159,'Niger','NE','NER'),(160,'Nigeria','NG','NGA'),(161,'Niue','NU','NIU'),(162,'Norfolk Island','NF','NFK'),(163,'North Korea','KP','PRK'),(164,'Northern Mariana Islands','MP','MNP'),(165,'Norway','NO','NOR'),(166,'Oman','OM','OMN'),(167,'Pakistan','PK','PAK'),(168,'Palau','PW','PLW'),(169,'Palestine','PS','PSE'),(170,'Panama','PA','PAN'),(171,'Papua New Guinea','PG','PNG'),(172,'Paraguay','PY','PRY'),(173,'Peru','PE','PER'),(174,'Phillipines','PH','PHL'),(175,'Pitcairn','PN','PCN'),(176,'Poland','PL','POL'),(177,'Portugal','PT','PRT'),(178,'Puerto Rico','PR','PRI'),(179,'Qatar','QA','QAT'),(180,'Reunion','RE','REU'),(181,'Romania','RO','ROU'),(182,'Russia','RU','RUS'),(183,'Rwanda','RW','RWA'),(184,'Saint Barthelemy','BL','BLM'),(185,'Saint Helena','SH','SHN'),(186,'Saint Kitts and Nevis','KN','KNA'),(187,'Saint Lucia','LC','LCA'),(188,'Saint Martin','MF','MAF'),(189,'Saint Pierre and Miquelon','PM','SPM'),(190,'Saint Vincent and the Grenadines','VC','VCT'),(191,'Samoa','WS','WSM'),(192,'San Marino','SM','SMR'),(193,'Sao Tome and Principe','ST','STP'),(194,'Saudi Arabia','SA','SAU'),(195,'Senegal','SN','SEN'),(196,'Serbia','RS','SRB'),(197,'Seychelles','SC','SYC'),(198,'Sierra Leone','SL','SLE'),(199,'Singapore','SG','SGP'),(200,'Sint Maarten','SX','SXM'),(201,'Slovakia','SK','SVK'),(202,'Slovenia','SI','SVN'),(203,'Solomon Islands','SB','SLB'),(204,'Somalia','SO','SOM'),(205,'South Africa','ZA','ZAF'),(206,'South Georgia and the South Sandwich Islands','GS','SGS'),(207,'South Korea','KR','KOR'),(208,'South Sudan','SS','SSD'),(209,'Spain','ES','ESP'),(210,'Sri Lanka','LK','LKA'),(211,'Sudan','SD','SDN'),(212,'Suriname','SR','SUR'),(213,'Svalbard and Jan Mayen','SJ','SJM'),(214,'Swaziland','SZ','SWZ'),(215,'Sweden','SE','SWE'),(216,'Switzerland','CH','CHE'),(217,'Syria','SY','SYR'),(218,'Taiwan','TW','TWN'),(219,'Tajikistan','TJ','TJK'),(220,'Tanzania','TZ','TZA'),(221,'Thailand','TH','THA'),(222,'Timor-Leste (East Timor)','TL','TLS'),(223,'Togo','TG','TGO'),(224,'Tokelau','TK','TKL'),(225,'Tonga','TO','TON'),(226,'Trinidad and Tobago','TT','TTO'),(227,'Tunisia','TN','TUN'),(228,'Turkey','TR','TUR'),(229,'Turkmenistan','TM','TKM'),(230,'Turks and Caicos Islands','TC','TCA'),(231,'Tuvalu','TV','TUV'),(232,'Uganda','UG','UGA'),(233,'Ukraine','UA','UKR'),(234,'United Arab Emirates','AE','ARE'),(235,'United Kingdom','GB','GBR'),(236,'United States','US','USA'),(237,'United States Minor Outlying Islands','UM','UMI'),(238,'Uruguay','UY','URY'),(239,'Uzbekistan','UZ','UZB'),(240,'Vanuatu','VU','VUT'),(241,'Vatican City','VA','VAT'),(242,'Venezuela','VE','VEN'),(243,'Vietnam','VN','VNM'),(244,'Virgin Islands, British','VG','VGB'),(245,'Virgin Islands, US','VI','VIR'),(246,'Wallis and Futuna','WF','WLF'),(247,'Western Sahara','EH','ESH'),(248,'Yemen','YE','YEM'),(249,'Zambia','ZM','ZMB'),(250,'Zimbabwe','ZW','ZWE');
/*!40000 ALTER TABLE `spotlist_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory`
--

DROP TABLE IF EXISTS `spotlist_directory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory` (
  `spotlist_directory_id` int unsigned NOT NULL AUTO_INCREMENT,
  `sys_users_id` bigint unsigned NOT NULL,
  `spotlist_parent_category_id` int unsigned DEFAULT NULL,
  `spotlist_directory_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spotlist_directory_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `google_analytics_id` text COLLATE utf8mb4_unicode_ci,
  `country_id` int DEFAULT NULL,
  `city` int DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_location` text COLLATE utf8mb4_unicode_ci,
  `thumb_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_provider` enum('Youtube','Vimeo','Facebook','HTML file') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Youtube',
  `video_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `seo_meta_description` text COLLATE utf8mb4_unicode_ci,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'General',
  `registration_step` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int NOT NULL DEFAULT '2',
  `updated_by` int NOT NULL DEFAULT '2',
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `video_ability` tinyint(1) NOT NULL DEFAULT '0',
  `visit_count` bigint NOT NULL DEFAULT '0',
  `booking_count` bigint NOT NULL DEFAULT '0',
  `wishing_count` bigint NOT NULL DEFAULT '0',
  `status` enum('Active','Inactive','Declined','Incomplete') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Incomplete',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`spotlist_directory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory`
--

LOCK TABLES `spotlist_directory` WRITE;
/*!40000 ALTER TABLE `spotlist_directory` DISABLE KEYS */;
INSERT INTO `spotlist_directory` VALUES (11,26,2,'Hotel Royel','hotel-royel','Set in Khulna, Hotel Royal International has a restaurant, a shared lounge, and a garden. This 3-star hotel offers an ATM and a tour desk.',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/directory/9evqb8AjXQkR5dZZ.jpg','Backend/images/directory/D9qGFKA0ginVwtfb.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Hotel','basic,features,gallery,schedule,contact,type',2,2,1,0,16,1,0,'Active','2020-10-23 07:28:12','2020-10-23 07:28:12'),(12,22,3,'Spicy Restaurant','mexican-spicy-restaurant','\"Mexican Spicy\" brings special decoration of taste for you.',1,NULL,19,7,'16 KDA','22.819002752821124','89.55323205396726',NULL,'Backend/images/directory/JcWV1hHP627mjGTx.jpg','Backend/images/directory/6KQODz9rZF8dsayk.jpg','Youtube',NULL,NULL,NULL,'https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','Restaurant','basic,location,features,gallery,schedule,contact,type',2,2,1,1,18,3,1,'Active','2020-11-19 11:28:11','2020-11-19 11:28:11'),(13,2,3,'Hotel Cityinn','hotel-cityinn','Book at Hotel City Inn, Budapest. No Reservation Costs. Great Rates. Great Availability. Special Offers. Low Rates. Types: Double Or Twin Room, Superior Double, Or Twin Room, Superior Apartment.',0,NULL,19,7,'B-52','22.820970309818644','89.550620833313',NULL,'Backend/images/directory/gEEioTv68WKTdeIM.jpg','Backend/images/directory/5TZokDhC0X3ZpVZw.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Restaurant','basic,location,features,gallery,schedule,contact,type',2,2,1,0,33,3,0,'Active','2020-10-28 13:56:55','2020-10-28 13:56:55'),(14,22,1,'Sam Shop','sam-shop','Best Air Conditioner Repair, Installation and Servicing Company in Dhaka. Free Quotation for AC Fixing, Cooling Problem, Water Leakage, Gas Filling Charges.',1,NULL,19,8,'Bashundhara','22.819009982916523','89.5512269327881',NULL,'Backend/images/directory/kgUEIV8UybgWmG91.jpg','Backend/images/directory/1Bo9rf61pidHLX8B.jpg','Youtube','https://www.youtube.com/embed/s6NHxfKUkUo',NULL,NULL,NULL,NULL,'014562',NULL,NULL,NULL,NULL,'General','basic,location,features,gallery,schedule,contact,type',2,2,1,1,12,0,1,'Active','2020-10-23 12:51:39','2020-10-23 12:51:39'),(15,26,4,'Exclusive Parlor','exclusive-parlor','Exclusive Bridal Beauty Parlour.  We Provide Best Quality of Beauty Care Services.',0,NULL,19,7,NULL,'40.7831','73.9712',NULL,'Backend/images/directory/jetdGyIVn3HR5df8.jpg','Backend/images/directory/b8pyuzCcJmrJX560.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Beauty','basic,location,features,gallery,schedule,contact,type',2,2,1,0,14,2,2,'Active','2020-10-23 13:04:42','2020-10-23 13:04:42'),(16,22,5,'Jamuna Shopping Complex','jamuna-shopping-complex','Jamuna Future Park is a shopping mall in Dhaka. It was inaugurated on 6 September 2013. Construction began in 2002, by Jamuna Builders Ltd., a subsidiary of the Jamuna Group and the exterior was completed in 2008. It has a total floor area of 4,100,000 square feet.',1,NULL,236,2,NULL,'22.819009982916523','89.5512269327881',NULL,'Backend/images/directory/5ITm3c5uTrMAHyir.jpg','Backend/images/directory/qwsQLjjF1tPNzjGd.jpg','Youtube',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Shopping','basic,location,features,schedule,contact,gallery,type',2,2,1,1,11,0,1,'Active','2020-10-23 13:29:32','2020-10-23 13:29:32'),(17,26,6,'Fitness First','fitness-first','Fitness First is an international fitness centre brand founded in 1993 in the United Kingdom. The company owned and operated its clubs around the world, until financial pressures saw parts of the company sold off to various owners in different regions between 2016 and 2017.',1,NULL,236,1,'Tokyo','22.819009982916523','89.5512269327881',NULL,'Backend/images/directory/ZZkCQfhnaOfwi7qw.jpg','Backend/images/directory/uFP3HYaIE9dvHM49.jpg','Youtube',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Fitness','basic,location,features,gallery,schedule,contact,type',2,2,1,1,9,1,0,'Active','2020-10-23 13:18:04','2020-10-23 13:18:04'),(18,2,13,'Gold Plaza','gold-plaza','Cheap jewelry Shop in USA Gold Plaza',1,NULL,236,1,'Manhattan','40.7831','73.9712',NULL,'Backend/images/directory/kJLnKtDBAIFCMvas.jpg','Backend/images/directory/ciN281JZl4o8kLdN.jpg','',NULL,NULL,NULL,'https://spotlisting-laravel.itech-theme.com/','itechsite1601@gmail.com','+8801718538815',NULL,NULL,NULL,NULL,'Accessories','basic,location,features,gallery,schedule,contact,type',2,2,1,0,30,0,2,'Active','2020-10-23 13:10:12','2020-10-23 13:10:12'),(20,29,3,'KFC','kfc','KFC is an American fast-food restaurant chain headquartered in Louisville, Kentucky, that specializes in fried chicken. It is the world\'s second-largest restaurant chain after McDonald\'s, with 22,621 locations globally in 150 countries as of December 2019. The chain is a subsidiary of Yum!',1,NULL,19,7,NULL,NULL,NULL,NULL,'Backend/images/directory/GbxB7RA3bfp7AtDR.jpg','Backend/images/directory/ziEu5vF3ZKPO1Ixu.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Restaurant','basic,location,features,gallery,schedule,contact,type',2,2,1,0,1,0,0,'Active','2020-10-27 07:09:47','2020-10-27 07:09:47'),(21,22,3,'Mexican Spicy','mexican-spicy','If you love spicy Mexican food, you\'ll be so into these 20 spicy Mexican recipes for Cinco de Mayo, Taco',0,NULL,19,8,'2555','22.82325408133824','89.53994241259765',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Restaurant','basic,location,features,gallery,schedule,contact,type',2,2,1,0,28,1,1,'Active','2020-10-28 12:49:31','2020-10-28 12:49:31'),(22,31,3,'Top in town','top-in-town','The best Indian food from Top in Town Restaurant and Biryani House now delivered to your home and office via food delivery service Deliveroo in Brisbane.',0,NULL,19,8,'KDA','22.825174779130016','89.55205453967284',NULL,'Backend/images/directory/2tKHb4ouGxVhcXw3.jpg','Backend/images/directory/sCdoNXIVZKTSR0Le.jpg','Youtube',NULL,NULL,NULL,'https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','Restaurant','basic,location,features,schedule,contact,type,gallery',2,2,1,1,29,1,1,'Active','2020-10-28 13:30:44','2020-10-28 13:30:44'),(23,31,1,'Fashion House','fashion-house','Fashion houses do an outstanding job for the Bangladeshi individuals. Many designers are personally open their fashion houses. Best Fashion House In Dhaka. All the fashion houses in Bangladesh, designers are constantly attempting to provide new designs for the client.',0,NULL,19,7,'7/A Sonadaga, khulna','22.820593497007696','89.54096968764732',NULL,'Backend/images/directory/EjojPztv7Sc0VF7j.jpg','Backend/images/directory/P96Lvbh5zeFeReFL.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'General','basic,location,features,gallery,schedule,contact,type',2,2,1,0,1,0,0,'Active','2020-11-10 14:17:59','2020-11-10 14:17:59'),(24,22,4,'Exclusive Parlor1','exclusive-parlor1','Exclusive Bridal Beauty Parlour',1,NULL,19,7,'Dak Bangla mor','22.816936939580398','89.56281320509846',NULL,'Backend/images/directory/9DtstEyaxxfo2B1q.jpg','Backend/images/directory/OWqCt3QIEEHZNhL7.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Beauty','basic,location,features,gallery,schedule,contact,type',2,2,1,0,0,0,0,'Active','2020-11-10 14:09:47','2020-11-10 14:09:47'),(25,2,2,'Hotel The Cox Today','hotel-the-cox-today','Hotel The Cox Today is a privately owned 5 Star Standard Luxury Hotel in Cox\'s Bazar, Bangladesh. It is situated in an attractive location of Kolatoli Road, Cox\'s Bazar which is only 5 minutes drive form the Cox\'s Bazar Airport. It is a place of choice of most of the corporate executives, foreigners, newly married couples, and for those who prefer to stay in luxury with stunning view of The \'Bay of Bengal\'.',1,NULL,19,8,'Plot-7, Road-02 Hotel Motel Zone, Kolatoly Road, Dhaka','23.801135421732152','90.4228741284505',NULL,'Backend/images/directory/tW3SG5g92CQ1a6MA.jpg','Backend/images/directory/t77jO2CkIeciBboO.jpg','',NULL,NULL,NULL,'https://hotelthecoxtoday.com/gallery.php','eservation@hotelthecoxtoday.com','755598450','https://www.facebook.com/thecoxtoday/',NULL,NULL,NULL,'Hotel','basic,location,features,gallery,schedule,contact,type',2,2,1,0,7,0,0,'Active','2020-11-09 13:29:52','2020-11-09 13:29:52'),(26,31,NULL,'Le Méridien Dhaka','le-meridien-dhaka','A brilliant travel experience awaits guests of Le Méridien Dhaka. Ideal for both business and leisure travel, our hotel welcomes you with a prime location in Bangladesh\'s capital city, tastefully designed rooms and a wealth of smart amenities.',1,NULL,19,8,'79/A Commercial Area, Airport Rd., Nikunja 2, Khilkhet, Dhaka  1229 Bangladesh','23.792382098256407','90.41291957578126',NULL,'Backend/images/directory/fXhpovoZAwG6A3No.jpg','Backend/images/directory/YJGdmBfpT2dv2h1A.jpg','Youtube',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'General','basic,location,features,gallery,schedule,contact',2,2,1,1,6,0,0,'Active','2020-11-10 13:51:10','2020-11-10 13:51:10'),(27,2,3,'Eatery','eatery','The eatery is a restaurant or other commercial establishment serving food',1,NULL,19,7,'47, Ferdous Plaza, Opposite Khulna Museum, Majid Sarani, Shib Bari More Cir, Khulna 9000','22.82165215970336','89.55053938728028',NULL,'Backend/images/directory/RIGJuRZUpTVni4jB.jpg','Backend/images/directory/1EPPdG87n14b3f5W.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Restaurant','basic,location,features,gallery,schedule,contact,type',2,2,1,0,1,0,0,'Active','2020-11-19 10:32:56','2020-11-19 10:32:56'),(28,2,3,'Roadside Dhaba','roadside-dhaba','Dhaba or Punjabi Dhaba is a roadside restaurant in the Indian subcontinent. They are on highways, generally serve local cuisine, and also serve as truck stops. They are most commonly found next to petrol stations, and most are open 24 hours a day.',0,NULL,19,7,'B-52 172 Road','22.81579520745774','89.54291128669433',NULL,'Backend/images/directory/r4k7GhXzvKX9BVZt.png','Backend/images/directory/PyvkSCaZjd4ZWFWE.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Restaurant','basic,location,features,gallery,schedule,contact,type',2,2,1,0,2,0,0,'Active','2020-11-19 10:49:45','2020-11-19 10:49:45'),(29,2,3,'Tiger Garden','tiger-garden','Tiger Garden restaurant is the largest Garden restaurant. This restaurant provides large car parking & comfort with security.',1,NULL,19,7,'A Block At baridhara','22.801702169042237','89.57906070654295',NULL,'Backend/images/directory/4Lp1CwGnZKZqP0Rd.jpg','Backend/images/directory/Ef0lRf2wVbkvOoGI.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Restaurant','basic,location,features,gallery,schedule,contact,type',2,2,1,0,0,0,0,'Active','2020-11-19 11:10:43','2020-11-19 11:10:43'),(30,2,3,'City Light Cafe','city-light-cafe','Citylight Cafe & Restaurant is the First Rooftop Cafe & Restaurant of Khulna that offers the most magnificent river view with quality food & service.',1,NULL,19,7,'B-52 Bangladesh','23.903397849325383','90.43518110875245',NULL,'Backend/images/directory/r1bcLQGeop0HtYkL.jpg','Backend/images/directory/oYujEqbGCmthvdIe.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Restaurant','basic,location,features,gallery,schedule,contact,type',2,2,1,0,1,0,0,'Active','2020-11-19 11:24:05','2020-11-19 11:24:05'),(31,2,2,'Hotel Sayman','hotel-sayman','After fifty years of glorious past, Sayeman Beach Resort revives its famed legacy of comfort, elegance and impeccable service. An eminent landmark constructed in 1964, this legendary first private hotel of Cox’s Bazar is reborn, infusing modern sophistication into this vintage-chic, iconic hotel at a new beachfront location of Marine Drive, Kolatoli, Cox’s Bazar.',1,NULL,19,8,'Jaliapalong, Inani, Ukhia, Cox’s Bazar 4750, Bangladesh.','23.93317834287528','90.41023189375001',NULL,'Backend/images/directory/C0MzB6tZxwhUuxJG.jpg','Backend/images/directory/ZarSYZlii4nOfNN1.jpg','',NULL,NULL,NULL,'https://spotlisting-laravel.itech-theme.com/','Sayeman@yopmail.com','1236598','https://www.facebook.com/','https://www.linkedin.com/','https://twitter.com/',NULL,'Hotel','basic,location,features,gallery,schedule,contact,type',2,2,1,0,0,0,0,'Active','2020-11-19 11:40:30','2020-11-19 11:40:30'),(32,2,NULL,'Royal Tulip Hotel','royal-tulip-hotel','Royal Tulip Sea Pearl Beach Resort & Spa is located on Inani beach, Cox\'s Bazar with lush green hills rise from the east and endless sea stretching on the west, the resort offers panoramic visuals of Bay of Bengal.',1,NULL,19,7,'B-53 Block C','22.824994291134484','89.53983988190916',NULL,'Backend/images/directory/r5dQRQiR0YxcCsHd.jpg','Backend/images/directory/VKN1k6wWcv9Jv1uy.jpg','',NULL,NULL,NULL,'https://spotlisting-laravel.itech-theme.com/',NULL,NULL,NULL,NULL,NULL,NULL,'General','basic,location,features,gallery,schedule,contact',2,2,1,0,0,0,0,'Active','2020-11-19 12:02:21','2020-11-19 12:02:21'),(33,2,2,'Seagull Hotel','seagull-hotel','Welcome to SEAGULL HOTEL where business and leisure blend together. Enjoying an unrivalled location, overlooking the Bay of Bengal (only 25 yards from the Bay water) and sitting in the laps of hills, we offer deluxe accommodation in 181 well appointed guest rooms and suites. The panoramic view of the ocean, the majestic hills and the natural beauty of the tamarisk trees are all wonderfully complemented by luxury facilities and Bangladeshi hospitality.',0,NULL,19,8,'Seagull Hotel, Hotel Motel Zone, Cox\'s Bazar Sea Beach, Cox’s Bazar.','22.368722914771183','91.80619921113279',NULL,'Backend/images/directory/gxLYI7yCN1K7SwuY.jpg','Backend/images/directory/Uk9eRYN1584AVMfj.jpg','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Hotel','basic,location,features,gallery,schedule,contact,type',2,2,1,0,0,0,0,'Active','2020-11-19 12:02:07','2020-11-19 12:02:07'),(34,22,NULL,'Hotel Castle Salam','hotel-castle-salam','Established in 1996 as a small business Hotel in Khulna, since our inception we have since grown to a 100 room luxurious business boutique Hotel, serving both leisure travelers and corporate clients. Recently we have gone through a major renovation, having raised the bar on Hotel standards in Khulna with unparalleled facilities. Staying true to our corporate clients we continue to offer generous corporate discounts on accommodations and corporate events.',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Youtube',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'General','basic',2,2,0,0,0,0,0,'Incomplete','2020-11-19 12:54:11','2020-11-19 12:54:11'),(35,22,NULL,'Hotel Sea Crown','hotel-sea-crown','Hotel Sea Crown is a Three Star Deluxe Hotel spectacularly designed and added with traditional hospitality of courteous, attractive and personalized service and is conveniently located closest to the Beach, Where one can enjoy the Sea, the Sunset and the waves of the Sea lying on the bed of our Hotel.',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Youtube',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'General','basic',2,2,0,0,0,0,0,'Incomplete','2020-11-19 12:54:59','2020-11-19 12:54:59');
/*!40000 ALTER TABLE `spotlist_directory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_gallary`
--

DROP TABLE IF EXISTS `spotlist_directory_gallary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_gallary` (
  `spotlist_directory_gallary_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_directory_id` int unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_directory_gallary_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_gallary`
--

LOCK TABLES `spotlist_directory_gallary` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_gallary` DISABLE KEYS */;
INSERT INTO `spotlist_directory_gallary` VALUES (1,1,'Backend/images/directory/R3BuKw4tdSpT1S5J.jpg','Active'),(2,1,'Backend/images/directory/a7GAfjSY28zE7aan.jpg','Active'),(3,1,'Backend/images/directory/tSNqPXc65tfmRZRx.jpg','Active'),(4,1,'Backend/images/directory/X9XdOxZxkl5wSzZy.jpg','Active'),(8,3,'Backend/images/directory/lDg9itpGQ0yULzEe.jpg','Active'),(9,3,'Backend/images/directory/w2NWMhvGRl9piaRL.jpg','Active'),(10,4,'Backend/images/directory/JCrykk3Z4Itusjp7.jpg','Active'),(11,4,'Backend/images/directory/Qy0nHTAxY1iqvZjl.jpg','Active'),(12,4,'Backend/images/directory/uAQ7h3wrcMXDn42D.jpg','Active'),(13,4,'Backend/images/directory/ndzWKQvc45Fy6v1d.jpg','Active'),(14,4,'Backend/images/directory/bkdMUiLKwZGCjrO4.jpg','Active'),(15,5,'Backend/images/directory/vYStnZ8uihSv1qio.jpg','Active'),(16,5,'Backend/images/directory/PklIo7EQLKpgtZ8d.jpg','Active'),(17,12,'Backend/images/directory/u1swiE1QkRaF6tLS.jpg','Active'),(18,12,'Backend/images/directory/4pExyUMDbctRGwoa.jpg','Active'),(19,12,'Backend/images/directory/8WaLjsdIiWkGGyvO.jpg','Active'),(20,13,'Backend/images/directory/n3CpExtq5kfAE60p.jpg','Active'),(21,13,'Backend/images/directory/JahDN9C11lfcsm7G.jpg','Active'),(22,14,'Backend/images/directory/okJsAXfM6rtRWEJG.jpg','Active'),(23,18,'Backend/images/directory/dwPotH2naGugtQVH.jpg','Active'),(24,17,'Backend/images/directory/bnxh077mU3k8xJKI.jpg','Active'),(25,17,'Backend/images/directory/SDUtwdIcKmYTqS7j.jpg','Active'),(26,16,'Backend/images/directory/wUBGdCRT6D3Hdt2m.jpg','Active'),(27,15,'Backend/images/directory/pZUJeEPOv4P7RGFr.jpg','Active'),(28,15,'Backend/images/directory/2O5nFlxmkBnuOjHH.png','Active'),(29,14,'Backend/images/directory/hdfjbYIkFP3hIUYq.jpg','Active'),(30,20,'Backend/images/directory/6shnBv0ZGojPXP7f.jpg','Active'),(31,20,'Backend/images/directory/7QGrJBRFty3xk367.jpg','Active'),(32,22,'Backend/images/directory/mxUwaJKHIRtj4ehN.jpg','Active'),(33,22,'Backend/images/directory/U7AhqmVEtIBk0LuG.jpeg','Active'),(34,25,'Backend/images/directory/nICKHtjxV2G90tz3.jpg','Active'),(35,25,'Backend/images/directory/rdS6wpKp7rsSHtpv.jpg','Active'),(36,25,'Backend/images/directory/cNUiRwJJfjGE8UIL.jpg','Active'),(37,26,'Backend/images/directory/CQS8m5e4G3LsaItI.jpg','Active'),(38,26,'Backend/images/directory/oipphI3NRFbQXLUG.jpg','Active'),(39,24,'Backend/images/directory/0UxpqhzSjQ37CJxb.jpg','Active'),(40,27,'Backend/images/directory/JQrqDTnCwQbBR1xu.jpg','Active'),(41,27,'Backend/images/directory/TBWSuIPJmyp82g6x.jpg','Active'),(42,27,'Backend/images/directory/9kupNLdp0mcduaqy.jpg','Active'),(43,28,'Backend/images/directory/rqlWPJGP2IeSPRyg.jpg','Active'),(44,28,'Backend/images/directory/rDReq0jNeZTDPX9N.jpg','Active'),(45,29,'Backend/images/directory/OAKhFWJB9SVurzWV.jpg','Active'),(46,29,'Backend/images/directory/gMJHfPx49KEC1J7o.jpg','Active'),(47,30,'Backend/images/directory/eh1kPNk88mG3JNcI.jpg','Active'),(48,30,'Backend/images/directory/KhsCEnR09ifVitM4.jpg','Active'),(49,31,'Backend/images/directory/Dhnzasf4gHGKLxOk.jpg','Active'),(50,31,'Backend/images/directory/eFXUDE2agHwyyMNv.jpg','Active'),(51,32,'Backend/images/directory/n8NcL0wWGkqo5OIx.jpg','Active'),(52,33,'Backend/images/directory/icuHR9HhG7Kr5L0N.jpg','Active'),(53,32,'Backend/images/directory/pcZDnaLG6jGFc6dC.jpg','Active');
/*!40000 ALTER TABLE `spotlist_directory_gallary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_messages`
--

DROP TABLE IF EXISTS `spotlist_directory_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_messages` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `spotlist_directory_id` int unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `type` tinyint NOT NULL DEFAULT '0',
  `status` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_messages`
--

LOCK TABLES `spotlist_directory_messages` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `spotlist_directory_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_reviews`
--

DROP TABLE IF EXISTS `spotlist_directory_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_reviews` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `spotlist_directory_id` int NOT NULL,
  `rating` int DEFAULT NULL,
  `review` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_reviews`
--

LOCK TABLES `spotlist_directory_reviews` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_reviews` DISABLE KEYS */;
INSERT INTO `spotlist_directory_reviews` VALUES (1,4,1,3,'Some text will be written','2020-10-13 08:04:23','2020-10-13 08:04:23','Active'),(2,19,1,5,'Thank you','2020-10-14 13:43:57','2020-10-14 13:43:57','Active'),(3,19,3,4,'Nice and Great Hotel','2020-10-15 12:31:57','2020-10-15 12:31:57','Active'),(4,19,11,3,NULL,'2020-10-23 11:43:59','2020-10-23 11:43:59','Active'),(5,28,14,3,'Like it','2020-10-26 14:10:13','2020-10-26 14:10:13','Active'),(6,33,22,3,NULL,'2020-10-29 07:03:51','2020-10-29 07:03:51','Active'),(7,19,21,4,'thank you','2020-11-02 07:57:25','2020-11-02 07:57:25','Active');
/*!40000 ALTER TABLE `spotlist_directory_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_schedule`
--

DROP TABLE IF EXISTS `spotlist_directory_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_schedule` (
  `spotlist_directory_schedule_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_directory_id` int unsigned NOT NULL,
  `monday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tuesday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wednesday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thursday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `saturday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sunday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_directory_schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_schedule`
--

LOCK TABLES `spotlist_directory_schedule` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_schedule` DISABLE KEYS */;
INSERT INTO `spotlist_directory_schedule` VALUES (1,1,'12 AM-02 PM','12 AM-10 AM','12 AM-06 PM','12 AM-03 PM','09 AM-08 PM','09 AM-08 PM','Closed-Closed','Active'),(2,2,'05 AM-05 PM','07 AM-05 PM','05 AM-03 PM','06 AM-07 PM','09 AM-06 PM','06 AM-07 PM','Closed-Closed','Active'),(3,3,'07 AM-08 PM','07 AM-08 PM','07 AM-08 PM','07 AM-08 PM','07 AM-08 PM','Closed-Closed','07 AM-08 PM','Active'),(4,4,'10 AM-08 PM','10 AM-08 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(5,5,'10 AM-07 PM','10 AM-07 PM','10 AM-07 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(6,6,'11 AM-Closed','11 AM-Closed','11 AM-Closed','11 AM-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(7,7,'Closed-Closed','Closed-Closed','12 PM-06 PM','Closed-Closed','09 AM-07 PM','Closed-Closed','Closed-Closed','Active'),(8,9,'11 AM-06 PM','02 PM-09 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(9,10,'01 AM-05 AM','05 AM-07 AM','01 AM-11 AM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(10,12,'08 AM-08 PM','08 AM-08 PM','08 AM-08 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(11,13,'09 AM-09 PM','09 AM-09 PM','09 AM-09 PM','09 AM-09 PM','09 AM-09 PM','09 AM-09 PM','09 AM-09 PM','Active'),(12,11,'02 AM-07 PM','02 AM-07 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(13,14,'10 AM-07 PM','03 PM-08 PM','07 AM-07 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(14,18,'07 AM-09 PM','Closed-Closed','Closed-Closed','Closed-Closed','07 AM-08 PM','Closed-Closed','07 AM-06 PM','Active'),(15,17,'07 AM-08 PM','07 AM-03 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(16,16,'10 AM-06 PM','10 AM-06 PM','10 AM-06 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(17,15,'Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(18,20,'02 AM-02 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(19,22,'10 AM-07 PM','10 AM-07 PM','10 AM-07 PM','09 AM-08 PM','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(20,21,'Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(21,23,'Closed-Closed','Closed-Closed','Closed-Closed','01 AM-01 PM','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(22,25,'05 AM-05 PM','05 AM-05 PM','05 AM-05 PM','05 AM-05 PM','05 AM-05 PM','05 AM-05 PM','05 AM-05 PM','Active'),(23,26,'10 AM-11 PM','10 AM-11 PM','10 AM-11 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(24,24,'10 AM-08 PM','10 AM-08 PM','10 AM-08 PM','11 AM-08 PM','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(25,27,'12 PM-08 PM','12 PM-08 PM','12 PM-08 PM','12 PM-08 PM','12 PM-08 PM','12 PM-08 PM','12 PM-08 PM','Active'),(26,28,'07 AM-07 PM','07 PM-07 PM','07 AM-07 PM','07 AM-06 PM','07 AM-07 PM','Closed-Closed','Closed-Closed','Active'),(27,29,'11 AM-07 PM','11 AM-07 PM','11 AM-07 PM','11 AM-07 PM','11 AM-07 PM','11 AM-07 PM','11 AM-07 PM','Active'),(28,30,'07 AM-03 PM','07 AM-03 PM','07 AM-03 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(29,31,'07 AM-07 PM','07 AM-07 PM','07 AM-07 PM','Closed-Closed','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(30,33,'02 AM-02 PM','02 AM-02 PM','02 AM-02 PM','02 AM-02 PM','Closed-Closed','Closed-Closed','Closed-Closed','Active'),(31,32,'02 AM-09 PM','Closed-Closed','03 AM-09 PM','01 PM-09 PM','Closed-Closed','Closed-Closed','Closed-Closed','Active');
/*!40000 ALTER TABLE `spotlist_directory_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_type_beauty`
--

DROP TABLE IF EXISTS `spotlist_directory_type_beauty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_type_beauty` (
  `spotlist_directory_type_beauty_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_directory_id` int unsigned NOT NULL,
  `service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_start_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_end_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_duration` int DEFAULT NULL,
  `service_price` decimal(10,2) DEFAULT NULL,
  `service_description` text COLLATE utf8mb4_unicode_ci,
  `service_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_directory_type_beauty_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_type_beauty`
--

LOCK TABLES `spotlist_directory_type_beauty` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_type_beauty` DISABLE KEYS */;
INSERT INTO `spotlist_directory_type_beauty` VALUES (1,6,'Spa','10 AM','07 PM',59,318.00,NULL,'Backend/images/directory/beauty/9tCHFneAA1gjuL2Z.jpg','Active'),(2,15,'Make Up','10 AM','11 AM',30,3000.00,NULL,'Backend/images/directory/beauty/NCoSeZrtOQ9tQ2gh.jpg','Active'),(3,24,'Haircut, Beard Shave, Facial',NULL,NULL,NULL,1250.00,'Valid for 90 days from date of purchase\r\nValid for men only\r\n1 Voucher per person, may buy 1 additional as a gift.\r\nValid for any hair length\r\nValid daily including public holidays\r\nTiming: 10:00 am - 10:00 pm, Valid 7 days a week','Backend/images/directory/beauty/8OEwFcf0s69cJhoY.jpg','Active');
/*!40000 ALTER TABLE `spotlist_directory_type_beauty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_type_fitness`
--

DROP TABLE IF EXISTS `spotlist_directory_type_fitness`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_type_fitness` (
  `spotlist_directory_type_fitness_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_directory_id` int unsigned NOT NULL,
  `fitness_service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fitness_service_price` decimal(10,2) DEFAULT NULL,
  `fitness_service_description` text COLLATE utf8mb4_unicode_ci,
  `fitness_service_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_directory_type_fitness_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_type_fitness`
--

LOCK TABLES `spotlist_directory_type_fitness` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_type_fitness` DISABLE KEYS */;
INSERT INTO `spotlist_directory_type_fitness` VALUES (1,7,'Trademil',50.00,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',NULL,'Active'),(2,7,'Trademil',50.00,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.','Backend/images/directory/fitness/wMkb7fjOEwJoE40N.jpg','Active'),(3,7,'Trademil',50.00,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',NULL,'Active'),(4,7,'Trademil',50.00,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',NULL,'Active'),(5,7,'',NULL,NULL,NULL,'Active'),(6,8,'Swimming pool',100.00,'some text',NULL,'Active'),(7,8,'Gym',1000.00,'some text will be written',NULL,'Active'),(8,17,'Trademil',120.00,'Best Treadmill Overall: Nautilus T618.\r\nBest Upright Folding Treadmill: ProForm SMART Pro 2000.\r\nBest Treadmill For Big and Tall Users: 3G Cardio Elite Treadmill.\r\nBest Splurge Treadmill: Peloton Tread.\r\nBest Treadmill With Guided Workouts: NordicTrack T 8.5 S.\r\nBest Compact Treadmill: Goplus 2 in 1 Folding Treadmill.','Backend/images/directory/fitness/bZnV72sH8WWEjSma.jpg','Active');
/*!40000 ALTER TABLE `spotlist_directory_type_fitness` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_type_hotel_room`
--

DROP TABLE IF EXISTS `spotlist_directory_type_hotel_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_type_hotel_room` (
  `spotlist_directory_type_hotel_room_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_directory_id` int unsigned NOT NULL,
  `room_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_price` decimal(10,2) DEFAULT NULL,
  `room_description` text COLLATE utf8mb4_unicode_ci,
  `room_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_facilities` text COLLATE utf8mb4_unicode_ci,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_directory_type_hotel_room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_type_hotel_room`
--

LOCK TABLES `spotlist_directory_type_hotel_room` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_type_hotel_room` DISABLE KEYS */;
INSERT INTO `spotlist_directory_type_hotel_room` VALUES (1,2,'Single bed Ac',100.00,'Some text will be written here','Backend/images/directory/hotel/Zs1b8YteP6d3t6vW.jpeg','Tools1,Tools2','Active'),(2,2,'Super delux',200.00,'some text will be written','Backend/images/directory/hotel/oMmrP6EDQyylQiBC.jpg','Free Meal,Bufe','Active'),(3,3,'Suite Single',4255.00,'Lorem Ipsum','Backend/images/directory/hotel/7qtk1KQu31gkfzw8.jpg','AC,wifi','Active'),(4,10,'Single Person',3200.00,'A single room is for one person and contains a single bed, and will usually be quite small (but that\'s the norm for European hotel rooms anyway). A double room is for two people and may contain a double bed or two singles','Backend/images/directory/hotel/YuCMMCWnBrmxtHtN.jpg','Freeze','Active'),(6,11,'Single Person',3214.00,NULL,'Backend/images/directory/hotel/VjTJcFZr4PXMS2dz.jpg','Free breakfast','Active'),(7,13,'Double Room',555.00,NULL,'Backend/images/directory/hotel/mXp4TIIrgRltUmKT.jpg',NULL,'Active'),(8,25,'Presidential Suite',80000.00,'Attractively ornamented with complete marble & tiles and luxurious fabrics, our two prominent Presidential suites are 1900 & 1800 sq ft.','Backend/images/directory/hotel/vwgseLKMF3AgS7UB.jpg','Free car parking,Free smoking','Active'),(9,31,'Super Deluxe King',3652.00,'Bed type: King\r\nMax Room capacity:3','Backend/images/directory/hotel/OpUGMyOmErZzGUE4.jpg','Free Breakfast','Active'),(10,31,'Infinity Sea View',3700.00,'Bed type: Twin\r\nMax Room capacity:3','Backend/images/directory/hotel/vSpM0TAxR5OK2EUb.jpg','Free wifi','Active'),(11,33,'Double Room',6985.00,'Best for you','Backend/images/directory/hotel/4ylu9x9omYojcbZ3.jpg',NULL,'Active');
/*!40000 ALTER TABLE `spotlist_directory_type_hotel_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_type_like_table`
--

DROP TABLE IF EXISTS `spotlist_directory_type_like_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_type_like_table` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `sys_users_id` bigint unsigned NOT NULL,
  `spotlist_directory_id` int NOT NULL,
  `spotlist_directory_type_id` int NOT NULL,
  `like` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_type_like_table`
--

LOCK TABLES `spotlist_directory_type_like_table` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_type_like_table` DISABLE KEYS */;
INSERT INTO `spotlist_directory_type_like_table` VALUES (1,4,1,1,1),(2,4,1,2,1),(3,4,2,1,1),(4,4,2,2,1),(5,4,3,3,1),(6,19,5,5,1),(7,19,5,4,1),(8,19,6,1,1),(9,21,6,1,0),(10,21,5,5,1),(11,19,12,6,1),(12,2,16,1,0),(13,28,15,2,1),(14,33,22,11,1),(15,19,21,14,0);
/*!40000 ALTER TABLE `spotlist_directory_type_like_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_type_restaurant`
--

DROP TABLE IF EXISTS `spotlist_directory_type_restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_type_restaurant` (
  `spotlist_directory_type_restaurant_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_directory_id` int unsigned NOT NULL,
  `menu_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_price` decimal(10,2) DEFAULT NULL,
  `ingredients_item` text COLLATE utf8mb4_unicode_ci,
  `menu_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_directory_type_restaurant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_type_restaurant`
--

LOCK TABLES `spotlist_directory_type_restaurant` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_type_restaurant` DISABLE KEYS */;
INSERT INTO `spotlist_directory_type_restaurant` VALUES (1,1,'Pizza',120.00,'Half','Backend/images/directory/restaurant/QekntPdXCgRFf3of.jpg',NULL,'Active'),(2,1,'Burger',20.00,'Chesse ,Chicken','Backend/images/directory/restaurant/1agf3wcmF5AScL8N.jpg',NULL,'Active'),(3,4,'French Fry',85.00,NULL,NULL,NULL,'Active'),(4,5,'Pizza',350.00,'chicken,Beef,mutton,vegetable','Backend/images/directory/restaurant/ksV3bdOEaYhXV1Oj.png','Lorem Ipsum','Active'),(5,5,'French Fry',90.00,'Spicy fry,Mashala Fry','Backend/images/directory/restaurant/jsqR9CAt58BVcfD0.jpg','Lorem Ipsum','Active'),(6,12,'French Fry',60.00,'Mashala ,Spicy ,Simple','Backend/images/directory/restaurant/H9TloK8CZ579oCqC.jpg','French fries are served hot, either soft or crispy, and are generally eaten as part of lunch or dinner or by themselves as a snack, and they commonly appear on the menus of diners, fast food restaurants, pubs, and bars. They are usually salted and, depending on the country, may be served with ketchup, vinegar, mayonnaise, tomato sauce, or other local specialties. Fries can be topped more heavily, as in the dishes of poutine or chili cheese fries. Chips can be made from sweet potatoes instead of potatoes. A baked variant,','Active'),(7,20,'Burger',NULL,NULL,NULL,NULL,'Active'),(8,20,'Burger',320.00,'Spicy',NULL,'A hamburger is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread roll or bun. The patty may be pan fried, grilled, smoked or flame broiled.','Active'),(10,13,'asdf',NULL,NULL,NULL,NULL,'Active'),(11,22,'French fry',120.00,'Spicy ,Hot,Cool','Backend/images/directory/restaurant/5x2Zf8qSasR8YXnI.jpg','French fries, or simply fries, chips, finger chips, or French-fried potatoes, are batonnet or allumette-cut deep-fried potatoes.','Active'),(12,13,'French fry',23.00,'wer','Backend/images/directory/restaurant/y0qWMZIlPwofSDvV.jpg','sdgsfgf','Active'),(13,13,'French fry1',23.00,'as','Backend/images/directory/restaurant/inoVVKHWjwdJjInx.jpg','asfdgn','Active'),(14,21,'French fry',11.00,'asc','Backend/images/directory/restaurant/2Xx8cJ5n8EeJHdVO.jpg',NULL,'Active'),(15,21,'French fry1',1222.00,'sd','Backend/images/directory/restaurant/8h9c7JyKcmbKdlDX.jpg',NULL,'Active'),(16,27,'Chicken Burger',350.00,'cheese','Backend/images/directory/restaurant/LOqwwkNh1osQ7upj.jpg','A chicken buerger is a burger that typically consists of boneless, skinless chicken breast served between slices of bread.','Active'),(17,28,'Pizza',360.00,'Cheese','Backend/images/directory/restaurant/9Thdu6gQ0yRC6Mkd.jpg','Pizza is a savory dish of Italian origin consisting of a usually round, flattened base of leavened wheat-based dough topped with tomatoes, cheese, and often various other ingredients, which is then baked at a high temperature, traditionally in a wood-fired oven.','Active'),(18,28,'Prawn Fry',120.00,NULL,'Backend/images/directory/restaurant/wjhwX57FI65gUTH4.jpg','Fried shrimp or fried prawn is deep-fried shrimp and prawns.','Active'),(19,29,'Chicken Fry',160.00,'Cucumber','Backend/images/directory/restaurant/5945AYOydQyzE1G9.jpg','Southern fried chicken, also known simply as fried chicken, is a dish consisting of chicken pieces that have been coated in a seasoned batter and pan-fried, deep-fried, or pressure fried.','Active'),(20,30,'Pasta',222.00,NULL,'Backend/images/directory/restaurant/oOX36LMvd7WEzEJ6.jpg','Pasta is a type of food typically made from an unleavened dough of wheat flour mixed with water or eggs.','Active');
/*!40000 ALTER TABLE `spotlist_directory_type_restaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_type_shop`
--

DROP TABLE IF EXISTS `spotlist_directory_type_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_type_shop` (
  `spotlist_directory_type_shop_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_directory_id` int unsigned NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_varient` text COLLATE utf8mb4_unicode_ci,
  `product_price` decimal(10,2) DEFAULT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_directory_type_shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_type_shop`
--

LOCK TABLES `spotlist_directory_type_shop` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_type_shop` DISABLE KEYS */;
INSERT INTO `spotlist_directory_type_shop` VALUES (1,16,'Kidz zone','Toys,cars,kids dresses',300.00,'Backend/images/directory/beauty/f543gGhk6UkqCjGb.jpg','Kids Items from all categories such as School Bag, Music Swing/Auto Car, Push Car, Tricycle Basket, Dresses, Toys, watches.','Active');
/*!40000 ALTER TABLE `spotlist_directory_type_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_vs_category`
--

DROP TABLE IF EXISTS `spotlist_directory_vs_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_vs_category` (
  `spotlist_directory_id` int unsigned NOT NULL,
  `spotlist_category_id` int unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_vs_category`
--

LOCK TABLES `spotlist_directory_vs_category` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_vs_category` DISABLE KEYS */;
INSERT INTO `spotlist_directory_vs_category` VALUES (1,3),(2,2),(3,2),(3,7),(4,12),(5,12),(6,13),(8,7),(8,12),(9,12),(10,14),(11,7),(14,15),(15,17),(18,19),(17,16),(20,12),(21,12),(22,12),(13,7),(13,14),(25,14),(26,7),(24,17),(23,18),(27,12),(28,20),(29,20),(30,12),(12,12),(31,14),(33,14),(32,7),(34,7),(34,12),(35,7);
/*!40000 ALTER TABLE `spotlist_directory_vs_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_vs_features`
--

DROP TABLE IF EXISTS `spotlist_directory_vs_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_vs_features` (
  `spotlist_directory_id` int unsigned NOT NULL,
  `spotlist_features_id` int unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_vs_features`
--

LOCK TABLES `spotlist_directory_vs_features` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_vs_features` DISABLE KEYS */;
INSERT INTO `spotlist_directory_vs_features` VALUES (1,1),(1,4),(1,6),(1,7),(2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(3,1),(3,2),(3,5),(3,6),(3,7),(6,4),(7,5),(7,6),(7,7),(9,2),(9,3),(9,4),(10,2),(10,3),(10,4),(10,6),(10,7),(12,1),(11,2),(11,3),(11,4),(11,6),(11,7),(18,1),(17,1),(17,3),(17,5),(16,1),(16,2),(16,3),(16,4),(16,5),(16,6),(16,7),(15,1),(15,2),(15,3),(15,4),(15,5),(15,6),(15,7),(20,2),(20,3),(20,6),(20,7),(22,1),(22,2),(22,3),(22,4),(22,5),(22,6),(22,7),(22,10),(13,1),(13,2),(13,3),(13,4),(13,5),(13,6),(13,7),(21,2),(21,3),(21,4),(21,6),(21,10),(25,1),(25,3),(25,5),(25,6),(25,7),(25,10),(26,1),(26,2),(26,3),(26,5),(26,6),(24,1),(24,5),(23,1),(23,2),(23,3),(23,4),(27,1),(27,3),(28,1),(29,1),(29,2),(29,3),(30,3),(31,1),(31,2),(31,3),(31,4),(31,5),(31,6),(31,10),(33,4),(33,10),(32,3),(32,4);
/*!40000 ALTER TABLE `spotlist_directory_vs_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_directory_wishinglist`
--

DROP TABLE IF EXISTS `spotlist_directory_wishinglist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_directory_wishinglist` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `sys_users_id` bigint unsigned NOT NULL,
  `spotlist_directory_id` int NOT NULL,
  `is_wished` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_directory_wishinglist`
--

LOCK TABLES `spotlist_directory_wishinglist` WRITE;
/*!40000 ALTER TABLE `spotlist_directory_wishinglist` DISABLE KEYS */;
INSERT INTO `spotlist_directory_wishinglist` VALUES (1,19,1,1,'2020-10-14 13:44:18',NULL),(2,19,2,0,'2020-10-15 13:16:58',NULL),(3,19,3,1,'2020-10-15 12:29:07',NULL),(4,4,3,1,'2020-10-15 12:40:30',NULL),(5,19,5,1,'2020-10-16 11:17:54',NULL),(6,19,6,1,'2020-10-16 12:37:19',NULL),(7,21,5,1,'2020-10-16 13:07:34',NULL),(8,21,7,1,'2020-10-16 13:18:28',NULL),(9,19,10,1,'2020-10-22 18:48:49',NULL),(10,19,12,1,'2020-10-23 08:53:39',NULL),(11,28,14,1,'2020-10-26 13:53:04',NULL),(12,2,16,1,'2020-10-26 15:13:45',NULL),(13,28,15,1,'2020-10-26 16:57:12',NULL),(14,28,18,1,'2020-10-26 20:26:20',NULL),(15,33,18,1,'2020-10-29 07:43:08',NULL),(16,19,21,1,'2020-11-03 06:38:56',NULL),(17,19,15,1,'2020-11-02 10:23:37',NULL),(18,19,22,1,'2020-11-03 05:42:41',NULL);
/*!40000 ALTER TABLE `spotlist_directory_wishinglist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_faqs`
--

DROP TABLE IF EXISTS `spotlist_faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_faqs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_faqs`
--

LOCK TABLES `spotlist_faqs` WRITE;
/*!40000 ALTER TABLE `spotlist_faqs` DISABLE KEYS */;
INSERT INTO `spotlist_faqs` VALUES (1,'How to make payments ?','How to Set Up a Debt Repayment Plan in 6 Easy Steps\r\nMake a List of All Your Debts. Before you can come up with a strategy, you need to be able to see all your debts in one place. ...\r\nRank Your Debts. ...\r\nFind Extra Money to Pay Your Debts. ...\r\nFocus on One Debt at a Time. ...\r\nMove Onto the Next Debt on Your List. ...\r\nBuild Up Your Savings.','Active'),(2,'Help! I need to change/cancel my reservation!','Help is on the way: if you booked your travel on the Spotlisting site with one of our providers, then you can go to Bookings and get info on around-the-clock support. If you booked your travel outside of Spotlisting or were redirected from our site to a provider\'s web page to complete your booking, then you\'ll need to get in contact with the provider directly. Not sure who your provider is? Our partnering travel providers take your data privacy seriously, which is why Spotlisting doesn\'t have access to your booking to make changes or resend confirmation emails.','Active'),(3,'How do I create a Price Alert?','On our site, you can create one by signing in and going directly to Price Alerts in your account or you can look for the \"Track Prices\" button on the top left-hand side of flight results. If you\'re searching hotels, select \"Set Price Alert\" at the top right of the hotel results list. If you\'re on our app, you can get to Price Alerts using the app\'s navigation menu and follow instructions there.','Active'),(5,'Where do the star ratings for hotels come from?','Star ratings are provided by the hotels themselves as well as online travel agencies that sell hotel rooms. Spotlisting takes that information and displays what\'s most accurate. We understand that some of these providers are more reliable than others and take this into account when displaying the number of stars.','Active'),(6,'Where can I find my booking confirmation?','If you booked with one of our providers on the site, go to Bookings for info. However, if you booked off-site or we redirected you to a provider\'s website from Spotlisting to complete your purchase, then you\'ll need to contact the provider directly. Not sure who the provider is? Check your credit card statement to see who charged your card.','Active'),(7,'How to make payments ?','How to make payments ?\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet.','Inactive'),(9,'How to make payments ?','Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet.','Inactive');
/*!40000 ALTER TABLE `spotlist_faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_features`
--

DROP TABLE IF EXISTS `spotlist_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_features` (
  `spotlist_features_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_features_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spotlist_features_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int NOT NULL DEFAULT '2',
  `updated_by` int NOT NULL DEFAULT '2',
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`spotlist_features_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_features`
--

LOCK TABLES `spotlist_features` WRITE;
/*!40000 ALTER TABLE `spotlist_features` DISABLE KEYS */;
INSERT INTO `spotlist_features` VALUES (1,'Free wifi','Backend/images/features/7p0LhATr1TDzMyUI.jpg',2,2,'Active',NULL,NULL),(2,'KIds Zone','Backend/images/features/tlvcMrAuYCiQGHbw.jpg',2,2,'Active',NULL,NULL),(3,'Live music','Backend/images/features/VjpV0vZgrbtznIfk.jpg',2,2,'Active',NULL,NULL),(4,'Refreshment','Backend/images/features/ceNso9Qvs60k4tud.jpg',2,2,'Active',NULL,NULL),(5,'Gym','Backend/images/features/Lxwl4KvERI9rAOpe.jpg',2,2,'Active',NULL,NULL),(6,'Swimming Pool','Backend/images/features/MOuxo7aiXiKKSFqy.jpg',2,2,'Active',NULL,NULL),(7,'Indoor games','Backend/images/features/sWiFG6zzQO4ovcct.jpg',2,2,'Active',NULL,NULL),(8,'Free Breakfast',NULL,2,2,'Inactive',NULL,NULL),(9,'Guider',NULL,2,2,'Inactive',NULL,NULL),(10,'Free lunch','Backend/images/features/h8oxaGl4AD4DElKy.jpg',2,2,'Active',NULL,NULL);
/*!40000 ALTER TABLE `spotlist_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_how_it_works`
--

DROP TABLE IF EXISTS `spotlist_how_it_works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_how_it_works` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_how_it_works`
--

LOCK TABLES `spotlist_how_it_works` WRITE;
/*!40000 ALTER TABLE `spotlist_how_it_works` DISABLE KEYS */;
INSERT INTO `spotlist_how_it_works` VALUES (1,'Find Interesting Place','Backend/images/how_works/buhluklgKyOZq6n8.png','Spotlisting also offers a wide range of customized holidays thereby giving individual travelers the complete freedom and flexibility to define their preference for the destination, length of stay, and pace of travel.','Active'),(2,'Contact a Few Owners','Backend/images/how_works/9ERQZW9KhD7KYODR.png','We have tried to provide you some destination with a designed itinerary, however, if that is not matched your choice, we are able to offer a customized itinerary, tailored to your pace, and suit your interest and requirement.','Active'),(9,'Make a Listing','Backend/images/how_works/Fj4mySmnu9Ilddnq.png','Maecenas pulvinar, the laughter from her facilisis dignissim, nisi hendrerit than none at all, there is no fear that the court of the gate of the pull.','Active');
/*!40000 ALTER TABLE `spotlist_how_it_works` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_offline_payment`
--

DROP TABLE IF EXISTS `spotlist_offline_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_offline_payment` (
  `spotlist_offline_payment_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `sys_users_id` bigint unsigned NOT NULL,
  `spotlist_package_id` int unsigned NOT NULL,
  `payment_amount` decimal(10,2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`spotlist_offline_payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_offline_payment`
--

LOCK TABLES `spotlist_offline_payment` WRITE;
/*!40000 ALTER TABLE `spotlist_offline_payment` DISABLE KEYS */;
INSERT INTO `spotlist_offline_payment` VALUES (1,22,5,30.00,'Stripe','Active','2020-10-22 07:56:35','2020-10-22 07:56:35'),(2,22,3,49.00,'Card','Active','2020-10-23 05:59:41','2020-10-23 05:59:41'),(3,26,5,30.00,'Cash on delivery','Active','2020-10-23 06:31:17','2020-10-23 06:31:17'),(4,22,5,30.00,'Card','Active','2020-10-23 06:44:53','2020-10-23 06:44:53'),(5,31,3,49.00,'Cash on delivery','Active','2020-10-28 13:09:52','2020-10-28 13:09:52');
/*!40000 ALTER TABLE `spotlist_offline_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_packages`
--

DROP TABLE IF EXISTS `spotlist_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_packages` (
  `spotlist_packages_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_packages_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `packages_type` enum('Free','Paid') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Paid',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `day_validity` int DEFAULT NULL,
  `num_of_listing` int DEFAULT NULL,
  `num_of_categories` int DEFAULT NULL,
  `num_of_tags` int DEFAULT NULL,
  `num_of_photos` int DEFAULT NULL,
  `add_video_ability` tinyint(1) NOT NULL DEFAULT '0',
  `add_contact_form_ability` tinyint(1) NOT NULL DEFAULT '0',
  `is_recomended` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int NOT NULL DEFAULT '0',
  `updated_by` int NOT NULL DEFAULT '0',
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`spotlist_packages_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_packages`
--

LOCK TABLES `spotlist_packages` WRITE;
/*!40000 ALTER TABLE `spotlist_packages` DISABLE KEYS */;
INSERT INTO `spotlist_packages` VALUES (1,'Free subscription','Free',0.00,10,1,NULL,1,5,1,1,1,1,NULL,0,0,'Active',NULL,NULL),(3,'Standard','Paid',49.00,90,20,NULL,10,20,1,1,1,1,NULL,0,0,'Active',NULL,NULL),(4,'Premium','Paid',99.00,365,50,NULL,50,50,1,1,1,1,NULL,0,0,'Active',NULL,NULL),(5,'Basic','Paid',30.00,30,10,NULL,2,10,1,1,1,0,NULL,0,0,'Active',NULL,NULL),(6,'Special Pack','Free',10.00,2,2,NULL,2,2,0,0,0,0,NULL,0,0,'Active',NULL,NULL),(7,'Gold Pack','Paid',200.00,10,10,NULL,10,10,1,1,1,1,NULL,0,0,'Active',NULL,NULL);
/*!40000 ALTER TABLE `spotlist_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_rating_quality`
--

DROP TABLE IF EXISTS `spotlist_rating_quality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_rating_quality` (
  `spotlist_rating_quality_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_rating_quality_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating_from` decimal(3,1) DEFAULT NULL,
  `rating_to` decimal(3,1) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_rating_quality_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_rating_quality`
--

LOCK TABLES `spotlist_rating_quality` WRITE;
/*!40000 ALTER TABLE `spotlist_rating_quality` DISABLE KEYS */;
/*!40000 ALTER TABLE `spotlist_rating_quality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_subscription_purchase_history`
--

DROP TABLE IF EXISTS `spotlist_subscription_purchase_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_subscription_purchase_history` (
  `spotlist_subscription_purchase_history_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_packages_id` int unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `purchase_date` datetime DEFAULT NULL,
  `amount_paid` decimal(10,2) DEFAULT NULL,
  `payment_method` text COLLATE utf8mb4_unicode_ci,
  `purchase_log_data` text COLLATE utf8mb4_unicode_ci,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`spotlist_subscription_purchase_history_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_subscription_purchase_history`
--

LOCK TABLES `spotlist_subscription_purchase_history` WRITE;
/*!40000 ALTER TABLE `spotlist_subscription_purchase_history` DISABLE KEYS */;
INSERT INTO `spotlist_subscription_purchase_history` VALUES (1,1,5,'2020-10-13 06:24:43',0.00,'Card','{\"amount\":0,\"description\":\"User free subscription\"}','Free','Active','2020-10-13 06:24:43',NULL),(2,1,20,'2020-10-15 06:41:48',0.00,'Card','{\"amount\":0,\"description\":\"User free subscription\"}','Free','Active','2020-10-15 06:41:48',NULL),(3,2,20,'2020-10-15 12:52:08',29.00,'Card','{\"success\":true,\"transaction\":{\"id\":\"nskrf610\",\"status\":\"submitted_for_settlement\",\"type\":\"sale\",\"currencyIsoCode\":\"USD\",\"amount\":\"29.00\",\"merchantAccountId\":\"itechsoftsolutions\",\"subMerchantAccountId\":null,\"masterMerchantAccountId\":null,\"orderId\":null,\"createdAt\":{\"date\":\"2020-10-15 12:52:07.000000\",\"timezone_type\":3,\"timezone\":\"UTC\"},\"updatedAt\":{\"date\":\"2020-10-15 12:52:07.000000\",\"timezone_type\":3,\"timezone\":\"UTC\"},\"customer\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"company\":null,\"email\":null,\"website\":null,\"phone\":null,\"fax\":null},\"billing\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"company\":null,\"streetAddress\":null,\"extendedAddress\":null,\"locality\":null,\"region\":null,\"postalCode\":null,\"countryName\":null,\"countryCodeAlpha2\":null,\"countryCodeAlpha3\":null,\"countryCodeNumeric\":null},\"refundId\":null,\"refundIds\":[],\"refundedTransactionId\":null,\"partialSettlementTransactionIds\":[],\"authorizedTransactionId\":null,\"settlementBatchId\":null,\"shipping\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"company\":null,\"streetAddress\":null,\"extendedAddress\":null,\"locality\":null,\"region\":null,\"postalCode\":null,\"countryName\":null,\"countryCodeAlpha2\":null,\"countryCodeAlpha3\":null,\"countryCodeNumeric\":null},\"customFields\":null,\"avsErrorResponseCode\":null,\"avsPostalCodeResponseCode\":\"I\",\"avsStreetAddressResponseCode\":\"I\",\"cvvResponseCode\":\"I\",\"gatewayRejectionReason\":null,\"processorAuthorizationCode\":\"NYR5ZJ\",\"processorResponseCode\":\"1000\",\"processorResponseText\":\"Approved\",\"additionalProcessorResponse\":null,\"voiceReferralNumber\":null,\"purchaseOrderNumber\":null,\"taxAmount\":null,\"taxExempt\":false,\"processedWithNetworkToken\":false,\"creditCard\":{\"token\":null,\"bin\":\"424242\",\"last4\":\"4242\",\"cardType\":\"Visa\",\"expirationMonth\":\"02\",\"expirationYear\":\"2025\",\"customerLocation\":\"US\",\"cardholderName\":null,\"imageUrl\":\"https:\\/\\/assets.braintreegateway.com\\/payment_method_logo\\/visa.png?environment=sandbox\",\"prepaid\":\"Unknown\",\"healthcare\":\"Unknown\",\"debit\":\"Unknown\",\"durbinRegulated\":\"Unknown\",\"commercial\":\"Unknown\",\"payroll\":\"Unknown\",\"issuingBank\":\"Unknown\",\"countryOfIssuance\":\"Unknown\",\"productId\":\"Unknown\",\"globalId\":null,\"accountType\":null,\"uniqueNumberIdentifier\":null,\"venmoSdk\":false},\"statusHistory\":[{},{}],\"planId\":null,\"subscriptionId\":null,\"subscription\":{\"billingPeriodEndDate\":null,\"billingPeriodStartDate\":null},\"addOns\":[],\"discounts\":[],\"descriptor\":{},\"recurring\":false,\"channel\":null,\"serviceFeeAmount\":null,\"escrowStatus\":null,\"disbursementDetails\":{},\"disputes\":[],\"authorizationAdjustments\":[],\"paymentInstrumentType\":\"credit_card\",\"processorSettlementResponseCode\":null,\"processorSettlementResponseText\":null,\"networkResponseCode\":\"XX\",\"networkResponseText\":\"sample network response text\",\"threeDSecureInfo\":null,\"shipsFromPostalCode\":null,\"shippingAmount\":null,\"discountAmount\":null,\"networkTransactionId\":\"020201015125207\",\"processorResponseType\":\"approved\",\"authorizationExpiresAt\":{\"date\":\"2020-10-22 12:52:07.000000\",\"timezone_type\":3,\"timezone\":\"UTC\"},\"refundGlobalIds\":[],\"partialSettlementTransactionGlobalIds\":[],\"refundedTransactionGlobalId\":null,\"authorizedTransactionGlobalId\":null,\"globalId\":\"dHJhbnNhY3Rpb25fbnNrcmY2MTA\",\"retryIds\":[],\"retriedTransactionId\":null,\"retrievalReferenceNumber\":\"1234567\",\"installmentCount\":null,\"responseEmvData\":null,\"creditCardDetails\":{},\"customerDetails\":{},\"billingDetails\":{},\"shippingDetails\":{},\"subscriptionDetails\":{}}}','Paid','Active','2020-10-15 12:52:08',NULL),(4,2,20,'2020-10-16 06:36:24',29.00,'Card','{\"success\":true,\"transaction\":{\"id\":\"g8tf1ggv\",\"status\":\"submitted_for_settlement\",\"type\":\"sale\",\"currencyIsoCode\":\"USD\",\"amount\":\"29.00\",\"merchantAccountId\":\"itechsoftsolutions\",\"subMerchantAccountId\":null,\"masterMerchantAccountId\":null,\"orderId\":null,\"createdAt\":{\"date\":\"2020-10-16 06:36:24.000000\",\"timezone_type\":3,\"timezone\":\"UTC\"},\"updatedAt\":{\"date\":\"2020-10-16 06:36:24.000000\",\"timezone_type\":3,\"timezone\":\"UTC\"},\"customer\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"company\":null,\"email\":null,\"website\":null,\"phone\":null,\"fax\":null},\"billing\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"company\":null,\"streetAddress\":null,\"extendedAddress\":null,\"locality\":null,\"region\":null,\"postalCode\":null,\"countryName\":null,\"countryCodeAlpha2\":null,\"countryCodeAlpha3\":null,\"countryCodeNumeric\":null},\"refundId\":null,\"refundIds\":[],\"refundedTransactionId\":null,\"partialSettlementTransactionIds\":[],\"authorizedTransactionId\":null,\"settlementBatchId\":null,\"shipping\":{\"id\":null,\"firstName\":null,\"lastName\":null,\"company\":null,\"streetAddress\":null,\"extendedAddress\":null,\"locality\":null,\"region\":null,\"postalCode\":null,\"countryName\":null,\"countryCodeAlpha2\":null,\"countryCodeAlpha3\":null,\"countryCodeNumeric\":null},\"customFields\":null,\"avsErrorResponseCode\":null,\"avsPostalCodeResponseCode\":\"I\",\"avsStreetAddressResponseCode\":\"I\",\"cvvResponseCode\":\"I\",\"gatewayRejectionReason\":null,\"processorAuthorizationCode\":\"NH8QC3\",\"processorResponseCode\":\"1000\",\"processorResponseText\":\"Approved\",\"additionalProcessorResponse\":null,\"voiceReferralNumber\":null,\"purchaseOrderNumber\":null,\"taxAmount\":null,\"taxExempt\":false,\"processedWithNetworkToken\":false,\"creditCard\":{\"token\":null,\"bin\":\"555555\",\"last4\":\"4444\",\"cardType\":\"MasterCard\",\"expirationMonth\":\"02\",\"expirationYear\":\"2023\",\"customerLocation\":\"US\",\"cardholderName\":null,\"imageUrl\":\"https:\\/\\/assets.braintreegateway.com\\/payment_method_logo\\/mastercard.png?environment=sandbox\",\"prepaid\":\"Unknown\",\"healthcare\":\"Unknown\",\"debit\":\"Unknown\",\"durbinRegulated\":\"Unknown\",\"commercial\":\"Unknown\",\"payroll\":\"Unknown\",\"issuingBank\":\"Unknown\",\"countryOfIssuance\":\"Unknown\",\"productId\":\"Unknown\",\"globalId\":null,\"accountType\":null,\"uniqueNumberIdentifier\":null,\"venmoSdk\":false},\"statusHistory\":[{},{}],\"planId\":null,\"subscriptionId\":null,\"subscription\":{\"billingPeriodEndDate\":null,\"billingPeriodStartDate\":null},\"addOns\":[],\"discounts\":[],\"descriptor\":{},\"recurring\":false,\"channel\":null,\"serviceFeeAmount\":null,\"escrowStatus\":null,\"disbursementDetails\":{},\"disputes\":[],\"authorizationAdjustments\":[],\"paymentInstrumentType\":\"credit_card\",\"processorSettlementResponseCode\":null,\"processorSettlementResponseText\":null,\"networkResponseCode\":\"XX\",\"networkResponseText\":\"sample network response text\",\"threeDSecureInfo\":null,\"shipsFromPostalCode\":null,\"shippingAmount\":null,\"discountAmount\":null,\"networkTransactionId\":\"020201016063624\",\"processorResponseType\":\"approved\",\"authorizationExpiresAt\":{\"date\":\"2020-10-23 06:36:24.000000\",\"timezone_type\":3,\"timezone\":\"UTC\"},\"refundGlobalIds\":[],\"partialSettlementTransactionGlobalIds\":[],\"refundedTransactionGlobalId\":null,\"authorizedTransactionGlobalId\":null,\"globalId\":\"dHJhbnNhY3Rpb25fZzh0ZjFnZ3Y\",\"retryIds\":[],\"retriedTransactionId\":null,\"retrievalReferenceNumber\":\"1234567\",\"installmentCount\":null,\"responseEmvData\":null,\"creditCardDetails\":{},\"customerDetails\":{},\"billingDetails\":{},\"shippingDetails\":{},\"subscriptionDetails\":{}}}','Paid','Active','2020-10-16 06:36:24',NULL),(5,2,20,'2020-10-16 07:19:54',29.00,'Card','{\"id\":\"ch_1HcnEDLWWCPWHM8DhbpQ9aHn\",\"object\":\"charge\",\"amount\":2900,\"amount_captured\":2900,\"amount_refunded\":0,\"application\":null,\"application_fee\":null,\"application_fee_amount\":null,\"balance_transaction\":\"txn_1HcnEELWWCPWHM8DxSZCWRNv\",\"billing_details\":{\"address\":{\"city\":null,\"country\":null,\"line1\":null,\"line2\":null,\"postal_code\":null,\"state\":null},\"email\":null,\"name\":null,\"phone\":null},\"calculated_statement_descriptor\":\"Stripe\",\"captured\":true,\"created\":1602832793,\"currency\":\"usd\",\"customer\":null,\"description\":\"Test payment from itsolutionstuff.com.\",\"destination\":null,\"dispute\":null,\"disputed\":false,\"failure_code\":null,\"failure_message\":null,\"fraud_details\":[],\"invoice\":null,\"livemode\":false,\"metadata\":[],\"on_behalf_of\":null,\"order\":null,\"outcome\":{\"network_status\":\"approved_by_network\",\"reason\":null,\"risk_level\":\"normal\",\"risk_score\":63,\"seller_message\":\"Payment complete.\",\"type\":\"authorized\"},\"paid\":true,\"payment_intent\":null,\"payment_method\":\"card_1HcnECLWWCPWHM8DeAxuR6rQ\",\"payment_method_details\":{\"card\":{\"brand\":\"visa\",\"checks\":{\"address_line1_check\":null,\"address_postal_code_check\":null,\"cvc_check\":\"pass\"},\"country\":\"US\",\"exp_month\":12,\"exp_year\":2024,\"fingerprint\":\"25yBVU4zcDoISmBQ\",\"funding\":\"credit\",\"installments\":null,\"last4\":\"4242\",\"network\":\"visa\",\"three_d_secure\":null,\"wallet\":null},\"type\":\"card\"},\"receipt_email\":null,\"receipt_number\":null,\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1G0BlwLWWCPWHM8D\\/ch_1HcnEDLWWCPWHM8DhbpQ9aHn\\/rcpt_IDDfAhapSESH9RAThk4sOWc355qsmDQ\",\"refunded\":false,\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1HcnEDLWWCPWHM8DhbpQ9aHn\\/refunds\"},\"review\":null,\"shipping\":null,\"source\":{\"id\":\"card_1HcnECLWWCPWHM8DeAxuR6rQ\",\"object\":\"card\",\"address_city\":null,\"address_country\":null,\"address_line1\":null,\"address_line1_check\":null,\"address_line2\":null,\"address_state\":null,\"address_zip\":null,\"address_zip_check\":null,\"brand\":\"Visa\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":12,\"exp_year\":2024,\"fingerprint\":\"25yBVU4zcDoISmBQ\",\"funding\":\"credit\",\"last4\":\"4242\",\"metadata\":[],\"name\":null,\"tokenization_method\":null},\"source_transfer\":null,\"statement_descriptor\":null,\"statement_descriptor_suffix\":null,\"status\":\"succeeded\",\"transfer_data\":null,\"transfer_group\":null}','Paid','Active','2020-10-16 07:19:54',NULL),(6,3,5,'2020-10-19 11:01:24',49.00,'Card','{\"id\":\"ch_1Hdw7DLWWCPWHM8DEps12R8U\",\"object\":\"charge\",\"amount\":4900,\"amount_captured\":4900,\"amount_refunded\":0,\"application\":null,\"application_fee\":null,\"application_fee_amount\":null,\"balance_transaction\":\"txn_1Hdw7ELWWCPWHM8DubbGAwUh\",\"billing_details\":{\"address\":{\"city\":null,\"country\":null,\"line1\":null,\"line2\":null,\"postal_code\":null,\"state\":null},\"email\":null,\"name\":null,\"phone\":null},\"calculated_statement_descriptor\":\"Stripe\",\"captured\":true,\"created\":1603105283,\"currency\":\"usd\",\"customer\":null,\"description\":\"Test payment from itsolutionstuff.com.\",\"destination\":null,\"dispute\":null,\"disputed\":false,\"failure_code\":null,\"failure_message\":null,\"fraud_details\":[],\"invoice\":null,\"livemode\":false,\"metadata\":[],\"on_behalf_of\":null,\"order\":null,\"outcome\":{\"network_status\":\"approved_by_network\",\"reason\":null,\"risk_level\":\"normal\",\"risk_score\":63,\"seller_message\":\"Payment complete.\",\"type\":\"authorized\"},\"paid\":true,\"payment_intent\":null,\"payment_method\":\"card_1Hdw7CLWWCPWHM8Din7JA80o\",\"payment_method_details\":{\"card\":{\"brand\":\"visa\",\"checks\":{\"address_line1_check\":null,\"address_postal_code_check\":null,\"cvc_check\":\"pass\"},\"country\":\"US\",\"exp_month\":12,\"exp_year\":2024,\"fingerprint\":\"25yBVU4zcDoISmBQ\",\"funding\":\"credit\",\"installments\":null,\"last4\":\"4242\",\"network\":\"visa\",\"three_d_secure\":null,\"wallet\":null},\"type\":\"card\"},\"receipt_email\":null,\"receipt_number\":null,\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1G0BlwLWWCPWHM8D\\/ch_1Hdw7DLWWCPWHM8DEps12R8U\\/rcpt_IEOvk6VAD6aaa5t7DWW8L6O1G2BH8J9\",\"refunded\":false,\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1Hdw7DLWWCPWHM8DEps12R8U\\/refunds\"},\"review\":null,\"shipping\":null,\"source\":{\"id\":\"card_1Hdw7CLWWCPWHM8Din7JA80o\",\"object\":\"card\",\"address_city\":null,\"address_country\":null,\"address_line1\":null,\"address_line1_check\":null,\"address_line2\":null,\"address_state\":null,\"address_zip\":null,\"address_zip_check\":null,\"brand\":\"Visa\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":12,\"exp_year\":2024,\"fingerprint\":\"25yBVU4zcDoISmBQ\",\"funding\":\"credit\",\"last4\":\"4242\",\"metadata\":[],\"name\":null,\"tokenization_method\":null},\"source_transfer\":null,\"statement_descriptor\":null,\"statement_descriptor_suffix\":null,\"status\":\"succeeded\",\"transfer_data\":null,\"transfer_group\":null}','Paid','Active','2020-10-19 11:01:24',NULL),(7,5,22,'2020-10-22 07:56:35',NULL,'Stripe',NULL,'Paid','Active','2020-10-22 07:56:35',NULL),(8,3,22,'2020-10-23 05:59:41',NULL,'Card',NULL,'Paid','Active','2020-10-23 05:59:41',NULL),(9,6,26,'2020-10-23 06:27:02',10.00,'Free','{\"amount\":0,\"description\":\"User free subscription\"}','Free','Active','2020-10-23 06:27:02',NULL),(10,5,26,'2020-10-23 06:31:17',NULL,'Cash on delivery',NULL,'Paid','Active','2020-10-23 06:31:17',NULL),(11,5,26,'2020-10-23 06:43:05',30.00,'Card','{\"id\":\"ch_1HfJzRLWWCPWHM8DKUjhRU9L\",\"object\":\"charge\",\"amount\":3000,\"amount_captured\":3000,\"amount_refunded\":0,\"application\":null,\"application_fee\":null,\"application_fee_amount\":null,\"balance_transaction\":\"txn_1HfJzRLWWCPWHM8D9pzyrJVB\",\"billing_details\":{\"address\":{\"city\":null,\"country\":null,\"line1\":null,\"line2\":null,\"postal_code\":null,\"state\":null},\"email\":null,\"name\":null,\"phone\":null},\"calculated_statement_descriptor\":\"Stripe\",\"captured\":true,\"created\":1603435385,\"currency\":\"usd\",\"customer\":null,\"description\":\"Test payment from itsolutionstuff.com.\",\"destination\":null,\"dispute\":null,\"disputed\":false,\"failure_code\":null,\"failure_message\":null,\"fraud_details\":[],\"invoice\":null,\"livemode\":false,\"metadata\":[],\"on_behalf_of\":null,\"order\":null,\"outcome\":{\"network_status\":\"approved_by_network\",\"reason\":null,\"risk_level\":\"normal\",\"risk_score\":20,\"seller_message\":\"Payment complete.\",\"type\":\"authorized\"},\"paid\":true,\"payment_intent\":null,\"payment_method\":\"card_1HfJzPLWWCPWHM8DA9PFKkiQ\",\"payment_method_details\":{\"card\":{\"brand\":\"visa\",\"checks\":{\"address_line1_check\":null,\"address_postal_code_check\":null,\"cvc_check\":\"pass\"},\"country\":\"US\",\"exp_month\":12,\"exp_year\":2024,\"fingerprint\":\"25yBVU4zcDoISmBQ\",\"funding\":\"credit\",\"installments\":null,\"last4\":\"4242\",\"network\":\"visa\",\"three_d_secure\":null,\"wallet\":null},\"type\":\"card\"},\"receipt_email\":null,\"receipt_number\":null,\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1G0BlwLWWCPWHM8D\\/ch_1HfJzRLWWCPWHM8DKUjhRU9L\\/rcpt_IFpekz7XrAQ0pUnEVElJ9CkjYYO85Kh\",\"refunded\":false,\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1HfJzRLWWCPWHM8DKUjhRU9L\\/refunds\"},\"review\":null,\"shipping\":null,\"source\":{\"id\":\"card_1HfJzPLWWCPWHM8DA9PFKkiQ\",\"object\":\"card\",\"address_city\":null,\"address_country\":null,\"address_line1\":null,\"address_line1_check\":null,\"address_line2\":null,\"address_state\":null,\"address_zip\":null,\"address_zip_check\":null,\"brand\":\"Visa\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":12,\"exp_year\":2024,\"fingerprint\":\"25yBVU4zcDoISmBQ\",\"funding\":\"credit\",\"last4\":\"4242\",\"metadata\":[],\"name\":null,\"tokenization_method\":null},\"source_transfer\":null,\"statement_descriptor\":null,\"statement_descriptor_suffix\":null,\"status\":\"succeeded\",\"transfer_data\":null,\"transfer_group\":null}','Paid','Active','2020-10-23 06:43:05',NULL),(12,5,22,'2020-10-23 06:44:53',NULL,'Card',NULL,'Paid','Active','2020-10-23 06:44:53',NULL),(13,6,29,'2020-10-27 07:04:10',10.00,'Free','{\"amount\":0,\"description\":\"User free subscription\"}','Free','Active','2020-10-27 07:04:10',NULL),(14,6,31,'2020-10-28 13:03:12',10.00,'Free','{\"amount\":0,\"description\":\"User free subscription\"}','Free','Active','2020-10-28 13:03:12',NULL),(15,3,31,'2020-10-28 13:09:52',NULL,'Cash on delivery',NULL,'Paid','Active','2020-10-28 13:09:52',NULL);
/*!40000 ALTER TABLE `spotlist_subscription_purchase_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_user_follower`
--

DROP TABLE IF EXISTS `spotlist_user_follower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_user_follower` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `follower_id` bigint unsigned NOT NULL,
  `is_follow` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_user_follower`
--

LOCK TABLES `spotlist_user_follower` WRITE;
/*!40000 ALTER TABLE `spotlist_user_follower` DISABLE KEYS */;
INSERT INTO `spotlist_user_follower` VALUES (1,19,20,1),(2,4,20,1),(3,19,2,1),(4,2,2,1),(5,28,22,1),(6,19,26,1),(7,19,22,1),(8,19,31,1);
/*!40000 ALTER TABLE `spotlist_user_follower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_user_message_table`
--

DROP TABLE IF EXISTS `spotlist_user_message_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_user_message_table` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `status` enum('Pending','Seen','Replied') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_user_message_table`
--

LOCK TABLES `spotlist_user_message_table` WRITE;
/*!40000 ALTER TABLE `spotlist_user_message_table` DISABLE KEYS */;
INSERT INTO `spotlist_user_message_table` VALUES (1,NULL,'Alemnda','Lidia','lidia@yopmail.com','Please help','Help','Replied','2020-10-16 11:05:03','2020-10-16 11:05:03'),(2,NULL,'Ana','Lidia','lidia11@yopmail.com','Problem','I need help','Replied','2020-10-28 13:49:24','2020-10-28 13:49:24'),(3,NULL,'Salman','Shafi','shafi@yopmail.com','Need to talk','I need some help','Replied','2020-10-29 07:41:46','2020-10-29 07:41:46');
/*!40000 ALTER TABLE `spotlist_user_message_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_user_subscription`
--

DROP TABLE IF EXISTS `spotlist_user_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_user_subscription` (
  `spotlist_user_subscription_id` int unsigned NOT NULL AUTO_INCREMENT,
  `spotlist_packages_id` int unsigned NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `day_remain` int DEFAULT NULL,
  `num_of_listing` int DEFAULT NULL,
  `listing_item_used` int DEFAULT NULL,
  `num_of_categories` int DEFAULT NULL,
  `category_used` int DEFAULT NULL,
  `num_of_tags` int DEFAULT NULL,
  `tag_used` int DEFAULT NULL,
  `num_of_photos` int DEFAULT NULL,
  `photos_used` int DEFAULT NULL,
  `add_video_ability` int DEFAULT NULL,
  `add_contact_form_ability` int DEFAULT NULL,
  `free_subscription_check` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`spotlist_user_subscription_id`),
  KEY `spotlist_user_subscription_user_id_foreign` (`user_id`),
  CONSTRAINT `spotlist_user_subscription_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `sys_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_user_subscription`
--

LOCK TABLES `spotlist_user_subscription` WRITE;
/*!40000 ALTER TABLE `spotlist_user_subscription` DISABLE KEYS */;
INSERT INTO `spotlist_user_subscription` VALUES (1,3,5,100,22,3,0,0,11,0,30,4,1,1,1,'Active'),(2,2,20,100,31,4,0,0,31,0,35,9,1,1,1,'Active'),(3,5,22,30,10,2,NULL,0,2,0,10,6,1,1,1,'Active'),(4,6,23,2,2,0,NULL,0,2,0,2,0,0,0,1,'Active'),(5,6,25,2,2,0,NULL,0,2,0,2,0,0,0,1,'Active'),(6,5,26,60,20,2,0,0,4,0,20,2,1,1,1,'Active'),(7,6,29,2,2,1,NULL,0,2,0,2,2,0,0,1,'Active'),(8,3,31,92,22,2,0,0,12,0,22,4,1,1,1,'Active');
/*!40000 ALTER TABLE `spotlist_user_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlist_user_subscription_by_email`
--

DROP TABLE IF EXISTS `spotlist_user_subscription_by_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlist_user_subscription_by_email` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','Suspended') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `spotlist_user_subscription_by_email_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlist_user_subscription_by_email`
--

LOCK TABLES `spotlist_user_subscription_by_email` WRITE;
/*!40000 ALTER TABLE `spotlist_user_subscription_by_email` DISABLE KEYS */;
INSERT INTO `spotlist_user_subscription_by_email` VALUES (1,'pedro@yopmail.com','Active',NULL,NULL),(2,'Lissa@yopmail.com','Active','2020-10-22 09:30:23',NULL),(3,'pinto@yopmail.com','Active','2020-10-22 09:30:37',NULL),(4,'Jose@yopmail.com','Active','2020-10-23 07:43:00',NULL),(5,'User@yopmail.com','Active','2020-10-29 09:03:55',NULL);
/*!40000 ALTER TABLE `spotlist_user_subscription_by_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlisting_teams`
--

DROP TABLE IF EXISTS `spotlisting_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spotlisting_teams` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlisting_teams`
--

LOCK TABLES `spotlisting_teams` WRITE;
/*!40000 ALTER TABLE `spotlisting_teams` DISABLE KEYS */;
INSERT INTO `spotlisting_teams` VALUES (2,'AnaLidia','Supervisor','Backend/images/teams/TeKb6Ksg4oUcXr1E.jpg','https://www.facebook.com/','https://www.facebook.com/','https://www.facebook.com/','https://www.facebook.com/','https://www.facebook.com/','Active'),(3,'Alice Lima','Moderator','Backend/images/teams/M9T0ri8h4IZ125Nc.jpg','https://www.facebook.com/',NULL,'https://www.facebook.com/',NULL,NULL,'Active');
/*!40000 ALTER TABLE `spotlisting_teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dropdowns`
--

DROP TABLE IF EXISTS `sys_dropdowns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dropdowns` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `dropdown_slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dropdown_mode` enum('dropdown','dropdown_grid') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'dropdown',
  `sys_search_panel_slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sqltext` text COLLATE utf8mb4_unicode_ci,
  `sqlsource` text COLLATE utf8mb4_unicode_ci,
  `sqlcondition` text COLLATE utf8mb4_unicode_ci,
  `sqlgroupby` text COLLATE utf8mb4_unicode_ci,
  `sqlhaving` text COLLATE utf8mb4_unicode_ci,
  `sqlorderby` text COLLATE utf8mb4_unicode_ci,
  `sqllimit` int DEFAULT NULL,
  `value_field` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_field` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `multiple` tinyint(1) DEFAULT NULL,
  `search_columns` text COLLATE utf8mb4_unicode_ci,
  `dropdown_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dropdowns`
--

LOCK TABLES `sys_dropdowns` WRITE;
/*!40000 ALTER TABLE `sys_dropdowns` DISABLE KEYS */;
INSERT INTO `sys_dropdowns` VALUES (12,'module-list','dropdown',NULL,'SELECT\nsys_modules.id,\nsys_modules.name','FROM\nsys_modules',NULL,NULL,NULL,NULL,NULL,'id','name',0,NULL,'sys_modules_id',NULL,NULL,NULL,'Active','2020-02-11 15:33:00',NULL),(13,'user_level','dropdown',NULL,'SELECT sys_user_levels.id,sys_user_levels.name ','FROM `sys_user_levels`',NULL,NULL,NULL,'ORDER BY sys_user_levels.id ASC',NULL,'id','name',0,NULL,'sys_user_level_id',NULL,NULL,NULL,'Active','2020-02-17 17:23:07',NULL),(14,'user_list','dropdown',NULL,'SELECT\nsys_users.id,\nsys_users.name\n\n','FROM\nsys_users',NULL,NULL,NULL,NULL,NULL,'id','name',0,NULL,'sys_user_id',NULL,NULL,NULL,'Active','2020-02-20 17:56:03',NULL),(15,'project_truck','dropdown',NULL,'SELECT project_truck.project_truck_id, project_truck.project_truck_name','FROM project_truck','WHERE `status` = \'Active\'',NULL,NULL,NULL,NULL,'project_truck_id','project_truck_name',0,NULL,'project_truck_id','',NULL,NULL,'Active','2020-06-13 04:03:53',NULL),(16,'parent_level','dropdown',NULL,'SELECT parent_level_id, parent_level_name FROM parent_level WHERE `status` = \'Active\'',NULL,NULL,NULL,NULL,NULL,NULL,'parent_level_id','parent_level_name',0,NULL,'parent_level_id','',NULL,NULL,'Active','2020-06-16 14:19:22',NULL),(18,'master_entry_dropdown','dropdown',NULL,'SELECT\nsys_master_entry.sys_master_entry_name','FROM\nsys_master_entry',NULL,NULL,NULL,NULL,NULL,'sys_master_entry_name','sys_master_entry_name',NULL,NULL,'sys_master_grid_name',NULL,1,NULL,'Active','2020-06-18 20:31:02',NULL),(19,'dropdown_slug','dropdown',NULL,'SELECT dropdown_slug','FROM sys_dropdowns',NULL,NULL,NULL,NULL,NULL,'dropdown_slug','dropdown_slug',0,NULL,'dropdown_slug',NULL,NULL,NULL,'Active','2020-06-19 17:57:10',NULL),(20,'search_panel_slug','dropdown',NULL,'select search_panel_name','from sys_search_panel',NULL,NULL,NULL,NULL,NULL,'search_panel_name','search_panel_name',NULL,NULL,'Search Panel Dropdown List',NULL,1,NULL,'Active','2020-06-20 21:44:51',NULL),(21,'spotlist_parent_category','dropdown',NULL,'SELECT spotlist_category_id as parent_id,spotlist_category_name as parent_name','from spotlist_category ','WHERE `status` = \'Active\' AND parent_id = 0',NULL,NULL,NULL,NULL,'parent_id','parent_name',0,NULL,'parent_id','',NULL,NULL,'Active',NULL,NULL),(22,'spotlist_category','dropdown',NULL,'SELECT spotlist_category_id, spotlist_category_name','from spotlist_category ','WHERE `status` = \'Active\'',NULL,NULL,NULL,NULL,'spotlist_category_id','spotlist_category_name',NULL,NULL,'spotlist_category_id',NULL,NULL,NULL,'Active',NULL,NULL),(23,'customer_list','dropdown',NULL,'SELECT\nsys_users.id,\nsys_users.name\n\n','FROM\nsys_users','WHERE  default_module_id = 4',NULL,NULL,NULL,NULL,'id','name',0,NULL,'sys_user_id',NULL,NULL,NULL,'Active','2020-02-20 17:56:03',NULL),(24,'spotlist_packages','dropdown',NULL,'SELECT\nspotlist_packages.spotlist_packages_id, \nspotlist_packages.spotlist_packages_name, \nspotlist_packages.price','FROM\nspotlist_packages','WHERE `status` = \'Active\'',NULL,NULL,NULL,NULL,'spotlist_packages_id','spotlist_packages_name',0,NULL,'spotlist_packages_id',NULL,NULL,NULL,'Active','2020-02-20 17:56:03',NULL),(25,'spotlist_countries','dropdown',NULL,'SELECT *','FROM\nspotlist_countries','',NULL,NULL,NULL,NULL,'id','name',0,NULL,'country_id',NULL,NULL,NULL,'Active','2020-02-20 17:56:03',NULL);
/*!40000 ALTER TABLE `sys_dropdowns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_system_settings`
--

DROP TABLE IF EXISTS `sys_system_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_system_settings` (
  `sys_system_settings_id` int unsigned NOT NULL AUTO_INCREMENT,
  `option_group` enum('application_settings','authentication','payment_settings','admin_settings','frontend_settings','about_us_settings') COLLATE utf8mb4_unicode_ci DEFAULT 'application_settings',
  `option_key` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` text COLLATE utf8mb4_unicode_ci,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci DEFAULT 'Active',
  `created_at` datetime DEFAULT NULL,
  `created_by` int DEFAULT '1',
  PRIMARY KEY (`sys_system_settings_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_system_settings`
--

LOCK TABLES `sys_system_settings` WRITE;
/*!40000 ALTER TABLE `sys_system_settings` DISABLE KEYS */;
INSERT INTO `sys_system_settings` VALUES (1,'admin_settings','notification','On','Active',NULL,1),(2,'admin_settings','qwe','qwe qwe','Active',NULL,1),(3,'admin_settings','layout','detached','Active',NULL,1),(4,'admin_settings','theme','light','Active',NULL,1),(5,'admin_settings','theme_width','fluid','Active',NULL,1),(6,'admin_settings','menu_position','fixed','Active',NULL,1),(7,'admin_settings','sidebar_theme','light','Active',NULL,1),(8,'admin_settings','sidebar_size','default','Active',NULL,1),(9,'admin_settings','sidbar_show_user','true','Active',NULL,1),(10,'admin_settings','theme_topbar','dark','Active',NULL,1),(18,'admin_settings','favicon','Backend/images/favicon.ico','Active',NULL,1),(19,'admin_settings','dark_logo_large','Backend/images/dark_logo_large.jpg','Active',NULL,1),(20,'admin_settings','dark_logo_small','Backend/images/dark_logo_small.jpg','Active',NULL,1),(21,'admin_settings','light_logo_large','Backend/images/light_logo_large.jpg','Active',NULL,1),(22,'admin_settings','light_logo_small','Backend/images/light_logo_small.jpg','Active',NULL,1),(23,'admin_settings','copyright','<i class=\"fa fa-copyright\"></i> asdasdasd','Active',NULL,1),(30,'application_settings','file_upload_test_name','12','Active',NULL,1),(31,'application_settings','doc_file','Backend/storage/pcidI9Sp3Umy7gmb.jpg','Active',NULL,1),(32,'application_settings','status','Active','Active',NULL,1),(33,'application_settings','file_image','Backend/images/qxtGDTVmRSvvT86F.docx','Active',NULL,1),(34,'application_settings','theme_settings','On','Active',NULL,1),(36,'application_settings','app_notification','Off','Active',NULL,1),(37,'application_settings','site_title','Lara Charm','Active',NULL,1),(38,'application_settings','app_lang','Off','Active',NULL,1),(50,'admin_settings','system_title','rewrewwer','Active',NULL,1),(51,'frontend_settings','status','Active','Active',NULL,1),(52,'frontend_settings','app_title','App titlle','Active',NULL,1),(53,'frontend_settings','home_page_title','Home page title','Active',NULL,1),(54,'frontend_settings','contact_email','spotlisting@gmail.com','Active',NULL,1),(55,'frontend_settings','contact_number','239432882','Active',NULL,1),(56,'frontend_settings','contact_address','15 KDA, khulna','Active',NULL,1),(57,'frontend_settings','facebook_link','www.facebook.com','Active',NULL,1),(58,'frontend_settings','twitter_link','www.twitter.com','Active',NULL,1),(59,'frontend_settings','instagram_link','www.instagram.com','Active',NULL,1),(60,'frontend_settings','google_link','www.google.com','Active',NULL,1),(61,'frontend_settings','description','some text will be written','Active',NULL,1),(62,'frontend_settings','footer_descrpition','Spotlisting, the world’s largest travel platform, makes every trip their best trip. Travelers around the world can easily use the spotlisting site to book different places, restaurants, beauty salons, experiences, and gyms.','Active',NULL,1),(63,'frontend_settings','copy_right_text','@copyright 2020','Active',NULL,1),(64,'frontend_settings','about_us','some text will be written about us','Active',NULL,1),(65,'frontend_settings','google_location','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.5873335784217!2d89.55269461195519!3d22.817750010413683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff90068a55a627%3A0x38f7c151c31a27ab!2sMutual%20Trust%20Bank%20Limited!5e0!3m2!1sen!2sbd!4v1600859967553!5m2!1sen!2sbd\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>','Active',NULL,1),(66,'frontend_settings','app_logo','Backend/images/application/uQye6H9IMaZsg4SK.png','Active',NULL,1),(67,'frontend_settings','favicon_logo','Backend/images/application/DVGIBqwNYVnr2ktB.png','Active',NULL,1),(68,'payment_settings','payment_method','stripe','Active',NULL,1),(69,'payment_settings','brain_tree_env','sandbox','Active',NULL,1),(70,'payment_settings','brain_tree_merchant_id','fyjwt74tcs8qmfvb','Active',NULL,1),(71,'payment_settings','brain_tree_public_key','v7qwrrmzjmvchh3r','Active',NULL,1),(72,'payment_settings','brain_tree_private_key','1150261b4483675ab74191d525ea5533','Active',NULL,1),(73,'payment_settings','stripe_key','pk_test_atkU8vZV2ey9RLLcCMEfSrXg00x2Pv7LmR','Active',NULL,1),(74,'payment_settings','stripe_secret','sk_test_Rplhic0MGNCgjgGvwDSvtvaE00vq1N5e6q','Active',NULL,1),(75,'about_us_settings','about_us_title','About Us','Active',NULL,1),(76,'about_us_settings','about_us_description','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English.\r\n\r\nMany desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable.','Active',NULL,1),(77,'about_us_settings','about_us_picture','Backend/images/about_us/sZaZf6891SaTTr0k.jpg','Active',NULL,1),(78,'admin_settings','app_title','Spotlisting Admin','Active',NULL,1),(79,'admin_settings','contact_email','info@itech.international','Active',NULL,1),(80,'admin_settings','contact_number','0213345663','Active',NULL,1),(81,'admin_settings','description','some text will be written here','Active',NULL,1),(82,'admin_settings','copy_right_text','@copyright2020','Active',NULL,1),(83,'admin_settings','about_us','Some text will be written here','Active',NULL,1),(84,'admin_settings','google_location','USA','Active',NULL,1),(85,'admin_settings','favicon_logo','Backend/images/application/HgcKxELjIhIRz2pT.png','Active',NULL,1),(86,'admin_settings','app_logo_large','Backend/images/application/ZdCBkg4wWzUcVBYD.png','Active',NULL,1),(87,'admin_settings','app_logo_small','Backend/images/application/wGxzi9bgnqgfdjQc.png','Active',NULL,1),(88,'admin_settings','social_login_enable','1','Active',NULL,1),(89,'admin_settings','email_verification_enable','1','Active',NULL,1),(90,'admin_settings','currency','BDT','Active',NULL,1),(91,'frontend_settings','latitude','22.819009982916523','Active',NULL,1),(92,'frontend_settings','longitude','89.5512269327881','Active',NULL,1);
/*!40000 ALTER TABLE `sys_system_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_users`
--

DROP TABLE IF EXISTS `sys_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Employee ID',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_key` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified` tinyint(1) NOT NULL DEFAULT '0',
  `email_verified_at` datetime DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `blood_group` enum('A+','A-','B+','B-','O+','O-','AB+','AB-','') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('Female','Male') COLLATE utf8mb4_unicode_ci DEFAULT 'Male',
  `marital_status` enum('Married','Unmarried','Devorced','Widow','Single') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'NID or Birth certificate id will store in this same field',
  `tin` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '/img/users/Avatar.png',
  `address` text COLLATE utf8mb4_unicode_ci COMMENT 'Present Address',
  `last_login` datetime DEFAULT NULL,
  `date_of_join` date DEFAULT NULL,
  `default_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_module_id` tinyint DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_changed_date` timestamp NULL DEFAULT NULL,
  `wrong_attempts_count` int DEFAULT '0',
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('Active','Pending','Suspended','Blocked','Deleted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_users`
--

LOCK TABLES `sys_users` WRITE;
/*!40000 ALTER TABLE `sys_users` DISABLE KEYS */;
INSERT INTO `sys_users` VALUES (1,'','Super Admin',NULL,'superadmin@gmail.com','$2y$10$Mdpv8wuH7XyOR7OsA.0fQ.Qo6X5zwiitDLxe4LycvegQYRT5IxdVS','','Nirjhar Mandal',0,NULL,'+8801855149196','1993-01-04','','Male','Married','','','','','',NULL,'','Nikunjo, Uttra, Dhaka',NULL,'2019-01-07','',1,'aybPFmCgv1qNa1ulBgVnTjURcACfeZnxvJ9HksbuAEcIjqIsNMAUpxnMCFzj','2020-01-01 03:50:27',0,NULL,1,'2019-05-05 11:15:53','2020-06-24 03:01:53','Active'),(2,NULL,'Sujoy Nath','softwaredeveloper','admin@gmail.com','$2y$10$qhgMGkbyDCU0WDRo6ZT/H.dFBrWlWGf6zWooF.gpwJqARdyXENwbS',NULL,'Sujoy Nath',0,NULL,'22131234',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/gBZGm4aXfgo6d6aK.jpg','sdfsdfsdf',NULL,NULL,NULL,2,NULL,'2020-09-22 06:36:01',0,NULL,NULL,'2020-09-22 06:36:01','2020-09-22 06:36:01','Active'),(3,NULL,'Listing Manager',NULL,'manager@gmail.com','$2y$10$Mdpv8wuH7XyOR7OsA.0fQ.Qo6X5zwiitDLxe4LycvegQYRT5IxdVS',NULL,'Test demo',0,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,3,NULL,'2020-09-22 07:35:43',0,NULL,NULL,'2020-09-22 07:35:43','2020-09-22 07:35:43','Active'),(4,NULL,'Sujoy Nath','software developer','user@gmail.com','$2y$10$Mdpv8wuH7XyOR7OsA.0fQ.Qo6X5zwiitDLxe4LycvegQYRT5IxdVS',NULL,'Sujoy Nath',0,NULL,'21312555',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/5tAMIa9kmLqTHnWO.jpg','sdfds',NULL,NULL,NULL,4,NULL,'2020-09-22 07:43:52',0,NULL,NULL,'2020-09-22 07:43:52','2020-09-22 07:43:52','Active'),(5,NULL,'List manager Test',NULL,'listmanagertest@gmail.com','$2y$10$wqxrpE7WXUEP5Y9q0DLrB.xcWWfezlix8j7HhBXaeG78b1kbzxXXC',NULL,'List manager Test',0,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,3,NULL,'2020-10-13 06:23:33',0,NULL,NULL,'2020-10-13 06:23:33','2020-10-13 06:23:33','Active'),(17,NULL,'Sujoy Nath',NULL,'sujoy.csesust@gmail.com','$2y$10$gztBxrCzTV1BxHGJ8J20HePnPivbACl2d/mNjw9R6jZlsNlcmkeBK',NULL,'Sujoy Nath',1,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,4,NULL,'2020-10-14 05:07:20',0,NULL,NULL,'2020-10-14 05:07:20','2020-10-14 05:08:04','Active'),(18,NULL,'Sujoy Nath',NULL,'sujoy.cse11@gmail.com','$2y$10$bs84pF3Lkh6d883OoTYPWOFiVctva7zXTF/ax7vGJ7AE5zlr.xVOC',NULL,'Sujoy Nath',0,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,4,NULL,'2020-10-14 05:09:48',0,NULL,NULL,'2020-10-14 05:09:48','2020-11-17 11:53:55','Active'),(19,NULL,'Almenda Lidia','Engineer','lidia@yopmail.com','$2y$10$wjvijYs6VSaNPOJTCoJD3eL6gTqEcUzoOlKI8bkbDDjeoH0g1srqW',NULL,'Almenda Lidia',0,NULL,'01988982892',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/k6drJ2svmyFRtNEQ.jpg','Manhattan, New York, USA',NULL,NULL,NULL,4,NULL,'2020-10-14 13:27:21',0,NULL,NULL,'2020-10-14 13:27:21','2020-10-14 13:27:21','Active'),(20,NULL,'Thiago Silva',NULL,'Silva@yopmail.com','$2y$10$N8hcAy9jHpLMQL2RLSbCL.ajUfZb2dxuiRvkrhW6gn06jWcFG.ARO',NULL,'Thiago Silva',0,NULL,'asdfdfasdfsfasdfasdf',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/Y2S69FCOB4zboKnM.jpg','adsfasdf',NULL,NULL,NULL,3,NULL,'2020-10-15 05:32:06',0,NULL,NULL,'2020-10-15 05:32:06','2020-10-16 12:55:55','Active'),(21,NULL,'Lima Isa',NULL,'lima@yopmail.com','$2y$10$Tx9tg7a.JzK.lOngMUASVOjAm9PtTmE1GBWmDv6bEOa8WsfnlfuAS',NULL,'Lima Isa',0,NULL,'0147852369',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/VQrJNFhHEMP4umqR.jpg',NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL,'2020-10-16 12:58:28','2020-10-16 12:58:28','Active'),(22,NULL,'Shafi Felipno','Head','shafi@yopmail.com','$2y$10$iunUV2onsTFHYVy22uau8.b4S6owq4Y54eg2sgd.7kU39O3vWXLU2',NULL,'Shafi Felipno',0,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/w4dx5VZXykUXqXFL.jpg',NULL,NULL,NULL,NULL,3,NULL,'2020-10-22 07:49:44',0,NULL,NULL,'2020-10-22 07:49:44','2020-10-22 07:49:44','Active'),(23,NULL,'Almenda Lima',NULL,'almenda@yopmail.com','$2y$10$g6W77omPWGtwLPEJRI9ACOO2aRY/vJhnVNVlFpPJNLGGK/6iIWhQ6',NULL,'Almenda Lima',0,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,3,NULL,'2020-10-22 09:15:23',0,NULL,NULL,'2020-10-22 09:15:23','2020-10-22 09:15:23','Active'),(24,NULL,'AmaLIma',NULL,'limaisa@yopmail.com','$2y$10$UmESrCC3xtZUJSKjx/rXp.rSjBpZ6FeVkhvvMw/n2Nc7Oe7ax9nF2',NULL,'AmaLIma',0,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,4,NULL,'2020-10-22 09:20:57',0,NULL,NULL,'2020-10-22 09:20:57','2020-10-22 09:20:57','Active'),(25,NULL,'AmaLIma',NULL,'limaisa1@yopmail.com','$2y$10$dNtmOXsMboxRSukfJnGtZexkMzFWJhgnjLiTcyaC2Izj.T8iGQgGa',NULL,'AmaLIma',0,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,3,NULL,'2020-10-22 09:21:20',0,NULL,NULL,'2020-10-22 09:21:20','2020-10-22 09:21:20','Active'),(26,NULL,'Samuel Lima','Manager','Samuel@yopmail.com','$2y$10$sjEr1qCCEWhLPWiECCuoNOENHNLc6F2E4ZwalEI34hX7fpCJ6a71G',NULL,'Samuel Lima',0,NULL,'012365478',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/FEkStOLCHC5tzTj6.png','122',NULL,NULL,NULL,3,'225df0c9851746e226c267839eacefbf','2020-10-23 05:59:05',0,NULL,NULL,'2020-10-23 05:59:05','2020-10-23 07:33:17','Active'),(27,NULL,'IsaLima',NULL,'Isa@yopmail.com','$2y$10$Cqu6Mxpu.dOrI65qD1cQA.Ifk4vQQVY9t8yA5SJv7lTdoqx.rKQ2C',NULL,'IsaLima',0,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,3,'ce5d4272a5b249ffdb67a0cde34ea00c','2020-10-23 09:32:25',0,NULL,NULL,'2020-10-23 09:32:25','2020-10-23 09:32:25','Active'),(28,NULL,'Sofia Cavalcanti','User','sofia@yopmail.com','$2y$10$N8NcNOYb3B2cIcgEaQZ6eOFtfP8WhqndNa9Zv2qtDDHC5tigC8h.m',NULL,'Sofia Cavalcanti',1,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/EX9BIheg146Jihn6.jpg',NULL,NULL,NULL,NULL,4,'0ba109f88d8c5b4cc72adbeefae9e6df','2020-10-23 10:10:29',0,NULL,NULL,'2020-10-23 10:10:29','2020-10-29 14:00:10','Active'),(29,NULL,'Samuel Silva',NULL,'Samuel1@yopmail.com','$2y$10$0U33Iw/JVU1sDn9k08z7JuM1EHx2KRO/h6golcyszE5mMQtmvJhMe',NULL,'Samuel Silva',1,NULL,'01326547',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,3,'a871797303719d24a05d48effa08801a',NULL,0,NULL,NULL,'2020-10-27 06:58:15','2020-10-27 07:02:00','Suspended'),(31,NULL,'Rebeca Dias','Listing Manager','dias@yopmail.com','$2y$10$peqf83pwlCriInLmnUPRUOd2UlwWSiNGTlAUJtCDy01KjSEi5AOe2',NULL,'Rebeca Dias',1,NULL,'012365478',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/2IGly7bF38RPV9sg.jpg','B-42',NULL,NULL,NULL,3,'TlbXCjXX40I2CcVDjo5VSzksDjytVa6w7zYrbMnVUlKf7rlshVn9cSrT5ToE',NULL,0,NULL,NULL,'2020-10-28 12:53:33','2020-10-28 12:53:33','Active'),(33,NULL,'Castro Luan',NULL,'castro@yopmail.com','$2y$10$ErRb9C9v7CiFH6XXcRRueuRQVS8X5oLo1Sl46yT9GHPmT4OMQmBZS',NULL,'Castro Luan',1,NULL,'01718538814',NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/6cFJfjwHwYZlHL04.jpg','B-52 central west block road no. 172 Bangladesh.',NULL,NULL,NULL,4,'ghFW7WQpqD7vz3EDl0BMw6FtdpohWsoRjaeYgSZFc0UKPEkF7V92rwtICoBJ',NULL,0,NULL,NULL,'2020-10-29 06:39:28','2020-10-29 06:39:28','Active'),(34,NULL,'Sujoy Nath',NULL,'sujoy.cse50@gmail.com','$2y$10$0Wsh5JzRw52skkXuVeFRTuHFHHkV0WAdpb0dvPB5ATOsSJRQ5NQVW',NULL,'Sujoy Nath',1,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'/img/users/Avatar.png',NULL,NULL,NULL,NULL,4,'c5d5e268ebc56b94da71acc42335220e','2020-10-29 10:45:41',0,NULL,NULL,'2020-10-29 10:45:41','2020-10-29 10:46:07','Active'),(35,NULL,'Sujoy Nath',NULL,'sujoy.cse21@gmail.com','$2y$10$CT9iuAfVCzR1jvddVGkrTuowINiLE2helUh.0lrmEu89wg634knd2',NULL,'Sujoy Nath',1,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/2d6a12a5564c20c2583c19b935a9ebc6.jpg',NULL,NULL,NULL,NULL,4,'8d530ec7286ed05af69ea2b908f2e326','2020-11-17 11:54:51',0,NULL,NULL,'2020-11-17 11:54:51','2020-11-17 11:54:51','Active'),(36,NULL,'Sujoy Nath',NULL,'sujoycse50@gmail.com','$2y$10$tAfIZCz1CDnUYYp91d292.ZEemEcLxRfF5GQ2BomjMdjB6lOwP5bi',NULL,'Sujoy Nath',1,NULL,NULL,NULL,NULL,'Male',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Backend/images/users/67db1f70baaac96da340f6a5f89b328b.jpg',NULL,NULL,NULL,NULL,4,'74b804bbad8213b3b56a29e77097e56e','2020-11-17 12:07:14',0,NULL,NULL,'2020-11-17 12:07:14','2020-11-17 12:07:14','Active');
/*!40000 ALTER TABLE `sys_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_users_additional_info`
--

DROP TABLE IF EXISTS `sys_users_additional_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_users_additional_info` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `sys_users_id` bigint NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_users_additional_info`
--

LOCK TABLES `sys_users_additional_info` WRITE;
/*!40000 ALTER TABLE `sys_users_additional_info` DISABLE KEYS */;
INSERT INTO `sys_users_additional_info` VALUES (1,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sdfdsf'),(2,19,NULL,NULL,NULL,'https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','I love to travel. By being exposed to new places, people and cultures, you\'ll develop a wider world view. Another reason why people love to travel: it helps open your mind. You realize that there\'s no one way to live life. ... By being exposed to new places, people and cultures, you\'ll develop a wider world view.'),(3,20,NULL,NULL,NULL,'asdfasdf','asdfasfd','adsfasdf','sadfasdf',NULL,'asdfasdf'),(4,21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,26,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Hello, my name is lima. And designation manager'),(6,28,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,31,NULL,NULL,NULL,'https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/','https://spotlisting-laravel.itech-theme.com/',NULL,'Hello, my name is Dias. I have a restaurant. please visit my site.'),(10,33,NULL,NULL,NULL,'https://spotlisting-laravel.itech-theme.com/','https://www.facebook.com/','https://twitter.com/','https://spotlisting-laravel.itech-theme.com/',NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vehicula et est convallis feugiat. Donec laoreet nec nibh quis dapibus. Pellentesque sagittis enim eleifend eros volutpat, at venenatis dui finibus. Ut auctor egestas nisl, at scelerisque ante tincidunt in. Cras eget quam leo. Aliquam sed elementum mauris, a rhoncus tellus. In et rutrum nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam vitae urna urna. Fusce felis elit, tempor nec elit pretium, imperdiet volutpat felis. Maecenas maximus neque at efficitur ornare. Pellentesque volutpat luctus libero sit amet efficitur. Nullam elit lorem, efficitur vel ligula eu, pellentesque aliquet libero. Nunc pulvinar ligula eu eros lacinia, id iaculis urna imperdiet. Cras euismod ante vel elit gravida interdum.\r\n\r\nMorbi dapibus maximus dui cursus faucibus. Ut tincidunt felis enim. Nullam orci mauris, tincidunt at dolor id, mollis maximus felis. Sed enim ex, sodales vitae neque at, pellentesque tempus odio. Nulla vitae tortor viverra, eleifend quam eu, semper lorem. Curabitur faucibus eu felis sit amet consectetur. Sed sit amet posuere lectus. Nam laoreet, magna sed hendrerit malesuada, nisl magna volutpat ex, sed semper sem dolor quis mauris. Integer ut mollis tortor, non tristique leo. Phasellus at nibh facilisis, venenatis nisi eu, eleifend odio. Duis ac convallis felis, quis gravida erat.'),(11,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sys_users_additional_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int unsigned NOT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `translations_language_id_foreign` (`language_id`),
  CONSTRAINT `translations_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_verification_codes`
--

DROP TABLE IF EXISTS `user_verification_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_verification_codes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expired_at` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_verification_codes`
--

LOCK TABLES `user_verification_codes` WRITE;
/*!40000 ALTER TABLE `user_verification_codes` DISABLE KEYS */;
INSERT INTO `user_verification_codes` VALUES (3,34,1,'333493','2020-11-13',0,'2020-10-29 10:45:41','2020-10-29 10:45:41');
/*!40000 ALTER TABLE `user_verification_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `websockets_statistics_entries`
--

DROP TABLE IF EXISTS `websockets_statistics_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `websockets_statistics_entries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `app_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peak_connection_count` int NOT NULL,
  `websocket_message_count` int NOT NULL,
  `api_message_count` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `websockets_statistics_entries`
--

LOCK TABLES `websockets_statistics_entries` WRITE;
/*!40000 ALTER TABLE `websockets_statistics_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `websockets_statistics_entries` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-19 13:10:51
