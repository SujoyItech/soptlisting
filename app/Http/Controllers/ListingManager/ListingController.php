<?php

namespace App\Http\Controllers\ListingManager;

use App\Http\Controllers\Common\Directory\CreateDirectoryService;
use App\Http\Controllers\Common\Directory\DirectoryService;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ListingController extends Controller
{
    private $create_directory_service,$directory_service;

    public function __construct() {
        $this->create_directory_service  = new CreateDirectoryService();
        $this->directory_service  = new DirectoryService();
    }

    public function createDirectory($id = NULL){
        $data = [];
        if ($id != NULL){
            $data = $this->directory_service->getDirectoryAllInfo($id);
        }
        $data['features'] = DB::table('spotlist_features')->where('status','Active')->get();
        return view('Common.Directory.directory_add');
    }

    public function directoryList(Request $request,$directory_id='') {
        $status = '';
        $user_id = Auth::user()->id;
        $verification_status = '';

        if ($request->ajax()) {

            $directory_list = $this->directory_service->getDirectoryList($status, $user_id, $verification_status,$directory_id);

            return datatables($directory_list)
                ->editColumn('thumb_image', function ($item) {
                    $html = '<img src="'.getImageUrl($item['thumb_image'],'other').'" class="rounded-circle" width="50" height="50">';
                    return $html;
                })
                ->editColumn('title', function ($item) {
                    if ($item['status'] == 'Active'){
                        $html = '<strong><a href="'.route('user.listing',['slug'=>$item['spotlist_directory_slug']]).'">' . $item['spotlist_directory_name'] . '</a></strong><br>';
                    }else{
                        $html = '<strong>' . $item['spotlist_directory_name'] . '</strong><br>';
                    }

                    $html .= '<small>'.$item['name'].'<br>'. Carbon::parse($item['created_at'])
                                                            ->format('Y-m-d h:iA') . '</small>';
                    return $html;
                })
                ->editColumn('categories', function ($item) {
                    $html = '';
                    if (isset($item['category_name']) && !empty($item['category_name'][0])) {
                        foreach ($item['category_name'] as $category_name) {
                            $html .= '<span class="badge badge-secondary">' . $category_name . '</span><br>';
                        }
                    }
                    return $html;
                })
                ->editColumn('location', function ($item) {
                    return $item['city_name'] . ', ' . $item['country_name'];
                })
                ->editColumn('status', function ($item) {
                    $html = '';
                    if ($item['status'] == 'Incomplete') {
                        $html .= '<span class="badge badge-warning">'.__('Incomplete').'</span><br>';
                    } else if ($item['status'] == 'Inactive') {
                        $html .= '<span class="badge badge-warning">'.__('Pending').'</span><br>';
                    }else if ($item['status'] == 'Active') {
                        $html .= '<span class="badge badge-success">'.__('Active').'</span><br>';
                        if ($item['is_featured'] == TRUE){
                            $html .= '<span class="badge badge-success">'.__('Featured').'</span>';
                        }
                    }else {
                        $html .= '<span class="badge badge-danger">'.__('Declined').'</span><br>';
                    }

                    return $html;
                })
                ->editColumn('action', function ($item) {
                    $html = ' <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-rounded btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.__('Action').'</button>'.
                                    '<div class="dropdown-menu">';
                    if ($item['status'] == 'Active'){
                        $html .= '<a class="dropdown-item" href="'.url('user/listing/'.$item['spotlist_directory_slug']).'">'.__('View in website').'</a>';
                    }
                    $html .=   '<a class="dropdown-item" href="'.url('directory-add/'.$item['spotlist_directory_id']).'">'.__('Edit').'</a>
                                        <div class="dropdown-divider"></div>
                                        <button class="dropdown-item remove_directory" type="button" data-id="'.$item['spotlist_directory_id'].'">'.__('Delete').'</button>
                                    </div>
                                </div>';
                    return $html;
                })
                ->rawColumns(['thumb_image','title', 'categories', 'location', 'status', 'action'])
                ->make(TRUE);
        }
        $data['directory_id'] = $directory_id;
        return view('ListingManager.Directory.directory_list',$data);
    }

    public function index()
    {
        return view('ListingManager.my_listing');
    }

    public function createList(){
        return view('ListingManager.add_listing');
    }

    public function wishingList(Request $request){
        $user_id = Auth::user()->id;
        $wishing_list = $this->directory_service->getWishingList($user_id);
        if ($request->ajax()) {

            return datatables($wishing_list)

                ->editColumn('thumb_image', function ($item) {
                    $html = '<img src="'.asset($item->thumb_image).'" class="rounded" width="50">';
                    return $html;
                })->editColumn('title', function ($item) {
                    $html = '<strong>'.$item->spotlist_directory_name.'</strong><br>';
                    return $html;
                })
                ->editColumn('category_name', function ($item) {
                    $html = '<span class="badge badge-secondary">' . $item->spotlist_category_name . '</span><br>';
                    return $html;
                }) ->editColumn('created_at', function ($item) {
                    $html = '<span class="badge badge-secondary">' . Carbon::parse($item->created_at )->format('d F Y'). '</span><br>';
                    return $html;
                })
                ->rawColumns(['thumb_image', 'title', 'category_name','created_at'])
                ->make(TRUE);
        }
        return view('ListingManager.wishing_list');
    }
}
