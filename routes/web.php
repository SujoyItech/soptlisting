<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::fallback(function(){
    if (isset(Auth::user()->default_module_id) && (Auth::user()->default_module_id == SYSTEM_ADMIN || Auth::user()->default_module_id == ADMIN || Auth::user()->default_module_id == LISTING_MANAGER)){
        return response()->view('Backend.errors.404');
    }else{
        return response()->view('FontEnd.errors.404');
    }

});

Route::get('no-permission', function(){
    if (isset(Auth::user()->default_module_id) && (Auth::user()->default_module_id == SYSTEM_ADMIN || Auth::user()->default_module_id == ADMIN || Auth::user()->default_module_id == LISTING_MANAGER)){
        return response()->view('Backend.errors.403');
    }else{
        return response()->view('FontEnd.errors.403');
    }
})->name('no-permission');

