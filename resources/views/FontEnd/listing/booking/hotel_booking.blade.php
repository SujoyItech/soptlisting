<div class="list-single-wiget book">
    <h2>{{__('Book a room')}}</h2>
    <form action="{{route('user.booking.request.send')}}" method="post" class="parsley-examples" id="room_booking">
        <div class="form-group">
            <input type="text" name="reservation_contact" required parsley-trigger="change" parsley-type="phone" id="phone">
        </div>
        <div class="form-group form-group2">

            <div class="form-1 bk">
                <div class="form-3">
                    <input placeholder="Adult" disabled />
                    <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/10.svg" alt=""></div>
                </div>
                <div class="quantity cart-plus-minus">
                    <input type="text" value="1" name="adult_guest" required min="1" />

                </div>
            </div>
        </div>
        <div class="form-group form-group2">
            <div class="form-3">
                <input placeholder="Child" disabled />
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/10.svg" alt=""></div>
            </div>
            <div class="form-1">
                <div class="quantity cart-plus-minus">
                    <input type="text" value="1" name="child_guest" required parsley-trigger="change" />
                </div>
            </div>
        </div>
        <div class="form-group form-group2">
            <div class="form-1">
                <input type="text" class="booking_date" width="276" placeholder="From" required parsley-trigger="change" name="reservation_start_date" id="from" readonly/>
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/10.svg" alt=""></div>
            </div>
            <div class="form-2">
                <input type="text" class="booking_date"  width="276" placeholder="To" required parsley-trigger="change" name="reservation_end_date" id="to" readonly/>
                <div class="f-img"><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/10.svg" alt=""></div>
            </div>
        </div>
        <div class="form-group">
            <select class="form-control" name="spotlist_directory_type_hotel_room_id" required parsley-trigger="change">
                <option value="">{{__('Room type')}}</option>
                @if(isset($types) && !empty($types[0]))
                    @foreach($types as $type)
                        <option value="{{$type->spotlist_directory_type_hotel_room_id}}">{{$type->room_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <textarea class="form-control" rows="3" name="reservation_note" placeholder="{{__('Add a note here')}}"></textarea>
        </div>
        <div class="form-group form-btn">
            <input type="hidden" name="type" value="{{$directory->spotlist_parent_category_id}}">
            <input type="hidden" name="spotlist_directory_id" value="{{$directory->spotlist_directory_id}}">
            <input type="hidden" name="reservation_for" value="hotel">
            @if(isset(Auth::user()->id))
                <button type="submit" class="theme-btn booking_now">{{__('BOOK NOW')}}</button>
                @include('FontEnd.listing.booking.wish_list')
            @else
                <a href="{{route('login')}}" class="theme-btn btn-block text-center">{{__('Login')}}</a>
            @endif
        </div>
    </form>
</div>
<script>
    $(document).ready(function (){
        $('#from').datepicker(
            {
                minDate: 0,
                beforeShow: function() {
                    $(this).datepicker('option', 'maxDate', $('#to').val());
                }
            });
        $('#to').datepicker(
            {
                defaultDate: "+1w",
                beforeShow: function() {
                    $(this).datepicker('option', 'minDate', $('#from').val());
                    if ($('#from').val() === '') $(this).datepicker('option', 'minDate', 0);
                }
            });

        $('.booking_date').on('change',function (){
            if ($(this).val() != "" && $(this).siblings('ul.parsley-errors-list').length > 0) {
                $(this).siblings('ul').remove();
            }
        })
    })


</script>
@include('FontEnd.listing.booking.phone_script')

