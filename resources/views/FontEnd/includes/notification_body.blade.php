@if(isset($notifications) &&!empty($notifications[0]))
    @foreach($notifications as $notify)
        @php($notify_body = json_decode($notify->body))
        @if($notify->type == USER_BOOKING)
            <a class="dropdown-item" href="{{route('user.bookings',['notify_id'=>$notify->id,'booking_id'=>$notify_body->id ?? ''])}}">
                <div class="single-item clearfix">
                    <span class="user-avater">
                        <img src="{{getImageUrl($notify_body->directory_image ?? '','others')}}" alt="" class="rounded-circle" alt="" width="40" height="40">
                    </span>
                    <div class="user-message">
                        <h6><span class="date">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</span></h6>
                        <p class="{{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}}">{{$notify_body->title ?? ''}}</p>
                    </div>
                </div>
            </a>
        @else
            <a class="dropdown-item" href="{{route('user.bookings',['notify_id'=>$notify->id,'booking_id'=>$notify_body->id ?? ''])}}">
                <div class="single-item clearfix">
                    <span class="user-avater">
                        <img src="{{getImageUrl($notify_body->user_image ?? '')}}" alt="" class="rounded-circle" alt="" width="40" height="40">
                    </span>
                    <div class="user-message">
                        <h6><span class="date">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</span></h6>
                        <p class="{{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}}">{{$notify_body->title ?? ''}}</p>
                    </div>
                </div>
            </a>
        @endif
    @endforeach
@endif

