<div class="list-single-wiget book">
    <h2>{{__('Contact us')}}</h2>
    <form action="{{route('user.booking.request.send')}}" method="post" class="parsley-examples" id="shop_booking">
        <div class="form-group">
            <input type="text" name="reservation_contact" required parsley-trigger="change" parsley-type="phone" id="phone">
        </div>
        <div class="form-group">
            <textarea class="form-control" name="reservation_note" placeholder="{{__('Your message')}}" rows="5"></textarea>
        </div>
        <div class="form-group form-btn">
            <input type="hidden" name="type" value="{{$directory->spotlist_parent_category_id}}">
            <input type="hidden" name="spotlist_directory_id" value="{{$directory->spotlist_directory_id}}">
            <input type="hidden" name="reservation_for" value="shop">
            @if(isset(Auth::user()->id))
                <button type="submit" class="theme-btn booking_now">{{__('BOOK NOW')}}</button>
                @include('FontEnd.listing.booking.wish_list')
            @else
                <a href="{{route('login')}}" class="theme-btn btn-block text-center">{{__('Login')}}</a>
            @endif
        </div>
    </form>
</div>
@include('FontEnd.listing.booking.phone_script')
