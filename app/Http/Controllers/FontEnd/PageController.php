<?php

namespace App\Http\Controllers\FontEnd;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FontEnd\Listing\ListingService;
use App\Http\Service\NotificationService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\IFTTTHandler;
use View;
class PageController extends Controller
{
    private $service;
    public function __construct(){
        $this->service = new FontEndService();
    }

    public function blog($slug=null){
        $blog = $this->service->getBlog($slug);
        return $slug == NULL ? view('FontEnd.blog.blog',$blog) : view('FontEnd.blog.blog_details',$blog);
    }


    public function pricing(){
        $packages = $this->service->getPackages();
        return view('FontEnd.pages.pricing',$packages);
    }

    public function profile($id=null){
        $profile = $this->service->getProfile($id);
        return view('FontEnd.profile.profile',$profile);
    }

    public function contact(){
        return view('FontEnd.pages.contact');
    }

    public function about(){
        $data['how_we_work'] = __options(['about_us_settings']);
        $data['how_it_works'] = DB::table('spotlist_how_it_works')->get();
        $data['recent_reviews'] = DB::table('spotlist_directory_reviews')
                                    ->select('spotlist_directory_reviews.user_id','spotlist_directory_reviews.rating',
                                        'spotlist_directory_reviews.review','sys_users.username','sys_users.user_image','spotlist_directory_reviews.created_at')
                                    ->join('sys_users','spotlist_directory_reviews.user_id','=','sys_users.id')
                                    ->orderBy('spotlist_directory_reviews.created_at','desc')
                                    ->paginate(2);
        $data['teams'] = DB::table('spotlisting_teams')->where('status','Active')->get();

        return view('FontEnd.pages.about',$data);
    }

    public function faq(){
        $data['faqs'] = DB::table('spotlist_faqs')->where('status','Active')->get();
        return view('FontEnd.pages.faq',$data);
    }
    public function wishing(){
        $wishing_list = DB::table('spotlist_directory_wishinglist')
                          ->where(['sys_users_id'=>Auth::user()->id,'is_wished'=>TRUE])->pluck('spotlist_directory_id')->toArray();
        $service = new ListingService();
        $where_in [0] = 'spotlist_directory.spotlist_directory_id';
        $where_in [1] = $wishing_list;
        $data['list'] = $service->getDirectoryList(NULL,NULL,20,$where_in,['spotlist_directory.status'=>'Active']);
        return view('FontEnd.profile.wishing_listing',$data);
    }

}
