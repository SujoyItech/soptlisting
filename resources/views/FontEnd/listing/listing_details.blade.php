@extends('FontEnd.master',['menu'=>'listing'])
@php($settings = __options(['admin_settings']))
@section('style')
    <script src="{{asset('FontEnd/')}}/assets/js/jquery.min.js"></script>
    <script src="{{asset('FontEnd/')}}/assets/js/jquery-ui.min.js"></script>
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <style>
        /* Rating Star Widgets Style */
        .rating-stars ul {
            list-style-type:none;
            padding:0;

            -moz-user-select:none;
            -webkit-user-select:none;
        }
        .rating-stars ul > li.star {
            display:inline-block;

        }

        /* Idle State of the stars */
        .rating-stars ul > li.star > i.fa {
            color:#ccc; /* Color on idle state */
        }

        /* Hover state of the stars */
        .rating-stars ul > li.star.hover > i.fa {
            color:#FFCC36;
        }

        /* Selected state of the stars */
        .rating-stars ul > li.star.selected > i.fa {
            color:#FF912C;
        }

        .parsley-required{
            color: red;
        }
        .listing-image{
            position: relative;
        }
        .listing-details {
            background-color: rgb(8 37 76 / 49%);
            color: #fff;
            min-width: 900px;
            min-height: 360px;
            margin: 50px;
            top: 50%;
            transform: translateY(-50%);
            padding: 30px;
            position: absolute;
            border: 1px solid #08254c;
            border-radius: 5px;
        }

        .listing-details h1 {
            font-size: 80px;
            font-weight: bold;
            color: #fff;
            margin-top: 1.0em;
            margin-bottom: 0.3em;
        }
        .listing-details h3 {
            font-weight: bold;
            color: #fff;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="{{asset('FontEnd/')}}/dist/bootstrap-clockpicker.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/css/intlTelInput.css" rel="stylesheet" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>
@endsection
@section('head','v2')
@section('content')
    <div class="list-v3">
        <div class="listing-image">
            <img src="{{ getImage($directory->cover_image ?? '','directory')}}" alt="">
            <div class="listing-details">
                <div class="row">
                    <div class="col-10">
                        <div class="thumb">
                            <span>{{$directory->category_name}}</span>
                        </div>
                        <h1>{{$directory->spotlist_directory_name}}</h1>
                        <h3><i class="fa fa-map-marker"></i> {{$directory->city_name.' , '.$directory->country_name}}</h3>
                        <div class="ratting-area">
                            <ul>
                                @for($x = 0; $x< 5 ; $x++)
                                    @if(floor($directory->rating) - $x >=1)
                                        <li><i class="fa fa-star"></i></li>
                                    @elseif($directory->rating -$x >0)
                                        <li><i class="fa fa-star-half-o"></i></li>
                                    @else
                                        <li><i class="fa fa-star-o"></i></li>
                                    @endif
                                @endfor
                            </ul>
                            <span class="text-light">{{isset($directory->rating) ? number_format($directory->rating,1) : '0.0'}} <small>({{$directory->total_reviews}} {{__('Reviews')}})</small></span>
                            @if(isset(Auth::user()->id))
                                @if(isset($user_ratings))
                                    <span class="badge badge-success ml-3"><a href="javascript:void(0)" class="text-light give_ratings" data-directory-id="{{$directory->spotlist_directory_id}}">{{__('Change review')}}</a></span>
                                @else
                                    <span class="badge badge-success ml-3"><a href="javascript:void(0)" class="text-light give_ratings" data-directory-id="{{$directory->spotlist_directory_id}}">{{__('Give review')}}</a></span>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="pd-social">
                            <ul>
                                @if(isset(Auth::user()->id))
                                    @if(isset($wishlist) && $wishlist->is_wished == TRUE)
                                        <li><a href="{{route('user.directory.wishlist',['id'=>$directory->spotlist_directory_id])}}"><i class="fa fa-heart red"></i></a></li>
                                    @else
                                        <li><a href="{{route('user.directory.wishlist',['id'=>$directory->spotlist_directory_id])}}"><i class="fa fa-heart"></i></a></li>
                                    @endif
                                @endif
{{--                                <li><button class="share" data-share-title="Spotlisting" data-share-url="{{url('user/listing/'.$directory->spotlist_directory_id)}}"><i class="fa fa-share-alt" aria-hidden="true"></i></button></li>--}}
                                    <div class="btn-group dropright">
                                        <button type="button" class="btn btn-info">
                                            <i class="fa fa-share-alt" aria-hidden="true"></i> {{__('Share')}}
                                        </button>
                                        <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="sr-only">{{__('Share')}}</span>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{url('user.listing',['id'=>$directory->spotlist_directory_id])}}" class="dropdown-item text-primary"><i class="fa fa-facebook"></i> {{__('Facebook')}}</a>
                                            <a target="_blank" href="https://twitter.com/intent/tweet?url={{url('user.listing',['id'=>$directory->spotlist_directory_id])}}" class="dropdown-item text-info"><i class="fa fa-twitter"></i> {{__('Twitter')}}</a>
                                            <a target="_blank" href="https://plus.google.com/share?url={{url('user.listing',['id'=>$directory->spotlist_directory_id])}}" class="dropdown-item text-danger"><i class="fa fa-google"></i> {{__('Google')}}</a>
                                            <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{url('user.listing',['id'=>$directory->spotlist_directory_id])}}" class="dropdown-item text-info"><i class="fa fa-linkedin"></i> {{__('Linked In')}}</a>
                                            <a target="_blank" href="http://pinterest.com/pin/create/button/?url={{url('user.listing',['id'=>$directory->spotlist_directory_id])}}" class="dropdown-item text-danger"><i class="fa fa-pinterest"></i> {{__('Pinterest')}}</a>
                                        </div>
                                    </div>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="listing-single-area section-padding pt-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="listing-single-left">
                        <div class="list-about list-sub">
                            <h2>{{__('About')}}</h2>
                            <p>{{$directory->description ?? ''}}</p>
                        </div>
                        <div class="list-social list-single-wiget location con-sub">
                            <h2>{{__('Location/Contacts')}}</h2>
                            <div class="contact-map" id="directory_map">

                            </div>
                            <ul>
                                <li><img src="{{asset('FontEnd/assets/')}}/images/listing/icon/12.svg" alt=""><strong>{{__('Adress :')}} </strong>{{$directory->address}}</li>
                                <li><img src="{{asset('FontEnd/assets/')}}/images/listing/icon/13.svg" alt=""><strong>{{__('Phone :')}}</strong> {{$directory->contact_number}}</li>
                                <li><img src="{{asset('FontEnd/assets/')}}/images/listing/icon/14.svg" alt=""><strong>{{__('Mail :')}}</strong> {{$directory->email}}</li>
                                <li><img src="{{asset('FontEnd/assets/')}}/images/listing/icon/15.svg" alt=""><strong>{{__('Website :')}}</strong> {{$directory->website}}</li>
                            </ul>
                        </div>
                        @if($directory->video_ability == TRUE && isValidUrl($directory->video_url))
                            <div class="list-gallery list-sub">
                                <h2>{{__('Video')}}</h2>

                                <div class="gallery-img">
                                    <iframe src="{{$directory->video_url}}"
                                            width="100%" height="400" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        @endif
                        @if(isset($gallery) && !empty($gallery[0]))
                        <div class="list-gallery list-sub">
                            <h2>{{__('Gallery')}}</h2>
                            <div class="gallery-container gallery-fancybox masonry-gallery gallery-img">
                                <div class="grid Print-Design Web-Application Photography">
                                    @foreach($gallery as $gal)
                                        <a href="{{asset($gal->image)}}" class="fancybox" data-fancybox-group="gall-1" id="galary_click">
                                            <img id="garrery_image" src="{{asset($gal->image)}}" alt class="img img-responsive">
                                            <div class="icon">
                                                <i class="fa fa-eye"></i>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endif

                        @if(isset($reviews) && !empty($reviews[0]))
                            <div class="list-review list-sub">
                                <h2>{{__('Reviews')}}</h2>
                                <div class="list-review-wrap">
                                    @foreach($reviews as $review)
                                        <div class="list-review-item">
                                            <div class="rev-top">
                                                <div class="rev-item">
                                                    <div class="rev-img"><img src="{{ isset($review->user_image) && file_exists(public_path($review->user_image)) ? asset($review->user_image) : asset('Backend/images/users/avatar.svg')}}" alt="" width="50"></div>
                                                    <div class="rev-text">
                                                        <h4>{{$review->name ?? ''}}</h4>
                                                        <span>{{__('Posted')}} {{Carbon\Carbon::parse($review->created_at)->diffForHumans()}}</span>
                                                    </div>
                                                </div>
                                                <ul class="list-rating">
                                                    @for($x=0; $x<5 ; $x++)
                                                        @if(floor($review->rating)- $x >= 1 )
                                                            <li><i class="fa fa-star"></i></li>
                                                        @elseif($review->rating - $x > 0)
                                                            <li><i class="fa fa-star-half-o"></i></li>
                                                        @else
                                                            <li><i class="fa fa-star-o"></i></li>
                                                        @endif
                                                    @endfor
                                                </ul>
                                            </div>
                                            <p>{{$review->review}}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        @if($directory->spotlist_parent_category_id == CATEGORY_HOTEL)
                            @include('FontEnd.listing.services.room_types')
                        @elseif($directory->spotlist_parent_category_id == CATEGORY_RESTAURANT)
                            @include('FontEnd.listing.services.menu_items')
                        @elseif($directory->spotlist_parent_category_id == CATEGORY_BEAUTY)
                            @include('FontEnd.listing.services.beauty_service')
                        @elseif($directory->spotlist_parent_category_id == CATEGORY_SHOPPING)
                            @include('FontEnd.listing.services.product_items')
                        @elseif($directory->spotlist_parent_category_id == CATEGORY_FITNESS)
                            @include('FontEnd.listing.services.fitness_service')
                        @endif

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="list-sidebar">
                        @include('FontEnd.listing.booking.booking')
                        @if(isset($features) && !empty($features[0]))
                        <div class="list-social list-single-wiget amenities">
                            <h2>{{__('Amenities')}}</h2>
                            <ul>
                                @foreach($features as $feature)
                                    <li><a href="javascript:void(0)"><img src="{{asset($feature->spotlist_features_icon)}}" width="20" height="20" class="rounded-circle"> {{$feature->spotlist_features_name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="list-single-wiget location hours">
                            <h2 class="text-center">{{__('Working Hours')}}</h2>
                            <ul>
                                <li><strong>{{__('Monday')}} </strong><span>{{$working_hours->monday ?? ''}}</span></li>
                                <li><strong>{{__('Tuesday')}}</strong> <span>{{$working_hours->tuesday ?? ''}}</span></li>
                                <li><strong>{{__('Wednesday')}}</strong> <span>{{$working_hours->wednesday ?? ''}}</span></li>
                                <li><strong>{{__('Thursday')}}</strong> <span>{{$working_hours->thursday ?? ''}}</span></li>
                                <li><strong>{{__('Friday')}}</strong> <span>{{$working_hours->friday ?? ''}}</span></li>
                                <li><strong>{{__('Saturday')}}</strong> <span>{{$working_hours->saturday ?? ''}}</span></li>
                                <li><strong>{{__('Sunday')}}</strong> <span>{{$working_hours->sunday ?? ''}}</span> </li>
                            </ul>
                        </div>
                        <div class="list-single-wiget location Listed">
                            <h2 class="text-center">{{__('Listed By')}}</h2>
                            <div class="listed">
                                <div class="listed-img">
                                    <img src="{{isset($user->user_image) && file_exists($user->user_image) ? asset($user->user_image) : asset('Backend/images/users/avatar.svg')}}" alt="">
                                </div>
                                <h3><a href="{{route('user.profile',['id'=>$user->id])}}">{{$user->username ?? ''}}</a></h3>
                                <span class="text-center text-success follower" data-follower="{{userFollower($user->id)}}"><strong class="show_follower">{{userFollower($user->id)}}</strong> {{__('followers')}}</span>
                            </div>
                            <ul>
                                <li><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/12.svg" alt=""><strong>{{__('Adress :')}} </strong>{{$user->address ?? ''}}</li>
                                <li><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/13.svg" alt=""><strong>{{__('Phone :')}}</strong> {{$user->mobile ?? ''}}</li>
                                <li><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/14.svg" alt=""><strong>{{__('Mail :')}}</strong> {{$user->email ?? ''}}</li>
                                <li><img src="{{asset('FontEnd/')}}/assets/images/listing/icon/15.svg" alt=""><strong>Website :</strong> www.elevatorcafe.com</li>
                            </ul>
                            @if(isset(Auth::user()->id))
                                <div class="form-group form-btn">
                                    <button type="submit" class="theme-btn follow {{checkFollower(Auth::user()->id,$user->id ?? '') == TRUE ? 'd-none' : ''}}" data-user-id="{{$user->id ?? ''}}">{{__('Follow')}}</button>
                                    <button type="submit" class="theme-btn unfollow {{checkFollower(Auth::user()->id,$user->id ?? '') == TRUE ? '' : 'd-none'}}" data-user-id="{{$user->id ?? ''}}"><i class="fa fa-check"></i> {{__('Unfollow')}}</button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($list_by_category) && !empty($list_by_category[0]))
                <div class="deals-area mt-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                                <div class="section-title">
                                    <h2>{{__('Similar Listing')}}</h2>
                                    <a class="btn theme-btn" href="{{route('show-all-listing')}}">{{__('View All')}}</a>
                                </div>
                                <div class="country-r">
                                    <div class="deals-carousel owl-carousel owl-theme owl-btn-center-lr owl-dots-primary-full owl-btn-3 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                                        @foreach($list_by_category as $deals)
                                            <div class="product-item">
                                                <a href="{{route('user.listing',['slug'=>$deals->spotlist_directory_slug])}}">
                                                    <div class="product-img">
                                                        <img src="{{getImage($deals->thumb_image,'directory')}}" alt="" height="200">
                                                    </div>
                                                </a>
                                                <div class="product-content">
                                                    <h5><a href="{{route('user.listing',['slug'=>$deals->spotlist_directory_slug])}}">{{$deals->spotlist_directory_name}}</a></h5>
                                                    <span>{{$deals->city_name.' , '.$deals->country_name}}</span>
                                                    <p>{{$deals->address}}</p>
                                                    <div class="thumb-item">
                                                        <ul class="list-rating">
                                                            @for($x=0; $x<5 ; $x++)
                                                                @if(floor($deals->rating)- $x >= 1 )
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @elseif($deals->rating - $x > 0)
                                                                    <li><i class="fa fa-star-half-o"></i></li>
                                                                @else
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                @endif
                                                            @endfor
                                                        </ul>
                                                        <span>({{isset($deals->rating) ? number_format($deals->rating,1) : '0.0'}})</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')
    @include('FontEnd.listing.review')
    @include('FontEnd.listing.services.service_details_modal')
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>

    <script type="text/javascript" src="{{asset('FontEnd/')}}/dist/bootstrap-clockpicker.min.js"></script>
    <script>
        $('.clockpicker').clockpicker();
    </script>
    <script>
        function initMap() {
            var local_lat =  parseFloat("{{$directory->latitude ?? ''}}");
            var local_lon =  parseFloat("{{$directory->longitude ?? ''}}");
            var myLatlng = {lat: local_lat, lng: local_lon};
            var map = new google.maps.Map(
                document.getElementById('directory_map'), {zoom: 15, center: myLatlng});

            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: {lat: local_lat, lng: local_lon }
            });

        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{$settings->map_key ?? 'AIzaSyBj0o3QDEGBtW3S8AEq0CRlpZdpWxNZckA'}}&libraries=geometry,places&callback=initMap"></script>
    <script>
        $(document).on('click','.share',function (){
            swalSocialShare();
        });
        function swalSocialShare(){
            return  Swal.fire({
                html: '<a href="" class="btn btn-primary"> <i class="fa fa-facebook"></i> {{__('Facebook')}}</a>\n' +
                    '<a href="" class="btn btn-info"> <i class="fa fa-twitter"></i> {{__('Twitter')}}</a>\n' +
                    '<a href="" class="btn btn-success"> <i class="fa fa-google"></i> {{__('Google')}}</a>',
                reverseButtons: true,
                allowOutsideClick: false,
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: false,
                showConfirmButton: false,
            })
        }

        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });


        $(document).on('click','.give_ratings',function (){
            $('#myModal').modal('show');
        })



        $('img.xyz').hover(function (){
            var newSource = $(this).data('alt-src');
            $('#garrery_image').attr('src', newSource);
            $('#galary_click').attr('href', newSource);
        });

        $('.follow').on('click',function (){
            var user_id = $(this).data('user-id');
            var data = {
                follower_id : user_id,
                is_follow : 1,
            }
            var url = "{{route('user.follow')}}";
            makeAjaxPost(data,url,null).done(function (response){
                if (response.success == true){
                    $('.follow').addClass('d-none');
                    $('.unfollow').removeClass('d-none');
                    var follower = $('.follower').data('follower');
                    follower += 1;
                    $('.follower').data('follower',follower)
                    $('.show_follower').html(follower) ;
                }else {
                    swalError(response.message);
                }
            })
        });

        $('.unfollow').on('click',function (){
            var user_id = $(this).data('user-id');
            var data = {
                follower_id : user_id,
                is_follow : 0,
            }
            var url = "{{route('user.follow')}}";
            makeAjaxPost(data,url,null).done(function (response){
                if (response.success == true){
                    $('.unfollow').addClass('d-none');
                    $('.follow').removeClass('d-none');
                    var follower = $('.follower').data('follower');
                    follower -= 1;
                    $('.follower').data('follower',follower);
                    $('.show_follower').html(follower) ;
                }else {
                    swalError(response.message);
                }
            })
        });


    </script>
    <script>

        $(document).on('click','.add-cart',function (){
            var spotlist_directory_id = $(this).data('directory-id');
            var spotlist_directory_type_id = $(this).data('directory-type-id');
            var spotlist_like = $(this).data('liked');
            var this_list = $(this);
            var data = {
                'spotlist_directory_id': spotlist_directory_id,
                'spotlist_directory_type_id': spotlist_directory_type_id,
                'spotlist_like': spotlist_like
            };
            var submit_url = "{{route('user.directory.type.like')}}";
            makeAjaxPost(data, submit_url, null).done(function (response) {
                if (response.success === true){
                    if (response.data.like === 1){
                        this_list.addClass('text-success');
                        this_list.data('liked',1);
                        $('#'+response.data.spotlist_directory_type_id).data('like',$('#'+response.data.spotlist_directory_type_id).data('like')+1);
                        var like_data = $('#'+spotlist_directory_type_id).data('like');
                        like_data = like_data >1000 ? like_data/1000+'k' : like_data;
                        $('#'+response.data.spotlist_directory_type_id).text(like_data);

                    }else {
                        this_list.removeClass('text-success');
                        this_list.data('liked',0);
                        $('#'+response.data.spotlist_directory_type_id).data('like',$('#'+response.data.spotlist_directory_type_id).data('like')-1);
                        var like_data = $('#'+spotlist_directory_type_id).data('like');
                        like_data = like_data >1000 ? like_data/1000+'k' : like_data;
                        $('#'+response.data.spotlist_directory_type_id).text(like_data);

                    }
                }else{
                    var redirect_url = "{{Request::url()}}";
                    swalRedirect(redirect_url, response.message, 'error');
                }
            });
        });



    </script>

    <script>
        $(document).on('click','.view_details',function (){
            var details_url = "{{url('view-service-details')}}";
            var details_data = {
                type : $(this).data('type'),
                id : $(this).data('id')
            };
            makeAjaxPostText(details_data,details_url,null).done(function (response){
                $('.service_details').html(response);
                $('#serviceDetails').modal('show');
            })
        })
    </script>

@endsection
