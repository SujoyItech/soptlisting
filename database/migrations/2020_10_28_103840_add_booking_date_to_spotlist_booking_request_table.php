<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBookingDateToSpotlistBookingRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spotlist_booking_request', function (Blueprint $table) {
            $table->string('reservation_start_date')->nullable();
            $table->string('reservation_end_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spotlist_booking_request', function (Blueprint $table) {
            //
            $table->dropColumn('reservation_start_date');
            $table->dropColumn('reservation_end_date');
        });
    }
}
