<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectoryGallarySeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory_gallary')->delete();
        
        \DB::table('spotlist_directory_gallary')->insert(array (
            0 => 
            array (
                'spotlist_directory_gallary_id' => 1,
                'spotlist_directory_id' => 1,
                'image' => 'Backend/images/directory/R3BuKw4tdSpT1S5J.jpg',
                'status' => 'Active',
            ),
            1 => 
            array (
                'spotlist_directory_gallary_id' => 2,
                'spotlist_directory_id' => 1,
                'image' => 'Backend/images/directory/a7GAfjSY28zE7aan.jpg',
                'status' => 'Active',
            ),
            2 => 
            array (
                'spotlist_directory_gallary_id' => 3,
                'spotlist_directory_id' => 1,
                'image' => 'Backend/images/directory/tSNqPXc65tfmRZRx.jpg',
                'status' => 'Active',
            ),
            3 => 
            array (
                'spotlist_directory_gallary_id' => 4,
                'spotlist_directory_id' => 1,
                'image' => 'Backend/images/directory/X9XdOxZxkl5wSzZy.jpg',
                'status' => 'Active',
            ),
            4 => 
            array (
                'spotlist_directory_gallary_id' => 5,
                'spotlist_directory_id' => 2,
                'image' => 'Backend/images/directory/aiuKkLj4jKOeXqtj.jpeg',
                'status' => 'Active',
            ),
            5 => 
            array (
                'spotlist_directory_gallary_id' => 6,
                'spotlist_directory_id' => 2,
                'image' => 'Backend/images/directory/pDQDxqekrX5MkcDg.jpg',
                'status' => 'Active',
            ),
            6 => 
            array (
                'spotlist_directory_gallary_id' => 7,
                'spotlist_directory_id' => 2,
                'image' => 'Backend/images/directory/0EeSt9C7mj77KK8f.jpg',
                'status' => 'Active',
            ),
        ));

        
    }
}