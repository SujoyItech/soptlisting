<div class="popular-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <div class="section-title">
                    <h2>{{__('Popular Destination')}}</h2>
                    <a class="btn theme-btn" href="{{route('all-cities')}}">{{__('View All')}}</a>
                </div>
                <div class="country-r">
                    <div class="populer-carousel owl-carousel owl-theme owl-btn-center-lr owl-dots-primary-full owl-btn-3 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">

                            @foreach($popular_location as $location)
                                <div class="item">
                                    <a href="{{route('show-list-by-place',['slug'=>$location->spotlist_cities_slug])}}">
                                        <div class="popular-item">
                                            <div class="popular-img">
                                                <img src="{{isset($location->city_image) ? asset($location->city_image) :  ''}}" alt="" height="300">
                                            </div>
                                            <div class="popular-content">
                                                <h5><a href="{{route('show-list-by-place',['slug'=>$location->spotlist_cities_slug])}}">{{$location->spotlist_cities_name}}</a></h5>

                                            </div>
                                            <div class="thumb">
                                                <span>{{$location->total_listing ?? 0 }} {{__('Listing')}}</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
