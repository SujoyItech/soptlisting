<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSysSystemSettingsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sys_system_settings', function (Blueprint $table) {
            $table->increments('sys_system_settings_id');
            $table->enum('option_group', ['application_settings','authentication','payment_settings','admin_settings','frontend_settings','about_us_settings'])
                  ->default('application_settings')->nullable();
            $table->string('option_key', 150);
            $table->text('option_value')->nullable();
            $table->enum('status', ['Active', 'Inactive'])->default('Active')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->integer('created_by')->default(1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('sys_system_settings');
    }
}
