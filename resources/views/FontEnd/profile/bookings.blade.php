@extends('FontEnd.master')

@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('My booking list')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- start faq-pg-section -->
    <section class="faq-pg-section section-padding">
        <div class="container">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="{{route('user.profile.settings')}}"  aria-expanded="false" class="nav-link">
                        {{__('Profile')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.bookings')}}"  aria-expanded="true" class="nav-link active">
                        {{__('My bookings')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.wishing')}}" aria-expanded="false" class="nav-link">
                        {{__('My Wishlist')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user-all-notification')}}" aria-expanded="false" class="nav-link">
                        {{__('My notifications')}}
                    </a>
                </li>
            </ul>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body booking-table table-responsive">
                            <table id="example" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>{{__('ID')}}</th>
                                        <th>{{__('Directory')}}</th>
                                        <th>{{__('Category')}}</th>
                                        <th>{{__('Booking date')}}</th>
                                        <th>{{__('Reservation start date')}}</th>
                                        <th>{{__('Start time')}}</th>
                                        <th>{{__('Status')}}</th>
                                        <th>{{__('Action')}}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>

                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
        </div> <!-- end container -->
    </section>
    <!-- end faq-pg-section -->
    @include('FontEnd.pages.user_email_subscription')
    @include('FontEnd.pages.booking_data.booking_details')
    @include('Common.Booking.booking_message')
@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>
    <script>
        $('#example').dataTable({
            processing: true,
            serverSide: true,
            pageLength: 10,
            responsive: false,
            ajax: '{{route('user.bookings',['notify_id'=>$notify_id ?? '','booking_id'=>$booking_id ?? ''])}}',
            order: [],
            autoWidth:false,
            columns: [
                {"data": "booking_id"},
                {"data": "spotlist_directory_name"},
                {"data": "reservation_for"},
                {"data": "created_at"},
                {"data": "reservation_start_date"},
                {"data": "reservation_start_time"},
                {"data": "status"},
                {"data": "action"},
            ],
        });

        $(document).on('click','.view_details',function (){
            var url = "{{url('view-user-booking-details')}}";
            var data = {
                id : $(this).data('id')
            }
            makeAjaxPostText(data,url,null).done(function (response) {
               $('.modal-body').html(response)
                $('#detailsModal').modal('show');
            });
        });

        $(document).on('click','.cancel',function (){
            var cancel_url = "{{url('user/booking/request/cancel')}}";
            var cancel_data = {
                id : $(this).data('id')
            }
            swalConfirm("Do you really want to cancel booking?").then(function (s) {
                if(s.value){
                    makeAjaxPost(cancel_data, cancel_url, null).done(function (response) {
                        if(response.success == false) {
                            swalError(response.message);
                        } else {
                            swalRedirect('',response.message,'success');
                        }
                    });
                }
            })

        });
        $(document).on('click','.delete_list',function (){
            var delete_url = "{{url('user/booking/request/delete')}}";
            var delete_data = {
                id : $(this).data('id')
            }
            swalConfirm("Do you really want to delete this?").then(function (s) {
                if(s.value){
                    makeAjaxPost(delete_data, delete_url, null).done(function (response) {
                        if(response.success == false) {
                            swalError(response.message);
                        } else {
                            swalRedirect('',response.message,'success');
                        }
                    });
                }
            })

        });

        $(document).on('click','.message',function (){
            var message_url = "{{url('view-user-booking-message')}}";
            var message_data = {
                id : $(this).data('id')
            }
            makeAjaxPostText(message_data,message_url,null).done(function (response) {
                $('.message_modal').html(response)
                $('#bookingMessageModal').modal('show');
            });
        })
        $(document).on('click','.view_reason',function (){
            var message_url = "{{url('view-user-booking-status-message')}}";
            var message_data = {
                id : $(this).data('id')
            }
            makeAjaxPostText(message_data,message_url,null).done(function (response) {
                $('.message_modal').html(response)
                $('#bookingMessageModal').modal('show');
            });
        })
    </script>
@endsection

