<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_features', function (Blueprint $table) {
            $table->increments('spotlist_features_id');
            $table->string('spotlist_features_name');
            $table->string('spotlist_features_icon')->nullable();
            $table->integer('created_by')->default(2);
            $table->integer('updated_by')->default(2);
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_features');
    }
}
