<?php

namespace App\Http\Controllers\Common\Booking;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    private $service;

    public function __construct() {
        $this->service = new BookingService();
    }

    public function index(){
        return view('ListingManager.bookings');
    }
    public function userBookingRequest(Request $request,$type='',$booking_id=''){

        if ($request->ajax()){
            $booking_data = $this->service->getBookingRequestData($type,$booking_id);
            return datatables($booking_data)
               ->editColumn('listing', function ($item) {
                    return $this->getListingInfo($item);
                })->editColumn('user_details', function ($item) {
                    $html = '<strong>'.$item->name.'</strong><br>';
                    $html .= '<span>'.$item->reservation_contact.'</span><br>';
                    $html .= '<span>'.$item->email.'</span><br>';
                    return $html;
                })
                ->editColumn('date', function ($item) {
                    return !empty($item->updated_at) ? Carbon::parse($item->updated_at)->format('d-m-Y h:iA') : '';
                })
                ->editColumn('additional_information', function ($item){
                    return $this->getAdditionalInfo($item);
                })
                ->editColumn('status', function ($item) {
                    $html = '';
                    if ($item -> status  == 'Requested') {
                        $html .= '<span class="badge badge-warning">'.__('Requested').'</span><br>';
                    } else if ($item->status == 'Declined') {
                        $html .= '<span class="badge badge-danger">'.__('Declined').'</span><br>';
                    }else if ($item->status == 'Expired') {
                        $html .= '<span class="badge badge-danger">'.__('Expired').'</span><br>';
                    }else if ($item->status == 'Approve') {
                        $html .= '<span class="badge badge-success">'.__('Approved').'</span><br>';
                    }else if ($item->status == 'Completed') {
                        $html .= '<span class="badge badge-success">'.__('Completed').'</span><br>';
                    }else{
                        $html .= '<span class="badge badge-danger">'.$item->status.'</span><br>';
                    }
                    return $html;
                })
                ->editColumn('action', function ($item) {
                    if ($item->status == 'Completed'){
                        $html = '<span class="badge badge-success">'.__('Completed').'</span>';
                    }elseif ($item->status == 'Expired') {
                        $html = '<span class="badge badge-danger">'.__('Expired').'</button>';
                    }else{

                        $html = ' <div class="btn-group m-1">
                                    <button type="button" class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.__('Action').'</button>'.
                            '<div class="dropdown-menu">
                                        <button class="dropdown-item change_booking_status" data-type="decline" type="button" data-id="'.$item->spotlist_booking_request_id.'">'.__('Declined').'</button>
                                        <button class="dropdown-item change_booking_status" data-type="expire" type="button" data-id="'.$item->spotlist_booking_request_id.'">'.__('Expired').'</button>
                                        <button class="dropdown-item change_booking_status" data-type="approve" type="button" data-id="'.$item->spotlist_booking_request_id.'">'.__('Approve').'</button>
                                        <button class="dropdown-item change_booking_status" data-type="complete" type="button" data-id="'.$item->spotlist_booking_request_id.'">'.__('Completed').'</button>

                                        <div class="dropdown-divider"></div>
                                        <button class="dropdown-item change_booking_status" data-type="delete" type="button" data-id="'.$item->spotlist_booking_request_id.'">'.__('Delete').'</button>
                                    </div>
                                </div>';
                        $html .= '<a href="javascript:void(0);" class="btn btn-xs btn-info text-light message" data-id="'.$item->spotlist_booking_request_id.'" data-toggle="tooltip" data-placement="top" title="Message"><i class="fa fa-reply"></i>'. __('Reply').'</a>';
                    }

                    return $html;

                })
                ->rawColumns(['listing','user_details', 'date', 'additional_information', 'status', 'action'])
                ->make(TRUE);
        }

        $data['type'] = $type;
        $data['booking_id'] = $booking_id;

        return view('Common.Booking.booking_list',$data);
    }

    public function allBookingRequest(Request $request){
        $booking_data = $this->service->getAllBookingRequestData();

        if ($request->ajax()){

            return datatables($booking_data)
               ->editColumn('listing', function ($item) {
                    return $this->getListingInfo($item);
                })->editColumn('user_details', function ($item) {
                    $html = '<strong>'.$item->name.'</strong><br>';
                    $html .= '<span>'.$item->reservation_contact.'</span><br>';
                    $html .= '<span>'.$item->email.'</span><br>';
                    return $html;
                })
                ->editColumn('date', function ($item) {
                    return !empty($item->updated_at) ? Carbon::parse($item->updated_at)->format('d-m-Y h:iA') : '';
                })
                ->editColumn('additional_information', function ($item){
                    return $this->getAdditionalInfo($item);                 })
                ->editColumn('status', function ($item) {
                    $html = '';
                    if ($item -> status  == 'Requested') {
                        $html .= '<span class="badge badge-warning">'.__('Requested').'</span><br>';
                    } else if ($item->status == 'Declined') {
                        $html .= '<span class="badge badge-danger">'.__('Declined').'</span><br>';
                    }else if ($item->status == 'Expired') {
                        $html .= '<span class="badge badge-danger">'.__('Expired').'</span><br>';
                    }else if ($item->status == 'Approve') {
                        $html .= '<span class="badge badge-success">'.__('Approved').'</span><br>';
                    }else if ($item->status == 'Completed') {
                        $html .= '<span class="badge badge-success">'.__('Completed').'</span><br>';
                    }else{
                        $html .= '<span class="badge badge-danger">'.$item->status.'</span><br>';
                    }
                    return $html;
                })
                ->rawColumns(['listing','user_details', 'date', 'additional_information', 'status'])
                ->make(TRUE);
        }

        return view('Admin.Directories.all_booking_list');
    }

    private function getAdditionalInfo ($item) {
        if ($item->reservation_for == 'hotel'){
            return $this->hotelAdditionalInfo($item);
        }else if ($item->reservation_for == 'restaurant'){
            return $this->restaurantAdditionalInfo($item);
        }else if ($item->reservation_for == 'beauty'){
            return $this->beautyAdditionalInfo($item);
        }else if ($item->reservation_for == 'shopping'){
            return $this->shopAdditionalInfo($item);
        }else if ($item->reservation_for == 'fitness'){
            return $this->fitnessAdditionalInfo($item);
        }
    }

    private function hotelAdditionalInfo($item){
        $html = '<p><strong>'. $item->room_name . '</strong><br>';
        $html .= '<strong>'. __('No. of guests: ') . '</strong>'.$item->adult_guest.'<br>';
        $html .= '<strong>'. __('Price: ') . '</strong>'.$item->room_price.'( '.get_currency().' )'.'<br>';
        return $html;
    }

    private function restaurantAdditionalInfo($item){
        $html = '';
        if (!empty($item->reservation_start_date)){
            $html = '<strong>'. __('Start: ') . '</strong>'.Carbon::parse($item->reservation_start_date)->format('d-m-Y').'<br>';
        }
        if (!empty($item->reservation_end_date)){
            $html .= '<strong>'. __('End: ') . '</strong>'.Carbon::parse($item->reservation_end_date)->format('d-m-Y').'<br>';
        }

        return $html;
    }

    private function beautyAdditionalInfo($item){
        $html = '<p><strong>'. $item->service_name . '</strong><br>';
        if (!empty($item->reservation_end_time)){
            $html .= '<strong>'. __('Reservation Date: ') . '</strong>'.Carbon::parse($item->reservation_start_date)->format('d-m-Y').'<br>';
        }
        if (!empty($item->reservation_end_time)){
            $html .= '<strong>'. __('Start: ') . '</strong>'. Carbon::parse($item->reservation_start_time)->format('h:iA').'<br>';
        }
        if (!empty($item->reservation_end_time)){
            $html .= '<strong>'. __('End: ') . '</strong>'. Carbon::parse($item->reservation_end_time)->format('h:iA').'<br>';
        }

        $html .= '<strong>'. __('Price: ') . '</strong>'.$item->service_price.'( '.get_currency().' )'.'<br>';
        return $html;
    }

    private function shopAdditionalInfo($item){
        $html = '';
        $html .= '<span>'.$item->reservation_note.'</span>'.'<br>';
        return $html;
    }

    private function fitnessAdditionalInfo($item){
        $html = '';
        $html .= '<span>'.$item->reservation_note.'</span>'.'<br>';
        return $html;
    }

    private function getListingInfo($item){
        $html ='';
        if ($item->reservation_for == 'hotel'){
            $html = '<strong> Directory Name: </strong><br>';
            $html .=  $item->spotlist_directory_name.'<br>';
            $html .= '<strong> Room Name: ' . $item->room_name . '</strong><br>';
            $html .=  $item->room_name . '<br>';
        }else if ($item->reservation_for == 'beauty'){
            $html = '<strong> Directory Name: </strong><br>';
            $html .= $item->spotlist_directory_name . '<br>';
            $html .= '<strong> Service Name: </strong><br>';
            $html .= $item->service_name . '<br>';
        }else if ($item->reservation_for == 'fitness'){
            $html = '<strong>' . $item->spotlist_directory_name . '</strong><br>';
        }else{
            $html .= '<strong>'.$item->spotlist_directory_name.'</strong><br>';
        }
        return $html;
    }

    public function bookingStatusChange(Request  $request){
        $response = $this->service->bookingStatusChange($request);
        return response()->json($response);
    }

    public function invoices(){
        return view('ListingManager.invoices');
    }

}
