<div class="col-lg-3 stick-wrap listing-col">
    <div class="search-btn-hide">
        <div class="btn btn btn-xs btn-info menu-toggler">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
    </div>
    <div class="listing-sidebar">
        <div>
            <div class="btn btn btn-xs btn-info menu-close">
                <i class="fa fa-times" aria-hidden="true"></i>
            </div>
            <form action="{{route('directory-search')}}" method="get">
                <div class="list-wiget search">
                    <div class="row">
                        <div class="col-6">
                            <h2>{{__('Search')}}</h2>
                        </div>
                    </div>
                    <div class="form-serch">
                        <input type="text" placeholder="Type Keywords" name="search_text">
                        <button type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
{{--                <div class="listing-scrool">--}}
                    <div class="list-wiget category">
                        <h2>{{__('Category')}}</h2>
                        @if(isset($categories) && !empty($categories[0]))
                            @foreach($categories as $category)
                                <div class="input-group">
                                    <input type="checkbox" id="category_{{$category->spotlist_category_id}}" name="categories[]" value="{{$category->spotlist_category_id}}"
                                        {{isset($search_key['categories'] ) && in_array($category->spotlist_category_id,$search_key['categories']) ? 'checked': ''}}>
                                    <label for="category_{{$category->spotlist_category_id}}">{{$category->spotlist_category_name}}</label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="list-wiget category">
                        <h2>{{__('Location')}}</h2>
                        @if(isset($locations) && !empty($locations[0]))
                            @foreach($locations as $location)
                                <div class="input-group">
                                    <input type="checkbox" id="location_{{$location->spotlist_cities_id}}" name="locations[]" value="{{$location->spotlist_cities_id}}"
                                        {{isset($search_key['locations'] ) && in_array($location->spotlist_cities_id,$search_key['locations']) ? 'checked': ''}}>
                                    <label for="location_{{$location->spotlist_cities_id}}">{{$location->spotlist_cities_name}}</label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="list-wiget category">
                        <h2>{{__('FACILITY')}}</h2>
                        @if(isset($features) && !empty($features[0]))
                            @foreach($features as $feature)
                                <div class="input-group">
                                    <input type="checkbox" id="features_{{$feature->spotlist_features_id}}" name="features[]" value="{{$feature->spotlist_features_id}}"
                                        {{isset($search_key['features'] ) && in_array($feature->spotlist_features_id,$search_key['features']) ? 'checked': ''}}>
                                    <label for="features_{{$feature->spotlist_features_id}}">{{$feature->spotlist_features_name}}</label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="list-wiget lratting">
                        <h2>{{__('RATING')}}</h2>
                        <div class="input-group">
                            <input type="checkbox" id="rt1" name="rating[]" value="5" {{isset($search_key['rating'] ) && in_array(5,$search_key['rating']) ? 'checked': ''}}>
                            <label for="rt1">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </label>
                        </div>
                        <div class="input-group">
                            <input type="checkbox" id="rt2" name="rating[]" value="4" {{isset($search_key['rating'] ) && in_array(4,$search_key['rating']) ? 'checked': ''}}>
                            <label for="rt2">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                </ul>
                            </label>
                        </div>
                        <div class="input-group">
                            <input type="checkbox" id="rt3" name="rating[]" value="3" {{isset($search_key['rating'] ) && in_array(3,$search_key['rating']) ? 'checked': ''}}>
                            <label for="rt3">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                </ul>
                            </label>
                        </div>
                        <div class="input-group">
                            <input type="checkbox" id="rt4" name="rating[]" value="2" {{isset($search_key['rating'] ) && in_array(2,$search_key['rating']) ? 'checked': ''}}>
                            <label for="rt4">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                </ul>
                            </label>
                        </div>
                        <div class="input-group">
                            <input type="checkbox" id="rt5" name="rating[]" value="1" {{isset($search_key['rating'] ) && in_array(1,$search_key['rating']) ? 'checked': ''}}>
                            <label for="rt5">
                                <ul>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                    <li><span><i class="fa fa-star"></i></span></li>
                                </ul>
                            </label>
                        </div>
                    </div>
{{--                </div>--}}
                <div class="side-btn">
                    <button class="btn theme-btn" type="submit">{{__('Apply')}}</button>
                </div>

            </form>
        </div>
    </div>

</div>
