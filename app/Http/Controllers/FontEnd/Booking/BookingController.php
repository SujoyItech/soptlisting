<?php

namespace App\Http\Controllers\FontEnd\Booking;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FontEnd\Listing\ListingService;
use App\Http\Service\NotificationService;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use View;

class BookingController extends Controller
{
    private $service;
    public function __construct(){
        $this->service = new BookingService();
    }
    public function bookings(Request $request,$notify_id = '',$booking_id = ''){

        if ($request->ajax()){
            $bookings = DB::table('spotlist_booking_request')
                          ->select('spotlist_booking_request.*','spotlist_directory.spotlist_directory_name')
                          ->join('spotlist_directory','spotlist_booking_request.spotlist_directory_id','=','spotlist_directory.spotlist_directory_id');
            if (!empty($booking_id)){
                $bookings = $bookings->where('spotlist_booking_request.spotlist_booking_request_id',$booking_id)->get();
            }else{
                $bookings->orderBy('spotlist_booking_request.spotlist_booking_request_id','desc')
                         ->where('customer_id',Auth::user()->id)->get();
            }
            return datatables($bookings)
                ->editColumn('status', function ($item) {
                    if ($item->status == 'Requested'){
                        return '<span class="badge badge-primary">'.$item->status.'</span>';
                    }else if ($item->status == 'Expired' || $item->status == 'Declined' || $item->status == 'Canceled'){
                        return '<span class="badge badge-danger">'.$item->status.'</span>';
                    }else if ($item->status == 'Confirmed' || $item->status == 'Approved'){
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }else{
                        return '<span class="badge badge-success">'.$item->status.'</span>';
                    }
                }) ->editColumn('reservation_for', function ($item) {
                    return ucfirst($item->reservation_for);
                })->editColumn('action', function ($item) {
                    $html = '';
                    if ($item->status == 'Completed'){
                        $html .= '<span href="" class="button btn-sm badge-info m-1"><a href="javascript:void(0);" class="text-light view_details" data-id="'.$item->spotlist_booking_request_id.'" data-toggle="tooltip" data-placement="top" title="View Details"><i class="fa fa-eye"></i></a></span>';
                    }else{
                        if ($item->status == 'Requested'){
                            $html .= '<span class="badge-warning button btn-sm m-1"><a href="javascript:void(0);" class="text-light cancel" data-id="'.$item->spotlist_booking_request_id.'" data-toggle="tooltip" data-placement="top" title="Cancel"><i class="fa fa-undo"></i></a></span>';
                        }
                        $html .= '<span href="" class="button btn-sm badge-info m-1"><a href="javascript:void(0);" class="text-light view_details" data-id="'.$item->spotlist_booking_request_id.'" data-toggle="tooltip" data-placement="top" title="View Details"><i class="fa fa-eye"></i></a></span>';
                        $html .= '<span href="" class="button btn-sm badge-info m-1"><a href="javascript:void(0);" class="text-light message" data-id="'.$item->spotlist_booking_request_id.'" data-toggle="tooltip" data-placement="top" title="Message"><i class="fa fa-send"></i></a></span>';
                        $html .= '<span href="" class="button btn-sm badge-danger m-1"><a href="javascript:void(0);" class="text-light delete_list" data-id="'.$item->spotlist_booking_request_id.'" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></a></span>';
                    }

                    return $html;
                })
                ->rawColumns(['status','reservation_for','action'])
                ->make(TRUE);
        }
        if (!empty($notify_id)){
            Notification::where('id',$notify_id)->update(['status'=>STATUS_ACTIVE]);
        }
        $data['notify_id'] = $notify_id;
        $data['booking_id'] = $booking_id;
        return view('FontEnd.profile.bookings',$data);
    }

    public function userBookingsDetails(Request $request){
        $reservation_data = DB::table('spotlist_booking_request')
            ->select('spotlist_booking_request.*','spotlist_directory.spotlist_directory_slug')
            ->join('spotlist_directory','spotlist_booking_request.spotlist_directory_id','=','spotlist_directory.spotlist_directory_id')
            ->where('spotlist_booking_request.spotlist_booking_request_id',$request->id)->first();
        $data['booking_id'] = $reservation_data->booking_id;
        if ($reservation_data->reservation_for == 'hotel'){
            if (!empty($reservation_data->spotlist_directory_type_hotel_room_id)){
                $data['hotel_data'] = DB::table('spotlist_directory_type_hotel_room')->where('spotlist_directory_type_hotel_room_id',$reservation_data->spotlist_directory_type_hotel_room_id)->first();
                $data['reservation_data'] = $reservation_data;
            }
        } elseif ($reservation_data->reservation_for == 'beauty'){
            if (!empty($reservation_data->spotlist_directory_type_beauty_id)){
                $data['beauty_data'] = DB::table('spotlist_directory_type_beauty')->where('spotlist_directory_type_beauty_id',$reservation_data->spotlist_directory_type_beauty_id)->first();
                $data['reservation_data'] = $reservation_data;
            }
        }
        else{
            $data_with_directory =  DB::table('spotlist_booking_request')
                                      ->leftJoin('spotlist_directory','spotlist_booking_request.spotlist_directory_id','=','spotlist_directory.spotlist_directory_id')
                                      ->select('spotlist_booking_request.*','spotlist_directory.spotlist_directory_name','spotlist_directory.spotlist_directory_slug','spotlist_directory.description','spotlist_directory.thumb_image')
                                      ->where('spotlist_booking_request.spotlist_booking_request_id',$request->id)
                                      ->first();
            if ($reservation_data->reservation_for == 'restaurant'){
                $data['restaurant_data'] = $data_with_directory;
            }elseif ($reservation_data->reservation_for == 'fitness'){
                $data['fitness_data'] = $data_with_directory;
            }elseif ($reservation_data->reservation_for == 'shop'){
                $data['shop_data'] = $data_with_directory ;
            }
        }

        return View::make('FontEnd.pages.booking_data.booking_details_body', $data);
    }

    public function checkBookingStartTime(Request $request){
        $response['success'] = FALSE;
        try {
            $start_time = Carbon::parse($request->start_time)->format('h:iA');
            $dayOfTheWeek = Carbon::parse($request->booking_date)->dayOfWeek;
            $weekMap = [
                0 => 'sunday',
                1 => 'monday',
                2 => 'tuesday',
                3 => 'wednesday',
                4 => 'thursday',
                5 => 'friday',
                6 => 'saturday',
            ];
            $weekday = $weekMap[$dayOfTheWeek];
            $schedule = DB::table('spotlist_directory_schedule')->where('spotlist_directory_id',$request->spotlist_directory_id)->first();
            if (isset($schedule)){
                $date = explode('-',$schedule->$weekday);
                $date[0] = strtolower($date[0]);

                if ($date[0] == 'closed'){
                    $response['success'] = FALSE;
                }else{
                    if (Carbon::parse($start_time)->gt(Carbon::parse($date[0]))){
                        $response['success'] = TRUE;
                    }else{
                        $response['success'] = FALSE;
                    }
                }
            }else{
                $response['success'] = FALSE;
            }
        }catch (\Exception $exception){
            $response['success'] = FALSE;
        }
        return response()->json($response);
    }
    public function checkBookingEndTime(Request $request){
        try {
            $response = [];
            $end_time = Carbon::parse($request->end_time)->format('h:iA');
            $dayOfTheWeek = Carbon::parse($request->booking_date)->dayOfWeek;
            $weekMap = [
                0 => 'sunday',
                1 => 'monday',
                2 => 'tuesday',
                3 => 'wednesday',
                4 => 'thursday',
                5 => 'friday',
                6 => 'saturday',
            ];
            $weekday = $weekMap[$dayOfTheWeek];
            $schedule = DB::table('spotlist_directory_schedule')->where('spotlist_directory_id',$request->spotlist_directory_id)->first();
            if (isset($schedule)){
                $date = explode('-',$schedule->$weekday);
                if ($date[1] == 'Closed'){
                    $response['success'] = FALSE;
                }else{
                    if (Carbon::parse($date[1])->gt(Carbon::parse($end_time))){
                        $response['success'] = TRUE;
                    }else{
                        $response['success'] = FALSE;
                    }
                }
            }else{
                $response['success'] = FALSE;
            }
            return response()->json($response);
        }catch (\Exception $exception){
           $response['success'] = FALSE;
           return response()->json($response);
        }
    }
    public function bookingRequestSend(Request $request){
        $response = $this->service->bookingRequestSend($request);
        if ($response['success'] == TRUE){
            $this->service->listingBookingsCount($request->spotlist_directory_id);
        }
        return response()->json($response);
    }

    public function userBookingsRequestCancel(Request $request){
        try {
            $post_data = [
                'spotlist_booking_request_id' => $request->id,
                'updated_at' => Carbon::now(),
                'status' => 'Canceled'
            ];
            $booking_request = DB::table('spotlist_booking_request')->where('spotlist_booking_request_id',$request->id)->first();
            DB::table('spotlist_booking_request')->where('spotlist_booking_request_id',$request->id)->update($post_data);
            $this->userBooingNotificationSend($booking_request);
            $response['success'] = TRUE;
            $response['message'] = __('Booking request canceled successfully.');
        }catch (\Exception $exception){
            $response['success'] = FALSE;
            $response['message'] = __('Booking request canceled failed.');
        }
        return response()->json($response);
    }

    public function userBookingsRequestDelete(Request $request){
        try {
            $booking_request = DB::table('spotlist_booking_request')->where('spotlist_booking_request_id',$request->id)->delete();
            $response['success'] = TRUE;
            $response['message'] = __('Booking request deleted successfully.');
        }catch (\Exception $exception){
            $response['success'] = FALSE;
            $response['message'] = __('Booking request deleted failed.');
        }
        return response()->json($response);
    }



    private function userBooingNotificationSend($booking_request){

        $directory = DB::table('spotlist_directory')->where('spotlist_directory_id',$booking_request->spotlist_directory_id)->first();
        $notify_body = [
            'username' => Auth::user()->username,
            'user_image' => Auth::user()->user_image,
            'id' => $booking_request->spotlist_booking_request_id,
            'title' => __(' Booking request cancel'),
            'body' => Auth::user()->username.__(' sent you cancel request for ').$directory->spotlist_directory_name,
            'type' => strtolower($directory->type)
        ];
        $notify_to = [
            'user_id' => $directory->sys_users_id,
            'type' => LISTING_MANAGER_ADMIN_BOOKING,
        ];
        $service = new NotificationService();
        $service->sendNotification($notify_body,$notify_to);
    }

}
