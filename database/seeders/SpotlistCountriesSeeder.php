<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistCountriesSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_countries')->delete();
        
        \DB::table('spotlist_countries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Afghanistan',
                'two_char_code' => 'AF',
                'three_char_code' => 'AFG',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Aland Islands',
                'two_char_code' => 'AX',
                'three_char_code' => 'ALA',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Albania',
                'two_char_code' => 'AL',
                'three_char_code' => 'ALB',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Algeria',
                'two_char_code' => 'DZ',
                'three_char_code' => 'DZA',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'American Samoa',
                'two_char_code' => 'AS',
                'three_char_code' => 'ASM',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Andorra',
                'two_char_code' => 'AD',
                'three_char_code' => 'AND',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Angola',
                'two_char_code' => 'AO',
                'three_char_code' => 'AGO',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Anguilla',
                'two_char_code' => 'AI',
                'three_char_code' => 'AIA',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Antarctica',
                'two_char_code' => 'AQ',
                'three_char_code' => 'ATA',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Antigua and Barbuda',
                'two_char_code' => 'AG',
                'three_char_code' => 'ATG',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Argentina',
                'two_char_code' => 'AR',
                'three_char_code' => 'ARG',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Armenia',
                'two_char_code' => 'AM',
                'three_char_code' => 'ARM',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Aruba',
                'two_char_code' => 'AW',
                'three_char_code' => 'ABW',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Australia',
                'two_char_code' => 'AU',
                'three_char_code' => 'AUS',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Austria',
                'two_char_code' => 'AT',
                'three_char_code' => 'AUT',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Azerbaijan',
                'two_char_code' => 'AZ',
                'three_char_code' => 'AZE',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Bahamas',
                'two_char_code' => 'BS',
                'three_char_code' => 'BHS',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Bahrain',
                'two_char_code' => 'BH',
                'three_char_code' => 'BHR',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Bangladesh',
                'two_char_code' => 'BD',
                'three_char_code' => 'BGD',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Barbados',
                'two_char_code' => 'BB',
                'three_char_code' => 'BRB',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Belarus',
                'two_char_code' => 'BY',
                'three_char_code' => 'BLR',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Belgium',
                'two_char_code' => 'BE',
                'three_char_code' => 'BEL',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Belize',
                'two_char_code' => 'BZ',
                'three_char_code' => 'BLZ',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Benin',
                'two_char_code' => 'BJ',
                'three_char_code' => 'BEN',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Bermuda',
                'two_char_code' => 'BM',
                'three_char_code' => 'BMU',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Bhutan',
                'two_char_code' => 'BT',
                'three_char_code' => 'BTN',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Bolivia',
                'two_char_code' => 'BO',
                'three_char_code' => 'BOL',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Bonaire, Sint Eustatius and Saba',
                'two_char_code' => 'BQ',
                'three_char_code' => 'BES',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Bosnia and Herzegovina',
                'two_char_code' => 'BA',
                'three_char_code' => 'BIH',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Botswana',
                'two_char_code' => 'BW',
                'three_char_code' => 'BWA',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Bouvet Island',
                'two_char_code' => 'BV',
                'three_char_code' => 'BVT',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Brazil',
                'two_char_code' => 'BR',
                'three_char_code' => 'BRA',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'British Indian Ocean Territory',
                'two_char_code' => 'IO',
                'three_char_code' => 'IOT',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Brunei',
                'two_char_code' => 'BN',
                'three_char_code' => 'BRN',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Bulgaria',
                'two_char_code' => 'BG',
                'three_char_code' => 'BGR',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Burkina Faso',
                'two_char_code' => 'BF',
                'three_char_code' => 'BFA',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Burundi',
                'two_char_code' => 'BI',
                'three_char_code' => 'BDI',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Cambodia',
                'two_char_code' => 'KH',
                'three_char_code' => 'KHM',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Cameroon',
                'two_char_code' => 'CM',
                'three_char_code' => 'CMR',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Canada',
                'two_char_code' => 'CA',
                'three_char_code' => 'CAN',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Cape Verde',
                'two_char_code' => 'CV',
                'three_char_code' => 'CPV',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Cayman Islands',
                'two_char_code' => 'KY',
                'three_char_code' => 'CYM',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Central African Republic',
                'two_char_code' => 'CF',
                'three_char_code' => 'CAF',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Chad',
                'two_char_code' => 'TD',
                'three_char_code' => 'TCD',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Chile',
                'two_char_code' => 'CL',
                'three_char_code' => 'CHL',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'China',
                'two_char_code' => 'CN',
                'three_char_code' => 'CHN',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Christmas Island',
                'two_char_code' => 'CX',
                'three_char_code' => 'CXR',
            ),
            47 => 
            array (
                'id' => 48,
            'name' => 'Cocos (Keeling) Islands',
                'two_char_code' => 'CC',
                'three_char_code' => 'CCK',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Colombia',
                'two_char_code' => 'CO',
                'three_char_code' => 'COL',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Comoros',
                'two_char_code' => 'KM',
                'three_char_code' => 'COM',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Congo',
                'two_char_code' => 'CG',
                'three_char_code' => 'COG',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Cook Islands',
                'two_char_code' => 'CK',
                'three_char_code' => 'COK',
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Costa Rica',
                'two_char_code' => 'CR',
                'three_char_code' => 'CRI',
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Ivory Coast',
                'two_char_code' => 'CI',
                'three_char_code' => 'CIV',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Croatia',
                'two_char_code' => 'HR',
                'three_char_code' => 'HRV',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Cuba',
                'two_char_code' => 'CU',
                'three_char_code' => 'CUB',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Curacao',
                'two_char_code' => 'CW',
                'three_char_code' => 'CUW',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Cyprus',
                'two_char_code' => 'CY',
                'three_char_code' => 'CYP',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Czech Republic',
                'two_char_code' => 'CZ',
                'three_char_code' => 'CZE',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Democratic Republic of the Congo',
                'two_char_code' => 'CD',
                'three_char_code' => 'COD',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Denmark',
                'two_char_code' => 'DK',
                'three_char_code' => 'DNK',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Djibouti',
                'two_char_code' => 'DJ',
                'three_char_code' => 'DJI',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Dominica',
                'two_char_code' => 'DM',
                'three_char_code' => 'DMA',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Dominican Republic',
                'two_char_code' => 'DO',
                'three_char_code' => 'DOM',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Ecuador',
                'two_char_code' => 'EC',
                'three_char_code' => 'ECU',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Egypt',
                'two_char_code' => 'EG',
                'three_char_code' => 'EGY',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'El Salvador',
                'two_char_code' => 'SV',
                'three_char_code' => 'SLV',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Equatorial Guinea',
                'two_char_code' => 'GQ',
                'three_char_code' => 'GNQ',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Eritrea',
                'two_char_code' => 'ER',
                'three_char_code' => 'ERI',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Estonia',
                'two_char_code' => 'EE',
                'three_char_code' => 'EST',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Ethiopia',
                'two_char_code' => 'ET',
                'three_char_code' => 'ETH',
            ),
            71 => 
            array (
                'id' => 72,
            'name' => 'Falkland Islands (Malvinas)',
                'two_char_code' => 'FK',
                'three_char_code' => 'FLK',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Faroe Islands',
                'two_char_code' => 'FO',
                'three_char_code' => 'FRO',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Fiji',
                'two_char_code' => 'FJ',
                'three_char_code' => 'FJI',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Finland',
                'two_char_code' => 'FI',
                'three_char_code' => 'FIN',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'France',
                'two_char_code' => 'FR',
                'three_char_code' => 'FRA',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'French Guiana',
                'two_char_code' => 'GF',
                'three_char_code' => 'GUF',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'French Polynesia',
                'two_char_code' => 'PF',
                'three_char_code' => 'PYF',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'French Southern Territories',
                'two_char_code' => 'TF',
                'three_char_code' => 'ATF',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Gabon',
                'two_char_code' => 'GA',
                'three_char_code' => 'GAB',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Gambia',
                'two_char_code' => 'GM',
                'three_char_code' => 'GMB',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Georgia',
                'two_char_code' => 'GE',
                'three_char_code' => 'GEO',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Germany',
                'two_char_code' => 'DE',
                'three_char_code' => 'DEU',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Ghana',
                'two_char_code' => 'GH',
                'three_char_code' => 'GHA',
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Gibraltar',
                'two_char_code' => 'GI',
                'three_char_code' => 'GIB',
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Greece',
                'two_char_code' => 'GR',
                'three_char_code' => 'GRC',
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Greenland',
                'two_char_code' => 'GL',
                'three_char_code' => 'GRL',
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'Grenada',
                'two_char_code' => 'GD',
                'three_char_code' => 'GRD',
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'Guadaloupe',
                'two_char_code' => 'GP',
                'three_char_code' => 'GLP',
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'Guam',
                'two_char_code' => 'GU',
                'three_char_code' => 'GUM',
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Guatemala',
                'two_char_code' => 'GT',
                'three_char_code' => 'GTM',
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Guernsey',
                'two_char_code' => 'GG',
                'three_char_code' => 'GGY',
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'Guinea',
                'two_char_code' => 'GN',
                'three_char_code' => 'GIN',
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Guinea-Bissau',
                'two_char_code' => 'GW',
                'three_char_code' => 'GNB',
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Guyana',
                'two_char_code' => 'GY',
                'three_char_code' => 'GUY',
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'Haiti',
                'two_char_code' => 'HT',
                'three_char_code' => 'HTI',
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Heard Island and McDonald Islands',
                'two_char_code' => 'HM',
                'three_char_code' => 'HMD',
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'Honduras',
                'two_char_code' => 'HN',
                'three_char_code' => 'HND',
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Hong Kong',
                'two_char_code' => 'HK',
                'three_char_code' => 'HKG',
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'Hungary',
                'two_char_code' => 'HU',
                'three_char_code' => 'HUN',
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'Iceland',
                'two_char_code' => 'IS',
                'three_char_code' => 'ISL',
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'India',
                'two_char_code' => 'IN',
                'three_char_code' => 'IND',
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'Indonesia',
                'two_char_code' => 'ID',
                'three_char_code' => 'IDN',
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Iran',
                'two_char_code' => 'IR',
                'three_char_code' => 'IRN',
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Iraq',
                'two_char_code' => 'IQ',
                'three_char_code' => 'IRQ',
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Ireland',
                'two_char_code' => 'IE',
                'three_char_code' => 'IRL',
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Isle of Man',
                'two_char_code' => 'IM',
                'three_char_code' => 'IMN',
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'Israel',
                'two_char_code' => 'IL',
                'three_char_code' => 'ISR',
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Italy',
                'two_char_code' => 'IT',
                'three_char_code' => 'ITA',
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Jamaica',
                'two_char_code' => 'JM',
                'three_char_code' => 'JAM',
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Japan',
                'two_char_code' => 'JP',
                'three_char_code' => 'JPN',
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Jersey',
                'two_char_code' => 'JE',
                'three_char_code' => 'JEY',
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'Jordan',
                'two_char_code' => 'JO',
                'three_char_code' => 'JOR',
            ),
            113 => 
            array (
                'id' => 114,
                'name' => 'Kazakhstan',
                'two_char_code' => 'KZ',
                'three_char_code' => 'KAZ',
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Kenya',
                'two_char_code' => 'KE',
                'three_char_code' => 'KEN',
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Kiribati',
                'two_char_code' => 'KI',
                'three_char_code' => 'KIR',
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'Kosovo',
                'two_char_code' => 'XK',
                'three_char_code' => '---',
            ),
            117 => 
            array (
                'id' => 118,
                'name' => 'Kuwait',
                'two_char_code' => 'KW',
                'three_char_code' => 'KWT',
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'Kyrgyzstan',
                'two_char_code' => 'KG',
                'three_char_code' => 'KGZ',
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Laos',
                'two_char_code' => 'LA',
                'three_char_code' => 'LAO',
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Latvia',
                'two_char_code' => 'LV',
                'three_char_code' => 'LVA',
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Lebanon',
                'two_char_code' => 'LB',
                'three_char_code' => 'LBN',
            ),
            122 => 
            array (
                'id' => 123,
                'name' => 'Lesotho',
                'two_char_code' => 'LS',
                'three_char_code' => 'LSO',
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Liberia',
                'two_char_code' => 'LR',
                'three_char_code' => 'LBR',
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Libya',
                'two_char_code' => 'LY',
                'three_char_code' => 'LBY',
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Liechtenstein',
                'two_char_code' => 'LI',
                'three_char_code' => 'LIE',
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Lithuania',
                'two_char_code' => 'LT',
                'three_char_code' => 'LTU',
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'Luxembourg',
                'two_char_code' => 'LU',
                'three_char_code' => 'LUX',
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Macao',
                'two_char_code' => 'MO',
                'three_char_code' => 'MAC',
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Macedonia',
                'two_char_code' => 'MK',
                'three_char_code' => 'MKD',
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'Madagascar',
                'two_char_code' => 'MG',
                'three_char_code' => 'MDG',
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Malawi',
                'two_char_code' => 'MW',
                'three_char_code' => 'MWI',
            ),
            132 => 
            array (
                'id' => 133,
                'name' => 'Malaysia',
                'two_char_code' => 'MY',
                'three_char_code' => 'MYS',
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Maldives',
                'two_char_code' => 'MV',
                'three_char_code' => 'MDV',
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'Mali',
                'two_char_code' => 'ML',
                'three_char_code' => 'MLI',
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'Malta',
                'two_char_code' => 'MT',
                'three_char_code' => 'MLT',
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'Marshall Islands',
                'two_char_code' => 'MH',
                'three_char_code' => 'MHL',
            ),
            137 => 
            array (
                'id' => 138,
                'name' => 'Martinique',
                'two_char_code' => 'MQ',
                'three_char_code' => 'MTQ',
            ),
            138 => 
            array (
                'id' => 139,
                'name' => 'Mauritania',
                'two_char_code' => 'MR',
                'three_char_code' => 'MRT',
            ),
            139 => 
            array (
                'id' => 140,
                'name' => 'Mauritius',
                'two_char_code' => 'MU',
                'three_char_code' => 'MUS',
            ),
            140 => 
            array (
                'id' => 141,
                'name' => 'Mayotte',
                'two_char_code' => 'YT',
                'three_char_code' => 'MYT',
            ),
            141 => 
            array (
                'id' => 142,
                'name' => 'Mexico',
                'two_char_code' => 'MX',
                'three_char_code' => 'MEX',
            ),
            142 => 
            array (
                'id' => 143,
                'name' => 'Micronesia',
                'two_char_code' => 'FM',
                'three_char_code' => 'FSM',
            ),
            143 => 
            array (
                'id' => 144,
                'name' => 'Moldava',
                'two_char_code' => 'MD',
                'three_char_code' => 'MDA',
            ),
            144 => 
            array (
                'id' => 145,
                'name' => 'Monaco',
                'two_char_code' => 'MC',
                'three_char_code' => 'MCO',
            ),
            145 => 
            array (
                'id' => 146,
                'name' => 'Mongolia',
                'two_char_code' => 'MN',
                'three_char_code' => 'MNG',
            ),
            146 => 
            array (
                'id' => 147,
                'name' => 'Montenegro',
                'two_char_code' => 'ME',
                'three_char_code' => 'MNE',
            ),
            147 => 
            array (
                'id' => 148,
                'name' => 'Montserrat',
                'two_char_code' => 'MS',
                'three_char_code' => 'MSR',
            ),
            148 => 
            array (
                'id' => 149,
                'name' => 'Morocco',
                'two_char_code' => 'MA',
                'three_char_code' => 'MAR',
            ),
            149 => 
            array (
                'id' => 150,
                'name' => 'Mozambique',
                'two_char_code' => 'MZ',
                'three_char_code' => 'MOZ',
            ),
            150 => 
            array (
                'id' => 151,
            'name' => 'Myanmar (Burma)',
                'two_char_code' => 'MM',
                'three_char_code' => 'MMR',
            ),
            151 => 
            array (
                'id' => 152,
                'name' => 'Namibia',
                'two_char_code' => 'NA',
                'three_char_code' => 'NAM',
            ),
            152 => 
            array (
                'id' => 153,
                'name' => 'Nauru',
                'two_char_code' => 'NR',
                'three_char_code' => 'NRU',
            ),
            153 => 
            array (
                'id' => 154,
                'name' => 'Nepal',
                'two_char_code' => 'NP',
                'three_char_code' => 'NPL',
            ),
            154 => 
            array (
                'id' => 155,
                'name' => 'Netherlands',
                'two_char_code' => 'NL',
                'three_char_code' => 'NLD',
            ),
            155 => 
            array (
                'id' => 156,
                'name' => 'New Caledonia',
                'two_char_code' => 'NC',
                'three_char_code' => 'NCL',
            ),
            156 => 
            array (
                'id' => 157,
                'name' => 'New Zealand',
                'two_char_code' => 'NZ',
                'three_char_code' => 'NZL',
            ),
            157 => 
            array (
                'id' => 158,
                'name' => 'Nicaragua',
                'two_char_code' => 'NI',
                'three_char_code' => 'NIC',
            ),
            158 => 
            array (
                'id' => 159,
                'name' => 'Niger',
                'two_char_code' => 'NE',
                'three_char_code' => 'NER',
            ),
            159 => 
            array (
                'id' => 160,
                'name' => 'Nigeria',
                'two_char_code' => 'NG',
                'three_char_code' => 'NGA',
            ),
            160 => 
            array (
                'id' => 161,
                'name' => 'Niue',
                'two_char_code' => 'NU',
                'three_char_code' => 'NIU',
            ),
            161 => 
            array (
                'id' => 162,
                'name' => 'Norfolk Island',
                'two_char_code' => 'NF',
                'three_char_code' => 'NFK',
            ),
            162 => 
            array (
                'id' => 163,
                'name' => 'North Korea',
                'two_char_code' => 'KP',
                'three_char_code' => 'PRK',
            ),
            163 => 
            array (
                'id' => 164,
                'name' => 'Northern Mariana Islands',
                'two_char_code' => 'MP',
                'three_char_code' => 'MNP',
            ),
            164 => 
            array (
                'id' => 165,
                'name' => 'Norway',
                'two_char_code' => 'NO',
                'three_char_code' => 'NOR',
            ),
            165 => 
            array (
                'id' => 166,
                'name' => 'Oman',
                'two_char_code' => 'OM',
                'three_char_code' => 'OMN',
            ),
            166 => 
            array (
                'id' => 167,
                'name' => 'Pakistan',
                'two_char_code' => 'PK',
                'three_char_code' => 'PAK',
            ),
            167 => 
            array (
                'id' => 168,
                'name' => 'Palau',
                'two_char_code' => 'PW',
                'three_char_code' => 'PLW',
            ),
            168 => 
            array (
                'id' => 169,
                'name' => 'Palestine',
                'two_char_code' => 'PS',
                'three_char_code' => 'PSE',
            ),
            169 => 
            array (
                'id' => 170,
                'name' => 'Panama',
                'two_char_code' => 'PA',
                'three_char_code' => 'PAN',
            ),
            170 => 
            array (
                'id' => 171,
                'name' => 'Papua New Guinea',
                'two_char_code' => 'PG',
                'three_char_code' => 'PNG',
            ),
            171 => 
            array (
                'id' => 172,
                'name' => 'Paraguay',
                'two_char_code' => 'PY',
                'three_char_code' => 'PRY',
            ),
            172 => 
            array (
                'id' => 173,
                'name' => 'Peru',
                'two_char_code' => 'PE',
                'three_char_code' => 'PER',
            ),
            173 => 
            array (
                'id' => 174,
                'name' => 'Phillipines',
                'two_char_code' => 'PH',
                'three_char_code' => 'PHL',
            ),
            174 => 
            array (
                'id' => 175,
                'name' => 'Pitcairn',
                'two_char_code' => 'PN',
                'three_char_code' => 'PCN',
            ),
            175 => 
            array (
                'id' => 176,
                'name' => 'Poland',
                'two_char_code' => 'PL',
                'three_char_code' => 'POL',
            ),
            176 => 
            array (
                'id' => 177,
                'name' => 'Portugal',
                'two_char_code' => 'PT',
                'three_char_code' => 'PRT',
            ),
            177 => 
            array (
                'id' => 178,
                'name' => 'Puerto Rico',
                'two_char_code' => 'PR',
                'three_char_code' => 'PRI',
            ),
            178 => 
            array (
                'id' => 179,
                'name' => 'Qatar',
                'two_char_code' => 'QA',
                'three_char_code' => 'QAT',
            ),
            179 => 
            array (
                'id' => 180,
                'name' => 'Reunion',
                'two_char_code' => 'RE',
                'three_char_code' => 'REU',
            ),
            180 => 
            array (
                'id' => 181,
                'name' => 'Romania',
                'two_char_code' => 'RO',
                'three_char_code' => 'ROU',
            ),
            181 => 
            array (
                'id' => 182,
                'name' => 'Russia',
                'two_char_code' => 'RU',
                'three_char_code' => 'RUS',
            ),
            182 => 
            array (
                'id' => 183,
                'name' => 'Rwanda',
                'two_char_code' => 'RW',
                'three_char_code' => 'RWA',
            ),
            183 => 
            array (
                'id' => 184,
                'name' => 'Saint Barthelemy',
                'two_char_code' => 'BL',
                'three_char_code' => 'BLM',
            ),
            184 => 
            array (
                'id' => 185,
                'name' => 'Saint Helena',
                'two_char_code' => 'SH',
                'three_char_code' => 'SHN',
            ),
            185 => 
            array (
                'id' => 186,
                'name' => 'Saint Kitts and Nevis',
                'two_char_code' => 'KN',
                'three_char_code' => 'KNA',
            ),
            186 => 
            array (
                'id' => 187,
                'name' => 'Saint Lucia',
                'two_char_code' => 'LC',
                'three_char_code' => 'LCA',
            ),
            187 => 
            array (
                'id' => 188,
                'name' => 'Saint Martin',
                'two_char_code' => 'MF',
                'three_char_code' => 'MAF',
            ),
            188 => 
            array (
                'id' => 189,
                'name' => 'Saint Pierre and Miquelon',
                'two_char_code' => 'PM',
                'three_char_code' => 'SPM',
            ),
            189 => 
            array (
                'id' => 190,
                'name' => 'Saint Vincent and the Grenadines',
                'two_char_code' => 'VC',
                'three_char_code' => 'VCT',
            ),
            190 => 
            array (
                'id' => 191,
                'name' => 'Samoa',
                'two_char_code' => 'WS',
                'three_char_code' => 'WSM',
            ),
            191 => 
            array (
                'id' => 192,
                'name' => 'San Marino',
                'two_char_code' => 'SM',
                'three_char_code' => 'SMR',
            ),
            192 => 
            array (
                'id' => 193,
                'name' => 'Sao Tome and Principe',
                'two_char_code' => 'ST',
                'three_char_code' => 'STP',
            ),
            193 => 
            array (
                'id' => 194,
                'name' => 'Saudi Arabia',
                'two_char_code' => 'SA',
                'three_char_code' => 'SAU',
            ),
            194 => 
            array (
                'id' => 195,
                'name' => 'Senegal',
                'two_char_code' => 'SN',
                'three_char_code' => 'SEN',
            ),
            195 => 
            array (
                'id' => 196,
                'name' => 'Serbia',
                'two_char_code' => 'RS',
                'three_char_code' => 'SRB',
            ),
            196 => 
            array (
                'id' => 197,
                'name' => 'Seychelles',
                'two_char_code' => 'SC',
                'three_char_code' => 'SYC',
            ),
            197 => 
            array (
                'id' => 198,
                'name' => 'Sierra Leone',
                'two_char_code' => 'SL',
                'three_char_code' => 'SLE',
            ),
            198 => 
            array (
                'id' => 199,
                'name' => 'Singapore',
                'two_char_code' => 'SG',
                'three_char_code' => 'SGP',
            ),
            199 => 
            array (
                'id' => 200,
                'name' => 'Sint Maarten',
                'two_char_code' => 'SX',
                'three_char_code' => 'SXM',
            ),
            200 => 
            array (
                'id' => 201,
                'name' => 'Slovakia',
                'two_char_code' => 'SK',
                'three_char_code' => 'SVK',
            ),
            201 => 
            array (
                'id' => 202,
                'name' => 'Slovenia',
                'two_char_code' => 'SI',
                'three_char_code' => 'SVN',
            ),
            202 => 
            array (
                'id' => 203,
                'name' => 'Solomon Islands',
                'two_char_code' => 'SB',
                'three_char_code' => 'SLB',
            ),
            203 => 
            array (
                'id' => 204,
                'name' => 'Somalia',
                'two_char_code' => 'SO',
                'three_char_code' => 'SOM',
            ),
            204 => 
            array (
                'id' => 205,
                'name' => 'South Africa',
                'two_char_code' => 'ZA',
                'three_char_code' => 'ZAF',
            ),
            205 => 
            array (
                'id' => 206,
                'name' => 'South Georgia and the South Sandwich Islands',
                'two_char_code' => 'GS',
                'three_char_code' => 'SGS',
            ),
            206 => 
            array (
                'id' => 207,
                'name' => 'South Korea',
                'two_char_code' => 'KR',
                'three_char_code' => 'KOR',
            ),
            207 => 
            array (
                'id' => 208,
                'name' => 'South Sudan',
                'two_char_code' => 'SS',
                'three_char_code' => 'SSD',
            ),
            208 => 
            array (
                'id' => 209,
                'name' => 'Spain',
                'two_char_code' => 'ES',
                'three_char_code' => 'ESP',
            ),
            209 => 
            array (
                'id' => 210,
                'name' => 'Sri Lanka',
                'two_char_code' => 'LK',
                'three_char_code' => 'LKA',
            ),
            210 => 
            array (
                'id' => 211,
                'name' => 'Sudan',
                'two_char_code' => 'SD',
                'three_char_code' => 'SDN',
            ),
            211 => 
            array (
                'id' => 212,
                'name' => 'Suriname',
                'two_char_code' => 'SR',
                'three_char_code' => 'SUR',
            ),
            212 => 
            array (
                'id' => 213,
                'name' => 'Svalbard and Jan Mayen',
                'two_char_code' => 'SJ',
                'three_char_code' => 'SJM',
            ),
            213 => 
            array (
                'id' => 214,
                'name' => 'Swaziland',
                'two_char_code' => 'SZ',
                'three_char_code' => 'SWZ',
            ),
            214 => 
            array (
                'id' => 215,
                'name' => 'Sweden',
                'two_char_code' => 'SE',
                'three_char_code' => 'SWE',
            ),
            215 => 
            array (
                'id' => 216,
                'name' => 'Switzerland',
                'two_char_code' => 'CH',
                'three_char_code' => 'CHE',
            ),
            216 => 
            array (
                'id' => 217,
                'name' => 'Syria',
                'two_char_code' => 'SY',
                'three_char_code' => 'SYR',
            ),
            217 => 
            array (
                'id' => 218,
                'name' => 'Taiwan',
                'two_char_code' => 'TW',
                'three_char_code' => 'TWN',
            ),
            218 => 
            array (
                'id' => 219,
                'name' => 'Tajikistan',
                'two_char_code' => 'TJ',
                'three_char_code' => 'TJK',
            ),
            219 => 
            array (
                'id' => 220,
                'name' => 'Tanzania',
                'two_char_code' => 'TZ',
                'three_char_code' => 'TZA',
            ),
            220 => 
            array (
                'id' => 221,
                'name' => 'Thailand',
                'two_char_code' => 'TH',
                'three_char_code' => 'THA',
            ),
            221 => 
            array (
                'id' => 222,
            'name' => 'Timor-Leste (East Timor)',
                'two_char_code' => 'TL',
                'three_char_code' => 'TLS',
            ),
            222 => 
            array (
                'id' => 223,
                'name' => 'Togo',
                'two_char_code' => 'TG',
                'three_char_code' => 'TGO',
            ),
            223 => 
            array (
                'id' => 224,
                'name' => 'Tokelau',
                'two_char_code' => 'TK',
                'three_char_code' => 'TKL',
            ),
            224 => 
            array (
                'id' => 225,
                'name' => 'Tonga',
                'two_char_code' => 'TO',
                'three_char_code' => 'TON',
            ),
            225 => 
            array (
                'id' => 226,
                'name' => 'Trinidad and Tobago',
                'two_char_code' => 'TT',
                'three_char_code' => 'TTO',
            ),
            226 => 
            array (
                'id' => 227,
                'name' => 'Tunisia',
                'two_char_code' => 'TN',
                'three_char_code' => 'TUN',
            ),
            227 => 
            array (
                'id' => 228,
                'name' => 'Turkey',
                'two_char_code' => 'TR',
                'three_char_code' => 'TUR',
            ),
            228 => 
            array (
                'id' => 229,
                'name' => 'Turkmenistan',
                'two_char_code' => 'TM',
                'three_char_code' => 'TKM',
            ),
            229 => 
            array (
                'id' => 230,
                'name' => 'Turks and Caicos Islands',
                'two_char_code' => 'TC',
                'three_char_code' => 'TCA',
            ),
            230 => 
            array (
                'id' => 231,
                'name' => 'Tuvalu',
                'two_char_code' => 'TV',
                'three_char_code' => 'TUV',
            ),
            231 => 
            array (
                'id' => 232,
                'name' => 'Uganda',
                'two_char_code' => 'UG',
                'three_char_code' => 'UGA',
            ),
            232 => 
            array (
                'id' => 233,
                'name' => 'Ukraine',
                'two_char_code' => 'UA',
                'three_char_code' => 'UKR',
            ),
            233 => 
            array (
                'id' => 234,
                'name' => 'United Arab Emirates',
                'two_char_code' => 'AE',
                'three_char_code' => 'ARE',
            ),
            234 => 
            array (
                'id' => 235,
                'name' => 'United Kingdom',
                'two_char_code' => 'GB',
                'three_char_code' => 'GBR',
            ),
            235 => 
            array (
                'id' => 236,
                'name' => 'United States',
                'two_char_code' => 'US',
                'three_char_code' => 'USA',
            ),
            236 => 
            array (
                'id' => 237,
                'name' => 'United States Minor Outlying Islands',
                'two_char_code' => 'UM',
                'three_char_code' => 'UMI',
            ),
            237 => 
            array (
                'id' => 238,
                'name' => 'Uruguay',
                'two_char_code' => 'UY',
                'three_char_code' => 'URY',
            ),
            238 => 
            array (
                'id' => 239,
                'name' => 'Uzbekistan',
                'two_char_code' => 'UZ',
                'three_char_code' => 'UZB',
            ),
            239 => 
            array (
                'id' => 240,
                'name' => 'Vanuatu',
                'two_char_code' => 'VU',
                'three_char_code' => 'VUT',
            ),
            240 => 
            array (
                'id' => 241,
                'name' => 'Vatican City',
                'two_char_code' => 'VA',
                'three_char_code' => 'VAT',
            ),
            241 => 
            array (
                'id' => 242,
                'name' => 'Venezuela',
                'two_char_code' => 'VE',
                'three_char_code' => 'VEN',
            ),
            242 => 
            array (
                'id' => 243,
                'name' => 'Vietnam',
                'two_char_code' => 'VN',
                'three_char_code' => 'VNM',
            ),
            243 => 
            array (
                'id' => 244,
                'name' => 'Virgin Islands, British',
                'two_char_code' => 'VG',
                'three_char_code' => 'VGB',
            ),
            244 => 
            array (
                'id' => 245,
                'name' => 'Virgin Islands, US',
                'two_char_code' => 'VI',
                'three_char_code' => 'VIR',
            ),
            245 => 
            array (
                'id' => 246,
                'name' => 'Wallis and Futuna',
                'two_char_code' => 'WF',
                'three_char_code' => 'WLF',
            ),
            246 => 
            array (
                'id' => 247,
                'name' => 'Western Sahara',
                'two_char_code' => 'EH',
                'three_char_code' => 'ESH',
            ),
            247 => 
            array (
                'id' => 248,
                'name' => 'Yemen',
                'two_char_code' => 'YE',
                'three_char_code' => 'YEM',
            ),
            248 => 
            array (
                'id' => 249,
                'name' => 'Zambia',
                'two_char_code' => 'ZM',
                'three_char_code' => 'ZMB',
            ),
            249 => 
            array (
                'id' => 250,
                'name' => 'Zimbabwe',
                'two_char_code' => 'ZW',
                'three_char_code' => 'ZWE',
            ),
        ));

        
    }
}