<div class="featured-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <div class="section-title">
                    <h2>{{__('Featured Categories')}}</h2>
                    <a class="btn theme-btn" href="{{route('all-categories')}}">{{__('View All')}}</a>
                </div>
                <div class="country-r">
                    <div class="country-carousel owl-carousel owl-theme owl-btn-center-lr owl-dots-primary-full owl-btn-3 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">

                            @foreach($featured_category as $category)
                                <div class="item">
                                    <a href="{{route('show-list-by-category',['category_name'=>$category->spotlist_category_name])}}">
                                        <div class="featured-item">
                                            <div class="featured-img">
                                                <img src="{{isset($category->category_image) && file_exists(public_path($category->category_image)) ? asset($category->category_image) : getDefaultImage()}}" alt="" height="300">
                                            </div>
                                            <div class="featured-content">
                                                <h5><a href="{{route('show-list-by-category',['category_name'=>$category->spotlist_category_name])}}">{{$category->spotlist_category_name}}</a></h5>
                                            </div>
                                            <div class="thumb">
                                                <span>{{$category->total_listing ?? 0 }} {{__('Listing')}}</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
