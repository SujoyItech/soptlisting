@extends('Backend.layouts.app',['menu'=>'settings','sub_menu'=>'how_works'])
@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-question-circle"></i> {{__('How it works')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('how-it-work-save')}}" class="parsley-examples" method="POST" id="submit_how_works" enctype="multipart/form-data">
            <div class="works_title">
                @if(isset($how_it_works) && !empty($how_it_works[0]))
                    @php($flag = 0)
                    @foreach($how_it_works as $how_it_work)
                        @if($flag == 0)
                            <div class="card-box mb-3">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="name">{{__('Title')}} <span class="text-danger">*</span></label>
                                                    <input type="text" name="title[]"  value="{{ $how_it_work->title ?? ''}}" parsley-trigger="change" required  placeholder="Title" class="form-control" >
                                                    <input type="hidden" name="id[]" value="{{ $how_it_work->id ?? ''}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="name">{{__('Description')}}</label>
                                                    <textarea name="description[]" placeholder="Description here" class="form-control" rows="3">{{$how_it_work->description ?? ''}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="name">{{__('Icon')}}</label>
                                            <input type="file" name="icon[]" data-plugins="dropify" class="dropify"
                                                   data-default-file="{{isset($how_it_work->icon) ? asset($how_it_work->icon) : ''}}"
                                                   data-allowed-file-extensions="png jpg jpeg jfif"
                                                   data-max-file-size="2M" />
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <button class="btn btn-info btn-sm add_new" type="button"><i class="fa fa-plus"></i> {{__('Add more')}}</button>
                                    </div>
                                </div>
                            </div>
                            @php($flag = 1)
                        @else
                        <div class="card-box mb-3 work_heading">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="name">{{__('Title')}} <span class="text-danger">*</span></label>
                                                <input type="text" name="title[]"  value="{{ $how_it_work->title ?? ''}}" parsley-trigger="change" required  placeholder="Title" class="form-control" >
                                                <input type="hidden" name="id[]" value="{{ $how_it_work->id ?? ''}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="name">{{__('Description')}}</label>
                                                <textarea name="description[]" placeholder="Description here" class="form-control" rows="3">{{$how_it_work->description ?? ''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="name">{{__('Icon')}}</label>
                                        <input type="file" name="icon[]" data-plugins="dropify" class="dropify"
                                               data-default-file="{{isset($how_it_work->icon) ? asset($how_it_work->icon) : ''}}"
                                               data-allowed-file-extensions="png jpg jpeg jfif"
                                               data-max-file-size="2M" />
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button class="btn btn-danger btn-sm remove" type="button"><i class="fa fa-minus"></i> {{__('Remove')}}</button>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                @else
                    <div class="card-box mb-3">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name">{{__('Title')}} <span class="text-danger">*</span></label>
                                            <input type="text" name="title[]"  value="" parsley-trigger="change" required  placeholder="Title" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="name">{{__('Description')}}</label>
                                            <textarea name="description[]" placeholder="Description here" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="name">{{__('Icon')}}</label>
                                    <input type="file" name="icon[]" data-plugins="dropify" class="dropify"
                                           data-default-file=""
                                           data-allowed-file-extensions="png jpg jpeg jfif"
                                           data-max-file-size="2M" />
                                </div>
                            </div>

                            <div class="col-12">
                                <button class="btn btn-info btn-sm add_new" type="button"><i class="fa fa-plus"></i> {{__('Add more')}}</button>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row pl-2">
                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>

        </form>
    </div>
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        $(document).ready(function (){
            var x = "{{$total_counts}}";
            var work_html = '<div class="card-box mb-3 work_heading">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-lg-9">\n' +
                '                                        <div class="row">\n' +
                '                                            <div class="col-lg-12">\n' +
                '                                                <div class="form-group">\n' +
                '                                                    <label for="name">{{__('Title')}} <span class="text-danger">*</span></label>\n' +
                '                                                    <input type="text" name="title[]"  value="" parsley-trigger="change" required  placeholder="Title" class="form-control" >\n' +
                '                                                </div>\n' +
                '                                            </div>\n' +
                '                                            <div class="col-lg-12">\n' +
                '                                                <div class="form-group">\n' +
                '                                                    <label for="name">{{__('Description')}}</label>\n' +
                '                                                    <textarea name="description[]" placeholder="Description here" class="form-control" rows="3"></textarea>\n' +
                '                                                </div>\n' +
                '                                            </div>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '                                    <div class="col-lg-3">\n' +
                '                                        <div class="form-group">\n' +
                '                                            <label for="name">{{__('Icon')}}</label>\n' +
                '                                            <input type="file" name="icon[]" data-plugins="dropify" class="dropify"\n' +
                '                                                   data-default-file=""\n' +
                '                                                   data-allowed-file-extensions="png jpg jpeg jfif"\n' +
                '                                                   data-max-file-size="2M" />\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-12">\n' +
                '                                        <button class="btn btn-danger btn-sm remove" type="button"><i class="fa fa-minus"></i> {{__('Remove')}}</button>\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '                            </div>';


                $(document).on('click','.add_new',function (){
                    if (x<4){
                        $('.works_title').append(work_html);
                        $('.dropify').dropify();
                        x++;
                    }else {
                        swalWarning('You can add maximum four data!');
                    }
                });

                $(document).on('click','.remove',function (e) {
                    e.preventDefault();
                    $(this).closest('.work_heading').remove();
                    x--;
                })
        })




        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            swalSuccess(response.message)
                        }else{
                            swalError(response.message)
                        }
                    });
                }
            });
        });
    </script>
@endsection

