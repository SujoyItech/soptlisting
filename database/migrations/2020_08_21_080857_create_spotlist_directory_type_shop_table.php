<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistDirectoryTypeShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_directory_type_shop', function (Blueprint $table) {
            $table->increments('spotlist_directory_type_shop_id');
            $table->unsignedInteger('spotlist_directory_id');
            $table->string('product_name')->nullable();
            $table->text('product_varient')->nullable();
            $table->decimal('product_price',10,2)->nullable();
            $table->string('product_imag')->nullable();
            $table->text('product_description')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_directory_type_shop');
    }
}
