@extends('Backend.layouts.app',['menu'=>'city','sub_menu'=>'city_list'])
@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-city-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New City')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('City List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body cl-table">
                    <table id="example" class="table">
                        <thead>
                        <tr>
                            <th>{{__('Image')}}</th>
                            <th>{{__('City name')}}</th>
                            <th>{{__('Country name')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.jqueryui.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: true,
                ajax: '{{url('admin-city-list')}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.spotlist_cities_id);
                },
                columns: [
                    {"data": "city_image"},
                    {"data": "spotlist_cities_name"},
                    {"data": "spotlist_country_name"},
                    {"data": "status"},
                    {"data": "action"},
                ],
            });
        });
        $(document).on('click',".delete_row",function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var deleted_row = $(this).data('id');
            var check_city_used_url = "{{url('check-city-used')}}";
            var check_city_data = {
                id : deleted_row
            };
            makeAjaxPost(check_city_data,check_city_used_url,load).done(function (response){
                if (response.success == true){
                    swalConfirm("Do you really want to delete this?").then(function (s) {
                        if(s.value){
                            var url = "{{url('delete-list-by-id')}}";
                            var data = {
                                id : deleted_row,
                                table_name : 'spotlist_cities',
                                primary_key : 'spotlist_cities_id',
                            };
                            makeAjaxPost(data, url, load).done(function (response) {
                                if(response.success == false) {
                                    swalError(response.message);
                                    load.ladda('stop');
                                } else {
                                    swalRedirect('',response.message,'success');
                                }
                            });
                        }else{
                            load.ladda('stop');
                        }
                    })
                }else {
                    load.ladda('stop');
                    swalError('This city is used in directory.')
                }

            })
        });
    </script>

@endsection
