@if(isset($messages) &&!empty($messages[0]))
    @foreach($messages as $notify)
        @php($notify_body = json_decode($notify->body))

        @if($notify->type == MESSAGE_NOTIFICATION)
        <!-- item-->
        <a href="{{route('user-booking-request',['type'=>$notify_body->type ?? 'all','booking_id'=>$notify_body->id ?? ''])}}" class="dropdown-item notify-item" data-id="{{$notify->id}}">
            <div class="notify-icon">
                <img src="{{getImageUrl($notify_body->user_image)}}" class="rounded-circle" alt="" width="40" height="40"/>
            </div>
            <p class="notify-details">
                {{$notify_body->username ?? ''}}
            </p>
            <p class="text-muted mb-0 user-msg">
                <small class="{{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}}">{{$notify_body->title}}</small><br>
                <small class="text-muted">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</small>
            </p>
        </a>
        @elseif($notify->type == GENERAL_MESSAGE_NOTIFICATION)
            <a href="{{url('admin-user-message-list')}}" class="dropdown-item notify-item" data-id="{{$notify->id}}">
                <p class="notify-details">
                    {{$notify_body->username ?? ''}}
                </p>
                <p class="text-muted mb-0 user-msg">
                    <small class="{{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}}">{{$notify_body->title}}</small><br>
                    <small class="text-muted">{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</small>
                </p>
            </a>
        @endif
    @endforeach
@else
    <p class="text-center">{{__('No new messages')}}</p>
@endif
