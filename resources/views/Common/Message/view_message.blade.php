@extends('Backend.layouts.app')

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('Message details')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <!-- Left sidebar -->
                <div class="inbox-leftbar">

                    <div class="mail-list mt-4">
                        <a href="{{route('view-all-admin-message')}}"><i class="dripicons-inbox mr-2"></i>{{__('ALL')}}<span class="badge badge-soft-info float-right ml-2">{{$all_message_count ?? 0}}</span></a>
                        <a href="{{route('view-all-admin-message',['type'=>'read'])}}"><i class="dripicons-star mr-2"></i>{{__('Read')}}<span class="badge badge-soft-info float-right ml-2">{{$read_message_count ?? 0}}</span></a>
                        <a href="{{route('view-all-admin-message',['type'=>'unread'])}}"><i class="dripicons-clock mr-2"></i>{{__('Unread')}}<span class="badge badge-soft-info float-right ml-2">{{$unread_message_count ?? 0}}</span></a>
                    </div>
                </div>
                <!-- End Left sidebar -->

                <div class="inbox-rightbar">

                    @php($message_body = json_decode($message->body))
                    <div class="mt-4">
                        <h5 class="font-18">{{$message_body->body ?? ''}}</h5>
                        <hr/>

                        <div class="media mb-3 mt-1">
                            <img class="d-flex mr-2 rounded-circle" src="{{getImageUrl($message_body->user_image ?? '')}}" height="32">
                            <div class="media-body">
                                <small class="float-right">{{Carbon\Carbon::parse($message->created_at)->diffForHumans()}}</small>
                                <h6 class="m-0 font-14">{{$message_body->username ?? ''}}</h6>
                            </div>
                        </div>
                        <h6>{{$message_body->title ?? ''}}</h6>
                        <p>{{$message_body->message ?? ''}}</p>

                    </div>
                    <!-- end .mt-4 -->

                    <!-- end row-->
                </div>
                <!-- end inbox-rightbar-->

                <div class="clearfix"></div>
            </div> <!-- end card-box -->

        </div> <!-- end Col -->
    </div>
@endsection
@section('script')
@endsection
