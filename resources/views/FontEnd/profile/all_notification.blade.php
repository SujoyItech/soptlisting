@extends('FontEnd.master')

@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('My Notification')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- start faq-pg-section -->
    <section class="faq-pg-section section-padding">
        <div class="container">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="{{route('user.profile.settings')}}"  aria-expanded="false" class="nav-link">
                        {{__('Profile')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.bookings')}}"  aria-expanded="true" class="nav-link">
                        {{__('My bookings')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.wishing')}}" aria-expanded="false" class="nav-link">
                        {{__('My Wishlist')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user-all-notification')}}" aria-expanded="false" class="nav-link active">
                        {{__('My notifications')}}
                    </a>
                </li>
            </ul>
            @if(isset($notifications) &&!empty($notifications[0]))
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <!-- users -->
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div data-simplebar>
                                            @foreach($notifications as $notify)
                                                @php($notify_body = json_decode($notify->body))
                                                @if($notify->type == USER_BOOKING)
                                                    <a href="{{route('user.bookings',['notify_id'=>$notify->id,'booking_id'=>$notify_body->id ?? ''])}}" class="text-body">
                                                        <div class="media p-2">
                                                            <img src="{{getImageUrl($notify_body->directory_image,'others')}}" class="mt-3 mr-4 rounded-circle"  alt="" width="50" height="50" />
                                                            <div class="media-body">
                                                                <div class="d float-right not-p">
                                                                    <span class="text-muted font-weight-normal"><small>{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</small></span>
                                                                    <span class="text-muted font-weight-normal d-block"><small>{{$notify->status == STATUS_SUCCESS ? 'seen' : ''}}</small></span>
                                                                </div>
                                                                {{$notify_body->directory_name ?? ''}}
                                                                <h5 class="mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                                    <span class="w-75">{{$notify_body->title ?? ''}}</span>
                                                                </h5>
                                                                <p class="mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                                    <span class="w-75">{{$notify_body->body ?? ''}}</span>
                                                                </p>
                                                                <p class="mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                                    <span class="w-75">{{$notify_body->message ?? ''}}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @else
                                                    <a href="{{route('user.bookings',['notify_id'=>$notify->id,'booking_id'=>$notify_body->id ?? ''])}}" class="text-body">
                                                        <div class="media p-2">
                                                            <img src="{{getImageUrl($notify_body->user_image,'user')}}" class="mt-3 mr-4 rounded-circle"  alt="" width="50" height="50" />
                                                            <div class="media-body">
                                                                <div class="d float-right not-p">
                                                                    <span class="text-muted font-weight-normal"><small>{{Carbon\Carbon::parse($notify->created_at)->diffForHumans()}}</small></span>
                                                                    <span class="text-muted font-weight-normal d-block"><small>{{$notify->status == STATUS_SUCCESS ? 'seen' : ''}}</small></span>
                                                                </div>
                                                                {{$notify_body->user_name ?? ''}}
                                                                <h5 class="mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                                    <span class="w-75">{{$notify_body->title ?? ''}}</span>
                                                                </h5>
                                                                <p class="mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                                    <span class="w-75">{{$notify_body->body ?? ''}}</span>
                                                                </p>
                                                                <p class="mb-0 {{$notify->status == STATUS_SUCCESS ? 'text-muted' : 'text-dark'}} font-14">
                                                                    <span class="w-75">{{$notify_body->message ?? ''}}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @endif
                                                <hr>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="listing-wrap">
                                        <div class="pagination-wrapper">
                                            {{ $notifications->links('vendor.pagination.bootstrap-4') }}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
            @endif
        </div> <!-- end container -->
    </section>
    <!-- end faq-pg-section -->
    @include('FontEnd.pages.user_email_subscription')
@endsection


