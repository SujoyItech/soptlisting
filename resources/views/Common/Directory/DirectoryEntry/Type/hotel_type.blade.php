@if(isset($hotel_data) && !empty($hotel_data[0]))
    @foreach($hotel_data as $hotel)
        <div class="row mb-3 heading">
            <div class="col-md-8 col-lg-9">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="col-form-label" for="name">{{__('Room name')}}</label>
                            <input type="text" name="room_name[]" value="{{$hotel->room_name ?? ''}}"
                                   placeholder="Enter room name" class="form-control">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="col-form-label" for="room_price">{{__('Room price')}} ( {{get_currency()}} )</label>
                            <input type="number" class="form-control" name="room_price[]" value="{{$hotel->room_price ?? ''}}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label" for="room_description">{{__('Hotel room amenities (Press enter after entering every amenity)')}}</label>
                            <input type="text" data-role="tagsinput" name="room_facilities[]" value="{{$hotel->room_facilities ?? ''}}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="col-form-label" for="room_description">{{__('Room description ( Max length 500 word )')}}</label>
                            <textarea id="address" class="form-control" name="room_description[]" maxlength="500">{{$hotel->room_description ?? ''}}</textarea>
                        </div>
                    </div>

                    <div class="col-12">
                        <input type="hidden" name="spotlist_directory_type_hotel_room_id[]" value="{{$hotel->spotlist_directory_type_hotel_room_id ?? ''}}">
                        <button class="btn btn-danger remove_info" type="button"
                                data-id="{{$hotel->spotlist_directory_type_hotel_room_id ?? ''}}" data-table="spotlist_directory_type_hotel_room" data-primary-key="spotlist_directory_type_hotel_room_id">
                            <i class="fa fa-minus"></i> {{__('Remove')}}</button>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-3">
                <div class="form-group">
                    <label class="col-form-label" for="picture">{{__('Room image')}}</label>
                    <input type="file" name="room_image[]" data-plugins="dropify"
                           data-default-file="{{isset($hotel->room_image) ? asset($hotel->room_image) : ''}}"
                           data-allowed-file-extensions="png jpg jpeg jfif"
                           data-max-file-size="2M" />
                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                </div>
            </div>
        </div>

    @endforeach
@endif
<div class="hotel-list">
    <div class="row mb-5">
        <div class="col-md-8 col-lg-9">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-form-label" for="name">{{__('Room name')}}</label>
                        <input type="text" name="room_name[]" value=""
                               placeholder="Enter room name" class="form-control">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label class="col-form-label" for="room_price">{{__('Room price')}} ( {{get_currency()}} )</label>
                        <input type="number" class="form-control" name="room_price[]">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label" for="room_description">{{__('Hotel room amenities (Press enter after entering every amenity)')}}</label>
                        <input type="text" value="" data-role="tagsinput" name="room_facilities[]">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="col-form-label" for="room_description">{{__('Room description ( Max length 500 word )')}}</label>
                        <textarea id="address" class="form-control" name="room_description[]" maxlength="500" ></textarea>
                    </div>
                </div>

                <div class="col-12">
                    <button class="btn btn-info add_hotel" type="button"><i class="fa fa-plus"></i> {{__('Add more')}}</button>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4">
            <div class="form-group">
                <label class="col-form-label" for="picture">{{__('Room image')}}</label>
                <input type="file" name="room_image[]" data-plugins="dropify"
                       data-default-file=""
                       data-allowed-file-extensions="png jpg jpeg jfif"
                       data-max-file-size="2M" />
                <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
            </div>
        </div>
    </div>
</div>



<script>

    $('.add_hotel').on('click',function (){

        var hotel_html = '<div class="row hotel_heading mb-5">\n' +
            '    <div class="col-lg-9 col-md-3">\n' +
            '        <div class="row">\n' +
            '            <div class="col-lg-6">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label" for="name">{{__('Room name')}}</label>\n' +
            '                    <input type="text" name="room_name[]" parsley-trigger="change" required value=""\n' +
            '                           placeholder="Enter room name" class="form-control">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-6">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label" for="room_price">{{__('Room price')}} ({{get_currency()}})</label>\n' +
            '                    <input type="number" class="form-control" name="room_price[]">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label" for="room_description">{{__('Hotel room amenities (Press enter after entering every amenity)')}}</label>\n' +
            '                    <input type="text" value="" data-role="tagsinput" name="room_facilities[]">\n' +
            '                </div>\n' +
            '            </div>\n' +
            '            <div class="col-12">\n' +
            '                <div class="form-group">\n' +
            '                    <label class="col-form-label" for="room_description">{{__('Room description( Max length 500 word )')}}</label>\n' +
            '                    <textarea id="address" class="form-control" name="room_description[]" maxlength="500"></textarea>\n' +
            '                </div>\n' +
            '            </div>\n' +
            '\n' +
            '            <div class="col-12 remove_hotel">\n' +
            '                <button class="btn btn-danger remove_hotel" type="button"><i class="fa fa-minus"></i> {{__('Remove')}}</button>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '    <div class="col-md-4 col-lg-3">\n' +
            '        <div class="form-group">\n' +
            '            <label class="col-form-label" for="picture">{{__('Room image')}}</label>\n' +
            '            <input type="file" name="room_image[]" class="dropify" data-plugins="dropify"\n' +
            '                   data-default-file=""\n' +
            '                   data-allowed-file-extensions="png jpg jpeg jfif"\n' +
            '                   data-max-file-size="2M" />\n' +
            '            <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';


        $('.hotel-list').append(hotel_html);
        $('input[data-role=tagsinput]').tagsinput();
        $('.dropify').dropify();
    });

    $(document).on('click','.remove_hotel',function (e) {
        e.preventDefault();
        $(this).closest('.hotel_heading').remove();
    })
</script>

