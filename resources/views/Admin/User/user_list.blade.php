@extends('Backend.layouts.app',['menu'=>'user','sub_menu'=>'user_list'])
@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-user-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New User')}}</a>
                    <button class="btn btn-info btn-xs no-display" id="edit"><i class="fa fa-edit" aria-hidden="true"></i> {{__('Edit')}}</button>
                    <button class="btn btn-danger btn-xs no-display" id="delete-rows"><i class="fa fa-trash" aria-hidden="true"></i> {{__('Delete')}}</button>
                </div>
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('User List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table">
                        <thead>
                        <tr>
                            <th>{{__('Picture')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('User type')}}</th>
                            <th>{{__('Address')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Action')}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.jqueryui.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: true,
                ajax: '{{url('admin-users-list')}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.spotlist_subscription_purchase_history_id);
                },
                columns: [
                    {"data": "user_image"},
                    {"data": "username"},
                    {"data": "email"},
                    {"data": "default_module_id"},
                    {"data": "address"},
                    {"data": "status"},
                    {"data": "action"},
                ],
            });
        });
        var ids = [];
        $(document).on('click', '#table-to-print tbody tr', function(){
            if($(this).toggleClass('selected')) {
                var id = $(this).attr('id');
                if ($(this).hasClass('selected')) {
                    ids.push(id);
                } else {
                    ids.splice(ids.indexOf(id), 1);
                }
                if (ids.length == 1) {
                    $('#edit').show();
                    $('#delete-rows').show();
                }else if(ids.length > 1){
                    $('#edit').hide();
                    $('#delete-rows').show();
                }else {
                    $('#edit').hide();
                    $('#delete-rows').hide();
                }
            }
        });
        $("#edit").on('click', function (e) {
            var data_id = ids[0];
            if (data_id.length === 0) {
                swalError("{{__('Please_select_an_item')}}");
                return false;
            } else {
                window.location = '<?php echo URL::to('admin-user-create');?>/'+data_id;
            }
        });
    </script>
@endsection
