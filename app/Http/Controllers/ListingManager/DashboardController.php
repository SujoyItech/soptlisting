<?php

namespace App\Http\Controllers\ListingManager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index(){
        $data = [];
        $directory = DB::table('spotlist_directory')->where('status','Active')->get();
        if (isset($directory) && !empty($directory[0])) {
            $data['listing'] = DB::table('spotlist_directory')
                                 ->select(DB::raw("COUNT(spotlist_directory.sys_users_id) as total_directory"))
                                 ->where(['spotlist_directory.sys_users_id' => Auth::user()->id, 'spotlist_directory.status' => 'Active'])
                                 ->first();

            $data['wishing_list'] = DB::table('spotlist_directory_wishinglist')
                                      ->leftJoin('spotlist_directory', 'spotlist_directory_wishinglist.spotlist_directory_id', '=', 'spotlist_directory.spotlist_directory_id')
                                      ->select(DB::raw("COUNT(spotlist_directory_wishinglist.id) as total_wishing_list"))
                                      ->where(['spotlist_directory.sys_users_id' => Auth::user()->id])
                                      ->first();

            $data['total_reviews'] = DB::table('spotlist_directory_reviews')
                                       ->leftJoin('spotlist_directory', 'spotlist_directory_reviews.spotlist_directory_id', '=', 'spotlist_directory.spotlist_directory_id')
                                       ->select(DB::raw("COUNT(spotlist_directory_reviews.id) as total_reviews"))
                                       ->where(['spotlist_directory.sys_users_id' => Auth::user()->id])
                                       ->first();

            $data['booking_request'] = DB::table('spotlist_booking_request')
                                        ->select(DB::raw("COUNT(spotlist_booking_request.spotlist_booking_request_id) as total_booking_request"))
                                        ->leftJoin('spotlist_directory','spotlist_booking_request.spotlist_directory_id','=','spotlist_directory.spotlist_directory_id')
                                        ->leftJoin('sys_users','spotlist_directory.sys_users_id','=','sys_users.id')
                                        ->groupBy('sys_users.id')
                                        ->where(['sys_users.id' => Auth::user()->id])
                                        ->first();


        }
        return view('ListingManager.dashboard',$data);
    }

    public function profile(){
        return view('ListingManager.profile');
    }
}
