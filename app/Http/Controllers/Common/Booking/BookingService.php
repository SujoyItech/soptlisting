<?php


namespace App\Http\Controllers\Common\Booking;


use App\Http\Service\MailService;
use App\Http\Service\NotificationService;
use App\Models\Notification;
use App\Traits\DataSaveTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookingService {
    use DataSaveTrait;
    public function getBookingRequestData($type,$booking_id){

        if ($type == 'all'){
            return  $this->getALLBookingData($booking_id);
        } if ($type == 'hotel'){
            return  $this->getHotelBookingData('',$booking_id);
        }else if ($type == 'restaurant'){
            return  $this->getRestaurantBookingData('',$booking_id);
        }else if ($type == 'beauty'){
            return $this->getBeautyBookingData('',$booking_id);
        }else if ($type == 'shopping'){
           return $this->getShopBookingData('',$booking_id);
        }else if ($type == 'fitness'){
           return $this->getFitnessBookingData('',$booking_id);
        }
    }

    private function getAllBookingData($booking_id){
        $data = $this->getHotelBookingData('',$booking_id)
                     ->merge($this->getRestaurantBookingData('',$booking_id))
                     ->merge($this->getBeautyBookingData('',$booking_id))
                     ->merge($this->getFitnessBookingData('',$booking_id))
                     ->merge($this->getShopBookingData('',$booking_id));
        return $data;
    }

    public function getAllBookingRequestData(){
        $data = $this->getHotelBookingData('all')
                     ->merge($this->getRestaurantBookingData('all'))
                     ->merge($this->getBeautyBookingData('all'))
                     ->merge($this->getFitnessBookingData('all'))
                     ->merge($this->getShopBookingData('all'));
        return $data;
    }

    private function getHotelBookingData($type='',$booking_id = ''){

        $query = DB::table('spotlist_booking_request')
                 ->join('spotlist_directory','spotlist_booking_request.spotlist_directory_id','=','spotlist_directory.spotlist_directory_id')
                 ->join('sys_users','spotlist_booking_request.customer_id','sys_users.id')
                 ->join('spotlist_directory_type_hotel_room','spotlist_booking_request.spotlist_directory_type_hotel_room_id','=','spotlist_directory_type_hotel_room.spotlist_directory_type_hotel_room_id')
                 ->select('spotlist_booking_request.*','spotlist_directory.spotlist_directory_name',
                     'spotlist_directory_type_hotel_room.room_name','spotlist_directory_type_hotel_room.room_price','sys_users.name','sys_users.email')
                 ->where('spotlist_booking_request.reservation_for','hotel');
        if (!empty($type)){
            $query = $query->whereIn('spotlist_booking_request.spotlist_directory_id',$this->directoryListByUser());
        }
        if (!empty($booking_id)){
            $query = $query->where('spotlist_booking_request.spotlist_booking_request_id',$booking_id);
        }

        $query = $query->orderBy('spotlist_booking_request.updated_at','desc')
                 ->get();
        return $query;
    }

    private function getRestaurantBookingData($type = '',$booking_id = ''){

        $query =  DB::table('spotlist_booking_request')
                 ->join('spotlist_directory','spotlist_booking_request.spotlist_directory_id','spotlist_directory.spotlist_directory_id')
                 ->join('sys_users','spotlist_booking_request.customer_id','sys_users.id')
                 ->select('spotlist_booking_request.*', 'spotlist_directory.spotlist_directory_name','sys_users.name','sys_users.email')
                 ->where('spotlist_booking_request.reservation_for','restaurant');
        if (!empty($type)){
            $query = $query->whereIn('spotlist_booking_request.spotlist_directory_id',$this->directoryListByUser());
        }
        if (!empty($booking_id)){
            $query = $query->where('spotlist_booking_request.spotlist_booking_request_id',$booking_id);
        }

        $query = $query->orderBy('spotlist_booking_request.updated_at','desc')
                ->get();
        return $query;
    }
    private function getBeautyBookingData($type = '',$booking_id = ''){
       $query =  DB::table('spotlist_booking_request')
          ->join('spotlist_directory','spotlist_booking_request.spotlist_directory_id','spotlist_directory.spotlist_directory_id')
          ->join('sys_users','spotlist_booking_request.customer_id','sys_users.id')
          ->join('spotlist_directory_type_beauty','spotlist_booking_request.spotlist_directory_type_beauty_id','spotlist_directory_type_beauty.spotlist_directory_type_beauty_id')
          ->select('spotlist_booking_request.*','spotlist_directory.spotlist_directory_name',
              'spotlist_directory_type_beauty.service_name','spotlist_directory_type_beauty.service_price','spotlist_directory_type_beauty.service_duration',
              'spotlist_directory_type_beauty.service_start_time','spotlist_directory_type_beauty.service_end_time','sys_users.name','sys_users.email')
          ->where('spotlist_booking_request.reservation_for','beauty');
           if(!empty($type)){
               $query = $query->whereIn('spotlist_booking_request.spotlist_directory_id',$this->directoryListByUser());
           }
           if (!empty($booking_id)){
                $query = $query->where('spotlist_booking_request.spotlist_booking_request_id',$booking_id);
           }
           $query = $query->orderBy('spotlist_booking_request.updated_at','desc')->get();
           return $query;
    }
    private function getShopBookingData($type = '',$booking_id = ''){
        $query = DB::table('spotlist_booking_request')
          ->join('spotlist_directory','spotlist_booking_request.spotlist_directory_id','spotlist_directory.spotlist_directory_id')
          ->join('sys_users','spotlist_booking_request.customer_id','sys_users.id')
          ->select('spotlist_booking_request.*',
              'spotlist_directory.spotlist_directory_name','sys_users.name','sys_users.email')
          ->where('spotlist_booking_request.reservation_for','shop');
        if (!empty($type)){
            $query= $query->whereIn('spotlist_booking_request.spotlist_directory_id',$this->directoryListByUser());
        }
        if (!empty($booking_id)){
            $query = $query->where('spotlist_booking_request.spotlist_booking_request_id',$booking_id);
        }

        $query= $query->orderBy('spotlist_booking_request.updated_at','desc')
          ->get();
        return $query;
    }
    private function getFitnessBookingData($type = '',$booking_id = ''){
        $query =  DB::table('spotlist_booking_request')
                 ->join('spotlist_directory','spotlist_booking_request.spotlist_directory_id','spotlist_directory.spotlist_directory_id')
                 ->join('sys_users','spotlist_booking_request.customer_id','sys_users.id')
                 ->select('spotlist_booking_request.*', 'spotlist_directory.spotlist_directory_name','sys_users.name','sys_users.email')
                 ->where('spotlist_booking_request.reservation_for','fitness');
        if (!empty($type)){
            $query = $query->whereIn('spotlist_booking_request.spotlist_directory_id',$this->directoryListByUser());
        }
        if (!empty($booking_id)){
            $query = $query->where('spotlist_booking_request.spotlist_booking_request_id',$booking_id);
        }
        $query = $query->orderBy('spotlist_booking_request.updated_at','desc')
                 ->get();
        return $query;
    }

    private function directoryListByUser(){
        return DB::table('spotlist_directory')->where('sys_users_id',Auth::user()->id)->pluck('spotlist_directory_id')->toArray();
    }

    public function bookingStatusChange(Request $request){
        $success_message = __('Booking status changed successfully.');
        $warning_message = __('Booking status change failed!');
        $notify_message = [];
        if ($request->type =='decline'){
            $update_data = ['status'=>'Declined','status_message'=>$request->message,'updated_at'=>Carbon::now()];
            $message = $this->messageColumnUpdate($request);
            if (!empty($message)){
                $update_data['message'] = $message;
            }
            $response = $this->updateTable('spotlist_booking_request',$update_data,'spotlist_booking_request_id',$request->id,$success_message,$warning_message);
            $notify_message = [
                'id' => $request->id,
                'title' => __('Booking request declined.'),
                'body' => __('Sorry! your booking request has been declined. Please contact with admin.'),
                'message' =>  $request->message
            ];

        }else if ($request->type =='expire'){
            $update_data = ['status'=>'Expired','status_message'=>$request->message,'updated_at'=>Carbon::now()];
            $message = $this->messageColumnUpdate($request);
            if (!empty($message)){
                $update_data['message'] = $message;
            }
            $response = $this->updateTable('spotlist_booking_request',$update_data,'spotlist_booking_request_id',$request->id,$success_message,$warning_message);
            $notify_message =[
                'id' => $request->id,
                'title' => __('Booking request expired.'),
                'body' => __('Sorry! your booking request has been expired.'),
                'message' =>  $request->message
            ];
        }else if ($request->type =='approve'){

            $update_data = ['status'=>'Approve','status_message'=>$request->message,'updated_at'=>Carbon::now()];
            $message = $this->messageColumnUpdate($request);
            if (!empty($message)){
                $update_data['message'] = $message;
            }
            $response = $this->updateTable('spotlist_booking_request',$update_data,'spotlist_booking_request_id',$request->id,$success_message,$warning_message);
            $notify_message = [
                'id' => $request->id,
                'title' => __('Booking request approved.'),
                'body' => __('Your booking request has been approved.'),
                'message' =>  $request->message
            ];
        }else if ($request->type =='complete'){
            $update_data = ['status'=>'Completed','status_message'=>$request->message,'updated_at'=>Carbon::now()];
            $message = $this->messageColumnUpdate($request);
            if (!empty($message)){
                $update_data['message'] = $message;
            }
            $response = $this->updateTable('spotlist_booking_request',$update_data,'spotlist_booking_request_id',$request->id,$success_message,$warning_message);
            $notify_message = [
                'id' => $request->id,
                'title' => __('Booking completed.'),
                'body' => __('Thanks! your booking has been completed successfully.'),
                'message' =>  $request->message
            ];
        }
        $this->sendNotification($request,$notify_message);
        return $response;
    }

    private function messageColumnUpdate($request){

        if (!empty($request->message)){
            $message = [
                'type'=> 'manager',
                'message' => $request->message,
                'time' => Carbon::now()
            ];
            $booking_request = DB::table('spotlist_booking_request')->where('spotlist_booking_request_id',$request->id)->first();

            if (isset($booking_request->message)){
                $booking_message = json_decode($booking_request->message);

            }else{
                $booking_message = [];
            }
            array_push($booking_message,$message);
            return json_encode($booking_message);
        }
        return [];
    }

    private function sendNotification($request,$notify_message){
        $booking_request = DB::table('spotlist_booking_request')
            ->leftJoin('sys_users','spotlist_booking_request.customer_id','sys_users.id')
            ->leftJoin('spotlist_directory','spotlist_booking_request.spotlist_directory_id','spotlist_directory.spotlist_directory_id')
            ->select('spotlist_booking_request.*','sys_users.username','sys_users.user_image','sys_users.email','spotlist_directory.spotlist_directory_name','spotlist_directory.thumb_image',)
            ->where('spotlist_booking_request_id',$request->id)->first();

        $notifcation_body = [
            'directory_name' => $booking_request->spotlist_directory_name,
            'directory_image' =>  $booking_request->thumb_image,
            'id' => $notify_message['id'],
            'title' => $booking_request->spotlist_directory_name.' '.$notify_message['title'],
            'body' =>  $notify_message['body'],
            'message' =>  $notify_message['message']
        ];
         $mail_body = [
                    'name' => $booking_request->username,
                    'body' =>  $notify_message['body'],
                    'message' =>  $notify_message['message']
                ];

        $notification_data = [
            'user_id' => $booking_request->customer_id,
            'body'=>json_encode($notifcation_body),
            'type' => USER_BOOKING,
            'status' => STATUS_PENDING
        ];

        Notification::create($notification_data);
        $channel = 'user_'.$booking_request->customer_id;
        NotificationService::triggerNotification($channel,'user_notification',$notify_message['title'],$notify_message['body']);
        MailService::sendMessageMail($booking_request->email,$notify_message['title'],$mail_body);

    }
}
