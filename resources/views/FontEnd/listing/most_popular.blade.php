@extends('FontEnd.master',['menu'=>'listing'])
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Popular list')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- .articles-area-start -->
    <div class="articles-area blog-area section-padding">
        @if(isset($most_popular_deals) && !empty($most_popular_deals))
        <div class="container">
            <div class="section-title text-center">
                <h2>{{__('Popular list')}}</h2>
            </div>
            <div class="row">
                @foreach($most_popular_deals as $deals)
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="product-item">
                            <a href="{{route('user.listing').'/'.$deals->spotlist_directory_slug}}">
                                <div class="product-img">
                                    <img src="{{getImage($deals->thumb_image,'directory')}}" alt="" height="200">
                                </div>
                            </a>
                            <div class="product-content">
                                <h5><a href="{{route('user.listing').'/'.$deals->spotlist_directory_slug}}">{{$deals->spotlist_directory_name}}</a></h5>
                                <span>{{$deals->city_name.' , '.$deals->country_name}}</span>
                                <div class="thumb-item">
                                    <ul class="list-rating">
                                        @for($x=0; $x<5 ; $x++)
                                            @if(floor($deals->rating)- $x >= 1 )
                                                <li><i class="fa fa-star"></i></li>
                                            @elseif($deals->rating - $x > 0)
                                                <li><i class="fa fa-star-half-o"></i></li>
                                            @else
                                                <li><i class="fa fa-star-o"></i></li>
                                            @endif
                                        @endfor
                                    </ul>
                                    <span>({{isset($deals->rating) ? number_format($deals->rating,1) : '0'}})</span>
                                </div>
                                <p>{{$deals->address}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @else
            <div class="text-center no-data-image">
                <img src="{{asset('FontEnd/assets/images/error/nodata.svg')}}">
            </div>

        @endif
    </div>
    <!-- .articles-area-start -->
    <!-- listing-area start -->
    @include('FontEnd.pages.user_email_subscription')
@endsection
