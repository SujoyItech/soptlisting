<?php

namespace App\Http\Controllers\FontEnd\Notification;

use App\Http\Controllers\Controller;
use App\Http\Service\NotificationService;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function loadUserNotification(){
        $data['notifications'] = Notification::where(['user_id'=>Auth::user()->id])->where('is_cleared',STATUS_PENDING)->orderBy('created_at','desc')->limit(10)->get();
        return view('FontEnd.includes.notification_body',$data);
    }

    public function userNotificationMakeSeen(Request $request){
        $notify_service = new NotificationService();
        $response = $notify_service->notificationMakeSeen($request);
        return response()->json($response);
    }

    public function clearAllUserNotification(){
        $response = $this->clearAllNotification();
        if ($response['success'] == TRUE){
            return redirect()->back()->with(['success'=>$response['message']]);
        }else{
            return redirect()->back()->with(['dismiss'=>$response['message']]);
        }

    }

    public function allUserNotification($notify_id=''){
        if (!empty($notify_id)){
            $notifications = Notification::where(['id'=>$notify_id])->paginate(15);
        }else{
            $notifications = Notification::where(['user_id'=>Auth::user()->id])->orderBy('created_at','desc')->paginate(15);
        }
        $data['notifications'] = $notifications;
        return view('FontEnd.profile.all_notification',$data);
    }

    private function clearAllNotification(){
        try {
            Notification::where(['user_id'=>Auth::user()->id])->update(['is_cleared'=>STATUS_SUCCESS,'status'=>STATUS_SUCCESS]);
            $response['success'] = TRUE;
            $response['message'] = __('All notifications are cleared.');
        }catch (\Exception $exception){
            $response['success'] = FALSE;
            $response['message'] = __('Something went wrong.');
        }
        return $response;
    }
}
