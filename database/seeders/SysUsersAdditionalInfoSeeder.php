<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SysUsersAdditionalInfoSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sys_users_additional_info')->delete();
        
        \DB::table('sys_users_additional_info')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sys_users_id' => 4,
                'country' => NULL,
                'language' => NULL,
                'timezone' => NULL,
                'website' => NULL,
                'facebook_link' => NULL,
                'twitter_link' => NULL,
                'linkedin_link' => NULL,
                'instagram_link' => NULL,
                'about' => 'sdfdsf',
            ),
        ));

        
    }
}