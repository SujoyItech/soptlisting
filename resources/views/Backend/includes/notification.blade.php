<a class="nav-link dropdown-toggle waves-effect waves-light notify-expand" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
    <i class="fe-bell noti-icon"></i>
    <span class="badge badge-danger rounded-circle noti-icon-badge" id="count_notification">{{countAdminUnreadNotification(\Illuminate\Support\Facades\Auth::user()->id)}}</span>
</a>
<div class="dropdown-menu dropdown-menu-right dropdown-lg">
    <!-- item-->
    <div class="dropdown-item noti-title">
        <h5 class="m-0">
            <span class="float-right">
                <a href="{{route('clear-all-admin-notification')}}" class="text-dark">
                    <small>{{__('Clear All')}}</small>
                </a>
            </span>{{__('Notification')}}
        </h5>
    </div>
    <div class="noti-scroll notification-content overflow-auto">

    </div>
    <!-- All-->
    <a href="{{url('view-all-admin-notification')}}" class="dropdown-item text-center text-primary">
        {{__('View all')}}
        <i class="fe-arrow-right">

        </i>
    </a>
</div>
<script src="https://js.pusher.com/3.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/laravel-echo/1.8.1/echo.iife.min.js"></script>
<script>
    Pusher.logToConsole = true;
    window.Echo = new Echo({
        broadcaster: 'pusher',
        wsHost: window.location.hostname,
        wsPort: 6007,
        wssPort: 443,
        key: 'test',
        cluster: 'mt1',
        encrypted: false,
        disableStats: true
    });
</script>

<script>

    Pusher.logToConsole = true;
    var channel_name = 'user_'+{{\Illuminate\Support\Facades\Auth::user()->id}};
    Echo.channel(channel_name).listen('.user_notification', (response) => {
        notifyIconUpdate();
        makeToast(response.body, response.title);
    });

    function makeToast(title, body) {
        toastr.success(title, body);
        toastr.options.timeOut = 1500;
        toastr.options.fadeOut = 1500;
        // window.location.reload();
    }

    function notifyIconUpdate() {
        var count = $('#count_notification').html() == '' ? 0 : $('#count_notification').html();
        var new_count = parseInt(count) + 1;
        $('#count_notification').html(new_count);
        $('#count_notification').show();
        $('#count_notification').addClass('notifying');
        setTimeout(function () {
            $('#count_notification').removeClass('notifying')
        }, 1000);
    }

    $(document).on("click", ".notify-expand", function (event) {
        event.preventDefault();
        $( ".notification-content" ).load("{{route('load-admin-notification-body')}}");
    });
    $(document).on("click", ".notify-item", function (event) {
        event.preventDefault();
        var data = {
            id : $(this).data('id')
        }
        var url = "{{url('admin-notification-make-seen')}}";
        var this_directory = $(this);
        makeAjaxPost(data,url,null).done(function (response){
            if (response.success == true){
                var redirect_url = $(this_directory).attr('href');
                window.location = redirect_url;
            }

        });
    });


</script>
