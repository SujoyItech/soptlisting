<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistBookingRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_booking_request', function (Blueprint $table) {
            $table->bigIncrements('spotlist_booking_request_id');
            $table->integer('spotlist_directory_id');
            $table->unsignedBigInteger('customer_id');
            $table->enum('reservation_for',['hotel','restaurant','shop','beauty','fitness']);
            $table->string('reservation_contact')->nullable();
            $table->integer('adult_guest')->nullable();
            $table->integer('child_guest')->nullable();
            $table->string('request_time')->nullable();
            $table->string('reservation_start_time')->nullable();
            $table->string('reservation_end_time')->nullable();
            $table->text('reservation_note')->nullable();
            $table->text('status_message')->nullable();
            $table->longText('message')->nullable();
            $table->integer('spotlist_directory_type_hotel_room_id')->nullable();
            $table->integer('spotlist_directory_type_restaurant_id')->nullable();
            $table->integer('spotlist_directory_type_beauty_id')->nullable();
            $table->integer('spotlist_directory_type_fitness_id')->nullable();
            $table->integer('spotlist_directory_type_shop_id')->nullable();
            $table->enum('status',['Requested','Confirmed','Approve','Declined','Expired','Completed','Canceled'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_booking_request');
    }
}
