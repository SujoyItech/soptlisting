<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistBlogsSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_blogs')->delete();
        
        \DB::table('spotlist_blogs')->insert(array (
            0 => 
            array (
                'spotlist_blogs_id' => 1,
                'sys_users_id' => NULL,
                'spotlist_blogs_title' => 'What to Do in the Berkshires ?',
                'slug' => '',
                'spotlist_category_id' => 1,
                'contents' => '<p style="margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 18px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size',
                'blog_thumb_image' => 'Backend/images/blogs/LINSoCfBQNo2a8bc.jpg',
                'blog_cover_image' => 'Backend/images/blogs/Hn3trnBsz2CK2gAQ.jpg',
                'is_featured' => 1,
                'status' => 'Active',
                'created_at' => '2020-10-13 06:12:21',
                'updated_at' => '2020-10-13 06:18:58',
            ),
        ));

        
    }
}