<div class="tab-pane" id="features">
    <form action="{{url('directory-features-save')}}" class="parsley-examples" method="POST" id="submit_features_location" enctype="multipart/form-data">
        <div class="row mb-4">
            @if(isset($features) && !empty($features))
                @foreach($features as $feature)
                    <div class="col-md-3 col-sm-4 col-12">

                        <div class="checkbox checkbox-primary mb-2">

                            <input id="{{$feature->spotlist_features_name}}" type="checkbox" name="spotlist_features_id[]" value="{{$feature->spotlist_features_id}}" {{is_checked($feature->spotlist_features_id,$directory_features ?? [])}}>
                            <label for="{{$feature->spotlist_features_name}}">
                                <img src="{{asset($feature->spotlist_features_icon)}}" width="32" height="32" class="rounded-circle">
                                {{$feature->spotlist_features_name}}
                            </label>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <ul class="list-inline mb-0 wizard">
            <li class="previous list-inline-item">
                <a href="javascript: void(0);" class="btn btn-sm btn-secondary">{{__('Previous')}}</a>
            </li>
            <li class="next list-inline-item float-right">
                <button type="submit" class="btn btn-sm btn-success submit_info btn-next">{{__('Save & Next')}}</button>
            </li>
        </ul>
    </form>

</div>
