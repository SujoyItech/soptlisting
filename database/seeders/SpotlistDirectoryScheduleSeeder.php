<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SpotlistDirectoryScheduleSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('spotlist_directory_schedule')->delete();
        
        \DB::table('spotlist_directory_schedule')->insert(array (
            0 => 
            array (
                'spotlist_directory_schedule_id' => 1,
                'spotlist_directory_id' => 1,
                'monday' => '12 AM-02 PM',
                'tuesday' => '12 AM-10 AM',
                'wednesday' => '12 AM-06 PM',
                'thursday' => '12 AM-03 PM',
                'friday' => '09 AM-08 PM',
                'saturday' => '09 AM-08 PM',
                'sunday' => 'Closed-Closed',
                'status' => 'Active',
            ),
            1 => 
            array (
                'spotlist_directory_schedule_id' => 2,
                'spotlist_directory_id' => 2,
                'monday' => '05 AM-05 PM',
                'tuesday' => '07 AM-05 PM',
                'wednesday' => '05 AM-03 PM',
                'thursday' => '06 AM-07 PM',
                'friday' => '09 AM-06 PM',
                'saturday' => '06 AM-07 PM',
                'sunday' => 'Closed-Closed',
                'status' => 'Active',
            ),
        ));

        
    }
}