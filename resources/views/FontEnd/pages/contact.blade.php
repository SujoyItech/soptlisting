@extends('FontEnd.master',['menu'=>'contact'])
@section('style')
    <style>
        .error{
            color: red;
        }
    </style>
@endsection
@php($settings = __options(['admin_settings','frontend_settings']))
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>{{__('Contact Us')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->
    <!-- .contact-area-start -->
    <div class="contact-map con-map" id="show_contact_map">
    </div>
    <div class="contact-area section-padding">
        <div class="container">
            <div class="contact-form-area">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="contact-info">
                            <h2>{{__('Do You Need Any Help? Contact With Us')}}</h2>
                            <p>{{__('Customer Success agents has a sole goal of Making You Successful! That is why our dedicated support team works.')}}</p>
                            <ul>
                                <li>{{__('Address')}} <span>{{$settings->contact_address ?? ''}}</span></li>
                                <li>{{__('Telephone')}} <span>{{$settings->contact_number ?? ''}}</span></li>
                                <li>{{__('E-mail')}} <span>{{$settings->contact_email ?? ''}}</span></li>
                                <li>{{__('Connect With Us')}}<span>
                                        <ul class="profile-social">
                                            <li><a href="{{$settings->facebook_link}}" target="_blank"><img src="{{asset('FontEnd')}}/assets/images/listing/icon/16.svg" alt=""></a></li>
                                            <li><a href="{{$settings->twitter_link}}" target="_blank"><img src="{{asset('FontEnd')}}/assets/images/listing/icon/17.svg" alt=""></a></li>
                                            <li><a href="{{$settings->instagram_link}}" target="_blank"><img src="{{asset('FontEnd')}}/assets/images/listing/icon/18.svg" alt=""></a></li>
                                        </ul>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="contact-content">
                            <h2>{{__('Send a Message')}}</h2>
                            <div class="contact-form">
                                <form method="post" action="{{route('user.message.send')}}" class="parsley-examples">
                                    @csrf
                                    <div class="half-col">
                                        <input type="text" name="fname" id="fname" class="form-control" placeholder="First Name*" required>
                                    </div>
                                    <div class="half-col">
                                        <input type="text" name="lname" id="lname" class="form-control" placeholder="Last Name*" required>
                                    </div>
                                    <div class="half-col">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email Address*" required>
                                    </div>
                                    <div class="half-col">
                                        <input type="text" name="subject" id="subject" class="form-control" placeholder="Subjects" required>
                                    </div>
                                    <div>
                                        <textarea class="form-control" name="message" id="message" placeholder="Write Your Message" required></textarea>
                                    </div>
                                    <div class="submit-btn">
                                        <input type="hidden" name="user_id" value="{{$user->id ?? ''}}">
                                        <button type="submit" class="btn theme-btn">{{__('SUBMIT NOW')}}</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('FontEnd.pages.user_email_subscription')
@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>
        function initMap() {
            var local_lat =  parseFloat("{{$settings->latitude ?? ''}}");
            var local_lon =  parseFloat("{{$settings->longitude ?? ''}}");
            var myLatlng = {lat: local_lat, lng: local_lon};
            var map = new google.maps.Map(
                document.getElementById('show_contact_map'), {zoom: 15, center: myLatlng});

            var marker = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: {lat: local_lat, lng: local_lon }
            });

        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{$settings->map_key ?? 'AIzaSyBj0o3QDEGBtW3S8AEq0CRlpZdpWxNZckA'}}&libraries=geometry,places&callback=initMap"></script>
@endsection
