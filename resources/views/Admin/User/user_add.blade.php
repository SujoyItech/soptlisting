@extends('Backend.layouts.app',['menu'=>'user','sub_menu'=>'user_add'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-user-create')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New User')}}</a>
                    <a class="btn btn-primary btn-xs text-white" id="" href="{{url('admin-users-list')}}"><i class="fa fa-list" aria-hidden="true"></i> {{__('User list')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('Add new user')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-box">
        <form action="{{url('admin-user-information-save')}}" class="parsley-examples" method="POST" id="submit_user" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="name">{{__('Type')}}<span class="text-danger">*</span></label>
                                <select class="form-control" name="default_module_id">
                                    <option value="{{SYSTEM_USER}}" {{is_selected(SYSTEM_USER,$user->default_module_id ?? '')}}>{{__('User')}}</option>
                                    <option value="{{LISTING_MANAGER}}" {{is_selected(LISTING_MANAGER,$user->default_module_id ?? '')}}>{{__('Listing Manager')}}</option>
                                    <option value="{{ADMIN}}" {{is_selected(ADMIN,$user->default_module_id ?? '')}}>{{__('Admin')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="name">{{__('Name')}}<span class="text-danger">*</span></label>
                                <input type="text" name="name" parsley-trigger="change" required value="{{$user->name ?? ''}}"
                                       placeholder="Name" class="form-control" id="name">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="email">{{__('Email')}}<span class="text-danger">*</span></label>
                                <input type="email" name="email" parsley-trigger="change" required value="{{$user->email ?? ''}}" {{isset($user->id) ? 'readonly': ''}}
                                       placeholder="Email" class="form-control" id="email">
                                <span class="text-danger d-none" id="email_error"></span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="mobile">{{__('Phone')}}</label>
                                <input type="text" name="mobile" parsley-trigger="change" value="{{$user->mobile ?? ''}}"
                                       placeholder="Phone" class="form-control" id="mobile">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="col-form-label" for="user_image">{{__('User Image')}}</label>
                        <input type="file" name="user_image" data-plugins="dropify"
                               data-default-file="{{isset($user->user_image) ? getImageUrl($user->user_image) : ''}}"
                               data-allowed-file-extensions="png jpg jpeg jfif"
                               data-max-file-size="1M" />
                        <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 1mb')}}</p>
                    </div>
                </div>

                <div class="col-lg-6 col-12">
                    <div class="form-group">
                        <label for="website">{{__('Website')}}</label>
                        <input type="text" name="website" parsley-trigger="change" value="{{$user->website ?? ''}}"
                               placeholder="Website" class="form-control" id="website">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group">
                        <label for="facebook_link">{{__('Facebook link')}}</label>
                        <input type="text" name="facebook_link" parsley-trigger="change" value="{{$user->facebook_link ?? ''}}"
                               placeholder="Write down facebook url" class="form-control" id="facebook_link">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group">
                        <label for="twitter_link">{{__('Twitter link')}}</label>
                        <input type="text" name="twitter_link" parsley-trigger="change" value="{{$user->twitter_link ?? ''}}"
                               placeholder="Write down twitter link" class="form-control" id="twitter_link">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group">
                        <label for="linkedin_link">{{__('Linkedin link')}}</label>
                        <input type="text" name="linkedin_link" parsley-trigger="change" value="{{$user->linkedin_link ?? ''}}"
                               placeholder="Write down linked in link" class="form-control" id="linkedin_link">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="website">{{__('Address')}}</label>
                        <textarea id="address" class="form-control" name="address" data-parsley-trigger="keyup" data-parsley-validation-threshold="10">{{$user->address ?? ''}}</textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="about">{{__('About')}}</label>
                        <textarea id="about" class="form-control" name="about" data-parsley-trigger="keyup" data-parsley-validation-threshold="10">{{$user->about ?? ''}}</textarea>
                    </div>
                </div>
                @if(isset($user) && !empty($user))
                @else
                    <div class="col-lg-6 col-12">
                        <div class="form-group">
                            <label for="pass1">{{__('New Password')}}<span class="text-danger">*</span></label>
                            <input id="pass1" type="password" minlength="6" name="password" placeholder="Password" required
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="form-group">
                            <label for="passWord2">{{__('Confirm Password')}} <span class="text-danger">*</span></label>
                            <input data-parsley-equalto="#pass1" minlength="6" type="password" required name="confirm_password"
                                   placeholder="Password" class="form-control" id="passWord2">
                        </div>
                    </div>
                @endif
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                            <i class="fa fa-save"></i>
                            {{__('Save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
        <input type="hidden" name="sys_users_id" value="{{$user->id ?? ''}}" id="sys_users_id">
        @if(isset($user) && !empty($user))
            @include('Admin.User.user_change_password')
        @endif
    </div>

@endsection
@section('script')
    <script src="{{asset('Backend/')}}/libs/parsleyjs/parsley.min.js"></script>
    <script src="{{asset('Backend/')}}/js/pages/form-validation.init.js"></script>
    <script>

        $('.submit_button').on('click', function (e) {
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $('#email_error').addClass('d-none');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    if ($('#sys_users_id').val().length !== 0){
                        formData.append('sys_users_id',$('#sys_users_id').val());
                    }
                    makeAjaxPostUser(formData, submit_url).done(function (response) {
                        if (response.success == true){
                            var redirect_url = "{{url('admin-users-list')}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'error');
                        }
                    });
                }
            });
        });
        function makeAjaxPostUser(data, url) {
            return $.ajax({
                url: url,
                type: 'post',
                headers: { 'X-CSRF-Token' : $('meta[name=laraframe]').attr('content') },
                data: data,
                cache: false,
                processData: false,
                contentType: false,
            }).always(function() {

            }).fail(function(err) {
                var allerror = Object.values(err.responseJSON);
                $('#email_error').html(allerror[1].email[0]);
                $('#email_error').removeClass('d-none');
            })
        }
    </script>
@endsection
