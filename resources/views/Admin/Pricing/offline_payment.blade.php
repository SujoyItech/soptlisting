@extends('Backend.layouts.app',['menu'=>'offline_payment'])
@section('style')
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('Offline payment')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <form action="{{url('admin-user-offline-payment-save')}}" class="parsley-examples" method="POST" id="submit_payment" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="col-form-label" for="sys_users_id">{{__('Listing manager')}} <span class="text-danger">*</span></label>
                                <select class="selectpicker" data-style="btn-light" data-live-search="true"  name="sys_users_id" required data-size="5">
                                    <option value="">{{__('Select user')}}</option>
                                    @if(isset($users) && !empty($users))
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="col-form-label" for="spotlist_packages_id">{{__('Choose package')}} <span class="text-danger">*</span></label>
                                <select class="selectpicker" data-style="btn-light" data-live-search="true" name="spotlist_packages_id" required>
                                    <option>{{__('Select package')}}</option>
                                    @if(isset($packages) && !empty($packages))
                                        @foreach($packages as $package)
                                            <option value="{{$package->spotlist_packages_id}}">{{$package->spotlist_packages_name}} ( {{$package->price }} {{get_currency()}}) </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="payment_method">{{__('Payment method')}}</label>
                                <input type="text" name="payment_method" parsley-trigger="change" value=""
                                       placeholder="Payment method" class="form-control" id="payment_method">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-rounded btn-primary waves-effect waves-light submit_button" type="submit">
                                    <i class="fa fa-save"></i>
                                    {{__('Save')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clear-fix"></div>

@endsection
@section('script')
    <script>
        $('.submit_button').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        if (response.success == true){
                            var redirect_url = "{{url('admin-payment-history')}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'error');
                        }
                    });
                }
            });
        });
    </script>
@endsection
