@extends('FontEnd.master')
@section('content')
    <!-- .breadcumb-area start -->
    <div class="breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>Pricing</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .breadcumb-area end -->

    <!-- pricing-area start -->

    <div class="pricing-area section-padding">
        <div class="container">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2>{{__('The Best Pricing We Offered')}}</h2>
                </div>
            </div>
            <div class="row">
                @if(isset($packages) && !empty($packages))
                    @foreach($packages as $package)
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="pricing-item">
                                <div class="price-top">
                                    <h3>{{$package->spotlist_packages_name}}</h3>
                                </div>
                                <div class="price-bottom">
                                    <h2>{{$package->price}} ({{get_currency()}})</h2>
                                    <span>{{__('(Per Month)')}}</span>
                                    <ul>
                                        <li>{{$package->num_of_listing }} {{__('Listing')}}</li>
                                        <li>{{$package->day_validity}} {{__('Days Availability')}}</li>
                                        <li>{{$package->is_featured == 1 ? 'Featured' : 'Non-Featured'}}</li>
                                        <li>{{$package->num_of_listing}} {{__('Number of Listing')}}</li>
                                        <li>{{$package->num_of_categories}} {{__('Number of Category')}}</li>
                                        <li>{{$package->num_of_photos}} {{__('Photos Per Listing')}}</li>
                                        <li>{{$package->num_of_tags}} {{__('Tags Per Listing')}}</li>
                                        <li>{{$package->add_video_ability == 1 ? 'Add video enabled' : 'Add video disabled'}}</li>
                                        <li>{{$package->is_recomended == 1 ?'Recommended' : 'Not recommended'}}</li>
                                    </ul>
                                </div>
                                <a class="theme-btn p-btn" href="#">Choose Plan</a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <!-- pricing-area end -->

    <!-- listing-area start -->
   @include('FontEnd.pages.user_email_subscription')
@endsection
