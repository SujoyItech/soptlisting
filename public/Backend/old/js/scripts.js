"use strict";
$(document).ready(function() {
    /*------------ Start site menu  ------------*/
    // Start sticky header
    $(window).on('scroll', function() {
        if ($(window).scrollTop() >= 150) {
            $('#sticky-header').addClass('sticky-menu');
        } else {
            $('#sticky-header').removeClass('sticky-menu');
        }
    });

    // slicknav
    $('ul#navigation').slicknav({
        prependTo: ".responsive-menu-wrap"
    });

    $("#metismenu").metisMenu();


    /* scroll bar */
    $('.scrollbar-macosx').scrollbar();

    $('.sidebar-icon').on('click', function() {
        $('.nimmu-sidebar').toggleClass('nimmu-sidebar-hide');
    });

    $('.sidebar-icon').on('click', function() {
        $(this).toggleClass('sidebar-icon-move');
    });


    $(".nimmu-right-sidebar-show-btn").on('click', function() {
        $('.nimmu-right-sidebar').removeClass('nimmu-right-sidebar-hide');
    });
    $(".nimmu-right-bar-close").on('click', function() {
        $('.nimmu-right-sidebar').addClass('nimmu-right-sidebar-hide');
    });

    $(".nimmu-live-chat-btn").on('click', function() {
        $('.nimmu-chat-bot').toggleClass('nimmu-chat-bot-hide');
    });

    $(".nimmu-collaps-menu").on('click', function() {
        $('.nimmu-sidebar').toggleClass('nimmu-sidebar-collapsed');
        $('.nimmu-wrapper').toggleClass('nimmu-wrapper-expend');
        $('.logo-large').toggleClass('d-none');
        $('.logo-small').toggleClass('d-block');
    });

    $(".nimmu-sidebar").on('mouseenter', function() {
        $(this).removeClass('nimmu-sidebar-collapsed');
        $('.nimmu-wrapper').removeClass('nimmu-wrapper-expend');
        $('.logo-large').removeClass('d-none');
        $('.logo-small').removeClass('d-block');
    });


    $(".nimmu-squared-card-box").on('click', function() {
        $('.nimmu-default-card').toggleClass('nimmu-default-card-square');
    });
    $(".nimmu-box-shadows").on('click', function() {
        $('.nimmu-default-card').toggleClass('nimmu-default-card-shadow');
    });
    $(".nimmu-top-menu").on('click', function() {
        $('.nimmu-sidebar').toggleClass('item-hide');
        $('.nimmu-top-header-primary').toggleClass('item-hide');
        $('.nimmu-header-menu').toggleClass('item-hide');
        $('.nimmu-wrapper').toggleClass('wrapper-expend-full');
    });





    /*
    --------------------------------------
    fullscreen script
    --------------------------------------
    */

    $('.nimmu-fullscreen-btn').on('click', function() {
        var elem = document.querySelector("body");

        function openFullscreen() {
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) {
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            }
        };

        openFullscreen();
    });
});
