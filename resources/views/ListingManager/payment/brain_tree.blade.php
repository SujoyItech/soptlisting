<div class="card-box">
    <div class="text-center">
        <input type="hidden" name="package_id" value="{{$packages->spotlist_packages_id ?? ''}}" id="package_id">
        <input type="hidden" name="price" value="{{$packages->price ?? ''}}" id="price">
        <div id="dropin-container"></div>
        <button id="submit-button" class="btn btn-info btn-sm">{{__('Request payment method')}}</button>
    </div>
</div>
<script src="https://js.braintreegateway.com/web/dropin/1.8.1/js/dropin.min.js"></script>
<script>
    var button = document.querySelector('#submit-button');
    braintree.dropin.create({
        authorization: "{{ Braintree\ClientToken::generate() }}",
        container: '#dropin-container'
    }, function (createErr, instance) {
        $('#submit-button').removeClass('d-none');
        button.addEventListener('click', function () {
            instance.requestPaymentMethod(function (err, payload) {
                var submit_url = "{{route('payment.make')}}";
                var formData = {
                    spotlist_packages_id : $('#package_id').val(),
                    price : $('#price').val(),
                    payload : payload
                };

                makeAjaxPost(formData,submit_url,null).done(function (response){
                    var url ="{{url('listing-manager-payment-history')}}";
                    if (response.success) {
                        swalRedirect(url,response.message,'success')
                    } else {
                    }
                });
            });
        });

    });
</script>
