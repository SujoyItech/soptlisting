<?php

namespace App\Http\Controllers\FontEnd\Listing;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FontEnd\FontEndService;
use App\Http\Requests\FontEnd\BookingRequesRequest;
use App\Traits\DataReturn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ListingController extends Controller
{
    use DataReturn;
    private $service;
    public function __construct(){
        $this->service = new ListingService();
    }

    public function listing($slug = null){
        $data = [];
        $spotlist_directory = DB::table('spotlist_directory')->where('status','Active')->get();
        if (isset($spotlist_directory) && !empty($spotlist_directory[0])){
            if ($slug != null){
                $directory = DB::table('spotlist_directory')->where('spotlist_directory_slug',$slug)->first();
                $id = $directory->spotlist_directory_id ?? '';
            }else{
                $id = NULL;
            }
            $data = $this->service->getList($id);
            $data['all_listing'] = $this->directoryMapInfo($this->service->getDirectoryList(NULL,NULL,NULL,[],['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active','sys_users.status'=>'Active']));
            if ($id != NULL){
                $this->service->visitCount($id);
            }
        }

        return $id == NULL ? view('FontEnd.listing.listing',$data):view('FontEnd.listing.listing_details',$data);
    }

    private function directoryMapInfo($all_listing){
        $listing_array = [];
        foreach ($all_listing as $key => $value){
            $listing_array[$key]['latitude'] = $value->latitude ?? '';
            $listing_array[$key]['longitude'] = $value->longitude ?? '';
            $listing_array[$key]['thumb_image'] = $value->thumb_image ?? '';
            $listing_array[$key]['category_name'] = $value->category_name ?? '';
            $listing_array[$key]['spotlist_directory_slug'] = $value->spotlist_directory_slug ?? '';
            $listing_array[$key]['spotlist_directory_name'] = $value->spotlist_directory_name ?? '';
            $listing_array[$key]['address'] = $value->address ?? '' ;
        }
        return $listing_array;
    }

    public function showAllCategories(){
        $data['categories'] = DB::table('spotlist_category')
                                       ->select('spotlist_category.spotlist_category_id','spotlist_category.spotlist_category_name','spotlist_category.category_image',
                                           DB::raw("COUNT(spotlist_category.spotlist_category_id) as total_listing"))
                                       ->leftJoin('spotlist_directory','spotlist_category.spotlist_category_id','spotlist_directory.spotlist_parent_category_id')
                                       ->leftJoin('sys_users','spotlist_directory.sys_users_id','sys_users.id')
                                       ->where(['spotlist_category.status'=>'Active','spotlist_directory.status'=>'Active','sys_users.status'=>'Active'])
                                       ->groupBy('spotlist_category.spotlist_category_id')
                                       ->get();
        return view('FontEnd.listing.all_categories',$data);
    }

    public function showAllCities(){
        $data['cities'] = DB::table('spotlist_cities')
                                      ->select('spotlist_cities.*',
                                          DB::raw("COUNT(spotlist_cities.spotlist_cities_id) as total_listing"))
                                      ->join('spotlist_directory','spotlist_cities.spotlist_cities_id','spotlist_directory.city')
                                      ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','spotlist_category.spotlist_category_id')
                                      ->leftJoin('sys_users','spotlist_directory.sys_users_id','sys_users.id')
                                      ->where(['spotlist_cities.status'=>'Active','spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','sys_users.status'=>'Active'])
                                      ->groupBy('spotlist_cities.spotlist_cities_id')
                                      ->get();
        return view('FontEnd.listing.all_cities',$data);
    }

    public function showMostPopular(){
        $data['most_popular_deals'] = $this->mostVisitedList();
        return view('FontEnd.listing.most_popular',$data);
    }


    private function mostVisitedList(){
        $most_bookings = DB::table('spotlist_directory')
            ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','=','spotlist_category.spotlist_category_id')
            ->join('spotlist_cities','spotlist_directory.city','=','spotlist_cities.spotlist_cities_id')
            ->join('sys_users','spotlist_directory.sys_users_id','=','sys_users.id')
            ->select('spotlist_directory.spotlist_directory_id')
            ->where(['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active','sys_users.status'=>'Active'])
            ->orderBy('spotlist_directory.booking_count','DESC')
            ->limit(20);
        $most_visitings = DB::table('spotlist_directory')
            ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','=','spotlist_category.spotlist_category_id')
            ->join('spotlist_cities','spotlist_directory.city','=','spotlist_cities.spotlist_cities_id')
            ->join('sys_users','spotlist_directory.sys_users_id','=','sys_users.id')
            ->select('spotlist_directory.spotlist_directory_id')
            ->where(['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active','sys_users.status'=>'Active'])
            ->orderBy('spotlist_directory.visit_count','DESC')
            ->limit(20);
        $most_wishing = DB::table('spotlist_directory')
            ->join('spotlist_category','spotlist_directory.spotlist_parent_category_id','=','spotlist_category.spotlist_category_id')
            ->join('spotlist_cities','spotlist_directory.city','=','spotlist_cities.spotlist_cities_id')
            ->join('sys_users','spotlist_directory.sys_users_id','=','sys_users.id')
            ->select('spotlist_directory.spotlist_directory_id')
            ->where(['spotlist_directory.status'=>'Active','spotlist_category.status'=>'Active','spotlist_cities.status'=>'Active','sys_users.status'=>'Active'])
            ->orderBy('spotlist_directory.wishing_count','DESC')
            ->limit(20);
        $directory_id = $most_bookings->union($most_visitings)->union($most_wishing)->pluck('spotlist_directory_id')->toArray();
        $where_in[0] = 'spotlist_directory.spotlist_directory_id';
        $where_in[1] = $directory_id;
        $listing_service = new ListingService();
        return $listing_service->getDirectoryList(NULL,NULL,NULL,$where_in);
    }

    public function directoryRating(Request $request){
        $response = $this->service->directoryRatingSave($request);
        if ($response['success'] == TRUE){
            return redirect()->back()->with(['success'=>$response['message']]);
        }else{
            return redirect()->back()->with(['dismiss'=>$response['message']]);
        }
    }


    public function addListToWishlist($id){
        $response = $this->service->addListToWishlist($id);
        if ($response['success'] == TRUE){
            return redirect()->back()->with(['success'=>$response['message']]);
        }else{
            return redirect()->back()->with(['dismiss'=>$response['message']]);
        }
    }

    public function listingItems($id = null){
        $data = $this->service->getList($id);
        return view('FontEnd.listing.listing_bookings',$data);
    }

    public function directoryTypeLike(Request $request){
        $response = $this->service->directoryTypeLikeSave($request);
        return response()->json($response);
    }

    public function checkUserRating(Request $request){
        $response = $this->service->getUserRating($request);
        return response()->json($response);
    }

    public function searchDirectory(){
        $data = [];
        $data['search_key']['categories'] = $_GET['categories'] ?? [];
        $data['search_key']['locations'] = $_GET['locations'] ?? [];
        $data['search_key']['features']= $_GET['features'] ?? [];
        $data['search_key']['rating'] = $_GET['rating'] ?? [];
        $data['search_key']['search_text'] = $_GET['search_text'] ?? '';

        $listing_data =  $this->service->showListBySearch($data);
        $data['list'] = $listing_data['search_result'];
        $data['all_listing'] = $this->directoryMapInfo($listing_data['search_result_map']);
        $data['categories'] = $this->basicTableInfo('spotlist_category',[],[],[],['status'=>'Active','parent_id'=>0]);
        $data['locations'] = $this->basicTableInfo('spotlist_cities',[],[],[],['status'=>'Active']);
        $data['features'] = $this->basicTableInfo('spotlist_features',[],[],[],['status'=>'Active']);
        return view('FontEnd.listing.search.search_listing',$data);
    }

    public function searchHomePageDirectory(){
        $city_id = $_GET['location'];
        $category_id = $_GET['category'];
        $where = ['spotlist_directory.status'=>'Active','spotlist_directory.city'=>$city_id,'spotlist_directory.spotlist_parent_category_id'=>$category_id,'sys_users.status'=>'Active'];
        $data['list'] = $this->service->getDirectoryList(NULL,NULL,20,[],$where);
        $data['all_listing'] = $this->directoryMapInfo($this->service->getDirectoryList(NULL,NULL,NULL,[],$where));
        $data['categories'] = $this->basicTableInfo('spotlist_category',[],[],[],['status'=>'Active','parent_id'=>0]);
        $data['locations'] = $this->basicTableInfo('spotlist_cities',[],[],[],['status'=>'Active']);
        $data['features'] = $this->basicTableInfo('spotlist_features',[],[],[],['status'=>'Active']);
        return view('FontEnd.listing.search.search_listing',$data);
    }

    public function showAllListing(){
        $data['list'] = $this->service->showAllListing();
        $data['all_listing'] = $this->service->getDirectoryList();
        $data['categories'] = $this->basicTableInfo('spotlist_category',[],[],[],['status'=>'Active','parent_id'=>0]);
        $data['locations'] = $this->basicTableInfo('spotlist_cities',[],[],[],['status'=>'Active']);
        $data['features'] = $this->basicTableInfo('spotlist_features',[],[],[],['status'=>'Active']);
        return view('FontEnd.listing.search.search_listing',$data);
    }

    public function showListByCategory($category_name){
        $category = DB::table('spotlist_category')->where(['parent_id'=>0,'spotlist_category_name'=>$category_name])->first();
        $category_id = intval($category->spotlist_category_id);
        $data['list'] = $this->service->showListByCategory($category_id);
        $data['all_listing'] = $this->service->getDirectoryList(NULL,NULL,NULL,[],['spotlist_directory.spotlist_parent_category_id'=>$category_id,'spotlist_directory.status'=>'Active']);
        $data['categories'] = $this->basicTableInfo('spotlist_category',[],[],[],['status'=>'Active','parent_id'=>0]);
        $data['locations'] = $this->basicTableInfo('spotlist_cities',[],[],[],['status'=>'Active']);
        $data['features'] = $this->basicTableInfo('spotlist_features',[],[],[],['status'=>'Active']);
        return view('FontEnd.listing.search.search_listing',$data);
    }
    public function showListByPlace($place_slug=NULL){
        $place_id = NULL;
        if ($place_slug !== NULL){
            $place = DB::table('spotlist_cities')->where('spotlist_cities_slug',$place_slug)->first();
            $place_id = intval($place->spotlist_cities_id);
        }
        $data['list'] = $this->service->showListByPlace($place_id);
        $data['all_listing'] = $this->service->getDirectoryList(NULL,NULL,NULL,[],['spotlist_directory.city'=>$place_id,'spotlist_directory.status'=>'Active']);
        $data['categories'] = $this->basicTableInfo('spotlist_category',[],[],[],['status'=>'Active','parent_id'=>0]);
        $data['locations'] = $this->basicTableInfo('spotlist_cities',[],[],[],['status'=>'Active']);
        $data['features'] = $this->basicTableInfo('spotlist_features',[],[],[],['status'=>'Active']);
        return view('FontEnd.listing.search.search_listing',$data);
    }

    public function viewServiceDetails(Request $request){
        $data = [];
        if ($request->type == 'restaurant'){
            $data['restaurant'] = DB::table('spotlist_directory_type_restaurant')->where('spotlist_directory_type_restaurant_id',$request->id)->first();
        }elseif($request->type == 'shop'){
            $data['shop'] = DB::table('spotlist_directory_type_shop')->where('spotlist_directory_type_shop_id',$request->id)->first();
        }if ($request->type == 'hotel'){
            $data['hotel'] = DB::table('spotlist_directory_type_hotel_room')->where('spotlist_directory_type_hotel_room_id',$request->id)->first();
        }if ($request->type == 'beauty'){
            $data['beauty'] = DB::table('spotlist_directory_type_beauty')->where('spotlist_directory_type_beauty_id',$request->id)->first();
        }if ($request->type == 'fitness'){
            $data['fitness'] = DB::table('spotlist_directory_type_fitness')->where('spotlist_directory_type_fitness_id',$request->id)->first();
        }
        return view('FontEnd.listing.services.service_details',$data);
    }
}
