<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommonPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->default_module_id == LISTING_MANAGER || Auth::user()->default_module_id == ADMIN || Auth::user()->default_module_id == SYSTEM_ADMIN){
            return $next($request);
        }else{
            return redirect()->route('no-permission');
        }
    }
}
