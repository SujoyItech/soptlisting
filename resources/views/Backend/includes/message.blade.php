<a class="nav-link dropdown-toggle waves-effect waves-light message-expand" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
    <i class="fe-message-square noti-icon"></i>
    <span class="badge badge-danger rounded-circle noti-icon-badge" id="count_message">{{countUnreadMessage(\Illuminate\Support\Facades\Auth::user()->id)}}</span>
</a>
<div class="dropdown-menu dropdown-menu-right dropdown-lg">
    <!-- item-->
    <div class="dropdown-item noti-title">
        <h5 class="m-0">
            <span class="float-right">
                <a href="{{url('clear-all-admin-message')}}" class="text-dark">
                    <small>{{__('Clear All')}}</small>
                </a>
            </span>{{__('Message')}}
        </h5>
    </div>
    <div class="noti-scroll message-content overflow-auto">

    </div>
    <!-- All-->
    <a href="{{url('view-all-admin-message')}}" class="dropdown-item text-center text-primary">
        {{__('View all')}}
        <i class="fe-arrow-right">

        </i>
    </a>
</div>
<script src="https://js.pusher.com/3.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/laravel-echo/1.8.1/echo.iife.min.js"></script>
<script>
    Pusher.logToConsole = true;
    window.Echo = new Echo({
        broadcaster: 'pusher',
        wsHost: window.location.hostname,
        wsPort: 6007,
        wssPort: 443,
        key: 'test',
        cluster: 'mt1',
        encrypted: false,
        disableStats: true
    });
</script>

<script>

    Pusher.logToConsole = true;
    var message_channel_name = 'user_message_'+{{\Illuminate\Support\Facades\Auth::user()->id}};
    Echo.channel(message_channel_name).listen('.user_message', (response) => {
        messageIconUpdate();
        makeMessageToast(response.body, response.title);
    });

    function makeMessageToast(title, body) {
        toastr.info(title, body);
    }

    function messageIconUpdate() {
        var count = $('#count_message').html() == '' ? 0 : $('#count_message').html();
        var new_count = parseInt(count) + 1;
        $('#count_message').html(new_count);
        $('#count_message').show();
        $('#count_message').addClass('notifying');
        setTimeout(function () {
            $('#count_message').removeClass('notifying')
        }, 1000);
    }

    $(document).on("click", ".message-expand", function (event) {
        event.preventDefault();
        $( ".message-content" ).load("{{route('load-admin-message-body')}}");
    });
    $(document).on("click", ".notify-item", function (event) {
        event.preventDefault();
        var data = {
            id : $(this).data('id')
        }
        var url = "{{url('admin-notification-make-seen')}}";
        var this_directory = $(this);
        makeAjaxPost(data,url,null).done(function (response){
            if (response.success == true){
                var redirect_url = $(this_directory).attr('href');
                window.location = redirect_url;
            }

        });
    });


</script>
