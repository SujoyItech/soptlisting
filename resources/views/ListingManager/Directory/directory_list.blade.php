@extends('Backend.layouts.app',['menu'=>'directory','sub_menu'=>'directory_list'])

@section('style')
    <link rel="stylesheet" href="{{asset('/Backend/vendors/DataTables/css/dataTables.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <a class="btn btn-primary btn-xs text-white" href="{{url('directory-add')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('Add new directory')}}</a>
                </div>
                <h4 class="page-title"><i class="fa fa-arrow-circle-right"></i> {{__('Directory List')}}</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table">
                            <thead>
                            <tr>
                                <th>{{__('Image')}}</th>
                                <th>{{__('Title')}}</th>
                                <th>{{__('Categories')}}</th>
                                <th>{{__('Location')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Action')}}</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        Pusher.logToConsole = true;
        var channel_name = 'user_'+{{\Illuminate\Support\Facades\Auth::user()->id}};
        Echo.channel(channel_name).listen('.user_notification', (response) => {
            reloadPage(response.body, response.title);
        });

        function reloadPage() {
            window.location.reload();
        }
    </script>
    <script src="{{asset('/Backend/vendors/DataTables/js/datatables.min.js')}}"></script>
    <script src="{{asset('/Backend/vendors/DataTables/js/dataTables.bootstrap4.min.js')}}"></script>
{{--    <script src="{{asset('Backend/')}}/js/pages/datatables.init.js"></script>--}}

    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: false,
                ajax: '{{route('list-manager-directory-list',['directory_id'=>$directory_id ?? ''])}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.spotlist_directory_id);
                },
                columns: [
                    {"data": "thumb_image"},
                    {"data": "title"},
                    {"data": "categories"},
                    {"data": "location"},
                    {"data": "status"},
                    {"data": "action"},
                ],
            });
        });

        $(document).on('click','.remove_directory', function (e) {
            e.preventDefault();
            Ladda.bind(this);
            var obj = $(this);
            var load = $(this).ladda();
            var id = $(this).data('id');
            var url = "{{url('remove-directory-file')}}";
            swalConfirm('Are You sure to delete this directory?').then(function (s) {
                if(s.value){
                    var data = {
                        value: id,
                        table_name: 'spotlist_directory',
                        primary_key: 'spotlist_directory_id'
                    };
                    makeAjaxPost(data, url, load).done(function(sresult){
                        $(obj).closest('tr').remove();
                        swalSuccess('Deleted Successfully');
                    });
                }
            });
        });

    </script>
@endsection
