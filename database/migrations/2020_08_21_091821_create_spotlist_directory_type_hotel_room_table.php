<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpotlistDirectoryTypeHotelRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spotlist_directory_type_hotel_room', function (Blueprint $table) {
            $table->increments('spotlist_directory_type_hotel_room_id');
            $table->unsignedInteger('spotlist_directory_id');
            $table->string('room_name');
            $table->decimal('room_price',10,2)->nullable();
            $table->text('room_description')->nullable();
            $table->string('room_image')->nullable();
            $table->text('room_facilities')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spotlist_directory_type_hotel_room');
    }
}
