<div class="card mt-3">
    <div class="card-body">
        <form action="{{route('user-password-update')}}" class="parsley-examples" method="POST" id="profile_password">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="pass1">{{__('Old Password')}}<span class="text-danger">*</span></label>
                        <input type="password" name="old_password" placeholder="Old Password" required
                               class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="pass1">{{__('New Password')}}<span class="text-danger">*</span></label>
                        <input id="pass1" type="password" minlength="6" name="password" placeholder="New Password" required
                               class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="passWord2">{{__('Confirm Password')}} <span class="text-danger">*</span></label>
                        <input data-parsley-equalto="#pass1" minlength="6" type="password" required name="confirm_password"
                               placeholder="Confirm Password" class="form-control" id="passWord2">
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="hidden" name="id" value="{{$user->id ?? ''}}">
                    <button type="submit" class="btn theme-btn submit_info"><i class="fa fa-key"></i> {{__('Change Password')}}</button>
                </div>

            </div>

        </form>
    </div>
</div>
