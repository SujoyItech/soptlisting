<div class="select-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="select-wrap">
                    <div class="select-area">
                        <form class="clearfix" action="{{route('home-page-search')}}" method="GET">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="select-sub">
                                        <span>{{__('Location')}}</span>
                                        <div class="form-group">
                                            <div id="filterDate2">
                                                <div class="input-group">
                                                    <select class="select wide selectpicker" data-live-search="true"  data-style="btn-light" name="location">
                                                        @if(isset($locations) && !empty($locations))
                                                            @foreach($locations as $location)
                                                                <option value="{{$location->spotlist_cities_id}}">{{$location->spotlist_cities_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="select-sub">
                                        <span> {{__('Category')}}</span>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select class="form-control select wide" name="category">
                                                    @if(isset($main_categories) && !empty($main_categories))
                                                        @foreach($main_categories as $category)
                                                            <option value="{{$category->spotlist_category_id}}">{{$category->spotlist_category_name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="select-sub select-sub-2">
                                        <button class="theme-btn-s2" type="submit">{{__('Search')}}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
